package com.choice.scm.service;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.UperSp_code;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Template;
import com.choice.scm.persistence.CodeDesMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.GrpTypMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.persistence.TemplateMapper;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.PublicExportExcel;
import com.choice.scm.util.PublicExportTemplate;

@Service
public class TemplateService {

	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired 
	private GrpTypMapper grpTypMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private TemplateMapper templateMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private CodeDesMapper codeDesMapper;
	private final transient Log log = LogFactory.getLog(SupplyService.class);
	

	

	/**
	 * author ghc
	 * @param template
	 * @return List<Template>
	 * @throws CRUDException
	 */
	public List<Template> getTemplate(Template template) throws CRUDException {
		try {
			return templateMapper.getTemplate(template);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * author ghc
	 * @param template
	 * @return
	 * @throws CRUDException
	 * 查找模版名称
	 */
	public String findTemplateName(Template template) throws CRUDException{
		try{
			return getTemplate(template).get(0).getRealName();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	
	
	/**
	 * 将文件上传到数据库查询出路径的文件夹下
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String uploadTemplate(HttpServletRequest request,HttpServletResponse response,String fileName) throws IOException {
		return PublicExportTemplate.uploadTemplate(request, response,fileName);
	}
	
	/**
	 * 对execl进行验证
	 */
	public List<String> check(String path, String accountId) throws CRUDException {
		boolean checkResult = ReadSupplyExcel.check(path, grpTypMapper,positnMapper,deliverMapper,codeDesMapper);
		Map<String, String> mapMsg = new HashMap<String, String>();
		if (checkResult) {
			List<Supply> supplyList = ReadSupplyExcel.SupplyList;
//			System.out.println("账套:"+supplyList.get(0).getAcct()+" 编码:"+supplyList.get(0).getSp_code()+" 名称:"+supplyList.get(0).getSp_name()+"  缩写:"+supplyList.get(0).getSp_init());
			saveSupplyList(supplyList, accountId);
		} else {
			mapMsg = ReadSupplyExcel.map;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
//		ReadSupplyExcel.map.clear();//清空错误信息
		return listError(mapMsg);
	}
	/**
	 * 返回导入的错误结果信息
	 */
	public List<String> listError(Map<String, String> map) {
		List<String> listError = new ArrayList<String>();
		if (map.size() != 0) {
			for (String key : map.keySet()) {
				listError.add(key);
				listError.add(map.get(key));
			}
		}
		return listError;
	}
	/**
	 * 读取操作文档详细列表
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public List<Map<String,Object>> showHelpFiles(HttpServletResponse response,HttpServletRequest request) throws IOException {
		String filePath = request.getSession().getServletContext().getRealPath("/")+ "\\" + "template\\";//如果是服务器上linux 换成/
		File f = new File(filePath);// 建立当前目录中文件的File对象
		File[] files = f.listFiles();// 取得目录中所有文件的File对象数组
		DecimalFormat df = new DecimalFormat("#.00"); 
		List<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		if(null != files){
			String len="";
			for(File file : files) {
				Map<String,Object> map = new HashMap<String,Object>();
				// 目录下的文件：
				if(file.isFile()) {
					//计算文件大小
					if(file.length()<1024){
						len = file.length()+"B"; 
					}else if(file.length() < 1048576){
						len = df.format((double)file.length()/1024)+"KB";
					}else if(file.length() < 1073741824){
						len = df.format((double)file.length()/1024/1024)+"MB";
					}else{
						len = df.format((double)file.length()/1024/1024/1024)+"GB";	
					}
					map.put("name",file.getName());
					map.put("len",len);
				}else if (file.isDirectory()) { 	// 目录下的目录：
				}
				fileList.add(map);
			}
		}
		return fileList;
	}
	
	/**
	 * 下载模板信息标准版
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplateStandard(HttpServletResponse response,HttpServletRequest request,String realName) throws IOException {
		//下载模板，调用公用方法，适用windows和linux，templete文件夹下文件的下载
		PublicExportTemplate.filePathUseStandard(response, request, realName);
	}
	
	/**
	 * 下载模板信息个性化版
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplateIndividualization(HttpServletResponse response,HttpServletRequest request,String realName) throws IOException {
		//下载模板，调用公用方法，适用windows和linux，templete文件夹下文件的下载
		 PublicExportTemplate.filePathUseIndividualization(response, request, realName);
	}
	
	/**
	 * 下载模板信息个性化版之前判断存不存在
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public String fileExitsOrNot(HttpServletResponse response,HttpServletRequest request,String realName) throws IOException {
		//下载模板，调用公用方法，适用windows和linux，templete文件夹下文件的下载
		return PublicExportTemplate.fileExitsOrNot(response, request, realName);
	}
	
	

	
	public void saveSupply(Supply supply) throws CRUDException {
		try {
//			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			supply.setSp_init(UperSp_code.getInit(supply.getSp_name()));//缩写转为大写
			supplyMapper.saveSupply(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public void saveSupplyNew(Supply supply) throws CRUDException {
		try {
//			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			if(null!=supply && "".equals(supply.getSp_init())){
				supply.setSp_init(UperSp_code.getInit(supply.getSp_name()));//缩写转为大写				
			}

			supplyMapper.saveSupply_new(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量保存物资信息
	 * 
	 * @param teacherInfoList
	 * @throws CRUDException
	 */
	private void saveSupplyList(List<Supply> supplyList, String accountId)
			throws CRUDException {
		for (Supply supply : supplyList) {
			Supply supply1 = new Supply();
			supply1.setSp_code(supply.getSp_code());
					
		}
	}
	

}
