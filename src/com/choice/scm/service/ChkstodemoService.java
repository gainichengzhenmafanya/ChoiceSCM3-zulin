package com.choice.scm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstodemo;
import com.choice.scm.persistence.ChkstodemoMapper;

@Service
public class ChkstodemoService {
	@Autowired
	private ChkstodemoMapper chkstodemoMapper;
	@Autowired
	private PageManager<Chkstodemo> pageManager;

	private final transient Log log = LogFactory.getLog(ChkstodemoService.class);
	
	/**
	 * 查找当前最大顺序号
	 * @return
	 * @throws CRUDException
	 */
	public int findMaxRec(Chkstodemo chkstodemo) throws CRUDException
	{
		try {
			return chkstodemoMapper.findMaxRec(chkstodemo);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找所有的申购单标题
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstodemo> findAllTitle(Chkstodemo chkstodemo) throws CRUDException
	{
		try {
			return chkstodemoMapper.findAllTitle(chkstodemo);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * ajax查询
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstodemo> findChkstodemoAjax(Chkstodemo chkstodemo) throws CRUDException
	{
		try {
			return chkstodemoMapper.findAllTitle(chkstodemo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 添加申购单
	 * @param sp_codes
	 * @throws CRUDException
	 */
	public void saveNewChkstodemo(Chkstodemo chkstodemo) throws CRUDException
	{
		try {
			chkstodemoMapper.saveNewChkstodemo(chkstodemo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询信息
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstodemo> findChkstodemo(Chkstodemo chkstodemo,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkstodemo, page, ChkstodemoMapper.class.getName()+".findChkstodemo");		
			//return chkstodemoMapper.findChkstodemo(chkstodemo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据分店查询信息
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstodemo> findChkstodemoForFirm(Chkstodemo chkstodemo,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkstodemo, page, ChkstodemoMapper.class.getName()+".findChkstodemoForFirm");		
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 模板数据查询
	 * @return
	 * @throws CRUDException
	 */
	public String findChkstodemoByRec(List<String> recList,List<String> cntList,List<String> cnt1List,String acct, String title) throws CRUDException
	{
		Map<String,Object> result = new HashMap<String,Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("recList", recList);
		map.put("acct", acct);
		map.put("title", title);
		try {
			List<Chkstodemo> chkstodemoList=chkstodemoMapper.findChkstodemoByRec(map);
			for (int i = 0; i < chkstodemoList.size(); i++) {
				Chkstodemo newChkstodemo=new Chkstodemo();
				newChkstodemo=chkstodemoList.get(i);
				newChkstodemo.setCnt(Double.parseDouble(cntList.get(i).toString()));
				newChkstodemo.setCnt1(Double.parseDouble(cnt1List.get(i).toString()));
				chkstodemoList.set(i, newChkstodemo);
			}
			result.put("chkstodemoList", chkstodemoList);
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 模板数据查询根据分店
	 * @return
	 * @throws CRUDException
	 */
	public String findChkstodemoForFirmByRec(List<String> recList, List<String> cntList, List<String> cnt1List, String acct, String firm, String title) throws CRUDException
	{
		Map<String,Object> result = new HashMap<String,Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("recList", recList);
		map.put("acct", acct);
		map.put("firm", firm);
		map.put("title", title);
		try {
			List<Chkstodemo> chkstodemoList=chkstodemoMapper.findChkstodemoForFirmByRec(map);
			for (int i = 0; i < chkstodemoList.size(); i++) {
				Chkstodemo newChkstodemo=new Chkstodemo();
				newChkstodemo=chkstodemoList.get(i);
				newChkstodemo.setCnt(Double.parseDouble(cntList.get(i).toString()));
				newChkstodemo.setCnt1(Double.parseDouble(cnt1List.get(i).toString()));
				chkstodemoList.set(i, newChkstodemo);
			}
			result.put("chkstodemoList", chkstodemoList);
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除申购信息
	 * @param sp_codes
	 * @throws CRUDException
	 */
	public void deleteChkstodemo(Map<String,Object> map) throws CRUDException
	{
		try {
			chkstodemoMapper.deleteChkstodemo(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
}
