package com.choice.scm.service;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.PositnMapper;

@Service
public class MonitService {

	@Autowired
	private PositnMapper positnMapper;
	
	/**
	 * 查询分店信息
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findMonit() throws CRUDException{
		try{
			List<Positn> positnList = positnMapper.findPositnMonit();
			for(Positn positn: positnList){
				String[] result = pingIP(positn.getIp()).split(",");
				positn.setIpPing(result[0]);
				positn.setIpTime(result[1]);
			}
			return positnList;
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * ping IP
	 * @param IP
	 * @return
	 * @throws InterruptedException 
	 */
	 public static String pingIP(String IP) throws IOException,InterruptedException {
		 if(null==IP || "".equals(IP)){
			 return " , ";
		 }
         Process process = Runtime.getRuntime().exec("ping "+IP+" -n 1 -w 1");
         int errorLevel = process.waitFor();
         InputStreamReader r = new InputStreamReader(process.getInputStream()); 
         LineNumberReader returnData = new LineNumberReader(r);
         String returnMsg="";  
         String line = "";  
         while ((line = returnData.readLine()) != null) {  
               returnMsg += line;  
         }
         if(errorLevel==1){
        	 return "<font color='red'>N</font>,连接超时";
         }else{  
        	 return "Y,延时 "+returnMsg.substring(returnMsg.lastIndexOf(" ")+1)+"";
         } 
	 }
}