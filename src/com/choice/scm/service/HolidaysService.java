package com.choice.scm.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Holiday;
import com.choice.scm.persistence.HolidaysMapper;

@Service
public class HolidaysService {

	private final transient Log log = LogFactory.getLog(HolidaysService.class);
	@Autowired
	private HolidaysMapper holidaysMapper;
	@Autowired
	private PageManager<Holiday> pageManager;
	@Autowired
	private PageManager<Map<String,Object>> pageManagerMap;
	
	/**
	 * 查找全节假日信息
	 * @param holiday
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Holiday> listHoliday(Holiday holiday, Page page) throws CRUDException {
		try{
			return pageManager.selectPage(holiday, page, HolidaysMapper.class.getName()+".listHoliday");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public List<Map<String, Object>> listHolidayMap(Holiday holiday, Page page) throws CRUDException {
		try{
			return pageManagerMap.selectPage(holiday, page, HolidaysMapper.class.getName()+".listHolidayMap");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 保存添加
	 * @param holiday
	 * @return
	 * @throws CRUDException
	 */
	public String saveByAddHoliday(Holiday holiday) throws CRUDException{
		try{
			if(holiday.getMemo()==null) {
				holiday.setMemo("");
			}
			holidaysMapper.saveByAdd(holiday);
			return "success";
		}catch(DuplicateKeyException e){
			log.error(e);
			return "节假日定义重复";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除节假日定义
	 * @param holiday
	 * @throws CRUDException
	 */
	public void deleteHoliday(String date) throws CRUDException {
		try{
			List<String> datList = Arrays.asList(date.split(","));
			for(int i=0;i<datList.size();i++){
				Holiday holiday = new Holiday();
				holiday.setDat(DateFormat.getDateByString(datList.get(i), "yyyy-MM-dd"));
				holidaysMapper.deleteHoliday(holiday);
			}
		}catch(Exception e){
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 自动生成周六日信息
	 * @param holiday
	 * @return
	 * @throws CRUDException
	 */
	public String saveBycreateHolidayAuto(Holiday holiday) throws CRUDException{
		try{
			if("".equals(holiday.getYear())){
				return "请选择年度！"; 
			}else{
				holidaysMapper.deleteHoliday(holiday);
				holidaysMapper.saveBycreateHolidayAuto(holiday);
				return "success";
			}
		}catch(DuplicateKeyException e){
			log.error(e);
			return "节假日定义重复";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 *查询当天是否节假日
	 */
	public Holiday findHolidayByDate(Holiday holiday) throws CRUDException{
		try{
			return holidaysMapper.findHolidayByDate(holiday);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 不分页查询所有节假日 wjf
	 * @param holiday
	 * @return
	 */
	public List<Holiday> listHoliday(Holiday holiday) throws Exception{
		return holidaysMapper.listHoliday(holiday);
	}
	
	/***
	 * 导出节假日报表
	 * @param os
	 * @param list
	 */
	public void exportHolidayService(ServletOutputStream os, List<Holiday> list) {
			WritableWorkbook workBook = null;
			WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
		            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
		            Colour.BLACK);
			WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
			try {
				titleStyle.setAlignment(Alignment.CENTRE);
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("节假日", 0);
				sheet.addCell(new Label(0, 0,"节假日表",titleStyle));
				sheet.mergeCells(0, 0, 3, 0);
				sheet.addCell(new Label(0, 1, "日期")); 
	            sheet.addCell(new Label(1, 1, "假期等级"));   
	            sheet.addCell(new Label(2, 1, "类型")); 
				sheet.addCell(new Label(3, 1, "备注"));  
	            //遍历list填充表格内容
				int index = 1;
	            for(int j=0; j<list.size(); j++) {
	            	if(j == list.size()){
	            		break;
	            	}
	            	index += 1;
	            	sheet.addCell(new Label(0, index, list.get(j).getSdat()));
	            	sheet.addCell(new Label(1, index, list.get(j).getGrade()));
	            	sheet.addCell(new Label(2, index, list.get(j).getTyp()));
	            	sheet.addCell(new Label(3, index, list.get(j).getMemo()));
		    	}	            
				workBook.write();
				os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (RowsExceededException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}	
}
