package com.choice.scm.service.reportFirm;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class LishiPandianService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	/**
	 * 历史盘点
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	public ReportObject<Map<String,Object>> findLishiPandian_bzb(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			String startNumber="";//期初数量
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
//			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			content.put("supplyAcct", sa);
			int month = Integer.parseInt(DateFormat.getStringByDate(sa.getBdat(),"yyyy-MM-dd").substring(5, 7));
			if(month>0){
				startNumber = "(";
				for(int i=0;i<month;i++){
					startNumber += "CAST(A.INC"+i+" AS NUMERIC(12, 2)) - CAST(A.OUTC"+i+" AS NUMERIC(12, 2)) +";
				}
				startNumber = startNumber.substring(0,startNumber.length()-1);
				startNumber += ")";
			}
			Date date = DateFormat.getDateByString(DateFormat.getStringByDate(sa.getBdat(),"yyyy-MM-dd").substring(0, 7)+"-01","yyyy-MM-dd");
			content.put("startNumber",startNumber);
			content.put("date",date);
//			List<Map<String,Object>> foot = lishiPandianMapper.findCalForSupplyInOutInfo(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findLishiPandian_bzb");
			mapReportObject.setRows(listInfo);
//			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
}
