package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;

@Service
public class RkMingxiHuizongFirmService {

	@Autowired
	private SupplyAcctFirmMapper rkMingxiHuizongFirmMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private PageManager<SupplyAcct> pageManager;
	
	/**
	 * 入库明细汇总
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkinDetailSum(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			if(null != ((SupplyAcct)conditions.get("supplyAcct")).getFirm()){
				((SupplyAcct)conditions.get("supplyAcct")).setFirm(CodeHelper.replaceCode(((SupplyAcct)conditions.get("supplyAcct")).getFirm()));
			}
			if(null != ((SupplyAcct)conditions.get("supplyAcct")).getDelivercode()){
				((SupplyAcct)conditions.get("supplyAcct")).setDelivercode(CodeHelper.replaceCode(((SupplyAcct)conditions.get("supplyAcct")).getDelivercode()));
			}
			List<Map<String,Object>> foot = rkMingxiHuizongFirmMapper.findCalForChkinDetailSum(conditions);
			reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctFirmMapper.class.getName()+".findChkinDetailSum"));
			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
