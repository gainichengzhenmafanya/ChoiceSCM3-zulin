package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class WzChengbenMingxiFirmService {

	@Autowired
	private SupplyAcctFirmMapper wzChengbenMingxiMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
		
	/**
	 * 物资成本明细表营业额获取
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public Map<String,Object> getNMoney(SupplyAcct supplyAcct) throws CRUDException{
		try{
			return wzChengbenMingxiMapper.getNMoney(supplyAcct);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 物资综合进出表
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findWzChengbenMingxi(Map<String,Object> conditions) throws CRUDException{
		try{			
			Map<String,Object> NMoney = (Map<String,Object>)conditions.get("listNMoney");
			conditions.put("nmoney", NMoney.get("NMONEY"));
			mapReportObject.setRows(wzChengbenMingxiMapper.findWzChengbenMingxi(conditions));
			List<Map<String,Object>> foot = wzChengbenMingxiMapper.findWzChengbenMingxiHeji(conditions);
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
