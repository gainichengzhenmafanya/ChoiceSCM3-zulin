package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;

@Service
public class SecondUnitService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	/**
	 * 第二单位查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findSecondUnit(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
//			List<Map<String,Object>> foot = secondUnitMapper.findSecondUnitHeji(conditions);
			SupplyAcct sa = (SupplyAcct)conditions.get("supplyAcct");
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			conditions.put("supplyAcct", sa);
			if(conditions.get("querytype").equals("2")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctFirmMapper.class.getName()+".findSecondUnit_date"));
			}else if(conditions.get("querytype").equals("1")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctFirmMapper.class.getName()+".findSecondUnit_supply"));
			}
//				mapReportObject.setFooter(foot);
				mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 每日差异对照表
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author CSS
	 */
	public ReportObject<Map<String,Object>> findSecondUnitCy(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			content.put("supplyAcct", sa);
			int days = Integer.parseInt(DateFormat.getStringByDate(sa.getEdat(), "yyyy-MM-dd").substring(DateFormat.getStringByDate(sa.getEdat(), "yyyy-MM-dd").length()-2));
			String strSql="";
			String day;
			for (int i=1;i<=days;i++){
				if (i<10){
					day="0"+i;
				}else{
					day=""+i;
				}
				strSql+= " sum(case when tt.CHECD = '"+day+"'  then cast(tt.jsyl-tt.qc-tt.COUNT+tt.thsl+tt.cxsl-tt.diaoru+tt.diaochu+tt.qm-tt.cpsh-tt.ylsh-tt.bcpsh-tt.wlSH AS Numeric(12,2)) else 0 end)as cy"+i+", ";
			}
			content.put("strSql", strSql);
//			List<Map<String,Object>> foot = meiriChayiDuizhaoService.findCalForSupplyInOutInfo(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findSecondUnitCy");
			mapReportObject.setRows(listInfo);
//			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
