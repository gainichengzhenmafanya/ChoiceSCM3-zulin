package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class CunhuoPandianService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired 
	private SupplyAcctFirmMapper cunhuoPandianMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	/**
	 * 存货盘点表
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	public ReportObject<Map<String,Object>> findCunhuoPandian(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			content.put("supplyAcct", sa);
			List<Map<String,Object>> foot = cunhuoPandianMapper.findCunhuoPandianHeji(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findCunhuoPandian");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
}
