package com.choice.scm.service.reportFirm;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class WzChengbenHuizongFirmService {

	@Autowired
	private SupplyAcctFirmMapper wzChengbenHuizongMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	
	/**
	 * 物资成本明细表营业额获取
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> getNMoneyList(SupplyAcct supplyAcct) throws CRUDException{
		try{
			return wzChengbenHuizongMapper.getNMoneyList(supplyAcct);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 物资综合进出表
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findWzChengbenHuizong(Map<String,Object> conditions) throws CRUDException{
		try{
			
//			mapReportObject.setRows(wzChengbenMingxiMapper.findWzChengbenMingxi(conditions));
//			List<Map<String,Object>> wzChengbenMingxi = wzChengbenMingxiMapper.findWzChengbenMingxi(conditions);
			List<Map<String,Object>> listNMoney = (List<Map<String,Object>>)conditions.get("listNMoney");
			List<Map<String,Object>> mapResult = new ArrayList<Map<String,Object>>();
			Map<String,Object> map = new HashMap<String,Object>();
			Double qc = 0.0;
			Double lh = 0.0;
			Double diaobo = 0.0;
			Double ylsh = 0.0;
			Double qm = 0.0;
			Double sjje = 0.0;
			Double llje = 0.0;
			Double cyje = 0.0;
			Double nmoney = 0.0;
			if (listNMoney != null && listNMoney.size()>0){
				for (Map<String,Object> moneyMap : listNMoney){
					conditions.put("dat", moneyMap.get("DWORKDATE"));
					conditions.put("nmoney", moneyMap.get("NMONEY"));
					map = wzChengbenHuizongMapper.findWzChengbenHuizong(conditions);
					qc+=((BigDecimal)map.get("QC")).doubleValue();
					lh+=((BigDecimal)map.get("LH")).doubleValue();
					diaobo+=((BigDecimal)map.get("DIAOBO")).doubleValue();
					ylsh+=((BigDecimal)map.get("YLSH")).doubleValue();
					qm+=((BigDecimal)map.get("QM")).doubleValue();
					sjje+=((BigDecimal)map.get("SJJE")).doubleValue();
					llje+=((BigDecimal)map.get("LLJE")).doubleValue();
					cyje+=((BigDecimal)map.get("CYJE")).doubleValue();
					nmoney+=((BigDecimal)moneyMap.get("NMONEY")).doubleValue();
					mapResult.add(map);
				}
			}
			mapReportObject.setRows(mapResult);
			Map<String,Object> mapFoot = new HashMap<String,Object>();
			DecimalFormat df = new DecimalFormat("######0.00");   
			mapFoot.put("TIME1", "合计");
			mapFoot.put("QC", df.format(qc));
			mapFoot.put("LH", df.format(lh));
			mapFoot.put("DIAOBO", df.format(diaobo));
			mapFoot.put("YLSH", df.format(ylsh));
			mapFoot.put("QM", df.format(qm));
			mapFoot.put("SJJE", df.format(sjje));
			mapFoot.put("SJZB", df.format(sjje/nmoney));
			mapFoot.put("LLJE", df.format(llje));
			mapFoot.put("LLZB", df.format(llje/nmoney));
			mapFoot.put("CYJE", df.format(cyje));
			mapFoot.put("CYZB", df.format(cyje/nmoney));
			List<Map<String,Object>> foot = new ArrayList<Map<String,Object>>();
			foot.add(mapFoot);
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
