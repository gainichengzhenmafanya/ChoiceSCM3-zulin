package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class DbHuizongService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 调拨汇总
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	public ReportObject<Map<String,Object>> findDbHuizong_bzb(Map<String,Object> content,Page pager) throws CRUDException{
		try{
//			List<Map<String,Object>> foot = lishiPandianMapper.findCalForSupplyInOutInfo(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findDbHuizong_bzb");
			mapReportObject.setRows(listInfo);
//			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
}
