package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class WzMingxiJinchuFirmService {

	@Autowired
	private SupplyAcctFirmMapper wzMingxiJinchuMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	/**
	 * 物资明细进出表
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author ZGL_ZANG
	 */
	public ReportObject<Map<String,Object>> findSupplyInOutInfo(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			List<Map<String,Object>> foot = wzMingxiJinchuMapper.findCalForSupplyInOutInfo(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findSupplyInOutInfo");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
}
