package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class YuemoPandianService {

	@Autowired
	private SupplyAcctFirmMapper yuemopandianMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	/**
	 * 月末盘点明细表
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	public ReportObject<Map<String,Object>> findYuemoPandian(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			sa.setGrptyp(CodeHelper.replaceCode(sa.getGrptyp()));
			sa.setGrp(CodeHelper.replaceCode(sa.getGrp()));
			sa.setTyp(CodeHelper.replaceCode(sa.getTyp()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			content.put("supplyAcct", sa);
			List<Map<String,Object>> foot = yuemopandianMapper.findYuemoPandianFoot(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findYuemoPandian");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 月末盘点汇总表
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author css
	 */
	public ReportObject<Map<String,Object>> findYuemoPandian2(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			sa.setGrptyp(CodeHelper.replaceCode(sa.getGrptyp()));
			sa.setGrp(CodeHelper.replaceCode(sa.getGrp()));
			sa.setTyp(CodeHelper.replaceCode(sa.getTyp()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			content.put("supplyAcct", sa);
			List<Map<String,Object>> foot = yuemopandianMapper.findYuemoPandianFoot2(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findYuemoPandian2");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
}
