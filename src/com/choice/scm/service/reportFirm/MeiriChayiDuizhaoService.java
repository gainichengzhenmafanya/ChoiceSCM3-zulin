package com.choice.scm.service.reportFirm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
/**
 * 门店日盘点状态表
 * @author 孙胜彬
 */
@Service
public class MeiriChayiDuizhaoService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired 
	private SupplyAcctFirmMapper supplyAcctFirmMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;

	/**
	 * 每日差异对照表
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author CSS
	 */
	public ReportObject<Map<String,Object>> findMeiriChayiDuizhao(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			content.put("supplyAcct", sa);
			int days = Integer.parseInt(DateFormat.getStringByDate(sa.getEdat(), "yyyy-MM-dd").substring(DateFormat.getStringByDate(sa.getEdat(), "yyyy-MM-dd").length()-2));
			String strSql="";
			String day;
			for (int i=1;i<=days;i++){
				if (i<10){
					day="0"+i;
				}else{
					day=""+i;
				}
				strSql+= " sum(case when tt.CHECD = '"+day+"'  then cast(tt.jsyl-tt.qc-tt.COUNT+tt.thsl+tt.cxsl-tt.diaoru+tt.diaochu+tt.qm-tt.cpsh-tt.ylsh-tt.bcpsh-tt.wlSH AS Numeric(12,2)) else 0 end)as cy"+i+", ";
			}
			content.put("strSql", strSql);
//			List<Map<String,Object>> foot = meiriChayiDuizhaoService.findCalForSupplyInOutInfo(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findMeiriChayiDuizhao");
			mapReportObject.setRows(listInfo);
//			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 每日差异对照表
	 * @param content
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author CSS
	 */
	public ReportObject<Map<String,Object>> findMeiriChayiDuizhao_bzb(Map<String,Object> content,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)content.get("supplyAcct");
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			content.put("year", mainInfoMapper.findYearrList().get(0));
			content.put("supplyAcct", sa);
			int days = Integer.parseInt(DateFormat.getStringByDate(sa.getEdat(), "yyyy-MM-dd").substring(DateFormat.getStringByDate(sa.getEdat(), "yyyy-MM-dd").length()-2));
			String strSql="";
			String day;
			for (int i=1;i<=days;i++){
				if (i<10){
					day="0"+i;
				}else{
					day=""+i;
				}
				strSql+= " sum(case when CHECD = "+day+" then  cyyl else 0 end)as cy"+i+", ";
			}
			content.put("strSql", strSql);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctFirmMapper.class.getName()+".findMeiriChayiDuizhao_bzb");
//			List<Map<String,Object>> listInfo = supplyAcctFirmMapper.findMeiriChayiDuizhao_bzb(content);
			mapReportObject.setRows(listInfo);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 动态生成表头
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object findHeader() throws CRUDException {
		try {
			List<String> str = new ArrayList<String>();
			for(int i=1;i<=31;i++){
				str.add("CY"+i);
			}
			return str;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
