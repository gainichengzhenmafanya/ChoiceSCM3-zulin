package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class WzZongheJinchubiaoFirmService {

	@Autowired
	private SupplyAcctFirmMapper wzZongheJinchubiaoMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	
	/**
	 * 物资综合进出表
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findSupplySumInOut(Map<String,Object> conditions) throws CRUDException{
		try{
			List<Map<String,Object>> foot = wzZongheJinchubiaoMapper.findCalForSupplySumInOut(conditions);
			mapReportObject.setRows(wzZongheJinchubiaoMapper.findSupplySumInOut(conditions));
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
