package com.choice.scm.service.reportFirm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class YueChengbenZongheFenxiService {

	@Autowired
	private SupplyAcctFirmMapper yueChengbenZongheFenxiMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
    private final transient Log log = LogFactory.getLog(YueChengbenZongheFenxiService.class);
	
	/**
	 * 月成本综合分析营业额获取
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> getNMoneyList(SupplyAcct supplyAcct,String ynck) throws CRUDException{
		try{
            if("Y".equals(ynck)) {
                return yueChengbenZongheFenxiMapper.findNMoneyList(supplyAcct);
            }else{
                return yueChengbenZongheFenxiMapper.getNMoneysList(supplyAcct);
            }
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
    /**
	 * 月成本综合分析营业额获取
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> getNMoneyList_hj(SupplyAcct supplyAcct,String ynck) throws CRUDException{
		try{
            if("Y".equals(ynck)) {
                return yueChengbenZongheFenxiMapper.findNMoneyList_hj(supplyAcct);
            }else{
                return null;//yueChengbenZongheFenxiMapper.getNMoneysList(supplyAcct);
            }
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 月成本综合分析
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findYueChengbenZongheFenxi(Map<String,Object> conditions) throws CRUDException{
		try{
			
//			mapReportObject.setRows(wzChengbenMingxiMapper.findWzChengbenMingxi(conditions));
//			List<Map<String,Object>> wzChengbenMingxi = wzChengbenMingxiMapper.findWzChengbenMingxi(conditions);
			List<Map<String,Object>> listNMoney = (List<Map<String,Object>>)conditions.get("listNMoney");
			List<Map<String,Object>> mapResult = new ArrayList<Map<String,Object>>();
			Map<String,Object> map = new HashMap<String,Object>();
			if (listNMoney != null && listNMoney.size()>0){
				for (Map<String,Object> moneyMap : listNMoney){
					conditions.put("dat", moneyMap.get("DWORKDATE"));
					conditions.put("firm", moneyMap.get("VSCODE"));
										
					map = yueChengbenZongheFenxiMapper.findYueChengbenZongheFenxi(conditions);
                    if(map==null){//如果获取的数据为空跳过
                        continue;
                    }
					//折前饭菜
					map.put("MONEY", moneyMap.get("MONEY"));
					//营业额
					map.put("YMONEY", moneyMap.get("YMONEY"));
					if (((BigDecimal)moneyMap.get("MONEY")).doubleValue()==0.0){
						map.put("ZQMLL", 0);
						map.put("CFYLZQFC", 0);
						map.put("CFTLZQFC", 0);
					}else{
						//折前毛利率
						map.put("ZQMLL", (((BigDecimal)moneyMap.get("MONEY")).doubleValue()-((BigDecimal)map.get("SJCB")).doubleValue())/((BigDecimal)moneyMap.get("MONEY")).doubleValue());
						//厨房原料占折前饭菜
						map.put("CFYLZQFC", (((BigDecimal)map.get("CFYL")).doubleValue())/((BigDecimal)moneyMap.get("MONEY")).doubleValue());
						//厨房调料占折前饭菜
						map.put("CFTLZQFC", (((BigDecimal)map.get("CFTL")).doubleValue())/((BigDecimal)moneyMap.get("MONEY")).doubleValue());
					}
					//综合毛利率
					if (((BigDecimal)moneyMap.get("YMONEY")).doubleValue()==0.0){
						map.put("ZHMLL", 0);
					}else{
						map.put("ZHMLL", (((BigDecimal)moneyMap.get("YMONEY")).doubleValue()-((BigDecimal)map.get("SJCB")).doubleValue())/((BigDecimal)moneyMap.get("YMONEY")).doubleValue());
					}
					//吧台收入
					map.put("BTSR", moneyMap.get("BTSR"));
					//厨房电费
					map.put("CFDF", moneyMap.get("CFDF"));
					//天然气
					map.put("TRQ", moneyMap.get("TRQ"));
					//卖出去的菜道数
					map.put("COUNT", moneyMap.get("COUNT"));
					if (((BigDecimal)moneyMap.get("COUNT")).doubleValue()==0.0){
						map.put("CJTRQ", 0);
						map.put("RJSF", 0);
					}else{
						//菜均天然气
						map.put("CJTRQ", (((BigDecimal)moneyMap.get("TRQ")).doubleValue())/((BigDecimal)moneyMap.get("COUNT")).doubleValue());
						//人均水费
						map.put("RJSF", (((BigDecimal)map.get("SF")).doubleValue())/((BigDecimal)moneyMap.get("COUNT")).doubleValue());
					}
					//职工餐
					map.put("ZGC", moneyMap.get("ZGC"));
					if (((BigDecimal)moneyMap.get("YGS")).doubleValue()==0.0){
						map.put("RJZGC", 0);
					}else{
						//人均职工餐
						map.put("RJZGC", (((BigDecimal)moneyMap.get("zgc")).doubleValue())/((BigDecimal)moneyMap.get("YGS")).doubleValue());
					}
					//客流
					map.put("KL", moneyMap.get("KL"));
					//员工数
					map.put("YGS", moneyMap.get("YGS"));
					mapResult.add(map);
				}
			}
			mapReportObject.setRows(mapResult);
			return mapReportObject;
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}

    /**
     * 月成本综合分析 YNKC=Y
     * @param supplyAcct
     * @return
     * @throws CRUDException
     */
    public ReportObject<Map<String,Object>> findYueChengbenZongheFenxi_Y(SupplyAcct supplyAcct) throws CRUDException{
        try{
            mapReportObject=new ReportObject<Map<String, Object>>();
            if(supplyAcct.getFirm()!=null&&supplyAcct.getFirm().trim()!=""){
                String firms[]=supplyAcct.getFirm().split(",");
                if(firms!=null&&firms.length>0&&firms[0]!="") {
                    supplyAcct.setFirmList(Arrays.asList(firms));
                }
            }
            if(supplyAcct.getFirmList()==null||supplyAcct.getFirmList().size()<=0){
                return null;
            }
            List<Map<String,Object>> listNMoney = getNMoneyList(supplyAcct, "Y");
            List<Map<String,Object>> chengben = yueChengbenZongheFenxiMapper.findYueChengbenZongheFenxi_chengb(supplyAcct);
            List<Map<String,Object>> sdq = yueChengbenZongheFenxiMapper.findYueChengbenZongheFenxi_sdq(supplyAcct);
            if((chengben!=null&&chengben.size()>0)||(sdq!=null&&sdq.size()>0)) {
                List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
                if (listNMoney != null && listNMoney.size() > 0) {
                    for (Map<String, Object> moneyMap : listNMoney) {
                        if(chengben!=null){
                            for(int i=0;i<chengben.size();i++){
                                Map<String,Object> cbMap=chengben.get(i);
                                if(cbMap.get("CODE").equals(moneyMap.get("CODE"))&&cbMap.get("DAT").equals(moneyMap.get("DAT"))) {
                                    moneyMap.putAll(cbMap);
                                    if(string2Big(String.valueOf(moneyMap.get("YMONEY"))).doubleValue()!=0) {
                                        //折后毛利
                                        moneyMap.put("APROFITS", string2Big(String.valueOf(moneyMap.get("YMONEY")))
                                                .subtract(string2Big(String.valueOf(cbMap.get("TOTALCOST"))))
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(String.valueOf(moneyMap.get("YMONEY"))), 4).multiply(BigDecimal.valueOf(100)));

                                    }else{
                                        moneyMap.put("APROFITS",0);
                                    }
                                    if(string2Big(String.valueOf(moneyMap.get("MONEY"))).doubleValue()!=0) {
                                        //折前毛利
                                        moneyMap.put("BPROFITS", string2Big(String.valueOf(moneyMap.get("MONEY")
                                                )).subtract(string2Big(String.valueOf(cbMap.get("TOTALCOST"))))
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(String.valueOf(moneyMap.get("MONEY"))), 4).multiply(BigDecimal.valueOf(100)));
                                    }else {
                                        moneyMap.put("BPROFITS",0);
                                    }
                                    if(string2Big(String.valueOf(moneyMap.get("TURNOVER"))).doubleValue()!=0){
                                        BigDecimal money=string2Big(String.valueOf(moneyMap.get("TURNOVER")));
                                        moneyMap.put("MATCOST_RATE",string2Big(String.valueOf(cbMap.get("MATCOST"))).multiply(BigDecimal.valueOf(100)).divide(money,4));
                                        moneyMap.put("FLACOST_RATE",string2Big(String.valueOf(cbMap.get("FLACOST"))).multiply(BigDecimal.valueOf(100)).divide(money,4));
                                        moneyMap.put("MERCOST_RATE",string2Big(String.valueOf(cbMap.get("MERCOST"))).multiply(BigDecimal.valueOf(100)).divide(money,4));
                                    }else {
                                        moneyMap.put("MATCOST_RATE",0);
                                        moneyMap.put("FLACOST_RATE",0);
                                        moneyMap.put("MERCOST_RATE",0);
                                    }
                                }
                            }
                        }else{
                            if(string2Big(String.valueOf(moneyMap.get("MONEY"))).doubleValue()!=0) {
                                moneyMap.put("APROFITS", string2Big(String.valueOf(moneyMap.get("MONEY"))).divide(string2Big(String.valueOf(moneyMap.get("NETSALES"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else{
                                moneyMap.put("APROFITS",0);
                            }

                            if(string2Big(String.valueOf(moneyMap.get("YMONEY"))).doubleValue()!=0) {
                                moneyMap.put("BPROFITS", string2Big(String.valueOf(moneyMap.get("YMONEY"))).divide(string2Big(String.valueOf(moneyMap.get("NETSALES"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else{
                                moneyMap.put("BPROFITS",0);
                            }
                        }
                        if(sdq!=null){
                            for(int i=0;i<sdq.size();i++){
                                Map<String,Object> sdqMap=sdq.get(i);
                                if(sdqMap.get("CODE").equals(moneyMap.get("CODE"))&&sdqMap.get("DAT").equals(moneyMap.get("DAT"))) {
                                    moneyMap.putAll(sdqMap);
                                }
                            }
                        }
                        mapResult.add(moneyMap);
                    }
                }
                mapReportObject.setRows(mapResult);
                mapReportObject.setFooter(findYueChengbenZongheFenxi_Y_hj(supplyAcct));
                return mapReportObject;
            }else{
                if (listNMoney != null && listNMoney.size() > 0) {
                    for (Map<String, Object> moneyMap : listNMoney) {
                        moneyMap.put("BPROFITS",100);
                        moneyMap.put("APROFITS",100);
                    }
                }
                mapReportObject.setRows(listNMoney);
                mapReportObject.setFooter(findYueChengbenZongheFenxi_Y_hj(supplyAcct));
                return mapReportObject;
            }
        }catch(Exception e){
            log.error(e.getMessage());
            throw new CRUDException(e);
        }
    }

    /**
     * 月成本综合分析 合计 YNKC=Y
     * @param supplyAcct
     * @return
     * @throws CRUDException
     */
    public List<Map<String,Object>> findYueChengbenZongheFenxi_Y_hj(SupplyAcct supplyAcct) throws CRUDException{
        try{
            if(supplyAcct.getFirm()!=null&&supplyAcct.getFirm().trim()!=""){
                String firms[]=supplyAcct.getFirm().split(",");
                if(firms!=null&&firms.length>0&&firms[0]!="") {
                    supplyAcct.setFirmList(Arrays.asList(firms));
                }
            }
            if(supplyAcct.getFirmList()==null||supplyAcct.getFirmList().size()<=0){
                return null;
            }
            List<Map<String,Object>> listNMoney = getNMoneyList_hj(supplyAcct, "Y");
            List<Map<String,Object>> chengben = yueChengbenZongheFenxiMapper.findYueChengbenZongheFenxi_chengb_hj(supplyAcct);
            List<Map<String,Object>> sdq = yueChengbenZongheFenxiMapper.findYueChengbenZongheFenxi_sdq_hj(supplyAcct);
            if((chengben!=null&&chengben.size()>0)||(sdq!=null&&sdq.size()>0)) {
                List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
                if (listNMoney != null && listNMoney.size() > 0) {
                    for (Map<String, Object> moneyMap : listNMoney) {
                        if(chengben!=null){
                            for(int i=0;i<chengben.size();i++){
                                Map<String,Object> cbMap=chengben.get(i);
                                moneyMap.putAll(cbMap);
                                if(string2Big(String.valueOf(moneyMap.get("YMONEY"))).doubleValue()!=0) {
                                    //折后毛利
                                    moneyMap.put("APROFITS", string2Big(String.valueOf(moneyMap.get("YMONEY")))
                                            .subtract(string2Big(String.valueOf(cbMap.get("TOTALCOST"))))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("YMONEY"))), 4).multiply(BigDecimal.valueOf(100)));

                                }else{
                                    moneyMap.put("APROFITS",0);
                                }
                                if(string2Big(String.valueOf(moneyMap.get("MONEY"))).doubleValue()!=0) {
                                    //折前毛利
                                    moneyMap.put("BPROFITS", string2Big(String.valueOf(moneyMap.get("MONEY")
                                            )).subtract(string2Big(String.valueOf(cbMap.get("TOTALCOST"))))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("MONEY"))), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("BPROFITS",0);
                                }
                                if(string2Big(String.valueOf(moneyMap.get("TURNOVER"))).doubleValue()!=0){
                                    BigDecimal money=string2Big(String.valueOf(moneyMap.get("TURNOVER")));
                                    moneyMap.put("MATCOST_RATE",string2Big(String.valueOf(cbMap.get("MATCOST"))).multiply(BigDecimal.valueOf(100)).divide(money,4));
                                    moneyMap.put("FLACOST_RATE",string2Big(String.valueOf(cbMap.get("FLACOST"))).multiply(BigDecimal.valueOf(100)).divide(money,4));
                                    moneyMap.put("MERCOST_RATE",string2Big(String.valueOf(cbMap.get("MERCOST"))).multiply(BigDecimal.valueOf(100)).divide(money,4));
                                }else {
                                    moneyMap.put("MATCOST_RATE",0);
                                    moneyMap.put("FLACOST_RATE",0);
                                    moneyMap.put("MERCOST_RATE",0);
                                }
                            }
                        }else{
                            if(string2Big(String.valueOf(moneyMap.get("MONEY"))).doubleValue()!=0) {
                                moneyMap.put("APROFITS", string2Big(String.valueOf(moneyMap.get("MONEY"))).divide(string2Big(String.valueOf(moneyMap.get("NETSALES"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else{
                                moneyMap.put("APROFITS",0);
                            }

                            if(string2Big(String.valueOf(moneyMap.get("YMONEY"))).doubleValue()!=0) {
                                moneyMap.put("BPROFITS", string2Big(String.valueOf(moneyMap.get("YMONEY"))).divide(string2Big(String.valueOf(moneyMap.get("NETSALES"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else{
                                moneyMap.put("BPROFITS",0);
                            }
                        }
                        if(sdq!=null){
                            for(int i=0;i<sdq.size();i++){
                                moneyMap.putAll(sdq.get(i));
                            }
                        }
                        mapResult.add(moneyMap);
                    }
                }
                return mapResult;
            }else{
                if (listNMoney != null && listNMoney.size() > 0) {
                    for (Map<String, Object> moneyMap : listNMoney) {
                        moneyMap.put("BPROFITS",100);
                        moneyMap.put("APROFITS",100);
                    }
                }
                return listNMoney;
            }
        }catch(Exception e){
            log.error(e.getMessage());
            throw new CRUDException(e);
        }
    }
    private BigDecimal string2Big(String val){
    	BigDecimal bigDecimal = new BigDecimal("0");
    	try{
    		bigDecimal = new BigDecimal((val!=null&&val.trim()!="")?val:"0");
    	}
    	catch(Exception e){
    	}
    	return bigDecimal;
    }
    
    /**
	 * 月成本综合分析
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findYueChengbenZongheFenxiChoice3(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)conditions.get("supplyAcct");
			List<Map<String,Object>> listNMoney = yueChengbenZongheFenxiMapper.findNMoneyListChoice3(supplyAcct);//查营业数据
			List<Map<String,Object>> chengben = yueChengbenZongheFenxiMapper.findYueChengbenZongheFenxi_chengbChoice3(supplyAcct);//成本
            List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
            if (listNMoney != null && listNMoney.size() > 0) {
                for (Map<String, Object> moneyMap : listNMoney) {
                    if(chengben!=null && chengben.size() > 0){
                        for(int i=0;i<chengben.size();i++){
                            Map<String,Object> cbMap=chengben.get(i);
                            if(cbMap.get("CODE").equals(moneyMap.get("CODE")) && cbMap.get("DAT").equals(moneyMap.get("DAT"))) {
                                moneyMap.putAll(cbMap);
                                //（折前饭菜收入-厨房原料-厨房调料）/折前饭菜收入*100%
                                //1收入|折前饭菜毛利率  zqmll （折前饭菜-厨房成本）/折前饭菜*100  未算
                                if(string2Big(String.valueOf(moneyMap.get("ZQFC"))).doubleValue()!=0) {
                                	if("西贝莜面".equals(String.valueOf(conditions.get("acctDes"))))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
                                		moneyMap.put("ZQMLL", string2Big(String.valueOf(moneyMap.get("ZQFC"))).divide(BigDecimal.valueOf(1.06),4)
                                				.subtract(string2Big(String.valueOf(cbMap.get("CFCB"))))
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
                                	else
                                		moneyMap.put("ZQMLL", string2Big(String.valueOf(moneyMap.get("ZQFC"))).subtract(string2Big(String.valueOf(cbMap.get("CFCB"))))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("ZQMLL",0);
                                }
                                //（营收净额-厨房原调料-吧台展台原料）/营收净额*100%
                                //2.收入|综合毛利率     （营业额-实际成本）/营业额*100
                                if(string2Big(String.valueOf(moneyMap.get("YYE"))).doubleValue()!=0) {
                                	if("西贝莜面".equals(String.valueOf(conditions.get("acctDes"))))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
                                		moneyMap.put("ZHMLL", string2Big(String.valueOf(moneyMap.get("YYE"))).divide(BigDecimal.valueOf(1.06),4)
                                				.subtract(string2Big(String.valueOf(cbMap.get("CFBTCB"))))
                                                .setScale(4, BigDecimal.ROUND_HALF_UP)
                                                .divide(string2Big(String.valueOf(moneyMap.get("YYE"))), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
                                	else
                                		moneyMap.put("ZHMLL", string2Big(String.valueOf(moneyMap.get("YYE"))).subtract(string2Big(String.valueOf(cbMap.get("CFBTCB"))))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("YYE"))), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("ZHMLL",0);
                                }
                                //3.厨房原料占折前饭菜   cfylzqfc
                                if(string2Big(String.valueOf(moneyMap.get("ZQFC"))).doubleValue()!=0) {
                                    moneyMap.put("CFYLZQFC", string2Big(String.valueOf(cbMap.get("CFYL")))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("CFYLZQFC",0);
                                }
                                //4.厨房调料占折前饭菜   cftlzqfc
                                if(string2Big(String.valueOf(moneyMap.get("ZQFC"))).doubleValue()!=0) {
                                    moneyMap.put("CFTLZQFC", string2Big(String.valueOf(cbMap.get("CFTL")))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(100)));
                                }else {
                                    moneyMap.put("CFTLZQFC",0);
                                }
                                //5.成本|职工餐人均 rjzgc = 职工餐/人数
                                if(string2Big(String.valueOf(moneyMap.get("YGS"))).doubleValue()!=0) {
                                    moneyMap.put("ZGCRJ", string2Big(String.valueOf(cbMap.get("ZGC"))).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("YGS"))), 4));
                                }else {
                                    moneyMap.put("ZGCRJ",0);
                                }
                                
                                //6.菜均天然气用量 cjtrq
                                if(string2Big(String.valueOf(moneyMap.get("FCFS"))).doubleValue()!=0) {
                                    moneyMap.put("CJTRQ", string2Big(String.valueOf(moneyMap.get("TRQ"))).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("FCFS"))), 4));
                                }else {
                                    moneyMap.put("CJTRQ",0);
                                }
                                //7.费用|人均水费 rjsf
                                if(string2Big(String.valueOf(moneyMap.get("YGS"))).doubleValue()!=0) {
                                    moneyMap.put("RJSF", string2Big(String.valueOf(moneyMap.get("SF"))).setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("YGS"))), 4));
                                }else {
                                    moneyMap.put("RJSF",0);
                                }
                            }
                        }
                    }
                    mapResult.add(moneyMap);
                }
            }
            mapReportObject.setRows(mapResult);
            mapReportObject.setFooter(findYueChengbenZongheFenxi_hjChoice3(conditions));
            return mapReportObject;
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 月成本综合分析
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findYueChengbenZongheFenxi_hjChoice3(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)conditions.get("supplyAcct");
			List<Map<String,Object>> listNMoney = yueChengbenZongheFenxiMapper.findNMoneyList_hjChoice3(supplyAcct);//查营业数据
			List<Map<String,Object>> chengben = yueChengbenZongheFenxiMapper.findYueChengbenZongheFenxi_chengb_hjChoice3(supplyAcct);//成本
            List<Map<String, Object>> mapResult = new ArrayList<Map<String, Object>>();
            if (listNMoney != null && listNMoney.size() > 0) {
                for (Map<String, Object> moneyMap : listNMoney) {
                    if(chengben!=null && chengben.size() > 0 && chengben.get(0).get("CFCB") != null){
                        for(int i=0;i<chengben.size();i++){
                            Map<String,Object> cbMap=chengben.get(i);
                            moneyMap.putAll(cbMap);
                            //1收入|折前饭菜毛利率  zqmll （折前饭菜-实际成本）/折前饭菜*100  未算
                            if(string2Big(String.valueOf(moneyMap.get("ZQFC"))).doubleValue()!=0) {
                            	if("西贝莜面".equals(String.valueOf(conditions.get("acctDes"))))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
                            		moneyMap.put("ZQMLL", string2Big(String.valueOf(moneyMap.get("ZQFC"))).divide(BigDecimal.valueOf(1.06),4)
                            				.subtract(string2Big(String.valueOf(cbMap.get("CFCB"))))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
                            	else
                            		moneyMap.put("ZQMLL", string2Big(String.valueOf(moneyMap.get("ZQFC")
                                        )).subtract(string2Big(String.valueOf(cbMap.get("CFCB"))))
                                        .setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("ZQMLL",0);
                            }
                            //2.收入|综合毛利率     （营业额-实际成本）/营业额*100
                            if(string2Big(String.valueOf(moneyMap.get("YYE"))).doubleValue()!=0) {
                            	if("西贝莜面".equals(String.valueOf(conditions.get("acctDes"))))//如果是西贝  计算折前饭菜毛利率使用的收入=折前饭菜收入/1.06；计算综合毛利率使用的收入=营收净额/1.06
                            		moneyMap.put("ZHMLL", string2Big(String.valueOf(moneyMap.get("YYE"))).divide(BigDecimal.valueOf(1.06),4)
                            				.subtract(string2Big(String.valueOf(cbMap.get("CFBTCB"))))
                                            .setScale(4, BigDecimal.ROUND_HALF_UP)
                                            .divide(string2Big(String.valueOf(moneyMap.get("YYE"))), 4).multiply(BigDecimal.valueOf(1.06)).multiply(BigDecimal.valueOf(100)));
                            	else
                            		moneyMap.put("ZHMLL", string2Big(String.valueOf(moneyMap.get("YYE")
                                        )).subtract(string2Big(String.valueOf(cbMap.get("CFBTCB"))))
                                        .setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(String.valueOf(moneyMap.get("YYE"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("ZHMLL",0);
                            }
                            //3.厨房原料占折前饭菜   cfylzqfc
                            if(string2Big(String.valueOf(moneyMap.get("ZQFC"))).doubleValue()!=0) {
                                moneyMap.put("CFYLZQFC", string2Big(String.valueOf(cbMap.get("CFYL")))
                                        .setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("CFYLZQFC",0);
                            }
                            //4.厨房调料占折前饭菜   cftlzqfc
                            if(string2Big(String.valueOf(moneyMap.get("ZQFC"))).doubleValue()!=0) {
                                moneyMap.put("CFTLZQFC", string2Big(String.valueOf(cbMap.get("CFTL")))
                                        .setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(String.valueOf(moneyMap.get("ZQFC"))), 4).multiply(BigDecimal.valueOf(100)));
                            }else {
                                moneyMap.put("CFTLZQFC",0);
                            }
                            //5.成本|职工餐人均 rjzgc = 职工餐/人数
                            if(string2Big(String.valueOf(moneyMap.get("YGS"))).doubleValue()!=0) {
                                moneyMap.put("ZGCRJ", string2Big(String.valueOf(cbMap.get("ZGC"))).setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(String.valueOf(moneyMap.get("YGS"))), 4));
                            }else {
                                moneyMap.put("ZGCRJ",0);
                            }
                            
                            //6.菜均天然气用量 cjtrq
                            if(string2Big(String.valueOf(moneyMap.get("FCFS"))).doubleValue()!=0) {
                                moneyMap.put("CJTRQ", string2Big(String.valueOf(moneyMap.get("TRQ"))).setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(String.valueOf(moneyMap.get("FCFS"))), 4));
                            }else {
                                moneyMap.put("CJTRQ",0);
                            }
                            //7.费用|人均水费 rjsf
                            if(string2Big(String.valueOf(moneyMap.get("YGS"))).doubleValue()!=0) {
                                moneyMap.put("RJSF", string2Big(String.valueOf(moneyMap.get("SF"))).setScale(4, BigDecimal.ROUND_HALF_UP)
                                        .divide(string2Big(String.valueOf(moneyMap.get("YGS"))), 4));
                            }else {
                                moneyMap.put("RJSF",0);
                            }
                        }
                    }
                    mapResult.add(moneyMap);
                }
            }
            return mapResult;
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}
