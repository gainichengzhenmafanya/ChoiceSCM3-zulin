package com.choice.scm.service.reportFirm;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class WzCangkuJinchubiaoFirmService {

	@Autowired
	private SupplyAcctFirmMapper wzCangkuJinchubiaoMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;	

	/**
	 * 物资仓库进出表
	 * @return
	 */
	public ReportObject<Map<String,Object>> findGoodsStoreInout(Map<String,Object> conditions,Page pager){
		String withoutZero="";
		if(conditions.get("noZero")!=null && !conditions.get("noZero").equals("") && conditions.get("noZero").equals("noZero")){
			withoutZero = " AND (AMTBLA+SUBTOTAL!=0 OR AMTIN!=0 OR AMTOUT!=0 OR AMTBLA+SUBTOTAL+AMTIN-AMTOUT!=0)";
		}
		conditions.put("withoutZero", withoutZero);
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctFirmMapper.class.getName()+".findGoodsStoreInout"));
		List<Map<String,Object>> foot = wzCangkuJinchubiaoMapper.findCalForGoodsStoreInout(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
}
