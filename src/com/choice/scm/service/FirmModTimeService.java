package com.choice.scm.service;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.FirmModTime;
import com.choice.scm.persistence.FirmModTimeMapper;

@Service
public class FirmModTimeService {

	@Autowired
	private FirmModTimeMapper firmModTimeMapper;
	@Autowired
	private PageManager<FirmModTime> pageManager;
	private final transient static Log log = LogFactory.getLog(FirmModTimeService.class);
	
	/**
	 * 查询成本卡方案
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmModTime> findFirmModTime(FirmModTime firmModTime) throws CRUDException{
		try{
			return firmModTimeMapper.findFirmModTime(firmModTime);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询成本卡方案(带分页)
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmModTime> findFirmModTime(FirmModTime firmModTime, Page page) throws CRUDException{
		try{
			return pageManager.selectPage(firmModTime, page, FirmModTimeMapper.class.getName()+".findFirmModTime");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 查询成本卡类型
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<CodeDes> findMod(FirmModTime firmModTime) throws CRUDException{
		try{
			return firmModTimeMapper.findMod(firmModTime);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增保存成本卡方案
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public void saveFirmModTime(FirmModTime firmModTime) throws CRUDException{
		try{
			String[] mods = firmModTime.getModCode().split(",");
			for(String mod:mods){
				firmModTime.setId(CodeHelper.createUUID());
				firmModTime.setModCode(mod);
				firmModTime.setBmonth(1);
				firmModTime.setEmonth(12);
				firmModTimeMapper.saveFirmModTime(firmModTime);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除成本卡方案
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public void deleteFirmModTime(String ids) throws CRUDException{
		try{
			FirmModTime firmModTime = new FirmModTime();
			firmModTime.setId(CodeHelper.replaceCode(ids));
			firmModTimeMapper.deleteFirmModTime(firmModTime);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新成本卡方案
	 * @param positn
	 * @throws CRUDException 
	 */
	public void updateFirmModTime(FirmModTime firmModTime) throws CRUDException{
		try{
			firmModTimeMapper.updateFirmModTime(firmModTime);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}