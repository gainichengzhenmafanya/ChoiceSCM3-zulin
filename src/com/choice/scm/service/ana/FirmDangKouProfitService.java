package com.choice.scm.service.ana;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.CostProfit;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.ana.FirmDangKouProfitMapper;

@Service
public class FirmDangKouProfitService {

	@Autowired
	private FirmDangKouProfitMapper firmDangKouProfitMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	private final static int MAXNUM = 30000;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
 
	public List<Map<String,Object>> findFirmDangKouProfit(CostProfit costProfit,Page page){
		costProfit.setDept(CodeHelper.replaceCode(costProfit.getDept()));
		StringBuffer sumF = new StringBuffer();
		StringBuffer caseF = new StringBuffer();
		
		//得到档口
		Positn p = new Positn();
		p.setUcode(costProfit.getPositn());
		List<Positn> deptList = positnMapper.findPositn(p);
		String functionStr = "IFNULL";
        
		//拼sql
		if(null != deptList && deptList.size() != 0){
			for(Positn f : deptList){
		        sumF.append(" "+functionStr+"(ROUND(SUM(A_"+f.getCode()+"),2),0) AS A_"+f.getCode()+",");
		        sumF.append(" "+functionStr+"(ROUND(SUM(C_"+f.getCode()+"),2),0) AS C_"+f.getCode()+",");
		        sumF.append(" CONCAT("+functionStr+"(ROUND(SUM(M_"+f.getCode()+"),2),0),'%') AS M_"+f.getCode()+",");
		        caseF.append(" CASE WHEN AA.DEPT1='"+f.getCode()+"' THEN AA.AMT END AS A_"+f.getCode()+",");
		        caseF.append(" CASE WHEN AA.DEPT1='"+f.getCode()+"' THEN AA.COST END AS C_"+f.getCode()+",");
		        caseF.append(" CASE WHEN AA.DEPT1='"+f.getCode()+"' THEN AA.ML END AS M_"+f.getCode()+",");
			}
			costProfit.setSumF(sumF.substring(0, sumF.lastIndexOf(",")));
			costProfit.setCaseF(caseF.substring(0, caseF.lastIndexOf(",")));
			if(null == page){
				reportObject.setRows(firmDangKouProfitMapper.findFirmDangKouProfit(costProfit));
			}else{
				reportObject.setRows(pageManager.selectPage(costProfit,page,FirmDangKouProfitMapper.class.getName()+".findFirmDangKouProfit"));
				reportObject.setTotal(page.getCount());
			}
			
			//合计毛利取平均值
			costProfit.setSumF(costProfit.getSumF().replaceAll("CONCAT\\("+functionStr+"\\(ROUND\\(SUM\\(M", "CONCAT\\("+functionStr+"\\(ROUND\\(AVG\\(M"));
			//查询
			List<Map<String,Object>> foot = firmDangKouProfitMapper.findFirmDangKouProfitSum(costProfit);
			reportObject.setFooter(foot);
			List<Map<String,Object>> datas = reportObject.getRows();
			datas.addAll(reportObject.getFooter());
			return datas;
		}else{
			return null;
		}
	}
	
	/**
	 * 查询 表头信息
	 * @param acct
	 * @param positnType
	 * @param codes
	 */
	public List<Positn> findTableHead(CostProfit costProfit){
		Positn p = new Positn();
		p.setUcode(costProfit.getPositn().replaceAll("'", ""));
		List<Positn> firmDeptList = positnMapper.findPositn(p);
		return firmDeptList;
	}
    /**
	 * 查询 表头信息
	 */
	public List<Positn> findTableHead_storedept(CostProfit costProfit){
		Positn p = new Positn();
		p.setUcode(costProfit.getPositn().replaceAll("'", ""));
		List<Positn> firmDeptList = positnMapper.findPositn(p);
		return firmDeptList;
	}
	
	/**
	 * 导出数据
	 * @param os
	 * @param list
	 * @param costProfit
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportExcel(OutputStream os,List<Map<String,Object>> list, CostProfit costProfit) throws CRUDException {   
		WritableWorkbook workBook = null;
		//得到档口
		List<Positn> deptList = findTableHead_storedept(costProfit);
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				sheet.addCell(new Label(0, 0,"分店档口毛利"));
	            sheet.addCell(new Label(0, 1, "周次/月"));   
	            sheet.addCell(new Label(1, 1, "起止日期")); 
				for (int k=0;k<deptList.size();k++) {
					sheet.addCell(new Label(k*3+2, 1, deptList.get(k).getDes()+"销售"));
					sheet.addCell(new Label(k*3+1+2, 1, deptList.get(k).getDes()+"成本"));
					sheet.addCell(new Label(k*3+2+2, 1, deptList.get(k).getDes()+"毛利"));
				}
				
	            //遍历list填充表格内容
				int index = 0;
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	sheet.addCell(new Label(0, index+2, list.get(j).get("WEEKNO").toString()));
	            	sheet.addCell(new Label(1, index+2, null == list.get(j).get("DES") ? "":list.get(j).get("DES").toString()));
	            	for (int k=0;k<deptList.size();k++) {
	            		String amt = "";
	            		String cnt = "";
	            		String ml = "";
	            		if (list.get(j).get("A_"+deptList.get(k).getCode())!=null) {
	            			amt = list.get(j).get("A_"+deptList.get(k).getCode()).toString();
	            			cnt = list.get(j).get("C_"+deptList.get(k).getCode()).toString();
	            			ml = list.get(j).get("M_"+deptList.get(k).getCode()).toString();
	            		}
						sheet.addCell(new Label(k*3+2, index+2, amt));
						sheet.addCell(new Label(k*3+1+2, index+2, cnt));
						sheet.addCell(new Label(k*3+2+2, index+2, ml));
					}
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
		
	}
}