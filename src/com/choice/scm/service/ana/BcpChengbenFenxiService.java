package com.choice.scm.service.ana;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class BcpChengbenFenxiService {

	@Autowired
	private SupplyAcctFirmMapper bcpChengbenFenxiMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
    private final transient Log log = LogFactory.getLog(BcpChengbenFenxiService.class);
	
	/***
	 * 查询半成品成本
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findBcpChengbenFenxi(Map<String,Object> conditions,Page page) throws CRUDException{
		try {
			List<Map<String,Object>> list = pageManager.selectPage(conditions, page, SupplyAcctFirmMapper.class.getName()+".findBcpChengbenFenxi");
			List<Map<String,Object>> list1 = bcpChengbenFenxiMapper.findBcpChengbenFenxiOut(conditions);
			List<Map<String,Object>> list2 = bcpChengbenFenxiMapper.findBcpChengbenFenxiCost(conditions);
			//循环便利比较数据，得到实际耗用合计
			Map<String,Double> itmap = new HashMap<String,Double>();//放实际耗用合计
			Map<String,Double> itmapll = new HashMap<String,Double>();//放理论耗用合计
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				if (map.containsKey("ITCODE")){
					if (null != map.get("ITCODE")){
						String positn = map.get("DEPT").toString();
						String sp_code = map.get("SP_CODE").toString();
						double llcnt = Double.parseDouble(map.get("LLCNT").toString());
						for(Map<String,Object> map1 : list1){
							String positn1 = map1.get("POSITN").toString();
							String sp_code1 = map1.get("SP_CODE").toString();
							if(positn.equals(positn1) && sp_code.equals(sp_code1)){
								double cntout = Double.parseDouble(map1.get("CNTOUT").toString());
								double amtoutt = Double.parseDouble(map1.get("AMTOUT").toString());
								map.put("SJCNT", Math.round(cntout*100.0)/100.0);
								map.put("SJAMT", Math.round(amtoutt*100.0)/100.0);
								map.put("YLCCL", Math.round(cntout == 0 ? 0 : llcnt/cntout*10000.0)/100.0);
							}
						}
						for(Map<String,Object> map2 : list2){
							String positn2 = map2.get("POSITN").toString();
							String sp_code2 = map2.get("SP_CODE").toString();
							if(positn.equals(positn2) && sp_code.equals(sp_code2)){
								double cnt = Double.parseDouble(map2.get("CNT").toString());
								double sjcnt = Double.parseDouble(map.get("SJCNT").toString());
								double price = sjcnt == 0 ? 0 : Double.parseDouble(map.get("SJAMT").toString())/sjcnt;
								if(cnt != 0){
									sjcnt = sjcnt*llcnt/cnt;
									map.put("SJCNT", Math.round(sjcnt*100.0)/100.0);
									map.put("SJAMT", Math.round(sjcnt*price*100.0)/100.0);
									map.put("YLCCL", Math.round(sjcnt == 0 ? 0 : llcnt/sjcnt*10000.0)/100.0);
								}
							}
						}
						String itcode = map.get("ITCODE").toString();
						double amtout = Double.parseDouble(map.get("SJAMT").toString());
						double amtoutll = Double.parseDouble(map.get("LLAMT").toString());
						if (itmap.containsKey(itcode) && null != itmap.get(itcode)){
							itmap.put(itcode, itmap.get(itcode) + amtout);
						}else{
							itmap.put(itcode, amtout);
						}
						if (itmapll.containsKey(itcode) && null != itmapll.get(itcode)){
							itmapll.put(itcode, itmapll.get(itcode) + amtoutll);
						}else{
							itmapll.put(itcode, amtoutll);
						}
					}
				}
			}
			List<Map<String,Object>> rs = new ArrayList<Map<String,Object>>();
			String vcodejs="";
			//循环便利比较数据，加入同一物资，则合并显示
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				rs.add(map);
				if (map.containsKey("ITCODE")){
					if (null != map.get("ITCODE")){
						if (vcodejs.equals(map.get("ITCODE").toString())){
							map.put("ITCODE", "");
							map.put("ITDES", "");
							map.put("ITUNIT", "");
							map.put("ITCNT", "");
							map.put("ITAMT", "");
							map.put("LLXJ", "");
							map.put("LLMLL", "");
							map.put("SJXJ", "");
							map.put("DCMLL", "");
							continue;
						}
					}
				}
				vcodejs=map.get("ITCODE")==null?"":map.get("ITCODE").toString();
				map.put("LLXJ", Math.round(itmapll.get(vcodejs)*100.0)/100.0);
				map.put("LLMLL", Math.round((Double.parseDouble(map.get("ITAMT").toString()) - itmapll.get(vcodejs))/Double.parseDouble(map.get("ITAMT").toString())*10000.0)/100.0);
				map.put("SJXJ", Math.round(itmap.get(vcodejs)*100.0)/100.0);
				map.put("DCMLL", Math.round((Double.parseDouble(map.get("ITAMT").toString()) - itmap.get(vcodejs))/Double.parseDouble(map.get("ITAMT").toString())*10000.0)/100.0);
			}
			reportObject.setRows(rs);
//			List<Map<String,Object>> foot = dancaiMaolilvMisBohMapper.findCalForDancaiMaolilv(conditions);
//			mapReportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}
