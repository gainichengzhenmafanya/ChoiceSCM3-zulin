package com.choice.scm.service.ana;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WeekSet;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class YlYingchanlvService {

	@Autowired
	private SupplyAcctFirmMapper ylYingchanlvMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
    private final transient Log log = LogFactory.getLog(YlYingchanlvService.class);
	
	/***
	 * 查询周次或月次表
	 * @param ws
	 * @return
	 */
	public List<WeekSet> findWeekSet(SupplyAcct sa) throws CRUDException{
		try{
			return ylYingchanlvMapper.findWeekSet(sa);
		}catch(Exception e){
            log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}

	/***
	 * 查询原料应产率
	 * @param wsList
	 * @param condition
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findYlYingchanlv(List<WeekSet> wsList, Map<String, Object> condition, Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)condition.get("supplyAcct"));
			sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			StringBuffer sqlStr = new StringBuffer();
			StringBuffer sqlStr2 = new StringBuffer();
			
			String functionStr = "IFNULL";
			for(WeekSet ws : wsList){
				sqlStr.append("SUM(CASE WHEN T1.WEEKK = "+ws.getWeekk()+" THEN T1.AMT ELSE 0 END) AS A_"+ws.getWeekk()+",")
					.append("SUM(CASE WHEN T1.WEEKK = "+ws.getWeekk()+" THEN "+functionStr+"(T2.AMTOUT,0) ELSE 0 END) AS CA_"+ws.getWeekk()+",")
					.append("SUM(CASE WHEN T1.WEEKK = "+ws.getWeekk()+" THEN T1.CNT ELSE 0 END) AS C_"+ws.getWeekk()+",")
					.append("SUM(CASE WHEN T1.WEEKK = "+ws.getWeekk()+" THEN "+functionStr+"(T2.CNTOUT,0) ELSE 0 END) AS CC_"+ws.getWeekk()+",");
				sqlStr2.append("CASE WHEN CC_"+ws.getWeekk()+" = 0 THEN 0 ELSE C_"+ws.getWeekk()+"/CC_"+ws.getWeekk()+"*100 END AS W_"+ws.getWeekk()+",");
			}
			condition.put("sqlStr", sqlStr.substring(0, sqlStr.length()-1));
			condition.put("sqlStr2", sqlStr2.substring(0, sqlStr2.length()-1));
			if(null == page){
				reportObject.setRows(ylYingchanlvMapper.findYlYingchanlv(condition));
			}else{
				reportObject.setRows(pageManager.selectPage(condition, page, SupplyAcctFirmMapper.class.getName()+".findYlYingchanlv"));
				reportObject.setTotal(page.getCount());
			}
			List<Map<String,Object>> foot = ylYingchanlvMapper.findYlYingchanlvSum(condition);
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
}
