package com.choice.scm.service.ana;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;

@Service
public class WzChengbenChayiService {

	@Autowired
	private SupplyAcctFirmMapper wzChengbenChayiMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private AcctMapper acctMapper;
	
	/**
	 * 分店部门物资进出表明细
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findCostVarianceChoice3(Map<String,Object> conditions,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		
		//是否是导出调用的
		boolean excel = conditions.get("excel")!=null;
		
		//不是导出的
		if(!excel){
			if(null!=supplyAcct && !"".equals(supplyAcct)){
				//positn改写
				String positn = supplyAcct.getPositn();
				StringBuilder pjstr = new StringBuilder();
				if(positn!=null&&!positn.trim().equals("")){
					String[] positnArray = positn.split(",");
					for(int i=0,len=positnArray.length;i<len;i++){
						//SELECT '20001' AS CODE FROM DUAL UNION
						if(i!=len-1){
							pjstr.append(" SELECT '"+positnArray[i]+"' AS CODE FROM DUAL UNION ALL");
						}
						else{
							pjstr.append(" SELECT '"+positnArray[i]+"' AS CODE FROM DUAL");
						}
					}
					supplyAcct.setPositn(pjstr.toString());
				}
				
				//grptyp改写
				String grptyp = supplyAcct.getGrptyp();
				pjstr = new StringBuilder();
				if(grptyp!=null&&!grptyp.trim().equals("")){
					String[] grptypArray = grptyp.split(",");
					for(int i=0,len=grptypArray.length;i<len;i++){
						//SELECT '20001' AS CODE FROM DUAL UNION
						if(i!=len-1){
							pjstr.append(" SELECT '"+grptypArray[i]+"' AS CODE FROM DUAL UNION ALL");
						}
						else{
							pjstr.append(" SELECT '"+grptypArray[i]+"' AS CODE FROM DUAL");
						}
					}
					supplyAcct.setGrptyp(pjstr.toString());
				}
				
				//grp改写
				String grp = supplyAcct.getGrp();
				pjstr = new StringBuilder();
				if(grp!=null&&!grp.trim().equals("")){
					String[] grpArray = grp.split(",");
					for(int i=0,len=grpArray.length;i<len;i++){
						//SELECT '20001' AS CODE FROM DUAL UNION
						if(i!=len-1){
							pjstr.append(" SELECT '"+grpArray[i]+"' AS CODE FROM DUAL UNION ALL");
						}
						else{
							pjstr.append(" SELECT '"+grpArray[i]+"' AS CODE FROM DUAL");
						}
					}
					supplyAcct.setGrp(pjstr.toString());
				}
				
				//typ改写
				String typ = supplyAcct.getTyp();
				pjstr = new StringBuilder();
				if(typ!=null&&!typ.trim().equals("")){
					String[] typArray = typ.split(",");
					for(int i=0,len=typArray.length;i<len;i++){
						//SELECT '20001' AS CODE FROM DUAL UNION
						if(i!=len-1){
							pjstr.append(" SELECT '"+typArray[i]+"' AS CODE FROM DUAL UNION ALL");
						}
						else{
							pjstr.append(" SELECT '"+typArray[i]+"' AS CODE FROM DUAL");
						}
					}
					supplyAcct.setTyp(pjstr.toString());
				}
			}
			//参数
			conditions.put("yearr", acctMapper.findYearrByDate(supplyAcct.getBdat()));
			mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctFirmMapper.class.getName()+".findCostVarianceChoice3"));
			List<Map<String,Object>> foot = wzChengbenChayiMapper.findCalForCostVarianceChoice3(conditions);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}
		
		//导出的，查询全部positn，grptyp，grp，typ，然后在代码里面过滤
		else{
			//保存条件集合
			List<String> positnList = new ArrayList<String>();
			List<String> grptypList = new ArrayList<String>();
			List<String> grpList = new ArrayList<String>();
			List<String> typList = new ArrayList<String>();
			if(null!=supplyAcct && !"".equals(supplyAcct)){
				//仓位
				String positn = supplyAcct.getPositn();
				if(positn!=null&&!positn.trim().equals("")){
					positnList = Arrays.asList(positn.split(","));
				}
				supplyAcct.setPositn("");
				//大类
				String grptyp = supplyAcct.getGrptyp();
				if(grptyp!=null&&!grptyp.trim().equals("")){
					grptypList = Arrays.asList(grptyp.split(","));
				}
				supplyAcct.setGrptyp("");
				//中类
				String grp = supplyAcct.getGrp();
				if(grp!=null&&!grp.trim().equals("")){
					grpList = Arrays.asList(grp.split(","));
				}
				supplyAcct.setGrp("");
				//小类
				String typ = supplyAcct.getTyp();
				if(typ!=null&&!typ.trim().equals("")){
					typList = Arrays.asList(typ.split(","));
				}
				supplyAcct.setTyp("");
			}
			//参数
			conditions.put("yearr", acctMapper.findYearrByDate(supplyAcct.getBdat()));
			//得到所有结果集
			List<Map<String,Object>> rowsAll = wzChengbenChayiMapper.findCostVarianceChoice3(conditions);
			//过滤后的结果集
			List<Map<String,Object>> afterAll = new ArrayList<Map<String,Object>>();
			//根据条件过滤
			if(rowsAll!=null){
				for(int i=0,len=rowsAll.size();i<len;i++){
					//仓位，大类，中类，小类
					Map<String,Object> map = rowsAll.get(i);
					if(positnList.size()==0||positnList.indexOf(map.get("CODE").toString())!=-1){
						if(grptypList.size()==0||grptypList.indexOf(map.get("GRPTYP").toString())!=-1){
							if(grpList.size()==0||grpList.indexOf(map.get("GRP").toString())!=-1){
								if(typList.size()==0||typList.indexOf(map.get("SP_TYPE").toString())!=-1){
									afterAll.add(map);
									continue;
								}
							}
						}
					}
				}
			}
			//置空
			rowsAll=null;
			//设置数据
			mapReportObject.setRows(afterAll);
			return mapReportObject;
		}
	}
	
	/**
	 * 分店部门物资进出表明细
	 * 存储过程版
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findWzChengbenChayiPrc(Map<String,Object> conditions,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		//条件判断
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getPositn()&&!"".equals(supplyAcct.getPositn())){
				supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
			}
			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
			}
			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
		}
		//参数
		conditions.put("yearr", acctMapper.findYearrByDate(supplyAcct.getBdat()));
		List<Map<String,Object>> resultList = new ArrayList<Map<String,Object>>();
		conditions.put("rcursor", resultList);
		//调用存储过程查询数据到临时表SCM_TEMP_WZCHENBENCHAYI
		wzChengbenChayiMapper.callScmPrcWZChenbenChayi(conditions);
		//结果集，未使用
		//@SuppressWarnings("unchecked")
		//List<Map<String,Object>> lists = (List<Map<String,Object>>)conditions.get("rcursor");
		
		//查询临时表数据SCM_TEMP_WZCHENBENCHAYI
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctFirmMapper.class.getName()+".findTempWZChenbenChayi"));
		List<Map<String,Object>> foot = wzChengbenChayiMapper.findTempWZChenbenChayiHj(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
}