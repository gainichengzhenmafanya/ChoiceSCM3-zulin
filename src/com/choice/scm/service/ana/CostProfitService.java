package com.choice.scm.service.ana;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.WeekSet;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.ana.CostProfitMapper;
@Service
public class CostProfitService {

	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	
	/** @检查本年是否 有周次维护，没有则自动生成 
    * @throws Exception */  
    public int checkAndUpdateWeek() throws Exception{  
    	String yearr = mainInfoMapper.findYearrList().get(0)+"";
    	if(mainInfoMapper.findWeekSet(Integer.parseInt(yearr)).size()==0){
    		int weeks = getWeekNumOfYearDay(DateFormat.getStringByDate(getCurrYearLast(Integer.parseInt(yearr)), "yyyy-MM-dd"));
    		WeekSet weekSet = new WeekSet();
    		for (int i=0;i<weeks;i++){
    			weekSet.setAcct("1");
    			weekSet.setYearr(yearr);
    			weekSet.setDes("");
    			weekSet.setWeekk(i+1+"");
    			weekSet.setWeekno(mainInfoMapper.getMaxWeekk()+"");
    			String yearWeekFirstDay = getYearWeekFirstDay(Integer.parseInt(yearr),i+1);
    			String yearWeekEndDay = getYearWeekEndDay(Integer.parseInt(yearr),i+1);
    			weekSet.setDatf(DateFormat.getDateByString(yearWeekFirstDay, "yyyy-MM-dd"));
    			weekSet.setDatt(DateFormat.getDateByString(yearWeekEndDay, "yyyy-MM-dd"));
    			weekSet.setDes(yearWeekFirstDay.substring(5, 10)+"到"+yearWeekEndDay.substring(5, 10));
    			mainInfoMapper.insertWeekSet(weekSet);
    		}
    		return 1;
    	}else{
    		return 0;
    	}
    } 
    
    /**  
     * @获取某年最后一天日期  
     * @param year 年份  
     * @return Date  
     */  
    public static Date getCurrYearLast(int year){   
        Calendar calendar = Calendar.getInstance();   
        calendar.clear();   
        calendar.set(Calendar.YEAR, year);   
        calendar.roll(Calendar.DAY_OF_YEAR, -1);   
        Date currYearLast = calendar.getTime();   
        return currYearLast;   
    } 
    
	/** @计算指定日期某年的第几周 
      * @return  interger 
      */  
    public int getWeekNumOfYearDay(String strDate ) throws Exception{  
	    Calendar calendar = Calendar.getInstance();  
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
	    Date curDate = format.parse(strDate);  
	    calendar.setTime(curDate);  
	    int currentWeekOfYear=0;
	    currentWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
	    if (currentWeekOfYear == 1 && calendar.get(Calendar.MONTH) == 11) {   
            currentWeekOfYear = 53;   
        }  
	    return currentWeekOfYear;
    } 
    
    /** @功能     计算某年某周的开始日期 
    * @return  interger 
    ****************************************/  
    public String getYearWeekFirstDay(int yearNum,int weekNum) throws Exception {  
  
	    Calendar cal = Calendar.getInstance();  
	    cal.set(Calendar.YEAR, yearNum);  
	    cal.set(Calendar.WEEK_OF_YEAR, weekNum);  
	    cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);  
	    //分别取得当前日期的年、月、日  
	    String tempYear = Integer.toString(yearNum);
	    String tempMonth = Integer.toString(cal.get(Calendar.MONTH) + 1);
	    if(weekNum==1 && tempMonth.equals("12")){
	    	tempYear = Integer.parseInt(tempYear)-1+"";
	    }
	    String tempDay = Integer.toString(cal.get(Calendar.DATE));  
	    String tempDate = tempYear + "-" +tempMonth + "-" + tempDay;  
	    return DateFormat.getStringByDate(DateFormat.getDateByString(tempDate, "yyyy-MM-dd"), "yyyy-MM-dd");    
    }  
    /***************************************** 
    * @功能     计算某年某周的结束日期 
    * @return  interger 
    ****************************************/  
    public String getYearWeekEndDay(int yearNum,int weekNum) throws Exception {  
	    Calendar cal = Calendar.getInstance();  
	    cal.set(Calendar.YEAR, yearNum);  
	    cal.set(Calendar.WEEK_OF_YEAR, weekNum + 1);  
	    cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);  
	    //分别取得当前日期的年、月、日  
	    String tempYear = Integer.toString(yearNum);  
	    String tempMonth = Integer.toString(cal.get(Calendar.MONTH) + 1);
	    if(weekNum==53 && tempMonth.equals("1")){
	    	tempYear = Integer.parseInt(tempYear)+1+"";
	    }
	    String tempDay = Integer.toString(cal.get(Calendar.DATE));  
	    String tempDate = tempYear + "-" +tempMonth + "-" + tempDay;  
	    return DateFormat.getStringByDate(DateFormat.getDateByString(tempDate, "yyyy-MM-dd"), "yyyy-MM-dd"); 
    }  
	
	public List<Map<String,Object>> findFirmProfitCmp(Map<String,Object> condition,Page page) throws Exception{  
		checkAndUpdateWeek();

		StringBuffer sumF = new StringBuffer();
		StringBuffer caseF = new StringBuffer();
		if(null!=condition.get("codes") && !"".equals(condition.get("codes"))){
			String positnCodes = condition.get("codes").toString();
			String[] positnArray = positnCodes.split(",");
			for(String p : positnArray){
				String p1 = p.substring(1,p.length()-1);
				sumF.append(" ROUND(SUM(F_"+p1+"),2) AS F_"+p1+",");
				caseF.append(" CASE WHEN AA.FIRM='"+p1+"' THEN AA.PROFIT ELSE 0 END AS F_"+p1+",");
			}
		}else{
			List<Positn> positnList = findTableHead(condition);
			for(Positn p : positnList){
				sumF.append(" ROUND(SUM(F_"+p.getCode()+"),2) AS F_"+p.getCode()+",");
				caseF.append(" CASE WHEN AA.FIRM='"+p.getCode()+"' THEN AA.PROFIT ELSE 0 END AS F_"+p.getCode()+",");
			}
		}
		condition.put("sumF", sumF.substring(0, sumF.lastIndexOf(",")));
		condition.put("caseF", caseF.substring(0, caseF.lastIndexOf(",")));
		return pageManager.selectPage(condition,page,CostProfitMapper.class.getName()+".findFirmProfitCmp");
	}
	
	
	/**
	 * 查询 表头信息
	 * @param acct
	 * @param positnType
	 * @param codes
	 */
	public List<Positn> findTableHead(Map<String,Object> condition){
		List<Positn> positnList = positnMapper.findPositnByTypCode(condition);
		return positnList;
	}

}