package com.choice.scm.service.ana;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.ana.PtivityAnaMapper;

@Service
public class PtivityAnaService {

	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private PositnMapper positnMapper;
 
	/**
	 * 应产率分析
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findPtivityAna(PtivityAna ptivity,Page page){
		//...
		String positnStr = ptivity.getPositn();
		//用于传参数
		Positn positn = new Positn();
		//用于union
		Map<String,String> unions = new HashMap<String,String>();
		//分店集合
		List<String> firms = new ArrayList<String>();
		//档口集合
		List<String> dangs = new ArrayList<String>();
		//数据
		String code = null;
		String ucode = null;
		List<Positn> lists = null;
		List<String> lists2 = null;
		//数据集合
		List<Map<String,Object>> list = null;
		if(positnStr!=null){
			//1.区分分店还是档口
			String[] codes = positnStr.split(",");
			for(int i=0,len=codes.length;i<len;i++){
				code = codes[i];
				positn = new Positn();
				positn.setCode(code);
				positn = positnMapper.findPositnByCode(positn);
				ucode = positn.getUcode();
				//档口
				if(ucode==null||"".equals(ucode)){
					firms.add(code);
				}
				else{
					dangs.add(code);
				}
			}
			
			//根据分店获得档口
			if(firms.size()>0){
				for(int i=0,len=firms.size();i<len;i++){
					code = firms.get(i);
					positn = new Positn();
					positn.setUcode(code);
					lists = positnMapper.findPositn(positn);
					lists2 = new ArrayList<String>();
					for(int j=0,len2=lists.size();j<len2;j++){
						lists2.add(lists.get(j).getCode());
					}
					dangs.removeAll(lists2);
					String values = code;
					for(int j=0,len2=lists2.size();j<len2;j++){
						values += (","+lists2.get(j));
					}
					//不同的分店
					unions.put(code, values);
				}
			}
			
			//档口
			if(dangs.size()>0){
				for(int i=0,len=dangs.size();i<len;i++){
					code = dangs.get(i);
					positn = new Positn();
					positn.setCode(code);
					positn = positnMapper.findPositnByCode(positn);
					//得到firm code
					ucode = positn.getUcode();
					if(unions.containsKey(ucode)){
						String value = unions.get(ucode);
						unions.put(ucode, value+","+code);
					}
					else{
						unions.put(ucode, code);
					}
				}
			}
			//得到第一个
			Iterator<String> iter = unions.keySet().iterator();
			String key = null;
			if(iter.hasNext()){
				key = iter.next();
				String value = unions.get(key);
				ptivity.setFirm(key);
				ptivity.setPositn(value);
			}
			//删除掉第一个
			unions.remove(key);
			ptivity.setParams(unions);
			
		}
		//参数
//		ptivity.setGrptyp((ptivity.getGrptyp()==null||ptivity.getGrptyp().equals(""))?null:CodeHelper.replaceCode(ptivity.getGrptyp()));
//		ptivity.setGrp((ptivity.getGrp()==null||ptivity.getGrp().equals(""))?null:CodeHelper.replaceCode(ptivity.getGrp()));
//		ptivity.setTyp((ptivity.getTyp()==null||ptivity.getTyp().equals(""))?null:CodeHelper.replaceCode(ptivity.getTyp()));
		ptivity.setPositn((ptivity.getPositn()==null||ptivity.getPositn().equals(""))?null:ptivity.getPositn());
		
		//查询数据
		list = mapPageManager.selectPage(ptivity, page, PtivityAnaMapper.class.getName()+".findPtivityAna");
		List<Map<String,Object>> rs =new ArrayList<Map<String,Object>>();
		String vcodejs="";
		//循环便利比较数据，加入同一物资，则合并显示
		for(int i=0;i<list.size();i++){
			Map<String,Object> map = list.get(i);
			rs.add(map);
			if (map.containsKey("POSITN")&&map.containsKey("SP_CODE")){
				if (null != map.get("POSITN")&&null != map.get("SP_CODE")){
					if (vcodejs.equals(map.get("POSITN").toString()+":"+map.get("SP_CODE").toString())){
						map.put("POSITN", "");
						map.put("POSITNDES", "");
						map.put("SP_CODE", "");
						map.put("SP_NAME", "");
						map.put("SP_DESC", "");
						map.put("UNIT", "");
						map.put("COST", "");
						map.put("BZNAME", "");
						map.put("BZCNT", "");
						map.put("LLCNT", 0);
						map.put("CYRATE", 0);
						map.put("REALCNT", "");
						map.put("RATE", "");
						continue;
					}
				}
			}
			vcodejs=(map.get("POSITN")==null||map.get("SP_CODE")==null)?"":(map.get("POSITN").toString()+":"+map.get("SP_CODE").toString());
		}
		mapReportObject.setRows(rs);
		mapReportObject.setTotal(page.getCount());
		return mapReportObject;
	}	

}