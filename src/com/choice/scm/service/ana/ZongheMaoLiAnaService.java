package com.choice.scm.service.ana;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.CostProfit;
import com.choice.scm.persistence.ana.ZongheMaoLiAnaMapper;

@Service
public class ZongheMaoLiAnaService {

	@Autowired
	private ZongheMaoLiAnaMapper zongheMaoLiAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 月综合毛利分析
	 */
	public ReportObject<Map<String,Object>> findFirmMonthMaoLiAna(CostProfit costProfit,Page page) throws CRUDException{
		try{
			costProfit.setPositn(CodeHelper.replaceCode(costProfit.getPositn()));
			List<Map<String,Object>> foot = zongheMaoLiAnaMapper.findFirmMonthMaoLiAnaSum(costProfit);
			mapReportObject.setRows(mapPageManager.selectPage(costProfit, page, ZongheMaoLiAnaMapper.class.getName()+".findFirmMonthMaoLiAna"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询月综合毛利分析客流员工数
	 */
	public List<Map<String,Object>> findMonthKeLiuEmpNo(CostProfit costProfit) throws CRUDException{
		try{
			costProfit.setPositn(CodeHelper.replaceCode(costProfit.getPositn()));
			return zongheMaoLiAnaMapper.findMonthKeLiuEmpNo(costProfit);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 周综合毛利分析
	 */
	public ReportObject<Map<String,Object>> findFirmWeekMaoLiAna(CostProfit costProfit,Page page) throws CRUDException{
		try{
			costProfit.setPositn(CodeHelper.replaceCode(costProfit.getPositn()));
			List<Map<String,Object>> foot = zongheMaoLiAnaMapper.findFirmWeekMaoLiAnaSum(costProfit);
			mapReportObject.setRows(mapPageManager.selectPage(costProfit, page, ZongheMaoLiAnaMapper.class.getName()+".findFirmWeekMaoLiAna"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询周综合毛利分析客流员工数
	 */
	public List<Map<String,Object>> findWeekKeLiuEmpNo(CostProfit costProfit) throws CRUDException{
		try{
			costProfit.setPositn(CodeHelper.replaceCode(costProfit.getPositn()));
			return zongheMaoLiAnaMapper.findWeekKeLiuEmpNo(costProfit);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}