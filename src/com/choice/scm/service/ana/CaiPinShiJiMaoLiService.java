package com.choice.scm.service.ana;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.ana.CaiPinShiJiMaoLiMapper;

@Service
public class CaiPinShiJiMaoLiService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;

	/**
	 * 菜品实际毛利查询
	 * @param proCostAna
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findCaiPinShiJiMaoLi(Map<String,Object> map,Page pager){
		mapReportObject.setRows(mapPageManager.selectPage(map, pager, CaiPinShiJiMaoLiMapper.class.getName()+".findCaiPinShiJiMaoLi"));
//		List<Map<String,Object>> foot = prdPrcCostManageMapper.findCalForCostVariance(conditions);
//		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}	

}