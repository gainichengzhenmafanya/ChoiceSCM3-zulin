package com.choice.scm.service.ana;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.ana.PtivityAna;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.ana.PtivityAnaMapper;

@Service
public class WzWanyuanyongliangFenxiService {

	@Autowired 
	private PageManager<Map<String,Object>> pageManager;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PtivityAnaMapper ptivityAnaMapper;
	@Autowired
	private AcctMapper acctMapper;
 
	/**
	 * 统计物资万元用量
	 * @param ptivity
	 * @return
	 */
	public String calWzWanyuanyongliangFenxi(PtivityAna ptivity) throws CRUDException{
		try{
			ptivity.setYearr(acctMapper.findYearrByDate(ptivity.getBdat()));
			ptivityAnaMapper.callScmWzWanyuanyongliangTemp(ptivity);
			return "1";
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 物资万元用量
	 * @param ptivity
	 * @param page
	 * @return
	 */
	public ReportObject<Map<String,Object>> findWzWanyuanyongliangFenxi(PtivityAna ptivity,Page page) throws CRUDException{
		try{
			ptivity.setYearr(acctMapper.findYearrByDate(ptivity.getBdat()));
			ptivity.setPositn(CodeHelper.replaceCode(ptivity.getPositn()));
			List<Map<String,Object>> foot = ptivityAnaMapper.findWzWanyuanyongliangFenxiSum(ptivity);
			if(null == page){
				reportObject.setRows(ptivityAnaMapper.findWzWanyuanyongliangFenxi(ptivity));
			}else{
				reportObject.setRows(pageManager.selectPage(ptivity, page, PtivityAnaMapper.class.getName()+".findWzWanyuanyongliangFenxi"));
				reportObject.setTotal(page.getCount());
			}
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}