package com.choice.scm.service.ana;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.ChartsXml;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.CreateChart;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.ana.UseCostAnaMapper;

@Service
public class UseCostAnaService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired 
	private UseCostAnaMapper useCostAnaMapper;
	private final transient Log log = LogFactory.getLog(UseCostAnaService.class);
	/**
	 * 耗用成本查询
	 * @param proCostAna
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findUseCostAna(Map<String,Object> map,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
				supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
			}
			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
			}
			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
			map.put("supplyAcct", supplyAcct);
		}
		String sqlStr="";
		for (int i = 1; i <= 12; i++) {
			sqlStr+=",Sum(Cast(A.Outa"+i+" As Numeric(12,2))) as \""+i+"amt\",Sum(Cast(A.Outc"+i+" As Numeric(12,2))) as \""+i+"cnt\"";
		}
		map.put("yearr", DateFormat.getYear(new Date()));
		map.put("sqlStr", sqlStr);
		if(null!=map.get("type") && !"".equals(map.get("type")) && "sum".equals(map.get("type"))){
			mapReportObject.setRows(mapPageManager.selectPage(map, pager, UseCostAnaMapper.class.getName()+".findUseCostAnaSum"));
		}else{
			mapReportObject.setRows(mapPageManager.selectPage(map, pager, UseCostAnaMapper.class.getName()+".findUseCostAnaDetail"));
		}
//		List<Map<String,Object>> foot = prdPrcCostManageMapper.findCalForCostVariance(conditions);
//		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
	/**
	 * 图表查询
	 * @param map
	 * @param pager
	 * @return
	 */
	public List<Map<String,Object>> findUseCostAnaForTuBiao(Map<String,Object> map){
		SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getFirm() && !"".equals(supplyAcct.getFirm())){
				supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
			}
			if(null!=supplyAcct.getGrptyp() && !"".equals(supplyAcct.getGrptyp())){
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			if(null!=supplyAcct.getGrp() && !"".equals(supplyAcct.getGrp())){
				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
			}
			if(null!=supplyAcct.getTyp() && !"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
			map.put("supplyAcct", supplyAcct);
		}
		String sqlStr="";
		for (int i = 1; i <= 12; i++) {
			sqlStr+=",Sum(Cast(A.Outa"+i+" As Numeric(12,2))) as \""+i+"amt\",Sum(Cast(A.Outc"+i+" As Numeric(12,2))) as \""+i+"cnt\"";
		}
		map.put("yearr", DateFormat.getYear(new Date()));
		map.put("sqlStr", sqlStr);
		return useCostAnaMapper.findUseCostAnaSum(map);
	}	

	/**
	 * 图表
	 * @param response
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public ChartsXml findXmlForUseCostAna(HttpServletResponse response,Map<String,Object> map) throws CRUDException{
		try{
			List<Map<String,Object>> list = findUseCostAnaForTuBiao(map);
			List<String> xNameList = new ArrayList<String>();
			List<String> typeList = new ArrayList<String>();
			List<Object[]> dataList = new ArrayList<Object[]>();
			Object[] cntObj = new Object[12];
			Object[] amtObj = new Object[12];
			for(int i=0;i<list.size();i++){
				for (int j = 0; j < 12; j++) {
					xNameList.add((j+1)+"月");
					cntObj[j] = list.get(i).get((j+1)+"cnt").toString();
					amtObj[j] = list.get(i).get((j+1)+"amt").toString();
				}
			}
			typeList.add("数量");
			typeList.add("金额");
			
			dataList.add(cntObj);
			dataList.add(amtObj);
			return CreateChart.createMSHistogram(response, "耗用成本图表分析", xNameList, typeList, "", dataList, null);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}