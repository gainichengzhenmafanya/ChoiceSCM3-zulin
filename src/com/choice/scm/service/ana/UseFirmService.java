package com.choice.scm.service.ana;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.ana.UseFirmMapper;

@Service
public class UseFirmService {

	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	/**
	 * 耗用分店查询
	 * @param proCostAna
	 * @param pager
	 * @return
	 */
	public List<Map<String,Object>> findUseFirm(Map<String,Object> map,Page page){
		SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
				supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
			}
//			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
//				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
//			}
//			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
//				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
//			}
//			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
//				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
//			}
			map.put("supplyAcct", supplyAcct);
		}
		StringBuffer  sqlStr = new StringBuffer();
		if(null!=map.get("codes") && !"".equals(map.get("codes"))){
			String firm = map.get("codes").toString();
			String[] firmArray = firm.split(",");
			for(String f : firmArray){
				sqlStr.append(" ROUND(sum(CASE WHEN (a.firm = "+f+" OR P.UCODE = "+f+") THEN a.amtout ELSE 0 END),2) as F_"+f.substring(1, f.length()-1)+",");
				//数量，表头F_CODE_SL
				sqlStr.append(" ROUND(SUM(CASE WHEN (A.FIRM = "+f+" OR P.UCODE = "+f+") THEN A.CNTOUT ELSE 0 END),2) AS F_"+f.substring(1, f.length()-1)+"_SL,");
			}
		}else{
			List<Positn> firmList = findTableHead(map);
			for(Positn p : firmList){
				sqlStr.append(" ROUND(sum(CASE WHEN (a.firm = '"+p.getCode()+"' OR P.UCODE = '"+p.getCode()+"') THEN a.amtout ELSE 0 END),2) as F_"+p.getCode()+",");
				//数量，表头F_CODE_SL
				sqlStr.append(" ROUND(SUM(CASE WHEN (A.FIRM = '"+p.getCode()+"' OR P.UCODE = '"+p.getCode()+"') THEN A.CNTOUT ELSE 0 END),2) AS F_"+p.getCode()+"_SL,");
			}
		}
		map.put("sqlStr", sqlStr.substring(0, sqlStr.lastIndexOf(",")));
//		return useFirmMapper.findUseFirm(map);
		return pageManager.selectPage(map,page,UseFirmMapper.class.getName()+".findUseFirm");
	}
	/**
	 * 查询 表头信息
	 * @param acct
	 * @param positnType
	 * @param codes
	 */
	public List<Positn> findTableHead(Map<String,Object> condition){
		List<Positn> positnList = positnMapper.findPositnByTypCode(condition);
		return positnList;
	}
}