package com.choice.scm.service.ana;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.WeekSet;
import com.choice.scm.persistence.reportFirm.SupplyAcctFirmMapper;
@Service
public class DcMaolilvService {

	@Autowired
	private SupplyAcctFirmMapper dcMaolilvMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
    private final transient Log log = LogFactory.getLog(DcMaolilvService.class);
	
	/***
	 * 查询单菜毛利率
	 * @param wsList
	 * @param condition
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findDcMaolilv(List<WeekSet> wsList, Map<String, Object> condition, Page page) throws CRUDException{
		try{
			//改为走存储过程
			if(null == page || page.getNowPage() == 1)//下一页时就不走存储过程了
				dcMaolilvMapper.callScmPrcDcMaolilvTemp(condition);
			
			SupplyAcct sa = ((SupplyAcct)condition.get("supplyAcct"));
			sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			StringBuffer sqlStr = new StringBuffer();
			StringBuffer sqlStr2 = new StringBuffer();
			for(WeekSet ws : wsList){
				sqlStr.append("SUM(CASE WHEN T1.WEEKK = "+ws.getWeekk()+" THEN T1.AMT ELSE 0 END) AS A_"+ws.getWeekk()+",")
					.append("SUM(CASE WHEN T1.WEEKK = "+ws.getWeekk()+" THEN T1.COST ELSE 0 END) AS C_"+ws.getWeekk()+",");
				if("西贝莜面".equals(condition.get("acctDes").toString()))//如果是西贝  计算单菜毛利率使用的收入=折前饭菜收入/1.06
					sqlStr2.append("CASE WHEN A_"+ws.getWeekk()+" = 0 THEN 0 ELSE (A_"+ws.getWeekk()+"/1.06-C_"+ws.getWeekk()+")/A_"+ws.getWeekk()+"*1.06*100 END AS W_"+ws.getWeekk()+",");
				else
					sqlStr2.append("CASE WHEN A_"+ws.getWeekk()+" = 0 THEN 0 ELSE (A_"+ws.getWeekk()+"-C_"+ws.getWeekk()+")/A_"+ws.getWeekk()+"*100 END AS W_"+ws.getWeekk()+",");
			}
			condition.put("sqlStr", sqlStr.substring(0, sqlStr.length()-1));
			condition.put("sqlStr2", sqlStr2.substring(0, sqlStr2.length()-1));
			if(null == page){
				reportObject.setRows(dcMaolilvMapper.findDcMaolilv2(condition));
			}else{
				reportObject.setRows(pageManager.selectPage(condition, page, SupplyAcctFirmMapper.class.getName()+".findDcMaolilv2"));
				reportObject.setTotal(page.getCount());
			}
			List<Map<String,Object>> foot = dcMaolilvMapper.findDcMaolilvSum2(condition);
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分店菜品利润明细
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findDancaiMaolilv(Map<String,Object> conditions,Page page) throws CRUDException{
		try {
			List<Map<String,Object>> list = pageManager.selectPage(conditions, page, SupplyAcctFirmMapper.class.getName()+".findDancaiMaolilv");
			List<Map<String,Object>> list1 = dcMaolilvMapper.findDancaiMaolilvOut(conditions);
			List<Map<String,Object>> list2 = dcMaolilvMapper.findDancaiMaolilvCost(conditions);
			//循环便利比较数据，得到实际耗用合计
			Map<String,Double> itmap = new HashMap<String,Double>();//放实际耗用合计
			Map<String,Double> itmapll = new HashMap<String,Double>();//放理论耗用合计
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				if (map.containsKey("ITCODE")){
					if (null != map.get("ITCODE")){
						String positn = map.get("DEPT").toString();
						String sp_code = map.get("SP_CODE").toString();
						double llcnt = Double.parseDouble(map.get("LLCNT").toString());
						for(Map<String,Object> map1 : list1){
							String positn1 = map1.get("POSITN").toString();
							String sp_code1 = map1.get("SP_CODE").toString();
							if(positn.equals(positn1) && sp_code.equals(sp_code1)){
								double cntout = Double.parseDouble(map1.get("CNTOUT").toString());
								double amtoutt = Double.parseDouble(map1.get("AMTOUT").toString());
								map.put("SJCNT", Math.round(cntout*100.0)/100.0);
								map.put("SJAMT", Math.round(amtoutt*100.0)/100.0);
								map.put("YLCCL", Math.round(cntout == 0 ? 0 : llcnt/cntout*10000.0)/100.0);
							}
						}
						for(Map<String,Object> map2 : list2){
							String positn2 = map2.get("POSITN").toString();
							String sp_code2 = map2.get("SP_CODE").toString();
							if(positn.equals(positn2) && sp_code.equals(sp_code2)){
								double cnt = Double.parseDouble(map2.get("CNT").toString());
								double sjcnt = Double.parseDouble(map.get("SJCNT").toString());
								double price = sjcnt == 0 ? 0 : Double.parseDouble(map.get("SJAMT").toString())/sjcnt;
								if(cnt != 0){
									sjcnt = sjcnt*llcnt/cnt;
									map.put("SJCNT", Math.round(sjcnt*100.0)/100.0);
									map.put("SJAMT", Math.round(sjcnt*price*100.0)/100.0);
									map.put("YLCCL", Math.round(sjcnt == 0 ? 0 : llcnt/sjcnt*10000.0)/100.0);
								}
							}
						}
						String itcode = map.get("ITCODE").toString();
						double amtout = Double.parseDouble(map.get("SJAMT").toString());
						double amtoutll = Double.parseDouble(map.get("LLAMT").toString());
						if (itmap.containsKey(itcode) && null != itmap.get(itcode)){
							itmap.put(itcode, itmap.get(itcode) + amtout);
						}else{
							itmap.put(itcode, amtout);
						}
						if (itmapll.containsKey(itcode) && null != itmapll.get(itcode)){
							itmapll.put(itcode, itmapll.get(itcode) + amtoutll);
						}else{
							itmapll.put(itcode, amtoutll);
						}
					}
				}
			}
			List<Map<String,Object>> rs = new ArrayList<Map<String,Object>>();
			String vcodejs="";
			//循环便利比较数据，加入同一物资，则合并显示
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = list.get(i);
				rs.add(map);
				if (map.containsKey("ITCODE")){
					if (null != map.get("ITCODE")){
						if (vcodejs.equals(map.get("ITCODE").toString())){
							map.put("ITCODE", "");
							map.put("ITDES", "");
							map.put("ITUNIT", "");
							map.put("ITCNT", "");
							map.put("ITAMT", "");
							map.put("LLXJ", "");
							map.put("LLMLL", "");
							map.put("SJXJ", "");
							map.put("DCMLL", "");
							continue;
						}
					}
				}
				vcodejs=map.get("ITCODE")==null?"":map.get("ITCODE").toString();
				map.put("LLXJ", Math.round(itmapll.get(vcodejs)*100.0)/100.0);
				map.put("LLMLL", Math.round((Double.parseDouble(map.get("ITAMT").toString()) - itmapll.get(vcodejs))/Double.parseDouble(map.get("ITAMT").toString())*10000.0)/100.0);
				map.put("SJXJ", Math.round(itmap.get(vcodejs)*100.0)/100.0);
				map.put("DCMLL", Math.round((Double.parseDouble(map.get("ITAMT").toString()) - itmap.get(vcodejs))/Double.parseDouble(map.get("ITAMT").toString())*10000.0)/100.0);
			}
			reportObject.setRows(rs);
//			List<Map<String,Object>> foot = dancaiMaolilvMisBohMapper.findCalForDancaiMaolilv(conditions);
//			mapReportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}
