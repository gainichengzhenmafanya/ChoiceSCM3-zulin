package com.choice.scm.service;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.constants.ChkoutConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Inventory;
import com.choice.scm.domain.MainInfo;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnPand;
import com.choice.scm.domain.PositnPanm;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.ChkoutdMapper;
import com.choice.scm.persistence.ChkoutmMapper;
import com.choice.scm.persistence.InventoryMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.util.CalChkNum;

@Service
public class InventoryService {
	@Autowired
	private InventoryMapper inventoryMapper;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private ChkoutmMapper chkoutmMapper;
	@Autowired
	private ChkoutdMapper chkoutdMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private AcctService acctService;
	
	/**
	 * 查询盘点信息
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findAllInventory(Inventory inventory) throws CRUDException{
		try{
			String inabal="0+Ina0",incbal="0+Inc0",incubal="0+Incu0";
			for(int i=1;i<=Integer.parseInt(inventory.getMonth());i++){
//				for(int i=1;i<=(new Date().getMonth()+1);i++){
				incbal += "+A.INC"+i+"-A.OUTC"+i;
				inabal += "+A.INA"+i+"-A.OUTA"+i;
				incubal += "+A.INCU"+i+"-A.OUTCU"+i;
			}
			inventory.setInabal(inabal);
			inventory.setIncbal(incbal);
			inventory.setIncubal(incubal);
//			return pageManager.selectPage(acct, page, AcctMapper.class.getName()+".findAcct");
			return inventoryMapper.findAllInventory(inventory);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询盘点信息
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findAllInventoryPage(Inventory inventory) throws CRUDException{
		try{
			String inabal="0+Ina0",incbal="0+Inc0",incubal="0+Incu0";
			for(int i=1;i<=Integer.parseInt(inventory.getMonth());i++){
//				for(int i=1;i<=(new Date().getMonth()+1);i++){
				incbal += "+A.INC"+i+"-A.OUTC"+i;
				inabal += "+A.INA"+i+"-A.OUTA"+i;
				incubal += "+A.INCU"+i+"-A.OUTCU"+i;
			}
			inventory.setInabal(inabal);
			inventory.setIncbal(incbal);
			inventory.setIncubal(incubal);
//			return pageManager.selectPage(acct, page, AcctMapper.class.getName()+".findAcct");
			
			//mysql用传值每一页多少条记录
			int start = inventory.getStartNum();
			int end = inventory.getEndNum();
			inventory.setPageSize(end-start);
			
			return inventoryMapper.findAllInventoryPage(inventory);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询盘点信息总数量（延迟加载用）
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public int findAllInventoryCount(Inventory inventory) throws CRUDException{
		try{
			return inventoryMapper.findAllInventoryCount(inventory);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 保存盘点
	 * @param inventory
	 * @return
	 * @throws CRUDException
	 */
	public void updateInventory(Inventory inventory, String acct) throws CRUDException{
		try{
			for (int i = 0; i < inventory.getInventoryList().size(); i++) {
				Inventory ivt = inventory.getInventoryList().get(i);
				ivt.setAcct(acct);
				ivt.setYearr(inventory.getYearr());
				Positn positn=new Positn();
				positn.setCode(inventory.getPositn().getCode());
				ivt.setPositn(positn);
			    inventoryMapper.updateInventory(ivt);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 添加日盘标记
	 * @throws CRUDException
	 */
	public void saveInventory(Inventory inventory) throws CRUDException{
		try{
			Map<String,Object> condition = new HashMap<String, Object>();
			condition.put("positnCd", inventory.getPositn().getCode());
			//condition.put("dat", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			condition.put("dat", inventory.getDate());
			condition.put("id", CodeHelper.createUUID());
			inventoryMapper.saveInventory(condition);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 结束盘点
	 * @throws CRUDException
	 */
	public String endInventory(String accountName,Inventory inven,List<Inventory> listInventory) throws CRUDException{
		try{
			ArrayList<Inventory> listInventoryChkIn =new ArrayList<Inventory>();
			ArrayList<Inventory> listInventoryChkOut=new ArrayList<Inventory>();
			String result="";
			for (int i = 0; i < listInventory.size(); i++) {
				Inventory inventory=listInventory.get(i);
				if((inventory.getCnttrival()-inventory.getCntbla()>0)){//盘盈入库
					listInventoryChkIn.add(inventory);
				}else if((inventory.getCnttrival()-inventory.getCntbla()<0)){//盘亏出库
					listInventoryChkOut.add(inventory);
				}
			}
			Deliver deliver=new Deliver();
			deliver.setCode(CalChkNum.profitDeliver);
			//拿到当前会计月的最后一天
			MainInfo mainInfo = mainInfoMapper.findMainInfo(Integer.parseInt(inven.getYearr()));
			String fun = "getEdat"+inven.getMonth();
			Method get = MainInfo.class.getDeclaredMethod(fun);
			Date date = (Date)get.invoke(mainInfo);
			//生成入库单
			if(listInventoryChkIn.size()>0){
				//生成入库单主表
				Chkinm chkinm=new Chkinm();
				String vouno=calChkNum.getNext(CalChkNum.CHKIN,date);
				int chkinno=chkinmMapper.getMaxChkinno();
			  	chkinm.setAcct(inven.getAcct());
			  	chkinm.setMadeby(accountName);
			  	chkinm.setYearr(inven.getYearr());
			  	chkinm.setChkinno(chkinno);
			  	chkinm.setVouno(vouno);
			  	chkinm.setDeliver(deliver);
			  	chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));//如库、单据日期为页面选择日期
				chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss "));
				chkinm.setInout(ChkinmConstants.in);
				chkinm.setTyp(ChkinmConstants.inVententoryIn);//盘盈入库
				chkinm.setPositn(listInventory.get(0).getPositn());   //仓位
				chkinmMapper.saveChkinm(chkinm);
				//生成入库单从表
				for (int i = 0; i < listInventoryChkIn.size(); i++) {
					Inventory invt=listInventoryChkIn.get(i);
					Chkind chkind=new Chkind();
					chkind.setChkinno(chkinm.getChkinno());
					chkind.setSupply(invt.getSupply());
					chkind.setAmount(invt.getCnttrival()-invt.getCntbla());  //数量
					chkind.setPrice(invt.getSupply().getSp_price());
					chkind.setMemo("");
//					//判断有有效期的  默认生产日期是今天
					Supply s = supplyService.findSupplyById(invt.getSupply());
					if(null != s && s.getSp_per1() > 0)
						chkind.setDued(new Date());
					chkind.setPound(0);
					chkind.setAmount1(invt.getCntutrival() - invt.getCntubla());
					chkind.setInout(ChkinmConstants.in);   //加入入库标志
					chkindMapper.saveChkind(chkind);
				}
				result +="入库单已生成,单号"+chkinno+" ";
			}
			//生成出库单
			if(listInventoryChkOut.size()>0){
				Map<String,Object> map = new HashMap<String,Object>();
				String vouno=calChkNum.getNext(CalChkNum.CHKOUT,date);
				int chkoutno=chkoutmMapper.findNextNo();
				map.put("acct", inven.getAcct());
				map.put("yearr", inven.getYearr());
				map.put("chkoutno", chkoutno);
				map.put("vouno", vouno);
				map.put("maded", date);
				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				map.put("positn", inven.getPositn().getCode());
				map.put("firm", deliver.getCode());
				map.put("madeby", accountName);
				map.put("typ", ChkoutConstants.inVententoryOut);
				map.put("memo", "");
				chkoutmMapper.saveChkoutm(map);
				for (int j = 0; j < listInventoryChkOut.size(); j++) {
					Inventory invt=listInventoryChkOut.get(j);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);///
					m.put("sp_code", invt.getSupply().getSp_code());
					m.put("amount", invt.getCntbla()-invt.getCnttrival());
					m.put("price", invt.getSupply().getSp_price());
					m.put("memo", "");
					m.put("stoid", "0");
					m.put("batchid", "0");
					m.put("deliver", deliver.getCode());
					m.put("amount1", invt.getCntubla()-invt.getCntutrival());
					m.put("pricesale", invt.getSupply().getSp_price());
					m.put("chkstono", "0");//wjf
					m.put("pr", 0);
					chkoutdMapper.saveChkoutd(m);
				}
				result +="出库单已生成,单号"+chkoutno+" ";
			}
			/**修改盘点无出库物资时该仓位pd属性不更新Bug*/
			//本仓位盘点结束 
			Positn positn=new Positn();
			positn.setAcct(inven.getAcct());
			positn.setPd("Y");
			positn.setOldcode(inven.getPositn().getCode());
			positnMapper.updatePositn(positn);//更新pd状态
			return  result;
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 月盘点差异报告
	 */
	public ReportObject<Map<String,Object>> findMonthPdSum(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			int monthFrom = Integer.valueOf(conditions.get("monthFrom").toString());
			int monthTo = Integer.valueOf(conditions.get("monthTo").toString());
			String QCCNT = "A.INC0-A.OUTC0";//期初数量
			String QCCNT2 = "A.INCU0-A.OUTCU0";//期初数量2
			String QCAMT = "A.INA0-A.OUTA0";//期初金额
			String INCNT = "A.INC"+monthFrom;//入库数量
			String INCNT2 = "A.INCU"+monthFrom;//入库数量2
			String INAMT = "A.INA"+monthFrom;//入库金额
			String OUTCNT = "A.OUTC"+monthFrom;//出库数量
			String OUTCNT2 = "A.OUTCU"+monthFrom;//出库数量2
			String OUTAMT = "A.OUTA"+monthFrom;//出库金额
			String CBCNT = "A.COST"+monthFrom;//成本数量
			String CBCNT2 = "A.COSTU"+monthFrom;//成本数量2
			String CBAMT = "A.COSTA"+monthFrom;//成本金额			
			String QMCNT = "A.INC0-A.OUTC0";//期末数量
			String QMCNT2 = "A.INCU0-A.OUTCU0";//期末数量2
			String QMAMT = "A.INA0-A.OUTA0";//期末金额
			
			for(int i=1;i<monthFrom;i++){
				QCCNT = QCCNT + "+A.INC"+i+"-A.OUTC"+i;
				QCCNT2 = QCCNT2 + "+A.INCU"+i+"-A.OUTCU"+i;
				QCAMT = QCAMT + "+A.INA"+i+"-A.OUTA"+i;
			}
			for(int i=monthFrom+1;i<=monthTo;i++){
				INCNT = INCNT + "+A.INC"+i;
				INCNT2 = INCNT2 + "+A.INCU"+i;
				INAMT = INAMT + "+A.INA"+i;
				OUTCNT = OUTCNT + "+A.OUTC"+i;
				OUTCNT2 = OUTCNT2 + "+A.OUTCU"+i;
				OUTAMT = OUTAMT + "+A.OUTA"+i;
				CBCNT = CBCNT + "+A.COST"+i;
				CBCNT2 = CBCNT2 + "+A.COSTU"+i; 
				CBAMT = CBAMT + "+A.COSTA"+i;
			}
			for(int i=1;i<=monthTo;i++){
				QMCNT = QMCNT + "+A.INC"+i+"-A.OUTC"+i;
				QMCNT2 = QMCNT2 + "+A.INCU"+i+"-A.OUTCU"+i;
				QMAMT = QMAMT + "+A.INA"+i+"-A.OUTA"+i;
			}
			
			conditions.put("QCCNT", QCCNT);
			conditions.put("QCCNT2", QCCNT2);
			conditions.put("QCAMT", QCAMT);
			conditions.put("INCNT", INCNT);
			conditions.put("INCNT2", INCNT2);
			conditions.put("INAMT", INAMT);
			conditions.put("OUTCNT", OUTCNT);
			conditions.put("OUTCNT2", OUTCNT2);
			conditions.put("OUTAMT", OUTAMT);
			conditions.put("CBCNT", CBCNT);
			conditions.put("CBCNT2", CBCNT2);
			conditions.put("CBAMT", CBAMT);
			conditions.put("QMCNT", QMCNT);
			conditions.put("QMCNT2", QMCNT2);
			conditions.put("QMAMT", QMAMT);
			
			conditions.put("year", DateFormat.getYear(new Date()));
			conditions.put("acct", ((SupplyAcct)conditions.get("supplyAcct")).getAcct());
			conditions.put("positn", ((SupplyAcct)conditions.get("supplyAcct")).getPositn());
			conditions.put("sp_code", ((SupplyAcct)conditions.get("supplyAcct")).getSp_code());
			conditions.put("typ", CodeHelper.replaceCode(((SupplyAcct)conditions.get("supplyAcct")).getTyp()));
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, InventoryMapper.class.getName()+".findMonthPdSum"));
			mapReportObject.setFooter(inventoryMapper.findMonthPdSum_total(conditions));
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
	
	public boolean checkSupply(SupplyAcct supplyAcct){
		boolean flag = false;
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("sp_code", supplyAcct.getSp_code());
		condition.put("positn", supplyAcct.getPositn());
		
		if(null != inventoryMapper.checkSupply(condition)){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 存货盘点表
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findStock(SupplyAcct supplyAcct,Page page){
		
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("positn", supplyAcct.getPositn());
		condition.put("bdat", DateFormat.formatDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
		condition.put("edat", DateFormat.formatDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
		condition.put("spCode", supplyAcct.getSp_code().trim());
		condition.put("firstDate", DateFormat.formatDate(DateFormat.getFirstDayOfCurrYear(supplyAcct.getBdat()), "yyyy-MM-dd"));
		//condition.clear();
		List<Map<String,Object>> foot = inventoryMapper.findStockSum(condition);
		mapReportObject.setRows(mapPageManager.selectPage(condition, page, InventoryMapper.class.getName()+".findStock"));
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(page.getCount());
		return mapReportObject;
	}
	
	/***
	 * 仓库盘点导出excel
	 * @param os
	 * @param list
	 * @return
	 */
	public boolean exportInventory(ServletOutputStream os, List<Inventory> list,String positn) {
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(positn+"盘点表", 0);//加上要盘点的仓位名称 wjf
			sheet.addCell(new Label(0, 0,positn+"盘点表",titleStyle));
			sheet.mergeCells(0, 0, 13, 0);
			sheet.addCell(new Label(0, 1, "货架")); 
            sheet.addCell(new Label(1, 1, "物资编码"));   
            sheet.addCell(new Label(2, 1, "物资名称")); 
			sheet.addCell(new Label(3, 1, "规格"));  
			sheet.addCell(new Label(4, 1, "类别"));
			sheet.addCell(new Label(5, 1, "标准单位"));
			sheet.addCell(new Label(6, 1, "参考单位"));
			sheet.addCell(new Label(7, 1, "结存|标准数量"));
			sheet.addCell(new Label(8, 1, "结存|参考数量"));
			sheet.addCell(new Label(9, 1, "结存|金额"));
			sheet.addCell(new Label(10, 1, "盘点|标准数量"));
			sheet.addCell(new Label(11, 1, "盘点|参考数量"));
			sheet.addCell(new Label(12, 1, "盈亏|标准数量"));
			sheet.addCell(new Label(13, 1, "盈亏|参考数量"));
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSupply().getPositn1()));
            	sheet.addCell(new Label(1, index, list.get(j).getSupply().getSp_code()));
            	sheet.addCell(new Label(2, index, list.get(j).getSupply().getSp_name()));
            	sheet.addCell(new Label(3, index, list.get(j).getSupply().getSp_desc()));
        	
            	String leiBie = list.get(j).getSupply().getTypothdes();
            	sheet.addCell(new Label(4, index, leiBie));
            	sheet.addCell(new Label(5, index, list.get(j).getSupply().getUnit()));
            	sheet.addCell(new Label(6, index, list.get(j).getSupply().getUnit1()));
            	sheet.addCell(new jxl.write.Number(7,index,list.get(j).getCntbla(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(8,index,list.get(j).getCntubla(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(9,index,list.get(j).getAmtbla(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(10,index,list.get(j).getCnttrival(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(11,index,list.get(j).getCntutrival(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(12,index,list.get(j).getCnttrival()-list.get(j).getCntbla(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(13,index,list.get(j).getCntutrival()-list.get(j).getCntubla(),doubleStyle));
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	/**
	 * 加工间结束盘点 修改ynjs状态
	 * @param in
	 * @return
	 */
	public String endInventoryJGJ(Inventory inventory,Positn positn,String code) throws CRUDException{
		String result="结束盘点操作失败！";
		Inventory in = new Inventory();
		in.setAcct(inventory.getAcct());
		in.setYearr(inventory.getYearr());
		in.setMonth(inventory.getMonth());
		in.setPositn(positn);
		//本仓位盘点结束 
		Positn positn_=new Positn();
		positn_.setAcct(inventory.getAcct());
		positn_.setPd("Y");
		positn_.setOldcode(code);
		positnMapper.updatePositn(positn_);
		try {
//			inventoryMapper.upadteInventoryByJGJ(inventory);
			result="结束盘点操作成功！";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * 查询加工间原材料耗用(盘点后)
	 * @param inventory
	 * @return
	 */
	public List<Inventory> findInventoryByJGJ(Inventory inventory) {
		return inventoryMapper.findInventoryByJGJ(inventory);
	}
	
	/***
	 * 保存盘点内容 
	 * @author wjf 2014.12.9
	 * @param listInventory
	 */
	public void savePositnPan(String accountName,Inventory inventory,List<Inventory> listInventory) {
		//插入盘点主表
		PositnPanm positnPanm = new PositnPanm();
		String panno = CodeHelper.createUUID();
		positnPanm.setPanno(panno);
		positnPanm.setAcct(inventory.getAcct());
		positnPanm.setYear(inventory.getYearr());
		positnPanm.setMonthh(inventory.getMonth());
		positnPanm.setPositn(inventory.getPositn().getCode());
		positnPanm.setDat(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
		positnPanm.setTim(DateFormat.getStringByDate(new Date(), "HH:mm:ss"));
		positnPanm.setAccby(accountName);//盘点人
		inventoryMapper.savePositnPanm(positnPanm);
		//插入盘点子表
		for(Inventory invent:listInventory){
			PositnPand pand = new PositnPand();
			pand.setPanno(panno);
			pand.setSp_code(invent.getSupply().getSp_code());
			pand.setCntbla(invent.getCntbla());//结存数量
			pand.setCntubla(invent.getCntubla());//结存参考数量
			pand.setAmtblan(invent.getAmtbla());//结存金额
			pand.setCntact(invent.getCnttrival());//盘点数量
			pand.setCntuact(invent.getCntutrival());//盘点参考数量
			pand.setCntdif(invent.getCnttrival()-invent.getCntbla());//差异 =  盘点-结存
			pand.setCntudif(invent.getCntutrival()-invent.getCntubla());//差异参考数量
			inventoryMapper.savePositnPand(pand);
		}
	}	
	
	//==================================加工间盘点===================================
	/**
	 * 查询盘点信息
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findExInventory(Inventory inventory) throws CRUDException{
		try{
			String inabal="0+Ina0",incbal="0+Inc0",incubal="0+Incu0";
			//根据日期取截止到几月
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(inventory.getDate().getTime());
			//月
			int month = cal.get(Calendar.MONTH) + 1;
			for(int i=1;i<month;i++){
				incbal += "+A.INC"+i+"-A.OUTC"+i;
				inabal += "+A.INA"+i+"-A.OUTA"+i;
				incubal += "+A.INCU"+i+"-A.OUTCU"+i;
			}
			//开始日期
			inventory.setEdate(cal.getTime());
			cal.set(Calendar.DAY_OF_MONTH, 1);
			inventory.setBdate(cal.getTime());
			//sql
			inventory.setInabal(inabal);
			inventory.setIncbal(incbal);
			inventory.setIncubal(incubal);
			return inventoryMapper.findExInventory(inventory);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询盘点信息
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<Inventory> findExInventoryPage(Inventory inventory) throws CRUDException{
		try{
			//根据选择日期计算会计月
			MainInfo mi = acctService.getmainInfo(inventory.getDate());
			int month = Integer.parseInt(mi.getMonthh());
			String inabal="0+Ina0",incbal="0+Inc0",incubal="0+Incu0";
			//根据日期取截止到几月
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(inventory.getDate().getTime());
			for(int i=1;i<month;i++){
				incbal += "+A.INC"+i+"-A.OUTC"+i;
				inabal += "+A.INA"+i+"-A.OUTA"+i;
				incubal += "+A.INCU"+i+"-A.OUTCU"+i;
			}
			//开始日期
			inventory.setEdate(cal.getTime());
			cal.set(Calendar.DAY_OF_MONTH, 1);
			inventory.setBdate(mi.getBdat1());
			//sql
			inventory.setInabal(inabal);
			inventory.setIncbal(incbal);
			inventory.setIncubal(incubal);
			//mysql用传值每一页多少条记录
			int start = inventory.getStartNum();
			int end = inventory.getEndNum();
			inventory.setPageSize(end-start);
			//返回
			return inventoryMapper.findExInventoryPage(inventory);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}

	/**
	 * 结束盘点
	 * @throws CRUDException
	 */
	public String endExInventory(String accountName,Inventory inven,List<Inventory> listInventory) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		result.put("pr", "1");
		try{
			ArrayList<Inventory> listInventoryChkIn =new ArrayList<Inventory>();
			ArrayList<Inventory> listInventoryChkOut=new ArrayList<Inventory>();
			for (int i = 0; i < listInventory.size(); i++) {
				Inventory inventory=listInventory.get(i);
				if((inventory.getCnttrival()-inventory.getCntbla()>0)){//盘盈入库
					listInventoryChkIn.add(inventory);
				}else if((inventory.getCnttrival()-inventory.getCntbla()<0)){//盘亏出库
					listInventoryChkOut.add(inventory);
				}
			}
			Deliver deliver=new Deliver();
			deliver.setCode(CalChkNum.profitDeliver);
			//加工间为日盘，取当前日期
			Date date = inven.getDate();
			//生成入库单
			if(listInventoryChkIn.size()>0){
				//生成入库单主表
				Chkinm chkinm=new Chkinm();
				String vouno=calChkNum.getNext(CalChkNum.CHKIN,date);
				int chkinno=chkinmMapper.getMaxChkinno();
			  	chkinm.setAcct(inven.getAcct());
			  	chkinm.setMadeby(accountName);
			  	chkinm.setYearr(inven.getYearr());
			  	chkinm.setChkinno(chkinno);
			  	chkinm.setVouno(vouno);
			  	chkinm.setDeliver(deliver);
			  	chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));//如库、单据日期为页面择日期
				chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss "));
				chkinm.setInout(ChkinmConstants.in);
				chkinm.setTyp(ChkinmConstants.inVententoryIn);//盘盈入库
				chkinm.setPositn(listInventory.get(0).getPositn());   //仓位
				chkinmMapper.saveChkinm(chkinm);
				//生成入库单从表
				for (int i = 0; i < listInventoryChkIn.size(); i++) {
					Inventory invt=listInventoryChkIn.get(i);
					Chkind chkind=new Chkind();
					chkind.setChkinno(chkinm.getChkinno());
					chkind.setSupply(invt.getSupply());
					chkind.setAmount(invt.getCnttrival()-invt.getCntbla());  //数量
					chkind.setPrice(invt.getSupply().getSp_price());
					chkind.setMemo("");
//					//判断效期的  默认生产日期是今天
					Supply s = supplyService.findSupplyById(invt.getSupply());
					if(null != s && s.getSp_per1() > 0){
						chkind.setDued(new Date());
					}
					chkind.setPound(0);
					chkind.setAmount1(invt.getCntutrival() - invt.getCntubla());
					chkind.setInout(ChkinmConstants.in);   //加入入库标志
					chkindMapper.saveChkind(chkind);
				}
				//result +="入库单已生成,单号"+chkinno+" ";
				//审核入库单
				//审核
				chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
				chkinm.setChkinno(chkinno);
				chkinm.setChecby(accountName);
				chkinmMapper.checkChkinm(chkinm);//循环批量 审核入库单
				int pr=chkinm.getPr();
				result.put("pr", pr+"");
			}
			//生成出库单
			if(listInventoryChkOut.size()>0){
				//生成出库单主表
				Map<String,Object> map = new HashMap<String,Object>();
				String vouno=calChkNum.getNext(CalChkNum.CHKOUT,date);
				int chkoutno=chkoutmMapper.findNextNo();
				map.put("acct", inven.getAcct());
				map.put("yearr", inven.getYearr());
				map.put("chkoutno", chkoutno);
				map.put("vouno", vouno);
				map.put("maded", date);
				map.put("madet", DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				map.put("positn", inven.getPositn().getCode());
				map.put("firm", deliver.getCode());
				map.put("madeby", accountName);
				map.put("typ", ChkoutConstants.inVententoryOut);
				map.put("memo", "");
				chkoutmMapper.saveChkoutm(map);
				for (int j = 0; j < listInventoryChkOut.size(); j++) {
					Inventory invt=listInventoryChkOut.get(j);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);///
					m.put("sp_code", invt.getSupply().getSp_code());
					m.put("amount", invt.getCntbla()-invt.getCnttrival());
					m.put("price", invt.getSupply().getSp_price());
					m.put("memo", "");
					m.put("stoid", "0");
					m.put("batchid", "0");
					m.put("deliver", deliver.getCode());
					m.put("amount1", invt.getCntubla()-invt.getCntutrival());
					m.put("pricesale", invt.getSupply().getSp_price());
					m.put("chkstono", "0");//wjf
					m.put("pr", 0);
					chkoutdMapper.saveChkoutd(m);
				}
				//result +="出库单已生成,单号"+chkoutno+" ";
				//审核出库单
				map.put("chkoutno", chkoutno);
				map.put("month", acctService.getOnlyAccountMonth(new Date())+"");
				map.put("emp", accountName);
				map.put("sta", "Y");
				map.put("chk1memo", "盘亏出库审核！");
				map.put("pr", 0);
				map.put("errdes", "");
				chkoutmMapper.AuditChkout(map);
				Chkoutm chkoutm1=new Chkoutm();
				chkoutm1.setChkoutno(chkoutno);
				chkoutmMapper.updateChkoutAmount(chkoutm1);
				//返回值
				String pr = map.get("pr") != null ? map.get("pr").toString() : "";
				String errdes = map.get("errdes") != null ? map.get("errdes").toString() : "";
				result.put("pr", pr);
				result.put("msg", errdes);
				//返回标志
				switch(Integer.parseInt(map.get("pr").toString())){
				case 0:break;
				case 1:break;
				case -1:break;
				case -3:break;
				case -4:break;
				case -5:break;
				default:
					throw new CRUDException(map.get("errdes").toString());
				}
			}
			/**修改盘点无出库物资时该仓位pd属性不更新Bug*/
			//本仓位盘点结束 
			Positn positn=new Positn();
			positn.setAcct(inven.getAcct());
			positn.setPd("Y");
			positn.setOldcode(inven.getPositn().getCode());
			positnMapper.updatePositn(positn);//更新pd状态
			//返回信息
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 得到加工间最近盘点日期
	 * @param map
	 * @return int
	 */
	public int findExLastPdDate(Map<String,Object> map){
		Map<String,Object> rmap = inventoryMapper.findExLastPdDate(map);
		if(rmap!=null){
			if(rmap.get("CNT")==null){
				return 0;
			}else{
				return Integer.parseInt(rmap.get("CNT").toString());
			}
		}else{
			return 0;
		}
	}
}