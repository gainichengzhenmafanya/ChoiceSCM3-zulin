package com.choice.scm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.scm.domain.AccountDis;
import com.choice.scm.persistence.AccountDisMapper;

/***
 * 报货分拨用户权限
 * @author 王吉峰
 *
 */
@Service
public class AccountDisService {

	@Autowired
	private AccountDisMapper accountDisMapper;

	/***
	 * 保存用户其他权限
	 * @param accountDis
	 * @return
	 */
	public String saveOtherPermission(AccountDis accountDis) {
		//判断有没有此用户，没有先新增
		AccountDis acc = getAccountDisByAccountId(accountDis.getAccountId());
		if(acc == null){//如果表里没有
			addAccountDis(accountDis);
		}else{
			updAccountDis(accountDis);
		}
		return "OK";
	}
	
	/***
	 * 根据用户id查询
	 * @param accountDis
	 * @return
	 */
	public AccountDis getAccountDisByAccountId(String accountId){
		return accountDisMapper.getAccountDisByAccountId(accountId);
	}
	
	/***
	 * 新增
	 * @param accountDis
	 */
	public void addAccountDis(AccountDis accountDis){
		accountDisMapper.addAccountDis(accountDis);
	}
	
	/***
	 * 修改
	 * @param accountDis
	 */
	private void updAccountDis(AccountDis accountDis) {
		accountDisMapper.updAccountDis(accountDis);
	}
}
