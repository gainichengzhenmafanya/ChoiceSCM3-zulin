package com.choice.scm.service;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ChkstoDemod;
import com.choice.scm.domain.ChkstoDemod_x;
import com.choice.scm.persistence.ChkstoDemodMapper;

/**
 * 报货单单据模板子表
 * @author 孙胜彬
 */
@Service
public class ChkstoDemodService {
	@Autowired
	private ChkstoDemodMapper chkstoDemodMapper;
	@Autowired
	private PageManager<ChkstoDemod> pageManager;
	@Autowired
	private PageManager<ChkstoDemod_x> pageManager_x;
	private static Logger log = Logger.getLogger(ChkstoDemodService.class);
	
	/**
	 * 查询报货单从表
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemod> listChkstoDemod(ChkstoDemod chkstoDemod,Page page) throws CRUDException{
		try{
//			return pageManager.selectPage(chkstoDemod, page, ChkstoDemodMapper.class.getName()+".listChkstoDemod");
			return chkstoDemodMapper.listChkstoDemod(chkstoDemod);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询报货单从表  不分页
	 * @param chkstoDemod
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemod> listChkstoDemodd(ChkstoDemod chkstoDemod) throws CRUDException{
		try{
			return chkstoDemodMapper.listChkstoDemod(chkstoDemod);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增报货单从表
	 * @param chkstoDemod
	 * @throws CRUDException
	 */
	public void saveChkstoDemod(ChkstoDemod chkstoDemod) throws CRUDException{
		try{
			chkstoDemodMapper.saveChkstoDemod(chkstoDemod);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改报货单从表
	 * @param chkstoDemod
	 * @throws CRUDException
	 */
	public void updateChkstoDemod(ChkstoDemod chkstoDemod) throws CRUDException{
		try{
			chkstoDemodMapper.updateChkstoDemod(chkstoDemod);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除报货单从表
	 * @param ids
	 * @throws CRUDException
	 */
	public void deleteChkstoDemod(String ids) throws CRUDException{
		try{
			List<String> isList=Arrays.asList(ids.split(","));
			chkstoDemodMapper.deleteChkstoDemod(isList);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询报货单从表  不分页
	 * @param chkstoDemod
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemod_x> listChkstoDemodd_x(ChkstoDemod_x chkstoDemod_x) throws CRUDException{
		try{
			return chkstoDemodMapper.listChkstoDemod_x(chkstoDemod_x);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询报货单从表
	 * @param chkstoDemod
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemod_x> listChkstoDemod_x(ChkstoDemod_x chkstoDemod_x,Page page) throws CRUDException{
		try{
			return pageManager_x.selectPage(chkstoDemod_x, page, ChkstoDemodMapper.class.getName()+".listChkstoDemod_x");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除报货单从表
	 * @param ids
	 * @throws CRUDException
	 */
	public void deleteChkstoDemod_x(String ids) throws CRUDException{
		try{
			List<String> isList=Arrays.asList(ids.split(","));
			chkstoDemodMapper.deleteChkstoDemod_x(isList);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
}
