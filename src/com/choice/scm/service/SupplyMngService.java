package com.choice.scm.service;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.OracleUtil;
import com.choice.scm.domain.CntUse;
import com.choice.scm.domain.PositnSpcode;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.SupplyMngMapper;

@Service
public class SupplyMngService {

	@Autowired
	private SupplyMngMapper supplyMngMapper;
	
	private final transient Log log = LogFactory.getLog(SupplyMngService.class);
	
	/**
	 * 修改物资的供应商信息
	 * @param id
	 * @param code
	 * @param des
	 * @throws CRUDException
	 */
	public void saveDelByUpd(String id, String code, String des) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			/*map.put("ids", id);*/
			map.put("code", code);
			map.put("des", des);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(id)));
			supplyMngMapper.saveDelByUpd(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *修改物资的虚拟物料信息
	 */
	public void saveSupply_xByUpd(String sp_codeList,String sp_code_x,String sp_name_x,String sp_init_x,String unit_x,Double unitRate_x)throws Exception{
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			/*map.put("ids", id);*/
			map.put("sp_code_x", sp_code_x);
			map.put("sp_name_x", sp_name_x);
			map.put("sp_init_x", sp_init_x);
			map.put("unit_x", unit_x);
			map.put("unitRate_x", unitRate_x);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(sp_codeList)));
			supplyMngMapper.saveSupply_xByUpd(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
		
	}
	
	/**
	 *修改物资的实际物料信息
	 */
	public void saveSupplyByUpd(String sp_codeList,String sp_code_x,String sp_name_x,String sp_init_x,String unit_x,Double unitRate_x)throws Exception{
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			/*map.put("ids", id);*/
			map.put("sp_code_x", sp_code_x);
			map.put("sp_name_x", sp_name_x);
			map.put("sp_init_x", sp_init_x);
			map.put("unit_x", unit_x);
			map.put("unitRate_x", unitRate_x);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(sp_codeList)));
			supplyMngMapper.saveSupplyByUpd(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
		
	}
	
	/**
	 * 修改物资的仓位信息
	 * @param id
	 * @param code
	 * @param des
	 * @throws CRUDException
	 */
	public void savePstByUpd(String id, String code, String des) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			/*map.put("ids", id);*/
			map.put("code", code);
			map.put("des", des);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(id)));
			supplyMngMapper.savePstByUpd(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 修改物资的货架信息
	 * @param id
	 * @param positn1
	 * @throws CRUDException
	 */
	public void savePst1ByUpd(String id, String positn1) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			/*map.put("ids", id);*/
			map.put("positn1", positn1==null?"":positn1);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(id)));
			supplyMngMapper.savePst1ByUpd(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改物资的ABC信息
	 * @param id
	 * @param abc
	 * @throws CRUDException
	 */
	public void saveABCByUpd(String id, String abc) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			/*map.put("ids", id);*/
			map.put("abc", abc);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(id)));
			supplyMngMapper.saveABCByUpd(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改最大最小库存
	 * @param supply
	 * @param acct
	 * @throws CRUDException
	 */
	public void updateStock(Supply supply, String acct) throws CRUDException {
		try {
			for (int i = 0; i < supply.getSupplyList().size(); i++) {//批量修改
				Supply supply_=new Supply();
				supply_.setAcct(acct);
				supply_.setSp_code(supply.getSupplyList().get(i).getSp_code());
				supply_.setSp_min1(supply.getSupplyList().get(i).getSp_min1());
				supply_.setSp_max1(supply.getSupplyList().get(i).getSp_max1());
				supplyMngMapper.updateStock(supply_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改最大最小库存
	 * @param supply
	 * @param acct
	 * @throws CRUDException
	 */
	public void updateCnt(String spcodeList, String ratemin, String ratemax) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("ratemin", ratemin);
			map.put("ratemax", ratemax);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(spcodeList)));
			supplyMngMapper.updateCnt(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新日均耗用
	 * @param cntUse
	 * @throws CRUDException 
	 */
	public int updateCntUse(CntUse cntUse) throws Exception{
		try{
			supplyMngMapper.updateCntUse(cntUse);
			return  cntUse.getPr();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 门店更新日均耗用
	 * @param cntUse
	 * @throws CRUDException 
	 */
	public int updateCntUseFirm(CntUse cntUse) throws Exception{
		try{
			supplyMngMapper.updateCntUseFirm(cntUse);
			return  cntUse.getPr();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 仓库更新日均耗用
	 */
	public int updateCntUsePositn(CntUse cntUse) throws Exception{
		try{
			supplyMngMapper.updateCntUsePositn(cntUse);
			return  cntUse.getPr();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新报货分类
	 * @param spcode 物资编码用逗号隔开
	 * @param code 报货分类编码
	 * @throws CRUDException 
	 */
	public void updateTypEas(String spcode, String code) throws CRUDException{
		try{
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("code", code);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(spcode)));
			supplyMngMapper.updateTypEas(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改最大最小库存
	 * @param supply
	 * @param acct
	 * @throws CRUDException
	 */
	public void updateStockFirm(PositnSpcode positnSpcode) throws CRUDException {
		try {
			for (int i = 0; i < positnSpcode.getPositnSpcodeList().size(); i++) {//批量修改
				PositnSpcode positnSpcode_=new PositnSpcode();
				positnSpcode_.setPositn(positnSpcode.getPositnSpcodeList().get(i).getPositn());
				positnSpcode_.setSp_code(positnSpcode.getPositnSpcodeList().get(i).getSp_code());
				positnSpcode_.setSp_min1(positnSpcode.getPositnSpcodeList().get(i).getSp_min1());
				positnSpcode_.setSp_max1(positnSpcode.getPositnSpcodeList().get(i).getSp_max1());
				supplyMngMapper.updateStockFirm(positnSpcode_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改物资的属性信息
	 * @param id
	 * @param attribute
	 * @throws CRUDException
	 */
	public void saveModifyAttribute(String id, String attribute) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			/*map.put("ids", id);*/
			map.put("attribute", attribute==null?"":attribute);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(id)));
			supplyMngMapper.saveModifyAttribute(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改物资在加工间是否领料的属性
	 * @param id
	 * @param attribute
	 * @throws CRUDException
	 */
	public void saveUpdateExkc(String id, String exkc) throws CRUDException {
		try {
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("exkc", exkc==null?"":exkc);
			map.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(id)));
			supplyMngMapper.saveUpdateExkc(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
