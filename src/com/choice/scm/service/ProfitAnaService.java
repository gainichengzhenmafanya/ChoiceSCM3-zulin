package com.choice.scm.service;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.FirmDept;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.Profit;
import com.choice.scm.persistence.FirmDeptMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.ProfitAnaMapper;

@Service
public class ProfitAnaService {

	@Autowired
	private PageManager<Profit> pageManager;
	@Autowired
	private ProfitAnaMapper profitAnaMapper;
	@Autowired
	private FirmDeptMapper firmDeptMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PageManager<Map<String,Object>> profitPageManager;
 
	/**
	 * 毛利日趋势
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitDay(Profit profit,Page page){
		return pageManager.selectPage(profit,page,ProfitAnaMapper.class.getName()+".findProfitDay");
	}
	
	public List<Profit> findProfitDay(Profit profit){
		return profitAnaMapper.findProfitDay(profit);
	}
	
	/**
	 * 毛利日趋势合计
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitDay_total(Profit profit){
		return profitAnaMapper.findProfitDay_total(profit);
	}
	
	/**
	 * 毛利月趋势
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitMonth(Profit profit,Page page){
		return pageManager.selectPage(profit,page,ProfitAnaMapper.class.getName()+".findProfitMonth");
	}
	public List<Profit> findProfitMonth(Profit profit){
		return profitAnaMapper.findProfitMonth(profit);
	}
	
	/**
	 * 毛利月趋势合计
	 * @return
	 * @throws Exception
	 */
	public List<Profit> findProfitMonth_total(Profit profit){
		return profitAnaMapper.findProfitMonth_total(profit);
	}
	
	/**
	 * 查询 表头信息
	 * @param profit
	 * @param ProfitList
	 */
	public List<FirmDept> findTableHead(Profit profit){
		return profitAnaMapper.findFirmDeptByCode(profit);
	}
	/**
	 * 分店日毛利分析
	 * @param condition
	 * @param page
	 * @return
	 */
	public List<Map<String,Object>> findFirmProfitCmp(Map<String,Object> condition,Page page){
		List<Positn> firmDeptList = positnMapper.findPositnByTypCode(condition);
//		List<FirmDept> firmDeptList=findFirmDept(condition);
		StringBuffer  sqlStr = new StringBuffer(",");
        StringBuffer  sqlField = new StringBuffer(",");
        if(firmDeptList.size()!=0){
            for (int i = 0; i < firmDeptList.size(); i++) {
                sqlField.append("sum(LYAMT")
                        .append(firmDeptList.get(i).getCode())
                        .append(") as LYAMT")//算领用金额
                        .append(firmDeptList.get(i).getCode())
                        .append(",");//算领用金额
                sqlField.append("sum(XSAMT")
                        .append(firmDeptList.get(i).getCode())
                        .append(") as XSAMT")//算销售金额
                        .append(firmDeptList.get(i).getCode())
                        .append(",");//算销售金额
                sqlField.append("CONCAT(sum(MAOLILV")
                        .append(firmDeptList.get(i).getCode())
                        .append("),'%') as MAOLILV")//算毛利率
                        .append(firmDeptList.get(i).getCode())
                        .append(",");//算毛利率
                sqlStr.append("sum(CASE WHEN t1.dept='")
                        .append(firmDeptList.get(i).getCode())
                        .append("' THEN t1.lyamt ELSE 0 END) as LYAMT")
                        .append(firmDeptList.get(i).getCode())
                        .append(",");//算领用金额
                sqlStr.append("sum(CASE WHEN t1.dept='")
                        .append(firmDeptList.get(i).getCode())
                        .append("' THEN t1.xsamt ELSE 0 END) as XSAMT")
                        .append(firmDeptList.get(i).getCode())
                        .append(",");//算销售金额
                sqlStr.append("(CASE WHEN t1.dept='")
                        .append(firmDeptList.get(i).getCode())
                        .append("' THEN (CASE WHEN sum(t1.xsamt) = 0 THEN 0 ELSE round((sum(t1.xsamt) - sum(t1.lyamt)) / sum(t1.xsamt) * 100, 2) END) ELSE 0 END) as MAOLILV")
                        .append(firmDeptList.get(i).getCode())
                        .append(",");//算毛利率
            }
            condition.put("sqlField", sqlField.substring(0, sqlField.lastIndexOf(",")));
            condition.put("sqlStr", sqlStr.substring(0, sqlStr.lastIndexOf(",")));
        }
		return profitPageManager.selectPage(condition,page,ProfitAnaMapper.class.getName()+".findFirmProfitCmp");
	}
	/**
	 * 查询分店部门
	 * @param firmDept
	 * @return
	 */
	public List<FirmDept> findFirmDept(Map<String,Object> map){
		String firmDeptCode=map.get("firmDeptCode").toString();
		SupplyAcct supplyAcct=(SupplyAcct) map.get("supplyAcct");
		FirmDept dept=new FirmDept();
		dept.setFirm(supplyAcct.getFirm());
		dept.setTyp("厨房");
		if(null!=firmDeptCode && !"".equals(firmDeptCode)){
			dept.setCode(CodeHelper.replaceCode(firmDeptCode));
		}
		return firmDeptMapper.findFirmDept(dept);
	}
}