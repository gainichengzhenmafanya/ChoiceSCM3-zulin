package com.choice.scm.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.UperSp_code;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.OfferCargo;
import com.choice.scm.domain.OfferCargoEntry;
import com.choice.scm.domain.OfferCargoWhEntry;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.persistence.EasMapper;

@Service
public class EasService {

	private static Logger log = Logger.getLogger(EasService.class);
	
	@Autowired
	private EasMapper easMapper;
	
	/**
	 * 查询所有的物资信息
	 */
	public List<Supply> findAllSupply() throws Exception{
		try{
			return easMapper.findAllSupply();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改成  金蝶同步的 物资
	 */
	public void updateSupply(Supply supply) throws CRUDException {
		try {
			supply.setSp_init(UperSp_code.getInit(supply.getSp_name()));//缩写转为大写
			easMapper.updateSupply(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询所有的物资信息
	 */
	public List<GrpTyp> findAllGrpTyp() throws Exception{
		try{
			return easMapper.findAllGrpTyp();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的物资信息
	 */
	public List<Grp> findAllGrp() throws Exception{
		try{
			return easMapper.findAllGrp();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的物资信息
	 */
	public List<Typ> findAllTyp() throws Exception{
		try{
			return easMapper.findAllTyp();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的物资信息
	 */
	public List<CodeDes> findAllBhTyp() throws Exception{
		try{
			return easMapper.findAllBhTyp();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存系统编码信息
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void saveCodeDes(CodeDes codeDes) throws CRUDException
	{
		easMapper.saveCodeDes(codeDes);
	}

	/**
	 * 删除物资大类
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void deleteAllGrpTyp() throws CRUDException
	{
		easMapper.deleteAllGrpTyp();
	}
	
	/**
	 * 删除物资中类
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void deleteAllGrp() throws CRUDException
	{
		easMapper.deleteAllGrp();
	}
	
	/**
	 * 删除物资小类
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void deleteAllTyp() throws CRUDException
	{
		easMapper.deleteAllTyp();
	}
	
	/**
	 * 删除报货类别
	 * @param codeDes
	 * @throws CRUDException
	 */
	public void deleteAllBhTyp(CodeDes codeDes) throws CRUDException
	{
		easMapper.deleteAllBhTyp(codeDes);
	}
	
	/**
	 * 保存报货单主表到金蝶数据库
	 * @param offerCargo
	 * @throws CRUDException
	 */
	public void saveOfferCargo(OfferCargo offerCargo) throws CRUDException
	{		
		offerCargo.setDeliverDate(new Date());//送货日期不对
		offerCargo.setBizDate(new Date());//业务日期不对
		offerCargo.setIsInvaild(1);//无效
		offerCargo.setCloseStatus(1);//关闭状态
		easMapper.saveOfferCargo(offerCargo);
	}
	
	/**
	 * 保存报货单从表1到金蝶数据库
	 * @param offerCargo
	 * @throws CRUDException
	 */
	public void saveOfferCargoWhEntry(OfferCargoWhEntry offerCargoWhEntry) throws CRUDException
	{		
		easMapper.saveOfferCargoWhEntry(offerCargoWhEntry);
	}
	
	/**
	 * 保存报货单从表2到金蝶数据库
	 * @param offerCargo
	 * @throws CRUDException
	 */
	public void saveOfferCargoEntry(OfferCargoEntry offerCargoEntry) throws CRUDException
	{		
		easMapper.saveOfferCargoEntry(offerCargoEntry);
	}
	
	/**
	 * 根据申购门店获取要货单位，发货单位，客户的id
	 * @param chkstom
	 * @throws CRUDException
	 */
	public OfferCargo findOfferCargo(Chkstom chkstom){
		return easMapper.findOfferCargo(chkstom);
	}
	
	/**
	 * 获取发货仓位，客户二者的id
	 * @param chkstom
	 * @throws CRUDException
	 */
	public OfferCargoWhEntry findOfferCargoWhEntry(Chkstom chkstom){
		return easMapper.findOfferCargoWhEntry(chkstom);
	}
	
	/**
	 * 根据报货分类查找金蝶数据库中的报货分类id
	 * @param chkstom
	 * @throws CRUDException
	 */
	public Chkstom getMaterialGroupID(Chkstom chkstom){
		return easMapper.getMaterialGroupID(chkstom);
	}
	
	/**
	 * 根据申购单中的物资编码获取金蝶数据库中物资id
	 * @param chkstom
	 * @throws CRUDException
	 */
	public OfferCargoEntry getMaterial(Supply supply){
		return easMapper.getMaterial(supply);
	}
}
