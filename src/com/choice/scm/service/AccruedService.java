package com.choice.scm.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.cert.CRLException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Accpaysum;
import com.choice.scm.domain.Braset;
import com.choice.scm.domain.BrasetItem;
import com.choice.scm.domain.BrasetItemd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Recsum;
import com.choice.scm.persistence.AccruedMapper;
import com.choice.scm.persistence.TeShuCaoZuoMapper;

/**
 * 应收应付
 * Created by mc on 14-12-27.
 */
@Service
public class AccruedService {
    @Autowired
    private AccruedMapper accruedMapper;
    @Autowired
    private TeShuCaoZuoMapper teShuCaoZuoMapper;
    @Autowired
    private PageManager pageManager;
    private final transient Log log = LogFactory.getLog(AccruedService.class);

    /**
     * 获取应收款汇总
     * @param recsum
     * @param page
     * @return
     * @throws CRLException
     */
    public List<Recsum> findRecsum(Recsum recsum,Page page) throws CRLException {
        try {
            if(recsum.getFirm()!=null&&!"".equals(recsum.getFirm().trim())){//判断是否为空
                String firms[]=recsum.getFirm().split(",");
                recsum.setFirmList((firms!=null&&firms.length>0)?Arrays.asList(firms):null);
            }
            if(recsum.getPositn()!=null&&!"".equals(recsum.getPositn().trim())){//判断是否为空
                String positns[]=recsum.getPositn().split(",");
                recsum.setPositnList((positns!=null&&positns.length>0)?Arrays.asList(positns):null);
            }
            if(null!=recsum.getPositnList()){
            	recsum.setPositns(CodeHelper.replaceCode(recsum.getPositnList().toString()));
            }
            if(null!=recsum.getFirmList()){
            	recsum.setFirms(CodeHelper.replaceCode(recsum.getFirmList().toString()));
            }
            return pageManager.selectPage(recsum,page,AccruedMapper.class.getName()+".findRecsum");
        } catch (Exception e) {
            log.error(e);
            throw new CRLException(e);
        }
    }

    /**
     * 获取应付款汇总
     * @param accpaysum
     * @param page
     * @return
     * @throws CRLException
     */
    public List<Accpaysum> findAccpaysum(Accpaysum accpaysum,Page page) throws CRLException {
        try {
            if(accpaysum.getDeliverCode()!=null&&!"".equals(accpaysum.getDeliverCode().trim())){
                String firms[]=accpaysum.getDeliverCode().split(",");
                accpaysum.setDeliverList((firms!=null&&firms.length>0)?Arrays.asList(firms):null);
            }
            if(accpaysum.getPositn()!=null&&!"".equals(accpaysum.getPositn().trim())){
                String positns[]=accpaysum.getPositn().split(",");
                accpaysum.setPositnList((positns!=null&&positns.length>0)?Arrays.asList(positns):null);
            }
            return pageManager.selectPage(accpaysum,page,AccruedMapper.class.getName()+".findAccpaysum");
        } catch (Exception e) {
            log.error(e);
            throw new CRLException(e);
        }
    }
    /**
     * 获取分店结算数据
     * @param braset
     * @param page
     * @return
     * @throws CRLException
     */
    public List<Braset> findBraset(Braset braset,Page page) throws CRLException {
        try {
            string2ListByBraset(braset);
            if(null!=braset.getPositnList()){
            	braset.setPositns(CodeHelper.replaceCode(StringUtils.join(braset.getPositnList(), ",")));
            }
            if(null!=braset.getDeliverList()){
            	braset.setDelivers(CodeHelper.replaceCode(braset.getDeliverList().toString()));
            }
            if(null!=braset.getFirmList()){
            	braset.setFirms(CodeHelper.replaceCode(StringUtils.join(braset.getFirmList(), ",")));
            }
            return pageManager.selectPage(braset,page,AccruedMapper.class.getName()+".findBraset");
        } catch (Exception e) {
            log.error(e);
            throw new CRLException(e);
        }
    }

    /**
     * 获取分店结算 支付明细
     * @param braset
     * @return
     * @throws CRLException
     */
    public List<BrasetItem> findBrasetItem(Braset braset) throws CRLException {
        try {
            string2ListByBraset(braset);
            return accruedMapper.findBrasetItem(braset);
        } catch (Exception e) {
            log.error(e);
            throw new CRLException(e);
        }
    }


    /**
     * 分店 付款
     * @param brasetItem
     * 改方法为出库单-入库单预留参数，如果后期加入库单的时候可以在做修改
     */
    public String toPayMoney(BrasetItem brasetItem) throws CRLException {
        if(!"".equals(brasetItem.getInvalue())&&brasetItem.getInvalue()!=null){//区分入库单
            String invalues[]=brasetItem.getInvalue().split(",");
            brasetItem.setInvalueList((invalues!=null&&invalues.length>0)?Arrays.asList(invalues):null);
        }
        if(!"".equals(brasetItem.getOutvalue())&&brasetItem.getOutvalue()!=null){//区分出库单
            String outvalues[]=brasetItem.getOutvalue().split(",");
            brasetItem.setOutvalueList((outvalues != null && outvalues.length > 0) ? Arrays.asList(outvalues) : null);
        }
        List<Braset> brasetList=accruedMapper.findBrasetByNo(brasetItem);
        if(brasetList==null){
            return "获取账单为空！";
        }
        //-------------------
        brasetItem.setId(accruedMapper.findFolioMaxId());//获取序列ID
        brasetItem.setFirm(brasetList.get(0).getFirm());
        brasetItem.setChkno(0);
        brasetItem.setAmtacct(0);
        brasetItem.setDeliver(brasetList.get(0).getFirm());
        brasetItem.setAmtadj(0);
        if("1".equals(brasetList.get(0).getBillTyp())) {
            brasetItem.setChktag(2);//1入库 2出口 3直发
        }else {
            brasetItem.setChktag(3);//1入库 2出口 3直发
        }
        accruedMapper.addBrasetItem(brasetItem);
        //-------------------
        BigDecimal money=BigDecimal.valueOf(brasetItem.getAmtpay());
        String defId=brasetList.get(0).getFirm();
        for(int i=0;i<brasetList.size();i++){
            Braset braset=brasetList.get(i);
            BrasetItemd brasetItemd=new BrasetItemd();
            if(!defId.equals(brasetList.get(i).getFirm())){//判断所有单据的门店是否统一
                throw new CRLException("支付门店不统一，不能支付！");
            }
            if(!"0".equals(brasetList.get(i).getFolio())){//判断该单据是否结单
                throw new CRLException("已节账单不能再支付！");
            }
            brasetItemd.setFolio(brasetItem.getId());
            brasetItemd.setChkno(brasetList.get(i).getChkinno());
            brasetItemd.setInout(brasetList.get(i).getInout());
            if(money.compareTo(BigDecimal.valueOf(braset.getAmt()-braset.getPay()))>0){//支付金额是否大于 需支付金额+已支付金额
                money=money.subtract(BigDecimal.valueOf(braset.getAmt()-braset.getPay()));
                brasetItemd.setPay(braset.getAmt()-braset.getPay());//foliochkm
                brasetItemd.setPay1(braset.getPay()+(braset.getAmt()-braset.getPay()));//出入库
            }else if(money.compareTo(BigDecimal.valueOf(braset.getAmt()-braset.getPay()))<=0){//支付金额是否小于等于 需支付金额+已支付金额
                brasetItemd.setPay(money.doubleValue());//foliochkm
                brasetItemd.setPay1(money.add(BigDecimal.valueOf(braset.getPay())).doubleValue());//出入库
                money=BigDecimal.valueOf(0);
            }
            if((i+1)==brasetList.size()&&money.doubleValue()!=0){//最后一个单子的支付金额是否大于0
                brasetItemd.setPay(money.add(BigDecimal.valueOf(brasetItemd.getPay())).doubleValue());
                brasetItemd.setPay1(money.add(BigDecimal.valueOf(brasetItemd.getPay1())).doubleValue());
            }
            if("1".equals(brasetList.get(i).getBillTyp())){//出库单
                accruedMapper.updateChkoutmPay(brasetItemd);
            }else{
                accruedMapper.updateChkinmPay(brasetItemd);
            }
            accruedMapper.addBrasetItemd(brasetItemd);
        }
        return "ok";
    }

    //-------------------------------------------------------------------
    /**
     * 分店实体 String 数组转 List
     * 判断String变量是否为空，如果不为空者转成List
     * @param braset
     */
    public void string2ListByBraset(Braset braset){
        if(braset.getFirm()!=null&&!"".equals(braset.getFirm().trim())){
            String firms[]=braset.getFirm().split(",");
            braset.setFirmList((firms!=null&&firms.length>0)?Arrays.asList(firms):null);
        }
        if(braset.getPositn()!=null&&!"".equals(braset.getPositn().trim())){
            String positns[]=braset.getPositn().split(",");
            braset.setPositnList((positns != null && positns.length > 0) ? Arrays.asList(positns) : null);
        }
        if(braset.getDeliver()!=null&&!"".equals(braset.getDeliver().trim())){
            String delivers[]=braset.getDeliver().split(",");
            braset.setDeliverList((delivers != null && delivers.length > 0) ? Arrays.asList(delivers) : null);
        }
    }

    /**
     * 账单审核结账
     * @param brasetItem
     */
    public void checkedBill(BrasetItem brasetItem) throws CRLException {
        try {
            if(!"".equals(brasetItem.getInvalue())&&brasetItem.getInvalue()!=null){
                String invalues[]=brasetItem.getInvalue().split(",");
                brasetItem.setInvalueList((invalues!=null&&invalues.length>0)?Arrays.asList(invalues):null);
            }
            if(!"".equals(brasetItem.getOutvalue())&&brasetItem.getOutvalue()!=null){
                String outvalues[]=brasetItem.getOutvalue().split(",");
                brasetItem.setOutvalueList((outvalues != null && outvalues.length > 0) ? Arrays.asList(outvalues) : null);
            }
            if(brasetItem.getOutvalueList()!=null&&brasetItem.getOutvalueList().size()>0){//判断出库单数据是否为空
                accruedMapper.updateChkoutmChk(brasetItem);
            }
            if(brasetItem.getInvalueList()!=null&&brasetItem.getInvalueList().size()>0){//判断直发单+入库单是否为空
                accruedMapper.updateChkinmChk(brasetItem);
            }
        } catch (Exception e) {
            log.error(e);
            throw new CRLException(e);
        }
    }

    /**
     * 期初未结金额
     */
    public void addChkoutm(Chkoutm chkoutm) throws CRLException {
        try {
            chkoutm.setYearr(teShuCaoZuoMapper.findMain().getYearr());
            accruedMapper.addChkoutm(chkoutm);
        } catch (Exception e) {
            log.error(e);
            throw new CRLException(e);
        }
    }

    public boolean exportAccpaysum(ServletOutputStream os,List<Accpaysum> accpaysumList) {
        WritableWorkbook workBook = null;
        WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,
                WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
                Colour.BLACK);
        WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
        try {
            titleStyle.setAlignment(Alignment.CENTRE);
            workBook = Workbook.createWorkbook(os);
            WritableSheet sheet = workBook.createSheet("应付款汇总导出表", 0);
            sheet.addCell(new Label(0, 0,"应付款汇总导出表",titleStyle));
            sheet.mergeCells(0, 0, 13, 0);
            sheet.addCell(new Label(0, 1, "编码"));
            sheet.addCell(new Label(1, 1, "供应商"));
            sheet.addCell(new Label(2, 1, "期初未审核|总进货"));
            sheet.addCell(new Label(3, 1, "期初未审核|总付款"));
            sheet.addCell(new Label(4, 1, "期初未审核|未付款"));
            sheet.addCell(new Label(5, 1, "期初未审核|未开发票"));
            sheet.addCell(new Label(6, 1, "本期已审核|进货"));
            sheet.addCell(new Label(7, 1, "本期已审核|已付款"));
            sheet.addCell(new Label(8, 1, "本期未审核|总进货"));
            sheet.addCell(new Label(9, 1, "本期未审核|已付款"));
            sheet.addCell(new Label(10, 1, "本期已开发票"));
            sheet.addCell(new Label(11, 1, "合计未付款"));
            sheet.addCell(new Label(12, 1, "总开发票仍未结"));
            //遍历list填充表格内容
            int index = 1;
            for(int j=0; j<accpaysumList.size(); j++) {
                if(j == accpaysumList.size()){
                    break;
                }
                index += 1;
                sheet.addCell(new Label(0, index, accpaysumList.get(j).getCode()));
                sheet.addCell(new Label(1, index, accpaysumList.get(j).getDes()));
                sheet.addCell(new Label(2, index, accpaysumList.get(j).getBeginTotalamt()));
                sheet.addCell(new Label(3, index, accpaysumList.get(j).getBeginPay()));
                sheet.addCell(new Label(4, index, accpaysumList.get(j).getBeginAmt()));
                sheet.addCell(new Label(5, index, accpaysumList.get(j).getBeginAmtpay()));
                sheet.addCell(new Label(6, index, accpaysumList.get(j).getThisTotalamt()));
                sheet.addCell(new Label(7, index, accpaysumList.get(j).getThisPay()));
                sheet.addCell(new Label(8, index, accpaysumList.get(j).getThisTotalamtUnaud()));
                sheet.addCell(new Label(9, index, accpaysumList.get(j).getThisPayUnaud()));
                sheet.addCell(new Label(10, index,accpaysumList.get(j).getThisInvpay()));
                sheet.addCell(new Label(11, index,accpaysumList.get(j).getTotalAmt()));
                sheet.addCell(new Label(12, index,accpaysumList.get(j).getTotalInvAmt()));
            }
            workBook.write();
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                if(workBook!=null) {
                    workBook.close();
                }
                os.close();
            } catch (WriteException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public boolean exportRecsum(ServletOutputStream outputStream, List<Recsum> recsumList) {
        WritableWorkbook workBook = null;
        WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,
                WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
                Colour.BLACK);
        WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
        try {
            titleStyle.setAlignment(Alignment.CENTRE);
            workBook = Workbook.createWorkbook(outputStream);
            WritableSheet sheet = workBook.createSheet("应收款汇总导出表", 0);
            sheet.addCell(new Label(0, 0,"应收款汇总导出表",titleStyle));
            sheet.mergeCells(0, 0, 6, 0);
            sheet.addCell(new Label(0, 1, "分店编码"));
            sheet.addCell(new Label(1, 1, "分店名称"));
            sheet.addCell(new Label(2, 1, "上期未结"));
            sheet.addCell(new Label(3, 1, "本期进货"));
            sheet.addCell(new Label(4, 1, "本期已结"));
            sheet.addCell(new Label(5, 1, "合计未结"));
            //遍历list填充表格内容
            int index = 1;
            for(int j=0; j<recsumList.size(); j++) {
                if(j == recsumList.size()){
                    break;
                }
                index += 1;
                sheet.addCell(new Label(0, index,recsumList.get(j).getCode()));
                sheet.addCell(new Label(1, index,recsumList.get(j).getName()));
                sheet.addCell(new Label(2, index,recsumList.get(j).getPriUnsett()));
                sheet.addCell(new Label(3, index,recsumList.get(j).getCurrentStock()));
                sheet.addCell(new Label(4, index,recsumList.get(j).getCurrentTett()));
                sheet.addCell(new Label(5, index,recsumList.get(j).getTotalUnsett()));
            }
            workBook.write();
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                if(workBook!=null) {
                    workBook.close();
                }
                outputStream.close();
            } catch (WriteException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }
}
