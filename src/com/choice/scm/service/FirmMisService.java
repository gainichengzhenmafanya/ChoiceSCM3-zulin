package com.choice.scm.service;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Main;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.FirmMisMapper;

@Service
public class FirmMisService {

	@Autowired
	private FirmMisMapper firmMisMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	private final static int MAXNUM = 30000;
	/**
	 * 毛利查询
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findGrossProfit(Map<String,Object> conditions,Page pager){
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, FirmMisMapper.class.getName()+".findGrossProfit"));
//		List<Map<String,Object>> foot = firmMisMapper.findCalForGrossProfit(conditions);
//		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
	/**
	 * 核减明细
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findHejian(Map<String,Object> conditions,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
			}
			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
			conditions.put("supplyAcct", supplyAcct);
		}
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, FirmMisMapper.class.getName()+".findHejian"));
//		List<Map<String,Object>> foot = prdPrcCostManageMapper.findCalForCostVariance(conditions);
//		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}	
	/**
	 * 查询每日差异报告
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findDayDifReport(Map<String,Object> conditions,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
			}
			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
			conditions.put("supplyAcct", supplyAcct);
		}
		List<Map<String,Object>> foot = firmMisMapper.findCalForDayDifReport(conditions);
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, FirmMisMapper.class.getName()+".findDayDifReport"));
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
	/**
	 * 查询每日差异对比
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findDayDifCompare(Map<String,Object> map,Page page) throws Exception{
		SupplyAcct supplyAcct = (SupplyAcct) map.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
			}
			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
			map.put("supplyAcct", supplyAcct);
		}
		StringBuffer  sqlStr = new StringBuffer();
		List<String> dateList = findTableHead(map);
		for(String p : dateList){
			sqlStr.append(" sum(ROUND(CASE WHEN dat = '"+p+"' THEN amt ELSE 0 END,2)) as \"F_"+p+"\",");
		}
		map.put("sqlStr", sqlStr.substring(0, sqlStr.lastIndexOf(",")));
//		return firmMisMapper.findDayDifCompare(map);
		return mapPageManager.selectPage(map,page,FirmMisMapper.class.getName()+".findDayDifCompare");
	}
	/**
	 * 查询 表头信息
	 * @param acct
	 * @param positnType
	 * @param codes
	 */
	public List<String> findTableHead(Map<String,Object> condition)throws Exception{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		List<String> list=new ArrayList<String>();
		String yearr=null;
		Date bdate=null;
		Date edate=null;
		SupplyAcct supplyAcct=(SupplyAcct) condition.get("supplyAcct");
		int bDay=1;
		int eDay=1;
		if(supplyAcct.getBdat()!=null){
			yearr=sdf.format(supplyAcct.getBdat()).substring(0,4);
			bdate=supplyAcct.getBdat();
			edate=supplyAcct.getEdat();
		}else{
			Date sysDate=new Date();
			yearr=sdf.format(sysDate).substring(0,4);
			bdate=sysDate;
			edate=sysDate;
		}
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		Calendar cal3 = Calendar.getInstance();
		cal1.setTime(bdate);
		cal2.setTime(edate);
		int bMonth=cal1.get(Calendar.MONTH)+1;
		int eMonth=cal2.get(Calendar.MONTH)+1;
		bDay=cal1.get(Calendar.DATE);
		eDay=cal2.get(Calendar.DATE);
		for (int i = bMonth; i <= eMonth; i++) {
			String m=i+"";
			SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM");
			Date date=sd.parse(yearr+"-"+i);
			cal3.setTime(date);
			int lastDay=cal3.getActualMaximum(Calendar.DATE);
			if(i==eMonth){
				lastDay=eDay;
			}
			for (int j = bDay;j <= lastDay; j++) {
				String d=j+"";
				String dat=null;
				if(i<10){m="0"+i;}
				if(j<10){d="0"+j;}
				dat=m+"-"+d;
				list.add(dat);
				if(j==lastDay){
					bDay=1;
					break;
				}
			}
		}		
		return list;
	}
	
	/**
	 *每日差异对比Excel 
	 *
	 */
	public boolean exportExcel(OutputStream os,List<Map<String,Object>> list, Map<String,Object> condition) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			List<String> dayList = findTableHead(condition);
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				sheet.addCell(new Label(0, 0,"每日差异对比"));
				sheet.addCell(new Label(0, 2, "物资编码")); 
	            sheet.addCell(new Label(1, 2, "物资名称"));   
	            sheet.addCell(new Label(2, 2, "物资规格")); 
				sheet.addCell(new Label(3, 2, "单位"));  
				for (int k=0;k<dayList.size();k++) {
					sheet.addCell(new Label(k+4, 2, dayList.get(k)));
				}
				
	            //遍历list填充表格内容
				int index = 0;
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	sheet.addCell(new Label(0, index+3, list.get(j).get("SP_CODE").toString()));
	            	sheet.addCell(new Label(1, index+3, list.get(j).get("SP_NAME").toString()));
	            	String  sp_desc = "";   
	            	if(list.get(j).get("SP_DESC")!=null){
	            		sp_desc = list.get(j).get("SP_DESC").toString();
	            	}
	            	sheet.addCell(new Label(2, index+3,sp_desc ));
	            	String  unit = "";
	            	if(list.get(j).get("unit")!=null){
	            		unit = list.get(j).get("unit").toString();
	            	}
	            	sheet.addCell(new Label(3, index+3,unit));
	            	for (int k=0;k<dayList.size();k++) {
	            		String name="";
	            		if (list.get(j).get("F_"+dayList.get(k))!=null) {
	            			name = list.get(j).get("F_"+dayList.get(k)).toString();
	            		}
						sheet.addCell(new Label(k+4, index+3, name));
					}
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 检查系统是否正在盘点
	 * @param accountId
	 * @return
	 */
	public Map<String,Object> findChktag(Map<String,Object> map){
		return firmMisMapper.findChktag(map);
	}
	/**
	 * 查找月末结转的月份
	 * @return
	 */
	public Main findMain(String yearr){
		return firmMisMapper.findMain(yearr);
	}
	/**
	 * 月末结转
	 * @param monthh
	 */
	public void updateMonthh(Map<String,Object> map){
		firmMisMapper.updateMonthh(map);
	}
}