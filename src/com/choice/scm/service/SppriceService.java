package com.choice.scm.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountMapper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SDList;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.SppriceDemoMapper;
import com.choice.scm.persistence.SppriceMapper;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.PublicExportExcel;

@Service
public class SppriceService {

	@Autowired
	private SppriceMapper sppriceMapper;
	@Autowired
	private SppriceDemoMapper sppriceDemoMapper;
	@Autowired
	private AccountMapper accountMapper;
	@Autowired
	private PageManager<Spprice> pageManager;
	@Autowired
	private PageManager<Map<String, Object>> pageManager1;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private PositnService positnService;
	//格式化时间
	private static SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * 模糊查询价格
	 * @param spprice
	 * @return
	 * @throws Exception
	 */
	public List<Spprice> findSpprice(Spprice spprice,Page page) throws Exception{
		try{
			return pageManager.selectPage(spprice, page, SppriceMapper.class.getName()+".findSpprice");
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 模糊查询价格(返回包含物资上下限的)
	 * @param spprice
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findSppriceT(Spprice spprice,Page page) throws Exception{
		try{
			if(page == null)
				return sppriceMapper.findSppriceT(spprice);
			else
				return pageManager1.selectPage(spprice, page, SppriceMapper.class.getName()+".findSppriceT");
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 下载模板信息
	 * jinshuai 20160504
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response, HttpServletRequest request) throws IOException {
		PublicExportExcel.downloadTemplate(response, request, "物资价格导入模板.xls"); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 将文件上传到temp文件夹下
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return PublicExportExcel.uploadToTemp(request, response); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 对execl进行验证
	 */
	@SuppressWarnings("unchecked")
	public List<String> check(String path, String accountName) throws CRUDException {
		//解析Excel
		Map<String,Object> returns = readSppriceFromFile(path);
		//数据集合
		List<Spprice> spprices = (List<Spprice>) returns.get("datas");
		//错误信息
		List<String> errorList = (List<String>) returns.get("messages");
		//制单时间字符串
		String madeTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		//入库
		for(int i=0,len=spprices.size();i<len;i++){
			Spprice spprice = spprices.get(i);
			spprice.setMadet(madeTime);
			spprice.setEmp(accountName);
			sppriceMapper.addSpprice(spprice);
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}
	
	/**
	 * 读取导入的价格信息
	 * @param path
	 * @return
	 */
	public Map<String,Object> readSppriceFromFile(String path){
		//返回数据及错误的信息
		Map<String,Object> mapAll = new HashMap<String,Object>();
		//保存数据集合
		List<Spprice> spprices = new ArrayList<Spprice>();
		//错误信息
		List<String> errorList = new ArrayList<String>();
		Cell cell = null;
		Supply paramSupply = null;
		Workbook book = null;
		try {
			//得到所有的分店用于下面验证是否有导入的分店
			List<Positn> positns = positnService.findPositnOut2();
			
			//基地仓库,主直拨库,加工间
			List<Positn> positns2 = positnService.findPositnOut1();
			if(positns2!=null&&positns2.size()>0){
				positns.addAll(positns2);
			}
			
			//转换为Map 容易判断	Key分店编码	Positn分店对象
			Map<String,Positn> positnMap = new HashMap<String,Positn>();
			for(int pi = 0,len=positns.size();pi<len;pi++){
				Positn p = positns.get(pi);
				positnMap.put(p.getCode(), p);
			}
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				for (int i = 1; i < rowNum; i++) {// 行循环
					//完整信息
					SppriceDemo sppriceDemo = new SppriceDemo();
					//标志
					int flag = 1;
					for (int col = 0; col < colNum; col++) {// 列循环
						cell = sheet.getCell(col, i);// 读取单元格
						String cellVal = null == cell.getContents() ? "" : cell.getContents().trim();
						//第一列
						if(col == 0){//帐套
							if(isEmpty(cell.getContents())) {
								sppriceDemo.setAcct("1");
							}else{
								sppriceDemo.setAcct(cellVal);
							}
							continue;
						}
						//其它列
						if(col == 1 || col == 2 || col == 3 || col == 4 || col == 5 || col == 6){//供应商编码 物资编码 分店编码 报价 开始日期 结束日期
							if(isEmpty(cell.getContents())) {//标红的不能为空 ，否则继续下一条
								errorList.add((i+1) + "_" + (col + 1) + "_标红的列不能为空！");
								break;
							}
							if(col == 1){//供应商编码
								//验证供应商编码
								Deliver deliver = new Deliver();
								deliver.setCode(cellVal);
								deliver = deliverMapper.findDeliverByCode(deliver);
								if(deliver == null){
									errorList.add((i+1) + "_" + (col + 1) + "_供应商验证不通过！没有此供应商！");
									//退出循环列
									break;
								}else{
									sppriceDemo.setDeliver(cellVal);
								}
							}
							if(col == 2){//验证物资编码
								paramSupply = new Supply();
								paramSupply.setSp_code(cellVal);
								if(supplyService.findSupplyById(paramSupply)==null){
									errorList.add((i+1) + "_" + (col + 1) + "_物资编码验证不通过！没有此物资！");
									//退出循环列
									break;
								}
								else{
									sppriceDemo.setSp_code(cellVal);
								}
							}
							if(col == 3){//验证分店编码
								paramSupply = new Supply();
								paramSupply.setSp_code(cellVal);
								if(!positnMap.containsKey(cellVal)){
									errorList.add((i+1) + "_" + (col + 1) + "_分店编码验证不通过！没有此分店！");
									//退出循环列
									break;
								}
								else{
									sppriceDemo.setFirm(cellVal);
								}
							}
							if(col == 4){//验证报价
								Pattern pattern = Pattern.compile("^(?:[1-9][0-9]*(?:\\.[0-9]+)?|0(?:\\.[0-9]+)?)$");
								Matcher matcher = pattern.matcher(cellVal);
								if(!matcher.matches()){
									errorList.add((i+1) + "_" + (col + 1) + "_报价验证不通过！报价错误！");
									//退出循环列
									break;
								}
								else{
									sppriceDemo.setPrice(BigDecimal.valueOf(Double.parseDouble(cellVal)));
								}
							}
							if(col == 5){//验证开始日期
								Date bDate = isTempleteDate(cellVal);
								if(bDate==null){
									errorList.add((i+1) + "_" + (col + 1) + "_开始日期验证不通过！");
									//退出循环列
									break;
								}
								else{
									sppriceDemo.setBdat(bDate);
								}
							}
							if(col == 6){//验证结束日期
								Date eDate = isTempleteDate(cellVal);
								if(eDate==null){
									errorList.add((i+1) + "_" + (col + 1) + "_结束日期验证不通过！");
									//退出循环列
									break;
								}
								else{
									sppriceDemo.setEdat(eDate);
									//设置成功标志
									flag = 2;
								}
							}
						}
					}
					
					//成功标志
					if(flag==2){
						//走到这里说明这一行数据都符合，则放到集合中去
						spprices.add(sppriceDemo.toSpprice());
					}
				}
			}
			
			//数据
			mapAll.put("messages", errorList);
			mapAll.put("datas", spprices);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (book != null) {
					book.close();
					book = null;
				}
			}
		return mapAll;
	}
		
	/**
	 * 判断是否为空
	 * 
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}
	
	/**
	 * 判断是否是指定的日期格式
	 * yyyy-MM-dd
	 * 如果不符合条件，则返回null
	 * 如果符合条件，则返回日期Date
	 * @param dstr
	 * @return
	 */
	public static Date isTempleteDate(String dstr){
		if(null==dstr||"".equals(dstr.trim())){
			return null;
		}
		if(dstr.length()==10){
			try{
				Date date = sim.parse(dstr);
				return date;
			}catch(Exception e){
				return null;
			}
		}
		return null;
	}
	
	
	/**
	 * 模糊查询所有价格
	 * @param spprice
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Spprice> findAllSpprice(Spprice spprice) throws Exception{
		try{
			return sppriceMapper.findSpprice(spprice);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新价格状态--全部反审核or全部审核
	 * @param sppriceList
	 * @throws Exception
	 */
	public void updateAllSpprice(String admin,String acct,Spprice spprice) throws Exception{
		try{
			if (spprice.getChecby().equals("checked")) {
				spprice.setSta("N");
			}
			if (spprice.getChecby().equals("uncheck")) {
				spprice.setSta("Y");
			}
			spprice.setChecby(admin);
			spprice.setChect(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			spprice.setAcct(acct);
			sppriceMapper.updateAllSppriceStatus(spprice);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新价格状态
	 * @param sppriceList
	 * @throws Exception
	 */
	public void updateSpprice(String listRec,String checby,String acct,String sta) throws Exception{
		try{
			Spprice spprice = new Spprice();
			if(sta.equals("anti"))spprice.setSta("N");
			if(sta.equals("audit"))spprice.setSta("Y");
			spprice.setChecby(checby);
			spprice.setChect(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
			spprice.setAcct(acct);
			sppriceMapper.updateSppriceStatus(Arrays.asList(listRec.split(",")), spprice);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  批量插入价格数据
	 * @param spprice
	 * @throws Exception
	 */
	public void saveBatch(Spprice spprice) throws Exception{
		try{
			BigDecimal rec;
			Spprice sp = sppriceMapper.getLastRec();
			if(sp == null)
				rec = BigDecimal.valueOf(1);
			else
				rec = sp.getRec();
			SppriceDemo demo = new SppriceDemo();
			demo.setDeliver(spprice.getDeliver());
			String time = spprice.getMadet();
			if(spprice.getDeliver()==null || spprice.getDeliver().equals(""))
				throw new Exception("Devliver can't be null!!!");
			List<SppriceDemo> demos = sppriceDemoMapper.findSppriceDemo(demo);
			for(SppriceDemo sppriceDemo : demos){
				Spprice cur = sppriceDemo.toSpprice();
				rec = rec.add(BigDecimal.valueOf(1));
				cur.setRec(rec);
				cur.setMadet(time);
				sppriceMapper.addSpprice(cur);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  批量插入选择的价格数据
	 * @param session 
	 * @param spprice
	 * @throws Exception
	 */
	public void saveBatch(SDList demos, HttpSession session) throws Exception{
		try{
			//修改rec值从序列中取 wjf
			DateFormat dft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			for(SppriceDemo sppriceDemo : demos.getDemos()){
				sppriceDemo = sppriceDemoMapper.findSppriceDemoById(sppriceDemo);
				Spprice cur = sppriceDemo.toSpprice();
				cur.setEmp(session.getAttribute("accountName").toString());
				cur.setMadet(dft.format(new Date()));
				sppriceMapper.addSpprice(cur);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除未审核报价
	 * @param rec
	 * @param string
	 * @throws CRUDException 
	 */
	public void deleteSpprice(String rec, String acct) throws CRUDException {
		try{
			Spprice spprice = new Spprice();
			spprice.setAcct(acct);
			sppriceMapper.deleteSpprice(Arrays.asList(rec.split(",")), spprice);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 查询将到期价格
	 * @param spprice
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Spprice> findSppriceByEdat(Spprice spprice,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(spprice, page, SppriceMapper.class.getName()+".findSppriceByEdat");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询将到期物资报价（我的桌面） wangjie 2014年11月10日 15:47:14
	 * @param spprice
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Spprice> findSppriceByEdatMain(Spprice spprice,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(spprice, page, SppriceMapper.class.getName()+".findSppriceByEdatMain");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
}
