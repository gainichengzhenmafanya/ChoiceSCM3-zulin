package com.choice.scm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.AccountSupplyTyp;
import com.choice.scm.persistence.AccountSupplyTypMapper;

/***
 * 账号 物资小类关联
 * @author 王吉峰
 *
 */
@Service
public class AccountSupplyTypService {

	@Autowired
	private AccountSupplyTypMapper accountSupplyTypMapper;

	/***
	 * 得到指定账号下的物资小类
	 * @param ast
	 * @return
	 */
	public List<AccountSupplyTyp> findAccountSupplyTyp(AccountSupplyTyp ast) {
		return accountSupplyTypMapper.findAccountSupplyTyp(ast);
	}

	/***
	 * 修改账号物资小类权限
	 * @param accountSupplyTyp
	 */
	public void saveAccountSupplyTyp(AccountSupplyTyp accountSupplyTyp) throws Exception{
		//1.先删除原来的权限
		accountSupplyTypMapper.deleteAccountSupplyTyp(accountSupplyTyp);
		//2.再新增
		String typcode = accountSupplyTyp.getTypcode();
		if(typcode == null || "".equals(typcode)){
			return;
		}else{
			String[] typcodes = typcode.split(",");
			for(String typcod:typcodes){
				AccountSupplyTyp newAccountSupplyTyp = new AccountSupplyTyp();
				newAccountSupplyTyp.setId(CodeHelper.createUUID());
				newAccountSupplyTyp.setTypcode(typcod);
				newAccountSupplyTyp.setAccountId(accountSupplyTyp.getAccountId());
				accountSupplyTypMapper.saveAccountSupplyTyp(newAccountSupplyTyp);
			}
		}
	}

}
