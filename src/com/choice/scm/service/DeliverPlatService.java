package com.choice.scm.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Arrivald;
import com.choice.scm.domain.Arrivalm;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.DeliverPlatMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.util.CalChkNum;

/***
 * 供应商平台service
 * @author wjf
 *
 */
@Service
public class DeliverPlatService {
	
	@Autowired
	private DeliverPlatMapper deliverPlatMapper;
	@Autowired
	private AcctMapper acctMapper;
	
	@Autowired
	private PageManager<Dis> disManager;
	@Autowired
	private PageManager<Arrivalm> arrivalmManager;
	@Autowired
	private PageManager<Arrivald> arrivaldManager;
	@Autowired
	private PageManager<Map<String,Object>> mapManager;
	
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private PageManager<Chkinm> pageManager;
	
	private final transient Log log = LogFactory.getLog(DeliverPlatService.class);
	
	/***
	 * 账号关联供应商
	 * @param accountId
	 * @param deliverCode
	 * @return
	 * @throws CRUDException
	 */
	public int saveAccountSupplier(String accountId, String deliverCode) throws CRUDException{
		try{
			deliverPlatMapper.saveAccountSupplier(accountId, deliverCode);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 根据用户id得到所属供应商
	 * @param account
	 * @return
	 */
	public Deliver findDeliverByAccount(String account) throws CRUDException{
		try{
			return deliverPlatMapper.findDeliverByAccount(account);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 供应商，采购汇总
	 */
	public List<Dis> findCaiGouTotal(Dis dis, Page page) throws CRUDException {
		try {
			if (page==null) {
				return deliverPlatMapper.findChkstomSum(dis);
			}else {
				return disManager.selectPage(dis, page, DeliverPlatMapper.class.getName()+".findChkstomSum");
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询门店报货单表
	 * @param dis
	 * @param page
	 * @return
	 */
	public List<Dis> findChkstom(Dis dis, Page page) throws CRUDException{
		try{
			//改为判断是不是非要采购确认 采购审核之后才能查询 
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			dis.setChk1("Y".equals(acct.getYncgqr()) ? "Y" : null);
			dis.setSta("Y".equals(acct.getYncgsh()) ? "Y" : null);
			if(page == null){
				return deliverPlatMapper.findChkstom(dis);
			}
			return disManager.selectPage(dis, page, DeliverPlatMapper.class.getName()+".findChkstom"); 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询门店报货汇总
	 * @param dis
	 * @param page
	 * @return
	 */
	public List<Dis> findChkstomSum(Dis dis, Page page) throws CRUDException{
		try{
			//改为判断是不是非要采购确认 采购审核之后才能查询 
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			dis.setChk1("Y".equals(acct.getYncgqr()) ? "Y" : null);
			dis.setChk1("Y".equals(acct.getYncgsh()) ? "Y" : null);
			if(page == null){
				return deliverPlatMapper.findChkstomSum(dis);
			}
			return disManager.selectPage(dis, page, DeliverPlatMapper.class.getName()+".findChkstomSum"); 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 报货确认
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public int confirmChkstom(String ids) throws CRUDException{
		try{
			deliverPlatMapper.confirmChkstom(ids);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 导出门店报货单
	 * @param os
	 * @param list
	 * @param dis
	 * @return
	 */
	public boolean exportChkstom(ServletOutputStream os, List<Dis> list, Dis dis) {
		WritableWorkbook workBook = null;
		WritableFont titleFont0 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle0 = new WritableCellFormat(titleFont0);
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle0.setAlignment(Alignment.CENTRE);
			titleStyle0.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("门店报货查询", 0);
			sheet.addCell(new Label(0, 0,"门店报货查询表",titleStyle0));
			sheet.mergeCells(0, 0, 14, 0);
			sheet.addCell(new Label(0, 1, "分店编码",titleStyle1)); 
            sheet.addCell(new Label(1, 1, "分店名称",titleStyle1));
            sheet.addCell(new Label(2, 1, "到货日期",titleStyle1));
            sheet.addCell(new Label(3, 1, "报货日期",titleStyle1));
            sheet.addCell(new Label(4, 1, "报货单号",titleStyle1));
            sheet.addCell(new Label(5, 1, "凭证号",titleStyle1));
            sheet.addCell(new Label(6, 1, "物资编码",titleStyle1));
            sheet.addCell(new Label(7, 1, "物资名称",titleStyle1));
        	sheet.addCell(new Label(8, 1, "规格",titleStyle1));
        	sheet.addCell(new Label(9, 1, "采购数量",titleStyle1));
        	sheet.addCell(new Label(10, 1, "采购单位",titleStyle1));
        	sheet.addCell(new Label(11, 1, "标准数量",titleStyle2));
        	sheet.addCell(new Label(12, 1, "标准单位",titleStyle1));
        	String isReportJmj = "0";
        	if("0".equals(isReportJmj)){
        		sheet.addCell(new Label(13, 1, "单价",titleStyle1));
            	sheet.addCell(new Label(14, 1, "金额",titleStyle1));
            	sheet.addCell(new Label(15, 1, "备注",titleStyle1));
            	sheet.addCell(new Label(16, 1, "是否确认",titleStyle1));
        	}else{
        		sheet.addCell(new Label(13, 1, "备注",titleStyle1));
            	sheet.addCell(new Label(14, 1, "是否确认",titleStyle1));
        	}
        	
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getFirmCode()));
            	sheet.addCell(new Label(1, index, list.get(j).getFirmDes()));
            	sheet.addCell(new Label(2, index, DateFormat.getStringByDate(list.get(j).getMaded(),"yyyy-MM-dd")));
            	sheet.addCell(new Label(3, index, DateFormat.getStringByDate(list.get(j).getInd(),"yyyy-MM-dd")));
            	sheet.addCell(new Label(4, index, list.get(j).getChkstoNo()+""));
            	sheet.addCell(new Label(5, index, list.get(j).getVouno()));
            	sheet.addCell(new Label(6, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(7, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(8, index, list.get(j).getSp_desc()));
				sheet.addCell(new jxl.write.Number(9,index,list.get(j).getAmount1sto(),doubleStyle));
				sheet.addCell(new Label(10, index, list.get(j).getUnit3()));
            	sheet.addCell(new jxl.write.Number(11,index,list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new Label(12, index, list.get(j).getUnit()));
            	if("0".equals(isReportJmj)){
            		sheet.addCell(new jxl.write.Number(13,index,list.get(j).getPricein(),doubleStyle));
                	sheet.addCell(new jxl.write.Number(14,index,list.get(j).getPricein()*list.get(j).getAmountin(),doubleStyle));
                	sheet.addCell(new Label(15, index, list.get(j).getMemo()));
                	sheet.addCell(new Label(16, index, "N".equals(list.get(j).getDeliveryn())?"未确认":"已确认"));
            	}else{
                	sheet.addCell(new Label(13, index, list.get(j).getMemo()));
                	sheet.addCell(new Label(14, index, "N".equals(list.get(j).getDeliveryn())?"未确认":"已确认"));
            	}
            	
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 导出门店验货汇总表
	 * @param os
	 * @param list
	 * @param dis
	 * @return
	 */
	public boolean exportChkstomSum(ServletOutputStream os, List<Dis> list, Dis dis) {
		WritableWorkbook workBook = null;
		WritableFont titleFont0 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle0 = new WritableCellFormat(titleFont0);
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle0.setAlignment(Alignment.CENTRE);
			titleStyle0.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("门店报货汇总", 0);
			sheet.addCell(new Label(0, 0,"门店报货汇总表",titleStyle0));
			sheet.mergeCells(0, 0, 6, 0);
            sheet.addCell(new Label(0, 1, "物资编码",titleStyle1));
            sheet.addCell(new Label(1, 1, "物资名称",titleStyle1));
        	sheet.addCell(new Label(2, 1, "规格",titleStyle1));
        	sheet.addCell(new Label(3, 1, "采购数量",titleStyle1));
        	sheet.addCell(new Label(4, 1, "采购单位",titleStyle1));
        	sheet.addCell(new Label(5, 1, "标准数量",titleStyle2));
        	sheet.addCell(new Label(6, 1, "标准单位",titleStyle1));
        	String isReportJmj = "0";
        	if("0".equals(isReportJmj)){
        		sheet.addCell(new Label(7, 1, "单价",titleStyle1));
            	sheet.addCell(new Label(8, 1, "金额",titleStyle1));
        	}
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
				sheet.addCell(new jxl.write.Number(3,index,list.get(j).getAmount1sto(),doubleStyle));
				sheet.addCell(new Label(4, index, list.get(j).getUnit3()));
            	sheet.addCell(new jxl.write.Number(5,index,list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new Label(6, index, list.get(j).getUnit()));
            	if("0".equals(isReportJmj)){
            		sheet.addCell(new jxl.write.Number(7,index,list.get(j).getPricein(),doubleStyle));
                	sheet.addCell(new jxl.write.Number(8,index,list.get(j).getPricein()*list.get(j).getAmountin(),doubleStyle));
            	}
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	

	/***
	 * 查询门店到货单表
	 * @param dis
	 * @param page
	 * @return
	 */
	public List<Dis> findInspection(Dis dis, Page page) throws CRUDException{
		try{
			if(page == null){
				return deliverPlatMapper.findInspection(dis);
			}
			return disManager.selectPage(dis, page, DeliverPlatMapper.class.getName()+".findInspection"); 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询门店验货
	 * @param dis
	 * @param page
	 * @return
	 */
	public List<Dis> findInspectionBZB(Dis dis, Page page) throws CRUDException{
		try{
			if(page == null){
				return deliverPlatMapper.findInspectionBZB(dis);
			}
			return disManager.selectPage(dis, page, DeliverPlatMapper.class.getName()+".findInspectionBZB"); 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询门店到货汇总
	 * @param dis
	 * @param page
	 * @return
	 */
	public List<Dis> findInspectionSum(Dis dis, Page page) throws CRUDException{
		try{
			if(page == null){
				return deliverPlatMapper.findInspectionSum(dis);
			}
			return disManager.selectPage(dis, page, DeliverPlatMapper.class.getName()+".findInspectionSum"); 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 验货确认
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public int confirmIns(Dis dis) throws CRUDException{
		try{
			deliverPlatMapper.confirmIns(dis);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 导出门店验货确认表
	 * @param os
	 * @param list
	 * @param dis
	 * @return
	 */
	public boolean exportIns(ServletOutputStream os, List<Dis> list, Dis dis) {
		WritableWorkbook workBook = null;
		WritableFont titleFont0 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle0 = new WritableCellFormat(titleFont0);
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle0.setAlignment(Alignment.CENTRE);
			titleStyle0.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("门店验货确认", 0);
			sheet.addCell(new Label(0, 0,"门店验货确认表",titleStyle0));
			sheet.mergeCells(0, 0, 13, 0);
			sheet.addCell(new Label(0, 1, "分店编码",titleStyle1)); 
            sheet.addCell(new Label(1, 1, "分店名称",titleStyle1));
            sheet.addCell(new Label(2, 1, "到货日期",titleStyle1));
            sheet.addCell(new Label(3, 1, "报货单号",titleStyle1));
            sheet.addCell(new Label(4, 1, "凭证号",titleStyle1));
            sheet.addCell(new Label(5, 1, "物资编码",titleStyle1));
            sheet.addCell(new Label(6, 1, "物资名称",titleStyle1));
        	sheet.addCell(new Label(7, 1, "规格",titleStyle1));
        	sheet.addCell(new Label(8, 1, "单位",titleStyle1));
        	sheet.addCell(new Label(9, 1, "报货数量",titleStyle1));
        	sheet.addCell(new Label(10, 1, "到货数量",titleStyle1));
        	sheet.addCell(new Label(11, 1, "验货数量",titleStyle2));
        	sheet.addCell(new Label(12, 1, "差异数量",titleStyle1));
        	String isReportJmj = "0";
        	if("0".equals(isReportJmj)){
        		sheet.addCell(new Label(13, 1, "单价",titleStyle1));
            	sheet.addCell(new Label(14, 1, "金额",titleStyle1));
            	sheet.addCell(new Label(15, 1, "是否确认",titleStyle1));
        	}else{
        		sheet.addCell(new Label(13, 1, "是否确认",titleStyle1));
        	}
        	
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getFirmCode()));
            	sheet.addCell(new Label(1, index, list.get(j).getFirmDes()));
            	sheet.addCell(new Label(2, index, list.get(j).getHoped()));
            	sheet.addCell(new Label(3, index, list.get(j).getChkstoNo()+""));
            	sheet.addCell(new Label(4, index, list.get(j).getVouno()));
            	sheet.addCell(new Label(5, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(6, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(7, index, list.get(j).getSp_desc()));
            	sheet.addCell(new Label(8, index, list.get(j).getUnit()));
				sheet.addCell(new jxl.write.Number(9,index,list.get(j).getAmount(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(10,index,list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(11,index,list.get(j).getAmountyh(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(12,index,list.get(j).getAmountin()-list.get(j).getAmountyh(),doubleStyle));
            	if("0".equals(isReportJmj)){
            		sheet.addCell(new jxl.write.Number(13,index,list.get(j).getPricein(),doubleStyle));
                	sheet.addCell(new jxl.write.Number(14,index,list.get(j).getPricein()*list.get(j).getAmountyh(),doubleStyle));
                	sheet.addCell(new Label(15, index, "0".equals(list.get(j).getDeliveryn())?"未确认":"已确认"));
            	}else{
            		sheet.addCell(new Label(13, index, "0".equals(list.get(j).getDeliveryn())?"未确认":"已确认"));
            	}
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 导出门店验货汇总表
	 * @param os
	 * @param list
	 * @param dis
	 * @return
	 */
	public boolean exportInsSum(ServletOutputStream os, List<Dis> list, Dis dis) {
		WritableWorkbook workBook = null;
		WritableFont titleFont0 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle0 = new WritableCellFormat(titleFont0);
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle0.setAlignment(Alignment.CENTRE);
			titleStyle0.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("门店验货汇总", 0);
			sheet.addCell(new Label(0, 0,"门店验货汇总表",titleStyle0));
			sheet.mergeCells(0, 0, 7, 0);
            sheet.addCell(new Label(0, 1, "物资编码",titleStyle1));
            sheet.addCell(new Label(1, 1, "物资名称",titleStyle1));
        	sheet.addCell(new Label(2, 1, "规格",titleStyle1));
        	sheet.addCell(new Label(3, 1, "单位",titleStyle1));
        	sheet.addCell(new Label(4, 1, "报货数量",titleStyle1));
        	sheet.addCell(new Label(5, 1, "到货数量",titleStyle1));
        	sheet.addCell(new Label(6, 1, "验货数量",titleStyle2));
        	sheet.addCell(new Label(7, 1, "差异数量",titleStyle1));
        	String isReportJmj = "0";
        	if("0".equals(isReportJmj)){
        		sheet.addCell(new Label(8, 1, "单价",titleStyle1));
            	sheet.addCell(new Label(9, 1, "金额",titleStyle1));
        	}
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
            	sheet.addCell(new Label(3, index, list.get(j).getUnit()));
				sheet.addCell(new jxl.write.Number(4,index,list.get(j).getAmount(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(5,index,list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(6,index,list.get(j).getAmountyh(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(7,index,list.get(j).getAmountin()-list.get(j).getAmountyh(),doubleStyle));
            	if("0".equals(isReportJmj)){
            		sheet.addCell(new jxl.write.Number(8,index,list.get(j).getPricein(),doubleStyle));
                	sheet.addCell(new jxl.write.Number(9,index,list.get(j).getPricein()*list.get(j).getAmountyh(),doubleStyle));
            	}
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 导出门店验货确认表
	 * @param os
	 * @param list
	 * @param dis
	 * @return
	 */
	public boolean exportInsNormal(ServletOutputStream os, List<Dis> list, Dis dis) {
		WritableWorkbook workBook = null;
		WritableFont titleFont0 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle0 = new WritableCellFormat(titleFont0);
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle0.setAlignment(Alignment.CENTRE);
			titleStyle0.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("门店验货查询", 0);
			sheet.addCell(new Label(0, 0,"门店验货查询表",titleStyle0));
			sheet.mergeCells(0, 0, 15, 0);
			sheet.addCell(new Label(0, 1, "分店编码",titleStyle1)); 
            sheet.addCell(new Label(1, 1, "分店名称",titleStyle1));
            sheet.addCell(new Label(2, 1, "到货日期",titleStyle1));
            sheet.addCell(new Label(3, 1, "报货单号",titleStyle1));
            sheet.addCell(new Label(4, 1, "凭证号",titleStyle1));
            sheet.addCell(new Label(5, 1, "物资编码",titleStyle1));
            sheet.addCell(new Label(6, 1, "物资名称",titleStyle1));
        	sheet.addCell(new Label(7, 1, "规格",titleStyle1));
        	sheet.addCell(new Label(8, 1, "单位",titleStyle1));
        	sheet.addCell(new Label(9, 1, "报货数量",titleStyle1));
        	sheet.addCell(new Label(10, 1, "到货数量",titleStyle1));
        	sheet.addCell(new Label(11, 1, "验货数量",titleStyle2));
        	sheet.addCell(new Label(12, 1, "差异数量",titleStyle1));
        	String isReportJmj = "0";
        	if("0".equals(isReportJmj)){
        		sheet.addCell(new Label(13, 1, "单价",titleStyle1));
            	sheet.addCell(new Label(14, 1, "金额",titleStyle1));
            	sheet.addCell(new Label(15, 1, "入库单凭证号",titleStyle1));
        	}else{
        		sheet.addCell(new Label(13, 1, "入库单凭证号",titleStyle1));
        	}
        	
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getFirmCode()));
            	sheet.addCell(new Label(1, index, list.get(j).getFirmDes()));
            	sheet.addCell(new Label(2, index, DateFormat.getStringByDate(list.get(j).getInd(),"yyyy-MM-dd")));
            	sheet.addCell(new Label(3, index, list.get(j).getChkstoNo()+""));
            	sheet.addCell(new Label(4, index, list.get(j).getVouno()));
            	sheet.addCell(new Label(5, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(6, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(7, index, list.get(j).getSp_desc()));
            	sheet.addCell(new Label(8, index, list.get(j).getUnit()));
				sheet.addCell(new jxl.write.Number(9,index,list.get(j).getAmount(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(10,index,list.get(j).getAmountsto(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(11,index,list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(12,index,list.get(j).getAmountin()-list.get(j).getAmountsto(),doubleStyle));
            	if("0".equals(isReportJmj)){
            		sheet.addCell(new jxl.write.Number(13,index,list.get(j).getPricein(),doubleStyle));
                	sheet.addCell(new jxl.write.Number(14,index,list.get(j).getPricein()*list.get(j).getAmountin(),doubleStyle));
                	sheet.addCell(new Label(15, index, null == list.get(j).getInvouno()?"":list.get(j).getInvouno()));
            	}else{
            		sheet.addCell(new Label(13, index, null == list.get(j).getInvouno()?"":list.get(j).getInvouno()));
            	}
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 导出门店验货汇总表
	 * @param os
	 * @param list
	 * @param dis
	 * @return
	 */
	public boolean exportInsSumNormal(ServletOutputStream os, List<Dis> list, Dis dis) {
		WritableWorkbook workBook = null;
		WritableFont titleFont0 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle0 = new WritableCellFormat(titleFont0);
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.GREY_80_PERCENT);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.RED);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			titleStyle0.setAlignment(Alignment.CENTRE);
			titleStyle0.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("门店验货汇总", 0);
			sheet.addCell(new Label(0, 0,"门店验货汇总表",titleStyle0));
			sheet.mergeCells(0, 0, 7, 0);
            sheet.addCell(new Label(0, 1, "物资编码",titleStyle1));
            sheet.addCell(new Label(1, 1, "物资名称",titleStyle1));
        	sheet.addCell(new Label(2, 1, "规格",titleStyle1));
        	sheet.addCell(new Label(3, 1, "单位",titleStyle1));
        	sheet.addCell(new Label(4, 1, "报货数量",titleStyle1));
        	sheet.addCell(new Label(5, 1, "到货数量",titleStyle1));
        	sheet.addCell(new Label(6, 1, "验货数量",titleStyle2));
        	sheet.addCell(new Label(7, 1, "差异数量",titleStyle1));
        	String isReportJmj = "0";
        	if("0".equals(isReportJmj)){
        		sheet.addCell(new Label(8, 1, "单价",titleStyle1));
            	sheet.addCell(new Label(9, 1, "金额",titleStyle1));
        	}
            //遍历list填充表格内容
			int index = 1;
            for(int j=0; j<list.size(); j++) {
            	if(j == list.size()){
            		break;
            	}
            	index += 1;
            	sheet.addCell(new Label(0, index, list.get(j).getSp_code()));
            	sheet.addCell(new Label(1, index, list.get(j).getSp_name()));
            	sheet.addCell(new Label(2, index, list.get(j).getSp_desc()));
            	sheet.addCell(new Label(3, index, list.get(j).getUnit()));
				sheet.addCell(new jxl.write.Number(4,index,list.get(j).getAmount(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(5,index,list.get(j).getAmountsto(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(6,index,list.get(j).getAmountin(),doubleStyle));
            	sheet.addCell(new jxl.write.Number(7,index,list.get(j).getAmountin()-list.get(j).getAmountsto(),doubleStyle));
            	if("0".equals(isReportJmj)){
            		sheet.addCell(new jxl.write.Number(8,index,list.get(j).getPricein(),doubleStyle));
                	sheet.addCell(new jxl.write.Number(9,index,list.get(j).getPricein()*list.get(j).getAmountin(),doubleStyle));
            	}
	    	}	            
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	/***
	 * 九毛九直配到货单
	 * @param Arrivalm
	 * @param page
	 * @return
	 */
	public List<Arrivalm> findArrivalmList(Arrivalm arrivalm,Page page) throws CRUDException {
		try{
			String pk_supplier = arrivalm.getPk_supplier();
			arrivalm.setPk_supplier(CodeHelper.replaceCode(pk_supplier));
			String pk_org = arrivalm.getPk_org();
			arrivalm.setPk_org(CodeHelper.replaceCode(pk_org));
			List<Arrivalm> arrivalmList = arrivalmManager.selectPage(arrivalm, page, DeliverPlatMapper.class.getName()+".findArrivalmList");
			arrivalm.setPk_supplier(pk_supplier);
			arrivalm.setPk_org(pk_org);
			return arrivalmList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 九毛九直配到货单
	 * @param Arrivalm
	 * @return
	 */
	public List<Arrivalm> findArrivalmList(Arrivalm arrivalm) throws CRUDException {
		try{
			String pk_supplier = arrivalm.getPk_supplier();
			arrivalm.setPk_supplier(CodeHelper.replaceCode(pk_supplier));
			String pk_org = arrivalm.getPk_org();
			arrivalm.setPk_org(CodeHelper.replaceCode(pk_org));
			arrivalm.setPk_arrivals(CodeHelper.replaceCode(arrivalm.getPk_arrivals()));
			List<Arrivalm> arrivalmList = deliverPlatMapper.findArrivalmList(arrivalm);
			arrivalm.setPk_supplier(pk_supplier);
			arrivalm.setPk_org(pk_org);
			return arrivalmList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 九毛九直配到货单按物资
	 * @param Arrivalm
	 * @param page
	 * @return
	 */
	public List<Arrivald> findArrivalmListSupply(Arrivalm arrivalm,Page page) throws CRUDException {
		try{
			String pk_supplier = arrivalm.getPk_supplier();
			arrivalm.setPk_supplier(CodeHelper.replaceCode(pk_supplier));
			String pk_org = arrivalm.getPk_org();
			arrivalm.setPk_org(CodeHelper.replaceCode(pk_org));
			List<Arrivald> arrivaldList = arrivaldManager.selectPage(arrivalm, page, DeliverPlatMapper.class.getName()+".findArrivalmListSupply");
			arrivalm.setPk_supplier(pk_supplier);
			arrivalm.setPk_org(pk_org);
			return arrivaldList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 九毛九直配到货单按日期
	 * @param Arrivalm
	 * @param page
	 * @return
	 */
	public List<Arrivald> findArrivalmListDate(Arrivalm arrivalm,Page page) throws CRUDException {
		try{
			String pk_supplier = arrivalm.getPk_supplier();
			arrivalm.setPk_supplier(CodeHelper.replaceCode(pk_supplier));
			String pk_org = arrivalm.getPk_org();
			arrivalm.setPk_org(CodeHelper.replaceCode(pk_org));
			List<Arrivald> arrivaldList = arrivaldManager.selectPage(arrivalm, page, DeliverPlatMapper.class.getName()+".findArrivalmListDate");
			arrivalm.setPk_supplier(pk_supplier);
			arrivalm.setPk_org(pk_org);
			return arrivaldList;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 根据主表查询到货单从表
	 * @param Arrivalm
	 * @return
	 */
	public List<Arrivald> findArrivaldList(Arrivalm arrivalm) throws CRUDException {
		try{
			return deliverPlatMapper.findArrivaldList(arrivalm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 修改九毛九直配验货
	 * @param disList
	 * @return
	 * @throws CRUDException
	 */
	public String updateArrivalm(Arrivalm arrivalm) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			arrivalm.setPk_arrival(CodeHelper.replaceCode(arrivalm.getPk_arrival()));
			deliverPlatMapper.updateArrivalm(arrivalm);
			result.put("pr", "succ");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 修改九毛九直配验货2
	 * @param disList
	 * @return
	 * @throws CRUDException
	 */
	public String updateArrivalm2(Arrivalm arrivalm) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			deliverPlatMapper.updateArrivalm2(arrivalm);
			result.put("pr", "succ");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	
	/***
	 * 九毛九直配验收入库
	 * @param arrivalmMis
	 * @param adList
	 * @return
	 */
	public String checkArrivalm(Arrivalm arrivalmMis, List<Arrivald> adList) throws CRUDException {
		try{
			
			//如果验收数量都是空，则没必要生成入库单
			boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
			if(adList.size() != 0){
				for (int j = 0; j < adList.size(); j++) {
					Arrivald dis = (Arrivald)adList.get(j);
					if(dis.getNinsnum() != 0){//如果验货数量为0，则直接跳过这条，继续下次循环
						flag = true;
						break;
					}
				}
			}
			if(!flag){//如果数量都是0  不生成入库单，直接返回成功
				deliverPlatMapper.updateChkstodByArrivalm(arrivalmMis);
				return "1";
			}
			//2.验收入库
			Chkinm chkinm = new Chkinm();
			String vouno = calChkNum.getNext(CalChkNum.CHKIN,arrivalmMis.getMaded());
			int chkinno = chkinmMapper.getMaxChkinno();
		  	chkinm.setAcct(arrivalmMis.getAcct());
		  	chkinm.setMadeby(arrivalmMis.getMadeby());
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.formatDate(arrivalmMis.getMaded(),"yyyy-MM-dd"));
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(arrivalmMis.getPk_org());
			chkinm.setPositn(positn);
			Deliver deliver = new Deliver();
			deliver.setCode(arrivalmMis.getPk_supplier());
			chkinm.setDeliver(deliver);
			chkinm.setTyp("9900");//正常入库
			chkinm.setMemo(null == arrivalmMis.getVmemo()?"直配验货生成入库单,"+arrivalmMis.getVarrbillno():arrivalmMis.getVmemo()+","+arrivalmMis.getVarrbillno());
			chkinm.setInout("in");
			chkinmMapper.saveChkinm(chkinm);
			for (int j = 0; j < adList.size(); j++) {
				Arrivald dis=(Arrivald)adList.get(j);
				if(dis.getNinsnum() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				Chkind chkind = new Chkind();//入库单从表
				Supply supply = new Supply();
				supply.setSp_code(dis.getVcode());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getNinsnum());//数量为验货数量
				chkind.setPrice(dis.getNprice());
				chkind.setMemo(dis.getVmemo());
				chkind.setDued(null);
				chkind.setInout("in");
				chkind.setPound(0);
				chkind.setSp_id(0);
				chkind.setChkstono(arrivalmMis.getChkstono());
				chkind.setAmount1(0);//可以不填 存储过程会算
				chkind.setPcno(null);//保存批次号
				chkind.setChkno(Integer.parseInt(arrivalmMis.getVarrbillno()));//arrivalm的varrbillno
				chkindMapper.saveChkind(chkind);
			}
			chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
			chkinm.setChecby(arrivalmMis.getMadeby());
			chkinm.setStatus("in");//存储过程中未用
			chkinm.setChk1memo("直配验货生成入库单审核通过");
			chkinmMapper.checkChkinm(chkinm);//审核入库单
			//3.修改报货单状态
			deliverPlatMapper.updateChkstodByArrivalm(arrivalmMis);
			//4.到货单里存入库单号
			arrivalmMis.setIchkinno(chkinno);
			deliverPlatMapper.updateArrivalm2(arrivalmMis);
			return "1";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 根据到货单条件查询当前条件下的报货门店
	 * @param arrivalmMis
	 * @return
	 */
	public List<Positn> findPositnByArrivalm(Arrivalm arrivalmMis) throws Exception{
		try{
			return deliverPlatMapper.findPositnByArrivalm(arrivalmMis);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}

	/***
	 * 按物资，门店分组查询合计
	 * @param arrivalmMis
	 * @param page 
	 * @param listPositn 
	 * @return
	 */
	public List<Map<String, Object>> findArrivaldListSum(Arrivalm arrivalmMis, List<Positn> listPositn, Page page) throws Exception{
		try{
			StringBuffer sqlStr = new StringBuffer();
			for(Positn positn : listPositn){
				sqlStr.append(" SUM(CASE WHEN M.PK_ORG='"+positn.getCode()+"' THEN ROUND(D.NPURNUM,2) ELSE 0 END) AS C_"+positn.getCode()+",");
				if(arrivalmMis.getChkstono() != 1){//要查配送单位
					sqlStr.append(" MAX(CASE WHEN M.PK_ORG='"+positn.getCode()+"' THEN PS.DISUNIT ELSE NULL END) AS D_"+positn.getCode()+",");
					sqlStr.append(" MAX(CASE WHEN M.PK_ORG='"+positn.getCode()+"' THEN ROUND(D.NPURNUM*NVL(PS.DISUNITPER,0),2) ELSE 0 END) AS U_"+positn.getCode()+",");
				}
			}
			arrivalmMis.setContent(sqlStr.toString());
			List<Map<String, Object>> list = null;
			if(null == page){
				list = deliverPlatMapper.findArrivaldListSum(arrivalmMis);
			}else{
				list = mapManager.selectPage(arrivalmMis, page, DeliverPlatMapper.class.getName()+".findArrivaldListSum");
			}
			if(arrivalmMis.getChkstono() != 1){//要查配送单位
				//写入配送单位合计
				for(Map<String,Object> map : list){
					double distotal = 0;
					String disunit = "";
					for(Positn p : listPositn){
						distotal += Double.parseDouble(map.get("U_"+p.getCode()).toString());//配送数量合计
						String du = null == map.get("D_"+p.getCode()) ? "" : map.get("D_"+p.getCode()).toString();
						if(!"NO".equals(disunit)){//所有店配送单位都一样才会有合计配送单位
							if(!"".equals(du)){//每个店的配送单位不是空
								if("".equals(disunit)){//配送单位是空
									disunit = du;
								}else{
									if(!du.equals(disunit)){//有两家店的配送单位不一样，则不能显示合计的配送单位
										disunit = "NO";
									}
								}
							}
						}
					}
					map.put("DISTOTAL", distotal);
					if(!"NO".equals(disunit))
						map.put("DISUNIT", disunit);
				}
			}
			return list;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	
	/***
	 * 
	 * @param os
	 * @param listPositn
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportArrivalm(OutputStream os, Arrivalm am, List<Positn> listPositn,List<Map<String,Object>> listMap) throws CRUDException{
		WritableWorkbook workBook = null;
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try{
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			//标准单位导出
			if(am.getChkstono() == 0){
				//新建工作表
				WritableSheet sheet = workBook.createSheet("报货汇总表", 0);
				//定义label
				Label label = new Label(0,0,"门店报货汇总",titleStyle1);
				//添加label到cell
				sheet.addCell(label);
				sheet.mergeCells(0, 0, listPositn.size()*3+6, 0);
				//设置表格表头
				sheet.addCell(new Label(0,1,"物资编码",titleStyle2));
				sheet.mergeCells(0, 1, 0, 2);
				sheet.addCell(new Label(1,1,"物资名称",titleStyle2));
				sheet.mergeCells(1, 1, 1, 2);
				sheet.addCell(new Label(2,1,"物资规格",titleStyle2));
				sheet.mergeCells(2, 1, 2, 2);
				sheet.addCell(new Label(3,1,"标准单位",titleStyle2));
				sheet.mergeCells(3, 1, 3, 2);
				sheet.addCell(new Label(4,1,"合计",titleStyle2));
				sheet.mergeCells(4, 1, 6, 1);
				sheet.addCell(new Label(4,2,"标准数量",titleStyle2));
				sheet.addCell(new Label(5,2,"配送数量",titleStyle2));
				sheet.addCell(new Label(6,2,"单位",titleStyle2));
				for(int i = 0 ; i < listPositn.size();i ++){
					sheet.addCell(new Label(i*3+7,1,listPositn.get(i).getDes(),titleStyle2));
					sheet.mergeCells(i*3+7, 1, i*3+9, 1);
					sheet.addCell(new Label(i*3+7,2,"标准数量",titleStyle2));
					sheet.addCell(new Label(i*3+8,2,"配送数量",titleStyle2));
					sheet.addCell(new Label(i*3+9,2,"单位",titleStyle2));
				}
				for(int i = 0 ; i < listMap.size() ; i ++ ){
					Map<String,Object> map = listMap.get(i);
					sheet.addCell(new Label(0,3+i,null == map.get("SP_CODE")?"":map.get("SP_CODE").toString()));
					sheet.addCell(new Label(1,3+i,null == map.get("SP_NAME")?"":map.get("SP_NAME").toString()));
					sheet.addCell(new Label(2,3+i,null == map.get("SP_DESC")?"":map.get("SP_DESC").toString()));
					sheet.addCell(new Label(3,3+i,null == map.get("UNIT")?"":map.get("UNIT").toString()));
					double total = null == map.get("TOTAL") ? 0 : Double.parseDouble(map.get("TOTAL").toString());
					sheet.addCell(new jxl.write.Number(4,3+i,total,doubleStyle));
					double distotal = null == map.get("DISTOTAL") ? 0 : Double.parseDouble(map.get("DISTOTAL").toString());
					sheet.addCell(new jxl.write.Number(5,3+i,distotal,doubleStyle));
					sheet.addCell(new Label(6,3+i,null == map.get("DISUNIT")?"":map.get("DISUNIT").toString()));
					for(int j = 0 ; j < listPositn.size(); j++){
						double val = null == map.get("C_"+listPositn.get(j).getCode()) ? 0:Double.parseDouble(map.get("C_"+listPositn.get(j).getCode()).toString());
						if(val == 0)
							sheet.addCell(new Label(j*3+7,3+i,""));
						else
							sheet.addCell(new jxl.write.Number(j*3+7,3+i,val,doubleStyle));
						double disval = null == map.get("U_"+listPositn.get(j).getCode()) ? 0:Double.parseDouble(map.get("U_"+listPositn.get(j).getCode()).toString());
						if(disval == 0)
							sheet.addCell(new Label(j*3+8,3+i,""));
						else
							sheet.addCell(new jxl.write.Number(j*3+8,3+i,disval,doubleStyle));
						sheet.addCell(new Label(j*3+9,3+i,null == map.get("D_"+listPositn.get(j).getCode())?"":map.get("D_"+listPositn.get(j).getCode()).toString()));
					}
				}
			}else if(am.getChkstono() == 3){
				//新建工作表
				WritableSheet sheet = workBook.createSheet("配送单位报货汇总表", 0);
				//定义label
				Label label = new Label(0,0,"配送单位报货汇总",titleStyle1);
				//添加label到cell
				sheet.addCell(label);
				sheet.mergeCells(0, 0, listPositn.size()*2+5, 0);
				//设置表格表头
				sheet.addCell(new Label(0,1,"物资编码",titleStyle2));
				sheet.mergeCells(0, 1, 0, 2);
				sheet.addCell(new Label(1,1,"物资名称",titleStyle2));
				sheet.mergeCells(1, 1, 1, 2);
				sheet.addCell(new Label(2,1,"物资规格",titleStyle2));
				sheet.mergeCells(2, 1, 2, 2);
				sheet.addCell(new Label(3,1,"标准单位",titleStyle2));
				sheet.mergeCells(3, 1, 3, 2);
				sheet.addCell(new Label(4,1,"合计",titleStyle2));
				sheet.mergeCells(4, 1, 5, 1);
				sheet.addCell(new Label(4,2,"配送数量",titleStyle2));
				sheet.addCell(new Label(5,2,"单位",titleStyle2));
				for(int i = 0 ; i < listPositn.size();i ++){
					sheet.addCell(new Label(i*2+6,1,listPositn.get(i).getDes(),titleStyle2));
					sheet.mergeCells(i*2+6, 1, i*2+7, 1);
					sheet.addCell(new Label(i*2+6,2,"配送数量",titleStyle2));
					sheet.addCell(new Label(i*2+7,2,"单位",titleStyle2));
				}
				for(int i = 0 ; i < listMap.size() ; i ++ ){
					Map<String,Object> map = listMap.get(i);
					sheet.addCell(new Label(0,3+i,null == map.get("SP_CODE")?"":map.get("SP_CODE").toString()));
					sheet.addCell(new Label(1,3+i,null == map.get("SP_NAME")?"":map.get("SP_NAME").toString()));
					sheet.addCell(new Label(2,3+i,null == map.get("SP_DESC")?"":map.get("SP_DESC").toString()));
					sheet.addCell(new Label(3,3+i,null == map.get("UNIT")?"":map.get("UNIT").toString()));
					double distotal = null == map.get("DISTOTAL") ? 0 : Double.parseDouble(map.get("DISTOTAL").toString());
					sheet.addCell(new jxl.write.Number(4,3+i,distotal,doubleStyle));
					sheet.addCell(new Label(5,3+i,null == map.get("DISUNIT")?"":map.get("DISUNIT").toString()));
					for(int j = 0 ; j < listPositn.size(); j++){
						double disval = null == map.get("U_"+listPositn.get(j).getCode()) ? 0:Double.parseDouble(map.get("U_"+listPositn.get(j).getCode()).toString());
						if(disval == 0)
							sheet.addCell(new Label(j*2+6,3+i,""));
						else
							sheet.addCell(new jxl.write.Number(j*2+6,3+i,disval,doubleStyle));
						sheet.addCell(new Label(j*2+7,3+i,null == map.get("D_"+listPositn.get(j).getCode())?"":map.get("D_"+listPositn.get(j).getCode()).toString()));
					}
				}
			}else{
				//新建工作表
				WritableSheet sheet = workBook.createSheet("标准单位报货汇总表", 0);
				//定义label
				Label label = new Label(0,0,"标准单位报货汇总",titleStyle1);
				//添加label到cell
				sheet.addCell(label);
				sheet.mergeCells(0, 0, listPositn.size()+4, 0);
				//设置表格表头
				sheet.addCell(new Label(0,1,"物资编码",titleStyle2));
				sheet.addCell(new Label(1,1,"物资名称",titleStyle2));
				sheet.addCell(new Label(2,1,"物资规格",titleStyle2));
				sheet.addCell(new Label(3,1,"标准单位",titleStyle2));
				sheet.addCell(new Label(4,1,"合计",titleStyle2));
				for(int i = 0 ; i < listPositn.size();i ++){
					sheet.addCell(new Label(i+5,1,listPositn.get(i).getDes(),titleStyle2));
				}
				for(int i = 0 ; i < listMap.size() ; i ++ ){
					Map<String,Object> map = listMap.get(i);
					sheet.addCell(new Label(0,2+i,null == map.get("SP_CODE")?"":map.get("SP_CODE").toString()));
					sheet.addCell(new Label(1,2+i,null == map.get("SP_NAME")?"":map.get("SP_NAME").toString()));
					sheet.addCell(new Label(2,2+i,null == map.get("SP_DESC")?"":map.get("SP_DESC").toString()));
					sheet.addCell(new Label(3,2+i,null == map.get("UNIT")?"":map.get("UNIT").toString()));
					double total = null == map.get("TOTAL") ? 0 : Double.parseDouble(map.get("TOTAL").toString());
					sheet.addCell(new jxl.write.Number(4,2+i,total,doubleStyle));
					for(int j = 0 ; j < listPositn.size(); j++){
						double val = null == map.get("C_"+listPositn.get(j).getCode()) ? 0:Double.parseDouble(map.get("C_"+listPositn.get(j).getCode()).toString());
						if(val == 0)
							sheet.addCell(new Label(j+5,2+i,""));
						else
							sheet.addCell(new jxl.write.Number(j+5,2+i,val,doubleStyle));
					}
				}
			}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 按物资
	 * @param os
	 * @param listPositn
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportArrivalm(String name,ServletOutputStream os, List<Arrivald> amList) throws CRUDException{
		WritableWorkbook workBook = null;
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 16,  
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
				Colour.BLACK);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
				Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try{
			titleStyle1.setAlignment(Alignment.CENTRE);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet(name, 0);
			//定义label
			Label label = new Label(0,0,name,titleStyle1);
			//添加label到cell
			sheet.addCell(label);
			sheet.mergeCells(0, 0, 13, 0);
			//设置表格表头
			sheet.addCell(new Label(0,1,"供应商编码",titleStyle2));
			sheet.addCell(new Label(1,1,"供应商名称",titleStyle2));
			sheet.addCell(new Label(2,1,"分店编码",titleStyle2));
			sheet.addCell(new Label(3,1,"分店名称",titleStyle2));
			sheet.addCell(new Label(4,1,"物资编码",titleStyle2));
			sheet.addCell(new Label(5,1,"物资名称",titleStyle2));
			sheet.addCell(new Label(6,1,"物资规格",titleStyle2));
			sheet.addCell(new Label(7,1,"报货数量",titleStyle2));
			sheet.addCell(new Label(8,1,"到货数量",titleStyle2));
			sheet.addCell(new Label(9,1,"验货数量",titleStyle2));
			sheet.addCell(new Label(10,1,"差异数量",titleStyle2));
			sheet.addCell(new Label(11,1,"标准单位",titleStyle2));
			sheet.addCell(new Label(12,1,"单价",titleStyle2));
			sheet.addCell(new Label(13,1,"金额",titleStyle2));
			for(int i = 0 ; i < amList.size() ; i ++ ){
				Arrivald ad = amList.get(i);
				sheet.addCell(new Label(0,2+i,ad.getPk_supplier()));
				sheet.addCell(new Label(1,2+i,ad.getDeliverDes()));
				sheet.addCell(new Label(2,2+i,ad.getPk_org()));
				sheet.addCell(new Label(3,2+i,ad.getPositnDes()));
				sheet.addCell(new Label(4,2+i,ad.getVcode()));
				sheet.addCell(new Label(5,2+i,ad.getVname()));
				sheet.addCell(new Label(6,2+i,ad.getSp_desc()));
				sheet.addCell(new jxl.write.Number(7,2+i,ad.getNpurnum(),doubleStyle));
				sheet.addCell(new jxl.write.Number(8,2+i,ad.getNarrnum(),doubleStyle));
				sheet.addCell(new jxl.write.Number(9,2+i,ad.getNinsnum(),doubleStyle));
				sheet.addCell(new jxl.write.Number(10,2+i,ad.getNarrnum()-ad.getNinsnum(),doubleStyle));
				sheet.addCell(new Label(11,2+i,ad.getPk_unit()));
				sheet.addCell(new jxl.write.Number(12,2+i,ad.getNprice(),doubleStyle));
				sheet.addCell(new jxl.write.Number(13,2+i,ad.getNmoney(),doubleStyle));
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 按物资
	 * @param os
	 * @param listPositn
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportArrivalm2(String name,ServletOutputStream os, List<Arrivald> amList) throws CRUDException{
		WritableWorkbook workBook = null;
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 16,  
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
				Colour.BLACK);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
				Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try{
			titleStyle1.setAlignment(Alignment.CENTRE);
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet(name, 0);
			//定义label
			Label label = new Label(0,0,name,titleStyle1);
			//添加label到cell
			sheet.addCell(label);
			sheet.mergeCells(0, 0, 14, 0);
			//设置表格表头
			sheet.addCell(new Label(0,1,"供应商编码",titleStyle2));
			sheet.addCell(new Label(1,1,"供应商名称",titleStyle2));
			sheet.addCell(new Label(2,1,"分店编码",titleStyle2));
			sheet.addCell(new Label(3,1,"分店名称",titleStyle2));
			sheet.addCell(new Label(4,1,"单据日期",titleStyle2));
			sheet.addCell(new Label(5,1,"物资编码",titleStyle2));
			sheet.addCell(new Label(6,1,"物资名称",titleStyle2));
			sheet.addCell(new Label(7,1,"物资规格",titleStyle2));
			sheet.addCell(new Label(8,1,"报货数量",titleStyle2));
			sheet.addCell(new Label(9,1,"到货数量",titleStyle2));
			sheet.addCell(new Label(10,1,"验货数量",titleStyle2));
			sheet.addCell(new Label(11,1,"差异数量",titleStyle2));
			sheet.addCell(new Label(12,1,"标准单位",titleStyle2));
			sheet.addCell(new Label(13,1,"单价",titleStyle2));
			sheet.addCell(new Label(14,1,"金额",titleStyle2));
			for(int i = 0 ; i < amList.size() ; i ++ ){
				Arrivald ad = amList.get(i);
				sheet.addCell(new Label(0,2+i,ad.getPk_supplier()));
				sheet.addCell(new Label(1,2+i,ad.getDeliverDes()));
				sheet.addCell(new Label(2,2+i,ad.getPk_org()));
				sheet.addCell(new Label(3,2+i,ad.getPositnDes()));
				sheet.addCell(new Label(4,2+i,ad.getDarrbilldate()));
				sheet.addCell(new Label(5,2+i,ad.getVcode()));
				sheet.addCell(new Label(6,2+i,ad.getVname()));
				sheet.addCell(new Label(7,2+i,ad.getSp_desc()));
				sheet.addCell(new jxl.write.Number(8,2+i,ad.getNpurnum(),doubleStyle));
				sheet.addCell(new jxl.write.Number(9,2+i,ad.getNarrnum(),doubleStyle));
				sheet.addCell(new jxl.write.Number(10,2+i,ad.getNinsnum(),doubleStyle));
				sheet.addCell(new jxl.write.Number(11,2+i,ad.getNarrnum()-ad.getNinsnum(),doubleStyle));
				sheet.addCell(new Label(12,2+i,ad.getPk_unit()));
				sheet.addCell(new jxl.write.Number(13,2+i,ad.getNprice(),doubleStyle));
				sheet.addCell(new jxl.write.Number(14,2+i,ad.getNmoney(),doubleStyle));
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 查询所有的入库单(供应商结算)
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinmNew(Chkinm chkinm,Page page,Date madedEnd,String locale,String cwqx, String accountId) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			if(null!=chkinm && null!=chkinm.getInout() && "zf".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.conk);
			}else if(null!=chkinm && null!=chkinm.getInout() && "rk".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.in);
			}
            if(chkinm!=null&&chkinm.getPositn()!=null&&chkinm.getPositn().getCode()!=null){
                String codes[]=chkinm.getPositn().getCode().split(",");
                chkinm.getPositn().setListCode(Arrays.asList(codes));
            }
            map.put("locale", locale);
			map.put("chkinm", chkinm);
			map.put("checkOrNot", "check");
			map.put("madedEnd", madedEnd);//checkOrNot == check 或者 uncheck   时候 模糊查询  已审核或则未审核
			map.put("cwqx", cwqx);
			map.put("accountId", accountId);
			return pageManager.selectPage(map,page,DeliverPlatMapper.class.getName()+".findAllChkinmNew");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}

}
