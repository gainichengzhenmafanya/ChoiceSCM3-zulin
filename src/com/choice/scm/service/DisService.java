package com.choice.scm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.DisMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.util.CalChkNum;
@Service
public class DisService {

	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private DisMapper disMapper;
	@Autowired
	private PageManager<Dis> pageManager;
	@Autowired
	private PageManager<SupplyAcct> pageManager1;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	private final transient Log log = LogFactory.getLog(DisService.class);
	
	/**
	 * 报货分拨、报货验收
	 */
	public List<Dis> findAllDis(Dis dis,Page page) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=pageManager.selectPage(dis, page, DisMapper.class.getName()+".findAllDis"); 			
			//list=disMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨、报货验收 ajax
	 */
	public List<Dis> findAllDisPage(Dis dis) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			
			//mysql pageSize
			int start = dis.getStartNum();
			int end = dis.getEndNum();
			dis.setPageSize(end-start);
			
			list=disMapper.findAllDisPage(dis); 			
			//list=disMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public List<Dis> findAllMisDisPage(Dis dis) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=disMapper.findAllMisDisPage(dis); 			
			//list=disMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨、报货验收 总数
	 */
	public int findAllDisCount(Dis dis) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			int count = disMapper.findAllDisCount(dis); 	
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return 	count;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨/报货验收，打印用，不带分页
	 */
	public List<Dis> findAllDisForReport(Dis dis) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=disMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量修改       验收入库     验收出库    采购确认  采购审核   等成功后的标志字段
	 */
	public void updateByIds(Dis dis,String accountName) throws CRUDException {
		try {
			if(null!=dis.getChk1() && !"".equals(dis.getChk1())){
				dis.setChk1emp(accountName);//采购确认人
				dis.setChk1tim(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));//采购确认时间
			}
			if(null!=dis.getSta() && !"".equals(dis.getSta())){
				dis.setChk2emp(accountName);//采购审核人
				dis.setChk2tim(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));//采购审核时间
			}
			disMapper.updateByIds(dis);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 保存修改
	 */
	public String updateDis(DisList disList) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			for (int i = 0; i < disList.getListDis().size(); i++) {
				String chkin=findChkByKey(disList.getListDis().get(i).getId()).get(0).getChkin();
				String chkout=findChkByKey(disList.getListDis().get(i).getId()).get(0).getChkout();
				
				//已经入库或者出库的不能修改
				if("Y".equals(chkin) || "Y".equals(chkout)){
					result.put("pr", "fail");
				}else{
					Dis dis=new Dis();
					dis.setId(disList.getListDis().get(i).getId());
					dis.setDeliverCode(disList.getListDis().get(i).getDeliverCode());
					dis.setAmountin(disList.getListDis().get(i).getAmountin());
					dis.setAmount1in(disList.getListDis().get(i).getAmount1in());//参考数量
					dis.setPricein(disList.getListDis().get(i).getPricein());
					dis.setInd(disList.getListDis().get(i).getInd());
					dis.setBak2(disList.getListDis().get(i).getBak2());
					
					disMapper.updateDis(dis);
					
					result.put("pr", "succ");
				}
			}
			result.put("updateNum", disList.getListDis().size());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询出入库状态
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findChkByKey(String id) throws CRUDException {
		try {
			return disMapper.findChkByKey(id);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 报货分拨，汇总
	 */
	public List<Dis> findTotal(Dis dis,Page page) throws CRUDException {
		try {
			return pageManager.selectPage(dis, page, DisMapper.class.getName()+".findTotal");
			//return disMapper.findTotal();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 获得收货单数据（分店接受配送中心送货用）
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<SupplyAcct> selectByKey(HashMap<String, Object> disMap, Page page) throws CRUDException
	{
		try {
			return pageManager1.selectPage(disMap, page, DisMapper.class.getName()+".selectByKey");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public List<SupplyAcct> selectByKey(HashMap<String, Object> disMap) throws CRUDException
	{
		try {
			return disMapper.selectByKey(disMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新验货信息
	 * @param supply
	 * @param acct
	 * @throws CRUDException
	 */
	public void updateAcct(SupplyAcct supplyAcct, String acct) throws CRUDException {
		try {
			for (int i = 0; i < supplyAcct.getSupplyAcctList().size(); i++) {//批量修改
				SupplyAcct supplyAcct_=new SupplyAcct();
				supplyAcct_.setAcct(acct);
				supplyAcct_.setId(supplyAcct.getSupplyAcctList().get(i).getId());
				supplyAcct_.setCntfirm(supplyAcct.getSupplyAcctList().get(i).getCntfirm());
				supplyAcct_.setCntfirm1(supplyAcct.getSupplyAcctList().get(i).getCntfirm1());
				supplyAcct_.setCntfirmks(supplyAcct.getSupplyAcctList().get(i).getCntfirmks());
				supplyAcct_.setCntlogistks(supplyAcct.getSupplyAcctList().get(i).getCntlogistks());
				disMapper.updateAcct(supplyAcct_);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 更新验货审核信息
	 * @param supply
	 * @param acct
	 * @throws CRUDException
	 */
	public void check(String ids, String accountName, String acct) throws CRUDException {
		try {
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("accountName", accountName);
			disMap.put("acct", acct);
			disMap.put("check", 2);//只查为验货的单据（防止多帐号同时操作该菜单） wj 2015年5月18日 09:42:53
			disMap.put("listids", CodeHelper.replaceCode(ids));
			//生成门店的入库单
			List<SupplyAcct> disList = disMapper.selectByKey(disMap);
			disMapper.check(disMap);
			log.warn("配送中心到货验收入库：");
			//最后处理  list大小不为0  时候  在作为一条入库单据加入
			if(disList.size()!=0){
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < disList.size(); j++) {
					SupplyAcct dis=(SupplyAcct)disList.get(j);
					if(dis.getCntfirm() != 0){
						flag = true;
//						break;
					}else{//特殊情况 如果验货数量是0（可能是直接审核的极端情况  需要特殊处理一下wjf）
						SupplyAcct supplyAcct_ = new SupplyAcct();
						supplyAcct_.setAcct(acct);
						supplyAcct_.setId(dis.getId());
						supplyAcct_.setCntfirm(0);
						supplyAcct_.setCntfirm1(0);
						supplyAcct_.setCntfirmks(dis.getCntout());//门店亏
						supplyAcct_.setCntlogistks(0);
						disMapper.updateAcct(supplyAcct_);
					}
				}
				if(flag){
					saveChkinmBySupplyAcct(disList,acct,accountName);

				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 验收入库转为 入库单的方法！ 
	 */
	public void saveChkinmBySupplyAcct(List<SupplyAcct> list,String acct,String accountName) throws CRUDException {
		try {
			log.warn("配送中心到货验收入库生成入库单:");
			Date date=new Date();
			Chkinm chkinm=new Chkinm();
//			String vouno=calChkNum.getNext(CalChkNum.CHKIN,date);
			String vouno=calChkNum.getNext(CalChkNum.CHKIN,list.get(list.size()-1).getDat());//取得 最后一个日期  2015.7.20 xlh
			int chkinno = chkinmMapper.getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
//		  	chkinm.setMaded(DateFormat.formatDate(date,"yyyy-MM-dd HH:mm:ss"));
		  	chkinm.setMaded(list.get(list.size()-1).getDat());//取得 最后一个日期  2015.7.20 xlh
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(list.get(0).getFirm());
			chkinm.setPositn(positn);
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = disMapper.findTpDeliver(positn);
			chkinm.setDeliver(deliver);
			chkinm.setTyp("9900");
			chkinm.setInout("in");  //直发单也当成 入库
			chkinmMapper.saveChkinm(chkinm);
			for (int j = 0; j < list.size(); j++) {
				SupplyAcct dis=(SupplyAcct)list.get(j);
				if(dis.getCntfirm() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
					continue;
				}
				Chkind chkind=new Chkind();//入库单从表
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getCntfirm());//数量为门店验货数量
				chkind.setPrice(dis.getPricesale());//售价为0取出库价
				chkind.setMemo(dis.getDes());
				chkind.setDued(dis.getDued());
				chkind.setInout("in");
				chkind.setPound(0);
				chkind.setSp_id(dis.getSpno());
				chkind.setChkstono(Integer.parseInt(null == dis.getChkstoNo() ? "0" : dis.getChkstoNo()));
				chkind.setAmount1(dis.getCntfirm1());//
				chkind.setPcno(dis.getPcno());//保存批次号
				chkindMapper.saveChkind(chkind);
				//如果报货来的，则修改CHKYH = Y ，安全库存报货用
				if(null != dis.getChkstoNo() && !"0".equals(dis.getChkstoNo())){
					disMapper.updateChkstodByChkstonoSpcode(chkind);
				}
			}
			chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
			chkinm.setChecby(accountName);
			chkinm.setStatus("in");
			chkinm.setChk1memo("统配验货生成入库单审核通过");
			chkinmMapper.checkChkinm(chkinm);//审核入库单
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨，采购汇总
	 */
	public List<Dis> findCaiGouTotal(Dis dis,Page page) throws CRUDException {
		try {
			if (page==null) {
				return disMapper.findCaiGouTotal(dis);
			}else {
				return pageManager.selectPage(dis, page, DisMapper.class.getName()+".findCaiGouTotal");
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 获取入库单数据
	 */
	public List<Dis> findSupplyacctList(Dis dis,String iszp) throws CRUDException
	{
		try {
//			dis.setChkstoNos(CodeHelper.replaceCode(dis.getChkstoNos()));
//			dis.setId(CodeHelper.replaceCode(dis.getId()));
			if("Y".equals(iszp)){//直配验货查询
				return disMapper.findChkstodList(dis);
			}
			return disMapper.findSupplyacctList(dis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 判断物资是否属于多档口
	 */
	public List<Dis> findSupplyacctDeptList(Dis dis) throws CRUDException
	{
		try {
//			dis.setChkstoNos(CodeHelper.replaceCode(dis.getChkstoNos()));
			dis.setId(CodeHelper.replaceCode(dis.getId()));
			return disMapper.findSupplyacctDeptList(dis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  验收入库方法
	 * @param chkinm
	 * @throws CRUDException
	 */
	public void saveYsrkChkinm(List<Dis> listDistribution,String acct,Dis dis,String positnIn,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("验收入库：");
			String deliverCode=listDistribution.get(0).getDeliverCode();//供应商code
			ArrayList<Dis>  list=new ArrayList<Dis>();//临时list
			for (int i = 0; i < listDistribution.size(); i++) {//循环数据
				if(listDistribution.get(i).getDeliverCode().equals(deliverCode)){
					list.add(listDistribution.get(i));
				}else{//不相等的时候
					if(list.size()!=0){
						//如果验收数量都是空，则没必要生成入库单
						boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
						for (int j = 0; j < list.size(); j++) {
							Dis d=(Dis)list.get(j);
							if(d.getAmountin() != 0){
								flag = true;
								break;
							}
						}
						if(flag){
							saveChkinmByChkstom(list,acct,positnIn,maded,accountName);
						}
					}
					list.clear();//清空
					list.add(listDistribution.get(i));//在list重新加入该条数据
				}
				deliverCode=listDistribution.get(i).getDeliverCode();//把循环外的值换为当前数据
			}
			if(list.size()!=0){
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < list.size(); j++) {
					Dis d=(Dis)list.get(j);
					if(d.getAmountin() != 0){
						flag = true;
						break;
					}
				}
				if(flag){
					saveChkinmByChkstom(list,acct,positnIn,maded,accountName);
				}
			}
			dis.setChkin("Y");//已入库
			dis.setChkyh("Y");//已验货
			dis.setEmpfirm(accountName);//验货人
			dis.setId(dis.getInCondition());
			disMapper.updateByIds(dis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 验收入库转为 入库单的方法！ 
	 */
	public void saveChkinmByChkstom(ArrayList<Dis> list,String acct,String positnIn,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("供应商到货生成入库单:");
			Chkinm chkinm=new Chkinm();
			String vouno=calChkNum.getNext(CalChkNum.CHKIN,maded);
			int chkinno = chkinmMapper.getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.formatDate(maded,"yyyy-MM-dd HH:mm:ss"));
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			positn.setCode(positnIn);
			chkinm.setPositn(positn);
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = new Deliver();
			deliver.setCode(list.get(0).getDeliverCode());
			chkinm.setDeliver(deliver);
			chkinm.setTyp("9900");
			chkinm.setInout("in");  //直发单也当成 入库
			chkinmMapper.saveChkinm(chkinm);
			for (int j = 0; j < list.size(); j++) {
				Dis dis=(Dis)list.get(j);
				Chkind  chkind=new Chkind();//入库单从表
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getAmountin());//数量为采购数量 而非报货数量
				chkind.setPrice(dis.getPricein());
				chkind.setMemo(dis.getMemo());
				chkind.setDued(dis.getDued());
				chkind.setInout("in");
				chkind.setPound(0);
				chkind.setSp_id(dis.getSp_id());
				chkind.setChkstono(dis.getChkstoNo());
				chkind.setAmount1(dis.getAmount1in());//这里改为取amount1in  2014.12.30wjf 也能兼容以前版本
				chkind.setPcno(dis.getPcno());//保存批次号
				chkindMapper.saveChkind(chkind);
			}
			chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
			chkinm.setChecby(accountName);
			chkinm.setStatus("in");
			chkinmMapper.checkChkinm(chkinm);//审核入库单
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}

	/***
	 * 生成档口直发单
	 * @author wjf
	 * @param chkinm
	 * @return
	 */
	public int saveChkinmDept(Chkinm chkinm,String ids,String sp_ids) {
		//1.先更新状态
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("listids", CodeHelper.replaceCode(ids));
		disMap.put("accountName", chkinm.getMadeby());
		disMap.put("acct", chkinm.getAcct());
		disMapper.check(disMap);
		//1.5 更新报货单表chkstod为已验货状态 安全库存报货用
		disMapper.updateChkstodByChkstodDeptId(sp_ids);
		//2.生成档口的直发单
		log.warn("配送中心到货直发档口：");
		//最后处理  list大小不为0  时候  在作为一条入库单据加入
		if(chkinm.getChkindList().size()!=0){
			//如果验收数量都是空，则没必要生成入库单
			boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind dis=(Chkind)chkinm.getChkindList().get(j);
				if(dis.getAmount() != 0){
					flag = true;
					break;
				}
			}
			if(flag){
				saveChkinmzbDeptBySupplyAcct(chkinm,ids);
			}
		}
		//3.更新chkstod_dept chkin  字段为已入库
		disMapper.updateChkstodDeptChkin(sp_ids);
		return 1;
	}	
	private int saveChkinmzbDeptBySupplyAcct(Chkinm chkinm,String ids) {
		try {
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
			chkinm.setMaded(new Date());
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = disMapper.findTpDeliver(chkinm.getPositn());
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(ChkinmConstants.conk);
			chkinm.setTyp("9916");//正常直发
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setInout(ChkinmConstants.conk);   //加入直发标志
				chkind.setPound(0);
				chkindMapper.saveChkind(chkind);
				//如果报货来的，则修改CHKYH = Y ，安全库存报货用
				if(0 != chkind.getChkstono()){
					disMapper.updateChkstodByChkstonoSpcode(chkind);
				}
			}
			//审核
			chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
			//  此字段当成月份使用     加入会计月 
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(chkinm.getInout());
			chkinmMapper.checkChkinm(chkinm);//审核入库单
			HashMap<String, Object> disMap = new HashMap<String, Object>();
			disMap.put("listids", ids);
			disMapper.updateYnDept(disMap);
			return chkinm.getPr();
		} catch (CRUDException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/***
	 * 生成档口直发单（直配验货）
	 * @author wjf
	 * @param chkinm
	 * @return
	 */
	public int saveChkinmDept_zp(Chkinm chkinm,String ids,String sp_ids) {
			log.warn("验收入库：");
			String deliverCode = chkinm.getChkindList().get(0).getDeliver();//供应商code
			ArrayList<Chkind> list=new ArrayList<Chkind>();//临时list
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {//循环数据
				if(chkinm.getChkindList().get(i).getDeliver().equals(deliverCode)){
					list.add(chkinm.getChkindList().get(i));
				}else{//不相等的时候
					if(list.size()!=0){
						//如果验收数量都是空，则没必要生成入库单
						boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
						for (int j = 0; j < list.size(); j++) {
							Chkind d=(Chkind)list.get(j);
							if(d.getAmount() != 0){
								flag = true;
								break;
							}
						}
						if(flag){
							saveChkinmzbByChkstom(chkinm,list);
						}
					}
					list.clear();//清空
					list.add(chkinm.getChkindList().get(i));//在list重新加入该条数据
				}
				deliverCode=chkinm.getChkindList().get(i).getDeliver();//把循环外的值换为当前数据
			}
			if(list.size()!=0){
				//如果验收数量都是空，则没必要生成入库单
				boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
				for (int j = 0; j < list.size(); j++) {
					Chkind d=(Chkind)list.get(j);
					if(d.getAmount() != 0){
						flag = true;
						break;
					}
				}
				if(flag){
					saveChkinmzbByChkstom(chkinm,list);
				}
			}
			Dis dis = new Dis();
			dis.setChkin("Y");//已入库
			dis.setChkyh("Y");//已验货
			dis.setEmpfirm(chkinm.getMadeby());//验货人
			dis.setId(ids);
			disMapper.updateByIds(dis);
			//更新chkstod_dept chkin  字段为已入库
			disMapper.updateChkstodDeptChkin(sp_ids);
		return 1;
	}	
	private int saveChkinmzbByChkstom(Chkinm chkinm,ArrayList<Chkind> list) {
		try{
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
			chkinm.setMaded(null == chkinm.getMaded()?new Date():chkinm.getMaded());
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = new Deliver();
			deliver.setCode(list.get(0).getDeliver());//供应商为真正的供应商
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(ChkinmConstants.conk);
			chkinm.setTyp("9916");//正常直发
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMapper.saveChkinm(chkinm);
	 		for (int j = 0; j < list.size(); j++) {
				Chkind chkind = list.get(j);//入库单从表
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setInout(ChkinmConstants.conk);   //加入直发标志
				chkind.setPound(0);
				chkindMapper.saveChkind(chkind);
			}
			//审核
	 		chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
			//  此字段当成月份使用     加入会计月 
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(chkinm.getInout());
			chkinmMapper.checkChkinm(chkinm);//审核入库单
			return chkinm.getPr();
		} catch (CRUDException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/****
	 * 判断当前条件下是否有档口报货单生产的验货单
	 * @param ids
	 * @return
	 */
	public int findDeptChkstom(String ids) {
		return disMapper.findDeptChkstom(ids);
	}
	
	/***
	 * 直配验货生成档口直发单或者门店入库单
	 * @author wjf
	 * @param chkinm
	 * @return
	 * @throws Exception 
	 */
	public int saveChkinmGroupZP(Chkinm chkinm,String ids) throws Exception {
		//1.先更新状态
		Dis di = new Dis();
		di.setChkin("Y");//已入库
		di.setChkyh("Y");//已验货
		di.setEmpfirm(chkinm.getMadeby());//验货人
		di.setId(ids);
		disMapper.updateByIds(di);
		
		//判断走门店入库 还是档口直发
		if(chkinm.getChkindList().size()!=0){
			List<Chkind> firmChkind = new ArrayList<Chkind>();
			List<Chkind> deptChkind = new ArrayList<Chkind>();
			//如果验收数量都是空，则没必要生成入库单
			boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind dis=(Chkind)chkinm.getChkindList().get(j);
				if(dis.getAmount() != 0){
					flag = true;
					//档口的和门店的分开
					if(null == dis.getSupply().getSp_name() || "".equals(dis.getSupply().getSp_name())){//档口的
						deptChkind.add(dis);
					}else{
						firmChkind.add(dis);
					}
				}
			}
			if(flag){
				//先入门店
				if(firmChkind.size()!=0){
					String deliver = firmChkind.get(0).getDeliver();
					List<Chkind> list = new ArrayList<Chkind>();
					for (int i = 0; i < firmChkind.size(); i++) {//循环数据
						if(deliver.equals(firmChkind.get(i).getDeliver())){
							list.add(firmChkind.get(i));
						}else{//不相等的时候
							if(list.size() != 0){
								chkinm.setChkindList(list);
								saveChkinmBySupplyAcctZP(chkinm,ids);
								list.clear();//清空
								list.add(firmChkind.get(i));//在list重新加入该条数据
							}
							deliver = firmChkind.get(i).getDeliver();//把循环外的值换为当前数据
						}
					}
					if(list.size() != 0){
						chkinm.setChkindList(list);
						saveChkinmBySupplyAcctZP(chkinm,ids);
					}
				}
				//再直发档口
				if(deptChkind.size()!=0){
					String deliver = deptChkind.get(0).getDeliver();
					List<Chkind> list = new ArrayList<Chkind>();
					for (int i = 0; i < deptChkind.size(); i++) {//循环数据
						if(deliver.equals(deptChkind.get(i).getDeliver())){
							list.add(deptChkind.get(i));
						}else{//不相等的时候
							if(list.size() != 0){
								chkinm.setChkindList(list);
								saveChkinmzbDeptBySupplyAcctZP(chkinm,ids);
								list.clear();//清空
								list.add(deptChkind.get(i));//在list重新加入该条数据
							}
							deliver = deptChkind.get(i).getDeliver();//把循环外的值换为当前数据
						}
					}
					if(list.size() != 0){
						chkinm.setChkindList(list);
						saveChkinmzbDeptBySupplyAcctZP(chkinm,ids);
					}
				}
			}
		}
		return 1;
	}
	
	private int saveChkinmBySupplyAcctZP(Chkinm chkinm,String ids) throws Exception{
		chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		chkinm.setChkinno(chkinmMapper.getMaxChkinno());
		chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
		chkinm.setMaded(new Date());
		chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
		Positn positn=new Positn();
		positn.setCode(chkinm.getChkindList().get(0).getDeliver());
		Deliver deliver = new Deliver();
		deliver.setCode(chkinm.getChkindList().get(0).getDeliver());//供应商为真正的供应商
		chkinm.setDeliver(deliver);
		chkinm.setTotalamt(0);
		chkinm.setInout(ChkinmConstants.in);
		chkinm.setTyp("9900");//正常入库
		log.warn("添加入库单(主单)：\n"+chkinm);
 		chkinmMapper.saveChkinm(chkinm);
		for (int j = 0; j < chkinm.getChkindList().size(); j++) {
			Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
			chkind.setChkinno(chkinm.getChkinno());
			chkind.setIndept(null);
			chkind.setInout(ChkinmConstants.in);
			chkind.setPound(0);
			chkindMapper.saveChkind(chkind);
		}
		//审核
		chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月 
		chkinm.setChecby(chkinm.getMadeby());
		chkinm.setStatus(chkinm.getInout());
		chkinmMapper.checkChkinm(chkinm);//审核入库单
		return chkinm.getPr();
	}
	
	private int saveChkinmzbDeptBySupplyAcctZP(Chkinm chkinm,String ids) throws Exception{
		chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		chkinm.setChkinno(chkinmMapper.getMaxChkinno());
		chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
		chkinm.setMaded(new Date());
		chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
		Positn positn=new Positn();
		positn.setCode(chkinm.getChkindList().get(0).getDeliver());
		Deliver deliver = new Deliver();
		deliver.setCode(chkinm.getChkindList().get(0).getDeliver());//供应商为真正的供应商
		chkinm.setDeliver(deliver);
		chkinm.setTotalamt(0);
		chkinm.setInout(ChkinmConstants.conk);
		chkinm.setTyp("9916");//正常直发
		log.warn("添加直发单(主单)：\n"+chkinm);
 		chkinmMapper.saveChkinm(chkinm);
		for (int j = 0; j < chkinm.getChkindList().size(); j++) {
			Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
			chkind.setChkinno(chkinm.getChkinno());
			chkind.setInout(ChkinmConstants.conk);   //加入直发标志
			chkind.setPound(0);
			chkindMapper.saveChkind(chkind);
		}
		//审核
		chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
		//  此字段当成月份使用     加入会计月 
		chkinm.setChecby(chkinm.getMadeby());
		chkinm.setStatus(chkinm.getInout());
		chkinmMapper.checkChkinm(chkinm);//审核入库单
		return chkinm.getPr();
	}
	
	/***
	 * 统配验货生成档口直发单或者门店入库单
	 * @author wjf
	 * @param chkinm
	 * @return
	 */
	public int saveChkinmGroup(Chkinm chkinm,String ids) {
		//1.先更新状态
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("listids", CodeHelper.replaceCode(ids));
		disMap.put("accountName", chkinm.getMadeby());
		disMap.put("acct", chkinm.getAcct());
		disMapper.check(disMap);
		
		//判断走门店入库 还是档口直发
		if(chkinm.getChkindList().size()!=0){
			List<Chkind> firmChkind = new ArrayList<Chkind>();
			List<Chkind> deptChkind = new ArrayList<Chkind>();
			//如果验收数量都是空，则没必要生成入库单
			boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind dis=(Chkind)chkinm.getChkindList().get(j);
				if(dis.getAmount() != 0){
					flag = true;
					//档口的和门店的分开
					if(null == dis.getSupply().getSp_name() || "".equals(dis.getSupply().getSp_name())){//档口的
						deptChkind.add(dis);
					}else{
						firmChkind.add(dis);
					}
				}
				
			}
			if(flag){
				//先入门店
				chkinm.setChkindList(firmChkind);
				saveChkinmBySupplyAcct(chkinm,ids);
				//再直发档口
				chkinm.setChkindList(deptChkind);
				saveChkinmzbDeptBySupplyAcct(chkinm,ids);
			}
		}
		return 1;
	}
	
	private int saveChkinmBySupplyAcct(Chkinm chkinm,String ids) {
		try {
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
			chkinm.setMaded(new Date());
			chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
			//供应商为当前分店所属片区的主直拨库对应的供应商
			Deliver deliver = disMapper.findTpDeliver(chkinm.getPositn());
			chkinm.setDeliver(deliver);
			chkinm.setTotalamt(0);
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setTyp("9900");//正常入库
//			chkinm.setMemo("统配验货直发档口");
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMapper.saveChkinm(chkinm);
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setIndept(null);
				chkind.setInout(ChkinmConstants.in);   //加入直发标志
				chkind.setPound(0);
				chkindMapper.saveChkind(chkind);
				//如果报货来的，则修改CHKYH = Y ，安全库存报货用
				if(0 != chkind.getChkstono()){
					disMapper.updateChkstodByChkstonoSpcode(chkind);
				}
			}
			//审核
			chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
			//  此字段当成月份使用     加入会计月 
			chkinm.setChecby(chkinm.getMadeby());
			chkinm.setStatus(chkinm.getInout());
//			chkinm.setChk1memo("档口直发审核通过");
			chkinmMapper.checkChkinm(chkinm);//审核入库单
			return chkinm.getPr();
		} catch (CRUDException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * 更新验货审核信息
	 * @param supply
	 * @param acct
	 * @throws CRUDException
	 */
	public void checkDb(String ids, String accountName, String acct) throws CRUDException {
		try {
			HashMap<String, Object> disMap=new HashMap<String, Object>();
			disMap.put("listids", CodeHelper.replaceCode(ids));
			disMap.put("accountName", accountName);
			disMap.put("acct", acct);
			disMap.put("db", "db");
			disMapper.check(disMap);
			//生成门店的入库单
			List<SupplyAcct> disList = disMapper.selectByKey(disMap);
			log.warn("门店调拨到货验收入库：");
			//最后处理  list大小不为0  时候  在作为一条入库单据加入
			if(disList.size()!=0){
				String positn = disList.get(0).getPositn();
				List<SupplyAcct> list = new ArrayList<SupplyAcct>();
				for (int i = 0; i < disList.size(); i++) {//循环数据
					if(positn.equals(disList.get(i).getPositn())){
						list.add(disList.get(i));
					}else{//不相等的时候
						if(list.size() != 0){
							//如果验收数量都是空，则没必要生成入库单
							boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
							for (int j = 0; j < list.size(); j++) {
								SupplyAcct dis=(SupplyAcct)list.get(j);
								if(dis.getCntfirm() != 0){
									flag = true;
								}else{//特殊情况 如果验货数量是0（可能是直接审核的极端情况  需要特殊处理一下wjf）
									SupplyAcct supplyAcct_ = new SupplyAcct();
									supplyAcct_.setAcct(acct);
									supplyAcct_.setId(dis.getId());
									supplyAcct_.setCntfirm(0);
									supplyAcct_.setCntfirm1(0);
									supplyAcct_.setCntfirmks(dis.getCntout());//门店亏
									supplyAcct_.setCntlogistks(0);
									disMapper.updateAcct(supplyAcct_);
								}
							}
							if(flag){
								saveChkinmBySupplyAcctDb(list,acct,accountName);
							}
							list.clear();//清空
							list.add(disList.get(i));//在list重新加入该条数据
						}
						positn = disList.get(i).getPositn();//把循环外的值换为当前数据
					}
				}
				if(list.size() != 0){
					//如果验收数量都是空，则没必要生成入库单
					boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
					for (int j = 0; j < list.size(); j++) {
						SupplyAcct dis=(SupplyAcct)list.get(j);
						if(dis.getCntfirm() != 0){
							flag = true;
						}else{//特殊情况 如果验货数量是0（可能是直接审核的极端情况  需要特殊处理一下wjf）
							SupplyAcct supplyAcct_ = new SupplyAcct();
							supplyAcct_.setAcct(acct);
							supplyAcct_.setId(dis.getId());
							supplyAcct_.setCntfirm(0);
							supplyAcct_.setCntfirm1(0);
							supplyAcct_.setCntfirmks(dis.getCntout());//门店亏
							supplyAcct_.setCntlogistks(0);
							disMapper.updateAcct(supplyAcct_);
						}
					}
					if(flag){
						saveChkinmBySupplyAcctDb(list,acct,accountName);
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 验收入库转为 入库单的方法！ 
	 */
	public void saveChkinmBySupplyAcctDb(List<SupplyAcct> list,String acct,String accountName) throws CRUDException {
		log.warn("调拨验收入库生成入库单:");
		Date date=new Date();
		Chkinm chkinm=new Chkinm();
		String vouno=calChkNum.getNext(CalChkNum.CHKIN,date);
		int chkinno = chkinmMapper.getMaxChkinno();
	  	chkinm.setAcct(acct);
	  	chkinm.setMadeby(accountName);
	  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
	  	chkinm.setChkinno(chkinno);
	  	chkinm.setVouno(vouno);
	  	chkinm.setMaded(DateFormat.formatDate(date,"yyyy-MM-dd HH:mm:ss"));
		chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
		Positn p1=new Positn();
		p1.setCode(list.get(0).getFirm());
		chkinm.setPositn(p1);
		Positn positn=new Positn();
		positn.setCode(list.get(0).getPositn());
		//供应商为当前调拨出库的门店对应的供应商
		Deliver deliver = disMapper.findDeliverByPositn(positn);
		if(null == deliver){
			log.warn("没有设置仓位"+positn.getCode()+"对应的供应商！");
			throw new CRUDException("没有设置仓位"+positn.getCode()+"对应的供应商！");
		}
		chkinm.setDeliver(deliver);
		chkinm.setTyp("9940");//调拨入库
		chkinm.setInout("in");
		chkinmMapper.saveChkinm(chkinm);
		for (int j = 0; j < list.size(); j++) {
			SupplyAcct dis=(SupplyAcct)list.get(j);
			if(dis.getCntfirm() == 0){//如果验货数量为0，则直接跳过这条，继续下次循环
				continue;
			}
			Chkind chkind=new Chkind();//入库单从表
			Supply supply=new Supply();
			supply.setSp_code(dis.getSp_code());
			chkind.setChkinno(chkinno);
			chkind.setSupply(supply);
			chkind.setAmount(dis.getCntfirm());//数量为门店验货数量
			chkind.setPrice(dis.getPricesale());//售价为0取出库价
			chkind.setMemo(dis.getDes());
			chkind.setDued(dis.getDued());
			chkind.setInout("in");
			chkind.setPound(0);
			chkind.setSp_id(dis.getSpno());
			chkind.setChkstono(Integer.parseInt(null == dis.getChkstoNo() ? "0" : dis.getChkstoNo()));
			chkind.setAmount1(dis.getCntfirm1());//
			chkind.setPcno(dis.getPcno());//保存批次号
			chkindMapper.saveChkind(chkind);
		}
		chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
		chkinm.setChecby(accountName);
		chkinm.setStatus("in");
		chkinmMapper.checkChkinm(chkinm);//审核入库单
	}
	
	/***
	 * 调拨验货生成档口直发单或者门店入库单
	 * @author wjf
	 * @param chkinm
	 * @return
	 * @throws Exception 
	 */
	public int saveChkinmGroupDb(Chkinm chkinm,String ids) throws Exception {
		//1.先更新状态
		HashMap<String, Object> disMap=new HashMap<String, Object>();
		disMap.put("listids", CodeHelper.replaceCode(ids));
		disMap.put("accountName", chkinm.getMadeby());
		disMap.put("acct", chkinm.getAcct());
		disMapper.check(disMap);
		
		//判断走门店入库 还是档口直发
		if(chkinm.getChkindList().size()!=0){
			List<Chkind> firmChkind = new ArrayList<Chkind>();
			List<Chkind> deptChkind = new ArrayList<Chkind>();
			//如果验收数量都是空，则没必要生成入库单
			boolean flag = false;//默认为false,如果有一条数据验货数量不是0，则继续进行入库操作
			for (int j = 0; j < chkinm.getChkindList().size(); j++) {
				Chkind dis=(Chkind)chkinm.getChkindList().get(j);
				if(dis.getAmount() != 0){
					flag = true;
					//档口的和门店的分开
					if(null == dis.getSupply().getSp_name() || "".equals(dis.getSupply().getSp_name())){//档口的
						deptChkind.add(dis);
					}else{
						firmChkind.add(dis);
					}
				}
				
			}
			if(flag){
				//先入门店
				if(firmChkind.size()!=0){
					String deliver = firmChkind.get(0).getDeliver();
					List<Chkind> list = new ArrayList<Chkind>();
					for (int i = 0; i < firmChkind.size(); i++) {//循环数据
						if(deliver.equals(firmChkind.get(i).getDeliver())){
							list.add(firmChkind.get(i));
						}else{//不相等的时候
							if(list.size() != 0){
								chkinm.setChkindList(list);
								saveChkinmBySupplyAcctDb(chkinm,ids);
								list.clear();//清空
								list.add(firmChkind.get(i));//在list重新加入该条数据
							}
							deliver = firmChkind.get(i).getDeliver();//把循环外的值换为当前数据
						}
					}
					if(list.size() != 0){
						chkinm.setChkindList(list);
						saveChkinmBySupplyAcctDb(chkinm,ids);
					}
				}
				//再直发档口
				if(deptChkind.size()!=0){
					String deliver = deptChkind.get(0).getDeliver();
					List<Chkind> list = new ArrayList<Chkind>();
					for (int i = 0; i < deptChkind.size(); i++) {//循环数据
						if(deliver.equals(deptChkind.get(i).getDeliver())){
							list.add(deptChkind.get(i));
						}else{//不相等的时候
							if(list.size() != 0){
								chkinm.setChkindList(list);
								saveChkinmzbDeptBySupplyAcctDb(chkinm,ids);
								list.clear();//清空
								list.add(deptChkind.get(i));//在list重新加入该条数据
							}
							deliver = deptChkind.get(i).getDeliver();//把循环外的值换为当前数据
						}
					}
					if(list.size() != 0){
						chkinm.setChkindList(list);
						saveChkinmzbDeptBySupplyAcctDb(chkinm,ids);
					}
				}
			}
		}
		return 1;
	}
	
	private int saveChkinmBySupplyAcctDb(Chkinm chkinm,String ids) throws Exception{
		chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		chkinm.setChkinno(chkinmMapper.getMaxChkinno());
		chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,new Date()));
		chkinm.setMaded(new Date());
		chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
		Positn positn=new Positn();
		positn.setCode(chkinm.getChkindList().get(0).getDeliver());
		//供应商为当前调拨出库的门店对应的供应商
		Deliver deliver = disMapper.findDeliverByPositn(positn);
		if(null == deliver){
			log.warn("没有设置仓位"+positn.getCode()+"对应的供应商！");
			throw new CRUDException("没有设置仓位"+positn.getCode()+"对应的供应商！");
		}
		chkinm.setDeliver(deliver);
		chkinm.setTotalamt(0);
		chkinm.setInout(ChkinmConstants.in);
		chkinm.setTyp("9940");//调拨入库
		log.warn("添加入库单(主单)：\n"+chkinm);
 		chkinmMapper.saveChkinm(chkinm);
		for (int j = 0; j < chkinm.getChkindList().size(); j++) {
			Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
			chkind.setChkinno(chkinm.getChkinno());
			chkind.setIndept(null);
			chkind.setInout(ChkinmConstants.in);   //加入直发标志
			chkind.setPound(0);
			chkindMapper.saveChkind(chkind);
		}
		//审核
		chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月 
		chkinm.setChecby(chkinm.getMadeby());
		chkinm.setStatus(chkinm.getInout());
		chkinmMapper.checkChkinm(chkinm);//审核入库单
		return chkinm.getPr();
	}
	
	private int saveChkinmzbDeptBySupplyAcctDb(Chkinm chkinm,String ids) throws Exception{
		chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		chkinm.setChkinno(chkinmMapper.getMaxChkinno());
		chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,new Date()));
		chkinm.setMaded(new Date());
		chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
		Positn positn=new Positn();
		positn.setCode(chkinm.getChkindList().get(0).getDeliver());
		//供应商为当前调拨出库的门店对应的供应商
		Deliver deliver = disMapper.findDeliverByPositn(positn);
		if(null == deliver){
			log.warn("没有设置仓位"+positn.getCode()+"对应的供应商！");
			throw new CRUDException("没有设置仓位"+positn.getCode()+"对应的供应商！");
		}
		chkinm.setDeliver(deliver);
		chkinm.setTotalamt(0);
		chkinm.setInout(ChkinmConstants.conk);
		chkinm.setTyp("9916");//正常直发
		log.warn("添加直发单(主单)：\n"+chkinm);
 		chkinmMapper.saveChkinm(chkinm);
		for (int j = 0; j < chkinm.getChkindList().size(); j++) {
			Chkind chkind = chkinm.getChkindList().get(j);//入库单从表
			chkind.setChkinno(chkinm.getChkinno());
			chkind.setInout(ChkinmConstants.conk);   //加入直发标志
			chkind.setPound(0);
			chkindMapper.saveChkind(chkind);
		}
		//审核
		chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月
		//  此字段当成月份使用     加入会计月 
		chkinm.setChecby(chkinm.getMadeby());
		chkinm.setStatus(chkinm.getInout());
		chkinmMapper.checkChkinm(chkinm);//审核入库单
		return chkinm.getPr();
	}
}
