package com.choice.scm.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.choice.framework.exception.CRUDException;

@Service
public class CommonService {
	
	private final transient Log log = LogFactory.getLog(CommonService.class);

	/**
	 * 将文件上传到temp文件夹下
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String realFilePath = "";
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file");
			String realFileName = file.getOriginalFilename();
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "temp\\";
			String fileuploadPath = ctxPath;
			File dirPath = new File(fileuploadPath);
			if (!dirPath.exists()) {
				dirPath.mkdir();
			}
			realFilePath = fileuploadPath + realFileName;
			File uploadFile = new File(realFilePath);
			FileCopyUtils.copy(file.getBytes(), uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}
	/**
	 * 对execl进行验证
	 */
	public List<String> check(String path) throws CRUDException {
		
		return listError(null);
	}
	/**
	 * 返回导入的错误结果信息
	 */
	public List<String> listError(Map<String, String> map) {
		List<String> listError = new ArrayList<String>();
		if (map.size() != 0) {
			for (String key : map.keySet()) {
				listError.add(key);
				listError.add(map.get(key));
			}
		}
		return listError;
	}
	/**
	 * 下载模板信息
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request,
			String fileName,String fileAddress) throws IOException {
		OutputStream outp = null;
		FileInputStream in = null;
		try {
		//	String fileName = "supply.xls";
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
				+ "\\" +fileAddress+"\\";
		//	+ "\\" + "template\\";
			String filedownload = ctxPath + fileName;
			fileName = URLEncoder.encode(fileName, "UTF-8");
			// 要下载的内容所在的绝对路径
			response.reset();
			response.addHeader("Content-Disposition", "attachment; filename="
					+ fileName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			in = new FileInputStream(filedownload);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}
}
