package com.choice.scm.service.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.reportMis.SupplyAcctMisMapper;
@Service
public class CkMingxiChaxunMisService {

	@Autowired
	private SupplyAcctMisMapper ckMingxiChaxunMisMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private PageManager<SupplyAcct> pageManager;
	
	/**
	 * 出库明细查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkoutDetailQuery(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
			List<Map<String,Object>> foot = ckMingxiChaxunMisMapper.findCalForChkout(conditions);
			if(null != conditions.get("bysale") && conditions.get("bysale").equals("true")){
				reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMisMapper.class.getName()+".findChkoutDQBySale"));
			}else{
				reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMisMapper.class.getName()+".findChkoutDetailQuery"));
			}
			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
