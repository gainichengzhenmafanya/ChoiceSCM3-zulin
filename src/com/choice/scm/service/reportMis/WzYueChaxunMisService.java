package com.choice.scm.service.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.reportMis.SupplyAcctMisMapper;
@Service
public class WzYueChaxunMisService {

	@Autowired
	private SupplyAcctMisMapper wzYueChaxunMisMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	/**
	 * 查询物资余额列表
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findSupplyBalance(Map<String,Object> content,Page pager) throws CRUDException{
		try {
			String startNumber="";//期初数量
			String startMoney="";//期初金额
			String inNumber="";//入库数量
			String inMoney="";//入库金额
			String outNumber="";//出库数量
			String outMoney="";//出库金额
			String inwhere = "";//查询条件
			int without0=0;//过滤零值标志：0-为选，1-已选
			String withoutzero="";//过滤零值条条件
			
			int month = Integer.parseInt(content.get("month").toString());
			if(content.get("without0")!=null && !content.get("without0").equals(""))
				without0=Integer.parseInt(content.get("without0").toString());
			if(without0==1){
				withoutzero = " AND (STARTNUMBER!=0 OR STARTMONEY!=0 OR INNUMBER!=0 OR INMONEY!=0 OR OUTNUMBER!=0 "+
				  "OR OUTMONEY!=0 OR (STARTNUMBER + INNUMBER - OUTNUMBER)!=0 OR (STARTMONEY + INMONEY - OUTMONEY)!=0)";
			}
			if(month>0){
				startNumber = "(";
				startMoney = "(";
				inwhere = "(";
				for(int i=0;i<month;i++){
					startNumber += "CAST(A.INC"+i+" AS NUMERIC(12, 2)) - CAST(A.OUTC"+i+" AS NUMERIC(12, 2)) +";
					startMoney += "CAST(A.INA"+i+" AS NUMERIC(12, 2)) - CAST(A.OUTA"+i+" AS NUMERIC(12, 2)) +";
				}
				inwhere = startNumber + "CAST(A.INC"+month+" AS NUMERIC(12, 2)) - CAST(A.OUTC"+month + " AS NUMERIC(12, 2))) > 0";
				startNumber = startNumber.substring(0,startNumber.length()-1);
				startNumber += ")";
				startMoney = startMoney.substring(0, startMoney.length()-1);
				startMoney += ")";
				inNumber = "CAST(A.INC"+month+" AS NUMERIC(10, 2))";
				inMoney = "CAST(A.INA"+month+" AS NUMERIC(12, 2))";
				if ("2".equals(content.get("bill").toString())) {
					outNumber = "CAST(A.OUTC"+month+" AS NUMERIC(10, 2))";
					outMoney = "CAST(A.OUTA"+month+" AS NUMERIC(12, 2))";
				}else{
					outNumber = "CAST(A.COST"+month+" AS NUMERIC(10, 2))";
					outMoney = "CAST(A.COSTA"+month+" AS NUMERIC(12, 2))";
				}
//				outNumber = "(CAST(A.COST"+month+" AS NUMERIC(10, 2)) + CAST(A.OUTC"+month+" AS NUMERIC(10, 2)))";
//				outMoney = "(CAST(A.COSTA"+month+" AS NUMERIC(12, 2)) + CAST(A.OUTA"+month+" AS NUMERIC(12, 2)))";
			}
			content.put("startNumber",startNumber);
			content.put("startMoney",startMoney);
			content.put("inNumber",inNumber);
			content.put("inMoney",inMoney);
			content.put("outNumber",outNumber);
			content.put("outMoney",outMoney);
			content.put("inwhere",inwhere);
			content.put("withoutzero",withoutzero);
			List<Map<String,Object>> foot = wzYueChaxunMisMapper.findCalForSupplyBalance(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctMisMapper.class.getName()+".findSupplyBalance");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
}
