package com.choice.scm.service.reportMis;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.reportMis.SupplyAcctMisMapper;
@Service
public class CkLeibieHuizongMisService {

	@Autowired
	private SupplyAcctMisMapper ckLeibieHuizongMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private PositnMapper positnMapper;
	/**
	 * 出库类别汇总
	 * @param conditions
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkoutCategorySum(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			List<Map<String,Object>> foot = ckLeibieHuizongMapper.findCalForChkoutCategorySum(conditions);
			reportObject.setRows(ckLeibieHuizongMapper.findChkoutCategorySum(conditions));
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	//导出出库类别汇总报表
	public boolean exportChkoutCategorySum(OutputStream os,Map<String,Object> conditions) throws Exception{
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		supplyAcct.setChktyp(CodeHelper.replaceCode(supplyAcct.getChktyp()));
		WritableWorkbook workBook = null;
		try{
			//记录表头位置
			Map<String,Integer> position = new HashMap<String,Integer>();
			reportObject = findChkoutCategorySum(conditions);
			List<Positn > headers = positnMapper.findAllPositn();
			List<SupplyAcct> rows = reportObject.getRows();
			List<Map<String,Object>> foot = reportObject.getFooter();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("出库类别汇总", 0);
			//定义label
			Label label = new Label(0,0,"出库类别汇总");
			//添加label到cell
            sheet.addCell(label);
            //设置表格表头
            Label headfirst = new Label(0,1,"项目");
            sheet.addCell(headfirst);
            Label headsecond = new Label(1,1,"合计");
            sheet.addCell(headsecond);
            for(int i = 2 ; i < headers.size()+2;i ++){
            	Label head = new Label(i,1,headers.get(i-2).getDes());
            	position.put(headers.get(i-2).getDes(), i);
            	sheet.addCell(head);
            }
            sheet.mergeCells(0, 0, headers.size()+1, 0);
            
            //获取表格内容的行数
            List<String> rowlist = new ArrayList<String>();
            for(SupplyAcct acct:rows){
            	if(!rowlist.contains(acct.getTypdes())){
            		rowlist.add(acct.getTypdes());
            	}
            }
            
            
            double sum = 0.0; //总合计
            for(int m=0; m<rowlist.size(); m++){
            	//遍历list填充表格内容
//            	double total = 0; //每行合计
        		Label project = new Label(0,m+2,rowlist.get(m));//项目
        		sheet.addCell(project);
        		
        		double totalrow = 0.0;
            		
        		//计算每一行的 总合计
        		for(int i=0;i<rows.size();i++){
        			String innerfirm = (String) rows.get(i).getTypdes();
        			if(innerfirm!=null && innerfirm.equals(rowlist.get(m))){
        				totalrow +=rows.get(i).getAmtout();
        				
        				//具体值
//                		total = rows.get(i).getAmtout();
                		String firm = rows.get(i).getFirmdes();
                		Label lab = new Label(position.get(firm),m+2,String.valueOf(rows.get(i).getAmtout()));
            			sheet.addCell(lab);
        			}
        		}
        		
        		DecimalFormat fnum = new DecimalFormat("##0.00");//四舍五入
    			Label totalLab = new Label(1,m+2,fnum.format(totalrow));//合计
    			sheet.addCell(totalLab);
    			sum +=totalrow;
            }
            
            //填充合计
            int j = rowlist.size()+2;
            Label project = new Label(0,j,"合计");
            Label sub = new Label(1,j,String.valueOf(sum));//总合计
    		sheet.addCell(project);
    		sheet.addCell(sub);
    		
    		for(int i = 0 ; i < foot.size() ; i ++ ){
    			Label lab = new Label(position.get(foot.get(i).get("FIRMDES")),j,String.valueOf(((BigDecimal)foot.get(i).get("AMT")).doubleValue()));
    			sheet.addCell(lab);
    		}
            workBook.write();
            os.flush();
		}catch (Exception e) {
			throw new CRUDException(e);
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
