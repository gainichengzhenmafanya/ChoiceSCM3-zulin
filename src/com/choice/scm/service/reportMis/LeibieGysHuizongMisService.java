package com.choice.scm.service.reportMis;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.reportMis.SupplyAcctMisMapper;
@Service
public class LeibieGysHuizongMisService {

	@Autowired
	private SupplyAcctMisMapper leibieGysHuizongMisMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;

		/**
		 * 类别供应商汇总
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findCategoryDeliverSum(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				//判断供应商
				if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
					sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
				}
				List<Map<String,Object>> foot = leibieGysHuizongMisMapper.findCalForCategoryDeliverSum(conditions);
				mapReportObject.setRows(leibieGysHuizongMisMapper.findCategoryDeliverSum(conditions));
				mapReportObject.setFooter(foot);
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
		
		/**
		 * 类别供应商汇总1
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findCategoryDeliverSum1(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				//判断供应商
				if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
					sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
				}
				List<Map<String,Object>> foot = leibieGysHuizongMisMapper.findCalForCategoryDeliverSum1(conditions);
				mapReportObject.setRows(leibieGysHuizongMisMapper.findCategoryDeliverSum1(conditions));
				mapReportObject.setFooter(foot);
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
}
