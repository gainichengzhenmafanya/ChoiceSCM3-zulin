package com.choice.scm.service.reportMis;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.reportMis.SupplyAcctMisMapper;
@Service
public class WzMingxiZhangMisService {

	@Autowired
	private SupplyAcctMisMapper wzMingxiZhangMisMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctMapper acctMapper;

	/**
	 * 物资明细账表
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	public ReportObject<Map<String,Object>> findSupplyDetailsInfo(Map<String,Object> conditions,Page pager) throws CRUDException{
		try {
			SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
			int withdat=0;//选择日期标志：0-为选，1-已选时间有效
//			int month = new Date().getMonth()+1;
//			int yearr = new Date().getYear()+1900;
			if(conditions.get("withdat")!=null && !conditions.get("withdat").equals(""))
				withdat=Integer.parseInt(conditions.get("withdat").toString());
			int yearr = 1900;
			if(withdat == 0){//没选日期默认当前年
				yearr = Integer.parseInt(mainInfoMapper.findYearrList().get(0));
			}else{//选了日期要查开始日期所在会计年
				yearr = Integer.parseInt(acctMapper.findYearrByDate(supplyAcct.getBdat()));
			}
			String dataColumns = "";//查询语句
			String constraints = " where a.YEARR = '"+yearr+"'";
			if(supplyAcct.getAcct()!=null && !supplyAcct.getAcct().equals(""))
				constraints += " and a.ACCT = '"+supplyAcct.getAcct()+"'";
			if(supplyAcct.getPositn()!=null && !supplyAcct.getPositn().equals(""))
				constraints += " and a.Positn IN ("+supplyAcct.getPositn()+")";
			if(supplyAcct.getSp_code()!=null && !supplyAcct.getSp_code().equals(""))
				constraints += " AND A.SP_CODE = '"+supplyAcct.getSp_code()+"'";
			String dataSource = "oracle";//判断是用的什么数据库wjf
			if(null == dataSource || "".equals(dataSource) || "oracle".equals(dataSource)){
				//查询条件
				if(withdat==1){
					String bdat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getBdat());
					String edat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getEdat());
					String positnsupply = "";//期初
					String totalSupply = "";//总计
					positnsupply = " union select cast(A.INC0 as numeric(12,2)) as inc,cast(A.INA0 as numeric(12,2)) as ina,cast(A.OUTC0 as numeric(12,2)) as outc,cast(A.OUTA0 as numeric(12,2)) as outa from positnsupply a "+ constraints;
					totalSupply = " union SELECT a.DAT,a.CHKNO,a.VOUNO,a.ID,a.DES,a.INC,a.PRICEIN,a.INA,a.OUTC,a.PRICEOUT,a.OUTA,B.CNTBAL,B.PRICEBAL,B.AMTBAL "+
							"FROM (select '' as dat,'' AS chkno,'' as vouno,'' as id,'累计' as des, cast(nvl(sum(a.cntin), 0) as numeric(12, 2)) as inc,"+
							"cast((case when sum(a.cntin) > 0 then nvl(sum(a.amtin), 0) / nvl(sum(a.cntin), 0) else 0 end) as numeric(12, 2)) as pricein,"+
							"cast(nvl(sum(a.amtin), 0) as numeric(12, 2)) as ina,cast(nvl(sum(a.cntout), 0) as numeric(12, 2)) as outc,"+
							"cast((case when sum(a.cntout) > 0 then nvl(sum(a.amtout), 0) / nvl(sum(a.cntout), 0) else 0 end) as numeric(12, 2)) as priceout,"+
							"cast(nvl(sum(a.amtout), 0) as numeric(12, 2)) as outa from supplyacct a " + constraints +
						" and a.dat>=to_date('"+bdat+"','yyyy-mm-dd') and a.dat<=to_date('"+edat+"','yyyy-mm-dd')) a,(select cast((a.cntbala+b.cntbalb) as numeric(12,2)) as cntbal,cast((a.amtbala+b.amtbalb) as numeric(12,2)) as amtbal,"+
							  "cast((case when a.cntbala+b.cntbalb>0 then (a.amtbala+b.amtbalb)/(a.cntbala+b.cntbalb) else 0 end) as numeric(12,2)) as pricebal "+
							"from (select cast(nvl(sum(a.cntin - a.cntout), 0) as numeric(12, 2)) as cntbala,cast(nvl(sum(a.amtin - a.amtout), 0) as numeric(12, 2)) as amtbala"+
								" from supplyacct a "+constraints+" and a.dat>=(select bdat1 from main where yearr = '"+yearr+"') and a.dat<=to_date('"+edat+"','yyyy-mm-dd')) a,"+
								"(select cast(A.INC0-A.OUTC0 as numeric(12, 2)) as cntbalb,cast(A.INA0-A.OUTA0 as numeric(12, 2)) as amtbalb"+
								" from positnsupply a "+constraints +") b) b order by dat,id";
					dataColumns = "select dat,chkno,vouno,id,des,cast(inc as numeric(12,2)) as inc,cast((case when inc>0 then ina/inc else 0 end) as numeric(12,2)) as pricein,cast(ina as numeric(12,2)) as ina,"+
					      " cast(outc as numeric(12,2)) as outc,cast((case when outc>0 then outa/outc else 0 end) as numeric(12,2)) as priceout,cast(outa as numeric(12,2)) as outa,"+
					      " cast(sum(cntbal) over(order by dat,id rows unbounded preceding) as numeric(12,2)) as cntbal,"+
					      " cast((case when sum(cntbal) over(order by dat,id rows unbounded preceding)>0 then "+
					      		" sum(amtbal) over(order by dat,id rows unbounded preceding)/sum(cntbal) over(order by dat,id rows unbounded preceding) else 0 end) as numeric(12,2)) as pricebal,"+
					      " cast(sum(amtbal) over(order by dat,id rows unbounded preceding) as numeric(12,2)) as amtbal "+
					"from(select ' ' as dat,'' as chkno,'' as vouno,'' as id,'期初' as des,0 as inc,0 as ina,0 as outc,0 as outa,cast(sum(inc-outc) as numeric(12,2)) as cntbal,cast(sum(ina-outa) as numeric(12,2)) as amtbal"+
					      " from (select nvl(sum(a.cntin),0) as inc,nvl(sum(a.amtin),0) as ina, nvl(sum(a.cntout),0) as outc, nvl(sum(a.amtout),0) as outa from supplyacct a "+constraints+
					        " and a.dat>=(select bdat1 from main where yearr = '"+yearr+"') and a.dat<to_date('"+bdat+"','yyyy-mm-dd') group by a.cntbal,a.amtbal "+positnsupply+") "+
					    " union select to_char(a.dat,'yyyy-mm-dd') as dat,to_char(a.chkno) as chkno,a.vouno,to_char(a.id) as id,a.des,a.cntin as inc,a.amtin as ina,a.cntout as outc,a.amtout as outa,(a.cntin-a.cntout) as cntbal,(a.amtin-a.amtout) as amtbal "+
					    " from supplyacct a "+constraints+
					      "  and a.dat>=to_date('"+bdat+"','yyyy-mm-dd') and a.dat<=to_date('"+edat+"','yyyy-mm-dd') order by dat,id )" + totalSupply;
				}else{
					//总计查询
					String totalSql = "";
//					int[] mon31 = {1,3,5,7,8,10,12};
//					int[] mon30 = {4,6,9,11};
//					String montht="";
//					String bdate,datet="";
//					totalSql = " select ' ' as dat,'' as chkno,'' as vouno,'' as id,'13' as num,'本年合计' as des, sum(inc) as inc,"+
//					     "cast((case when sum(inc)>0 then sum(ina)/sum(inc) else 0 end) as numeric(12,2)) as pricein,sum(ina) as ina,sum(outc) as outc,"+
//					    " cast((case when sum(outc)>0 then sum(outa)/sum(outc) else 0 end) as numeric(12,2)) as priceout,sum(outa) as outa,sum(cntbal) as cntbal,"+
//					    " cast((case when sum(cntbal)>0 then sum(amtbal)/sum(cntbal) else 0 end) as numeric(12,2)) as pricebal,sum(amtbal) as amtbal "+
//					    " from (select cast(sum(a.cntin) as numeric(12,2)) as inc,"+
//					         " cast((case when sum(a.cntin)>0 then sum(a.amtin)/sum(a.cntin) else 0 end) as numeric(12,2)) as pricein,cast(sum(a.amtin) as numeric(12,2)) as ina ,"+
//					         " cast(sum(a.cntout) as numeric(12,2)) as outc,cast((case when sum(a.cntout)>0 then sum(a.amtout)/sum(a.cntout) else 0 end) as numeric(12,2)) as priceout,"+
//					         " cast(sum(a.amtout) as numeric(12,2)) as outa,cast(sum(a.cntin-a.cntout) as numeric(12,2)) as cntbal,"+
//					         " cast((case when (sum(a.cntin-a.cntout))>0 then (sum(a.amtin-a.amtout))/(sum(a.cntin-a.cntout)) else 0 end) as numeric(12,2)) as pricebal," +
//					         " cast(sum(a.amtin-a.amtout) as numeric(12,2)) as amtbal from  supplyacct a "+ constraints +
//					   "  union select 0 as inc,0 as pricein,0 as ina,0 as outc,0 as priceout,0 as outa,a.inc0 as cntbal,(case when a.inc0>0 then a.ina0/a.inc0 else 0 end) as pricebal,a.ina0 as amtbal "+
//					         "  from positnsupply a "+ constraints +")";
					totalSql = " select ' ' as dat,'' as chkno,'' as vouno,'' as id,'13' as num,'本年合计' as des, "+
							"sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12) as inc,"+
							"case when sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12) > 0 then "+
							"sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12)/sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12) else 0 end as pricein,"+
							"sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12) as ina,"+
							"sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) as outc,"+
							"case when sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) > 0 then "+
							"sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12)/sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) else 0 end as priceout,"+
							"sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12) as outa,"+
							"sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12)-sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) as cntbal,"+
							"case when (sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12)-sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12)) > 0 then "+
							"(sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12)-sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12))/(sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12)-sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12)) else 0 end as pricebal,"+
							"sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12)-sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12) as amtbal"+
							"  from positnsupply a "+ constraints;
					//循环生成查询语句
					dataColumns += "select ' ' as dat,' ' as chkno,'' as vouno,'' as id,'00' as num,'上年转结' as des, cast(sum(a.inc0) as numeric(12,2)) as inc," +
							"cast((case when sum(a.inc0)>0 then sum(a.ina0)/sum(a.inc0) else 0 end) as numeric(12,2)) as pricein,cast(sum(a.ina0) as numeric(12,2)) as ina ,cast(sum(a.outc0) as numeric(12,2)) as outc," +
							"cast((case when sum(a.outc0)>0 then sum(a.outa0)/sum(a.outc0) else 0 end) as numeric(12,2)) as priceout,cast(sum(a.outa0) as numeric(12,2)) as outa,cast((sum(a.inc0)-sum(a.outc0)) as numeric(12,2)) as cntbal," +
							"cast((case when (sum(a.inc0)-sum(a.outc0))>0 then (sum(a.ina0)-sum(a.outa0))/(sum(a.inc0)-sum(a.outc0)) else 0 end) as numeric(12,2)) as pricebal,cast((sum(a.ina0)-sum(a.outa0)) as numeric(12,2)) as amtbal from  positnsupply a";
					dataColumns += constraints + " union ";
					for(int i=1;i<=12;i++){
						String mon = "",mon1="";
						if(i<10){ 
							mon = "0"+i; 
						}else {
							mon = ""+i;
						}
						if(i-1<10){
							mon1 = "0"+(i-1);
						}else{
							mon1 = ""+(i-1);
						}
//						bdate = yearr+montht+"-01";
//						if(Arrays.binarySearch(mon31, i)>=0)  datet = yearr + montht +"-31";
//						else if(Arrays.binarySearch(mon30, i)>=0)  datet = yearr +montht +"-30";
//						else if(i==2){
//							if((yearr%4==0 && yearr%100!=0) || yearr%400==0) datet = yearr + montht +"-29";
//							else	datet = yearr + montht +"-28";
//						}
						dataColumns += "select to_char(a.dat,'yyyy-mm-dd') as dat,to_char(a.chkno) as chkno,a.vouno,to_char(a.id) as id,'"+mon1+"' as num,a.deliverdes as des,cast(a.cntin as numeric(12,2)) as inc,cast(a.pricein as numeric(12,2)) as pricein," +
								"cast(a.amtin as numeric(12,2)) as ina,cast(a.cntout as numeric(12,2)) as outc,cast(a.priceout as numeric(12,2)) as priceout,cast(a.amtout as numeric(12,2)) as outa,"+
								"cast(sum(a.cntin-a.cntout) as numeric(12,2)) cntbal,"+
								"cast((case when sum(a.cntin-a.cntout) >0 then "+
								"sum(a.amtin-a.amtout) /sum(a.cntin-a.cntout) "+
								"else 0 end ) as numeric(12,2)) as pricebal,cast(sum(a.amtin-a.amtout) as numeric(12,2)) amtbal "+
								"from supplyacct a " + constraints + " and a.dat>=(select bdat"+i+" from main where yearr = '"+yearr+"') and (select edat"+i+" from main where yearr = '"+yearr+"')>=a.dat group by a.dat, a.chkno,a.vouno,a.deliverdes,a.cntin,a.amtin,a.cntout,a.amtout,a.pricein,a.priceout,a.id union ";
						dataColumns += "select ' ' as dat,'' as chkno,'' as vouno,'' as id,'"+mon+"' as num,'"+i+"月合计' as des, cast(sum(a.cntin) as numeric(12,2)) as inc," +
								"cast((case when sum(a.cntin)>0 then sum(a.amtin)/sum(a.cntin) else 0 end) as numeric(12,2)) as pricein,cast(sum(a.amtin) as numeric(12,2)) as ina ," +
								"cast(sum(a.cntout) as numeric(12,2)) as outc,cast((case when sum(a.cntout)>0 then sum(a.amtout)/sum(a.cntout) else 0 end) as numeric(12,2)) as priceout," +
								"cast(sum(a.amtout) as numeric(12,2)) as outa,0 as cntbal," +
								"0 as pricebal,0 as amtbal " +
								"from  supplyacct a" + constraints  + " and a.dat>=(select bdat"+i+" from main where yearr = '"+yearr+"') and (select edat"+i+" from main where yearr = '"+yearr+"')>=a.dat union ";
					}
					dataColumns +=  totalSql + " order by num,dat,id ";
				}
			}else if("sqlserver".equals(dataSource)){
				if(withdat==1){
					String bdat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getBdat());
					String edat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getEdat());
					String positnsupply = "";//期初
					String totalSupply = "";//总计
					positnsupply = " union select cast(A.INC0 as numeric(12,2)) as inc,cast(A.INA0 as numeric(12,2)) as ina,cast(A.OUTC0 as numeric(12,2)) as outc,cast(A.OUTA0 as numeric(12,2)) as outa from positnsupply a "+ constraints;
					totalSupply = " union SELECT a.DAT,a.CHKNO,a.VOUNO,a.ID,a.DES,a.INC,a.PRICEIN,a.INA,a.OUTC,a.PRICEOUT,a.OUTA,B.CNTBAL,B.PRICEBAL,B.AMTBAL "+
							"FROM (select '' as dat,'' AS chkno,'' as vouno,'' as id,'累计' as des, cast(ISNULL(sum(a.cntin), 0) as numeric(12, 2)) as inc,"+
							"cast((case when sum(a.cntin) > 0 then ISNULL(sum(a.amtin), 0) / ISNULL(sum(a.cntin), 0) else 0 end) as numeric(12, 2)) as pricein,"+
							"cast(ISNULL(sum(a.amtin), 0) as numeric(12, 2)) as ina,cast(ISNULL(sum(a.cntout), 0) as numeric(12, 2)) as outc,"+
							"cast((case when sum(a.cntout) > 0 then ISNULL(sum(a.amtout), 0) / ISNULL(sum(a.cntout), 0) else 0 end) as numeric(12, 2)) as priceout,"+
							"cast(ISNULL(sum(a.amtout), 0) as numeric(12, 2)) as outa from supplyacct a " + constraints +
						" and a.dat>=CAST('"+bdat+"' AS DATE) and a.dat<=CAST('"+edat+"' AS DATE)) a,(select cast((a.cntbala+b.cntbalb) as numeric(12,2)) as cntbal,cast((a.amtbala+b.amtbalb) as numeric(12,2)) as amtbal,"+
							  "cast((case when a.cntbala+b.cntbalb>0 then (a.amtbala+b.amtbalb)/(a.cntbala+b.cntbalb) else 0 end) as numeric(12,2)) as pricebal "+
							"from (select cast(ISNULL(sum(a.cntin - a.cntout), 0) as numeric(12, 2)) as cntbala,cast(ISNULL(sum(a.amtin - a.amtout), 0) as numeric(12, 2)) as amtbala"+
								" from supplyacct a "+constraints+" and a.dat>=(select bdat1 from main where yearr = '"+yearr+"') and a.dat<=CAST('"+edat+"' AS DATE)) a,"+
								"(select cast(A.INC0-A.OUTC0 as numeric(12, 2)) as cntbalb,cast(A.INA0-A.OUTA0 as numeric(12, 2)) as amtbalb"+
								" from positnsupply a "+constraints +") b) b ";
					dataColumns = "select dat,chkno,vouno,id,des,cast(inc as numeric(12,2)) as inc,cast((case when inc>0 then ina/inc else 0 end) as numeric(12,2)) as pricein,cast(ina as numeric(12,2)) as ina,"+
					      " cast(outc as numeric(12,2)) as outc,cast((case when outc>0 then outa/outc else 0 end) as numeric(12,2)) as priceout,cast(outa as numeric(12,2)) as outa,"+
					      " cast((cntbal)  as numeric(12,2)) as cntbal,"+
					      " cast((case when (cntbal) >0 then "+
					      " (amtbal) /(cntbal) else 0 end) as numeric(12,2)) as pricebal,"+
					      " cast((amtbal)  as numeric(12,2)) as amtbal "+
					"from(select ' ' as dat,'' as chkno,'' as vouno,'' as id,'期初' as des,0 as inc,0 as ina,0 as outc,0 as outa,cast(sum(inc-outc) as numeric(12,2)) as cntbal,cast(sum(ina-outa) as numeric(12,2)) as amtbal"+
					      " from (select ISNULL(sum(a.cntin),0) as inc,ISNULL(sum(a.amtin),0) as ina, ISNULL(sum(a.cntout),0) as outc, ISNULL(sum(a.amtout),0) as outa from supplyacct a "+constraints+
					        " and a.dat>=(select bdat1 from main where yearr = '"+yearr+"') and a.dat<CAST('"+bdat+"' AS DATE) group by a.cntbal,a.amtbal "+positnsupply+")AA "+
					    " union select CONVERT(varchar,a.dat, 23) as dat,CAST(a.chkno AS VARCHAR) as chkno,a.vouno,CAST(a.id AS VARCHAR) as id,a.des,a.cntin as inc,a.amtin as ina,a.cntout as outc,a.amtout as outa,(a.cntin-a.cntout) as cntbal,(a.amtin-a.amtout) as amtbal "+
					    " from supplyacct a "+constraints+
					      "  and a.dat>=CAST('"+bdat+"' AS DATE) and a.dat<=CAST('"+edat+"' AS DATE) )TT" + totalSupply;
				}else{
					//总计查询
					String totalSql = "";
//					int[] mon31 = {1,3,5,7,8,10,12};
//					int[] mon30 = {4,6,9,11};
//					String montht="";
//					String bdate,datet="";
//					totalSql = " select ' ' as dat,'' as chkno,'' as vouno,'' as id,'13' as num,'本年合计' as des, sum(inc) as inc,"+
//					     "cast((case when sum(inc)>0 then sum(ina)/sum(inc) else 0 end) as numeric(12,2)) as pricein,sum(ina) as ina,sum(outc) as outc,"+
//					    " cast((case when sum(outc)>0 then sum(outa)/sum(outc) else 0 end) as numeric(12,2)) as priceout,sum(outa) as outa,sum(cntbal) as cntbal,"+
//					    " cast((case when sum(cntbal)>0 then sum(amtbal)/sum(cntbal) else 0 end) as numeric(12,2)) as pricebal,sum(amtbal) as amtbal "+
//					    " from (select cast(sum(a.cntin) as numeric(12,2)) as inc,"+
//					         " cast((case when sum(a.cntin)>0 then sum(a.amtin)/sum(a.cntin) else 0 end) as numeric(12,2)) as pricein,cast(sum(a.amtin) as numeric(12,2)) as ina ,"+
//					         " cast(sum(a.cntout) as numeric(12,2)) as outc,cast((case when sum(a.cntout)>0 then sum(a.amtout)/sum(a.cntout) else 0 end) as numeric(12,2)) as priceout,"+
//					         " cast(sum(a.amtout) as numeric(12,2)) as outa,cast(sum(a.cntin-a.cntout) as numeric(12,2)) as cntbal,"+
//					         " cast((case when (sum(a.cntin-a.cntout))>0 then (sum(a.amtin-a.amtout))/(sum(a.cntin-a.cntout)) else 0 end) as numeric(12,2)) as pricebal," +
//					         " cast(sum(a.amtin-a.amtout) as numeric(12,2)) as amtbal from  supplyacct a "+ constraints +
//					   "  union select 0 as inc,0 as pricein,0 as ina,0 as outc,0 as priceout,0 as outa,a.inc0 as cntbal,(case when a.inc0>0 then a.ina0/a.inc0 else 0 end) as pricebal,a.ina0 as amtbal "+
//					         "  from positnsupply a "+ constraints +")T";
					totalSql = " select ' ' as dat,'' as chkno,'' as vouno,'' as id,'13' as num,'本年合计' as des, "+
							"sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12) as inc,"+
							"case when sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12) > 0 then "+
							"sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12)/sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12) else 0 end as pricein,"+
							"sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12) as ina,"+
							"sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) as outc,"+
							"case when sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) > 0 then "+
							"sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12)/sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) else 0 end as priceout,"+
							"sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12) as outa,"+
							"sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12)-sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12) as cntbal,"+
							"case when (sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12)-sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12)) > 0 then "+
							"(sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12)-sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12))/(sum(inc0 + inc1 + inc2 + inc3 + inc4 + inc5 + inc6 + inc7 + inc8 + inc9 + inc10 + inc11 + inc12)-sum(outc0 + outc1 + outc2 + outc3 + outc4 + outc5 + outc6 + outc7 + outc8 + outc9 + outc10 + outc11 + outc12)) else 0 end as pricebal,"+
							"sum(ina0 + ina1 + ina2 + ina3 + ina4 + ina5 + ina6 + ina7 + ina8 + ina9 + ina10 + ina11 + ina12)-sum(outa0 + outa1 + outa2 + outa3 + outa4 + outa5 + outa6 + outa7 + outa8 + outa9 + outa10 + outa11 + outa12) as amtbal"+
							"  from positnsupply a "+ constraints;
					//循环生成查询语句
					dataColumns += "select ' ' as dat,' ' as chkno,'' as vouno,'' as id,'00' as num,'上年转结' as des, cast(a.inc0 as numeric(12,2)) as inc," +
							"cast((case when a.inc0>0 then a.ina0/a.inc0 else 0 end) as numeric(12,2)) as pricein,cast(a.ina0 as numeric(12,2)) as ina ,cast(a.outc0 as numeric(12,2)) as outc," +
							"cast((case when a.outc0>0 then a.outa0/a.outc0 else 0 end) as numeric(12,2)) as priceout,cast(a.outa0 as numeric(12,2)) as outa,cast((a.inc0-a.outc0) as numeric(12,2)) as cntbal," +
							"cast((case when (a.inc0-a.outc0)>0 then (a.ina0-a.outa0)/(a.inc0-a.outc0) else 0 end) as numeric(12,2)) as pricebal,cast((a.ina0-a.outa0) as numeric(12,2)) as amtbal from  positnsupply a";
					dataColumns += constraints + " union ";
					for(int i=1;i<=12;i++){
						String mon = "",mon1="";
						if(i<10){ 
							mon = "0"+i; 
						}else {
							mon = ""+i;
						}
						if(i-1<10){
							mon1 = "0"+(i-1);
						}else{
							mon1 = ""+(i-1);
						}
//						bdate = yearr+montht+"-01";
//						if(Arrays.binarySearch(mon31, i)>=0)  datet = yearr + montht +"-31";
//						else if(Arrays.binarySearch(mon30, i)>=0)  datet = yearr +montht +"-30";
//						else if(i==2){
//							if((yearr%4==0 && yearr%100!=0) || yearr%400==0) datet = yearr + montht +"-29";
//							else	datet = yearr + montht +"-28";
//						}
						dataColumns += "select CONVERT(varchar,a.dat, 23) as dat,cast(a.chkno as varchar) as chkno,a.vouno,cast(a.id as varchar) as id,'"+mon1+"' as num,a.deliverdes as des,cast(a.cntin as numeric(12,2)) as inc,cast(a.pricein as numeric(12,2)) as pricein," +
								"cast(a.amtin as numeric(12,2)) as ina,cast(a.cntout as numeric(12,2)) as outc,cast(a.priceout as numeric(12,2)) as priceout,cast(a.amtout as numeric(12,2)) as outa,"+
								"cast(sum(a.cntin-a.cntout)  as numeric(12,2)) cntbal,"+
								"cast((case when sum(a.cntin-a.cntout)  >0 then "+
								"sum(a.amtin-a.amtout)  /sum(a.cntin-a.cntout)  "+
								"else 0 end ) as numeric(12,2)) as pricebal,cast(sum(a.amtin-a.amtout)  as numeric(12,2)) amtbal "+
								"from supplyacct a " + constraints + " and a.dat>=(select bdat"+i+" from main where yearr = '"+yearr+"') and (select edat"+i+" from main where yearr = '"+yearr+"')>=a.dat group by a.dat, a.chkno,a.vouno,a.deliverdes,a.cntin,a.amtin,a.cntout,a.amtout,a.pricein,a.priceout,a.id union ";
						dataColumns += "select ' ' as dat,'' as chkno,'' as vouno,'' as id,'"+mon+"' as num,'"+i+"月合计' as des, cast(sum(a.cntin) as numeric(12,2)) as inc," +
								"cast((case when sum(a.cntin)>0 then sum(a.amtin)/sum(a.cntin) else 0 end) as numeric(12,2)) as pricein,cast(sum(a.amtin) as numeric(12,2)) as ina ," +
								"cast(sum(a.cntout) as numeric(12,2)) as outc,cast((case when sum(a.cntout)>0 then sum(a.amtout)/sum(a.cntout) else 0 end) as numeric(12,2)) as priceout," +
								"cast(sum(a.amtout) as numeric(12,2)) as outa,0 as cntbal," +
								"0 as pricebal,0 as amtbal " +
								"from  supplyacct a" + constraints  + " and a.dat>=(select bdat"+i+" from main where yearr = '"+yearr+"') and (select edat"+i+" from main where yearr = '"+yearr+"')>=a.dat union ";
					}
					dataColumns +=  totalSql + " order by num,dat,id ";
				}
			}
			conditions.put("dataColumns", dataColumns.toUpperCase());
//			List<Map<String,Object>> foot = wzMingxiZhangMisMapper.findCalForSupplyDetailsInfo(conditions);
//			List<Map<String,Object>> listInfo = mapPageManager.selectPage(conditions, pager, WzMingxiZhangMisMapper.class.getName()+".findSupplyDetailsInfo");
			List<Map<String, Object>> list = wzMingxiZhangMisMapper.findSupplyDetailsInfo(conditions);
			if(withdat!=1){
				double cntbal = 0;
				double amtbal = 0;
				for(Map<String,Object> map:list){
					if(map == list.get(list.size()-1)){break;}
					map.put("CNTBAL", Double.parseDouble(map.get("CNTBAL")==null?"0":map.get("CNTBAL").toString())+cntbal);
					map.put("AMTBAL", Double.parseDouble(map.get("AMTBAL")==null?"0":map.get("AMTBAL").toString())+amtbal);
					map.put("PRICEBAL", Double.parseDouble(map.get("CNTBAL")==null?"0":map.get("CNTBAL").toString()) == 0 ? 0 : Double.parseDouble(map.get("AMTBAL")==null?"0":map.get("AMTBAL").toString())/Double.parseDouble(map.get("CNTBAL")==null?"0":map.get("CNTBAL").toString()));
					cntbal = Double.parseDouble(map.get("CNTBAL")==null?"0":map.get("CNTBAL").toString());
					amtbal = Double.parseDouble(map.get("AMTBAL")==null?"0":map.get("AMTBAL").toString());
				}
			}
			mapReportObject.setRows(list);
//			mapReportObject.setFooter(foot);
//			mapReportObject.setRows(listInfo);
//			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

}
