package com.choice.scm.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.SppriceDemoMapper;
import com.choice.scm.persistence.SppriceQuickMapper;

@Service
public class SppriceQuickService {
	
	@Autowired
	private SppriceQuickMapper sppriceQuickMapper; 
	@Autowired
	private SppriceDemoMapper sppriceDemoMapper;
	/**
	 * 根据供货商ID 查询报价
	 * @param deliver
	 * @return
	 */
	public List<Spprice> findSppriceByDeliverCode(Deliver deliver){
		return sppriceQuickMapper.findSppriceByDeliverCode(deliver);
	}
	
	/**
	 * 查询 物资编码信息
	 */
	public List<Supply> findSupplyById(Supply supply) throws CRUDException {
		try {
			List<String> idList = Arrays.asList(supply.getSp_code().split(","));
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ids", idList);
			params.put("acct", supply.getAcct());
			return sppriceQuickMapper.findSupplyById(params);
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加报价
	 * @param session
	 * @param spprices
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String saveSpprice(HttpSession session,Spprice spprices){
		StringBuffer result = new StringBuffer();
		try {
			for (Spprice spprice : spprices.getSppriceList()) {
				for (Positn positn : spprice.getPositnList()) {
					Map map = new HashMap();
					map.put("deliver", spprice.getDeliver());
					map.put("area", positn.getArea());
					map.put("sp_code",spprice.getSp_code());
					map.put("price", spprice.getPrice());
					map.put("bdat", spprice.getBdat());
					map.put("edat", spprice.getEdat());
					if(findSppriceByDeliverPositnSupplyPriceDate(map) != 0){
						result.append(spprice.getSp_code()+"物资在"+positn.getArea()+"门店中当前价格，当前时间已添加报价；已过滤。\n");
						continue;
					}
					spprice.setAcct(session.getAttribute("ChoiceAcct").toString());
					spprice.setMadet(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd hh:mm:ss").toString());
					spprice.setSta("Y");
					spprice.setChecby(session.getAttribute("accountName").toString());
					spprice.setEmp(session.getAttribute("accountName").toString());
					spprice.setChect(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
					spprice.setArea(positn.getArea());
					spprice.setPriceold(spprice.getPriceold()==null?new BigDecimal(0):spprice.getPriceold());
					spprice.setPrice(spprice.getPrice()==null?new BigDecimal(0):spprice.getPrice());
					sppriceQuickMapper.saveSpprice(spprice);
				}
			}
			return "执行完成！\n"+result.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	/*****************************************************************************
	 * 添加报价
	 */
	public String saveSppriceDemo(HttpSession session,SppriceDemo sppriceDemos) throws CRUDException{
		try {
			for (SppriceDemo sppriceDemo : sppriceDemos.getSuppriceDemoList()) {
				for (Positn positn : sppriceDemo.getPositnList()) {
					sppriceDemo.setAcct(session.getAttribute("ChoiceAcct").toString());
					sppriceDemo.setFirm(positn.getCode());
					sppriceDemo.setCzdate((new SimpleDateFormat("yyyy-MM-dd  kk:mm:ss ")).format(new Date()));
					//改为 如果有则修改，没有才新增 wjf
					SppriceDemo sppriceD = sppriceDemoMapper.findSppriceDemoById(sppriceDemo);
					if(sppriceD == null){
						sppriceQuickMapper.saveSppriceDemo(sppriceDemo);
					}else{
						sppriceDemoMapper.updateSppriceDemo(sppriceDemo);
					}
				}
			}
			return "success";
		} catch (Exception e) {
			return "error";
		}
	}
	
	/**
	 * 根据条件：供应商，仓位，时间，物资，价格条件查询报价 ，校验 wjf
	 * @param map
	 * @return
	 */
	public Integer findSppriceByDeliverPositnSupplyPriceDate(Map map){
		return sppriceQuickMapper.findSppriceByDeliverPositnSupplyPriceDate(map);
	}
}
