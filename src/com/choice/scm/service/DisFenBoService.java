package com.choice.scm.service;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.assistant.persistence.bill.PuprOrderMapper;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.OracleUtil;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.ChkstodMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.DisFenBoMapper;
import com.choice.scm.persistence.DisMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.util.CalChkNum;
@Service
public class DisFenBoService {

	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private DisFenBoMapper disFenBoMapper;
	@Autowired
	private PageManager<Dis> pageManager;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private DisMapper disMapper;
	@Autowired
	private ChkstodMapper chkstodMapper;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private CoderuleService coderuleService;
	@Autowired
	private PuprOrderMapper puprorderMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	
	
	private final static int MAXNUM = 30000;
	private final transient Log log = LogFactory.getLog(DisFenBoService.class);
	
	@Autowired
	private CalChkNum calChkNum;
//	@Autowired
//	private PageManager<Map<String,Object>> pageManager1;
	/**
	 * 报货分拨
	 */
	public List<Dis> findAllDis(Dis dis,Page page) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=inout && !"".equals(inout) && "in".equals(inout)){
				dis.setInout(inout);
			}else{
				if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			}
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=pageManager.selectPage(dis, page, DisFenBoMapper.class.getName()+".findAllDis"); 			
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨分页
	 */
	public List<Dis> findAllDisPage(Dis dis) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			
			//mysql传值每页多少条记录
			int start = dis.getStartNum();
			int end = dis.getEndNum();
			dis.setPageSize(end-start);
			
			list=disFenBoMapper.findAllDisPage(dis); 			
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 采购订单
	 */
	public List<Dis> findAllDisPage_cg(Dis dis) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			
			//mysql传值每页多少条记录
			int start = dis.getStartNum();
			int end = dis.getEndNum();
			dis.setPageSize(end-start);
			
			list=disFenBoMapper.findAllDisPage_cg(dis); 			
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨分页 (导出分拨单用)
	 */
	public List<Dis> findAllDisPageFenbo(Dis dis) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			
			//mysql传值每页多少条记录
			int start = dis.getStartNum();
			int end = dis.getEndNum();
			dis.setPageSize(end-start);
			
			list=disFenBoMapper.findAllDisPageFenbo(dis); 			
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨查询总数
	 */
	public int findAllDisCount(Dis dis) throws CRUDException {
		try {
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			int count=disFenBoMapper.findAllDisCount(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return count; 	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨，打印用，不带分页
	 */
	public List<Dis> findAllDisForReport(Dis dis) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}  //分店编码
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}  //缩写码
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=disFenBoMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量修改       验收入库     验收出库    采购确认  采购审核   等成功后的标志字段
	 */
	public void updateByIds(Dis dis,String accountName) throws CRUDException {
		try {
			if(null!=dis.getId() && !"".equals(dis.getId())){
				dis.setId(CodeHelper.replaceCode(dis.getId()));
			}
			if(null!=dis.getChk1() && !"".equals(dis.getChk1())){
				dis.setChk1emp(accountName);//采购确认人
				dis.setChk1tim(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));//采购确认时间
			}
			if(null!=dis.getSta() && !"".equals(dis.getSta())){
				dis.setChk2emp(accountName);//采购审核人
				dis.setChk2tim(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));//采购审核时间
			}
			if (null != dis.getIspuprorder() && !"".equals(dis.getIspuprorder())) {//生成采购订单
				dis.setPuprorderemp(accountName);
				dis.setPuprordertim(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			}
			dis.setInCondition(OracleUtil.getOraInSql("id", dis.getId()));
			disFenBoMapper.updateByIds(dis);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改
	 */
	public String updateDis(DisList disList) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			for (int i = 0; i < disList.getListDis().size(); i++) {
				Chkstod c = disFenBoMapper.findChkByKey(disList.getListDis().get(i).getId());
				String chkin = c.getChkin();
				String chkout = c.getChkout();
				String chkyh = c.getChkyh();
				if("Y".equals(chkin) || "Y".equals(chkout) || "Y".equals(chkyh)){
					result.put("pr", "fail");
				}else{
					Dis dis=new Dis();
					dis.setId(disList.getListDis().get(i).getId());
					dis.setDeliverCode(disList.getListDis().get(i).getDeliverCode());
					dis.setAmount1sto(disList.getListDis().get(i).getAmount1sto());//采购数量
					dis.setAmountin(disList.getListDis().get(i).getAmountin());
					dis.setAmount1in(disList.getListDis().get(i).getAmount1in());
					dis.setPricein(disList.getListDis().get(i).getPricein());
					dis.setPricesale(disList.getListDis().get(i).getPricesale());
					dis.setInd(disList.getListDis().get(i).getInd());
					dis.setDued(disList.getListDis().get(i).getDued());//生产日期
					dis.setPcno(disList.getListDis().get(i).getPcno());//批次号
					dis.setBak2(disList.getListDis().get(i).getBak2());
					disFenBoMapper.updateDis(dis);
					if(null!=dis.getDeliverCode() && !"".equals(dis.getDeliverCode())){
						disFenBoMapper.deleteChkstodDeliver(dis);//修改供应商之后要执行的操作
						disFenBoMapper.saveChkstodDeliver(dis);
					}
					if(null!=dis.getInd()){
						disFenBoMapper.deleteChkstodInd(dis);//修改到货日期之后要执行的操作
						disFenBoMapper.saveChkstodInd(dis);
					}
					result.put("pr", "succ");
				}
			}
			result.put("updateNum", disList.getListDis().size());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			result.put("pr", "fail");
			throw new CRUDException(e);
		}
	}

	/**
	 * 报货分拨，采购汇总
	 */
	public List<Dis> findCaiGouTotal(Dis dis,Page page) throws CRUDException {
		try {
//			List<Dis> list=null;
//			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=inout && !"".equals(inout) && "in".equals(inout)){
				dis.setInout(inout);
			}else{
				if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			}
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			
			if (page==null) {
				return disFenBoMapper.findCaiGouTotal(dis);
			}else {
				return pageManager.selectPage(dis, page, DisFenBoMapper.class.getName()+".findCaiGouTotal");
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 报货分拨，配送汇总
	 */
	public List<Dis> findPeiSongTotal(Dis dis,Page page) throws CRUDException {
		try {
			return pageManager.selectPage(dis, page, DisFenBoMapper.class.getName()+".findPeiSongTotal");
			//return disFenBoMapper.findTotal();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 报货分拨，部门明细
	 */
	public List<Dis> findDeptDetail(Map<String,Object> map,Page page) throws CRUDException {
		try {
			List<String> deptList = findTableHead(map.get("firmCode").toString());
			StringBuffer sqlStr = new StringBuffer();
			if(findDateisNull(map.get("firmCode").toString(),map.get("deliverCode").toString())!=0){
				for(int i=0;i<deptList.size();i++){
					sqlStr.append(deptList.get(i)+",");
				}
				String sqlStrs= (CodeHelper.replaceCode(sqlStr.substring(0, sqlStr.lastIndexOf(",")).toString())).toString();
				map.put("sqlStrs", sqlStrs);
				StringBuffer  sqlBuffer = new StringBuffer();
				for(String deptdes:deptList){
					sqlBuffer.append("sum(decode(D.deptdes,'"+deptdes+"',D.count,null))as F_"+deptdes+",");		
				}
				map.put("sqlBuffer", sqlBuffer.substring(0, sqlBuffer.lastIndexOf(",")));
				return pageManager.selectPage(map, page, DisFenBoMapper.class.getName()+".findDeptDetail");
			}else{
				return null;
				}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<Map<String,Object>> findDeptDetail(Map<String,Object> map) throws CRUDException {
		try {
			List<String> deptList = findTableHead(map.get("firmCode").toString());
			StringBuffer sqlStr = new StringBuffer();
			if(findDateisNull(map.get("firmCode").toString(),map.get("deliverCode").toString())!=0){
				for(int i=0;i<deptList.size();i++){
					sqlStr.append(deptList.get(i)+",");
				}
				String sqlStrs= (CodeHelper.replaceCode(sqlStr.substring(0, sqlStr.lastIndexOf(",")).toString())).toString();
				map.put("sqlStrs", sqlStrs);
				StringBuffer  sqlBuffer = new StringBuffer();
				for(String deptdes:deptList){
					sqlBuffer.append("sum(decode(D.deptdes,'"+deptdes+"',D.count,null))as F_"+deptdes+",");		
				}
				map.put("sqlBuffer", sqlBuffer.substring(0, sqlBuffer.lastIndexOf(",")));
				return disFenBoMapper.findDeptDetail(map);
			}else{
				return null;
				}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 导出
	 */
	public boolean exportExcel(OutputStream os,List<Map<String,Object>> list, String firmCode) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			List<String> deptList = findTableHead(firmCode);
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				sheet.addCell(new Label(0, 0,"部门明细"));
				sheet.addCell(new Label(0, 2, "物资编码")); 
	            sheet.addCell(new Label(1, 2, "物资名称"));   
	            sheet.addCell(new Label(2, 2, "物资单位")); 
				sheet.addCell(new Label(3, 2, "物资规格"));  
				sheet.addCell(new Label(4, 2, "价格"));
				for (int k=0;k<deptList.size();k++) {
					sheet.addCell(new Label(k+5, 2, deptList.get(k)));
				}
				
	            //遍历list填充表格内容
				int index = 0;
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	sheet.addCell(new Label(0, index+3, list.get(j).get("SP_CODE").toString()));
	            	sheet.addCell(new Label(1, index+3, list.get(j).get("SP_NAME").toString()));
	            	sheet.addCell(new Label(2, index+3, list.get(j).get("UNIT").toString()));
	            	String  sp_desc = "";   
	            	if(list.get(j).get("SP_DESC")!=null){
	            		sp_desc = list.get(j).get("SP_DESC").toString();
	            	}
	            	sheet.addCell(new Label(3, index+3,sp_desc ));
	            	sheet.addCell(new Label(4, index+3, list.get(j).get("PRICE").toString()));
	            	for (int k=0;k<deptList.size();k++) {
	            		String name="";
	            		if (list.get(j).get("F_"+deptList.get(k))!=null) {
	            			name = list.get(j).get("F_"+deptList.get(k)).toString();
	            		}
						sheet.addCell(new Label(k+5, index+3, name));
					}
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 导出 部门明细2
	 */
	public boolean exportDeptDetail2(OutputStream os,List<Dis> list) throws CRUDException {   
		WritableWorkbook workBook = null;
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				sheet.addCell(new Label(0, 0,"部门明细"));
				sheet.addCell(new Label(0, 2, "单号"));
				sheet.addCell(new Label(1, 2, "序列号"));
				sheet.addCell(new Label(2, 2, "供应商名称"));
				sheet.addCell(new Label(3, 2, "分店(档口)")); 
	            sheet.addCell(new Label(4, 2, "物资名称"));   
	            sheet.addCell(new Label(5, 2, "物资单位")); 
				sheet.addCell(new Label(6, 2, "物资规格"));
				sheet.addCell(new Label(7, 2, "采购单位"));
				sheet.addCell(new Label(8, 2, "数量")); 
				sheet.addCell(new Label(9, 2, "备注"));
	            //遍历list填充表格内容
				int index = 0;
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
	            	sheet.addCell(new Label(0, index+3, list.get(j).getChkstoNo().toString()));
	            	sheet.addCell(new Label(1, index+3, list.get(j).getId().toString()));
	            	sheet.addCell(new Label(2, index+3, list.get(j).getDeliverDes()));
	            	sheet.addCell(new Label(3, index+3, list.get(j).getFirmDes()));
	            	sheet.addCell(new Label(4, index+3, list.get(j).getSp_code()));
	            	sheet.addCell(new Label(5, index+3, list.get(j).getSp_name()));
	            	sheet.addCell(new Label(6, index+3, list.get(j).getSp_desc()));
	            	sheet.addCell(new Label(7, index+3, list.get(j).getUnit3()));
	            	sheet.addCell(new Label(8, index+3, list.get(j).getAmount1().toString()));
	            	sheet.addCell(new Label(9, index+3, list.get(j).getMemo()));
	            	for (Chkstod chkstod : list.get(j).getDeptList()) {
	            		index ++;
		            	sheet.addCell(new Label(3, index+3, chkstod.getPosition().getDes()));
		            	sheet.addCell(new Label(8, index+3, chkstod.getAmount().toString()));
		            	sheet.addCell(new Label(9, index+3, chkstod.getMemo()));
					}
	            	index++;
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 导出 部门明细2
	 */
	public boolean exportDeptExcel(OutputStream os, List<Dis> list, Dis dis) throws CRUDException {   
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 12,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", dis.getFirmCode());
			map.put("positnType", "1203");
			map.put("ynUseDept", "Y");
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				titleStyle.setAlignment(Alignment.CENTRE);
				titleStyle.setBorder(Border.ALL, BorderLineStyle.THIN,
					     jxl.format.Colour.BLACK);
				titleStyle2.setAlignment(Alignment.CENTRE);
				titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,
					     jxl.format.Colour.BLACK);
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("档口分拨表"+(i+1), i);
				sheet.addCell(new Label(0, 0,"档口分拨表",titleStyle));
				sheet.mergeCells(0, 0, 21, 0);
				
				sheet.addCell(new Label(0, 1, "物资编码",titleStyle2));
				sheet.mergeCells(0, 1, 0, 2);
				sheet.addCell(new Label(1, 1, "物资名称",titleStyle2));
				sheet.mergeCells(1, 1, 1, 2);
				sheet.addCell(new Label(2, 1, "规格",titleStyle2));
				sheet.mergeCells(2, 1, 2, 2);
				sheet.addCell(new Label(3, 1, "采购单位",titleStyle2));
				sheet.mergeCells(3, 1, 3, 2);
				
				HashMap<String,Integer> weizhi = new HashMap<String,Integer>();
				int colspan = 0;//下一个分店在哪
//				int zj = 0;//总计在哪
				for (int k=0;k<firmList.size();k++) {
					Positn firm = firmList.get(k);
					firm.setUcode(firm.getCode());
					List<Positn> deptList = positnMapper.findDeptByPositn(firm);//门店下的档口
					sheet.addCell(new Label(k+4+colspan, 1, firmList.get(k).getDes(),titleStyle2));
					sheet.mergeCells(k+4+colspan, 1, k+4+colspan+deptList.size(), 1);
					for(int j = 0;j<deptList.size();j++){//中类
						sheet.addCell(new Label(k+j+4+colspan, 2, deptList.get(j).getDes(),titleStyle2));
						weizhi.put(deptList.get(j).getCode(), k+j+4+colspan);
//						zj += 1;
						if(j == deptList.size()-1){//如果是最后一个
							sheet.addCell(new Label(k+j+4+colspan+1, 2, "合计",titleStyle2));//最后有个合计
							weizhi.put(firmList.get(k).getCode(), k+j+4+colspan+1);
						}
					}
//					zj += 1;
					colspan += deptList.size();
				}
//				sheet.addCell(new Label(zj+2, 1, "总计",titleStyle2));//总计
//				sheet.mergeCells(zj+2, 1, zj+2, 2);
	            //遍历list填充表格内容
				HashMap<String,Integer> mouhang = new HashMap<String,Integer>();
				List<Integer> delhang = new ArrayList<Integer>();
				for(int k = 0;k<list.size();k++){
					Dis di = list.get(k);
					Integer hang = null;
					if(null != mouhang.get(di.getSp_code())){
						hang = mouhang.get(di.getSp_code());
						delhang.add(k+3);
					}else{
						hang = k + 3;
						mouhang.put(di.getSp_code(), k+3);
					}
					sheet.addCell(new Label(0, hang, di.getSp_code(),titleStyle2));
					sheet.addCell(new Label(1, hang, di.getSp_name(),titleStyle2));
					sheet.addCell(new Label(2, hang, di.getSp_desc(),titleStyle2));
					sheet.addCell(new Label(3, hang, di.getUnit3(),titleStyle2));
					
					String firmCode = di.getFirmCode();
					Integer lie = weizhi.get(firmCode);
					sheet.addCell(new Label(lie, hang, di.getAmount1().toString(),titleStyle2));//总计
					for(int j = 0; j < di.getDeptList().size(); j++){
						Chkstod cd = di.getDeptList().get(j);
						String deptCode = cd.getPosition().getCode();
						Integer lie2 = weizhi.get(deptCode);
						sheet.addCell(new Label(lie2, hang, cd.getAmount().toString(),titleStyle2));
					}
				}
				//删除空行
				for(int k = delhang.size()-1; k >= 0; k--){
					sheet.removeRow(delhang.get(k));
				}
				int row = sheet.getRows()+1;
				sheet.addCell(new Label(0, row, "验收:",titleStyle2));
				sheet.addCell(new Label(2, row, "发货:",titleStyle2));
				sheet.addCell(new Label(4, row, "配送:",titleStyle2));
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 导出/gzjj
	 */
	public boolean exportFenboExcel(OutputStream os, List<Dis> list, String firmCode) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", CodeHelper.replaceCode(firmCode));
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				for (int k=0;k<firmList.size();k++) {
					sheet.addCell(new Label(2*k+5, 1, firmList.get(k).getDes()));
					sheet.addCell(new Label(2*k+5, 2, "报单"));
					sheet.addCell(new Label(2*k+6, 2, "实发"));
				}
				
	            //遍历list填充表格内容
				sheet.addCell(new Label(0, 2, "物资编码"));
				sheet.addCell(new Label(1, 2, "物资名称"));
				sheet.addCell(new Label(2, 2, "物资规格"));
				sheet.addCell(new Label(3, 2, "单位"));
				sheet.addCell(new Label(4, 2, "货架"));
				int index = 0;
				String first = list.get(0).getSp_name();
				String sp_code = list.get(0).getSp_code();//物资编码
				String sp_desc = list.get(0).getSp_desc();//物资规格
				String unit = list.get(0).getUnit();
				String positn1 = list.get(0).getPositn1();//货架
				sheet.addCell(new Label(0, index+3, sp_code));
				sheet.addCell(new Label(1, index+3, first));
				sheet.addCell(new Label(2, index+3, sp_desc));
				sheet.addCell(new Label(3, index+3, unit));
				sheet.addCell(new Label(4, index+3, positn1));
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
//	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	String spName = list.get(j).getSp_name();
	            	String spCode = list.get(j).getSp_code();
	            	if(!spCode.equals(sp_code)){//这里用编码来判断更好  因为名字会有重复的 wjf
	            		index+=1;
	            		first=spName;
	            		sp_code = spCode;
	            		sheet.addCell(new Label(0, index+3, spCode));
	            		sheet.addCell(new Label(1, index+3, spName));
	            		sheet.addCell(new Label(2, index+3, list.get(j).getSp_desc()));
	            		sheet.addCell(new Label(3, index+3, list.get(j).getUnit()));
	            		sheet.addCell(new Label(4, index+3, list.get(j).getPositn1()));
	            	}
	            	
	            	for (int k=0;k<firmList.size();k++) {
	            		if (list.get(j).getFirmDes().equals(firmList.get(k).getDes())) {
	            			String samount = sheet.getCell(2*k+5, index+3).getContents();
	            			String samount1 = sheet.getCell(2*k+6, index+3).getContents();
	            			Double amount = 0.0;
	            			Double amount1 = 0.0;
	            			if(!"".equals(samount)){
	            				amount += Double.parseDouble(samount);
	            			}
	            			if(!"".equals(samount1)){
	            				amount1 += Double.parseDouble(samount1);
	            			}
	            			sheet.addCell(new Label(2*k+5, index+3, list.get(j).getAmount()+amount+""));
	            			sheet.addCell(new Label(2*k+6, index+3, list.get(j).getAmountin()+amount1+""));
	            			continue;
	            		}
					}
		    	}
			}
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
//		} catch (RowsExceededException e) {
//			e.printStackTrace();
//		} catch (WriteException e) {
//			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
	/***
	 * 新版本的导出分拨单3
	 * 供应商不分页签 金太子导出
	 * 20160331
	 * @author jinshuai
	 * @param os
	 * @param list
	 * @param firmCode
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportFenboExcel_newthree(OutputStream os, List<Dis> lists, String firmCode) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		if(lists==null||lists.size()==0){
			return false;
		}
		
		//保留两位小数
		DecimalFormat df = new DecimalFormat("0.00");
		
		// 设置格式
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		
		try {
			//设置居中
			titleStyle.setAlignment(Alignment.CENTRE);
			titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			contentStyle.setAlignment(Alignment.CENTRE);
			contentStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			
			//得到仓位信息
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", CodeHelper.replaceCode(firmCode));
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			workBook = Workbook.createWorkbook(os);
			
			//过滤掉档口
			Iterator<Positn> listIter = firmList.iterator();
			while(listIter.hasNext()){
				Positn p = listIter.next();
				String typ = p.getTyp();
				if(typ!=null&&typ.equals("1207")){
					listIter.remove();
				}
			}
			
			//不分页 只有一个sheet
			WritableSheet sheet = workBook.createSheet("sheet1", 1);
			//合并单元格
			sheet.mergeCells(0, 1, 0, 2);
			sheet.mergeCells(1, 1, 1, 2);
			sheet.mergeCells(2, 1, 2, 2);
			sheet.mergeCells(3, 1, 3, 2);
			sheet.mergeCells(4, 1, 4, 2);
			
			//标题
			int firmssize = 0;
			if(firmList!=null){
				firmssize = firmList.size();
			}
			sheet.mergeCells(0, 0, firmssize+4, 0);
			sheet.addCell(new Label(0,0,"分拨单",titleStyle));
			
			//遍历list填充表格内容
			sheet.addCell(new Label(0, 1, "物资编码",contentStyle));
			sheet.addCell(new Label(1, 1, "物资名称",contentStyle));
			sheet.addCell(new Label(2, 1, "二级类别",contentStyle));
			sheet.addCell(new Label(3, 1, "单位",contentStyle));
			sheet.addCell(new Label(4, 1, "合计数量",contentStyle));
			
			//金太子
			
			//遍历仓位信息，生成表头
			if(firmList!=null){
				for (int k=0;k<firmList.size();k++) {
					sheet.addCell(new Label(k+5, 1, firmList.get(k).getDes()));
					sheet.addCell(new Label(k+5, 2, "报单"));
				}
			}
			
			//String sp_code
			//Integer 行数
			Map<String,Integer> saveSpCod = new HashMap<String,Integer>();
			//循环集合
            for(int j=0,len=lists.size();j<len;j++) {
            	Dis itemDis = lists.get(j);
            	String spCode1 = itemDis.getSp_code();
            	if(saveSpCod.containsKey(spCode1)){
            		//得到行数
            		int lineNumber = saveSpCod.get(spCode1);
    				if(firmList!=null){
	            		for(int k=0;k<firmList.size();k++){
		            		if (itemDis.getFirmDes().equals(firmList.get(k).getDes())) {
		            			String samount1 = sheet.getCell(k+5, lineNumber).getContents();
		            			double amount1 = 0.0;
		            			if(null!=samount1&&!"".equals(samount1)){
		            				try{
		            					amount1 = Double.parseDouble(samount1);
		            				}catch(Exception e){
		            					System.out.println("转换出错-1");
		            				}
		            			}
		            			
		            			//保留两位小数
		            			double dvalue = Double.parseDouble(df.format(itemDis.getAmount1()+amount1));
		            			
								jxl.write.Number number = new jxl.write.Number(k+5, lineNumber, dvalue,doubleStyle);
		            			sheet.addCell(number);
		            			break;
		            		}
	            		}
    				}
            	}else{
            		int lineNumber = saveSpCod.size()+3;
            		//存放信息
            		saveSpCod.put(spCode1, lineNumber);
    				String first = itemDis.getSp_name();
    				String sp_code = itemDis.getSp_code();//物资编码
    				//二级类别
    				String grpdes = itemDis.getGrpdes();
    				//单位 采购单位
    				String unit = itemDis.getUnit3();
    				sheet.addCell(new Label(0, lineNumber, sp_code));
    				sheet.addCell(new Label(1, lineNumber, first));
    				//二级类别
    				sheet.addCell(new Label(2, lineNumber, grpdes));
    				//单位
    				sheet.addCell(new Label(3, lineNumber, unit));
    				//把对应供应商的数据放进去
    				if(firmList!=null){
	            		for(int k=0;k<firmList.size();k++){
		            		if (itemDis.getFirmDes().equals(firmList.get(k).getDes())) {
		            			String samount1 = sheet.getCell(k+5, lineNumber).getContents();
		            			double amount1 = 0.0;
		            			if(null!=samount1&&!"".equals(samount1)){
		            				try{
		            					amount1 = Double.parseDouble(samount1);
		            				}catch(Exception e){
		            					System.out.println("转换出错-2");
		            				}
		            			}
		            			
		            			//保留两位小数
		            			double dvalue = Double.parseDouble(df.format(itemDis.getAmount1()+amount1));
		            			
								jxl.write.Number number = new jxl.write.Number(k+5, lineNumber, dvalue,doubleStyle);
		            			sheet.addCell(number);
		            			break;
		            		}
	            		}
    				}	
            	}
	    	}
	            
            //合计列
            Iterator<String> iter2 = saveSpCod.keySet().iterator();
            //遍历保存的spcode和对应的列进行合计
            while(iter2.hasNext()){
            	String key = iter2.next();
            	int line = saveSpCod.get(key);
        		double dsum = 0d;
            	for(int k=0,lenk=firmList.size();k<lenk;k++){
            		String samount = sheet.getCell(k+5, line).getContents();
            		double ditem = 0d;
        			if(!"".equals(samount)&&null!=samount){
        				try{
        					ditem += Double.parseDouble(samount.trim());
        				}
        				catch(Exception e){
        				}
        			}
        			dsum = dsum + ditem;
            	}
            	
    			//保留两位小数
    			double dvalue = Double.parseDouble(df.format(dsum));
            	
            	//把合计数值放到4列
            	jxl.write.Number number = new jxl.write.Number(4, line, dvalue,doubleStyle);
				sheet.addCell(number);
            }
			
			workBook.write();
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	

	/***
	 * 新版本的导出分拨单2
	 * 向阳渔港——导出的分拨单——新增"规格""单位""合计"列，删除"报货"表头需要显示仓位和供应商
	 * 20160331
	 * @author jinshuai
	 * @param os
	 * @param list
	 * @param firmCode
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportFenboExcel_newtwo(OutputStream os, List<Dis> lists, String firmCode,Date dhd) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		if(lists==null||lists.size()==0){
			return false;
		}
		
		//根据供应商分集合
		Map<String,List<Dis>> fens = new HashMap<String,List<Dis>>();
		List<Dis> itemList;
		for(int i=0,len=lists.size();i<len;i++){
			Dis itemDis = lists.get(i);
			String deliverDes = itemDis.getDeliverDes();
			if(deliverDes==null){
				deliverDes="其它";
			}
			if(fens.containsKey(deliverDes)){
				fens.get(deliverDes).add(itemDis);
			}else{
				itemList = new ArrayList<Dis>();
				itemList.add(itemDis);
				fens.put(deliverDes, itemList);
			}
		}
		
		// 设置格式
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		
		try {
			//设置居中
			titleStyle.setAlignment(Alignment.CENTRE);
			titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			contentStyle.setAlignment(Alignment.CENTRE);
			contentStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			
			//得到仓位信息
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", CodeHelper.replaceCode(firmCode));
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			workBook = Workbook.createWorkbook(os);
			
			//过滤掉档口
			Iterator<Positn> listIter = firmList.iterator();
			while(listIter.hasNext()){
				Positn p = listIter.next();
				String typ = p.getTyp();
				if(typ!=null&&typ.equals("1207")){
					listIter.remove();
				}
			}
			
			if(null==firmList||firmList.size()==0){
				return false;
			}
			
			//门店数量
			int firmListSize = firmList.size();
			
			//循环
			Iterator<String> iter = fens.keySet().iterator();
			int i = -1;
			//循环创建sheet
			while(iter.hasNext()){
				String fensKey = iter.next();
				List<Dis> fensValue = fens.get(fensKey);
				//设置第几个标签
				i++;
				//创建sheet
				WritableSheet sheet = workBook.createSheet(fensKey, i);
				//合并单元格
				sheet.mergeCells(0, 1, 0, 2);
				sheet.mergeCells(1, 1, 1, 2);
				sheet.mergeCells(2, 1, 2, 2);
				sheet.mergeCells(3, 1, 3, 2);
				sheet.mergeCells(4, 1, 4, 2);
				sheet.mergeCells(5, 1, 5, 2);
				
				//设置序列号列自适应宽度
				sheet.setColumnView(0, 6);
				
				
				//遍历list填充表格内容
				sheet.addCell(new Label(0, 1, "序列号",contentStyle));
				//序列号
				int xlh = 1;
				sheet.addCell(new Label(1, 1, "物资编码",contentStyle));
				sheet.addCell(new Label(2, 1, "物资名称",contentStyle));
				sheet.addCell(new Label(3, 1, "规格",contentStyle));
				sheet.addCell(new Label(4, 1, "单位",contentStyle));
				sheet.addCell(new Label(5, 1, "合计",contentStyle));
				
				//String sp_code
				//Integer 行数
				Map<String,Integer> saveSpCod = new HashMap<String,Integer>();
				
				//保存数据
				Map<Integer,Map<String,String>> savedMap = new HashMap<Integer,Map<String,String>>();
				
				//循环集合
	            for(int j=0,len=fensValue.size();j<len;j++) {
	            	Dis itemDis = fensValue.get(j);
	            	String spCode1 = itemDis.getSp_code();
	            	if(saveSpCod.containsKey(spCode1)){
	            		//得到行数
	            		int lineNumber = saveSpCod.get(spCode1);
	            		Map<String,String> dataMap = savedMap.get(lineNumber);
	            		for(int k=0;k<firmListSize;k++){
		            		if (itemDis.getFirmDes().equals(firmList.get(k).getDes())) {
		            			if(dataMap.containsKey(String.valueOf(k))){
			            			String samount1 = dataMap.get(String.valueOf(k));
			            			double amount1 = strToNum(samount1);
			            			dataMap.put(String.valueOf(k), String.valueOf(itemDis.getAmount1sto()+amount1));
			            			//备注
			            			String ylmemo = dataMap.get(k+"==");
			            			String addmemo = itemDis.getMemo();
			            			if(addmemo!=null&&!"".equals(addmemo)){
			            				ylmemo = ylmemo + "|" + itemDis.getMemo();
			            				dataMap.put(k+"==", ylmemo);
			            			}
			            			break;
		            			}
		            			else{
			            			dataMap.put(String.valueOf(k), String.valueOf(itemDis.getAmount1sto()));
			            			//备注
			            			dataMap.put(k+"==", itemDis.getMemo());
			            			break;
		            			}
		            		}
	            		}
	            	}else{
	            		int lineNumber = saveSpCod.size()+3;
	            		//存放信息
	            		saveSpCod.put(spCode1, lineNumber);
	            		Map<String,String> dataMap = new HashMap<String,String>();
	            		savedMap.put(lineNumber, dataMap);
	    				String first = itemDis.getSp_name();
	    				String sp_code = itemDis.getSp_code();//物资编码
	    				//规格
	    				String sp_desc = itemDis.getSp_desc();
	    				//单位 采购单位
	    				String unit = itemDis.getUnit3();
	    				sheet.addCell(new Label(0, lineNumber, String.valueOf(xlh++)));
	    				sheet.addCell(new Label(1, lineNumber, sp_code));
	    				sheet.addCell(new Label(2, lineNumber, first));
	    				//规格
	    				sheet.addCell(new Label(3, lineNumber, sp_desc));
	    				//单位
	    				sheet.addCell(new Label(4, lineNumber, unit));
	    				//把对应供应商的数据放进去
	            		for(int k=0;k<firmListSize;k++){
		            		if (itemDis.getFirmDes().equals(firmList.get(k).getDes())) {
		            			dataMap.put(String.valueOf(k), String.valueOf(itemDis.getAmount1sto()));
		            			dataMap.put(k+"==", itemDis.getMemo());
		            			break;
		            		}
	            		}
	            	}
		    	}
	            
	            
	            //保存合计数据
	            //去掉没有数据的列
	            boolean[] isShows = new boolean[firmListSize];
	            Map<Integer,Double> heji = new HashMap<Integer,Double>();
	            Iterator<Integer> iterx1 = savedMap.keySet().iterator();
	            while(iterx1.hasNext()){
	            	int lineNumber = iterx1.next();
	            	Map<String,String> mapx1 = savedMap.get(lineNumber);
	            	double dnum = 0d;
		            for(int fs=0;fs<firmListSize;fs++){
		            	dnum += strToNum(mapx1.get(String.valueOf(fs)));
		            	if(strToNum(mapx1.get(String.valueOf(fs)))!=0){
		            		isShows[fs]=true;
		            	}
		            }
		            heji.put(lineNumber, dnum);
	            }
	            
	            //显示数据内容
	            iterx1 = savedMap.keySet().iterator();
	            while(iterx1.hasNext()){
	            	int lineNumber = iterx1.next();
	            	Map<String,String> mapx1 = savedMap.get(lineNumber);
	            	int jlnum = 0;
		            //显示
	        		for(int k=0;k<firmListSize;k++){
	        			boolean isShow = isShows[k];
	        			if(isShow){
	        				//update by jinshuai at 20160406
	        				//把0的数据改成空白
	        				double thenum = strToNum(mapx1.get(String.valueOf(k)));
	        				if(thenum==0){
			    				sheet.addCell(new Label(jlnum+6, lineNumber, ""));
	        				}else{
		    	            	jxl.write.Number number = new jxl.write.Number(jlnum+6, lineNumber, thenum, doubleStyle);
			    				sheet.addCell(number);
	        				}
		    				sheet.addCell(new Label(jlnum+7, lineNumber, mapx1.get(k+"==")));
	        				jlnum += 2;
	        			}
	        		}
	            }
	            
	            
            	int jlnum = 0;
				//遍历仓位信息，生成表头
				for (int k=0;k<firmListSize;k++) {
					if(isShows[k]){
						sheet.mergeCells(jlnum+6, 1, jlnum+7, 1);
						sheet.addCell(new Label(jlnum+6, 1, firmList.get(k).getDes(),contentStyle));
						sheet.addCell(new Label(jlnum+6, 2, "实发"));
						sheet.addCell(new Label(jlnum+7, 2, "备注"));
						jlnum += 2;
					}
				}
				
				//显示合计
	            iterx1 = heji.keySet().iterator();
	            while(iterx1.hasNext()){
	            	int lineNumber = iterx1.next();
	            	double value = heji.get(lineNumber);
	            	//把合计数值放到4列
	            	
	            	//把为0的改为空白
    				if(value==0){
	    				sheet.addCell(new Label(5, lineNumber, ""));
    				}else{
		            	jxl.write.Number number = new jxl.write.Number(5, lineNumber, value,doubleStyle);
	    				sheet.addCell(number);
    				}
	            }
				
				//标题
				sheet.mergeCells(0, 0, jlnum+5, 0);
				//添加到货日
				String title = fensKey;
				if(dhd!=null&&!"".equals(dhd)){
					title = title + "(到货日:" +new SimpleDateFormat("yyyy-MM-dd").format(dhd)+")";
				}
				sheet.addCell(new Label(0,0,title,titleStyle));
				
				
			}
			
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 新版本的导出分拨单4
	 * 聪少——导出的分拨单——表格添加门店，电话信息
	 * 20160331
	 * @author jinshuai
	 * @param os
	 * @param list
	 * @param firmCode
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportFenboExcel_newfour(OutputStream os, List<Dis> lists, String firmCode,Date dhd) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		if(lists==null||lists.size()==0){
			return false;
		}
		
		//根据供应商分集合
		Map<String,List<Dis>> fens = new HashMap<String,List<Dis>>();
		List<Dis> itemList;
		for(int i=0,len=lists.size();i<len;i++){
			Dis itemDis = lists.get(i);
			String deliverDes = itemDis.getDeliverDes();
			if(deliverDes==null){
				deliverDes="其它";
			}
			if(fens.containsKey(deliverDes)){
				fens.get(deliverDes).add(itemDis);
			}else{
				itemList = new ArrayList<Dis>();
				itemList.add(itemDis);
				fens.put(deliverDes, itemList);
			}
		}
		
		// 设置格式
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		
		try {
			//设置居中
			titleStyle.setAlignment(Alignment.CENTRE);
			titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			contentStyle.setAlignment(Alignment.CENTRE);
			contentStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			
			//得到仓位信息
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", CodeHelper.replaceCode(firmCode));
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			workBook = Workbook.createWorkbook(os);
			
			//过滤掉档口
			Iterator<Positn> listIter = firmList.iterator();
			while(listIter.hasNext()){
				Positn p = listIter.next();
				String typ = p.getTyp();
				if(typ!=null&&typ.equals("1207")){
					listIter.remove();
				}
			}
			
			if(null==firmList||firmList.size()==0){
				return false;
			}
			
			//门店数量
			int firmListSize = firmList.size();
			
			//循环
			Iterator<String> iter = fens.keySet().iterator();
			int i = -1;
			//循环创建sheet
			while(iter.hasNext()){
				String fensKey = iter.next();
				List<Dis> fensValue = fens.get(fensKey);
				//设置第几个标签
				i++;
				//创建sheet
				WritableSheet sheet = workBook.createSheet(fensKey, i);
				//合并单元格
				sheet.mergeCells(0, 1, 0, 2);
				sheet.mergeCells(1, 1, 1, 2);
				sheet.mergeCells(2, 1, 2, 2);
				sheet.mergeCells(3, 1, 3, 2);
				sheet.mergeCells(4, 1, 4, 2);
				sheet.mergeCells(5, 1, 5, 2);
				
				//设置序列号列自适应宽度
				sheet.setColumnView(0, 6);
				
				
				//遍历list填充表格内容
				sheet.addCell(new Label(0, 1, "序列号",contentStyle));
				//序列号
				int xlh = 1;
				sheet.addCell(new Label(1, 1, "物资编码",contentStyle));
				sheet.addCell(new Label(2, 1, "物资名称",contentStyle));
				sheet.addCell(new Label(3, 1, "规格",contentStyle));
				sheet.addCell(new Label(4, 1, "单位",contentStyle));
				sheet.addCell(new Label(5, 1, "合计",contentStyle));
				
				//String sp_code
				//Integer 行数
				Map<String,Integer> saveSpCod = new HashMap<String,Integer>();
				
				//保存数据
				Map<Integer,Map<String,String>> savedMap = new HashMap<Integer,Map<String,String>>();
				
				//循环集合
	            for(int j=0,len=fensValue.size();j<len;j++) {
	            	Dis itemDis = fensValue.get(j);
	            	String spCode1 = itemDis.getSp_code();
	            	if(saveSpCod.containsKey(spCode1)){
	            		//得到行数
	            		int lineNumber = saveSpCod.get(spCode1);
	            		Map<String,String> dataMap = savedMap.get(lineNumber);
	            		for(int k=0;k<firmListSize;k++){
		            		if (itemDis.getFirmDes().equals(firmList.get(k).getDes())) {
		            			if(dataMap.containsKey(String.valueOf(k))){
			            			String samount1 = dataMap.get(String.valueOf(k));
			            			double amount1 = strToNum(samount1);
			            			dataMap.put(String.valueOf(k), String.valueOf(itemDis.getAmount1sto()+amount1));
			            			//备注
			            			String ylmemo = dataMap.get(k+"==");
			            			String addmemo = itemDis.getMemo();
			            			if(addmemo!=null&&!"".equals(addmemo)){
			            				ylmemo = ylmemo + "|" + itemDis.getMemo();
			            				dataMap.put(k+"==", ylmemo);
			            			}
			            			break;
		            			}
		            			else{
			            			dataMap.put(String.valueOf(k), String.valueOf(itemDis.getAmount1sto()));
			            			//备注
			            			dataMap.put(k+"==", itemDis.getMemo());
			            			break;
		            			}
		            		}
	            		}
	            	}else{
	            		int lineNumber = saveSpCod.size()+3;
	            		//存放信息
	            		saveSpCod.put(spCode1, lineNumber);
	            		Map<String,String> dataMap = new HashMap<String,String>();
	            		savedMap.put(lineNumber, dataMap);
	    				String first = itemDis.getSp_name();
	    				String sp_code = itemDis.getSp_code();//物资编码
	    				//规格
	    				String sp_desc = itemDis.getSp_desc();
	    				//单位 采购单位
	    				String unit = itemDis.getUnit3();
	    				sheet.addCell(new Label(0, lineNumber, String.valueOf(xlh++)));
	    				sheet.addCell(new Label(1, lineNumber, sp_code));
	    				sheet.addCell(new Label(2, lineNumber, first));
	    				//规格
	    				sheet.addCell(new Label(3, lineNumber, sp_desc));
	    				//单位
	    				sheet.addCell(new Label(4, lineNumber, unit));
	    				//把对应供应商的数据放进去
	            		for(int k=0;k<firmListSize;k++){
		            		if (itemDis.getFirmDes().equals(firmList.get(k).getDes())) {
		            			dataMap.put(String.valueOf(k), String.valueOf(itemDis.getAmount1sto()));
		            			dataMap.put(k+"==", itemDis.getMemo());
		            			dataMap.put(k+"===", firmList.get(k).getVaddr());
		            			dataMap.put(k+"====", firmList.get(k).getVtel());
		            			break;
		            		}
	            		}
	            	}
		    	}
	            
	            
	            //保存合计数据
	            //去掉没有数据的列
	            boolean[] isShows = new boolean[firmListSize];
	            Map<Integer,Double> heji = new HashMap<Integer,Double>();
	            Iterator<Integer> iterx1 = savedMap.keySet().iterator();
	            while(iterx1.hasNext()){
	            	int lineNumber = iterx1.next();
	            	Map<String,String> mapx1 = savedMap.get(lineNumber);
	            	double dnum = 0d;
		            for(int fs=0;fs<firmListSize;fs++){
		            	dnum += strToNum(mapx1.get(String.valueOf(fs)));
		            	if(strToNum(mapx1.get(String.valueOf(fs)))!=0){
		            		isShows[fs]=true;
		            	}
		            }
		            heji.put(lineNumber, dnum);
	            }
	            
	            //显示数据内容
	            iterx1 = savedMap.keySet().iterator();
	            while(iterx1.hasNext()){
	            	int lineNumber = iterx1.next();
	            	Map<String,String> mapx1 = savedMap.get(lineNumber);
	            	int jlnum = 0;
		            //显示
	        		for(int k=0;k<firmListSize;k++){
	        			boolean isShow = isShows[k];
	        			if(isShow){
	        				//update by jinshuai at 20160406
	        				//把0的数据改成空白
	        				double thenum = strToNum(mapx1.get(String.valueOf(k)));
	        				if(thenum==0){
			    				sheet.addCell(new Label(jlnum+6, lineNumber, ""));
	        				}else{
		    	            	jxl.write.Number number = new jxl.write.Number(jlnum+6, lineNumber, thenum, doubleStyle);
			    				sheet.addCell(number);
	        				}
		    				sheet.addCell(new Label(jlnum+7, lineNumber, mapx1.get(k+"==")));
		    				sheet.addCell(new Label(jlnum+8, lineNumber, mapx1.get(k+"===") ));
		    				sheet.addCell(new Label(jlnum+9, lineNumber, mapx1.get(k+"====")));
	        				jlnum += 2;
	        			}
	        		}
	            }
	            
	            
            	int jlnum = 0;
				//遍历仓位信息，生成表头
				for (int k=0;k<firmListSize;k++) {
					if(isShows[k]){
						sheet.mergeCells(jlnum+6, 1, jlnum+9, 1);
						sheet.addCell(new Label(jlnum+6, 1, firmList.get(k).getDes(),contentStyle));
						sheet.addCell(new Label(jlnum+6, 2, "实发"));
						sheet.addCell(new Label(jlnum+7, 2, "备注"));
						sheet.addCell(new Label(jlnum+8, 2, "地址"));
						sheet.addCell(new Label(jlnum+9, 2, "电话"));
						jlnum += 4;
					}
				}
				
				//显示合计
	            iterx1 = heji.keySet().iterator();
	            while(iterx1.hasNext()){
	            	int lineNumber = iterx1.next();
	            	double value = heji.get(lineNumber);
	            	//把合计数值放到4列
	            	
	            	//把为0的改为空白
    				if(value==0){
	    				sheet.addCell(new Label(5, lineNumber, ""));
    				}else{
		            	jxl.write.Number number = new jxl.write.Number(5, lineNumber, value,doubleStyle);
	    				sheet.addCell(number);
    				}
	            }
				
				//标题
				sheet.mergeCells(0, 0, jlnum+5, 0);
				//添加到货日
				String title = fensKey;
				if(dhd!=null&&!"".equals(dhd)){
					title = title + "(到货日:" +new SimpleDateFormat("yyyy-MM-dd").format(dhd)+")";
				}
				sheet.addCell(new Label(0,0,title,titleStyle));
				
				
			}
			
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/***
	 * 向阳渔港导出分拨单
	 * @param os
	 * @param lists
	 * @param firmCode
	 * @param dhd
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportFenboExcel3(OutputStream os, List<Dis> lists, String firmCode,Date dhd) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		if(lists==null||lists.size()==0){
			return false;
		}
		
		//根据供应商分集合
		Map<String,List<Dis>> fens = new HashMap<String,List<Dis>>();
		List<Dis> itemList;
		for(int i=0,len=lists.size();i<len;i++){
			Dis itemDis = lists.get(i);
			String deliverDes = itemDis.getDeliverDes();
			if(deliverDes==null){
				deliverDes="其它";
			}
			if(fens.containsKey(deliverDes)){
				fens.get(deliverDes).add(itemDis);
			}else{
				itemList = new ArrayList<Dis>();
				itemList.add(itemDis);
				fens.put(deliverDes, itemList);
			}
		}
		
		// 设置格式
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);// 标题格式
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);// 文本内容格式
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont, new NumberFormat("0.00"));// 设定带小数数字单元格格式
		
		try {
			//设置居中
			titleStyle.setAlignment(Alignment.CENTRE);
			titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			contentStyle.setAlignment(Alignment.CENTRE);
			contentStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
			
			//1得到所有报货仓位
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", CodeHelper.replaceCode(firmCode));
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			//过滤掉档口
			Iterator<Positn> listIter = firmList.iterator();
			while(listIter.hasNext()){
				Positn p = listIter.next();
				String typ = p.getTyp();
				if(typ!=null&&typ.equals("1207")){
					listIter.remove();
				}
			}
			
			workBook = Workbook.createWorkbook(os);
			
			if(null==firmList||firmList.size()==0){
				return false;
			}
			
			//门店数量
//			int firmListSize = firmList.size();
			
			//循环
			Iterator<String> iter = fens.keySet().iterator();
			int i = -1;
			//循环创建sheet
			while(iter.hasNext()){
				String fensKey = iter.next();
				List<Dis> fensValue = fens.get(fensKey);
				//设置第几个标签
				i++;
				//创建sheet
				WritableSheet sheet = workBook.createSheet(fensKey, i);
			    sheet.setColumnView(0, 4);
			    sheet.setColumnView(3, 4);
			    sheet.setColumnView(4, 8);
			    sheet.setColumnView(5, 8);
			    sheet.setColumnView(7, 8);
			    sheet.setColumnView(9, 8);
			    sheet.setColumnView(11, 8);
			    sheet.setColumnView(13, 8);
				//2.得到这个供应商对应的报货分店
				List<Positn> positnList = new ArrayList<Positn>();
				positnList.addAll(firmList);
				Iterator<Positn> car = positnList.iterator();
				a:while(car.hasNext()){
					Positn p = car.next();
					for(Dis d : fensValue){
						if(d.getFirmCode().equals(p.getCode())){
							continue a;
						}
					}
					car.remove();
				}
				//2.5 得到一共多少个物资，合计多少
				Map<String,Double> maps = new HashMap<String,Double>();
				for(Dis d : fensValue){
					double amount = !maps.containsKey(d.getSp_code()) ? 0 : maps.get(d.getSp_code());
					amount += d.getAmount1sto();
					maps.put(d.getSp_code(), amount);
				}
				//3.5个一组  一共分几组
				int group = (positnList.size()-1)/5;//循环几组物资
				int hang = maps.size() + 3;
				int xuhao = 0;//序号
				int teshu = 0;
				int teshu1 = 0;
				for(int g = 0; g<= group; g++){
					teshu += teshu1;
					teshu1 = 0;
					//标题
					sheet.mergeCells(0, g*hang+teshu, 14, g*hang+teshu);
					//添加到货日
					String title = fensKey;
					if(dhd!=null&&!"".equals(dhd)){
						title = title + "(到货日:" +new SimpleDateFormat("yyyy-MM-dd").format(dhd)+")";
					}
					sheet.addCell(new Label(0,g*hang+teshu,"("+(g+1)+")"+title,titleStyle));
					//合并单元格
					sheet.mergeCells(0, g*hang+1+teshu, 0, g*hang+2+teshu);
					sheet.mergeCells(1, g*hang+1+teshu, 1, g*hang+2+teshu);
					sheet.mergeCells(2, g*hang+1+teshu, 2, g*hang+2+teshu);
					sheet.mergeCells(3, g*hang+1+teshu, 3, g*hang+2+teshu);
					sheet.mergeCells(4, g*hang+1+teshu, 4, g*hang+2+teshu);
					
					
					//遍历list填充表格内容
					sheet.addCell(new Label(0, g*hang+1+teshu, "序号",contentStyle));
					sheet.addCell(new Label(1, g*hang+1+teshu, "物资名称",contentStyle));
					sheet.addCell(new Label(2, g*hang+1+teshu, "规格",contentStyle));
					sheet.addCell(new Label(3, g*hang+1+teshu, "采购单位",contentStyle));
					sheet.addCell(new Label(4, g*hang+1+teshu, "合计",contentStyle));
					
	                //遍历仓位信息，生成表头
					Iterator<Positn> car1 = positnList.iterator();
					List<Positn> delPositn = new ArrayList<Positn>();
					int carfirst = 0;
					while(car1.hasNext()){
						Positn p = car1.next();
						if(carfirst >= 10){
							break;
						}
						sheet.mergeCells(carfirst+5, g*hang+1+teshu, carfirst+6, g*hang+1+teshu);
                        sheet.addCell(new Label(carfirst+5, g*hang+1+teshu, p.getDes(),contentStyle));
                        sheet.addCell(new Label(carfirst+5, g*hang+2+teshu, "实发"));
                        sheet.addCell(new Label(carfirst+6, g*hang+2+teshu, "备注"));
                        carfirst += 2;
                        delPositn.add(p);
                        car1.remove();
					}
					
					String sp_code = fensValue.get(0).getSp_code();//物资编码
					String first = fensValue.get(0).getSp_name();
					String sp_desc = null == fensValue.get(0).getSp_desc()?"":fensValue.get(0).getSp_desc();
                    String unit = fensValue.get(0).getUnit3();
                    sheet.addCell(new Label(0, g*hang+3+teshu, ++xuhao+""));//序号
					sheet.addCell(new Label(1, g*hang+3+teshu, first));
					sheet.addCell(new Label(2, g*hang+3+teshu, sp_desc));
                    sheet.addCell(new Label(3, g*hang+3+teshu, unit));
                    sheet.addCell(new jxl.write.Number(4, g*hang+3+teshu, maps.get(sp_code),doubleStyle));
                    int index = 0;
                    for(int j=0;j<fensValue.size();j++) {
                    	String spCode = fensValue.get(j).getSp_code();
		            	String spName = fensValue.get(j).getSp_name();
		            	String spDesc = null == fensValue.get(j).getSp_desc()?"":fensValue.get(j).getSp_desc();
		            	String Unit = fensValue.get(j).getUnit3();
		            	if(!spCode.equals(sp_code)){
		            		index++;
		            		
		            		//如果是40的倍数 就循环一下表头
	                    	if((index+1+g*hang+3+teshu)%40 == 0){
	                    		//标题
	        					sheet.mergeCells(0, index+g*hang+3+teshu, 14, index+g*hang+3+teshu);
	        					sheet.addCell(new Label(0,index+g*hang+3+teshu,"("+(g+1)+")"+title,titleStyle));
	        					//合并单元格
	        					sheet.mergeCells(0, index+g*hang+3+1+teshu, 0, index+g*hang+3+2+teshu);
	        					sheet.mergeCells(1, index+g*hang+3+1+teshu, 1, index+g*hang+3+2+teshu);
	        					sheet.mergeCells(2, index+g*hang+3+1+teshu, 2, index+g*hang+3+2+teshu);
	        					sheet.mergeCells(3, index+g*hang+3+1+teshu, 3, index+g*hang+3+2+teshu);
	        					sheet.mergeCells(4, index+g*hang+3+1+teshu, 4, index+g*hang+3+2+teshu);	        					
	        					
	        					//遍历list填充表格内容
	        					sheet.addCell(new Label(0, index+g*hang+3+1+teshu, "序号",contentStyle));
	        					sheet.addCell(new Label(1, index+g*hang+3+1+teshu, "物资名称",contentStyle));
	        					sheet.addCell(new Label(2, index+g*hang+3+1+teshu, "规格",contentStyle));
	        					sheet.addCell(new Label(3, index+g*hang+3+1+teshu, "采购单位",contentStyle));
	        					sheet.addCell(new Label(4, index+g*hang+3+1+teshu, "合计",contentStyle));
	        					
	        					for (int k=0; k<delPositn.size(); k++) {
	        						sheet.mergeCells(2*k+5, index+g*hang+3+1+teshu, 2*k+6, index+g*hang+3+1+teshu);
	                                sheet.addCell(new Label(2*k+5, index+g*hang+3+1+teshu, delPositn.get(k).getDes(),contentStyle));
	                                sheet.addCell(new Label(2*k+5, index+g*hang+3+2+teshu, "实发"));
	                                sheet.addCell(new Label(2*k+6, index+g*hang+3+2+teshu, "备注"));
	        					}
	        					index += 3;
	        					teshu1 += 3;
	                    	}
		            		sp_code = spCode;
		            		first=spName;
		            		sp_desc = spDesc;
		            		unit = Unit;
		            		sheet.addCell(new Label(0, index+g*hang+3+teshu, ++xuhao +""));//序号
		            		sheet.addCell(new Label(1, index+g*hang+3+teshu, spName));
		            		sheet.addCell(new Label(2, index+g*hang+3+teshu, spDesc));
		            		sheet.addCell(new Label(3, index+g*hang+3+teshu, Unit));
		            		sheet.addCell(new jxl.write.Number(4, index+g*hang+3+teshu, maps.get(spCode),doubleStyle));
		            	}
		            	
		            	for (int k=0; k<delPositn.size(); k++) {
		            		if (fensValue.get(j).getFirmDes().equals(delPositn.get(k).getDes())) {
		            			String samount1 = sheet.getCell(2*k+5, index+g*hang+3+teshu).getContents();
		            			Double amount1 = 0.0;
		            			if(!"".equals(samount1)){
		            				amount1 += Double.parseDouble(samount1);
		            			}
		            			sheet.addCell(new jxl.write.Number(2*k+5, index+g*hang+3+teshu, fensValue.get(j).getAmount1sto()+amount1,doubleStyle));
		            			sheet.addCell(new Label(2*k+6, index+g*hang+3+teshu, null == fensValue.get(j).getMemo()?"":fensValue.get(j).getMemo()));
		            			continue;
		            		}
						}
			    	}
				}
			}
			
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 字符串转double
	 * @param str
	 * @return
	 */
	public static double strToNum(String str){
		if(null==str||"".equals(str)){
			return 0d;
		}
		double num = 0d;
		try{
			num = Double.parseDouble(str.trim());
		}
		catch(Exception e){
		}
		return num;
	}
	
	
	/***
	 * 新版本的导出分拨单
	 * @author wjf
	 * @param os
	 * @param list
	 * @param firmCode
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportFenboExcel_new(OutputStream os, List<Dis> list, String firmCode) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", CodeHelper.replaceCode(firmCode));
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				for (int k=0;k<firmList.size();k++) {
					sheet.addCell(new Label(2*k+2, 1, firmList.get(k).getDes()));
					sheet.addCell(new Label(2*k+2, 2, "报单"));
					sheet.addCell(new Label(2*k+3, 2, "实发"));
				}
				
	            //遍历list填充表格内容
				sheet.addCell(new Label(0, 2, "物资编码"));
				sheet.addCell(new Label(1, 2, "物资名称"));
				int index = 0;
				String first = list.get(0).getSp_name();
				String sp_code = list.get(0).getSp_code();//物资编码
				sheet.addCell(new Label(0, index+3, sp_code));
				sheet.addCell(new Label(1, index+3, first));
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
//	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	String spName = list.get(j).getSp_name();
	            	String spCode = list.get(j).getSp_code();
	            	if(!spCode.equals(sp_code)){
	            		index+=1;
	            		first=spName;
	            		sp_code = spCode;
	            		sheet.addCell(new Label(0, index+3, spCode));
	            		sheet.addCell(new Label(1, index+3, spName));
	            	}
	            	
	            	for (int k=0;k<firmList.size();k++) {
	            		if (list.get(j).getFirmDes().equals(firmList.get(k).getDes())) {
	            			String samount = sheet.getCell(2*k+2, index+3).getContents();
	            			String samount1 = sheet.getCell(2*k+3, index+3).getContents();
	            			Double amount = 0.0;
	            			Double amount1 = 0.0;
	            			if(!"".equals(samount)){
	            				amount += Double.parseDouble(samount);
	            			}
	            			if(!"".equals(samount1)){
	            				amount1 += Double.parseDouble(samount1);
	            			}
	            			sheet.addCell(new Label(2*k+2, index+3, list.get(j).getAmount1()+amount+""));
	            			sheet.addCell(new Label(2*k+3, index+3, list.get(j).getAmount1sto()+amount1+""));
	            			continue;
	            		}
					}
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 *导出采购订单 
	 * 
	 */
	public boolean exportDisExcel(OutputStream os,List<Dis> disList,List<Dis>disListt )throws  CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (disList!=null) {
				totalCount = disList.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet("Sheet"+(i+1), i);
				WritableSheet sheet1 = workBook.createSheet("Sheet"+(i+1), i);
				sheet.setName("报货汇总表");
				sheet1.setName("报货明细表");
				sheet.addCell(new Label(0, 0,"报货汇总"));
				sheet.addCell(new Label(0, 2, "物资编码")); 
	            sheet.addCell(new Label(1, 2, "物资名称"));   
	            sheet.addCell(new Label(2, 2, "物资单位")); 
				sheet.addCell(new Label(3, 2, "物资规格"));  
				sheet.addCell(new Label(4, 2, "单价"));
				sheet.addCell(new Label(5,2,"数量"));
				sheet.addCell(new Label(6,2,"金额"));
				
				sheet1.addCell(new Label(0, 0,"报货明细"));
				sheet1.addCell(new Label(0, 2, "分店编码")); 
				sheet1.addCell(new Label(1, 2, "分店名称"));   
				sheet1.addCell(new Label(2, 2, "物资编码")); 
				sheet1.addCell(new Label(3, 2, "物资名称"));  
				sheet1.addCell(new Label(4, 2, "物资单位"));
				sheet1.addCell(new Label(5,2,"单价"));
				sheet1.addCell(new Label(6,2,"数量"));
				sheet1.addCell(new Label(7,2,"物资规格"));
				sheet1.addCell(new Label(8,2,"金额"));
				sheet1.addCell(new Label(9,2,"报货单号"));
				sheet1.addCell(new Label(10,2,"备注"));
				DecimalFormat  fnum  =  new  DecimalFormat("##0.00"); 
	            //遍历list填充表格内容
				int index = 0;
				if (disListt.size()>0) {
					for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
		            	if(j == totalCount){
		            		break;
		            	}
		            	index=j>=MAXNUM?(j-i*MAXNUM):j;
		            	if(disListt.get(j).getSp_code()!=null){
		            		sheet.addCell(new Label(0, index+3, disListt.get(j).getSp_code().toString()));
		            	}else{
		            		sheet.addCell(new Label(0,index+3,""));
		            	}
		            	if(disListt.get(j).getSp_name()!=null){
		            	 	sheet.addCell(new Label(1, index+3, disListt.get(j).getSp_name().toString()));
		            	}else{
		            		sheet.addCell(new Label(1, index+3,""));
						}	            	
		            	if( disListt.get(j).getUnit()!=null){
			            	sheet.addCell(new Label(2, index+3, disListt.get(j).getUnit().toString()));

		            	}else{
		            		sheet.addCell(new Label(2, index+3, ""));
		            	}
		            	String  sp_desc = "";   
		            	if(disListt.get(j).getSp_desc()!=null){
		            		sp_desc = disListt.get(j).getSp_desc().toString();
		            	}
		            	sheet.addCell(new Label(3, index+3,sp_desc ));
		            	String  price = "";   
		            	if(disListt.get(j).getPrice()!=null){
		            		price = disListt.get(j).getPrice().toString();
		            	}
		            	sheet.addCell(new Label(4, index+3, price));
		            	if(disListt.get(j).getAmountin()!=null){
		            		sheet.addCell(new Label(5, index+3, disListt.get(j).getAmountin().toString()));
		            	}else{
		            		sheet.addCell(new Label(5, index+3, ""));
		            	}
						if(disListt.get(j).getPrice()!=null){
							double total = ((Double.valueOf(disListt.get(j).getPrice()))*(disListt.get(j).getAmountin()));
							sheet.addCell(new Label(6, index+3,fnum.format(total)+""));
						}else{
							sheet.addCell(new Label(6, index+3,""));
						}
						//报货明细填充
						if(disList.get(j).getFirmCode()!=null){
							sheet1.addCell(new Label(0, index+3, disList.get(j).getFirmCode().toString()));
						}else{
							sheet1.addCell(new Label(0, index+3, ""));
						}
						if(disList.get(j).getFirmDes()!=null){
							sheet1.addCell(new Label(1, index+3, disList.get(j).getFirmDes().toString()));
						}else{
							sheet1.addCell(new Label(1, index+3, ""));
						}
						if(disList.get(j).getSp_code()!=null){
		            	sheet1.addCell(new Label(2, index+3, disList.get(j).getSp_code().toString()));
						}else{
			            	sheet1.addCell(new Label(2, index+3, ""));
						}
		            	if(disList.get(j).getSp_desc()!=null){
		            		sheet1.addCell(new Label(3, index+3, disList.get(j).getSp_name().toString()));
		            	}else{
		            		sheet1.addCell(new Label(3, index+3, ""));
		            	}
		            	if(disList.get(j).getUnit()!=null){
		            		sheet1.addCell(new Label(4, index+3, disList.get(j).getUnit().toString()));
		            	}else{
			            	sheet1.addCell(new Label(4, index+3, ""));
		            	}
		            	String  pricee = "";   
		            	if(disList.get(j).getSp_desc()!=null){
		            		pricee = disList.get(j).getPrice().toString();
		            	}
		            	sheet1.addCell(new Label(7, index+3,pricee ));
		            	sheet1.addCell(new Label(5, index+3, disList.get(j).getPrice().toString()));	            	
		            	sheet1.addCell(new Label(6, index+3, disList.get(j).getAmount1sto().toString()));//数量 应取调整后的报货数量（amount1sto）wangjie 2015年3月3日 15:27:57
		            	String  sp_descc = "";   
		            	if(disList.get(j).getSp_desc()!=null){
		            		sp_descc = disList.get(j).getSp_desc().toString();
		            	}
		            	sheet1.addCell(new Label(7, index+3,sp_descc ));
		            	double totall = ((disList.get(j).getPrice())*(disList.get(j).getAmount1sto().doubleValue()));
						sheet1.addCell(new Label(8, index+3,fnum.format(totall)+""));
						sheet1.addCell(new Label(9, index+3,disList.get(j).getChkstoNo().toString()));
						String memo = "";
						if(disList.get(j).getMemo()!=null){
							memo = disList.get(j).getMemo().toString();
						}
						sheet1.addCell(new Label(10, index+3,memo));
		            }
				}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 *查询动态表头 
	 */
	public List<String> findTableHead(String firmCode){
		return  disFenBoMapper.findAllDept(firmCode);
	}
	
	/**
	 *是否存在数据
	 */
	public int findDateisNull(String firmCode,String deliverCode){
		Dis dis = new Dis();
		dis.setFirmCode(firmCode);
		dis.setDeliverCode(deliverCode);
		return  disFenBoMapper.findDateisNull(dis);
	}

	public List<Dis> findYanHuoZhiFa(Dis dis,Page page) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf     直发不用采购确认，，，因为它查的视图中没有采购确认
			Acct acct = acctMapper.findAcctById(dis.getAcct());
//			if("Y".equals(acct.getYncgqr())){
//				dis.setChk1("Y");
//			}else{
//				dis.setChk1(null);
//			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			if(page == null){
				return disFenBoMapper.findYanHuoZhiFa(dis);
			}
			return pageManager.selectPage(dis, page, DisFenBoMapper.class.getName()+".findYanHuoZhiFa");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 保存并审核直发单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int saveChkinm(Chkinm chkinm) throws CRUDException {
		try {
			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			chkinm.setTyp("9916");
			chkinm.setMemo("报货直发");
			log.warn("添加直发单(主单)：\n"+chkinm);
	 		chkinmMapper.saveChkinm(chkinm);
	 		log.warn("添加直发单(详单)：");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				chkind.setDued(DateFormat.getDateByString("1899-12-30", "yyyy-MM-dd"));
					chkind.setInout(ChkinmConstants.conk);   //加入直发标志
				log.warn(chkind);
	 			chkindMapper.saveChkind(chkind);
	 			//修改申购单状态，设置入库和出库状态都为Y
	 			Dis dis=new Dis();
				dis.setChkin("Y");
				dis.setChkout("Y");
				dis.setId(chkind.getId()+"");
				disMapper.updateByIds(dis);
			}
			//审核
			chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月 
			chkinm.setStatus(chkinm.getInout());
			chkinmMapper.checkChkinm(chkinm);//审核入库单
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *查询供应商邮箱 
	 */
	public Deliver findDeliverOne(String deliverCode){
		return disFenBoMapper.findDeliverOne(deliverCode);
	}
	
	/**
	 *报货分拨数据拆分
	 */
	public void updateDispatch(Chkstod chkstod){
		for(int i=0; i<chkstod.getChkstodList().size(); i++) {
			Supply supply = new Supply();
			supply.setSp_code(chkstod.getChkstodList().get(i).getSupply().getSp_code());
			Supply su = supplyMapper.findSupplyById(supply);
			Double unitper = su.getUnitper();
			supply.setUnitper(unitper);
			Double unitper3 = su.getUnitper3();//采购数量
			supply.setUnitper3(unitper3);
			chkstod.getChkstodList().get(i).setSupply(supply);
			disFenBoMapper.updateDispatch_new(chkstod.getChkstodList().get(i));
			Chkstod chkstodd = new Chkstod();
			chkstodd.setId(chkstod.getChkstodList().get(i).getId());
			Chkstod chkstod_old = chkstodMapper.findChkstod(chkstodd).get(0);
			int psorder = chkstod_old.getPsorder()+1;
			chkstod.getChkstodList().get(i).setChkstoNo(chkstod_old.getChkstoNo());
			Double amount1 = chkstod.getChkstodList().get(i).getAmount1()-chkstod.getChkstodList().get(i).getAmount1sto();
			chkstod.getChkstodList().get(i).setAmount1(amount1);
			if(unitper3 != 0){//转换率不是0  标准数量就算一下
				chkstod.getChkstodList().get(i).setAmount(amount1/unitper3);
			}else{
				chkstod.getChkstodList().get(i).setAmount(0.0);
			}
			chkstodMapper.saveNewChkstod_new(chkstod.getChkstodList().get(i));
			chkstod.setPsorder(psorder);
			disFenBoMapper.updateDispatchNew(chkstod);
			
		}
	}
	
	/**
	 *报货分拨数据调整
	 */
	public void updateDispatchX(Chkstod chkstod){
		Supply supply = new Supply();
		supply.setSp_code(chkstod.getSupply().getSp_code());
		Double unitper = supplyMapper.findSupplyById(supply).getUnitper();
		Chkstod chkstod_old = chkstodMapper.findChkstod(chkstod).get(0);
		chkstod.setChkstoNo(chkstod_old.getChkstoNo());
		chkstod.setAmount1(chkstod.getAmount()*unitper);
		chkstod.setAmount1in(chkstod.getAmountin()*unitper);
		//售价获取
		SppriceSale spprice = new SppriceSale();
		Positn area = new Positn();
		chkstod_old = chkstodMapper.findAllChkstod(chkstod_old).get(0);
		area.setCode(chkstod_old.getFirm());
		spprice.setArea(area);
		spprice.setSupply(supply);
		spprice.setMadet(chkstod.getHoped());
		SppriceSale sppriceSale = supplyMapper.findSprice(spprice);
		
		if (sppriceSale==null){
			chkstod.setPricesale(supply.getSp_price());
		}else {
			chkstod.setPricesale(sppriceSale.getPrice().doubleValue());
		}
		//报价获取
		Spprice bprice = new Spprice();
		bprice.setSp_code(supply.getSp_code());
		bprice.setMadet(chkstod.getHoped());
		bprice.setArea(chkstod_old.getFirm());
		Spprice bprice1 = supplyMapper.findBprice(bprice);
		if (bprice1==null){
			chkstod.setPrice(supply.getSp_price());
		}else {
			chkstod.setPrice(bprice1.getPrice().doubleValue());
		}
		chkstodMapper.saveNewChkstodMis(chkstod);
		disFenBoMapper.updateDispatchNew_x(chkstod);
	}
	
	/**
	 * 导出 洞庭用
	 */
	public boolean exportFenboExcel2(OutputStream os, List<Dis> list, String firmCode) throws CRUDException {   
		WritableWorkbook workBook = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 12,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = 1;
			if(totalCount>65535){
				sheetNum = totalCount/MAXNUM+1;
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("codes", CodeHelper.replaceCode(firmCode));
			List<Positn> firmList = positnMapper.findPositnByTypCode(map);
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				titleStyle.setAlignment(Alignment.CENTRE);
				titleStyle.setBorder(Border.ALL, BorderLineStyle.THIN,
					     jxl.format.Colour.BLACK);
				titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,
					     jxl.format.Colour.BLACK);
				workBook = Workbook.createWorkbook(os);
				WritableSheet sheet = workBook.createSheet("分拨表"+(i+1), i);
				sheet.addCell(new Label(0, 0,"报货分拨表",titleStyle));
				sheet.mergeCells(0, 0, 21, 0);
//				sheet.addCell(new Label(22, 0,DateFormat.getStringByDate(list.get(0).getMaded(), "yyyy-mm-dd"),titleStyle2));
				sheet.addCell(new Label(0, 1, "物资名称",titleStyle2));
				sheet.addCell(new Label(1, 1, "规格",titleStyle2));
				for (int k=0;k<firmList.size();k++) {
					sheet.addCell(new Label(k+2, 1, firmList.get(k).getDes(),titleStyle2));
				}
	            //遍历list填充表格内容
				int index = 0;
				String first = list.get(0).getSp_name();
				String firstdesc = null == list.get(0).getSp_desc()?"" : list.get(0).getSp_desc();
				sheet.addCell(new Label(0, index+2, first,titleStyle2));
				sheet.addCell(new Label(1, index+2, firstdesc,titleStyle2));
	            for(int j=i*MAXNUM;j<(i+1)*MAXNUM;j++) {
	            	if(j == totalCount){
	            		break;
	            	}
//	            	index=j>=MAXNUM?(j-i*MAXNUM):j;
	            	String spName = list.get(j).getSp_name();
	            	String spDesc = null == list.get(j).getSp_desc()?"" :list.get(j).getSp_desc();
	            	if(!spName.equals(first) || (spName.equals(first) && !spDesc.equals(firstdesc))){
	            		index++;
	            		first=spName;
	            		firstdesc = spDesc;
	            		sheet.addCell(new Label(0, index+2, spName,titleStyle2));
	            		sheet.addCell(new Label(1, index+2, spDesc,titleStyle2));
	            	}
	            	
	            	for (int k=0;k<firmList.size();k++) {
	            		if(sheet.getCell(k+2, index+2) == null || sheet.getCell(k+2, index+2).getContents() == null || "".equals(sheet.getCell(k+2, index+2).getContents())){
	            			sheet.addCell(new Label(k+2, index+2, "",titleStyle2));
	            		}
	            		if (list.get(j).getFirmDes().equals(firmList.get(k).getDes())) {
	            			//如果同一家店报了同一个物资两次或以上。。。则要累加。。。。wjf
	            			String samount = sheet.getCell(k+2, index+2).getContents();
	            			Double amount = 0.0;
	            			if(!"".equals(samount)){
	            				amount += Double.parseDouble(samount);
	            			}
	            			sheet.addCell(new Label(k+2, index+2, list.get(j).getAmount()+amount+"",titleStyle2));
	            			continue;
	            		}
					}
		    	}
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/***
	 * 新的生成直发单方法
	 * @author wjf
	 * @param disList
	 * @param acct
	 * @param positnCode
	 * @param accountName
	 */
	public void saveChkinmzb(List<Dis> disList, String acct,String accountName,Date maded) {
		String year = mainInfoMapper.findYearrList().get(0);//会计年
		int month = 0;//会计月
		try{
			month = acctService.getOnlyAccountMonth(maded);
		}catch(Exception e){
			e.printStackTrace();
		}
		String deliverCode=disList.get(0).getDeliverCode();//供应商code
		String positnCode=disList.get(0).getPositnCode();//仓位code
		ArrayList<Dis> list=new ArrayList<Dis>();//临时list
		for (int i = 0; i < disList.size(); i++) {//循环数据
			if(disList.get(i).getPositnCode().equals(positnCode)
					&&disList.get(i).getDeliverCode().equals(deliverCode)){
				list.add(disList.get(i));
			}else{//不相等的时候
				saveChkinmzb(acct,accountName,year,month,list,maded);
				list.clear();//清空
				list.add(disList.get(i));//在list重新加入该条数据
			}
			deliverCode=disList.get(i).getDeliverCode();//把循环外的值换为当前数据
			positnCode=disList.get(i).getPositnCode();  //把循环外的值换为当前数据
		}
		//最后处理  list大小不为0  时候  在作为一条入库单据加入
		if(list.size()!=0){
			saveChkinmzb(acct,accountName,year,month,list,maded);
		}
	}
	
	/***
	 * 新的保存直发单的方法
	 * @author wjf
	 * @param acct
	 * @param accountName
	 * @param year
	 * @param month
	 * @param list
	 */
	private void saveChkinmzb(String acct, String accountName, String year, int month, ArrayList<Dis> list,Date maded){
		Chkinm chkinm=new Chkinm();
		chkinm.setAcct(acct);
		chkinm.setYearr(year);
		chkinm.setChkinno(chkinmMapper.getMaxChkinno());
		chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB,maded));
		chkinm.setMaded(maded);
		chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
		Positn positn=new Positn();
		Deliver deliver=new Deliver();
		positn.setCode(list.get(0).getPositnCode());
		deliver.setCode(list.get(0).getDeliverCode());
		chkinm.setPositn(positn);
		chkinm.setDeliver(deliver);
		chkinm.setMadeby(accountName);
		chkinm.setTotalamt(list.get(0).getAmountin()*list.get(0).getPricein());
		chkinm.setInout("inout");//直发
		chkinm.setTyp("9916");
		chkinm.setMemo("报货直发");
		log.warn("添加直发单(主单)：\n"+chkinm);
 		chkinmMapper.saveChkinm(chkinm);
		for (int j = 0; j < list.size(); j++) {
			Dis dis=(Dis)list.get(j);
			Chkind chkind=new Chkind();//入库单从表
			chkind.setChkinno(chkinm.getChkinno());
			Supply supply=new Supply();
			supply.setSp_code(dis.getSp_code());
			chkind.setSupply(supply);
			chkind.setAmount(dis.getAmountin());//数量为采购数量 而非报货数量
			chkind.setPrice(dis.getPricein());
			chkind.setMemo(dis.getMemo());
			chkind.setDued(dis.getDued());
			chkind.setInout("inout");   //加入直发标志
			chkind.setPound(0);
			chkind.setSp_id(dis.getSp_id());
			chkind.setIndept(dis.getFirmCode());
			chkind.setChkstono(dis.getChkstoNo());
			chkind.setAmount1(dis.getAmount1in());//这里改为取amount1in  2014.12.30wjf 也能兼容以前版本
			chkind.setPcno(dis.getPcno());//保存批次号
			chkindMapper.saveChkind(chkind);
 			//修改申购单状态，设置入库和出库状态都为Y
			dis.setChkin("Y");
			dis.setChkout("Y");
			disMapper.updateByIds(dis);
		}
		//审核
		chkinm.setMonth(month);//会计月 
		chkinm.setChecby(accountName);
		chkinm.setStatus(chkinm.getInout());
		chkinm.setChk1memo("报货直发审核通过");
		chkinmMapper.checkChkinm(chkinm);//审核入库单
	}

	/***
	 * 部门采购
	 * @param dis
	 * @param page
	 * @return
	 */
	public List<Dis> findDeptDetail2(Dis dis, Page page) {
		if (page==null) {
			return disFenBoMapper.findDeptDetail2(dis);
		}else {
			return pageManager.selectPage(dis, page, DisFenBoMapper.class.getName()+".findDeptDetail2");
		}
	}
	
	/**
	 * 报货分拨生成采购订单
	 * @param session
	 * @param dis
	 * @return
	 */
	public String savePuprOrder(HttpSession session,PuprOrderd puprOrderd)throws CRUDException {   
		try {
			String acct = session.getAttribute("ChoiceAcct").toString();
			String accountId = session.getAttribute("accountId").toString();
			String purmorg = "";
			Map<String,Map<String,PuprOrder>> suppliermap = new HashMap<String,Map<String,PuprOrder>>();
			double money = 0.0;
			for (PuprOrderd p : puprOrderd.getListPuprOrderd()) {
				String delivercode = p.getDelivercode();
				String positncode = p.getPositncode();
				String dhopedate = p.getDhopedate(); 
				if(null==suppliermap.get(delivercode+"#"+dhopedate+"#"+positncode)){//供应商+到货日期
					Map<String,PuprOrder> orgmap = new HashMap<String,PuprOrder>();
					PuprOrder order = new PuprOrder();
					order.setPk_puprorder(CodeHelper.createUUID());
					order.setDelivercode(delivercode);
					order.setDelivername(p.getDelivername());
					order.setPositncode(positncode);
					order.setDhopedate(dhopedate);
					order.setPositnname(p.getPositnname());
					try {
						order.setVbillno(coderuleService.getCode(CoderuleConstants.PURTEMPLET,acct));
					} catch (Exception e) {
						e.printStackTrace();
					}
					order.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
					order.setDmaketime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
					order.setPk_account(accountId);
					order.setIstate(1);
					order.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
					List<PuprOrderd> dlist = new ArrayList<PuprOrderd>();
					PuprOrderd orderd = new PuprOrderd();
					orderd.setPk_puprorder(order.getPk_puprorder());
					orderd.setPk_puprorderd(CodeHelper.createUUID());
					orderd.setNcnt(p.getNcnt());
					orderd.setSp_price(p.getSp_price());
					
//					//**************先取合约价******************
//					Material material = new Material();
//					material.setAcct(acct);
//					material.setSp_code(p.getSp_code());
//					material.setDelivercode(p.getDelivercode());
//					material.setPositncode(p.getPositncode());
//					material.setDdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
//					Material result1 = materialMapper.findMaterialNprice(material);
//					if (result1 != null && result1.getSp_price() != null && 
//							!"".equals(result1.getSp_price()) && result1.getSp_price() > 0) {
//						p.setSp_price(result1.getSp_price());
//						p.setNmoney(p.getNcnt() * result1.getSp_price());
//					//****************end********************
//						
//					//***************再取供应物资范围的价格/*********
//					} else {
//						Material result2 = materialMapper.findMaterialNpricesupplier(material);
//						if (result2 != null && result2.getSp_price() != null && 
//								!"".equals(result2.getSp_price()) && result2.getSp_price() > 0) {
//							p.setSp_price(result2.getSp_price());
//							p.setNmoney(p.getNcnt() * result2.getSp_price());
//					//*****************end*******************
//							
//					//*****************标准价******************
//						} else {
//							Supply supply = new Supply();
//							supply.setAcct(acct);
//							supply.setSp_code(p.getSp_code());
//							Supply sup = supplyMapper.findSupplyById(supply);
//							if (sup != null && sup.getSp_price() != null 
//									&& !"".equals(sup.getSp_price()) && sup.getSp_price() > 0) {
//								p.setSp_price(sup.getSp_price());
//								p.setNmoney(p.getNcnt() * sup.getSp_price());
//							}
//						}
//					}
					money += p.getNcnt() * orderd.getSp_price();
					orderd.setNmoney(p.getNcnt() * orderd.getSp_price());
					orderd.setAcct(acct);
					orderd.setPk_material(p.getSp_code());
					orderd.setPk_unit(p.getUnit());
					orderd.setUnit(p.getUnit());
					orderd.setSp_desc(p.getSp_desc());
					orderd.setSp_code(p.getSp_code());
					orderd.setSp_name(p.getSp_name());
					orderd.setPositncode(p.getPositncode());
					orderd.setPositnname(p.getPositnname());
					orderd.setVmemo(p.getVmemo());
					orderd.setBeditprice(p.getBeditprice());
					dlist.add(orderd);
					order.setNmoney(money);
					order.setPuprOrderdList(dlist);
					purmorg = order.getPositncode();
					orgmap.put(purmorg, order);
					suppliermap.put(delivercode+"#"+dhopedate+"#"+positncode, orgmap);
				}else{
					Map<String,PuprOrder> orgmap = suppliermap.get(delivercode+"#"+dhopedate+"#"+positncode);
					PuprOrder order = orgmap.get(purmorg);
					order.setNmoney(order.getNmoney());
					List<PuprOrderd> dlist = order.getPuprOrderdList();
					PuprOrderd orderd = new PuprOrderd();
					orderd.setPk_puprorder(order.getPk_puprorder());
					orderd.setPk_puprorderd(CodeHelper.createUUID());
					orderd.setNcnt(p.getNcnt());
					
//					//**************先取合约价******************
//					Material material = new Material();
//					material.setAcct(acct);
//					material.setSp_code(p.getSp_code());
//					material.setDelivercode(p.getDelivercode());
//					material.setPositncode(p.getPositncode());
//					material.setDdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
//					Material result1 = materialMapper.findMaterialNprice(material);
//					if (result1 != null && result1.getSp_price() != null && 
//							!"".equals(result1.getSp_price()) && result1.getSp_price() > 0) {
//						p.setSp_price(result1.getSp_price());
//						p.setNmoney(p.getNcnt() * result1.getSp_price());
//					//****************end********************
//						
//					//***************再取供应物资范围的价格/*********
//					} else {
//						Material result2 = materialMapper.findMaterialNpricesupplier(material);
//						if (result2 != null && result2.getSp_price() != null && 
//								!"".equals(result2.getSp_price()) && result2.getSp_price() > 0) {
//							p.setSp_price(result2.getSp_price());
//							p.setNmoney(p.getNcnt() * result2.getSp_price());
//					//*****************end*******************
//							
//					//*****************标准价******************
//						} else {
//							Supply supply = new Supply();
//							supply.setAcct(acct);
//							supply.setSp_code(p.getSp_code());
//							Supply sup = supplyMapper.findSupplyById(supply);
//							if (sup != null && sup.getSp_price() != null 
//									&& !"".equals(sup.getSp_price()) && sup.getSp_price() > 0) {
//								p.setSp_price(sup.getSp_price());
//								p.setNmoney(p.getNcnt() * sup.getSp_price());
//							}
//						}
//					}
					money += p.getNcnt() * orderd.getSp_price();
					orderd.setNmoney(p.getNcnt() * orderd.getSp_price());
					orderd.setAcct(acct);
					orderd.setPk_material(p.getSp_code());
					orderd.setUnit(p.getUnit());
					orderd.setPk_unit(p.getUnit());
					orderd.setSp_desc(p.getSp_desc());
					orderd.setSp_code(p.getSp_code());
					orderd.setSp_name(p.getSp_name());
					orderd.setPositncode(p.getPositncode());
					orderd.setPositnname(p.getPositnname());
					orderd.setVmemo(p.getVmemo());
					dlist.add(orderd);
					order.setNmoney(money);
					order.setPuprOrderdList(dlist);
					orgmap.put(purmorg, order);
					suppliermap.put(delivercode+"#"+dhopedate+"#"+positncode, orgmap);
				}
			}
			
			for(String key:suppliermap.keySet()){
				Map<String,PuprOrder> orgmap = suppliermap.get(key);
				for(String pk_org:orgmap.keySet()){
					PuprOrder order = orgmap.get(pk_org);
					order.setAcct(acct);
					puprorderMapper.savePuprOrderm(order);
					for(PuprOrderd orderd:order.getPuprOrderdList()){
						puprorderMapper.savePuprOrderd(orderd);
					}
				}
			}
//			List<Dis> listStr = new ArrayList<Dis>();
//			if (puprOrderd.getListPuprOrderd().size() > 0) {
//				for (PuprOrderd p : puprOrderd.getListPuprOrderd()) {
//					Dis dis = new Dis();
//					dis.setDeliverCode(p.getDelivercode());
//					dis.setDeliverDes(p.getDelivername());
//					dis.setPositnDes(p.getPositnname());
//					dis.setHoped(p.getDhopedate());
//					listStr.add(dis);
//				}
//				List<PuprOrder> listPuprOrder = new ArrayList<PuprOrder>();
//				for (Dis dis : listStr) {
//					PuprOrder puprOrder = new PuprOrder();
//					puprOrder.setAcct(acct);//企业号
//					puprOrder.setPk_puprorder(CodeHelper.createUUID());//订单主表主键
//					puprOrder.setVbillno(puprOrderService.selectMaxVbillno(puprOrder,acct));//订单单据号
//					puprOrder.setDelivercode(dis.getDeliverCode());//供应商编码
//					puprOrder.setDelivername(dis.getDeliverDes());//供应商名称
//					puprOrder.setPositncode(dis.getPositnCode());//采购组织编码
//					puprOrder.setPositnname(dis.getPositnDes());//采购组织名称
//					puprOrder.setIstate(1);//单据状态 1未提交 2待发货 3已发货 4已到货 5已验货 6已入库 7已结算   8待备货  9备货中 10备货完成  -1已撤销 100订单补录
//					puprOrder.setPk_account(session.getAttribute("accountId").toString());////制单人ID
//					puprOrder.setDmaketime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));//制单时间
//					puprOrder.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));//单据日期
//					puprOrder.setDhopedate(dis.getHoped());//期望到货日
//					List<PuprOrderd> list = new ArrayList<PuprOrderd>();
//					for (PuprOrderd p : puprOrderd.getListPuprOrderd()) {
//						if (dis.getDeliverCode().equals(p.getDelivercode())) {
//							//**************先取合约价******************
//							Material material = new Material();
//							material.setAcct(acct);
//							material.setSp_code(p.getSp_code());
//							material.setDelivercode(p.getDelivercode());
//							material.setPositncode(p.getPositncode());
//							material.setDdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
//							Material result1 = materialMapper.findMaterialNprice(material);
//							if (result1 != null && result1.getSp_price() != null && 
//									!"".equals(result1.getSp_price()) && result1.getSp_price() > 0) {
//								p.setSp_price(result1.getSp_price());
//								p.setNmoney(p.getNcnt() * result1.getSp_price());
//							//****************end********************
//								
//							//***************再取供应物资范围的价格/*********
//							} else {
//								Material result2 = materialMapper.findMaterialNpricesupplier(material);
//								if (result2 != null && result2.getSp_price() != null && 
//										!"".equals(result2.getSp_price()) && result2.getSp_price() > 0) {
//									p.setSp_price(result2.getSp_price());
//									p.setNmoney(p.getNcnt() * result2.getSp_price());
//							//*****************end*******************
//									
//							//*****************标准价******************
//								} else {
//									Supply supply = new Supply();
//									supply.setAcct(acct);
//									supply.setSp_code(p.getSp_code());
//									Supply sup = supplyMapper.findSupplyById(supply);
//									if (sup != null && sup.getSp_price() != null 
//											&& !"".equals(sup.getSp_price()) && sup.getSp_price() > 0) {
//										p.setSp_price(sup.getSp_price());
//										p.setNmoney(p.getNcnt() * sup.getSp_price());
//									}
//								}
//							}
//							list.add(p);
//						}
//					}
//					puprOrder.setPuprOrderdList(list);
//					listPuprOrder.add(puprOrder);
//				}
//				for (PuprOrder puprOrder : listPuprOrder) {
//					puprOrderService.savePuprOrder(puprOrder, acct);
//				}
//				return "ok";
//			}
			return "ok";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 更新最新报价
	 * @param dis
	 * @return
	 */
	public int updateLastPrice(Dis dis) throws CRUDException{
		try{
			dis.setInCondition(OracleUtil.getOraInSql("d.id", dis.getId()));
			return disFenBoMapper.updateLastPrice(dis);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货分拨供应商修改
	 */
	public String findDeliverTyp(Deliver deliver) throws CRUDException{
		Map<String,Object> map=new HashMap<String, Object>();
		try {
			//报货分拨修改供应商
			Deliver newDel = deliverMapper.findDeliverByCode(deliver);
			if(null != newDel){
				//查这个门店这个方向对应的供应商
				List<Deliver> deliverList = null;
				Map<String,Object> content = new HashMap<String, Object>();
				content.put("firm", deliver.getFirm());
				content.put("inout", deliver.getInout());
				if ("2".equals(deliver.getInout())) {//现在改为出库的不能修改供应商  入库的可以改 这里2代表查入库供应商 默认查所有供应商2015.1.8wjf
					deliverList = deliverMapper.findDeliverByOut(content);
				}else{
					deliverList = deliverMapper.findDeliverByInout(content);
				}
				map.put("sta", "2");//不能修改
				for(Deliver d : deliverList){
					if(deliver.getCode().equals(d.getCode())){//如果存在
						map.put("sta", "0");
						map.put("deliver", newDel);
						break;
					}
				}
			}else{
				return null;
			}
			JSONObject rs = JSONObject.fromObject(map);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
