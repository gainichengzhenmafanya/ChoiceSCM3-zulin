package com.choice.scm.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.LeiBieChengBenMapper;

@Service
public class LeiBieChengBenService {

	@Autowired
	private LeiBieChengBenMapper leiBieChengBenMapper;
	@Autowired
	private ReportObject<Map<String, Object>> mapReportObject;
	@Autowired
	private PageManager<Map<String, Object>> mapPageManager;

	/**
	 * 类别成本分析报表
	 * 
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String, Object>> findLeiBieChengBen(Map<String, Object> conditions, Page pager) {
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		if (null != supplyAcct && !"".equals(supplyAcct)) {
			if (null != supplyAcct.getPositn() && !"".equals(supplyAcct.getPositn())&&!supplyAcct.getPositn().startsWith("'")) {
				supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
			}
			if (null != supplyAcct.getGrptyp() && !"".equals(supplyAcct.getGrptyp())&&!supplyAcct.getGrptyp().startsWith("'")) {
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			conditions.put("supplyAcct", supplyAcct);
		}
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, LeiBieChengBenMapper.class.getName() + ".findLeiBieChengBen"));
		List<Map<String, Object>> foot = leiBieChengBenMapper.findLeiBieChengBenSum(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
}