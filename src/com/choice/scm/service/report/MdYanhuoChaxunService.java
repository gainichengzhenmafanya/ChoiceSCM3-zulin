package com.choice.scm.service.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.MdYanhuoChaxunMapper;

/**
 * 门店验货查询
 * 
 * @author jinshuai
 * 20160316
 */
@Service
public class MdYanhuoChaxunService {

	@Autowired
	private MdYanhuoChaxunMapper mdYanhuoChaxunMapper;
	@Autowired
	private ReportObject<Map<String, Object>> mapReportObject;

	/**
	 * 门店验货查询
	 * 返回数据
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> findYanhuoChaxunDatas(SupplyAcct conditions, Page pager) throws CRUDException {
		try {
			//时间
			Calendar calendar = Calendar.getInstance();
			if (null != conditions.getPositn() && !"".equals(conditions.getPositn())) {
				conditions.setPositn(CodeHelper.replaceCode(conditions.getPositn()));
			}
			StringBuffer sql = new StringBuffer();
			StringBuffer str = new StringBuffer();
			Calendar ca = Calendar.getInstance();
			if (null == conditions.getBdat() || "".equals(conditions.getBdat())) {
				conditions.setBdat(ca.getTime());
			}
			if (null == conditions.getEdat() || "".equals(conditions.getEdat())) {
				conditions.setEdat(ca.getTime());
			}
			long bdat = conditions.getBdat().getTime();
		
			for (; bdat <= conditions.getEdat().getTime(); bdat += 24 * 60 * 60 * 1000) {
				calendar.setTimeInMillis(bdat);
				String timeStr = getAllLengthDate(calendar);
				str.append("," + '`' + timeStr + '`');
				
			}
			sql.append(str + " FROM( ");
			List<String> listDat = Arrays.asList(str.toString().split(","));
			int bill = conditions.getBill();
			
			//适应不同的数据库
			StringBuilder sba = new StringBuilder();
			sba.append("        CONCAT(count(case ");
			sba.append("              when t.chkyh = 'Y' then ");
			sba.append("               '1' ");
			sba.append("              else ");
			sba.append("               '' ");
			sba.append("            end) , ':' , count(case ");
			sba.append("                                 when t.chkyh = 'N' then ");
			sba.append("                                  '1' ");
			sba.append("                                 else ");
			sba.append("                                  '' ");
			sba.append("                               end)) ");

			
			// 直配 查直配到某个门店的
			if (bill == 0) {
				sql.append("select P.CODE AS firmcode,P.DES AS firmdes FROM positn p WHERE p.TYP in ('1203') ) pa ");
				for (int i = 1; i < listDat.size(); i++) {
					sql.append(" left join ");
					sql.append(" (select t.firm, ");
					sql.append(sba.toString());
					sql.append(" as " + listDat.get(i) + " ");
					sql.append("   from (select t1.*, t2.firm from chkstod t1 left join chkstom t2 on t1.chkstono = t2.chkstono) t ");
					sql.append("  where t.inout = 'dire' ");
					sql.append("    and t.ind = STR_TO_DATE('" + listDat.get(i).replaceAll("\"", "") + "','%Y-%m-%d') ");
					sql.append("  group by t.firm) a_" + i + " on pa.firmcode = a_" + i + ".firm");
				}
			}
			// 配送（总部基地仓库或者物流中心送到门店的） 查配送到某个门店的
			else if (bill == 1) {
				sql.append("select P.CODE AS firmcode,P.DES AS firmdes FROM positn p WHERE p.TYP in ('1203') ) pa ");
				for (int i = 1; i < listDat.size(); i++) {
					sql.append(" left join ");
					sql.append(" (select t.firm, ");
					sql.append(sba.toString());
					sql.append(" as " + listDat.get(i) + " ");
					sql.append("   from supplyacct t ");
					sql.append("  where t.positn in ");
					sql.append("        (select t.code from positn t where t.typ in ('1201', '1202')) ");
					sql.append("    and t.firm in (select t.code from positn t where t.typ = '1203') ");
					sql.append("    and t.cntout != 0 ");
					sql.append("    and (t.chktyp not in('9913','9914','9921','9919','9920') or t.chktyp is null) ");
					sql.append("    and t.dat = STR_TO_DATE('" + listDat.get(i).replaceAll("\"", "") + "','%Y-%m-%d') ");
					sql.append("  group by t.firm) a_" + i + " on pa.firmcode = a_" + i + ".firm");
				}
			}
			// 调拨
			// 为门店或者档口 调到 门店
			// typ为1203门店 typ为1207为档口 查调拨到某个门店的
			else if (bill == 2) {
				sql.append("select P.CODE AS firmcode,P.DES AS firmdes FROM positn p WHERE p.TYP in ('1203') ) pa ");
				for (int i = 1; i < listDat.size(); i++) {
					sql.append(" left join ");
					sql.append(" (select t.firm, ");
					sql.append(sba.toString());
					sql.append(" as " + listDat.get(i) + " ");
					sql.append("   from supplyacct t ");
					sql.append("  where t.positn in ");
					sql.append("        (select t.code from positn t where t.typ in('1203','1207')) ");
					sql.append("    and t.firm in (select t.code from positn t where t.typ = '1203') ");
					sql.append("    and t.cntin < 0 ");
					//类型 调拨出库
					sql.append("    and t.chktyp = '9909' ");
					sql.append("    and t.dat = STR_TO_DATE('" + listDat.get(i).replaceAll("\"", "") + "','%Y-%m-%d') ");
					sql.append("  group by t.firm) a_" + i + " on pa.firmcode = a_" + i + ".firm");
				}
			} else {
				return mapReportObject;
			}

			conditions.setSql(sql.toString());
			List<Map<String, Object>> list = mdYanhuoChaxunMapper.findYanhuoChaxunDatas(conditions);
			mapReportObject.setRows(list);
			mapReportObject.setTotal(list.size());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 动态生成表头
	 * 
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object findFreetimeHeader(SupplyAcct condition) throws CRUDException {
		Calendar calendar = Calendar.getInstance();

		try {
			List<String> str = new ArrayList<String>();
			Calendar ca = Calendar.getInstance();
			if (null == condition.getBdat() || "".equals(condition.getBdat())) {
				condition.setBdat(ca.getTime());
			}
			if (null == condition.getEdat() || "".equals(condition.getEdat())) {
				condition.setEdat(ca.getTime());
			}
			long bdat = condition.getBdat().getTime();
			for (; bdat <= condition.getEdat().getTime(); bdat += 24 * 60 * 60 * 1000) {
				calendar.setTimeInMillis(bdat);
				String timeStr = getAllLengthDate(calendar);
				str.add(timeStr);
			}
			return str;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 得到日期yyyy-MM-dd
	 * 
	 * @param cal
	 * @return
	 */
	public static String getAllLengthDate(Calendar cal) {
		int month = cal.get(Calendar.MONTH) + 1;
		String monthstr = month < 10 ? "0" + month : String.valueOf(month);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		String daystr = day < 10 ? "0" + day : String.valueOf(day);
		int year = cal.get(Calendar.YEAR);
		return year + "-" + monthstr + "-" + daystr;
	}
}
