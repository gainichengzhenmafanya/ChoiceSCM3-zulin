package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class LeibieGysHuizongService {

	@Autowired
	private SupplyAcctMapper leibieGysHuizongMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;

		/**
		 * 类别供应商汇总
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findCategoryDeliverSum(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				//判断供应商
				if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
					sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
				}
				//判断仓位
				if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
					sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
				}
				List<Map<String,Object>> foot = leibieGysHuizongMapper.findCalForCategoryDeliverSum(conditions);
				mapReportObject.setRows(leibieGysHuizongMapper.findCategoryDeliverSum(conditions));
				mapReportObject.setFooter(foot);
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
		/**
		 * 类别供应商汇总1
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findCategoryDeliverSum1(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				//判断供应商
				if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
					sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
				}
				//判断仓位
				if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
					sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
				}
				List<Map<String,Object>> foot = leibieGysHuizongMapper.findCalForCategoryDeliverSum1(conditions);
				mapReportObject.setRows(leibieGysHuizongMapper.findCategoryDeliverSum1(conditions));
				mapReportObject.setFooter(foot);
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
}
