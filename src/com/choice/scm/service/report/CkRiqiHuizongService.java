package com.choice.scm.service.report;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class CkRiqiHuizongService {

	@Autowired
	private SupplyAcctMapper ckRiqiHuizongMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private PositnMapper positnMapper;
	
	
	/**
	 * 出库日期汇总
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkoutDateSum(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			conditions.put("jmj", 0);
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			//判断仓位
			if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			//判断领用仓位
			if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			conditions.put("supplyAcct", sa);
			List<Map<String,Object>> foot=null;
			if(null != conditions.get("bysale") && conditions.get("bysale").equals("true")){
				reportObject.setRows(ckRiqiHuizongMapper.findChkoutDSBySale(conditions));
				foot = ckRiqiHuizongMapper.findCalForChkoutDSBySale(conditions);
			}else{
				reportObject.setRows(ckRiqiHuizongMapper.findChkoutDateSum(conditions));
				foot = ckRiqiHuizongMapper.findCalForChkoutDate(conditions);
			}
			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	//导出出库日期汇总报表
	public boolean exportChkoutDateSum(OutputStream os,Map<String,Object> conditions,Page pager) throws CRUDException{
		SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
		sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
		WritableWorkbook workBook = null;
		try{
			//记录表头位置
			Map<String,Integer> position = new HashMap<String,Integer>();
			reportObject = findChkoutDateSum(conditions,pager);
			List<Positn> headers = positnMapper.findPositn(null);
			List<SupplyAcct> rows = reportObject.getRows();
			List<Map<String,Object>> foot = reportObject.getFooter();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("出库日期汇总", 0);
			//定义label
			Label label = new Label(0,0,"出库日期汇总");
			//添加label到cell
            sheet.addCell(label);
            //设置表格表头
            Label headfirst = new Label(0,1,"项目");
            sheet.addCell(headfirst);
            Label headsecond = new Label(1,1,"合计");
            sheet.addCell(headsecond);
            for(int i = 2 ; i < headers.size()+2;i ++){
            	Label head = new Label(i,1,headers.get(i-2).getDes());
            	position.put(headers.get(i-2).getDes(), i);
            	sheet.addCell(head);
            }
            sheet.mergeCells(0, 0, headers.size()+1, 0);
            //遍历list填充表格内容
            long curDate = 0;
            double total = 0;
            //解决导出报表显示问题乱行，多行问题 wjf 2014.7.8
            int j = 1;
            for(int i = 0 ; i < rows.size() ; i ++ ){
        		long innerDate = rows.get(i).getDat().getTime();
        		if(curDate != innerDate){
    				j++;
    				total = 0;
    			}
        		Label project = new Label(0,j,new SimpleDateFormat("yyyy-MM-dd").format(rows.get(i).getDat()));
        		sheet.addCell(project);
        		total += rows.get(i).getAmtout();
        		Label totalLab = new Label(1,j,String.valueOf(total));
    			sheet.addCell(totalLab);
    			
    			Label lab = new Label(position.get(rows.get(i).getFirmdes()),j,String.valueOf(rows.get(i).getAmtout()));
    			sheet.addCell(lab);
    			curDate = innerDate;
            }
//            Label totalLab = new Label(1,j,String.valueOf(total));
//			sheet.addCell(totalLab);
            //填充合计
            j++;
            Label project = new Label(0,j,"合计");
    		sheet.addCell(project);
    		for(int i = 0 ; i < foot.size() ; i ++ ){
    			Label lab = new Label(1,j,String.valueOf((new BigDecimal(String.valueOf(foot.get(i).get("AMTOUT")))).doubleValue()));
    			sheet.addCell(lab);
    		}
            workBook.write();
            os.flush();
		}catch (Exception e) {
			throw new CRUDException(e);
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	/**
	 * 出库日期汇总1
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkoutDateSum1(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			conditions.put("jmj", 0);
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			//判断仓位
			if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			//判断领用仓位
			if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			conditions.put("supplyAcct", sa);
//			if(null != conditions.get("bysale") && conditions.get("bysale").equals("true")){//加按售价查询  wjf2014.11.26
//				reportObject.setFooter(ckRiqiHuizongMapper.findCalForChkoutDSBySale(conditions));
//			}else{
				reportObject.setFooter(ckRiqiHuizongMapper.findChkoutDateSumHJ1(conditions));
//			}
			reportObject.setRows(ckRiqiHuizongMapper.findChkoutDateSum1(conditions));
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 出库日期汇总2
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkoutDateSum2(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			conditions.put("jmj", 0);
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			//判断仓位
			if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			//判断领用仓位
			if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			conditions.put("supplyAcct", sa);
//			List<Map<String,Object>> foot = ckRiqiHuizongMapper.findCalForChkoutDate(conditions);
//			if(null != conditions.get("bysale") && conditions.get("bysale").equals("true")){//加按售价查询  wjf2014.11.26
//				reportObject.setFooter(ckRiqiHuizongMapper.findCalForChkoutDSBySale(conditions));
//			}else{
				reportObject.setFooter(ckRiqiHuizongMapper.findChkoutDateSumHJ1(conditions));
//			}
			reportObject.setRows(ckRiqiHuizongMapper.findChkoutDateSum2(conditions));
//			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
