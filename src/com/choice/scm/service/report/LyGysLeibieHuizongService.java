package com.choice.scm.service.report;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
import com.choice.scm.service.DeliverService;

/**
 * 领用供应商类别汇总
 * 
 * @author Administrator
 * 
 */
@Service
public class LyGysLeibieHuizongService {

	@Autowired
	private SupplyAcctMapper lyGysLeibieHuizongMapper;
	@Autowired
	private DeliverService deliverService;

	/**
	 * 领用供应商类别汇总报表
	 * 
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<?> lyGysLeibieHuizongDetail(Map<String, Object> conditions, Page page) throws CRUDException {
		try {
			SupplyAcct supplyAcct = ((SupplyAcct) conditions.get("supplyAcct"));
			// 判断供应商
			if (null != supplyAcct && null != supplyAcct.getDelivercode() && !"".equals(supplyAcct.getDelivercode()) && !supplyAcct.getDelivercode().startsWith("'")) {
				supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
			}
			// 单据类型
			if (null != supplyAcct && null != supplyAcct.getChktyp() && !"".equals(supplyAcct.getChktyp()) && !supplyAcct.getChktyp().startsWith("'")) {
				supplyAcct.setChktyp(CodeHelper.replaceCode(supplyAcct.getChktyp()));
			}
			// 判断领用仓位
			if (null != supplyAcct && null != supplyAcct.getAcceptPositn() && !"".equals(supplyAcct.getAcceptPositn()) && !supplyAcct.getAcceptPositn().startsWith("'")) {
				supplyAcct.setAcceptPositn(CodeHelper.replaceCode(supplyAcct.getAcceptPositn()));
			}
			// 判断仓位
			if (null != supplyAcct && null != supplyAcct.getPositn() && !"".equals(supplyAcct.getPositn()) && !supplyAcct.getPositn().startsWith("'")) {
				supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
			}
			// 大类
			if (null != supplyAcct && null != supplyAcct.getGrptyp() && !"".equals(supplyAcct.getGrptyp()) && !supplyAcct.getGrptyp().startsWith("'")) {
				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			}
			// 中类
			if (null != supplyAcct && null != supplyAcct.getGrp() && !"".equals(supplyAcct.getGrp()) && !supplyAcct.getGrp().startsWith("'")) {
				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
			}
			// 拼装供应商
			StringBuilder sb = new StringBuilder();
			//如果供应商太多，oracle会报错
			//所以分开执行，每30个供应商执行一次
			List<String> pjLists = new ArrayList<String>();
			//供应商信息
			String inDeliverCodes = "";
			// 是否选择了供应商
			String deliverCodes = supplyAcct.getDelivercode();
			if (deliverCodes == null || deliverCodes.trim().equals("") || deliverCodes.split(",").length == 0) {
				// 供应商
				// 得到真正的供应商
				Deliver deliver = new Deliver();
				// deliver.setIsReal("true");
				List<Deliver> delivers = deliverService.findAllDelivers(deliver);
				for (int i = 0, len = delivers.size(); i < len; i++) {
					if(i%20==0&&i!=0){
						pjLists.add(sb.toString());
						sb = new StringBuilder();
					}
					Deliver d = delivers.get(i);
					String deliverCode = d.getCode();
					sb.append(",ROUND(SUM(CASE WHEN T.DELIVERCODE = '" + deliverCode + "' THEN T.AMTIN ELSE 0 END),2) AS D_" + deliverCode + "");
					inDeliverCodes += (i==0?"":",")+d.getCode();
				}
			} else {
				inDeliverCodes = deliverCodes;
				String[] dcos = deliverCodes.split(",");
				for (int i = 0, len = dcos.length; i < len; i++) {
					if(i%20==0&&i!=0){
						pjLists.add(sb.toString());
						sb = new StringBuilder();
					}
					String dcd = dcos[i].replace("'", "").trim();
					sb.append(",ROUND(SUM(CASE WHEN T.DELIVERCODE = '" + dcd + "' THEN T.AMTIN ELSE 0 END),2) AS D_" + dcd + "");
				}
			}
			
			//把最后一个加上
			if(!"".equals(sb.toString())){
				pjLists.add(sb.toString());
			}
			
			if(inDeliverCodes!=null&&!inDeliverCodes.startsWith("'")){
				inDeliverCodes = CodeHelper.replaceCode(inDeliverCodes);
			}
			
			//设置供应商
			supplyAcct.setDelivercode(inDeliverCodes);
			
			//返回值
			List<Object> returnLists = new ArrayList<Object>();
			List<Map<String, Object>> allRows = null;
			List<Map<String, Object>> allRowsFooter = null;
			
			//20个以下
			if(pjLists.size()==1){
				// 分店信息
				conditions.put("firmSumStr", pjLists.get(0));
				allRows = lyGysLeibieHuizongMapper.lyGysLeibieHuizongDetail(conditions);
				allRowsFooter =lyGysLeibieHuizongMapper.lyGysLeibieHuizongSum(conditions);
			}else if(pjLists.size()>1){
				//所有数据
				conditions.put("firmSumStr", pjLists.get(0));
				allRows = lyGysLeibieHuizongMapper.lyGysLeibieHuizongDetail(conditions);
				//分多次执行查询
				for(int i=1,len=pjLists.size();i<len;i++){
					String sumStr = pjLists.get(i);
					conditions.put("firmSumStr", sumStr);
					List<Map<String, Object>> rowQt = lyGysLeibieHuizongMapper.lyGysLeibieHuizongDetail(conditions);
					for(int t1=0,lent1=allRows.size();t1<lent1;t1++){
						Map<String, Object> allRowsMap = allRows.get(t1);
						String type1 = String.valueOf(allRowsMap.get("TYP"));
						for(int t2=0,lent2=rowQt.size();t2<lent2;t2++){
							Map<String, Object> rowQtMap = rowQt.get(t2);
							String type2 = String.valueOf(rowQtMap.get("TYP"));
							if(type1.equals(type2)){
								allRowsMap.putAll(rowQtMap);
								break;
							}
						}
					}
				}
				
				//合计
				conditions.put("firmSumStr", pjLists.get(0));
				allRowsFooter = lyGysLeibieHuizongMapper.lyGysLeibieHuizongSum(conditions);
				//分多次执行查询
				for(int i=1,len=pjLists.size();i<len;i++){
					String sumStr = pjLists.get(i);
					conditions.put("firmSumStr", sumStr);
					List<Map<String, Object>> rowQt = lyGysLeibieHuizongMapper.lyGysLeibieHuizongSum(conditions);
					for(int t1=0,lent1=allRowsFooter.size();t1<lent1;t1++){
						Map<String, Object> allRowsMap = allRowsFooter.get(t1);
						if(allRowsMap==null){
							continue;
						}
						String type1 = String.valueOf(allRowsMap.get("TYP"));
						for(int t2=0,lent2=rowQt.size();t2<lent2;t2++){
							Map<String, Object> rowQtMap = rowQt.get(t2);
							String type2 = String.valueOf(rowQtMap.get("TYP"));
							if(type1.equals(type2)){
								allRowsMap.putAll(rowQtMap);
								break;
							}
						}
					}
				}
			}
			
			//向阳渔港，去掉供应商，每个分类物资都为0的供应商列
			//根据footer判断，如果合计为0，则去掉
			//剩下的供应商
			List<Deliver> sygys = new ArrayList<Deliver>();
			
			//排除的供应商code集合
			//List<String> pcgyscode = new ArrayList<String>();
			
			//合计只有一行
			if(allRowsFooter!=null){
				Map<String,Object> map = allRowsFooter.get(0);
				if(map!=null){
					Iterator<String> iter = map.keySet().iterator();
					while(iter.hasNext()){
						//判断供应商
						String key = iter.next();
						if(key!=null&&key.startsWith("D_")){
							String value = String.valueOf(map.get(key));
							if(!"0".equals(value)){
								//保存不为0的供应商
								String dcd = key.substring(2);
								Deliver btDeliver = new Deliver();
								btDeliver.setCode(dcd);
								btDeliver = deliverService.findDeliverByCode(btDeliver);
								btDeliver.setAlias("D_" + btDeliver.getCode());
								sygys.add(btDeliver);
							}else{
								//pcgyscode.add(key);
								//合计删除为0的供应商
								iter.remove();
							}
						}
					}
				}
			}
			
			//去掉rows中的为空的记录
//			for(int i=0,len=allRows.size();i<len;i++){
//				Map<String,Object> test = allRows.get(i);
//				Iterator<String> iter = test.keySet().iterator();
//				while(iter.hasNext()){
//					String key = iter.next();
//					for(int j=0,lenj=pcgyscode.size();j<lenj;j++){
//						String code = "D_"+pcgyscode.get(j);
//						if(key!=null&&key.equals(code)){
//							iter.remove();
//							break;
//						}
//					}
//				}
//			}
			
			returnLists.add(allRows);
			returnLists.add(allRowsFooter);
			returnLists.add(sygys);
			return returnLists;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}