package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;

@Service
public class RkMingxiChaxunService {

	@Autowired
	private SupplyAcctMapper rkMingxiChaxunMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private PageManager<SupplyAcct> pageManager;
	
	/**
	 * 入库明细查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkinDetailS(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			//判断供应商
			if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
				sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
			}
			//判断仓位
			if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			//判断领用仓位
			if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			//判断单据类型
			if(null != sa && null != sa.getChktyp() && !"".equals(sa.getChktyp())){
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			}
			//备注 （可有可无）
			if(null != sa && null != sa.getDes() && !"".equals(sa.getDes())){
				sa.setDes(CodeHelper.replaceCode(sa.getDes()));
			}
			List<Map<String,Object>> foot = rkMingxiChaxunMapper.findCal(conditions);
			reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findChkinDetailS"));
			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
