package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class WzCangkuJinchubiaoService {

	@Autowired
	private SupplyAcctMapper wzCangkuJinchubiaoMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;	
	@Autowired
	private AcctMapper acctMapper;

	/**
	 * 物资仓库进出表
	 * @return
	 */
	public ReportObject<Map<String,Object>> findGoodsStoreInout(Map<String,Object> conditions,Page pager){
		String withoutZero="";
		if(conditions.get("noZero")!=null && !conditions.get("noZero").equals("") && conditions.get("noZero").equals("noZero")){
			withoutZero = " AND (AMTBLA+SUBTOTAL!=0 OR AMTIN!=0 OR AMTOUT!=0 OR AMTBLA+SUBTOTAL+AMTIN-AMTOUT!=0)";
		}
		conditions.put("withoutZero", withoutZero);
		SupplyAcct sa = (SupplyAcct)conditions.get("supplyAcct");
		conditions.put("yearr", acctMapper.findYearrByDate(sa.getBdat()));
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findGoodsStoreInout"));
		List<Map<String,Object>> foot = wzCangkuJinchubiaoMapper.findCalForGoodsStoreInout(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
}
