package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class CkHuizongChaxunService {

	@Autowired
	private SupplyAcctMapper ckHuizongChaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 出库汇总查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findChkoutSumQuery(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			conditions.put("jmj", 0);
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			//解决因为in设置的错误
			if(sa!=null){
				//1仓位
				String positn = sa.getPositn();
				if(positn!=null&&!positn.equals("")&&!positn.startsWith("'")){
					sa.setPositn(CodeHelper.replaceCode(positn));
				}
				
				//2大类
				String grptyp = sa.getGrptyp();
				if(grptyp!=null&&!grptyp.equals("")&&!grptyp.startsWith("'")){
					sa.setGrptyp(CodeHelper.replaceCode(grptyp));
				}
				
				//3领用仓位
				String firm = sa.getFirm();
				if(firm!=null&&!firm.equals("")&&!firm.startsWith("'")){
					sa.setFirm(CodeHelper.replaceCode(firm));
				}
				
				//4中类
				String grp = sa.getGrp();
				if(grp!=null&&!grp.equals("")&&!grp.startsWith("'")){
					sa.setGrp(CodeHelper.replaceCode(grp));
				}
				
				//5小类
				String typ = sa.getTyp();
				if(typ!=null&&!typ.equals("")&&!typ.startsWith("'")){
					sa.setTyp(CodeHelper.replaceCode(typ));
				}
				
			}
			
			//对比合计和普通合计不相同
			if("true".equals(conditions.get("bycomp"))){
				conditions.put("comp", 1);
			}
			
			List<Map<String,Object>> foot = ckHuizongChaxunMapper.findCalForChkout(conditions);
			if("true".equals(conditions.get("bycomp")))
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findChkoutSQComp"));
			else
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findChkoutSumQuery"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
