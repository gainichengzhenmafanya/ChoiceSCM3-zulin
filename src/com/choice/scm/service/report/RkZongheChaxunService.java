package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;

@Service
public class RkZongheChaxunService {

	@Autowired
	private SupplyAcctMapper rkZongheChaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 入库综合查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findAllChkinmZh(Map<String,Object> conditions,Page page) throws CRUDException{
		try{			
			SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
			if(supplyAcct.getChktyp() != null && !"".equals(supplyAcct.getChktyp())){
				supplyAcct.setChktyp(CodeHelper.replaceCode(supplyAcct.getChktyp()));
			}
			if(supplyAcct.getPositn() != null && !supplyAcct.getPositn().equals("")){
				supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
			}
			if(supplyAcct.getDelivercode() != null && !supplyAcct.getDelivercode().equals("")){
				supplyAcct.setDelivercode(CodeHelper.replaceCode(supplyAcct.getDelivercode()));
			}
			List<Map<String,Object>> foot = rkZongheChaxunMapper.findAllChkinmSum(conditions);
			
			if(conditions.get("querytype").equals("3")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findAllChkinmTypSum"));
			}else if(conditions.get("querytype").equals("2")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findAllChkinmSumQuery"));
			}else if(conditions.get("querytype").equals("1")){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findAllChkinmDetailQuery"));
			}
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}	
}
