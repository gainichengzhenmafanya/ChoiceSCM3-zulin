package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.PositnSupply;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class WzYueChaxunService {

	@Autowired
	private SupplyAcctMapper wzYueChaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	/**
	 * 查询物资余额列表
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findSupplyBalance(Map<String,Object> content,Page pager) throws CRUDException{
		try {
			String startNumber="";//期初数量
			String startMoney="";//期初金额
			String inNumber="";//入库数量
			String inMoney="";//入库金额
			String outNumber="";//出库数量
			String outMoney="";//出库金额
			String inwhere = "";//查询条件
			int without0=0;//过滤零值标志：0-为选，1-已选
			String withoutzero="";//过滤零值条条件
			
			int month = Integer.parseInt(content.get("month").toString());
			if(content.get("without0")!=null && !content.get("without0").equals(""))
				without0=Integer.parseInt(content.get("without0").toString());
			if(without0==1){
				withoutzero = " AND (STARTNUMBER!=0 OR STARTMONEY!=0 OR INNUMBER!=0 OR INMONEY!=0 OR OUTNUMBER!=0 "+
				  "OR OUTMONEY!=0 OR (STARTNUMBER + INNUMBER - OUTNUMBER)!=0 OR (STARTMONEY + INMONEY - OUTMONEY)!=0)";
			}
			if(month>0){
				startNumber = "(";
				startMoney = "(";
				inwhere = "(";
				for(int i=0;i<month;i++){
					startNumber += "A.INC"+i+" - A.OUTC"+i+" +";
					startMoney += "A.INA"+i+" - A.OUTA"+i+" +";
				}
				inwhere = startNumber + "A.INC"+month+" - A.OUTC"+month + ") > 0";
				startNumber = startNumber.substring(0,startNumber.length()-1);
				startNumber += ")";
				startMoney = startMoney.substring(0, startMoney.length()-1);
				startMoney += ")";
				inNumber = "A.INC"+month+"";
				inMoney = "A.INA"+month+"";
				if ("2".equals(content.get("bill").toString())) {
					outNumber = "A.OUTC"+month+"";
					outMoney = "A.OUTA"+month+"";
				}else{
					outNumber = "A.COST"+month+"";
					outMoney = "A.COSTA"+month+"";
				}
			}
			content.put("startNumber",startNumber);
			content.put("startMoney",startMoney);
			content.put("inNumber",inNumber);
			content.put("inMoney",inMoney);
			content.put("outNumber",outNumber);
			content.put("outMoney",outMoney);
			content.put("inwhere",inwhere);
			content.put("withoutzero",withoutzero);
			List<Map<String,Object>> foot = wzYueChaxunMapper.findCalForSupplyBalance(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctMapper.class.getName()+".findSupplyBalance");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询物资余额列表物资库存
	 * @param content
	 * @return
	 */
	public Double findSupplyBalanceOnlyEndNum(SupplyAcct supplyAcct) throws CRUDException{
		try {
			String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
			supplyAcct.setYearr(year);
			Double cnt = wzYueChaxunMapper.findSupplyBalanceOnlyEndNum(supplyAcct);
			if(null != cnt){
				return cnt;
			}
			return 0.0;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 专门用来查询物资在某个仓位的余额情况 
	 * @param supplyAcct
	 * @return
	 */
	public PositnSupply findViewPositnSupply(SupplyAcct supplyAcct) {
		String year = mainInfoMapper.findYearrList().get(0);//会计年2015.1.5wjf
		supplyAcct.setYearr(year);
		return wzYueChaxunMapper.findViewPositnSupply(supplyAcct);
	}
}
