package com.choice.scm.service.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.MdPandianchaxunMapper;
/**
 * 门店盘点状态表
 * @author wjf
 */
@Service
public class MdPdhjZhuangtaiChaxunService {

	@Autowired
	private MdPandianchaxunMapper mdPandianchaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;

	/**
	 * 门店日盘点状态表
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException 
	 */
	@SuppressWarnings("deprecation")
	public ReportObject<Map<String,Object>> findMdPdhjState(SupplyAcct conditions,Page pager) throws CRUDException{
		try{
			if(null!=conditions.getPositn() && !"".equals(conditions.getPositn())){
				conditions.setPositn(CodeHelper.replaceCode(conditions.getPositn()));
			}
			StringBuffer sql = new StringBuffer();
			StringBuffer str = new StringBuffer();
			StringBuffer datStr = new StringBuffer();
			Calendar ca = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			if(null==conditions.getBdat() || "".equals(conditions.getBdat())){
				conditions.setBdat(ca.getTime());
			}
			if(null==conditions.getEdat() || "".equals(conditions.getEdat())){
				conditions.setEdat(ca.getTime());
			}
			long bdat=conditions.getBdat().getTime();
			for(;bdat<=conditions.getEdat().getTime();bdat+=24*60*60*1000){
				Date t = new Date(bdat);
				str.append(","+'"'+t.getDate()+'"');
				datStr.append(","+'"'+df.format(t)+'"');
			}
			sql.append(str+" FROM( ");
			List<String> listDat=Arrays.asList(str.toString().split(","));
			List<String> dat = Arrays.asList(datStr.toString().split(","));
			if(conditions.getBill() == 1){//核减
				sql.append("select P.CODE AS firmcode,P.DES AS firmdes FROM positn p WHERE p.TYP in ('1203') ) pa ");
			    for(int i=1;i<listDat.size();i++){
			    	sql.append("left join ");
			    	sql.append("(select firm AS firmcode,2 as "+listDat.get(i)+" FROM costitemspcode "); 
					sql.append(" WHERE DAT = STR_TO_DATE('"+dat.get(i).substring(1,dat.get(i).length()-1)+"','%Y-%m-%d') GROUP BY FIRM) a"+i+" on a"+i+".firmcode=pa.firmcode ");
			    }
			}else{
			    sql.append("select P.CODE AS firmcode,P.DES AS firmdes FROM positn p WHERE p.TYP in ('1203','1207') ) pa "); 
			    for(int i=1;i<listDat.size();i++){
			    	sql.append("left join ");
			    	sql.append("(select DEPT AS firmcode,SUM(STATE + 1) as "+listDat.get(i)+" FROM IMS_CHKSTOREF "); 
			    	sql.append(" WHERE WORKDATE='"+dat.get(i).substring(1,dat.get(i).length()-1)+"' GROUP BY DEPT) a"+i+" on a"+i+".firmcode=pa.firmcode ");
			    }
			}
		    conditions.setSql(sql.toString());
		    List<Map<String, Object>>  list = mdPandianchaxunMapper.findMdPdhjState(conditions);
		    mapReportObject.setRows(list);
			mapReportObject.setTotal(list.size());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 动态生成表头
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("deprecation")
	public Object findFreetimeHeader(SupplyAcct condition) throws CRUDException {
		try {
			List<Integer> str = new ArrayList<Integer>();
			Calendar ca = Calendar.getInstance();
			if(null==condition.getBdat() || "".equals(condition.getBdat())){
				condition.setBdat(ca.getTime());
			}
			if(null==condition.getEdat() || "".equals(condition.getEdat())){
				condition.setEdat(ca.getTime());
			}
			long bdat=condition.getBdat().getTime();
			for(;bdat<=condition.getEdat().getTime();bdat+=24*60*60*1000){
				Date t = new Date(bdat);
				str.add(t.getDate());
			}
			return str;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
