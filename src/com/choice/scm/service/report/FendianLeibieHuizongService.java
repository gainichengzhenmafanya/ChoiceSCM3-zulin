package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class FendianLeibieHuizongService {

	@Autowired
	private SupplyAcctMapper fendianLeibieHuizongMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;

	
		/**
		 * 分店类别查询
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findPositnCategorySum(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				//判断仓位
				if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
					sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
				}
				//判断领用仓位
				if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
					sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
				}
				List<Map<String,Object>> foot = fendianLeibieHuizongMapper.findCalForCategoryPositnSum(conditions);
				mapReportObject.setRows(fendianLeibieHuizongMapper.findPositnCategorySum(conditions));
				mapReportObject.setFooter(foot);
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}

}
