package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class JhDanjuHuizongService {

	@Autowired
	private SupplyAcctMapper jhDanjuHuizongMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
		/**
		 * 进货单据货总列表
		 * @param content
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findStockBillSum(Map<String,Object> content,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)content.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				List<Map<String,Object>> foot = jhDanjuHuizongMapper.findCalForStockBillSum(content);
				List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, SupplyAcctMapper.class.getName()+".findStackBillSum");
				mapReportObject.setRows(listInfo);
				mapReportObject.setFooter(foot);
				mapReportObject.setTotal(pager.getCount());
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
}
