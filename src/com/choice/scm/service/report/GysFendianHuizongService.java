package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class GysFendianHuizongService {

	@Autowired
	private SupplyAcctMapper gysFendianHuizongMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	 
		/**
		 * 供应商分店汇总
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findDeliverPositnSum(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				//判断供应商
				if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
					sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
				}
				//判断领用仓位
				if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
					sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
				}
				List<Map<String,Object>> foot = gysFendianHuizongMapper.findCalForDeliverPositnSum(conditions);
				mapReportObject.setRows(gysFendianHuizongMapper.findDeliverPositnSum(conditions));
				mapReportObject.setFooter(foot);
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
}
