package com.choice.scm.service.report;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.OracleUtil;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.GrpTypMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class CangkuLeibieJinchuService {

	@Autowired
	private SupplyAcctMapper cangkuLeibieJinchuMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private GrpTypMapper grpTypMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	
	/**
	 * 仓库类别进出表
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findStockCategoryInOut(Map<String,Object> conditions,Page pager) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			String yearr = mainInfoMapper.findYearrList().get(0);//会计年wjf
			conditions.put("yearr", yearr);
			
			//仓位超过1000，用in会出现问题
			if(sa!=null){
				String positnStr = sa.getPositn();
				if(positnStr!=null&&!"".equals(positnStr.trim())){
					//根据1000个分割in里面的条件个数
					String apjstr = OracleUtil.getOraInSql("A.POSITN", Arrays.asList(positnStr.split(",")));
					//设置查询条件
					conditions.put("apositnStr", apjstr);
				}
			}
			
			int month = Integer.parseInt(conditions.get("month").toString());
			StringBuffer bcount = new StringBuffer();
			StringBuffer bamount = new StringBuffer();
			StringBuffer ecount = new StringBuffer();
			StringBuffer eamount = new StringBuffer();
			for(int i = 0 ; i < month ; i ++){
				if(i != 0){
					bcount.append("+");
					bamount.append("+");
					ecount.append("+");
					eamount.append("+");
				}
				bcount.append("a.inc").append(i).append("-a.outc").append(i);
				bamount.append("a.ina").append(i).append("-a.outa").append(i);
				ecount.append("a.inc").append(i).append("-a.outc").append(i);
				eamount.append("a.ina").append(i).append("-a.outa").append(i);
			}
			ecount.append("+");
			eamount.append("+");
			ecount.append("a.inc").append(month).append("-a.outc").append(month);
			eamount.append("a.ina").append(month).append("-a.outa").append(month);
			conditions.put("bcount", bcount);
			conditions.put("bamount", bamount);
			conditions.put("ecount", ecount);
			conditions.put("eamount", eamount);
			conditions.put("incount", "a.inc"+month);
			conditions.put("inamount", "a.ina"+month);
			conditions.put("outcount", "a.outc"+month);
			conditions.put("outamount", "a.outa"+month);
			List<Map<String,Object>> foot = cangkuLeibieJinchuMapper.findCalForStockCategoryInOut(conditions);
			mapReportObject.setRows(cangkuLeibieJinchuMapper.findStockCategoryInOut(conditions));
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 仓库类别进出表导出
	 * @param os
	 * @param supplyAcct
	 * @return
	 * @throws Exception
	 */
	public boolean exportStockCategoryInOut(OutputStream os,Map<String,Object> condition) throws Exception{
		WritableWorkbook workBook = null;
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont,new NumberFormat("0.00"));//设定带小数数字单元格格式
		try{
			Page pager = new Page();
			pager.setPageSize(Integer.MAX_VALUE);
			//记录表头位置
			Map<String,Integer> position = new HashMap<String,Integer>();
			mapReportObject = findStockCategoryInOut(condition,pager);
			List<GrpTyp> headers = grpTypMapper.findAllGrpTypA(((SupplyAcct)condition.get("supplyAcct")).getAcct());
			List<Map<String,Object>> rows = mapReportObject.getRows();
			List<Map<String,Object>> foot = mapReportObject.getFooter();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("仓库类别进出表", 0);
			//定义label
			Label label = new Label(0,0,"仓库类别进出表");
			//添加label到cell
            sheet.addCell(label);
            //设置表格表头
            Label headfirst = new Label(0,1,"编码");
            sheet.addCell(headfirst);
            Label headsecond = new Label(1,1,"名称");
            sheet.addCell(headsecond);
            int index = 2;
            double totalb = 0;
            double totalin = 0;
            double totalout = 0;
            double totale = 0;
            for(int i = 2 ; i < headers.size()+2;i ++){
            	String code = headers.get(i-2).getCode();
            	position.put(code+"BAMOUNT", index);
            	Label head = new Label(index++,1,headers.get(i-2).getDes()+"|期初");
            	sheet.addCell(head);
            	position.put(code+"INAMOUNT", index);
            	head = new Label(index++,1,headers.get(i-2).getDes()+"|领入");
            	sheet.addCell(head);
            	position.put(code+"OUTAMOUNT", index);
            	head = new Label(index++,1,headers.get(i-2).getDes()+"|出库");
            	sheet.addCell(head);
            	position.put(code+"EAMOUNT", index);
            	head = new Label(index++,1,headers.get(i-2).getDes()+"|结存");
            	sheet.addCell(head);
            }
            position.put("TOTALBAMOUNT", index);
            Label head = new Label(index++,1,"合计|期初");
	       	sheet.addCell(head);
	       	position.put("TOTALINAMOUNT", index);
	       	head = new Label(index++,1,"合计|领入");
	       	sheet.addCell(head);
	       	position.put("TOTALOUTAMOUNT", index);
	       	head = new Label(index++,1,"合计|出库");
	       	sheet.addCell(head);
	       	position.put("TOTALEAMOUNT", index);
	       	head = new Label(index++,1,"合计|结存");
	       	sheet.addCell(head);
            sheet.mergeCells(0, 0, headers.size()+1, 0);
            //遍历list填充表格内容
            String curPositn = "";
            int j = 2;
            for(int i = 0 ; i < rows.size() ; i ++ ){
        		String innerPositn = (String) rows.get(i).get("DEPT");
        		if(!innerPositn.equals(curPositn) || (i == rows.size()-1 && !innerPositn.equals(curPositn))){
        			if(!"".equals(curPositn))j++;
            		Label project = new Label(0,j,innerPositn);
            		sheet.addCell(project);
        			Label totalLab = new Label(1,j,(String) rows.get(i).get("POSITNDES"));
        			sheet.addCell(totalLab);
        			curPositn = innerPositn;
        			if(i != 0){
        				//Label totallab = new Label(position.get("TOTALBAMOUNT"),j-1,String.valueOf(totalb));
        				jxl.write.Number totallab = new jxl.write.Number(position.get("TOTALBAMOUNT"),j-1,totalb,doubleStyle);
        				sheet.addCell(totallab);
        				//totallab = new Label(position.get("TOTALINAMOUNT"),j-1,String.valueOf(totalin));
        				totallab = new jxl.write.Number(position.get("TOTALINAMOUNT"),j-1,totalin,doubleStyle);
        				sheet.addCell(totallab);
        				//totallab = new Label(position.get("TOTALOUTAMOUNT"),j-1,String.valueOf(totalout));
        				totallab = new jxl.write.Number(position.get("TOTALOUTAMOUNT"),j-1,totalout,doubleStyle);
        				sheet.addCell(totallab);
        				//totallab = new Label(position.get("TOTALEAMOUNT"),j-1,String.valueOf(totale));
        				totallab = new jxl.write.Number(position.get("TOTALEAMOUNT"),j-1,totale,doubleStyle);
        				sheet.addCell(totallab);
        			}
        			totalb = 0;
    	            totalin = 0;
    	            totalout = 0;
    	            totale = 0;
        		}
        		String code = (String) rows.get(i).get("GRP");
        		//Label lab = new Label(position.get(code + "BAMOUNT"),j,rows.get(i).get("BAMOUNT")==null?"0":rows.get(i).get("BAMOUNT").toString());
        		jxl.write.Number lab = new jxl.write.Number(position.get(code + "BAMOUNT"),j,Double.parseDouble((rows.get(i).get("BAMOUNT")==null?"0":rows.get(i).get("BAMOUNT").toString())),doubleStyle);
        		sheet.addCell(lab);
    			totalb += (new BigDecimal(rows.get(i).get("BAMOUNT")==null?"0":rows.get(i).get("BAMOUNT").toString())).doubleValue();
    			//lab = new Label(position.get(code + "INAMOUNT"),j,rows.get(i).get("INAMOUNT")==null?"0":rows.get(i).get("INAMOUNT").toString());
    			lab = new jxl.write.Number(position.get(code + "INAMOUNT"),j,Double.parseDouble((rows.get(i).get("INAMOUNT")==null?"0":rows.get(i).get("INAMOUNT").toString())),doubleStyle);
    			sheet.addCell(lab);
    			totalin += (new BigDecimal(rows.get(i).get("INAMOUNT")==null?"0":rows.get(i).get("INAMOUNT").toString())).doubleValue();
    			//lab = new Label(position.get(code + "OUTAMOUNT"),j,rows.get(i).get("OUTAMOUNT")==null?"0":rows.get(i).get("OUTAMOUNT").toString());
    			lab = new jxl.write.Number(position.get(code + "OUTAMOUNT"),j,Double.parseDouble((rows.get(i).get("OUTAMOUNT")==null?"0":rows.get(i).get("OUTAMOUNT").toString())),doubleStyle);
    			sheet.addCell(lab);
    			totalout += (new BigDecimal(rows.get(i).get("OUTAMOUNT")==null?"0":rows.get(i).get("OUTAMOUNT").toString())).doubleValue();
    			//lab = new Label(position.get(code + "EAMOUNT"),j,rows.get(i).get("EAMOUNT")==null?"0":rows.get(i).get("EAMOUNT").toString());
    			lab = new jxl.write.Number(position.get(code + "EAMOUNT"),j,Double.parseDouble((rows.get(i).get("EAMOUNT")==null?"0":rows.get(i).get("EAMOUNT").toString())),doubleStyle);
    			sheet.addCell(lab);
    			totale += (new BigDecimal(rows.get(i).get("EAMOUNT")==null?"0":rows.get(i).get("EAMOUNT").toString())).doubleValue();
    			
    			if(i == rows.size()-1){
    				//Label totallab = new Label(position.get("TOTALBAMOUNT"),j,String.valueOf(totalb));
    				jxl.write.Number totallab = new jxl.write.Number(position.get("TOTALBAMOUNT"),j,totalb,doubleStyle);
    				sheet.addCell(totallab);
    				//totallab = new Label(position.get("TOTALINAMOUNT"),j,String.valueOf(totalin));
    				totallab = new jxl.write.Number(position.get("TOTALINAMOUNT"),j,totalin,doubleStyle);
    				sheet.addCell(totallab);
    				//totallab = new Label(position.get("TOTALOUTAMOUNT"),j,String.valueOf(totalout));
    				totallab = new jxl.write.Number(position.get("TOTALOUTAMOUNT"),j,totalout,doubleStyle);
    				sheet.addCell(totallab);
    				//totallab = new Label(position.get("TOTALEAMOUNT"),j,String.valueOf(totale));
    				totallab = new jxl.write.Number(position.get("TOTALEAMOUNT"),j,totale,doubleStyle);
    				sheet.addCell(totallab);
    				totalb = 0;
    	            totalin = 0;
    	            totalout = 0;
    	            totale = 0;
    			}
            }
            //填充合计
            j++;
            Label project = new Label(0,j,"合计");
    		sheet.addCell(project);
    		for(int i = 0 ; i < foot.size() ; i ++ ){
    			String code = (String) foot.get(i).get("GRP");
    			//Label lab = new Label(position.get(code + "BAMOUNT"),j,foot.get(i).get("BAMOUNT")==null?"0":foot.get(i).get("BAMOUNT").toString());
    			jxl.write.Number lab = new jxl.write.Number(position.get(code + "BAMOUNT"),j,Double.parseDouble(foot.get(i).get("BAMOUNT")==null?"0":foot.get(i).get("BAMOUNT").toString()),doubleStyle);
    			sheet.addCell(lab);
    			totalb += (new BigDecimal(foot.get(i).get("BAMOUNT")==null?"0":foot.get(i).get("BAMOUNT").toString())).doubleValue();
    			//lab = new Label(position.get(code + "INAMOUNT"),j,foot.get(i).get("INAMOUNT")==null?"0":foot.get(i).get("INAMOUNT").toString());
    			lab = new jxl.write.Number(position.get(code + "INAMOUNT"),j,Double.parseDouble(foot.get(i).get("INAMOUNT")==null?"0":foot.get(i).get("INAMOUNT").toString()),doubleStyle);
    			sheet.addCell(lab);
    			totalin += (new BigDecimal(foot.get(i).get("INAMOUNT")==null?"0":foot.get(i).get("INAMOUNT").toString())).doubleValue();
    			//lab = new Label(position.get(code + "OUTAMOUNT"),j,foot.get(i).get("OUTAMOUNT")==null?"0":foot.get(i).get("OUTAMOUNT").toString());
    			lab = new jxl.write.Number(position.get(code + "OUTAMOUNT"),j,Double.parseDouble(foot.get(i).get("OUTAMOUNT")==null?"0":foot.get(i).get("OUTAMOUNT").toString()),doubleStyle);
    			sheet.addCell(lab);
    			totalout += (new BigDecimal(foot.get(i).get("OUTAMOUNT")==null?"0":foot.get(i).get("OUTAMOUNT").toString())).doubleValue();
    			//lab = new Label(position.get(code + "EAMOUNT"),j,foot.get(i).get("EAMOUNT")==null?"0":foot.get(i).get("EAMOUNT").toString());
    			lab = new jxl.write.Number(position.get(code + "EAMOUNT"),j,Double.parseDouble(foot.get(i).get("EAMOUNT")==null?"0":foot.get(i).get("EAMOUNT").toString()),doubleStyle);
    			sheet.addCell(lab);
    			totale += (new BigDecimal(foot.get(i).get("EAMOUNT")==null?"0":foot.get(i).get("EAMOUNT").toString())).doubleValue();
    		}
    		//Label totallab = new Label(position.get("TOTALBAMOUNT"),j,String.valueOf(totalb));
    		jxl.write.Number totallab = new jxl.write.Number(position.get("TOTALBAMOUNT"),j,totalb,doubleStyle);
			sheet.addCell(totallab);
			//totallab = new Label(position.get("TOTALINAMOUNT"),j,String.valueOf(totalin));
			totallab = new jxl.write.Number(position.get("TOTALINAMOUNT"),j,totalin,doubleStyle);
			sheet.addCell(totallab);
			//totallab = new Label(position.get("TOTALOUTAMOUNT"),j,String.valueOf(totalout));
			totallab = new jxl.write.Number(position.get("TOTALOUTAMOUNT"),j,totalout,doubleStyle);
			sheet.addCell(totallab);
			//totallab = new Label(position.get("TOTALEAMOUNT"),j,String.valueOf(totale));
			totallab = new jxl.write.Number(position.get("TOTALEAMOUNT"),j,totale,doubleStyle);
			sheet.addCell(totallab);
            workBook.write();
            os.flush();
		}catch (Exception e) {
			throw new CRUDException(e);
		}finally{
			try {
				if(workBook!=null){
					workBook.close();
				}
				if(os!=null){
					os.close();
				}
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;		
	}		
}
