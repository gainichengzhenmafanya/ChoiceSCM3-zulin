package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.report.LsPandianchaxunMapper;
@Service
public class LsPandianchaxunService {

	@Autowired
	private LsPandianchaxunMapper lsPandianchaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;

	/**
	 * 查询历史盘点
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findLsPandian(Map<String,Object> content,Page pager) throws CRUDException{
		try {
			List<Map<String,Object>> foot = lsPandianchaxunMapper.findLsPandianCount(content);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(content, pager, LsPandianchaxunMapper.class.getName()+".findLsPandianDetailQuery");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
