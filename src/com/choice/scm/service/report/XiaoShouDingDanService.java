package com.choice.scm.service.report;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.report.SupplyAcctMapper;

@Service
public class XiaoShouDingDanService {
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;

	/**
	 * 销售订单查询
	 * 
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findDatas(Map<String, Object> conditions, Page page) throws CRUDException {
		try {
			reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName() + ".xiaoShouDingDanDetail"));
			reportObject.setTotal(page.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
