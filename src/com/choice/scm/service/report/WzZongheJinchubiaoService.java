package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class WzZongheJinchubiaoService {

	@Autowired
	private SupplyAcctMapper wzZongheJinchubiaoMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired
	private AcctMapper acctMapper;
	
	/**
	 * 物资综合进出表
	 * @param conditions
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findSupplySumInOut(Map<String,Object> conditions) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			conditions.put("yearr", acctMapper.findYearrByDate(sa.getBdat()));
			//判断仓位
			if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			List<Map<String,Object>> foot = wzZongheJinchubiaoMapper.findCalForSupplySumInOut(conditions);
			mapReportObject.setRows(wzZongheJinchubiaoMapper.findSupplySumInOut(conditions));
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
