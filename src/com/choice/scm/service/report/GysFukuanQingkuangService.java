package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class GysFukuanQingkuangService {

	@Autowired
	private SupplyAcctMapper gysFukuanQingkuangMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
		/**
		 * 供应商付款情况
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findDeliverPayment(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				List<Map<String,Object>> foot = gysFukuanQingkuangMapper.findCalForDeliverPayment(conditions);
				mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findDeliverPayment"));
				mapReportObject.setTotal(pager.getCount());
				mapReportObject.setFooter(foot);
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
		
		
}
