package com.choice.scm.service.report;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class MdPandianHaoyongFenxiService {

	@Autowired
	private SupplyAcctMapper mdPandianHaoyongFenxiMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	
	/**
	 * 门店盘点分析表
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findMdPandianFenxi(List<Positn> positnList,Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			conditions.put("isReportJmj", 0);
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			//判断领用仓位
			if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			StringBuffer sqlStr = new StringBuffer();
			StringBuffer sqlStr1 = new StringBuffer();
			StringBuffer sqlStr2 = new StringBuffer();
			for(Positn positn : positnList){
				sqlStr.append(" IFNULL(SUM(CASE WHEN F.DEPT='"+positn.getCode()+"' THEN ROUND(IFNULL(O.STOCKCNT,0),2) ELSE 0 END),0) AS C_"+positn.getCode()+",");
				sqlStr.append(" IFNULL(SUM(CASE WHEN F.DEPT='"+positn.getCode()+"' THEN ROUND(IFNULL(O.STOCKCNT,0)*IFNULL(O.PRICE,0),2) ELSE 0 END),0) AS A_"+positn.getCode()+",");
				sqlStr1.append(" IFNULL(SUM(CASE WHEN VSCODE='"+positn.getCode()+"' THEN NMONEY ELSE 0 END),0) AS M_"+positn.getCode()+",");
				sqlStr2.append(" ROUND(IFNULL(CASE WHEN M_"+positn.getCode()+" = 0 THEN 0 ELSE A_"+positn.getCode()+"/M_"+positn.getCode()+"*100 END,0),2) AS P_"+positn.getCode()+",");
			}
			conditions.put("sqlStr", sqlStr);
			conditions.put("sqlStr1", sqlStr1.substring(0, sqlStr1.length()-1));
			conditions.put("sqlStr2", sqlStr2.substring(0, sqlStr2.length()-1));
			if(null == page){
				reportObject.setRows(mdPandianHaoyongFenxiMapper.findMdPandianFenxi(conditions));
			}else{
				reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findMdPandianFenxi"));
				reportObject.setTotal(page.getCount());
			}
			List<Map<String,Object>> foot = mdPandianHaoyongFenxiMapper.findMdPandianFenxiSum(conditions);
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 门店盘点分析表 导出
	 * @param os
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportMdPandianFenxi(OutputStream os,Map<String,Object> conditions,List<Positn> positnList) throws CRUDException{
		WritableWorkbook workBook = null;
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try{
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			//记录表头位置
			Map<Integer,String> position = new HashMap<Integer,String>();
			reportObject = findMdPandianFenxi(positnList,conditions,null);
			List<Map<String,Object>> rows = reportObject.getRows();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("门店盘点分析表", 0);
			//定义label
			Label label = new Label(0,0,"门店盘点分析表",titleStyle1);
			//添加label到cell
            sheet.addCell(label);
            sheet.mergeCells(0, 0, positnList.size()+4, 0);
            //设置表格表头
            sheet.addCell(new Label(0,1,"物资编码",titleStyle2));
            sheet.addCell(new Label(1,1,"物资名称",titleStyle2));
            sheet.addCell(new Label(2,1,"物资规格",titleStyle2));
            sheet.addCell(new Label(3,1,"单位",titleStyle2));
            for(int i = 0 ; i < positnList.size();i ++){
            	sheet.addCell(new Label(i*3+4,1,positnList.get(i).getDes()+"实盘数量",titleStyle2));
            	sheet.addCell(new Label(i*3+5,1,positnList.get(i).getDes()+"实盘金额",titleStyle2));
            	sheet.addCell(new Label(i*3+6,1,positnList.get(i).getDes()+"占收入比",titleStyle2));
            	position.put(i*3+4,positnList.get(i).getCode());
            }
            for(int i = 0 ; i < rows.size() ; i ++ ){
            	Map<String,Object> map = rows.get(i);
            	sheet.addCell(new Label(0,2+i,null == map.get("SPCODE")?"":map.get("SPCODE").toString()));
                sheet.addCell(new Label(1,2+i,null == map.get("SPNAME")?"":map.get("SPNAME").toString()));
                sheet.addCell(new Label(2,2+i,null == map.get("SPDESC")?"":map.get("SPDESC").toString()));
                sheet.addCell(new Label(3,2+i,null == map.get("UNIT")?"":map.get("UNIT").toString()));
                
                for(int j = 0 ; j < positnList.size(); j++){
                	sheet.addCell(new jxl.write.Number(j*3+4,2+i,null == map.get("C_"+position.get(j*3+4))?0:Double.parseDouble(map.get("C_"+position.get(j*3+4)).toString()),doubleStyle));
                	sheet.addCell(new jxl.write.Number(j*3+5,2+i,null == map.get("A_"+position.get(j*3+4))?0:Double.parseDouble(map.get("A_"+position.get(j*3+4)).toString()),doubleStyle));
                	sheet.addCell(new Label(j*3+6,2+i,null == map.get("P_"+position.get(j*3+4))?"0.00%":map.get("P_"+position.get(j*3+4)).toString()+"%",doubleStyle));
                }
            }
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 门店耗用分析表
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findMdHaoyongFenxi(List<Positn> positnList,Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			conditions.put("isReportJmj", 0);
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			//判断领用仓位
			if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			StringBuffer sqlStr = new StringBuffer();
			StringBuffer sqlStr1 = new StringBuffer();
			StringBuffer sqlStr2 = new StringBuffer();
			for(Positn positn : positnList){
				sqlStr.append(" IFNULL(SUM(CASE WHEN S.CHKTYP = '9910' AND S.POSITN='"+positn.getCode()+"' THEN ROUND(IFNULL(S.CNTOUT,0),2) ELSE 0 END),0) AS C_"+positn.getCode()+",");
				sqlStr.append(" IFNULL(SUM(CASE WHEN S.CHKTYP = '9910' AND S.POSITN='"+positn.getCode()+"' THEN ROUND(IFNULL(S.AMTOUT,0),2) ELSE 0 END),0) AS A_"+positn.getCode()+",");
				sqlStr1.append(" IFNULL(SUM(CASE WHEN VSCODE='"+positn.getCode()+"' THEN NMONEY ELSE 0 END),0) AS M_"+positn.getCode()+",");
				sqlStr2.append(" ROUND(IFNULL(CASE WHEN M_"+positn.getCode()+" = 0 THEN 0 ELSE A_"+positn.getCode()+"/M_"+positn.getCode()+"*100 END,0),2) AS P_"+positn.getCode()+",");
			}
			conditions.put("sqlStr", sqlStr);
			conditions.put("sqlStr1", sqlStr1.substring(0, sqlStr1.length()-1));
			conditions.put("sqlStr2", sqlStr2.substring(0, sqlStr2.length()-1));
			if(null == page){
				reportObject.setRows(mdPandianHaoyongFenxiMapper.findMdHaoyongFenxi(conditions));
			}else{
				reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findMdHaoyongFenxi"));
				reportObject.setTotal(page.getCount());
			}
			List<Map<String,Object>> foot = mdPandianHaoyongFenxiMapper.findMdHaoyongFenxiSum(conditions);
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 门店盘点分析表 导出
	 * @param os
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportMdHaoyongFenxi(OutputStream os,Map<String,Object> conditions,List<Positn> positnList) throws CRUDException{
		WritableWorkbook workBook = null;
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try{
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			//记录表头位置
			Map<Integer,String> position = new HashMap<Integer,String>();
			reportObject = findMdHaoyongFenxi(positnList,conditions,null);
			List<Map<String,Object>> rows = reportObject.getRows();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("门店耗用分析表", 0);
			//定义label
			Label label = new Label(0,0,"门店耗用分析表",titleStyle1);
			//添加label到cell
            sheet.addCell(label);
            sheet.mergeCells(0, 0, positnList.size()+4, 0);
            //设置表格表头
            sheet.addCell(new Label(0,1,"物资编码",titleStyle2));
            sheet.addCell(new Label(1,1,"物资名称",titleStyle2));
            sheet.addCell(new Label(2,1,"物资规格",titleStyle2));
            sheet.addCell(new Label(3,1,"单位",titleStyle2));
            for(int i = 0 ; i < positnList.size();i ++){
            	sheet.addCell(new Label(i*3+4,1,positnList.get(i).getDes()+"出库数量",titleStyle2));
            	sheet.addCell(new Label(i*3+5,1,positnList.get(i).getDes()+"出库金额",titleStyle2));
            	sheet.addCell(new Label(i*3+6,1,positnList.get(i).getDes()+"占收入比",titleStyle2));
            	position.put(i*3+4,positnList.get(i).getCode());
            }
            for(int i = 0 ; i < rows.size() ; i ++ ){
            	Map<String,Object> map = rows.get(i);
            	sheet.addCell(new Label(0,2+i,null == map.get("SPCODE")?"":map.get("SPCODE").toString()));
                sheet.addCell(new Label(1,2+i,null == map.get("SPNAME")?"":map.get("SPNAME").toString()));
                sheet.addCell(new Label(2,2+i,null == map.get("SPDESC")?"":map.get("SPDESC").toString()));
                sheet.addCell(new Label(3,2+i,null == map.get("UNIT")?"":map.get("UNIT").toString()));
                
                for(int j = 0 ; j < positnList.size(); j++){
                	sheet.addCell(new jxl.write.Number(j*3+4,2+i,null == map.get("C_"+position.get(j*3+4))?0:Double.parseDouble(map.get("C_"+position.get(j*3+4)).toString()),doubleStyle));
                	sheet.addCell(new jxl.write.Number(j*3+5,2+i,null == map.get("A_"+position.get(j*3+4))?0:Double.parseDouble(map.get("A_"+position.get(j*3+4)).toString()),doubleStyle));
                	sheet.addCell(new Label(j*3+6,2+i,null == map.get("P_"+position.get(j*3+4))?"0.00%":map.get("P_"+position.get(j*3+4)).toString()+"%",doubleStyle));
                }
            }
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
}
