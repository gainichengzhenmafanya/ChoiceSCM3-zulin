package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.report.SupplyAcctMapper;

@Service
public class SupplyAcctService {

	@Autowired
	private SupplyAcctMapper supplyAcctMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
		/**
		 * 毛利查询
		 * @param conditions
		 * @param pager
		 * @return
		 */
		public ReportObject<Map<String,Object>> findGrossProfit(Map<String,Object> conditions,Page pager){
			mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findGrossProfit"));
			List<Map<String,Object>> foot = supplyAcctMapper.findGrossProfitSum(conditions);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}	
}
