package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class PsYanhuoChaxunService {

	@Autowired
	private SupplyAcctMapper psYanhuoChaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
		/**
		 * 配送验货查询
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findDeliveryExamineQuery(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
				//判断仓位
				if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
					sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
				}
				//判断领用仓位
				if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
					sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
				}
				List<Map<String,Object>> foot = psYanhuoChaxunMapper.findCalForDeliveryExamineQuery(conditions);
				mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findDeliveryExamineQuery"));
				mapReportObject.setFooter(foot);
				mapReportObject.setTotal(pager.getCount());
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
	
	

}
