package com.choice.scm.service.report;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.domain.DictColumns;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;

@Service
public class CkMonthHuizongService {
	@Autowired
	private SupplyAcctMapper ckMonthHuizongMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private ReportObject<Map<String, Object>> reportMap;
	@Autowired
	private PositnMapper positnMapper;

	/**
	 * 根据日期拼接列头，封装到 DictColumns
	 * @param begindate
	 * @param enddate
	 * @return List<DictColumns>
	 */
	public List<DictColumns> getDateSql(String begindate, String enddate) {

		List<DictColumns> list = new ArrayList<DictColumns>();
		String[] begindatestr = begindate.split("-");
		int beginmonth = Integer.valueOf(begindatestr[1]) - 1;
		String[] enddatestr = enddate.split("-");
		int endmonth = Integer.valueOf(enddatestr[1]) - 1;
		Calendar start = Calendar.getInstance();
		start.set(Integer.parseInt(begindatestr[0]), beginmonth, Integer.parseInt(begindatestr[2]));
		Long startTime = start.getTimeInMillis();
		Calendar end = Calendar.getInstance();
		end.set(Integer.parseInt(enddatestr[0]), endmonth, Integer.parseInt(enddatestr[2]));
		Long endTime = end.getTimeInMillis();
		Long oneDay = 1000 * 60 * 60 * 24l;
		Long time = startTime;
		DictColumns dictColumns = new DictColumns();
		dictColumns.setColumnName("firm");
		dictColumns.setId("144");
		dictColumns.setZhColumnName("分店编码");
		dictColumns.setProperties("firm");
		dictColumns.setColumnWidth("70");
		list.add(dictColumns);
		dictColumns = new DictColumns();
		dictColumns.setColumnName("firmdes");
		dictColumns.setZhColumnName("分店名称");
		dictColumns.setProperties("firmdes");
		dictColumns.setId("145");
		dictColumns.setColumnWidth("205");
		list.add(dictColumns);
		dictColumns = new DictColumns();
		dictColumns.setColumnName("monthsum");
		dictColumns.setZhColumnName("合计");
		dictColumns.setProperties("monthsum");
		dictColumns.setId("146");
		dictColumns.setColumnWidth("150");
		list.add(dictColumns);
		while (time <= endTime) {
			Date d = new Date(time);
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			String str = df.format(d);
			int num = Integer.parseInt(str.substring(6));//取日期
			dictColumns = new DictColumns();
			dictColumns.setColumnName("QUERY" + str);
			dictColumns.setZhColumnName(num + "日");
			dictColumns.setProperties("QUERY" + str);
			dictColumns.setColumnWidth("70");
			list.add(dictColumns);
			time += oneDay;
		}
		return list;
	}

	/**
	 * 获取会计年和会计月,拼接sql
	 * @param year
	 * @param month 
	 * @param jmj
	 * @return 返回数组:begindate,enddate,拼接的sql
	 */

	public String[] getSQLByDateOfMonth(String year,String month, int jmj) {

		String[] strarray = new String[3];
		String sql = "";
		Map<String, Object> conditions = new HashMap<String, Object>();		
		String col = "BDAT" + month + ",EDAT" + month;
		conditions.put("col", col);
		conditions.put("year", year);
		List<Map<String, Object>> list = ckMonthHuizongMapper.findMainMonthDate(conditions);
		String begindate = new SimpleDateFormat("yyyy-MM-dd").format(list.get(0).get("BDAT" + month));
		String enddate = new SimpleDateFormat("yyyy-MM-dd").format(list.get(0).get("EDAT" + month));
		strarray[0] = begindate;
		strarray[1] = enddate;
		List<DictColumns> datelist = getDateSql(begindate, enddate);
		String sqlstr = "";
		String monthsumstr="";
		
		if (jmj == 0) {
			monthsumstr="sum(case when DATE_FORMAT(DAT,'%Y-%m-%d')>='"+begindate+"' and DATE_FORMAT(DAT,'%Y-%m-%d')<='"+enddate+"' then ROUND(A.PRICEOUT*A.CNTOUT ,2) else 0 end) as monthsum,";
			sqlstr = "sum(case when to_char(DAT,'yyyy-MM-dd')='";
		}else if (jmj == 1){
			monthsumstr="sum(case when DATE_FORMAT(DAT,'%Y-%m-%d')>= '"+begindate+"' and DATE_FORMAT(DAT,'%Y-%m-%d')<='"+enddate+"' then ROUND(C.SP_PRICE*A.CNTOUT ,2) else 0 end) as monthsum,";
			sqlstr = "sum(case when to_char(DAT,'yyyy-MM-dd')='";
		}
		for (int j = 3; j < datelist.size(); j++)// 去掉分店编码和分店名称
		{			
			String datestr = datelist.get(j).getColumnName().substring(5);//获取日期：yyyymmdd
			String str = datestr.substring(0,4)+'-'+datestr.substring(4,6)+'-'+datestr.substring(6);
			if (jmj == 0) {
				sql += sqlstr + str + "' then ROUND(A.PRICEOUT*A.CNTOUT ,2) else 0 end) as QUERY" + datestr + ",";
			} else if (jmj == 1) {
				sql += sqlstr + str + "' then ROUND(C.SP_PRICE*A.CNTOUT ,2) else 0 end) as QUERY" + datestr + ",";
			}

		}
		sql = sql.substring(0, sql.length() - 1);//去掉末尾,
		strarray[2] = monthsumstr+sql;
		return strarray;

	}

	/**
	 * 获取按月汇总报表列头
	 * @param year
	 * @param month
	 * @return List<DictColumns>
	 */

	public List<DictColumns> getCkMonthHuizongHead(String year,String month) {

		Map<String, Object> conditions = new HashMap<String, Object>();		
		String col = "BDAT" + month + ",EDAT" + month;
		conditions.put("col", col);
		conditions.put("year", year);
		List<Map<String, Object>> list = ckMonthHuizongMapper.findMainMonthDate(conditions);
		String begindate = new SimpleDateFormat("yyyy-MM-dd").format(list.get(0).get("BDAT" + month));
		String enddate = new SimpleDateFormat("yyyy-MM-dd").format(list.get(0).get("EDAT" + month));
		List<DictColumns> sqllist = getDateSql(begindate, enddate);
		return sqllist;

	}

	/**
	 * 按月汇总+合计	  
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> findChkoutMonthSum(Map<String, Object> conditions, Page page) throws CRUDException {
		try {
			SupplyAcct sa = ((SupplyAcct) conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			// 判断仓位
			if (null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())) {
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			// 判断领用仓位
			if (null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())) {
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			conditions.put("supplyAcct", sa);
			reportMap.setRows(ckMonthHuizongMapper.findChkoutMonthSum(conditions));
			reportMap.setFooter(ckMonthHuizongMapper.findChkoutMonthSumHJ(conditions));			
			reportMap.setTotal(page.getCount());
			return reportMap;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
