package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;

@Service
public class YxqChaxunService {

	@Autowired
	private SupplyAcctMapper yxqChaxunMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private PageManager<SupplyAcct> pageManager;
	
	/**
	 * 入库明细查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findYxqDetailS(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			Spbatch sp = ((Spbatch)conditions.get("spbatch"));
			//判断仓位
			if(null != sp.getPositn() && sp.getPositn()!=""){
				sp.setPositn(CodeHelper.replaceCode(sp.getPositn()));
			}
			//判断供应商
			if(null != sp.getDeliver() && sp.getDeliver()!=""){
				sp.setDeliver(CodeHelper.replaceCode(sp.getDeliver()));
			}
			List<Map<String,Object>> foot = yxqChaxunMapper.findYxqCal(conditions);
			reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findYxqDetailS"));
			reportObject.setFooter(foot);
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
}
