package com.choice.scm.service.report;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class WzFendianHuizongService {

	@Autowired
	private SupplyAcctMapper wzFendianHuizongMapper;
	@Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;
	
	
	/**
	 * 物资分店汇总
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findWzFendianHuizong(List<Positn> positnList,Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			//判断仓位
			if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			//判断领用仓位
			if(null != sa && null != sa.getFirm() && !"".equals(sa.getFirm())){
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
			}
			StringBuffer sqlStr = new StringBuffer();
			StringBuffer codes = new StringBuffer();//算合计用
			for(Positn positn : positnList){
				codes.append(CodeHelper.replaceCode(positn.getCode())+",");
				sqlStr.append(" SUM(CASE WHEN A.FIRM='"+positn.getCode()+"' THEN ROUND(CNTOUT,2) ELSE 0 END) AS C_"+positn.getCode()+",");
				if(null != conditions.get("bysale") && conditions.get("bysale").equals("true")){
					sqlStr.append("  CASE WHEN SUM( CASE WHEN FIRM ='"+positn.getCode()+"' THEN ROUND(CNTOUT,2) ELSE 0 END)=0 THEN 0 ELSE  SUM(CASE WHEN FIRM = '"+positn.getCode()
							+"' THEN ROUND((CASE WHEN PRICESALE=0 THEN PRICEOUT ELSE PRICESALE END)*A.CNTOUT,2) END)/ SUM( CASE WHEN FIRM = '"+positn.getCode() 
							+"' THEN ROUND(CNTOUT,2) ELSE 0 END) END AS P_"+positn.getCode()+",");
					sqlStr.append(" SUM(CASE WHEN A.FIRM='"+positn.getCode()+"' THEN ROUND((CASE WHEN PRICESALE=0 THEN PRICEOUT ELSE PRICESALE END)*A.CNTOUT,2) END) AS A_"+positn.getCode()+",");
				}else{
					sqlStr.append("  CASE WHEN SUM( CASE WHEN FIRM ='"+positn.getCode()+"' THEN ROUND(CNTOUT,2) ELSE 0 END)=0 THEN 0 ELSE  SUM(CASE WHEN FIRM = '"+positn.getCode()
							+"' THEN ROUND(PRICEOUT*A.CNTOUT,2) END)/ SUM( CASE WHEN FIRM = '"+positn.getCode() 
							+"' THEN ROUND(CNTOUT,2) ELSE 0 END) END AS P_"+positn.getCode()+",");
					sqlStr.append(" SUM(CASE WHEN A.FIRM='"+positn.getCode()+"' THEN ROUND(A.PRICEOUT*A.CNTOUT,2) END) AS A_"+positn.getCode()+",");
				}
			}
			String code = codes.substring(0, codes.length()-1);
			sqlStr.append(" SUM(CASE WHEN A.FIRM in ("+code+") THEN ROUND(CNTOUT,2) ELSE 0 END) AS C_TOTAL,");
			if(null != conditions.get("bysale") && conditions.get("bysale").equals("true")){
				sqlStr.append("  CASE WHEN SUM( CASE WHEN FIRM in ("+code+") THEN ROUND(CNTOUT,2) ELSE 0 END)=0 THEN 0 ELSE  SUM(CASE WHEN FIRM in ("+code
						+") THEN ROUND((CASE WHEN PRICESALE=0 THEN PRICEOUT ELSE PRICESALE END)*A.CNTOUT,2) END)/ SUM( CASE WHEN FIRM in ("+code
						+") THEN ROUND(CNTOUT,2) ELSE 0 END) END AS P_TOTAL,");
				sqlStr.append(" SUM(CASE WHEN A.FIRM in ("+code+") THEN ROUND((CASE WHEN PRICESALE=0 THEN PRICEOUT ELSE PRICESALE END)*A.CNTOUT,2) END) AS A_TOTAL,");
			}else{
				sqlStr.append("  CASE WHEN SUM( CASE WHEN FIRM in ("+code+") THEN ROUND(CNTOUT,2) ELSE 0 END)=0 THEN 0 ELSE  SUM(CASE WHEN FIRM in ("+code
						+") THEN ROUND(PRICEOUT*A.CNTOUT,2) END)/ SUM( CASE WHEN FIRM in ("+code
						+") THEN ROUND(CNTOUT,2) ELSE 0 END) END AS P_TOTAL,");
				sqlStr.append(" SUM(CASE WHEN A.FIRM in ("+code+") THEN ROUND(A.PRICEOUT*A.CNTOUT,2) END) AS A_TOTAL,");
			}
			conditions.put("sqlStr", sqlStr);
			conditions.put("supplyAcct", sa);
			if(null == page){
				reportObject.setRows(wzFendianHuizongMapper.findWzFendianHuizong(conditions));
			}else{
				reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findWzFendianHuizong"));
				reportObject.setTotal(page.getCount());
			}
			List<Map<String,Object>> foot = wzFendianHuizongMapper.findWzFendianHuizongSum(conditions);
			reportObject.setFooter(foot);
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 
	 * @param os
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public boolean exportWzFendianHuizong(OutputStream os,Map<String,Object> conditions,List<Positn> positnList) throws CRUDException{
		WritableWorkbook workBook = null;
		WritableFont titleFont1 = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle1 = new WritableCellFormat(titleFont1);
		WritableFont titleFont2 = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle2 = new WritableCellFormat(titleFont2);
		WritableCellFormat doubleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES),new NumberFormat("0.00"));//设定带小数数字单元格格式
		try{
			titleStyle1.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			titleStyle2.setBorder(Border.ALL, BorderLineStyle.THIN,jxl.format.Colour.BLACK);
			//记录表头位置
			Map<Integer,String> position = new HashMap<Integer,String>();
			reportObject = findWzFendianHuizong(positnList,conditions,null);
			List<Map<String,Object>> rows = reportObject.getRows();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("物资分店汇总", 0);
			//定义label
			Label label = new Label(0,0,"物资分店汇总",titleStyle1);
			//添加label到cell
            sheet.addCell(label);
            sheet.mergeCells(0, 0, positnList.size()+7, 0);
            //设置表格表头
            sheet.addCell(new Label(0,1,"物资编码",titleStyle2));
            sheet.addCell(new Label(1,1,"物资名称",titleStyle2));
            sheet.addCell(new Label(2,1,"物资规格",titleStyle2));
            sheet.addCell(new Label(3,1,"单位",titleStyle2));
            sheet.addCell(new Label(4,1,"合计数量",titleStyle2));
            sheet.addCell(new Label(5,1,"平均价",titleStyle2));
            sheet.addCell(new Label(6,1,"合计金额",titleStyle2));
            for(int i = 0 ; i < positnList.size();i ++){
            	sheet.addCell(new Label(i*3+7,1,positnList.get(i).getDes()+"数量",titleStyle2));
            	sheet.addCell(new Label(i*3+8,1,positnList.get(i).getDes()+"单价",titleStyle2));
            	sheet.addCell(new Label(i*3+9,1,positnList.get(i).getDes()+"金额",titleStyle2));
            	position.put(i*3+7,positnList.get(i).getCode());
            }
            for(int i = 0 ; i < rows.size() ; i ++ ){
            	Map<String,Object> map = rows.get(i);
            	sheet.addCell(new Label(0,2+i,null == map.get("SPCODE")?"":map.get("SPCODE").toString()));
                sheet.addCell(new Label(1,2+i,null == map.get("SPNAME")?"":map.get("SPNAME").toString()));
                sheet.addCell(new Label(2,2+i,null == map.get("SPDESC")?"":map.get("SPDESC").toString()));
                sheet.addCell(new Label(3,2+i,null == map.get("UNIT")?"":map.get("UNIT").toString()));
                sheet.addCell(new jxl.write.Number(4,2+i,null == map.get("C_TOTAL")?0:Double.parseDouble(map.get("C_TOTAL").toString()),doubleStyle));
                sheet.addCell(new jxl.write.Number(5,2+i,null == map.get("P_TOTAL")?0:Double.parseDouble(map.get("P_TOTAL").toString()),doubleStyle));
                sheet.addCell(new jxl.write.Number(6,2+i,null == map.get("A_TOTAL")?0:Double.parseDouble(map.get("A_TOTAL").toString()),doubleStyle));
                
                for(int j = 0 ; j < positnList.size(); j++){
                	sheet.addCell(new jxl.write.Number(j*3+7,2+i,null == map.get("C_"+position.get(j*3+7))?0:Double.parseDouble(map.get("C_"+position.get(j*3+7)).toString()),doubleStyle));
                	sheet.addCell(new jxl.write.Number(j*3+8,2+i,null == map.get("P_"+position.get(j*3+7))?0:Double.parseDouble(map.get("P_"+position.get(j*3+7)).toString()),doubleStyle));
                	sheet.addCell(new jxl.write.Number(j*3+9,2+i,null == map.get("A_"+position.get(j*3+7))?0:Double.parseDouble(map.get("A_"+position.get(j*3+7)).toString()),doubleStyle));
                }
            }
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	
}
