package com.choice.scm.service.report;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.GrpTypMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;

@Service
public class CangkuLeibieJinchuSecondService {

	@Autowired
	private SupplyAcctMapper cangkuLeibieJinchuSecondMapper;
	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private ReportObject<Map<String, Object>> mapReportObject;
	@Autowired
	private GrpTypMapper grpTypMapper;
	@Autowired
	private PageManager<Map<String, Object>> mapPageManager;

	/**
	 * 仓库类别进出表
	 * 
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String, Object>> findStockCategoryInOut(Map<String, Object> conditions, Page pager) throws CRUDException {
		try {
			SupplyAcct supplyAcct = ((SupplyAcct) conditions.get("supplyAcct"));
			// 条件判断
			if (null != supplyAcct && !"".equals(supplyAcct)) {
				if (null != supplyAcct.getPositn() && !"".equals(supplyAcct.getPositn())) {
					supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
				}
				if (null != supplyAcct.getGrptyp() && !"".equals(supplyAcct.getGrptyp())) {
					supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
				}

			}
			// 判断日期
			Date beginDate = supplyAcct.getBdat();
			Date endDate = supplyAcct.getEdat();
			Date date = new Date();
			// 开始日期
			if (beginDate == null) {
				supplyAcct.setBdat(date);
			}
			// 结束日期
			if (endDate == null) {
				supplyAcct.setEdat(date);
			}
			String yearr = acctMapper.findYearrByDate(beginDate);
			// 如果未获得 则取开始日期的年份
			if (null == yearr || "".equals(yearr)) {
				yearr = new SimpleDateFormat("yyyy").format(beginDate);
			}
			conditions.put("yearr", yearr);
			/*
			 * 修改前，不查询存储过程代码 //仓位超过1000，用in会出现问题 if(supplyAcct!=null){ String
			 * positnStr = supplyAcct.getPositn();
			 * if(positnStr!=null&&!"".equals(positnStr.trim())){
			 * //根据1000个分割in里面的条件个数 String pjstr =
			 * OracleUtil.getOraInSql("POSITN",
			 * Arrays.asList(positnStr.split(","))); String apjstr =
			 * OracleUtil.getOraInSql("A.POSITN",
			 * Arrays.asList(positnStr.split(","))); //设置查询条件
			 * conditions.put("positnStr", pjstr); conditions.put("apositnStr",
			 * apjstr); } }
			 * mapReportObject.setRows(mapPageManager.selectPage(conditions,
			 * pager,
			 * SupplyAcctMapper.class.getName()+".getCangkuLeibieJincheSd"));
			 * mapReportObject.setTotal(pager.getCount());
			 * mapReportObject.setFooter
			 * (cangkuLeibieJinchuSecondMapper.getCangkuLeibieJincheHj
			 * (conditions));
			 */
			// 调用存储过程
			cangkuLeibieJinchuSecondMapper.callScmPrcCkLeibieJinChuSecond(conditions);
			mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName() + ".getCangkuLeibieJinchuSdPage");
			mapReportObject.setRows(cangkuLeibieJinchuSecondMapper.findTempCkLeiBieJinChuSecond(conditions));
			mapReportObject.setTotal(pager.getCount());
			mapReportObject.setFooter(cangkuLeibieJinchuSecondMapper.findTempCkLeiBieJinChuSecondHj(conditions));
			return mapReportObject;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}

	/**
	 * Object转数字
	 * 
	 * @param obj
	 * @return
	 */
	public static double objectToNumber(Object obj) {
		if (obj == null) {
			return 0d;
		}
		try {
			return Double.parseDouble(obj.toString());
		} catch (Exception e) {
			return 0d;
		}
	}

	/**
	 * 仓库类别进出表导出
	 * 
	 * @param os
	 * @param supplyAcct
	 * @return
	 * @throws Exception
	 */
	public boolean exportStockCategoryInOut(OutputStream os, Map<String, Object> condition) throws Exception {
		WritableWorkbook workBook = null;
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat doubleStyle = new WritableCellFormat(contentFont,new NumberFormat("0.00"));//设定带小数数字单元格格式
		try {
			Page pager = new Page();
			pager.setPageSize(Integer.MAX_VALUE);
			// 记录表头位置
			Map<String, Integer> position = new HashMap<String, Integer>();
			mapReportObject = findStockCategoryInOut(condition, pager);
			List<GrpTyp> headers = grpTypMapper.findAllGrpTypA(((SupplyAcct) condition.get("supplyAcct")).getAcct());
			List<Map<String, Object>> rows = mapReportObject.getRows();
			List<Map<String, Object>> foot = mapReportObject.getFooter();
			workBook = Workbook.createWorkbook(os);
			// 新建工作表
			WritableSheet sheet = workBook.createSheet("仓库类别进出表", 0);
			// 定义label
			Label label = new Label(0, 0, "仓库类别进出表");
			// 添加label到cell
			sheet.addCell(label);
			// 设置表格表头
			Label headfirst = new Label(0, 1, "编码");
			sheet.addCell(headfirst);
			Label headsecond = new Label(1, 1, "名称");
			sheet.addCell(headsecond);
			int index = 2;
			double totalb = 0;
			double totalin = 0;
			double totalout = 0;
			double totale = 0;
			for (int i = 2; i < headers.size() + 2; i++) {
				String code = headers.get(i - 2).getCode();
				position.put(code + "BAMOUNT", index);
				Label head = new Label(index++, 1, headers.get(i - 2).getDes() + "|期初");
				sheet.addCell(head);
				position.put(code + "INAMOUNT", index);
				head = new Label(index++, 1, headers.get(i - 2).getDes() + "|领入");
				sheet.addCell(head);
				position.put(code + "OUTAMOUNT", index);
				head = new Label(index++, 1, headers.get(i - 2).getDes() + "|出库");
				sheet.addCell(head);
				position.put(code + "EAMOUNT", index);
				head = new Label(index++, 1, headers.get(i - 2).getDes() + "|结存");
				sheet.addCell(head);
			}
			position.put("TOTALBAMOUNT", index);
			Label head = new Label(index++, 1, "合计|期初");
			sheet.addCell(head);
			position.put("TOTALINAMOUNT", index);
			head = new Label(index++, 1, "合计|领入");
			sheet.addCell(head);
			position.put("TOTALOUTAMOUNT", index);
			head = new Label(index++, 1, "合计|出库");
			sheet.addCell(head);
			position.put("TOTALEAMOUNT", index);
			head = new Label(index++, 1, "合计|结存");
			sheet.addCell(head);
			sheet.mergeCells(0, 0, headers.size() + 1, 0);
			// 遍历list填充表格内容
			String curPositn = "";
			int j = 2;
			for (int i = 0; i < rows.size(); i++) {
				String innerPositn = (String) rows.get(i).get("DEPT");
				if (!innerPositn.equals(curPositn) || (i == rows.size() - 1 && !innerPositn.equals(curPositn))) {
					if (!"".equals(curPositn))
						j++;
					Label project = new Label(0, j, innerPositn);
					sheet.addCell(project);
					Label totalLab = new Label(1, j, (String) rows.get(i).get("POSITNDES"));
					sheet.addCell(totalLab);
					curPositn = innerPositn;
					if (i != 0) {
						//Label totallab = new Label(position.get("TOTALBAMOUNT"), j - 1, String.valueOf(totalb));
						jxl.write.Number totallab = new jxl.write.Number(position.get("TOTALBAMOUNT"), j-1, totalb,doubleStyle);
						sheet.addCell(totallab);
						//totallab = new Label(position.get("TOTALINAMOUNT"), j - 1, String.valueOf(totalin));
						totallab = new jxl.write.Number(position.get("TOTALINAMOUNT"), j-1, totalin,doubleStyle);
						sheet.addCell(totallab);
						//totallab = new Label(position.get("TOTALOUTAMOUNT"), j - 1, String.valueOf(totalout));
						totallab = new jxl.write.Number(position.get("TOTALOUTAMOUNT"), j-1, totalout,doubleStyle);
						sheet.addCell(totallab);
						//totallab = new Label(position.get("TOTALEAMOUNT"), j - 1, String.valueOf(totale));
						totallab = new jxl.write.Number(position.get("TOTALEAMOUNT"), j-1, totale,doubleStyle);
						sheet.addCell(totallab);
					}
					totalb = 0;
					totalin = 0;
					totalout = 0;
					totale = 0;
				}
				String code = (String) rows.get(i).get("GRP");
				//Label lab = new Label(position.get(code + "BAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(rows.get(i).get("BAMOUNT")))).doubleValue()));
				jxl.write.Number lab = new jxl.write.Number(position.get(code + "BAMOUNT"), j, (new BigDecimal(String.valueOf(rows.get(i).get("BAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totalb += (new BigDecimal(String.valueOf(rows.get(i).get("BAMOUNT")))).doubleValue();
				//lab = new Label(position.get(code + "INAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(rows.get(i).get("INAMOUNT")))).doubleValue()));
				lab = new jxl.write.Number(position.get(code + "INAMOUNT"), j, (new BigDecimal(String.valueOf(rows.get(i).get("INAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totalin += (new BigDecimal(String.valueOf(rows.get(i).get("INAMOUNT")))).doubleValue();
				//lab = new Label(position.get(code + "OUTAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(rows.get(i).get("OUTAMOUNT")))).doubleValue()));
				lab = new jxl.write.Number(position.get(code + "OUTAMOUNT"), j, (new BigDecimal(String.valueOf(rows.get(i).get("OUTAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totalout += (new BigDecimal(String.valueOf(rows.get(i).get("OUTAMOUNT")))).doubleValue();
				//lab = new Label(position.get(code + "EAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(rows.get(i).get("EAMOUNT")))).doubleValue()));
				lab = new jxl.write.Number(position.get(code + "EAMOUNT"), j, (new BigDecimal(String.valueOf(rows.get(i).get("EAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totale += (new BigDecimal(String.valueOf(rows.get(i).get("EAMOUNT")))).doubleValue();

				if (i == rows.size() - 1) {
					//Label totallab = new Label(position.get("TOTALBAMOUNT"), j, String.valueOf(totalb));
					jxl.write.Number totallab = new jxl.write.Number(position.get("TOTALBAMOUNT"), j, totalb,doubleStyle);
					sheet.addCell(totallab);
					//totallab = new Label(position.get("TOTALINAMOUNT"), j, String.valueOf(totalin));
					totallab = new jxl.write.Number(position.get("TOTALINAMOUNT"), j, totalin,doubleStyle);
					sheet.addCell(totallab);
					//totallab = new Label(position.get("TOTALOUTAMOUNT"), j, String.valueOf(totalout));
					totallab = new jxl.write.Number(position.get("TOTALOUTAMOUNT"), j, totalout,doubleStyle);
					sheet.addCell(totallab);
					//totallab = new Label(position.get("TOTALEAMOUNT"), j, String.valueOf(totale));
					totallab = new jxl.write.Number(position.get("TOTALEAMOUNT"), j, totale,doubleStyle);
					sheet.addCell(totallab);
					totalb = 0;
					totalin = 0;
					totalout = 0;
					totale = 0;
				}
			}
			// 填充合计
			j++;
			Label project = new Label(0, j, "合计");
			sheet.addCell(project);
			for (int i = 0; i < foot.size(); i++) {
				String code = (String) foot.get(i).get("GRP");
				//Label lab = new Label(position.get(code + "BAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(foot.get(i).get("BAMOUNT")))).doubleValue()));
				jxl.write.Number lab = new jxl.write.Number(position.get(code + "BAMOUNT"), j, (new BigDecimal(String.valueOf(foot.get(i).get("BAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totalb += (new BigDecimal(String.valueOf(foot.get(i).get("BAMOUNT")))).doubleValue();
				//lab = new Label(position.get(code + "INAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(foot.get(i).get("INAMOUNT")))).doubleValue()));
				lab = new jxl.write.Number(position.get(code + "INAMOUNT"), j, (new BigDecimal(String.valueOf(foot.get(i).get("INAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totalin += (new BigDecimal(String.valueOf(foot.get(i).get("INAMOUNT")))).doubleValue();
				//lab = new Label(position.get(code + "OUTAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(foot.get(i).get("OUTAMOUNT")))).doubleValue()));
				lab = new jxl.write.Number(position.get(code + "OUTAMOUNT"), j, (new BigDecimal(String.valueOf(foot.get(i).get("OUTAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totalout += (new BigDecimal(String.valueOf(foot.get(i).get("OUTAMOUNT")))).doubleValue();
				//lab = new Label(position.get(code + "EAMOUNT"), j, String.valueOf((new BigDecimal(String.valueOf(foot.get(i).get("EAMOUNT")))).doubleValue()));
				lab = new jxl.write.Number(position.get(code + "EAMOUNT"), j, (new BigDecimal(String.valueOf(foot.get(i).get("EAMOUNT")))).doubleValue(),doubleStyle);
				sheet.addCell(lab);
				totale += (new BigDecimal(String.valueOf(foot.get(i).get("EAMOUNT")))).doubleValue();
			}
			//Label totallab = new Label(position.get("TOTALBAMOUNT"), j, String.valueOf(totalb));
			jxl.write.Number totallab = new jxl.write.Number(position.get("TOTALBAMOUNT"), j, totalb,doubleStyle);
			sheet.addCell(totallab);
			//totallab = new Label(position.get("TOTALINAMOUNT"), j, String.valueOf(totalin));
			totallab = new jxl.write.Number(position.get("TOTALINAMOUNT"), j, totalin,doubleStyle);
			sheet.addCell(totallab);
			//totallab = new Label(position.get("TOTALOUTAMOUNT"), j, String.valueOf(totalout));
			totallab = new jxl.write.Number(position.get("TOTALOUTAMOUNT"), j, totalout,doubleStyle);
			sheet.addCell(totallab);
			//totallab = new Label(position.get("TOTALEAMOUNT"), j, String.valueOf(totale));
			totallab = new jxl.write.Number(position.get("TOTALEAMOUNT"), j, totale,doubleStyle);
			sheet.addCell(totallab);
			workBook.write();
			os.flush();
		} catch (Exception e) {
			throw new CRUDException(e);
		} finally {
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
