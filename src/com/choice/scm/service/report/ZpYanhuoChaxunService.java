package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class ZpYanhuoChaxunService {

	@Autowired
	private SupplyAcctMapper zpYanhuoChaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
		/**
		 * 直配验货查询
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findDireInspectionQuery(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
				sa.setFirm(CodeHelper.replaceCode(sa.getFirm()));
				List<Map<String,Object>> foot = zpYanhuoChaxunMapper.findCalForDireInspectionQuery(conditions);
				mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findDireInspectionQuery"));
				mapReportObject.setFooter(foot);
				mapReportObject.setTotal(pager.getCount());
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
	
	

}
