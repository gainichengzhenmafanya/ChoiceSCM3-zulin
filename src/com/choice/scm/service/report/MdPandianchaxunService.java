package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstore;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.persistence.report.MdPandianchaxunMapper;
@Service
public class MdPandianchaxunService {

	@Autowired
	private MdPandianchaxunMapper mdPandianchaxunMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 查询历史盘点
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findMdPandian(Chkstore chkstore,Page pager) throws CRUDException{
		try {
			List<Map<String,Object>> foot = mdPandianchaxunMapper.findMdPandianCount(chkstore);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(chkstore, pager, MdPandianchaxunMapper.class.getName()+".findMdPandian");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询历史盘点
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findMdPandian(Chkstore chkstore,Page pager, List<SupplyUnit> suList) throws CRUDException{
		try {
			Integer isReportJmj = 0;
			if(isReportJmj == 1)
				chkstore.setJmj("jmj");
			List<Map<String,Object>> foot = mdPandianchaxunMapper.findMdPandianDetailCount(chkstore);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(chkstore, pager, MdPandianchaxunMapper.class.getName()+".findMdPandianDetailQuery");
			if(!"jmj".equals(chkstore.getJmj())){
				for(Map<String,Object> map : listInfo){
					//1.从多规格map中取
					for(SupplyUnit su : suList){
						if(map.get("SP_CODE").equals(su.getSp_code())){
							if(1 == su.getSequence()){
								map.put("SPEC1", su.getUnit());
							}else if(2 == su.getSequence()){
								map.put("SPEC2", su.getUnit());
							}else if(3 == su.getSequence()){
								map.put("SPEC3", su.getUnit());
							}else if(4 == su.getSequence()){
								map.put("SPEC4", su.getUnit());
							}
						}
					}
				}
			}
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询历史盘点
	 * @param content
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findMdPandianMingxi(Chkstore chkstore,Page pager, List<SupplyUnit> suList) throws CRUDException{
		try {
			Integer isReportJmj = 0;
			if(isReportJmj == 1)
				chkstore.setJmj("jmj");
			List<Map<String,Object>> foot = mdPandianchaxunMapper.findMdPandianMingxiCount(chkstore);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(chkstore, pager, MdPandianchaxunMapper.class.getName()+".findMdPandianMingxiDetailQuery");
			if(!"jmj".equals(chkstore.getJmj())){
				for(Map<String,Object> map : listInfo){
					//1.从多规格map中取
					for(SupplyUnit su : suList){
						if(map.get("SP_CODE").equals(su.getSp_code())){
							if(1 == su.getSequence()){
								map.put("SPEC1", su.getUnit());
							}else if(2 == su.getSequence()){
								map.put("SPEC2", su.getUnit());
							}else if(3 == su.getSequence()){
								map.put("SPEC3", su.getUnit());
							}else if(4 == su.getSequence()){
								map.put("SPEC4", su.getUnit());
							}
						}
					}
				}
			}
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
