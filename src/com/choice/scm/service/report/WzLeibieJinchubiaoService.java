package com.choice.scm.service.report;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class WzLeibieJinchubiaoService {

	@Autowired
	private SupplyAcctMapper wzLeibieJinchubiaoMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private AcctMapper acctMapper;
	
	/**
	 * 物资类别进出表
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findSupplyTypInOut(Map<String,Object> conditions,Page pager) throws CRUDException{
		try {
			SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
			conditions.put("yearr", acctMapper.findYearrByDate(supplyAcct.getBdat()));
			//判断仓位
			if(null != supplyAcct && null != supplyAcct.getPositn() && !"".equals(supplyAcct.getPositn())){
				supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
			}
			String bdat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getBdat());
			String edat = new SimpleDateFormat("yyyy-MM-dd").format(supplyAcct.getEdat());
			conditions.put("bdat",bdat);
			conditions.put("edat",edat);
			int without0=0;//过滤零值标志：0-为选，1-已选
			String withoutzero="";//过滤零值条件
			
			if(conditions.get("without0")!=null && !conditions.get("without0").equals(""))
				without0=Integer.parseInt(conditions.get("without0").toString());
			if(without0==1){
//				withoutzero = " AND (CNTBLA!=0 OR AMTBLA!=0 OR CNTIN!=0 OR AMTIN!=0 OR CNTOUT!=0 "+
//						  "OR AMTOUT!=0 OR (CNTBLA+CNTIN-CNTOUT)!=0 OR (AMTBLA+AMTIN-AMTOUT)!=0)";
				withoutzero = " AND ( AMTBLA!=0 OR AMTIN!=0 OR AMTOUT!=0  OR (AMTBLA+AMTIN-AMTOUT)!=0)";
			}
			conditions.put("withoutzero",withoutzero);
			List<Map<String,Object>> foot = wzLeibieJinchubiaoMapper.findCalForSupplyTypInOut(conditions);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findSupplyTypInOut");
//			mapReportObject.setRows(wzLeibieJinchubiaoMapper.findSupplyTypInOut(conditions));
			mapReportObject.setFooter(foot);
			mapReportObject.setRows(listInfo);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
}
