package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.report.CustomReport;
import com.choice.scm.persistence.report.CustomReportScmMapper;

@Service
public class CustomReportScmService {
	@Autowired
	private CustomReportScmMapper customReportScmMapper;
	private static Logger log = Logger.getLogger(CustomReportScmService.class);
	
	/**
	 * 描述：查询自定义报表树
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-27
	 */
	public List<Map<String, Object>> listCustomReportTree() throws CRUDException{
		try{
			return customReportScmMapper.listCustomReportTree();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：保存自定义报表节点
	 * @param customReport
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-30
	 */
	public void saveCustomReport(CustomReport customReport) throws CRUDException{
		try{
			customReport.setPk_customReport(CodeHelper.createUUID().substring(0,20));
			 customReportScmMapper.saveCustomReport(customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：查询自定义报表对象
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-27
	 */
	public CustomReport searchReport(String pk_customReport) throws CRUDException {
		try{
			return customReportScmMapper.searchReport(pk_customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：根据id  查询自定义报表
	 * @param pk_customReport
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-6-6
	 */
	public CustomReport findCustomReport(String pk_customReport) throws CRUDException {
		try{
			return customReportScmMapper.findCustomReport(pk_customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：删除自定义报表节点
	 * @return
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-5-27
	 */
	public void deleteCustomReport(String pk_customReport) throws CRUDException {
		try{
			 customReportScmMapper.deleteCustomReport(pk_customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 描述：修改自定义报表节点
	 * @param customReport
	 * @throws CRUDException
	 * author:spt
	 * 日期：2014-6-6
	 */
	public void updateCustomReport(CustomReport customReport) throws CRUDException {
		try{
			customReportScmMapper.updateCustomReport(customReport);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
}
