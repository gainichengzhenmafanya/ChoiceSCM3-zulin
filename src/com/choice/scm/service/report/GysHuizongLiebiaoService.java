package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class GysHuizongLiebiaoService {

	@Autowired
	private SupplyAcctMapper gysHuizongLiebiaoMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
		/**
		 * 供应商汇总列表
		 * @param conditions
		 * @param pager
		 * @return
		 * @throws CRUDException 
		 */
		public ReportObject<Map<String,Object>> findDeliverSum(Map<String,Object> conditions,Page pager) throws CRUDException{
			try{
				SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
				conditions.put("chktyp", CodeHelper.replaceCode(sa.getChktyp()));
				conditions.put("delivertyp", CodeHelper.replaceCode(conditions.get("delivertyp").toString()));
				//判断供应商
				if(null != sa && null != sa.getDelivercode() && !"".equals(sa.getDelivercode())){
					sa.setDelivercode(CodeHelper.replaceCode(sa.getDelivercode()));
				}
				//判断仓位
				if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
					sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
				}
				List<Map<String,Object>> foot = gysHuizongLiebiaoMapper.findCalForDeliverSum(conditions);
				mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findDeliverSum"));
				mapReportObject.setFooter(foot);
				mapReportObject.setTotal(pager.getCount());
				return mapReportObject;
			}catch(Exception e){
				throw new CRUDException(e);
			}
		}
}
