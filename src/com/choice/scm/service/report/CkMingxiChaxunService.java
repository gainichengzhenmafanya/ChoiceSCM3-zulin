package com.choice.scm.service.report;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
@Service
public class CkMingxiChaxunService {

	@Autowired
	private SupplyAcctMapper ckMingxiChaxunMapper;
	@Autowired
	private ReportObject<SupplyAcct> reportObject;
	@Autowired
	private PageManager<SupplyAcct> pageManager;
	
	/**
	 * 出库明细查询
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<SupplyAcct> findChkoutDetailQuery(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			conditions.put("jmj", 0);
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			sa.setChktyp(CodeHelper.replaceCode(sa.getChktyp()));
			if(null != conditions.get("bysale") && conditions.get("bysale").equals("true")){
				List<Map<String,Object>> foot = ckMingxiChaxunMapper.findChkoutDQBySaleSUM(conditions);
				reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findChkoutDQBySale"));
				reportObject.setFooter(foot);
			}else{
				reportObject.setRows(pageManager.selectPage(conditions, page, SupplyAcctMapper.class.getName()+".findChkoutDetailQuery"));
				List<Map<String,Object>> foot = ckMingxiChaxunMapper.findChkoutDetailQuerySUM(conditions);
				reportObject.setFooter(foot);
			}
			reportObject.setTotal(page.getCount());
			return reportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
