package com.choice.scm.service.report;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.ShipAndReceiveMapper;

@Service
public class ShipAndReceivedService {

	private final transient Log log = LogFactory.getLog(ShipAndReceivedService.class);
	@Autowired
	private PageManager<SupplyAcct> pageManager;
	@Autowired
	private ShipAndReceiveMapper shipAndReceiveMapper;
	
	 
	
	/**
	 * 获得收货单数据（分店接受配送中心送货用）
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<SupplyAcct> findShipAndReceiveList(HashMap<String, Object> disMap, Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(disMap, page, ShipAndReceiveMapper.class.getName()+".findShipAndReceiveList");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<SupplyAcct> findShipAndReceiveList(HashMap<String, Object> disMap) throws CRUDException
	{
		try {
			return shipAndReceiveMapper.findShipAndReceiveList(disMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//导出Excel 
	public boolean exportExcel(OutputStream os,List<SupplyAcct> list) {   
		WritableWorkbook workBook = null;
		try {
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);
            sheet.addCell(new Label(0,0,"收发货报表"));
            //设置表格表头
            sheet.addCell(new Label(0, 2, "单号"));   
			sheet.addCell(new Label(1, 2, "编码"));  
			sheet.addCell(new Label(2, 2, "名称"));   
			sheet.addCell(new Label(3, 2, "规格"));  
			sheet.addCell(new Label(4, 2, "标准单位"));   
			sheet.addCell(new Label(5, 2, "参考单位"));  
			sheet.addCell(new Label(6, 2, "单价"));   
			sheet.addCell(new Label(7, 2, "配送数量"));  
			sheet.addCell(new Label(8, 2, "验货数量"));   
			sheet.addCell(new Label(9, 2, "金额"));  
			sheet.addCell(new Label(10, 2, "差异数量"));   
			sheet.addCell(new Label(11, 2, "备注"));  
			sheet.addCell(new Label(12, 2, "是否赠送"));   
            sheet.mergeCells(0, 0, list.size()-1, 0);
            //遍历list填充表格内容
            for(int i=0;i<list.size();i++) {
            	sheet.addCell(new Label(0, i+3, String.valueOf(list.get(i).getVouno())));
            	sheet.addCell(new Label(1, i+3, list.get(i).getSp_code()));
            	sheet.addCell(new Label(2, i+3, list.get(i).getSp_name()));
            	sheet.addCell(new Label(3, i+3, list.get(i).getSpdesc()));
            	sheet.addCell(new Label(4, i+3, list.get(i).getUnit()));
	    		sheet.addCell(new Label(5, i+3, list.get(i).getUnit1()));
	    		sheet.addCell(new Label(6, i+3, String.valueOf(list.get(i).getPriceout())));
	    		sheet.addCell(new Label(7, i+3, String.valueOf(list.get(i).getCntout())));
	    		sheet.addCell(new Label(8, i+3, String.valueOf(list.get(i).getCntfirm())));
	    		sheet.addCell(new Label(9, i+3, String.valueOf(list.get(i).getAmount())));
	    		sheet.addCell(new Label(10, i+3, String.valueOf(list.get(i).getCntfirm()-list.get(i).getCntout())));
	    		sheet.addCell(new Label(11, i+3,list.get(i).getDes()));
	    		sheet.addCell(new Label(12, i+3, String.valueOf(list.get(i).getIszs())));
	    		}
            workBook.write();
            os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}	
}
