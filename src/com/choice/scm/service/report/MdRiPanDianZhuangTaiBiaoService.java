package com.choice.scm.service.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.report.SupplyAcctMapper;
/**
 * 门店日盘点状态表
 * @author 孙胜彬
 */
@Service
public class MdRiPanDianZhuangTaiBiaoService {

	@Autowired
	private SupplyAcctMapper mdRiPanDianZhuangTaiBiao;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	

	/**
	 * 门店日盘点状态表
	 * @param conditions
	 * @param pager
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findMdRiPanDianZhuangTaiBiao(SupplyAcct conditions,Page pager) throws CRUDException{
		try{
			StringBuffer sql = new StringBuffer();
			StringBuffer str = new StringBuffer();
			Calendar ca = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			if(null==conditions.getBdat() || "".equals(conditions.getBdat())){
				conditions.setBdat(ca.getTime());
			}
			if(null==conditions.getEdat() || "".equals(conditions.getEdat())){
				conditions.setEdat(ca.getTime());
			}
			long bdat=conditions.getBdat().getTime();
			for(;bdat<=conditions.getEdat().getTime();bdat+=24*60*60*1000){
				Date t = new Date(bdat);
				str.append(","+'"'+df.format(t)+'"');
			}
			sql.append(str+" FROM( ");
		    sql.append("select P.CODE AS firmcode,P.DES AS firmdes FROM positn p WHERE p.TYP='1203' ) pa "); 
		    List<String> listDat=Arrays.asList(str.toString().split(","));
		    for(int i=1;i<listDat.size();i++){
		    	sql.append("left join ");
		    	sql.append("( select P.CODE AS firmcode,1 as "+listDat.get(i)+" FROM positn p LEFT JOIN POSITN_PAN PAN ON PAN.POSITNCD=P.CODE "); 
		    	sql.append(" WHERE p.TYP='1203' and pan.dat=to_date('"+listDat.get(i).substring(1,listDat.get(i).length()-1)+"','yyyy-MM-dd') ) a"+i+" on a"+i+".firmcode=pa.firmcode ");
		    }
		    conditions.setSql(sql.toString());
//			mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, SupplyAcctMapper.class.getName()+".findMdRiPanDianZhuangTaiBiao"));
		    List<Map<String, Object>>  list = mdRiPanDianZhuangTaiBiao.findMdRiPanDianZhuangTaiBiao(conditions);
		    mapReportObject.setRows(list);
			mapReportObject.setTotal(list.size());
//			mapReportObject.setFooter(list);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 动态生成表头
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public Object findFreetimeHeader(SupplyAcct condition) throws CRUDException {
		try {
			List<String> str = new ArrayList<String>();
			Calendar ca = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			if(null==condition.getBdat() || "".equals(condition.getBdat())){
				condition.setBdat(ca.getTime());
			}
			if(null==condition.getEdat() || "".equals(condition.getEdat())){
				condition.setEdat(ca.getTime());
			}
			long bdat=condition.getBdat().getTime();
			for(;bdat<=condition.getEdat().getTime();bdat+=24*60*60*1000){
				Date t = new Date(bdat);
				str.add(df.format(t));
			}
			return str;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
