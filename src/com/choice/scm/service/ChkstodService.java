package com.choice.scm.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.persistence.ChkstodMapper;

@Service
public class ChkstodService {

	@Autowired
	private ChkstodMapper chkstodMapper;
	@Autowired
	private PageManager<Chkstod> pageManager;

	private final transient Log log = LogFactory.getLog(ChkstodService.class);
	
	/**
	 * 查询报货单副表
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findChkstod(Chkstod chkstod,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkstod, page, ChkstodMapper.class.getName()+".findChkstod");
			//return chkstodMapper.findChkstod();
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 查找所有的报货单
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findAllChkstod(Chkstod chkstod) throws CRUDException
	{
		try {
			return chkstodMapper.findAllChkstod(chkstod);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据单号查询
	 * @param chkstod
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkstod> findByChkstoNo(Chkstod chkstod) throws CRUDException
	{
		try {
			return chkstodMapper.findByChkstoNo(chkstod);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改
	 * @param chkstod
	 * @throws CRUDException
	 */
	public void saveByUpdate(Chkstom chkstom) throws CRUDException
	{
		try {
			//报货单更新日志
			log.warn("报货单更新（主单）：\n" + chkstom);
			log.warn("报货单更新（详单）：" );
			for(Chkstod chkstod : chkstom.getChkstodList()){
				Map<String,Object> chkstodMap= new HashMap<String,Object>();
				chkstodMap.put("chkstoNo", chkstom.getChkstoNo());
				chkstodMap.put("sp_code", chkstod.getSupply().getSp_code());
				chkstodMap.put("amount", chkstod.getAmount());
				chkstodMap.put("amount1", chkstod.getAmount1());
				chkstodMap.put("totalAmt", chkstod.getTotalAmt());
				chkstodMap.put("price", chkstod.getPrice());
				chkstodMap.put("memo", chkstod.getMemo());
				chkstodMap.put("ind", chkstod.getInd());
				//报货单更新日志
				log.warn(chkstod);
				chkstodMapper.saveByUpdate(chkstodMap);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除报货单
	 * @param sp_codes
	 * @throws CRUDException
	 */
	public void delete(Map<String,Object> map) throws CRUDException
	{
		//删除报货单日志
		log.warn("删除报货单：\n");
		StringBuffer sbuff = new StringBuffer();
		Set<String> keys = map.keySet();
		for(String key : keys){
			sbuff.append(key).append(":").append(map.get(key)).append("\t");
		}
		log.warn(sbuff.toString());
		
		
		try {
			chkstodMapper.delete(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}

	/**
	 * 审核单条
	 * @param sp_codes
	 * @throws CRUDException
	 */
	public String checkOneChkstom(Chkstom chkstom) throws CRUDException
	{
		//审核报货单日志
		log.warn("审核报货单:\n" + chkstom);
		Map<String,String> result = new HashMap<String,String>();
		try {
			chkstodMapper.checkOneChkstom(chkstom);
			result.put("pr", chkstom.getPr().toString());	
			result.put("checby", chkstom.getChecby());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核多条
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	public String checkMoreChkstom(Chkstom chkstom,String chkstoNoIds) throws CRUDException
	{
		List<String> chkstonoList=Arrays.asList(chkstoNoIds.split(","));
		Map<String,String> result = new HashMap<String,String>();
		try {
			//审核报货单日志
			log.warn("审核报货单:\n" + chkstom + "\n单号：" + chkstoNoIds);
			for (int i = 0; i < chkstonoList.size(); i++) {
				chkstom.setChkstoNo(Integer.parseInt(chkstonoList.get(i)));
				chkstodMapper.checkOneChkstom(chkstom);	
				result.put("pr", chkstom.getPr().toString());	
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}