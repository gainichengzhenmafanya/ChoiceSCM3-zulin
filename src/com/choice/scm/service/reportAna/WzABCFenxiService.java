package com.choice.scm.service.reportAna;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class WzABCFenxiService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 查询物资ABC分析信息
	 * @param supplyAcct
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findSupplyABCStatistics(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			if(null != conditions && "detail".equals(conditions.get("typ"))){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAnaMapper.class.getName()+".findSupplyABCStatistics"));
				mapReportObject.setFooter(supplyAnaMapper.findSupplyABCStatisticsSumAll(conditions));
				mapReportObject.setTotal(page.getCount());
			}else{
				mapReportObject.setRows(supplyAnaMapper.findSupplyABCStatisticsSum(conditions));
				mapReportObject.setFooter(supplyAnaMapper.findSupplyABCStatisticsSumAll(conditions));
			}
			
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}	
}
