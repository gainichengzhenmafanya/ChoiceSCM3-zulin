package com.choice.scm.service.reportAna;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class CgCesuanService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
		
	/**
	 * 采购测算
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findCaiGouCeSuan(SupplyAcct supplyAcct,Page page) throws CRUDException{
		try{
			Map<String,Object> conditions = new HashMap<String, Object>();
			conditions.put("bdat", supplyAcct.getBdat());
			conditions.put("edat", supplyAcct.getEdat());
			conditions.put("positn", supplyAcct.getPositn());
			conditions.put("sp_code", CodeHelper.replaceCode(supplyAcct.getSp_code()));
			conditions.put("yearr", (mainInfoMapper.findYearrList().get(0)+""));
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAnaMapper.class.getName()+".findCaiGouCeSuan"));
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 采购测算计算
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public int changeCaiGouCeSuan(SupplyAcct supplyAcct) throws CRUDException{
		try{
			Map<String,Object> conditions = new HashMap<String, Object>();
			conditions.put("acct", supplyAcct.getAcct());
			conditions.put("bdat", supplyAcct.getBdat());
			conditions.put("edat", supplyAcct.getEdat());
			conditions.put("positn",supplyAcct.getPositn());
			
			supplyAnaMapper.changeCaiGouCeSuan(conditions);
			return (Integer)conditions.get("pr");
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
