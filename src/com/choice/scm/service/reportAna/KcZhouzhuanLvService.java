package com.choice.scm.service.reportAna;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class KcZhouzhuanLvService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
		
	/**
	 * 库存周转率
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findKuCunZhouZhuanLv(Map<String,Object> conditions,Page page,boolean detail) throws CRUDException{
		try{
			int year = Integer.valueOf(mainInfoMapper.findYearrList().get(0));
			String month = conditions.get("month").toString();
			int mon = Integer.valueOf(month);
			StringBuffer sqlsb = new StringBuffer("");
			for(int i=0;i<mon;i++){
				sqlsb.append("A.INA").append(i).append("-A.OUTA").append(i).append("+");
			}
			conditions.put("year", year);
			conditions.put("monSql", sqlsb.substring(0,sqlsb.lastIndexOf("+")));
			List<Map<String,Object>> foot = supplyAnaMapper.findKuCunZhouZhuanLvSum(conditions);
			if(detail){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAnaMapper.class.getName()+".findKuCunZhouZhuanLvDetail"));
			}else{
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAnaMapper.class.getName()+".findKuCunZhouZhuanLv"));
			}
			
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}
