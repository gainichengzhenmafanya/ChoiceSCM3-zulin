package com.choice.scm.service.reportAna;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.SupplyAnaMapper;
import com.choice.scm.service.GrpTypService;

@Service
public class CkZhoufahuoDuibiService {

	private final transient Log log = LogFactory.getLog(GrpTypService.class);
	
	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;

	/**
	 * 查询仓库周发货对比
	 * @param condition
	 * @param page
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	public ReportObject<Map<String,Object>> findCangKuZhouFaHuoDuiBi(Map<String,Object> condition,Page page) throws CRUDException{
		try {
			mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findCangKuZhouFaHuoDuiBi"));
			mapReportObject.setFooter(supplyAnaMapper.findCalForCangKuZhouFaHuoDuiBi(condition));
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
