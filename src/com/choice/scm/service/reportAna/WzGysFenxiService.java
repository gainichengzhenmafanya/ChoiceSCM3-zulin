package com.choice.scm.service.reportAna;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class WzGysFenxiService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	


	/**
	 * 根据传入的月份计算此月份的天数
	 * @param month
	 * @return
	 * @author ZGL
	 */
	public String getDateSql(int month){
		String dateSql = "";
		int[] mon31 = {1,3,5,7,8,10,12};
		int[] mon30 = {4,6,9,11};
		int year = new Date().getYear()+1900;
		if(month<=0){
			year -=1;
			month=month==0?12:11;
		}
		if(Arrays.binarySearch(mon31,month)>=0){
			dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/31', 'YYYY-MM-DD')>=A.DAT";
		}else if(Arrays.binarySearch(mon30,month)>=0){
			dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/30', 'YYYY-MM-DD')>=A.DAT";
		}else if(month==2){
			if((year%4==0 && year%100!=0) || year%400==0){
				dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/29', 'YYYY-MM-DD')>=A.DAT";
			}else{
				dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/28', 'YYYY-MM-DD')>=A.DAT";
			}				
		}
		return dateSql;
	}

	
	/**
	 * 物资供应商分析
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findSupplyDeliver(String spcode) throws CRUDException{
		try{
			mapReportObject.setRows(supplyAnaMapper.findSupplyDeliver(spcode));
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 物资供应商分析表头
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findSupplyDeliverHeaders(String spcode) throws CRUDException{
		try{
			return supplyAnaMapper.findSupplyDeliverHeaders(spcode);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}

		//导出物资供应商分析报表
		public boolean exportSupplyDeliver(OutputStream os,String spcode) throws Exception{
			List<Map<String,Object>> headers = supplyAnaMapper.findSupplyDeliverHeaders(spcode);
			List<Map<String,Object>> rows = supplyAnaMapper.findSupplyDeliver(spcode);
			String[] subCol = {"入库数量","入库金额","平均单价","最高单价","最低单价"}; 
			String[] VarKey = {"CNT","AMT","PRICEAVG","PRICEMAX","PRICEMIN"};
			Map<String,Object> total = new HashMap<String,Object>();
			total.put("CODE", "TOTAL");
			total.put("DES", "合计");
			headers.add(total);
			WritableWorkbook workBook = null;
			try{
				//记录表头位置
				Map<String,Integer> position = new HashMap<String,Integer>();
				workBook = Workbook.createWorkbook(os);
				//新建工作表
				WritableSheet sheet = workBook.createSheet("物资供应商分析", 0);
				//定义label
				Label label = new Label(0,0,"物资供应商分析");
				//添加label到cell
	            sheet.addCell(label);
	            sheet.mergeCells(0, 0, 1+headers.size()*5, 0);
	            //设置表格表头
	            Label headfirst = new Label(0,1,"年度");
	            sheet.addCell(headfirst);
	            sheet.mergeCells(0, 1, 0, 2);
	            position.put("YEARR", 0);
	            Label headsecond = new Label(1,1,"月份");
	            sheet.addCell(headsecond);
	            sheet.mergeCells(1, 1, 1, 2);
	            position.put("MONTHH", 1);
	            int index = 2;//记录单元格列号
	            for(int i = 0 ; i < headers.size();i ++){
	            	Map<String,Object> header = headers.get(i);
	            	Label head = new Label(index,1,header.get("DES").toString());
	            	sheet.addCell(head);
	            	sheet.mergeCells(index, 1, index + 4, 1);
	            	for(String str : subCol){
	            		position.put(header.get("CODE")+str, index);
	            		Label subHead = new Label(index ++ ,2,str);
	            		sheet.addCell(subHead);
	            	}
	            }
	            	
	            //遍历list填充表格内容
	            if(rows.size()!=0){
	            	int month = Integer.parseInt(rows.get(0).get("MONTHH").toString());
		            int rowIndex = 2;
		            for(int i = 0 ; i < rows.size() ; i ++ ){
		            	Map<String,Object> row = rows.get(i);
		            	int m = Integer.parseInt(row.get("MONTHH").toString());
		        		if(month != m || i == 0){
		        			rowIndex ++;
		        			month = m;
		        			Label lab = new Label(0,rowIndex,row.get("YEARR").toString());
			    			sheet.addCell(lab);
			    			lab = new Label(1,rowIndex,row.get("MONTHH").toString());
			    			sheet.addCell(lab);
		        		}
		        		for(int j = 0 ; j < subCol.length ; j ++){
		        			Label lab = new Label(position.get(row.get("DELIVER")+subCol[j]),rowIndex,String.format("%-5.2f", Double.parseDouble(row.get(VarKey[j]).toString())));
		        			sheet.addCell(lab);
		        		}
		            }
	            }
	            workBook.write();
	            os.flush();
			}catch (Exception e) {
				throw new CRUDException(e);
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return true;
			
			
		}
	/**
	 * 入库成本分析
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findReceiptCostAnalysisSum(Map<String,Object> condition,Page page) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)condition.get("supplyAcct");
			String inabal="CAST((";
			String ina="";
			String conditions = "";
			int month = Integer.parseInt(condition.get("month").toString());
			if(null!= supplyAcct.getPositn()&& supplyAcct.getPositn()!=""){
				String str = "'"+supplyAcct.getPositn()+"'";
				str = str.replace(",", "','");
				conditions += " AND A.POSITN IN ("+str+")";
			}
			if(null!=supplyAcct.getGrptyp() && supplyAcct.getGrptyp()!=""){
				String str = "'"+supplyAcct.getGrptyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.GRPTYP IN ("+str+")";
			}
			if(null!= supplyAcct.getGrp()&& supplyAcct.getGrp()!=""){
				String str = "'"+supplyAcct.getGrp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.GRP IN ("+str+")";
			}
			if(null!= supplyAcct.getTyp()&& supplyAcct.getTyp()!=""){
				String str = "'"+supplyAcct.getTyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.SP_TYPE IN ("+str+")";
			}
			for(int i=1;i<=(new Date().getMonth()+1);i++){
				inabal += "A.INA"+i+"+";
				//incbal += "A.INC"+i+"+";
			}
			inabal = inabal.substring(0,inabal.length()-1)+") AS NUMERIC(12,2)) AS INABAL";
		//	incbal = incbal.substring(0,incbal.length()-1)+") AS NUMERIC(12,2)) AS INCBAL";
			ina = "CAST(A.INA"+month+" AS NUMERIC(12,2)) AS INA";
		//	inc = "CAST(A.INC"+month+" AS NUMERIC(12,2)) AS INC";
	        condition.put("inabal", inabal);
	        condition.put("year", DateFormat.getStringByDate(new Date(),"yyyy"));
	        condition.put("conditions",conditions);
	        condition.put("month1", getDateSql(month-1));
			if(null != conditions && "detail".equals(((SupplyAcct)condition.get("supplyAcct")).getTypdes())){
				mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findReceiptCostAnalysisDetail"));
				mapReportObject.setFooter(supplyAnaMapper.findReceiptCostAnalysisDetail_total(condition));
			}else
				mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findReceiptCostAnalysisSum"));
			mapReportObject.setFooter(supplyAnaMapper.findReceiptCostAnalysisSum_total(condition));
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
	/**
	 * 资金占用情况
	 * @param condition
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findZiJinZhanYongQingKuang(Map<String,Object> condition,Page page) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)condition.get("supplyAcct");
			condition.put("bdat", DateFormat.formatDate(sa.getBdat(), "yyyy-MM-dd"));
			condition.put("edat", DateFormat.formatDate(sa.getEdat(), "yyyy-MM-dd"));
			condition.put("year", DateFormat.getYear(sa.getBdat()));
			condition.put("positn", CodeHelper.replaceCode(sa.getPositn()));
			condition.put("grptyp", CodeHelper.replaceCode(sa.getGrptyp()));
			condition.put("grp", CodeHelper.replaceCode(sa.getGrp()));
			
			mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findZiJinZhanYongQingKuang"));
			mapReportObject.setFooter(supplyAnaMapper.findZiJinZhanYongQingKuangSum(condition));
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
	/**
	 * 周入库成本分析
	 * @param condition
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findZhouRuKuChengBenFenXi(Map<String,Object> condition,Page page) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)condition.get("supplyAcct");
			Calendar   calendar=Calendar.getInstance(); 
			calendar.setTime(supplyAcct.getDat());
			int   weekOfYear=calendar.get(Calendar.WEEK_OF_YEAR);
			String conditions = "";
//			if(null!= supplyAcct.getPositn()&& supplyAcct.getPositn()!=""){
//				String str = "'"+supplyAcct.getPositn()+"'";
//				str = str.replace(",", "','");
//				conditions += " AND A.POSITN IN ("+str+")";
//			}
			if(null!=supplyAcct.getGrptyp() && supplyAcct.getGrptyp()!=""){
				String str = "'"+supplyAcct.getGrptyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND P.GRPTYP IN ("+str+")";
			}
			if(null!= supplyAcct.getGrp()&& supplyAcct.getGrp()!=""){
				String str = "'"+supplyAcct.getGrp()+"'";
				str = str.replace(",", "','");
				conditions += " AND P.CODE IN ("+str+")";
			} 
			condition.put("year", DateFormat.getStringByDate(new Date(),"yyyy"));
			condition.put("conditions",conditions);
			condition.put("weekOfYear",weekOfYear-1);//时间在今年是第几周
			condition.put("weekOfYear1",weekOfYear);//时间在今年是第几周+1
			mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findZhouRuKuChengBenFenXi"));
			mapReportObject.setFooter(supplyAnaMapper.findZhouRuKuChengBenFenXi_total(condition));
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
	
}
