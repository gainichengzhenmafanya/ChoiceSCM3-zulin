package com.choice.scm.service.reportAna;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class JgYujingBizhongService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private PositnMapper positnMapper;
	/**
	 * 查询价格预警与比重报表内容
	 * @param condition
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	public ReportObject<Map<String,Object>> findPriceWarning(Map<String, Object> condition, Page pager) throws CRUDException {
		try{
			SupplyAcct supplyAcct = (SupplyAcct)condition.get("supplyAcct");
			String inabal="ROUND(SUM(";//本年进货总额
			String ljbz="SUM(";//比重
			String incbal="ROUND(SUM(";//本年进货量
			String ina,inc,incex,incex2,inaex,inaex2="";//本月金额，本月数量，上月数量，上上月数量，上月金额，上上月金额
			String conditions = "";//查询条件
			int month1,month2,year1,year2=0;//上月所在月份，上上月所在月份，上月所在年份，上上月所在年份
			int month = Integer.parseInt(condition.get("month").toString());
			if(supplyAcct.getPositn()!=null && supplyAcct.getPositn()!=""){
				String str = "'"+supplyAcct.getPositn()+"'";
				str = str.replace(",", "','");
				conditions += " AND A.POSITN IN ("+str+")";
			}
			if(supplyAcct.getGrptyp()!=null && supplyAcct.getGrptyp()!=""){
				String str = "'"+supplyAcct.getGrptyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.GRPTYP IN ("+str+")";
			}
			if(supplyAcct.getGrp()!=null && supplyAcct.getGrp()!=""){
				String str = "'"+supplyAcct.getGrp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.GRP IN ("+str+")";
			}
			if(supplyAcct.getTyp()!=null && supplyAcct.getTyp()!=""){
				String str = "'"+supplyAcct.getTyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.SP_TYPE IN ("+str+")";
			}
			for(int i=1;i<=(new Date().getMonth()+1);i++){
				inabal += "A.INA"+i+"+";
				ljbz += "A.INA"+i+"+";
				incbal += "A.INC"+i+"+";
			}
			ljbz = ljbz.substring(0,ljbz.length()-1)+")  ";
			inabal = inabal.substring(0,inabal.length()-1)+"),2) AS INABAL";
			incbal = incbal.substring(0,incbal.length()-1)+"),2) AS INCBAL";
			ina = "ROUND(SUM(A.INA"+month+"),2) AS INA";
			inc = "ROUND(SUM(A.INC"+month+"),2) AS INC";
			//判断上月及前月所在月份和年份
			if(month==2){month1=1;month2=12;year1=new Date().getYear()+1900;year2=new Date().getYear()+1899;}
			else if(month==1){month1=12;month2=11;year1=year2=new Date().getYear()+1899;}
			else{month1=month-1;month2=month-2;year1=year2=new Date().getYear()+1900;}
			incex2 = "ROUND(SUM(A.INC"+month2+"),2) AS INCEX2";
			incex = "ROUND(SUM(A.INC"+month1+"),2) AS INCEX";
	        inaex2 = "ROUND(SUM(A.INA"+month2+"),2) AS INAEX2";
	        inaex = "ROUND(SUM(A.INA"+month1+"),2) AS INAEX";
	        
	        condition.put("inabal", inabal);
	        condition.put("ljbz", ljbz);
	        condition.put("incbal", incbal);
	        condition.put("ina", ina);
	        condition.put("inc", inc);
	        condition.put("inaex", inaex);
	        condition.put("inaex2", inaex2);
	        condition.put("incex", incex);
	        condition.put("incex2", incex2);
	        condition.put("conditions",conditions);
	        condition.put("month0", getDateSql(month));
	        condition.put("month1", getDateSql(month-1));
	        condition.put("month2", getDateSql(month-2));
	        condition.put("year", DateFormat.getStringByDate(new Date(),"yyyy"));
	        condition.put("year1",year1);
	        condition.put("year2",year2);
			//List<Map<String,Object>> foot = supplyAnaMapper.findCalForPriceWarning(condition);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(condition, pager, SupplyAnaMapper.class.getName()+".findPriceWarning");
			
			//计算全年累计比重,去掉查询条件
						String conditions2 = "";//查询条件		
						Positn positn=new Positn();
						positn.setTypn("'基地仓库'");
						 List<Positn> listPositn=positnMapper.findPositnSuper(positn);
						if(listPositn!=null && listPositn.size()>0){
							String str="'";
							for (int i = 0; i < listPositn.size(); i++) {
								str=str+","+listPositn.get(i).getCode();
							}
							str = str.replace(",", "','");
							conditions2 += " AND A.POSITN IN ("+str+"'"+")";
						}
						supplyAcct.setSp_code("");
						supplyAcct.setGrp("");
						supplyAcct.setGrptyp("");
						supplyAcct.setType("");
						condition.put("conditions", conditions2);
						condition.put("ups", "");
						condition.put("supplyAcct", supplyAcct);
						List<Map<String,Object>> listInfoALL = supplyAnaMapper.findPriceWarning(condition);
			
			//循环计算总的累积 累计比重， sql算不出来    xlh   2015.01.03  
			double  sumljz = 0;
			for (int i = 0; i < listInfoALL.size(); i++) {
				sumljz=sumljz+Double.parseDouble(listInfoALL.get(i).get("LJBZ").toString());
			}
			DecimalFormat df=new DecimalFormat(".##");
			
			List<Map<String,Object>> listInfoSupper= new ArrayList<Map<String, Object>>(3);
			for (int i = 0; i < listInfo.size(); i++) {
				double ljbz1=Double.parseDouble(listInfo.get(i).get("LJBZ").toString())/sumljz *100;
				double fluctuations1=Double.parseDouble(listInfo.get(i).get("PRICERATE").toString());
				if(condition.get("proportion")!=null){//总的比重
					String proportion=(String)condition.get("proportion");
					if (Double.parseDouble(proportion) > ljbz1){
						continue;
					}
				}
				if(condition.get("fluctuations")!=null){//波动幅度
					String fluctuations=(String)condition.get("fluctuations");
					if (Double.parseDouble(fluctuations) > fluctuations1){
						continue;
					}
				}
				listInfo.get(i).put("LJBZ",df.format(ljbz1)+"%");
				listInfo.get(i).put("PRICERATE",df.format(fluctuations1)+"%");
				listInfoSupper.add(listInfo.get(i));
			}
			mapReportObject.setRows(listInfoSupper);
			//mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据传入的月份计算此月份的天数
	 * @param month
	 * @return
	 * @author ZGL
	 */
	public String getDateSql(int month){
		String dateSql = "";
		int[] mon31 = {1,3,5,7,8,10,12};
		int[] mon30 = {4,6,9,11};
		int year = new Date().getYear()+1900;
		if(month<=0){
			year -=1;
			month=month==0?12:11;
		}
		if(Arrays.binarySearch(mon31,month)>=0){
			dateSql = " AND A.DAT >= STR_TO_DATE('"+year+"/"+month+"/1', '%Y/%m/%d') AND STR_TO_DATE('"+year+"/"+month+"/31', '%Y/%m/%d')>=A.DAT";
		}else if(Arrays.binarySearch(mon30,month)>=0){
			dateSql = " AND A.DAT >= STR_TO_DATE('"+year+"/"+month+"/1', '%Y/%m/%d') AND STR_TO_DATE('"+year+"/"+month+"/30', '%Y/%m/%d')>=A.DAT";
		}else if(month==2){
			if((year%4==0 && year%100!=0) || year%400==0){
				dateSql = " AND A.DAT >= STR_TO_DATE('"+year+"/"+month+"/1', '%Y/%m/%d') AND STR_TO_DATE('"+year+"/"+month+"/29', '%Y/%m/%d')>=A.DAT";
			}else{
				dateSql = " AND A.DAT >= STR_TO_DATE('"+year+"/"+month+"/1', '%Y/%m/%d') AND STR_TO_DATE('"+year+"/"+month+"/28', '%Y/%m/%d')>=A.DAT";
			}				
		}
		return dateSql;
	}
}
