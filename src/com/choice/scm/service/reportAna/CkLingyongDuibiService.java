package com.choice.scm.service.reportAna;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.SupplyAnaMapper;
import com.choice.scm.service.GrpTypService;

@Service
public class CkLingyongDuibiService {

	private final transient Log log = LogFactory.getLog(GrpTypService.class);
	
	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;

	/**
	 * 得到仓库领用对比表头
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	public List<Map<String, Object>> findWarehouseRequisitionedContrastHeader(Map<String, Object> condition) throws CRUDException {
		try {
			return supplyAnaMapper.findWarehouseRequisitionedContrastHeader(condition);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询仓库领用对比
	 * @param condition
	 * @param page
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	public ReportObject<Map<String,Object>> findCangKuLingYongDuiBi(Map<String,Object> condition,Page page) throws CRUDException{
		try {
			mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findCangKuLingYongDuiBi"));
			mapReportObject.setFooter(supplyAnaMapper.findCalForCangKuLingYongDuiBi(condition));
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 导出仓库领用对比Excel
	 * @param os
	 * @param condition
	 * @throws Exception
	 * @author ZGL
	 */
	public void exportCangKuLingYongDuiBi(OutputStream os,Map<String,Object> condition) throws Exception{
		List<Map<String,Object>> headers = supplyAnaMapper.findWarehouseRequisitionedContrastHeader(condition);
		List<Map<String,Object>> rows = supplyAnaMapper.findCangKuLingYongDuiBi(condition);
		List<Map<String,Object>> footer = supplyAnaMapper.findCalForCangKuLingYongDuiBi(condition);
		String[] subCol = {"数量","金额"}; 
		String[] VarKey = {"CNT","AMT"};
		WritableWorkbook workBook = null;
		try{
			//记录表头位置
			Map<String,Integer> position = new HashMap<String,Integer>();
			workBook = Workbook.createWorkbook(os);
			//新建工作表
			WritableSheet sheet = workBook.createSheet("仓库领用对比", 0);
			//定义label
			Label label = new Label(0,0,"仓库领用对比");
			//添加label到cell
            sheet.addCell(label);
            sheet.mergeCells(0, 0, 3+headers.size()*2, 0);
            //设置表格表头
            Label headfirst = new Label(0,1,"物资编码");
            sheet.addCell(headfirst);
            sheet.mergeCells(0, 1, 0, 2);
            position.put("SP_CODE", 0);
            Label headsecond = new Label(1,1,"物资名称");
            sheet.addCell(headsecond);
            sheet.mergeCells(1, 1, 1, 2);
            position.put("SP_NAME", 1);
            Label headthird = new Label(2,1,"物资规格");
            sheet.addCell(headthird);
            sheet.mergeCells(2, 1, 2, 2);
            position.put("SPDESC", 2);
            Label headfourth = new Label(3,1,"物资名称");
            sheet.addCell(headfourth);
            sheet.mergeCells(3, 1, 3, 2);
            position.put("UNIT", 3);
            int index = 4;//记录单元格列号
            for(int i = 0 ; i < headers.size();i ++){
            	Map<String,Object> header = headers.get(i);
            	Label head = new Label(index,1,header.get("DES").toString());
            	sheet.addCell(head);
            	sheet.mergeCells(index, 1, index + 1, 1);
            	for(String str : subCol){
            		position.put(header.get("CODE")+str, index);
            		Label subHead = new Label(index ++ ,2,str);
            		sheet.addCell(subHead);
            	}
            }
            	
            //遍历list填充表格内容
            int spcode = Integer.parseInt(rows.get(0).get("SP_CODE").toString());
            int rowIndex = 2;
            for(int i = 0 ; i < rows.size() ; i ++ ){
            	Map<String,Object> row = rows.get(i);
            	int m = Integer.parseInt(row.get("SP_CODE").toString());
        		if(spcode != m || i == 0){
        			rowIndex ++;
        			spcode = m;
        			Label lab = new Label(0,rowIndex,row.get("SP_CODE").toString());
	    			sheet.addCell(lab);
	    			lab = new Label(1,rowIndex,row.get("SP_NAME").toString());
	    			sheet.addCell(lab);
	    			lab = new Label(2,rowIndex,null==row.get("SPDESC")?"":row.get("SPDESC").toString());
	    			sheet.addCell(lab);
	    			lab = new Label(3,rowIndex,row.get("UNIT").toString());
	    			sheet.addCell(lab);
        		}
        		for(int j = 0 ; j < subCol.length ; j ++){
        			Label lab = new Label(position.get(row.get("FIRM")+subCol[j]),rowIndex,String.format("%-5.2f", Double.parseDouble(row.get(VarKey[j]).toString())));
        			sheet.addCell(lab);
        		}
            }
            rowIndex = rowIndex+1;
    		for(int x=0;x<footer.size();x++){
    			Map<String,Object> foot = new HashMap<String,Object>();
    			foot = footer.get(x);
    			Label labe = new Label(0,rowIndex,"总计");
    			sheet.addCell(labe);
    			for(int n=0;n<subCol.length;n++){
    				Label lab = new Label(position.get(foot.get("FIRM")+subCol[n]),rowIndex,String.format("%-5.2f",Double.parseDouble(foot.get(VarKey[n]).toString())));
    				sheet.addCell(lab);
    			}
    		}
            workBook.write();
            os.flush();
		}catch (Exception e) {
			throw new CRUDException(e);
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
