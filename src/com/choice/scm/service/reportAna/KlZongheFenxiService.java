package com.choice.scm.service.reportAna;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class KlZongheFenxiService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;	

	/**
	 * 查询库龄信息
	 * @param supplyAcct
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findInventoryAgingSum(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			if(null != conditions && "detail".equals(((SupplyAcct)conditions.get("supplyAcct")).getTyp())){
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAnaMapper.class.getName()+".findInventoryAgingDetail"));
			}else{
				mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAnaMapper.class.getName()+".findInventoryAgingSum"));
			}
			mapReportObject.setFooter(supplyAnaMapper.findCalForInventoryAging(conditions));
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
}
