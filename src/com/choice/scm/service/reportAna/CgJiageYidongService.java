package com.choice.scm.service.reportAna;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class CgJiageYidongService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 采购价格异动分析
	 * @param condition
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	@SuppressWarnings("deprecation")
	public ReportObject<Map<String,Object>> findPurchasePriceChange(Map<String, Object> condition, Page pager) throws CRUDException{
		try{
			int month = null == condition.get("month") ? new Date().getMonth() + 1 :  Integer.parseInt(condition.get("month").toString());
			condition.put("month", month);
			mapReportObject.setRows(mapPageManager.selectPage(condition, pager, SupplyAnaMapper.class.getName()+".findPurchasePriceChange"));
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 采购价格异动分析1
	 * @param condition
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findPurchasePriceChange1(Map<String, Object> condition, Page pager) throws CRUDException{
		try{
			mapReportObject.setRows(mapPageManager.selectPage(condition, pager, SupplyAnaMapper.class.getName()+".findPurchasePriceChange1"));
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
}
