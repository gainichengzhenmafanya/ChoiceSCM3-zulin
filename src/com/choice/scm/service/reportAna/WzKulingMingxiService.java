package com.choice.scm.service.reportAna;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class WzKulingMingxiService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 物资库龄明细
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findSupplyKuLingDetail(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			List<Map<String,Object>> foot = supplyAnaMapper.findSupplyKuLingDetailSum(conditions);
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, SupplyAnaMapper.class.getName()+".findSupplyKuLingDetail"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}	
}
