package com.choice.scm.service.reportAna;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class ZjZhanyongQingkuangService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 资金占用情况
	 * @param condition
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findZiJinZhanYongQingKuang(Map<String,Object> condition,Page page) throws CRUDException{
		try{
			SupplyAcct sa = (SupplyAcct)condition.get("supplyAcct");
			condition.put("bdat", DateFormat.formatDate(sa.getBdat(), "yyyy-MM-dd"));
			condition.put("edat", DateFormat.formatDate(sa.getEdat(), "yyyy-MM-dd"));
			condition.put("year", DateFormat.getYear(sa.getBdat()));
			condition.put("positn", CodeHelper.replaceCode(sa.getPositn()));
			condition.put("grptyp", CodeHelper.replaceCode(sa.getGrptyp()));
			condition.put("grp", CodeHelper.replaceCode(sa.getGrp()));
			
			mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findZiJinZhanYongQingKuang"));
			mapReportObject.setFooter(supplyAnaMapper.findZiJinZhanYongQingKuangSum(condition));
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
}
