package com.choice.scm.service.reportAna;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class BhgJinhuoTongjiService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 查询不合格进货统计
	 * @param condition
	 * @param pager
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 */
	public ReportObject<Map<String,Object>> findUnqualifiedStatistics(Map<String, Object> condition, Page pager) throws CRUDException {
	  try {
			List<Map<String,Object>> foot = supplyAnaMapper.findCalForUnqualifiedStatistics(condition);
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(condition, pager, SupplyAnaMapper.class.getName()+".findUnqualifiedStatistics");
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
