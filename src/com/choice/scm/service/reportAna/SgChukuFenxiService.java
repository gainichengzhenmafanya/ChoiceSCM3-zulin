package com.choice.scm.service.reportAna;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class SgChukuFenxiService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 * 查询申购出库分析
	 * @param condition
	 * @param pager
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findApplyOutAmount(Map<String, Object> condition,Page pager) throws CRUDException{
		try {
			List<Map<String,Object>> listInfo = mapPageManager.selectPage(condition, pager, SupplyAnaMapper.class.getName()+".findApplyOutAmount");
			
			Map<String,Object> footMap = new HashMap<String, Object>();
			List<Map<String,Object>> preFoot = supplyAnaMapper.findApplyOutAmountCount(condition);
			footMap.put("OUTAMOUNT", preFoot.get(0).get("COUNT"));
			footMap.put("APPLYAMOUNT", preFoot.get(1).get("COUNT"));
			footMap.put("DELIVER", "合计");
			footMap.put("SP_CODE", pager.getCount());
			
			List<Map<String,Object>> foot = new ArrayList<Map<String,Object>>();
			foot.add(footMap);
			
			mapReportObject.setRows(listInfo);
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
}
