package com.choice.scm.service.reportAna;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.SupplyAnaMapper;

@Service
public class RkChengbenFenxiService {

	@Autowired
	private SupplyAnaMapper supplyAnaMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;

	/**
	 * 根据传入的月份计算此月份的天数
	 * @param month
	 * @return
	 * @author ZGL
	 */
	public String getDateSql(int month){
		String dateSql = "";
		int[] mon31 = {1,3,5,7,8,10,12};
		int[] mon30 = {4,6,9,11};
		int year = new Date().getYear()+1900;
		if(month<=0){
			year -=1;
			month=month==0?12:11;
		}
		if(Arrays.binarySearch(mon31,month)>=0){
			dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/31', 'YYYY-MM-DD')>=A.DAT";
		}else if(Arrays.binarySearch(mon30,month)>=0){
			dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/30', 'YYYY-MM-DD')>=A.DAT";
		}else if(month==2){
			if((year%4==0 && year%100!=0) || year%400==0){
				dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/29', 'YYYY-MM-DD')>=A.DAT";
			}else{
				dateSql = " AND A.DAT >= TO_DATE('"+year+"/"+month+"/1', 'YYYY-MM-DD') AND TO_DATE('"+year+"/"+month+"/28', 'YYYY-MM-DD')>=A.DAT";
			}				
		}
		return dateSql;
	}
	/**
	 * 入库成本分析
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findReceiptCostAnalysisSum(Map<String,Object> condition,Page page) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)condition.get("supplyAcct");
			String inabal="CAST((";
			String ina="";
			String conditions = "";
			int month = Integer.parseInt(condition.get("month").toString());
			if(null!= supplyAcct.getPositn()&& supplyAcct.getPositn()!=""){
				String str = "'"+supplyAcct.getPositn()+"'";
				str = str.replace(",", "','");
				conditions += " AND A.POSITN IN ("+str+")";
			}
			if(null!=supplyAcct.getGrptyp() && supplyAcct.getGrptyp()!=""){
				String str = "'"+supplyAcct.getGrptyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.GRPTYP IN ("+str+")";
			}
			if(null!= supplyAcct.getGrp()&& supplyAcct.getGrp()!=""){
				String str = "'"+supplyAcct.getGrp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.GRP IN ("+str+")";
			}
			if(null!= supplyAcct.getTyp()&& supplyAcct.getTyp()!=""){
				String str = "'"+supplyAcct.getTyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND B.SP_TYPE IN ("+str+")";
			}
			for(int i=1;i<=(new Date().getMonth()+1);i++){
				inabal += "A.INA"+i+"+";
				//incbal += "A.INC"+i+"+";
			}
			inabal = inabal.substring(0,inabal.length()-1)+") AS NUMERIC(12,2)) AS INABAL";
		//	incbal = incbal.substring(0,incbal.length()-1)+") AS NUMERIC(12,2)) AS INCBAL";
			ina = "CAST(A.INA"+month+" AS NUMERIC(12,2)) AS INA";
		//	inc = "CAST(A.INC"+month+" AS NUMERIC(12,2)) AS INC";
	        condition.put("inabal", inabal);
	        condition.put("year", DateFormat.getStringByDate(new Date(),"yyyy"));
	        condition.put("conditions",conditions);
	        condition.put("month1", getDateSql(month-1));
			if(null != conditions && "detail".equals(((SupplyAcct)condition.get("supplyAcct")).getTypdes())){
				mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findReceiptCostAnalysisDetail"));
				mapReportObject.setFooter(supplyAnaMapper.findReceiptCostAnalysisDetail_total(condition));
			}else{//2014.10.22 wjf
				mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findReceiptCostAnalysisSum"));
				mapReportObject.setFooter(supplyAnaMapper.findReceiptCostAnalysisSum_total(condition));
			}
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
	/**
	 * 周入库成本分析
	 * @param condition
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findZhouRuKuChengBenFenXi(Map<String,Object> condition,Page page) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)condition.get("supplyAcct");
			Calendar   calendar=Calendar.getInstance(); 
			calendar.setTime(supplyAcct.getDat());
			int   weekOfYear=calendar.get(Calendar.WEEK_OF_YEAR);
			String conditions = "";
//			if(null!= supplyAcct.getPositn()&& supplyAcct.getPositn()!=""){
//				String str = "'"+supplyAcct.getPositn()+"'";
//				str = str.replace(",", "','");
//				conditions += " AND A.POSITN IN ("+str+")";
//			}
			if(null!=supplyAcct.getGrptyp() && supplyAcct.getGrptyp()!=""){
				String str = "'"+supplyAcct.getGrptyp()+"'";
				str = str.replace(",", "','");
				conditions += " AND P.GRPTYP IN ("+str+")";
			}
			if(null!= supplyAcct.getGrp()&& supplyAcct.getGrp()!=""){
				String str = "'"+supplyAcct.getGrp()+"'";
				str = str.replace(",", "','");
				conditions += " AND P.CODE IN ("+str+")";
			} 
			condition.put("year", DateFormat.getStringByDate(new Date(),"yyyy"));
			condition.put("conditions",conditions);
			if(weekOfYear==1){
				condition.put("lastyear",DateFormat.getYear(new Date())-1);
				condition.put("weekOfYear",53);//上周
			}else{
				condition.put("weekOfYear",weekOfYear-1);//上周
				condition.put("lastyear",DateFormat.getYear(new Date()));
			}
			condition.put("weekOfYear1",weekOfYear);//本周
			mapReportObject.setRows(mapPageManager.selectPage(condition, page, SupplyAnaMapper.class.getName()+".findZhouRuKuChengBenFenXi"));
			mapReportObject.setFooter(supplyAnaMapper.findZhouRuKuChengBenFenXi_total(condition));
			mapReportObject.setTotal(page.getCount());
		}catch(Exception e){
			throw new CRUDException(e);
		}
		return mapReportObject;
	}
}
