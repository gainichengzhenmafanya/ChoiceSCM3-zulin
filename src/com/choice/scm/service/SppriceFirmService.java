package com.choice.scm.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.AccountService;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.persistence.SppriceFirmMapper;

@Service
public class SppriceFirmService {
	
	private static Logger log = Logger.getLogger(AccountService.class);
	@Autowired
	SppriceFirmMapper sppriceFirmMapper;
	@Autowired
	private PageManager<Spprice> pageManager;
	@Autowired
	private PageManager<SppriceSale> pageSaleManager;

	/**
	 * 分店复制
	 * @param sppriceDemo
	 * @return
	 */
	public void saveByCopy(SppriceDemo sppriceDemo)throws CRUDException{
		try {
			String[] positnCodes = sppriceDemo.getPositnCode().split(",");//选择的分店
			String[] firmCodes = sppriceDemo.getFirm().split(",");//复制到的分店
			for(String positnCode:positnCodes){
				for(String firmCode:firmCodes){
					sppriceDemo.setPositnCode(positnCode);
					sppriceDemo.setFirm(firmCode);
					if ("0".equals(sppriceDemo.getType())) {//报价
						sppriceFirmMapper.saveByCopySpprice(sppriceDemo);
					}else if ("1".equals(sppriceDemo.getType())){//售价
						sppriceFirmMapper.saveByCopySppriceSale(sppriceDemo);	
					}
				}
			}
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询门店报价
	 * @param spprice
	 * @return
	 * @throws CRUDException
	 */
	public List<Spprice> findFirmPrice(Spprice spprice,Page page) throws CRUDException{
		try{
//			return sppriceFirmMapper.findFirmPrice(spprice);
			return pageManager.selectPage(spprice, page, SppriceFirmMapper.class.getName()+".findFirmPrice");
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询门店售价
	 * @param sale
	 * @return
	 * @throws CRUDException
	 */
	public List<SppriceSale> findFirmSalePrice(SppriceSale sale,Page page) throws CRUDException{
		try{
//			return sppriceFirmMapper.findFirmSalePrice(sale);
			return pageSaleManager.selectPage(sale, page, SppriceFirmMapper.class.getName()+".findFirmSalePrice");
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
}
