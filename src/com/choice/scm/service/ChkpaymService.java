package com.choice.scm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkpayd;
import com.choice.scm.domain.Chkpaym;
import com.choice.scm.persistence.ChkpaydMapper;
import com.choice.scm.persistence.ChkpaymMapper;
import com.choice.scm.persistence.MainInfoMapper;

@Service
public class ChkpaymService {

	@Autowired
	private ChkpaymMapper chkpaymMapper;
	@Autowired
	private ChkpaydMapper chkpaydMapper;
	@Autowired
	private PageManager<Chkpaym> pageManager;
	private final transient Log log = LogFactory.getLog(ChkpaymService.class);
	@Autowired
	private MainInfoMapper mainInfoMapper;
	
	/**
	 * 查询所有的报销单主表信息
	 * @param chkpaym
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkpaym> findChkpaym(Chkpaym chkpaym,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkpaym, page, ChkpaymMapper.class.getName()+".findChkpaym");
			//return chkpaymMapper.findChkpaym(chkpaym);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据单号查询
	 * @param chkpaym
	 * @return
	 * @throws CRUDException
	 */
	public Chkpaym findByChkno(Chkpaym chkpaym) throws CRUDException
	{
		try {
			
			//return pageManager.selectPage(chkpaym, page, ChkpaymMapper.class.getName()+".findChkpaym");
			return chkpaymMapper.findByChkno(chkpaym);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 查询最大单号
	 * @return
	 * @throws CRUDException
	 */
	public int getMaxChkno() throws CRUDException
	{
		try {
			return chkpaymMapper.getMaxChkno();
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询当前凭证号
	 * @param chkpayd
	 * @return
	 * @throws CRUDException
	 */
	public String findCurVouno() throws CRUDException
	{
		try {
			return chkpaymMapper.findCurVouno();
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	
	/**
	 * 添加报销单
	 * @throws CRUDException
	 */
	@SuppressWarnings("finally")
	public String saveNewChkpay(Chkpaym chkpaym,Map<String,Object> map) throws CRUDException
	{
		Map<String,String> result = new HashMap<String,String>();
		try {
				chkpaymMapper.deleteChkpaym(map);
				chkpaydMapper.deleteChkpayd(map);
				Map<String,Object> chkpaymMap = new HashMap<String,Object>();
				chkpaymMap.put("acct", chkpaym.getAcct());
				chkpaymMap.put("yearr", mainInfoMapper.findYearrList().get(0));
				chkpaymMap.put("chkno", chkpaym.getChkno());
				chkpaymMap.put("maded", chkpaym.getMaded());
				chkpaymMap.put("madet", chkpaym.getMadet());
				chkpaymMap.put("madeby", chkpaym.getMadeby());				
				chkpaymMap.put("vouno", chkpaym.getVouno());
				chkpaymMap.put("positn", chkpaym.getPositn().getCode());
				chkpaymMap.put("bdat", chkpaym.getBdat());
				chkpaymMap.put("edat", chkpaym.getEdat());
				chkpaymMap.put("typ", chkpaym.getTyp().getCode());
				chkpaymMap.put("memo", chkpaym.getMemo());
				
				chkpaymMapper.saveNewChkpaym(chkpaymMap);
				
					for(Chkpayd chkpayd : chkpaym.getChkpayd()){
						Map<String,Object> chkpaydMap= new HashMap<String,Object>();
						chkpaydMap.put("acct", chkpaym.getAcct());
						chkpaydMap.put("yearr", mainInfoMapper.findYearrList().get(0));
						chkpaydMap.put("id", chkpaydMapper.getCurId());
						chkpaydMap.put("chkno", chkpaym.getChkno());
						chkpaydMap.put("typDes", chkpayd.getTypDes());
						chkpaydMap.put("price", chkpayd.getPrice());
						chkpaydMap.put("quantity",chkpayd.getQuantity());
						chkpaydMap.put("typAmt", chkpayd.getTypAmt());
						chkpaydMap.put("uses", chkpayd.getUses());
						chkpaydMap.put("deliverAmt", chkpayd.getDeliverAmt());
						chkpaydMap.put("deliverDes", chkpayd.getDeliverDes());
						chkpaydMap.put("typAmt", chkpayd.getTypAmt());
						chkpaydMap.put("deliverAmt", chkpayd.getDeliverAmt());
						chkpaydMap.put("deliverPay", chkpayd.getDeliverPay());
						chkpaydMap.put("deliverAmt", chkpayd.getDeliverAmt());
						chkpaydMap.put("memo", chkpayd.getMemo());
						
						chkpaydMapper.saveNewChkpayd(chkpaydMap);
					}
					result.put("pr", "1");
		
		} catch (Exception e) {
			log.error(e);
			result.put("pr", "-1");
			throw new CRUDException(e);
		}finally{
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}
	}

	/**
	 * 删除报销单 包括主表和副表
	 * @param chknos
	 * @throws CRUDException
	 */
	@SuppressWarnings("finally")
	public String deleteMoreChkpay(Map<String,Object> map) throws CRUDException
	{
		Map<String,String> result = new HashMap<String,String>();
		try {
			chkpaymMapper.deleteChkpaym(map);
			chkpaydMapper.deleteChkpayd(map);
			result.put("pr", "1");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}finally{
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}
	}

	/**
	 * 整条单据删除
	 * @param chknos
	 * @return 
	 * @throws CRUDException
	 */
	@SuppressWarnings("finally")
	public String deleteOneChkpay(Chkpaym chkpaym) throws CRUDException
	{
		Map<String,String> result = new HashMap<String,String>();
		try {
			if(chkpaymMapper.findByChkno(chkpaym)==null)
			{
				result.put("pr", "0");
			}
			else
			{
				Map<String,Object> map=new HashMap<String,Object>();
				List<String> list=new ArrayList<String>();
				list.add(chkpaym.getChkno()+"");
				Chkpayd chkpayd=new Chkpayd();
				chkpayd.setAcct(chkpaym.getAcct());
				map.put("chkpayd", chkpayd);
				map.put("chkpaym", chkpaym);
				map.put("chkpaydList", list);
				map.put("chkpaymList", list);
				chkpaymMapper.deleteChkpaym(map);
				chkpaydMapper.deleteChkpayd(map);
				result.put("pr", "1");
			}
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}finally{
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}
	}
	
	
	/**
	 * 根据关键字查找报销单
	 * @param chkpaym
	 * @throws CRUDException
	 */
	public List<Chkpaym> findByKey(HashMap<Object, Object> chkpaymMap,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkpaymMap, page, ChkpaymMapper.class.getName()+".findByKey");
			//return chkpaymMapper.findByKey(chkpaym);
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

}
