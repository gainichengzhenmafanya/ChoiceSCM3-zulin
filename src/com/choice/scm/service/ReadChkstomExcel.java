package com.choice.scm.service;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Controller;

import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.wsyd.util.DateFormat;


/**
 * 读取excel 和 判断excel中数据是否存在错误信息 并将读取的信息放入对象中
 * 
 * @author 王吉峰
 * 
 */
@Controller
public class ReadChkstomExcel {
	
	// Excel表头信息
	static String[] excelColumns = { 
		"物资编码"                              
		,"物资名称"                              
		,"申购数量"                              
		,"到货日期"                              
		,"备注"                              
	};
	// 类中信息
	static String[] supplyColumns = { 
		"sp_code"           
		,"sp_name"           
	    ,"amount1.Double"
		,"hoped"
		,"memo"               
	};
	/**
	 * Map<String, String> 将错误信息存放到map中 用于前台显示使用
	 */
	public static Map<String, String> map = new HashMap<String, String>();
	
	public static List<String> errorList;

	/**
	 * List<Supply> 将Supply添加到list中
	 */
	public static List<Chkstod> chkstodList = new ArrayList<Chkstod>();
	
	public static Chkstom chkstom = new Chkstom();
	
	private static Chkstod chkstod;
	
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String path, SupplyMapper supplyMapper, PositnMapper positnMapper) {
		Boolean checkResult = true;
		Workbook book = null;
		errorList = new ArrayList<String>();
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				//1.判断日期格式对不对
				Cell cell1 = sheet.getCell(1, 0);
				if(isEmpty(cell1.getContents())){
					map.put("填制日期", "填制日期为空。");
					errorList.add("填制日期为空");
					checkResult = false;
				}else if(!checkDate(cell1.getContents())){
					map.put("填制日期", "填制日期格式不对。");
					errorList.add("填制日期格式不对");
					checkResult = false;
				}else{
					chkstom.setMaded(DateFormat.getDateByString(cell1.getContents(), "yyyy-MM-dd"));
				}
				//2.判断申购仓位有没有
				Cell cell2 = sheet.getCell(1,1);
				if(isEmpty(cell2.getContents())){
					map.put("申购仓位", "申购仓位为空。");
					errorList.add("申购仓位为空");
					checkResult = false;
				}else if(!checkPositn(cell2.getContents(),positnMapper)){
					map.put("申购仓位", "申购仓位不正确");
					errorList.add("申购仓位不正确");
					checkResult = false;
				}else{
					chkstom.setFirm(cell2.getContents());
				}
				if (!checkSupply(rowNum, colNum, sheet,supplyMapper)) {
					checkResult = false;
				}
			} else {
				checkResult = false;
			}
		} catch (Exception e) {
			checkResult = false;
			e.printStackTrace();
		} finally {
			if (book != null) {
				book.close();
				book = null;
			}

		}
		return checkResult;
	}
	
	
	/***
	 * 判断日期对不对 wjf
	 */
	private static boolean checkDate(String date){
		java.text.SimpleDateFormat format=new java.text.SimpleDateFormat("yyyy-MM-dd");
		try{
			format.parse(date);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	/***
	 * 判断是否有此仓位
	 * @param positncode
	 * @param positnMapper
	 * @return
	 */
	private static boolean checkPositn(String positncode,PositnMapper positnMapper){
		Positn positn = new Positn();
		positn.setCode(positncode);
		Positn positn1 = positnMapper.findPositnByCode(positn);
		if(positn1 == null){
			return false;
		}
		return true;
	}

	/**
	 * 检测 存在错误信息时返回false 如果不存在 返回true
	 * 
	 * @param rowNum
	 * @param colNum
	 * @param sheet
	 * @return
	 */
	private static boolean checkSupply(int rowNum, int colNum, Sheet sheet,SupplyMapper supplyMapper)
			throws Exception {
		List<Boolean> booleanList = new ArrayList<Boolean>();
		boolean bool = true;
		Cell cell = null;
		chkstodList.clear();
		for (int i = 3; i < rowNum; i++) { // 行循环
			chkstod = new Chkstod();
			for (int col = 0; col < colNum; col++) {// 列循环
				cell = sheet.getCell(col, i);// 读取单元格
				if (col <= 3) { // 判断必填字段是否为空
					if (isEmpty(cell.getContents())) {
						map.put((i+1) + "_" + (col + 1), excelColumns[col] + "为空");
						errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "为空");
						bool = false;
						continue;
					}
				}
				//验证
				if (!("").equals(cell.getContents())) {
					bool = validateColumns(cell.getContents(),i,col,supplyMapper);
				}
				if(bool){
					if (!("").equals(cell.getContents())) {// 如果不为""时将物资信息保存到实体类中
						if(col != 0 && col != 1){//如果是遍历第一列 也就是物资编码的时候 反射的时候放入的supply中sp_code ,然后将supply 再放入chkstod.........
							setChkstod(cell, chkstod, col);
						}
					}
				}
				booleanList.add(bool);
			}
			if(bool){
				//1.参考数量需要算出来。。
				if(chkstod.getSupply() != null && chkstod.getAmount1() != 0){
					if(0 != chkstod.getSupply().getUnitper3()){
						chkstod.setAmount(chkstod.getAmount1()/chkstod.getSupply().getUnitper());
					}else{
						chkstod.setAmount(0.0);
					}
				}
				chkstodList.add(chkstod);
			}
		}
		if(booleanList.size() != 0){
			for(Boolean b :booleanList){
				if(!b){
					return false;
				}
			}
		}
		return true;
	}
	private static boolean validateColumns(String cell, int i,int col,SupplyMapper supplyMapper) {
		boolean bool=true;
		switch (col) {
		case 0:
			//验证编码
			String regCode = "^[\\d\\w][\\d\\w-]*";;
			if(cell.matches(regCode)){
				Supply supply = checkSupply(cell,supplyMapper);
				if(supply == null){
					bool = false;
					map.put((i+1) + "_" + (col + 1), excelColumns[col] + "不存在");
					errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "不存在");
				}else{//，如果编码验证通过，则将里面物资表中的东西都set进去
					chkstod.setSupply(supply);
					bool = true;
				}
			}else{
				map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过");
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				bool = false;
			}
			break;
		case 2:
			//验证报货数量
			try{
				Double.parseDouble(cell);
				bool = true;
				break;
			}catch(Exception e){
				bool = false;
				map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过");
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				break;
			}
		case 3:
			//验证到货日期
			java.text.SimpleDateFormat format=new java.text.SimpleDateFormat("yyyy-MM-dd");
			try{
				format.parse(cell);
				bool = true;
				break;
			}catch(Exception e){
				bool = false;
				map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过");
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				break;
			}
		default:
			break;
		}
		return bool;
	}
	
	/***
	 * 校验是不是存在此物资 wjf
	 * @param code
	 * @param supplyMapper
	 * @return
	 */
	private static Supply checkSupply(String code,SupplyMapper supplyMapper){
		Supply supply = new Supply();
		supply.setSp_code(code);
		Supply supply1 = supplyMapper.findSupplyById(supply);
		return supply1;
	}
	
	private static void setChkstod(Cell cell,Object chkstod,
			int col) throws Exception {
		reflexMethod(chkstod,
				supplyColumns[col].trim(),
				cell.getContents());
	}

	/**
	 * 判断是否为空
	 * 
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

	/**
	 * 反射机制set参数
	 * 
	 * @param objParam
	 * @param propertyMethod
	 * @param param
	 * @throws Exception
	 */
	private static void reflexMethod(Object objParam, String propertyMethod,
			String param) throws Exception {
		if (propertyMethod.contains(".")) {
			// 获得.之前的get方法
			String getCode = propertyMethod.substring(0,
					propertyMethod.indexOf("."));
			StringBuffer getStringBuff = new StringBuffer("get");
			getStringBuff.append(toUpOfFirst(getCode));
			Method methodOne = objParam.getClass().getMethod(
					getStringBuff.toString(), new Class[] {});
			Object obj = new Object();
			String setCode = propertyMethod.substring(propertyMethod
					.indexOf(".") + 1);
			if("Double".equals(setCode)){
				obj = Double.parseDouble(param);
			}else if("Integer".equals(setCode)){
				obj = Integer.parseInt(param);
			}else if("Date".equals(setCode)){
				obj = DateFormat.getDateByString(param, "yyyy-MM-dd");
			}
			
//
//			// 获得get方法之后获得其返回类型，然后实例化该对象
//			String methodType = methodOne.getReturnType().toString();
//			Class objClass = Class.forName(methodType.substring(methodType
//					.indexOf(" ") + 1));
//			Object obj = objClass.newInstance();
//
//			// 截取.后面的方法名称，并获得其set方法
//			String setCode = propertyMethod.substring(propertyMethod
//					.indexOf(".") + 1);
//			StringBuffer setStringBuff = new StringBuffer("set");
//			setStringBuff.append(toUpOfFirst(setCode));
//			Method setMethodOne = obj.getClass().getMethod(
//					setStringBuff.toString(), param.getClass());
//			// 把需要设置的参数设置到get方法返回类型的对象当中
//			setMethodOne.invoke(obj, new Object[] { param });
//
			// 然后把对象传入到要保存的对象当中
			getStringBuff.replace(0, 1, "s");
			Method setMethodTwo = objParam.getClass().getMethod(
					getStringBuff.toString(), methodOne.getReturnType());
			setMethodTwo.invoke(objParam, new Object[] { obj });
		} else {
			String method = "set" + toUpOfFirst(propertyMethod);
			Method methodEl = objParam.getClass().getMethod(method,
					param.getClass());
			methodEl.invoke(objParam, new Object[] { param });
		}
	}

	/**
	 * 将首字母转换为大写
	 * 
	 * @param str
	 * @return
	 */
	private static String toUpOfFirst(String str) {
		char[] chars = str.toCharArray();
		if ((chars[0] >= 97) && (chars[0] <= 122)) {
			chars[0] = (char) (chars[0] - 32);
		}
		return new String(chars);
	}



}
