package com.choice.scm.service;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Announcement;
import com.choice.scm.persistence.AnnouncementMapper;

@Service
public class AnnouncementService {

	@Autowired
	private AnnouncementMapper announcementMapper;
	@Autowired
	private PageManager<Announcement> pageManager;
	
	private final transient Log log = LogFactory.getLog(AnnouncementService.class);
	
	/**
	 * 查询全部(不分页)
	 * @throws CRUDException
	 */
	public List<Announcement> findAllAnnouncement() throws CRUDException{
		try{
			Announcement announcement = new Announcement();
			return announcementMapper.findAllAnnouncement(announcement);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 模查询 公告信息
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public List<Announcement> findAnnouncement(Announcement announcement,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(announcement, page, AnnouncementMapper.class.getName()+".findAllAnnouncement");
			//return announcementMapper.findAnnouncement(announcement);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据ID查询公告信息
	 */
	public Announcement findAnnouncementById(String id) throws CRUDException{
		try{
			return announcementMapper.findAnnouncementById(id);
			
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询Id
	 */
	public int getId() throws CRUDException{
		try{
			return announcementMapper.getId();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 增加公告信息
	 */
	public void addAnnouncement(Announcement announcement) throws CRUDException{
		try{
			announcementMapper.addAnnouncement(announcement);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 修改公告信息
	 */
	public void updateAnnouncement(Announcement announcement) throws CRUDException{
		try{
			announcementMapper.updateAnnouncement(announcement);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除公告信息
	 */
	public void deleteAnnouncement(String ids) throws CRUDException{
		try{
			List<String> listId= Arrays.asList(ids.split(","));
			announcementMapper.deleteAnnouncement(listId);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public List<Announcement> findAnnouncementWithNoTeg(Announcement announcement, Page page) throws CRUDException{
		try {
			List<Announcement> announcements = findAnnouncement(announcement,page);
			if(announcements.size()>0){
				for(int i=0;i<announcements.size();i++){
					announcements.get(i).setContent(getStrWithoutHTMLElement(announcements.get(i).getContent()));
				}
			}
			return announcements;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除html标签
	 * @param str
	 * @return
	 */
	 public static String getStrWithoutHTMLElement (String str){
         if(null==str||"".equals(str.trim())){
             return str;
         }
         Pattern pattern=Pattern.compile("<[^<|^>]*>");
         Matcher matcher=pattern.matcher(str);
         StringBuffer txt=new StringBuffer();
         while(matcher.find()){
             String group=matcher.group();
             if(group.matches("<[\\s]*>")){
                 matcher.appendReplacement(txt,group);    
             }
             else{
                 matcher.appendReplacement(txt,"");
             }
         }
         matcher.appendTail(txt);
         return txt.toString().replace("&nbsp;", " ");
     }
}