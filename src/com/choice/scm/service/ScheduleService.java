package com.choice.scm.service;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.Account;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Schedule;
import com.choice.scm.domain.ScheduleD;
import com.choice.scm.domain.ScheduleStore;
import com.choice.scm.persistence.ScheduleMapper;

@Service
public class ScheduleService {

	@Autowired
	private ScheduleMapper scheduleMapper;
	@Autowired
	private PageManager<Schedule> pageManager;
	
	/**
	 * 查询配送班表 list
	 */
	public List<Schedule> findSchedule(Schedule schedule) throws CRUDException{
		try{
			return scheduleMapper.findSchedule(schedule);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分页
	 * @param schedule
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Schedule> findSchedule(Schedule schedule,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(schedule, page, ScheduleMapper.class.getName()+".findSchedule");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询分店对应的班表
	 * @return
	 */
	public List<Schedule> findFirmScheduleList(Schedule schedule,Page page){
		return pageManager.selectPage(schedule, page, ScheduleMapper.class.getName()+".findFirmScheduleList");
	}
	
	/**
	 * 删除分店班表
	 * @param schedule
	 */
	public void deleteFirmSchedule(ScheduleStore schedule) throws CRUDException{
		scheduleMapper.deleteFirmSchedule(schedule);
	}
	
	/**
	 * 查询当前账号是否是系统管理员角色
	 */
	public List<Account> findAccount(Account account) throws CRUDException{
		try{
 
			return scheduleMapper.findAccount(account);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询配送班表byid
	 */
	public Schedule findScheduleById(Schedule schedule) throws CRUDException{
		try{
 
			return scheduleMapper.findScheduleById(schedule);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据配送ID查询该配送线下的所有分店
	 */
	public List<ScheduleStore> findFirmByScheduleId(Schedule schedule) throws CRUDException{
		try{
			return scheduleMapper.findFirmByScheduleId(schedule);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	/**
	 * 添加配送路线
	 */
	public void saveSchedule(Schedule schedule) throws CRUDException{
		try{
			scheduleMapper.saveSchedule(schedule);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 刪除 左边 配送路线
	 */
	public void deleteSchedule(Schedule schedule) throws CRUDException{
		try{
			
			scheduleMapper.deleteFirmByScheduleId(schedule);
			scheduleMapper.deleteSchedule(schedule);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改 左边 配送路线
	 */
	public void updateSchedule(Schedule schedule) throws CRUDException{
		try{
			scheduleMapper.updateSchedule(schedule);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据配送班表查询  仓位
	 * @param PositnRole
	 * @throws CRUDException 
	 */
	public List<ScheduleStore> findFirmByScheduleId(String scheduleID) throws CRUDException{
		try{
			Schedule schedule = new Schedule();
			schedule.setScheduleID(Integer.valueOf(scheduleID));
			return scheduleMapper.findFirmByScheduleId(schedule);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  保存  配送班下的所有分店
	 * @param scheduleID
	 * @param listPositnId
	 * @throws CRUDException
	 */
	public void saveFirmByScheduleId(String scheduleID, List<String> listPositnId) throws CRUDException{
		try{
			Schedule schedule = new Schedule();
			schedule.setScheduleID(Integer.valueOf(scheduleID));
			scheduleMapper.deleteFirmByScheduleId(schedule);//先删除该配送班下的所有分店
			//再逐条添加该配送班的分店
			 for (int i = 0; i < listPositnId.size(); i++) {
				 ScheduleStore scheduleStore = new ScheduleStore(Integer.valueOf(scheduleID), listPositnId.get(i));
				 scheduleMapper.saveFirmByScheduleId(scheduleStore);
			}
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据配送班表主表查询明细 
	 * @param PositnRole
	 * @throws CRUDException 
	 */
	public List<ScheduleD> findDetailsByScheduleId(String scheduleID) throws CRUDException{
		try{
			ScheduleD scheduleD = new ScheduleD();
			scheduleD.setScheduleID(Integer.valueOf(scheduleID));
			return scheduleMapper.findDetailsByScheduleId(scheduleD);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据配送班表主表查询明细 
	 * @param PositnRole
	 * @throws CRUDException 
	 */
//	public Object findDetailsByDay(ScheduleD scheduleD) throws CRUDException{
//		try{
//			Map<String,Object> result = new HashMap<String,Object>();
//			ScheduleD scheduleD_ =  new ScheduleD();
//			scheduleD_.setScheduleID(scheduleD.getScheduleID());
//			scheduleD_.setCategory_Code(scheduleD.getCategory_Code());
//			scheduleD_.setReceiveDate(scheduleD.getReceiveDate());
//			scheduleD.setReceiveDate(null);
//			List<ScheduleD> detailsList = scheduleMapper.findDetailsByScheduleId(scheduleD);
//			if (detailsList.size()>0) {
//				result.put("data", detailsList);
//			}else{
//				result.put("data", "");
//			}
//			List<ScheduleD> detailsList_ = scheduleMapper.findDetailsByScheduleId(scheduleD_);
//			if (detailsList_.size()>0) {
//				result.put("data2", detailsList_);
//			}else{
//				result.put("data2", "");
//			}
//			JSONObject rs = JSONObject.fromObject(result);
//			return rs;
//		}catch(Exception e){
//			throw new CRUDException(e);
//		}
//	}
	
	/**
	 * 根据配送班表主表查询明细 
	 * @param PositnRole
	 * @throws CRUDException 
	 */
	public Object findScheduleByMonth(ScheduleD scheduleD, String scheduleMonth) throws CRUDException{
		try{
			Map<String,Object> result = new HashMap<String,Object>();
			scheduleD.setOrderDate_NY(scheduleMonth);
			List<ScheduleD> detailsList = scheduleMapper.findDetailsByScheduleId(scheduleD);
			if (detailsList.size()>0) {
				result.put("data", detailsList);
			}else{
				result.put("data", "");
			}
			scheduleD.setOrderDate_NY(null);
			scheduleD.setReceiveDate_NY(scheduleMonth);
			List<ScheduleD> detailsList_ = scheduleMapper.findDetailsByScheduleId(scheduleD);
			if (detailsList_.size()>0) {
				result.put("data2", detailsList_);
			}else{
				result.put("data2", "");
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询明细  
	 * @param PositnRole
	 * @throws CRUDException 
	 */
	public List<ScheduleD> findDetails(ScheduleD scheduleD) throws CRUDException{
		try{
			return scheduleMapper.findDetails(scheduleD);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除 右边  配送班表明细
	 * @param listId
	 * @throws CRUDException
	 */
	public Object deleteScheduleD(ScheduleD scheduleD) throws CRUDException {
		try {
			Map<String,Object> result = new HashMap<String,Object>();
			scheduleMapper.deleteScheduleD(scheduleD);
			result.put("data", "ok");
			JSONObject rs = JSONObject.fromObject(result);
			return rs;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	} 
	/**
	 * 添加 右边  配送班表明细
	 */
	public void saveScheduleD(ScheduleD scheduleD) throws CRUDException{
		try{
			String category_Code = scheduleD.getCategory_Code();
			List<String> list = Arrays.asList(category_Code.split(","));//批量类型添加
			for(String code:list){
				scheduleD.setCategory_Code(code);
				scheduleMapper.saveScheduleD(scheduleD);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改 右边  配送班表明细
	 */
	public void updateScheduleD(ScheduleD scheduleD, String ids) throws CRUDException{
		try{
			List<String> codeList = Arrays.asList(ids.split(",")); 
			scheduleMapper.updateScheduleD(scheduleD);
			
		if (!"".equals(ids)) {
				scheduleD = scheduleMapper.findDetails(scheduleD).get(0);
				SimpleDateFormat formater = new SimpleDateFormat();  
				formater.applyPattern("yyyy-MM-dd"); 
//				String odate = scheduleD.getOrderDate().substring(0, 8);
				long days = (formater.parse(scheduleD.getReceiveDate()).getTime()-formater.parse(scheduleD.getOrderDate()).getTime())/ (24 * 60 * 60 * 1000);
				for (int i=0;i<codeList.size();i++){
					scheduleD.setOrderDate(codeList.get(i));
					String receiveDate = DateFormat.getStringByDate(DateFormat.getDateBefore(DateFormat.getDateByString(scheduleD.getOrderDate(), "yyyy-MM-dd"), "day", 1, (int)days), "yyyy-MM-dd");
					scheduleD.setReceiveDate(receiveDate);
					scheduleMapper.saveScheduleD(scheduleD);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}

	/**
	 * 修改 右边  配送班表明细
	 */
	public void saveDetailesByCopy(ScheduleD scheduleD, String ids,String schedules) throws CRUDException{
		try{
			List<String> codeList = Arrays.asList(ids.split(","));//报货类别集合
			List<String> scheduleIds = Arrays.asList(schedules.split(","));//班表集合
			for(String scheduleid:scheduleIds){
				List<ScheduleD> scheduleDList = scheduleMapper.findDetails(scheduleD);
				for (int m=0;m<codeList.size();m++){
					//每次复制之前 先删除之前的数据
					ScheduleD d = new ScheduleD();
					d.setScheduleID(Integer.parseInt(scheduleid));
					d.setCategory_Code(codeList.get(m));
					scheduleMapper.deleteScheduleD(d);
					
					for (int i=0;i<scheduleDList.size();i++) {
						scheduleDList.get(i).setCategory_Code(codeList.get(m));
						scheduleDList.get(i).setScheduleID(Integer.parseInt(scheduleid));
						scheduleMapper.saveScheduleD(scheduleDList.get(i));
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量删除配送班表
	 */
	public Object batchDeleteScheduleD(ScheduleD scheduleD,String scheduleMonth) throws CRUDException{
		try {
			Map<String,Object> result = new HashMap<String,Object>();
			//批量删除操作
			scheduleD.setOrderDate(scheduleMonth);
			scheduleD.setReceiveDate(scheduleMonth);
			scheduleMapper.batchDeleteScheduleD(scheduleD);
			result.put("state", "ok");
			JSONObject rs = JSONObject.fromObject(result);
			return rs;
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 复制配送班表 
	 * @param schedule
	 * @throws CRUDException
	 */
	public void copySchedule(Schedule schedule) throws CRUDException{
		try{
			scheduleMapper.copySchedule(schedule);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 判断选择的from 和 要被复制的to 是否相同
	 * @param from
	 * @param to
	 * @return
	 */
	public boolean compareSchedule(List<ScheduleD> from,List<ScheduleD> to){
		List<String> fromstr = new ArrayList<String>();
		List<String> tostr = new ArrayList<String>();
		for(ScheduleD schedulefrom:from){
			fromstr.add(schedulefrom.getOrderColor()+","+schedulefrom.getOrderTime()+","
					+schedulefrom.getOrderDate()+","+schedulefrom.getReceiveColor()+","
					+schedulefrom.getReceiveTime()+","+schedulefrom.getReceiveDate());
		}
		for(ScheduleD scheduleto:to){
			tostr.add(scheduleto.getOrderColor()+","+scheduleto.getOrderTime()+","
					+scheduleto.getOrderDate()+","+scheduleto.getReceiveColor()+","
					+scheduleto.getReceiveTime()+","+scheduleto.getReceiveDate());
		}
		if(fromstr.containsAll(tostr) && tostr.containsAll(fromstr)){
			return true;
		}
		return false;
	}
}