package com.choice.scm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.OracleUtil;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.ana.DisList;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.DisYanShouMapper;
@Service
public class DisYanShouService {

	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private DisYanShouMapper disYanShouMapper;
	@Autowired
	private PageManager<Dis> pageManager;
	private final transient Log log = LogFactory.getLog(DisYanShouService.class);
	
	/**
	 * 报货验收
	 */
	public List<Dis> findAllDis(Dis dis,String ind1,Page page) throws CRUDException {
		try {
			List<Dis> list=null;
			//String firmCode=dis.getFirmCode();//记录原始值
			dis.setInout("入库,直发,出库");
			String inout=dis.getInout();
			dis.setChkin("N");
			dis.setChkout("N");
			//改为判断是不是非要采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			//if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			list=pageManager.selectPage(dis, page, DisYanShouMapper.class.getName()+".findAllDis"); 			
			//dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货验收
	 */
	public List<Dis> findAllDisPage(Dis dis,String ind1) throws CRUDException {
		try {
			List<Dis> list=null;
			//String firmCode=dis.getFirmCode();//记录原始值
			dis.setInout("入库,直发,出库");
			String inout=dis.getInout();
			dis.setChkin("N");
			dis.setChkout("N");
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			//if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			list=disYanShouMapper.findAllDisPage(dis); 			
			//dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 报货验收查询总数
	 */
	public int findAllDisCount(Dis dis,String ind1) throws CRUDException {
		try {
			dis.setInout("入库,直发,出库");
			dis.setChkin("N");
			dis.setChkout("N");
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			//if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
						
			return disYanShouMapper.findAllDisCount(dis); 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 报货验收，打印用，不带分页
	 */
	public List<Dis> findAllDisForReport(Dis dis,String ind1) throws CRUDException {
		try {
			List<Dis> list=null;
			//String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			dis.setChkin("N");
			dis.setChkout("N");
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			if(null!=ind1 && !"".equals(ind1)){
				dis.setChksend("Y");
			}else{
				dis.setChksend("N");
			}
			//if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			list=disYanShouMapper.findAllDis(dis);
			//dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改
	 */
	public String updateDis(DisList disList) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			for (int i = 0; i < disList.getListDis().size(); i++) {
				String chkin=findChkByKey(disList.getListDis().get(i).getId()).getChkin();
				String chkout=findChkByKey(disList.getListDis().get(i).getId()).getChkout();
				if("Y".equals(chkin) || "Y".equals(chkout)){
					result.put("pr", "fail");
				}else{
					Dis dis=new Dis();
					dis.setId(disList.getListDis().get(i).getId());
					dis.setDeliverCode(disList.getListDis().get(i).getDeliverCode());
					dis.setAmountin(disList.getListDis().get(i).getAmountin());
					dis.setPricein(disList.getListDis().get(i).getPricein());
					dis.setInd(disList.getListDis().get(i).getInd());
					dis.setBak2(disList.getListDis().get(i).getBak2());
					disYanShouMapper.updateDis(dis);
					result.put("pr", "succ");
				}
			}
			result.put("updateNum", disList.getListDis().size());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改全部已对
	 */
	public void updateAll(Dis dis) throws CRUDException {
		Map<String,Object> result = new HashMap<String,Object>();
		try {
			dis.setInCondition(OracleUtil.getOraInSql("id", dis.getId()));
			disYanShouMapper.updateAll(dis);
			result.put("pr", "succ");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询出入库状态
	 * @param dis
	 * @return
	 * @throws CRUDException
	 */
	public Chkstod findChkByKey(String id) throws CRUDException {
		try {
			return disYanShouMapper.findChkByKey(id);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
