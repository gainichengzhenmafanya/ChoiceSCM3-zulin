package com.choice.scm.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.domain.Typoth;
import com.choice.scm.persistence.GrpTypMapper;
import com.choice.scm.persistence.SupplyMapper;

@Service
public class GrpTypService {

	@Autowired
	private GrpTypMapper grpTypMapper;
	@Autowired
	private GrpTyp grpTyp;
	@Autowired
	private Grp grp;
	@Autowired
	private Typ typ;
	@Autowired
	private SupplyMapper supplyMapper;

	private final transient Log log = LogFactory.getLog(GrpTypService.class);

	/**
	 * 查询单个物资大类信息
	 * 
	 * @param grpTyp
	 * @return
	 * @throws CRUDException
	 */
	public Object findByCode(int level, String code, String acct)
			throws CRUDException {
		try {
			switch (level) {
			case 1:
				return grpTypMapper.findGrpTypByCode(code, acct);
			case 2:
				List<Grp> listGrp = grpTypMapper.findGrpByCode(code, acct);
				if (null != listGrp && listGrp.size() > 0) {
					return listGrp.get(0);
				} else {
					return null;
				}
			case 3:
				List<Typ> listTyp = grpTypMapper.findTypByCode(code, acct);
				if (null != listTyp && listTyp.size() > 0) {
					return listTyp.get(0);
				} else {
					return null;
				}
			default:
				return null;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public List<?> findByLevel(int level, String code, String acct)
			throws CRUDException {
		try {
			List<?> result = null;
			switch (level) {
			case 0:
				result = grpTypMapper.findAllGrpTypA(acct);
				break;
			case 1:
				result = grpTypMapper.findAllGrpA(code, acct);
				break;
			case 2:
				result = grpTypMapper.findAllTypA(code, acct);
				break;
			case 3:
				result = grpTypMapper.findTypByCode(code, acct);
			}
			return result;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 通过名称查询物资大类
	 * 
	 * @param grpTyp
	 * @return
	 */
	public List<GrpTyp> findGrpTypByDes(String code) throws CRUDException {
		try {
			return grpTypMapper.findGrpTypByDes(code);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询全部物资大类 中类 小类信息
	 * 
	 * @param grpTyp
	 * @return
	 * @throws CRUDException
	 */
	public List<GrpTyp> findAllGrpTyp(GrpTyp grpTyp) throws CRUDException {
		try {
			return grpTypMapper.findAllGrpTyp(new GrpTyp());
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public List<Grp> findAllGrp(Grp grp) throws CRUDException {
		try {
			return grpTypMapper.findAllGrp(null);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有中类 （入库类别汇总专用）
	 * @param grp
	 * @return
	 * @throws CRUDException
	 */
	public List<Grp> selectAllGrptypByCode(Grp grp) throws CRUDException {
		try {
			Map<String, Object> condition = new HashMap<String, Object>();
			List<String> codeList = new ArrayList<String>();
			List<String> grpList = new ArrayList<String>();
			if(grp.getCode()!=null && !"".equals(grp.getCode())){
				codeList = Arrays.asList(grp.getCode().split(","));
				condition.put("codeList", codeList);//中类集合
			}
			if(grp.getGrptyp()!=null && !"".equals(grp.getGrptyp())){
				grpList = Arrays.asList(grp.getGrptyp().split(","));
				condition.put("grpList", grpList);//大类集合
			}
			return grpTypMapper.selectAllGrptypByCode(condition);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<Typ> findAllTyp(Typ typ) throws CRUDException {
		try {	
			return grpTypMapper.findAllTyp(null);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询全部物资大类 中类 小类信息含帐套信息acct
	 * 
	 * @param grpTyp
	 * @return
	 * @throws CRUDException
	 */
	public List<GrpTyp> findAllGrpTypA(String acct) throws CRUDException {
		try {
			return grpTypMapper.findAllGrpTypA(acct);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public List<Grp> findAllGrpA(String acct, String parent)
			throws CRUDException {
		try {
			return grpTypMapper.findAllGrpA(parent, acct);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public List<Typ> findAllTypA(String acct, String parent)
			throws CRUDException {
		try {
			return grpTypMapper.findAllTypA(parent, acct);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询所有的物资大类信息，并标记当前账户已关联的物资大类，用于界面显示
	 * 
	 * @param accountId
	 * @return
	 */
	public List<GrpTyp> findGrpTypList(String accountId) throws CRUDException {
		try {
			return grpTypMapper.findGrpTypList(accountId);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 保存物资类别信息
	 * 
	 * @param grpTyp
	 * @throws CRUDException
	 */
	public void saveGrpTyp(String des, String grp, String grptyp, int level,
			String code, String acct,String type,String cost,String amt) throws CRUDException {
		try {
			switch (level + 1) {
			case 1:
				grpTyp.setCode(code);
				grpTyp.setDes(des);
				grpTyp.setAcct(acct);
				grpTypMapper.saveGrpTyp(grpTyp);
				break;
			case 2:
				this.grp.setCode(code);
				this.grp.setDes(des);
				this.grp.setGrptyp(grptyp);
				this.grp.setAcct(acct);
				// add wangjie 2014年10月28日 11:34:35
				if(!"".equals(type) && type != null){
					this.grp.setTyp(type.split(",")[1]);
				}else{
					this.grp.setTyp(type);
				}
				this.grp.setCost(cost);
				if(amt!=null && !"".equals(amt))
					this.grp.setAmt(Double.valueOf(amt));/*计划金额  15.01.19 */
				grpTypMapper.saveGrp(this.grp);
				break;
			case 3:
				typ.setCode(code);
				typ.setDes(des);
				typ.setGrp(grp);
				typ.setAcct(acct);
				typ.setGrptyp(grpTypMapper.findGrpByCode(grp, acct).get(0)
						.getGrptyp());
				grpTypMapper.saveTyp(typ);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 更新物资类别信息
	 * 
	 * @param grpTyp
	 * @throws CRUDException
	 */
	public void updateGrpTyp(String des, String code, int level, String oldCode,String type,String cost,String amt)
			throws CRUDException {
		try {
			switch (level) {
			case 1:
				grpTyp.setCode(code);
				grpTyp.setOldCode(oldCode);
				grpTyp.setDes(des);
				grpTypMapper.updateGrpTyp(grpTyp);
				grpTypMapper.updateGrpTypOfGrp(grpTyp);
				grpTypMapper.updateGrpTypOfTyp(grpTyp);
				supplyMapper.updateGrpTyp(grpTyp);
				break;
			case 2:
				grp.setCode(code);
				grp.setOldCode(oldCode);
				grp.setDes(des);
				// add wangjie 2014年10月28日 13:33:34
				if(!"".equals(type) && type != null){
					grp.setTyp(type.split(",")[1]);
				}else{
					grp.setTyp(type);
				}
				
				if(amt!=null && !"".equals(amt))
					grp.setAmt(Double.valueOf(amt));
				grp.setCost(cost);
				grpTypMapper.updateGrp(grp);
				grpTypMapper.updateGrpOfTyp(grp);
				supplyMapper.updateGrp(grp);
				break;
			case 3:
				typ.setCode(code);
				typ.setOldCode(oldCode);
				typ.setDes(des);
				grpTypMapper.updateTyp(typ);
				supplyMapper.updateTyp(typ);
				break;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 判断选中的类别是否已经被引用
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public String checkGrpTyp(String code,int level)throws CRUDException {
		String resul = "";
		String result = "";
		List<String> listCode = Arrays.asList(code.split(","));
		try {
			switch (level) {
			case 2:
				for (int i = 0; i < listCode.size(); i++) {
					if (grpTypMapper.checkGrpTyp(listCode.get(i)) > 0) {
						resul += listCode.get(i) + ",";
					}
				}
				result = resul.substring(0, resul.length()-1);
				break;
			case 3:
				for (int i = 0; i < listCode.size(); i++) {
					if (grpTypMapper.checkGrp(listCode.get(i)) > 0) {
						resul += listCode.get(i) + ",";
					}
				}
				result = resul.substring(0, resul.length()-1);
				break;
			case 4:
				for (int i = 0; i < listCode.size(); i++) {
					if (grpTypMapper.checkTyp(listCode.get(i)) > 0) {
						resul += listCode.get(i) + ",";
					}
				}
				result = resul.substring(0, resul.length()-1);
				break;
			}
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 删除物资大类，并且级联删除物资大类对应权限，物资大类对应权限操作范围
	 * 
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteGrpTyp(String code, String acct, int level)
			throws CRUDException {
		List<String> listCode = Arrays.asList(code.split(","));
		try {
			switch (level) {
			case 1:
				grpTypMapper.deleteGrpTyp(listCode);
				grpTypMapper.deleteGrpByGrpTyp(listCode);
				grpTypMapper.deleteTypByGrpTyp(listCode);
				break;
			case 2:
				grpTypMapper.deleteGrp(listCode);
				grpTypMapper.deleteTypByGrp(listCode);
				break;
			case 3:
				grpTypMapper.deleteTyp(listCode);
				break;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 
	 * @Title: findbydes
	 * @Description: TODO()
	 * @Author：LI Shuai
	 * @date：2014-2-12下午2:08:37
	 * @param level
	 * @param code
	 * @param des
	 * @return String
	 * @throws
	 */
	public Object findbydes(int level, String code, String des, String acct) {
		List<Typ> list = grpTypMapper.findByDes(code, acct, des);
		if (null != list && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}

	}
	
	/**
	 * 根据复选的大类查询出它们下的物资
	 * @param code
	 * 
	 */
	public List<Supply> findGrp(String  grptyp,String acct){
		String[] grptypstr = grptyp.split(",");
		StringBuffer sqlStr = new StringBuffer();
		for(int i=0;i<grptypstr.length;i++){
			sqlStr.append(grptypstr[i]+",");
		}
		//String  sql = sqlStr.substring(0, sqlStr.length()-1).toString();
		String sql= (CodeHelper.replaceCode(sqlStr.substring(0, sqlStr.lastIndexOf(",")).toString())).toString();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("sql", sql);
		map.put("acct", acct);
		List<Supply> list = grpTypMapper.findGrp(map);
		if(null!=list&&list.size()>0)
			return  list;
		else
			return null;
	}
	
	/**
	 * 根据复选的小类查询出它们下的物资
	 * @param code
	 */
	public List<Supply> findTyp(String  grp,String acct){
		String[] grptypstr = grp.split(",");
		StringBuffer sqlStr = new StringBuffer();
		for(int i=0;i<grptypstr.length;i++){
			sqlStr.append(grptypstr[i]+",");
		}
		//String  sql = sqlStr.substring(0, sqlStr.length()-1).toString();
		String sql= (CodeHelper.replaceCode(sqlStr.substring(0, sqlStr.lastIndexOf(",")).toString())).toString();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("sql", sql);
		map.put("acct", acct);
		List<Supply> list = grpTypMapper.findTyp(map);
		if(null!=list&&list.size()>0)
			return  list;
		else
			return null;
	}
	
	/**
	 * 根据复选的中类查询出它们下的物资
	 * @param code
	 */
	public List<Supply> findSupplyByTyp(String  typ,String acct){
		String[] grptypstr = typ.split(",");
		StringBuffer sqlStr = new StringBuffer();
		for(int i=0;i<grptypstr.length;i++){
			sqlStr.append(grptypstr[i]+",");
		}
		//String  sql = sqlStr.substring(0, sqlStr.length()-1).toString();
		String sql= (CodeHelper.replaceCode(sqlStr.substring(0, sqlStr.lastIndexOf(",")).toString())).toString();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("sql", sql);
		map.put("acct", acct);
		List<Supply> list = grpTypMapper.findSupplyByTyp(map);
		if(null!=list&&list.size()>0)
			return  list;
		else
			return null;
	}

	/***
	 * 根据条件查询大中类 供应商类别报表用
	 * @author wjf
	 * @param grpTyp2
	 * @return
	 */
	public List<GrpTyp> findAllGrpTypByCondition(GrpTyp grpTyp) {
		return grpTypMapper.findAllGrpTypByCondition(grpTyp);
	}

	/***
	 * 查询参考类别
	 * @author wjf
	 * @param typoth
	 * @return
	 */
	public List<Typoth> findTypoth(Typoth typoth) {
		return grpTypMapper.findTypoth(typoth);
	}

	/***
	 * 新增参考类别
	 * @author wjf
	 * @param typoth
	 */
	public void saveTypoth(Typoth typoth) {
		grpTypMapper.saveTypoth(typoth);
	}

	/***
	 * 修改参考类别
	 * @param typoth
	 */
	public void updateTypoth(Typoth typoth) {
		grpTypMapper.updateTypoth(typoth);
	}

	/***
	 * 删除参考类别
	 * @param typoth
	 */
	public void deleteTypoth(Typoth typoth) {
		grpTypMapper.deleteTypoth(typoth);
	}
	
	/**
	 * 通过名称查询所有大类中类小类
	 * 
	 * @param grpTyp
	 * @return
	 */
	public List<GrpTyp> findTypeByDes(GrpTyp grpTyp) throws CRUDException {
		try {
			return grpTypMapper.findTypeByDes(grpTyp);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
