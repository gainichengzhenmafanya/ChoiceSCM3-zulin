package com.choice.scm.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.OracleUtil;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.SupplyDefaultDeliverMapper;
import com.choice.scm.util.ArrayUtil;

@Service
public class SupplyDefaultDeliverService {

	@Autowired
	private SupplyDefaultDeliverMapper supplyDefaultDeliverMapper;
	@Autowired
	PageManager<Map<String,Object>> pageManager;
	
	
	private final transient static Log log = LogFactory.getLog(SupplyDefaultDeliverService.class);
	
	/**
	 * 保存供应商方法
	 */
	public void saveMethod(Map<String,Object> condition,String type) throws CRUDException{
		String allSpCode = condition.get("sp_code").toString();
		String existSpCode = "";//已存在的物资编码
		String noexistSpCode = "";//不存在的物资编码
		condition.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(allSpCode)));
		List<SpCodeMod> preList = findDefaultPositn(condition);
		if(preList.size()>0){
			for(SpCodeMod spCodeMod:preList){
				existSpCode = existSpCode + spCodeMod.getSp_code()+",";
			}
			existSpCode = existSpCode.substring(0, existSpCode.lastIndexOf(","));
		}
		
		
		Map<String,Object> newCondition = new HashMap<String, Object>();
		newCondition.put("positn", condition.get("positn")==null?"":condition.get("positn"));
		newCondition.put("positnex", condition.get("positnex")==null?"":condition.get("positnex"));
		newCondition.put("mod", condition.get("mod"));
		/*newCondition.put("sp_code", existSpCode);*/
		newCondition.put("type", type);
		
		if(existSpCode.length()>0){
			if(type.equals("POSITN")){//配送片区默认仓位
				newCondition.put("inCondition",OracleUtil.getOraInSql("sp_code", CodeHelper.replaceCode(existSpCode)));
//				newCondition.put("inCondition",OracleUtil.getOraInSql("sp_code", existSpCode));
				updateSupplyDefaultPositn(newCondition);//已存在的修改
			}else if(type.equals("POSITNEX")){//出品加工间
				newCondition.put("sp_code", CodeHelper.replaceCode(existSpCode));
				updateSupplyWorkShop(newCondition);//已存在的修改
			}
		}
		
		noexistSpCode = ArrayUtil.getMinString(allSpCode, existSpCode, ",");//找出不存在的编码 插入记录
		if(noexistSpCode.length()>0){
			newCondition.put("acct", condition.get("acct"));
			List<String> noexistSpCodeList = Arrays.asList(noexistSpCode.split(","));
			for(String spCode:noexistSpCodeList){
				newCondition.put("sp_code", spCode);
				saveSupplyDefaultPositn(newCondition);//不存在的插入
			}
		}
		
	}
	
	/**
	 * 查询物资信息和默认仓位信息 
	 */
	public List<Map<String,Object>> findSupplyDefaultPositn(Supply supply,String level, String code, String mod,String orderBy, String orderDes,Page page,String type) throws CRUDException{
		try{
			Map<String,Object> condition = new HashMap<String, Object>();
			if (null!=level) {
				if(level.equals("1")){   //大类
					condition.put("grptyp", code);
				}else if(level.equals("2")){//中类
					condition.put("grp", code);
				}else if(level.equals("3")){//小类
					condition.put("typ", code);
				}	
			}
			if(null!=supply.getGrptyp()|| null==condition.get("grptyp")){
				condition.put("grptyp", supply.getGrptyp());
			}
			condition.put("mod", mod);
			if (supply.getEx1()!=null) {
				condition.put("ex1", supply.getEx1());
			}
			condition.put("sp_code", supply.getSp_code());
			condition.put("sp_init", supply.getSp_init());
			condition.put("sp_name", supply.getSp_name());
			condition.put("wzqx", supply.getWzqx());
			condition.put("wzzhqx", supply.getWzzhqx());//物资账号权限 是关联物资还是关联小类 wjf
			condition.put("accountId", supply.getAccountId());
			condition.put("type", type);
			condition.put("orderBy", orderBy);
			condition.put("orderDes", orderDes);
			return pageManager.selectPage(condition, page, SupplyDefaultDeliverMapper.class.getName()+".findSupplyDefaultPositn");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增物资默认供应商
	 */
	public void saveSupplyDefaultPositn(Map<String,Object> condition) throws CRUDException{
		try{
			supplyDefaultDeliverMapper.saveSupplyDefaultPositn(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}	
	}
	
	/**
	 * 根据配送片区物资编码查询物资默认供应商信息
	 * @return
	 */
	public List<SpCodeMod> findDefaultPositn(Map<String,Object> condition) throws CRUDException{
		try{
			return supplyDefaultDeliverMapper.findDefaultPositn(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改默认供应商
	 */
	public void updateSupplyDefaultPositn(Map<String,Object> condition) throws CRUDException{
		try{
			supplyDefaultDeliverMapper.updateSupplyDefaultPositn(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改出品加工间
	 */
	public void updateSupplyWorkShop(Map<String,Object> condition) throws CRUDException{
		try{
			supplyDefaultDeliverMapper.updateSupplyWorkShop(condition);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 清楚默认仓位
	 * @param sp_code
	 * @param mod
	 * @throws CRUDException
	 */
	public void deletePositn(String sp_code,String mod) throws CRUDException{
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			sp_code = CodeHelper.replaceCode(sp_code);
			map.put("sp_code", sp_code);
			map.put("mod", mod);
			supplyDefaultDeliverMapper.deletePositn(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}