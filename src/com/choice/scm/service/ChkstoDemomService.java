package com.choice.scm.service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ChkstoDemoFirm;
import com.choice.scm.domain.ChkstoDemod;
import com.choice.scm.domain.ChkstoDemod_x;
import com.choice.scm.domain.ChkstoDemom;
import com.choice.scm.domain.ChkstoDemom_x;
import com.choice.scm.persistence.ChkstoDemodMapper;
import com.choice.scm.persistence.ChkstoDemomMapper;

/**
 * 报货单单据模板主表
 * @author 孙胜彬
 */
@Service
public class ChkstoDemomService {
	@Autowired
	private ChkstoDemomMapper chkstoDemomMapper;
	@Autowired
	private ChkstoDemodMapper chkstoDemodMapper;
	@Autowired
	private PageManager<ChkstoDemom> pageManager;
	@Autowired
	private PageManager<ChkstoDemom_x> pageManager_x;
	@Autowired
	private PageManager<ChkstoDemoFirm> pageManager1;
	private static Logger log = Logger.getLogger(ChkstoDemomService.class);
	
	/**
	 * 查询报货单主表信息
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemom> listChkstoDemom(ChkstoDemom chkstoDemom,Page page) throws CRUDException {
		try{
			return pageManager.selectPage(chkstoDemom, page, ChkstoDemomMapper.class.getName()+".listChkstoDemom");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<ChkstoDemom> listChkstoDemom(ChkstoDemom chkstoDemom) throws CRUDException {
		try{
			return chkstoDemomMapper.listChkstoDemom(chkstoDemom);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 新增报货单主表
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void saveChkstoDemom(ChkstoDemom chkstoDemom) throws CRUDException{
		try{
			chkstoDemomMapper.saveChkstoDemom(chkstoDemom);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 修改报货单主表
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void updateChkstoDemom(ChkstoDemom chkstoDemom) throws CRUDException{
		try{
			chkstoDemomMapper.updateChkstoDemom(chkstoDemom);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除报货单主表
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void deleteChkstoDemom(String ids) throws CRUDException{
		try{
			List<String> isList=Arrays.asList(ids.split(","));
			chkstoDemomMapper.deleteChkstoDemom(isList);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增报货单模板
	 */
	public String saveChkstoDemoms(ChkstoDemom chkstoDemom,HttpSession session) throws CRUDException{
		String acct=session.getAttribute("ChoiceAcct").toString();
		try{
			if(chkstoDemom !=null){
//				Double sum=0.0;
//				for(int i=0;i<chkstoDemom.getChkstoDemod().size();i++){
//					ChkstoDemod demod=chkstoDemom.getChkstoDemod().get(i);
//					sum=sum+demod.getPricesale();
//				}
				Calendar ca = Calendar.getInstance();
			    int year = ca.get(Calendar.YEAR);//获取年份
//			    int month=ca.get(Calendar.MONTH);//获取月份 
//			    int day=ca.get(Calendar.DATE);//获取日
			    int minute=ca.get(Calendar.MINUTE);//分 
			    int hour=ca.get(Calendar.HOUR_OF_DAY);//小时 
			    int second=ca.get(Calendar.SECOND);//秒
			    String madet=(hour<10?("0"+hour):hour)+":"+(minute<10?("0"+minute):minute)+":"+(second<10?("0"+second):second);
				chkstoDemom.setYearr(String.valueOf(year));
				chkstoDemom.setMaded(new Date());
				chkstoDemom.setMadet(madet);
//				chkstoDemom.setTotalamt(sum);
				chkstoDemom.setAcct(acct);
				saveChkstoDemom(chkstoDemom);
				for(int i=0;i<chkstoDemom.getChkstoDemod().size();i++){
					ChkstoDemod demod=chkstoDemom.getChkstoDemod().get(i);
					demod.setAcct(acct);
					demod.setYearr(String.valueOf(year));
					demod.setChkstodemono(chkstoDemom.getChkstodemono());
					chkstoDemodMapper.saveChkstoDemod(demod);
				}
				return "1";
			}else{
				return "-1";
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return "-1";
	}
	
	/**
	 * 修改报货单模板
	 */
	public int updChkstoDemom(ChkstoDemom chkstoDemom) throws CRUDException{
//		Double sum=0.0;
		try{
			if(chkstoDemom !=null){
				
//				for(int i=0;i<chkstoDemom.getChkstoDemod().size();i++){
//					ChkstoDemod demod=chkstoDemom.getChkstoDemod().get(i);
//					sum=sum+demod.getPricesale();
//				}
//				chkstoDemom.setTotalamt(sum);
				updateChkstoDemom(chkstoDemom);//修改主表
				List<String> isList=Arrays.asList((chkstoDemom.getChkstodemono()+"").split(","));//先删除从表
				chkstoDemodMapper.deleteChkstoDemod(isList);
				
				Calendar ca = Calendar.getInstance();
			    int year = ca.get(Calendar.YEAR);//获取年份
//			    int month=ca.get(Calendar.MONTH);//获取月份 
//			    int day=ca.get(Calendar.DATE);//获取日
			
				
				for(int i=0;i<chkstoDemom.getChkstoDemod().size();i++){
					ChkstoDemod demod=chkstoDemom.getChkstoDemod().get(i);
					demod.setAcct(chkstoDemom.getAcct());
					demod.setYearr(String.valueOf(year));
					demod.setChkstodemono(chkstoDemom.getChkstodemono());
					chkstoDemodMapper.saveChkstoDemod(demod);
//					chkstoDemodMapper.updateChkstoDemod(demod);
					
				}
				return 1;
			}else{
				return -1;
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * 查询报货单单据模板适用分店信息
	 * @param chkstoDemom
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemoFirm> listChkstoDemoFirm(ChkstoDemoFirm chkstoDemoFirm,Page page) throws CRUDException {
		try{
			return pageManager1.selectPage(chkstoDemoFirm, page, ChkstoDemomMapper.class.getName()+".listChkstoDemoFirm");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public List<ChkstoDemoFirm> listChkstoDemoFirm(ChkstoDemoFirm chkstoDemoFirm) throws CRUDException {
		try{
			return chkstoDemomMapper.listChkstoDemoFirm(chkstoDemoFirm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增报货单单据模板适用分店
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void saveChkstoDemoFirm(ChkstoDemoFirm chkstoDemoFirm,String chistodemo) throws CRUDException{
		try{
			deleteChkstoDemoFirm(chistodemo);
			List<String> idList=Arrays.asList(chkstoDemoFirm.getFirm().split(","));
			for(int j=0;j<idList.size();j++){
				chkstoDemoFirm.setFirm(idList.get(j));
				chkstoDemoFirm.setChkstodemono(Integer.parseInt(chistodemo));
				chkstoDemomMapper.saveChkstoDemoFirm(chkstoDemoFirm);
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除适用分店
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void deleteChkstoDemoFirm(String ids) throws CRUDException{
		try{
			List<String> isList=Arrays.asList(ids.split(","));
			chkstoDemomMapper.deleteChkstoDemoFirm(isList);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 查询报货单单据模板主表信息--虚拟物料
	 * @param chkstoDemom_x
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<ChkstoDemom_x> listChkstoDemom_x(ChkstoDemom_x chkstoDemom_x,Page page) throws CRUDException {
		try{
			return pageManager_x.selectPage(chkstoDemom_x, page, ChkstoDemomMapper.class.getName()+".listChkstoDemom_x");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增报货单模板--虚拟物料
	 */
	public String saveChkstoDemoms_x(ChkstoDemom_x chkstoDemom_x,HttpSession session) throws CRUDException{
		String acct=session.getAttribute("ChoiceAcct").toString();
		try{
			if(chkstoDemom_x !=null){
				Double sum=0.0;
				for(int i=0;i<chkstoDemom_x.getChkstoDemod_x().size();i++){
					ChkstoDemod_x demod_x=chkstoDemom_x.getChkstoDemod_x().get(i);
					sum=sum+demod_x.getPricesale();
				}
				Calendar ca = Calendar.getInstance();
			    int year = ca.get(Calendar.YEAR);//获取年份
//			    int month=ca.get(Calendar.MONTH);//获取月份 
//			    int day=ca.get(Calendar.DATE);//获取日
			    int minute=ca.get(Calendar.MINUTE);//分 
			    int hour=ca.get(Calendar.HOUR_OF_DAY);//小时 
			    int second=ca.get(Calendar.SECOND);//秒
			    String madet=(hour<10?("0"+hour):hour)+":"+(minute<10?("0"+minute):minute)+":"+(second<10?("0"+second):second);
			    chkstoDemom_x.setYearr(String.valueOf(year));
			    chkstoDemom_x.setMadet(madet);
			    chkstoDemom_x.setTotalamt(sum);
			    chkstoDemom_x.setAcct(acct);
				saveChkstoDemom_x(chkstoDemom_x);
				for(int i=0;i<chkstoDemom_x.getChkstoDemod_x().size();i++){
					ChkstoDemod_x demod_x=chkstoDemom_x.getChkstoDemod_x().get(i);
//					demod_x.setSp_code(demod_x.getSupply().getSp_code_x());
					demod_x.setAcct(acct);
					demod_x.setYearr(String.valueOf(year));
					demod_x.setChkstodemono(chkstoDemom_x.getChkstodemono());
					chkstoDemodMapper.saveChkstoDemod_x(demod_x);
				}
				return "1";
			}else{
				return "-1";
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return "-1";
	}

	/**
	 * 新增报货单主表--虚拟物料
	 * @param chkstoDemom_x
	 * @throws CRUDException
	 */
	public void saveChkstoDemom_x(ChkstoDemom_x chkstoDemom_x) throws CRUDException{
		try{
			chkstoDemomMapper.saveChkstoDemom_x(chkstoDemom_x);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 修改报货单模板
	 */
	public int updChkstoDemom_x(ChkstoDemom_x chkstoDemom_x) throws CRUDException{
		Double sum=0.0;
		try{
			if(chkstoDemom_x !=null){
				
				for(int i=0;i<chkstoDemom_x.getChkstoDemod_x().size();i++){
					ChkstoDemod_x demod=chkstoDemom_x.getChkstoDemod_x().get(i);
					sum=sum+demod.getPricesale();
				}
				chkstoDemom_x.setTotalamt(sum);
				updateChkstoDemom_x(chkstoDemom_x);//修改主表
				List<String> isList=Arrays.asList((chkstoDemom_x.getChkstodemono()+"").split(","));//先删除从表
				chkstoDemodMapper.deleteChkstoDemod_x(isList);
				
				Calendar ca = Calendar.getInstance();
			    int year = ca.get(Calendar.YEAR);//获取年份
//			    int month=ca.get(Calendar.MONTH);//获取月份 
//			    int day=ca.get(Calendar.DATE);//获取日
			
				
				for(int i=0;i<chkstoDemom_x.getChkstoDemod_x().size();i++){
					ChkstoDemod_x demod_x=chkstoDemom_x.getChkstoDemod_x().get(i);
					demod_x.setAcct(chkstoDemom_x.getAcct());
					demod_x.setYearr(String.valueOf(year));
					demod_x.setChkstodemono(chkstoDemom_x.getChkstodemono());
					chkstoDemodMapper.saveChkstoDemod_x(demod_x);
//					chkstoDemodMapper.updateChkstoDemod_x(demod_x);
					
				}
				return 1;
			}else{
				return -1;
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * 修改报货单主表
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void updateChkstoDemom_x(ChkstoDemom_x chkstoDemom_x) throws CRUDException{
		try{
			chkstoDemomMapper.updateChkstoDemom_x(chkstoDemom_x);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除报货单主表
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void deleteChkstoDemom_x(String ids) throws CRUDException{
		try{
			List<String> isList=Arrays.asList(ids.split(","));
			chkstoDemomMapper.deleteChkstoDemom_x(isList);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除适用分店
	 * @param chkstoDemom
	 * @throws CRUDException
	 */
	public void deleteChkstoDemoFirm_x(String ids) throws CRUDException{
		try{
			List<String> isList=Arrays.asList(ids.split(","));
			chkstoDemomMapper.deleteChkstoDemoFirm_x(isList);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
	}
	
}
