package com.choice.scm.service;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.ana.ProCostAna;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.PrdPrcCostManageMapper;

@Service
public class PrdPrcCostManageService {

	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private PrdPrcCostManageMapper prdPrcCostManageMapper;
	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	
	/**
	 *  查询表信息     菜品利润
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findCprlDetailsInfo(Map<String,Object> conditions,Page pager){
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, PrdPrcCostManageMapper.class.getName()+".findCprlDetailsInfo"));
//		List<Map<String,Object>> foot = prdPrcCostManageMapper.findCalForCostVariance(conditions);
//		mapReportObject.setFooter(foot);
//		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
	/**
	 * 分店部门物资进出表明细
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findCostVariance(Map<String,Object> conditions,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getPositn()&&!"".equals(supplyAcct.getPositn())){
				supplyAcct.setPositn(CodeHelper.replaceCode(supplyAcct.getPositn()));
			}
//			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
//				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
//			}
//			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
//				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
//			}
//			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
//				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
//			}
			supplyAcct.setFirm((String)conditions.get("firmCode"));
			conditions.put("supplyAcct", supplyAcct);
			conditions.put("sp_code", supplyAcct.getSp_code());
			conditions.put("grptyp", supplyAcct.getGrptyp());
			conditions.put("grp", supplyAcct.getGrp());
			conditions.put("typ", supplyAcct.getTyp());
		}
		String cnt="";
		String amt="";
		if(null!=conditions.get("month") && !"".equals(conditions.get("month"))){
			for (int i = 0; i < Integer.parseInt(conditions.get("month").toString()); i++) {
				cnt+="+A.INC"+i+"-A.OUTC"+i;
				amt+="+A.INA"+i+"-A.OUTA"+i;
			}
			conditions.put("cnt", cnt);
			conditions.put("amt", amt);
		}
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, PrdPrcCostManageMapper.class.getName()+".findCostVariance"));
		List<Map<String,Object>> foot = prdPrcCostManageMapper.findCalForCostVariance(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
	/**
	 * 核减明细
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findHejian(Map<String,Object> conditions,Page pager){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
//			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
//				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
//			}
//			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
//				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
//			}
//			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
//				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
//			}
			if(null!=supplyAcct.getBdat()){
				supplyAcct.setBdat(DateFormat.formatDate(supplyAcct.getBdat(), "yyyy-MM-dd"));
			}
			if(null!=supplyAcct.getEdat()){
				supplyAcct.setEdat(DateFormat.formatDate(supplyAcct.getEdat(), "yyyy-MM-dd"));
			}
			conditions.put("supplyAcct", supplyAcct);
		}
		mapReportObject.setRows(mapPageManager.selectPage(conditions, pager, PrdPrcCostManageMapper.class.getName()+".findHejian"));
		List<Map<String,Object>> foot = prdPrcCostManageMapper.findHejianFoot(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}
	/**
	 * 查询分店耗用对比
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findFirmUseCompare(Map<String,Object> conditions,Page page){
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		String firmCode = conditions.get("firmCode").toString();
		StringBuffer firmCodeBuffer = new StringBuffer();
		String[] firmCodes = firmCode.split(",");
		if(firmCodes.length!=0){
			for(int i=0;i<firmCodes.length;i++){
				firmCodeBuffer.append(firmCodes[i]+",");
			}	
			String firmCodess =  (CodeHelper.replaceCode(firmCodeBuffer.substring(0, firmCodeBuffer.lastIndexOf(",")).toString())).toString();
			conditions.put("firmCodes", firmCodess);
		}
		List<Positn> positnList=findPositnHead(conditions);
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
				supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()).toString());
			}
//			if(null!=supplyAcct.getGrptyp()&&!"".equals(supplyAcct.getGrptyp())){
//				supplyAcct.setGrptyp(CodeHelper.replaceCode(supplyAcct.getGrptyp()));
//			}
//			if(null!=supplyAcct.getGrp()&&!"".equals(supplyAcct.getGrp())){
//				supplyAcct.setGrp(CodeHelper.replaceCode(supplyAcct.getGrp()));
//			}
//			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
//				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
//			}
			conditions.put("supplyAcct", supplyAcct);
		}
		StringBuffer  sqlStr = new StringBuffer();
		for (int i = 0; i < positnList.size(); i++) {
			sqlStr.append("SUM(CASE WHEN (A.FIRM = '")
				  .append(positnList.get(i).getCode())
			      .append("'")
			      .append(" OR P.UCODE = '")
			      .append(positnList.get(i).getCode())
			      .append("') THEN A.AMTOUT ELSE 0 END) AS \"")
				  .append(positnList.get(i).getCode())
				  .append("AMT\",");
			sqlStr.append("SUM(CASE WHEN (A.FIRM = '")
			      .append(positnList.get(i).getCode())
			      .append("'")
			      .append(" OR P.UCODE = '")
			      .append(positnList.get(i).getCode())
			      .append("') THEN A.CNTOUT ELSE 0 END) AS \"")
			      .append(positnList.get(i).getCode())
			      .append("CNT\",");
		}
		conditions.put("sqlStr", sqlStr.substring(0, sqlStr.lastIndexOf(",")));
		return mapPageManager.selectPage(conditions,page,PrdPrcCostManageMapper.class.getName()+".findFirmUseCompare");
		//return prdPrcCostManageMapper.findFirmUseCompare(conditions);
	}
	/**
	 * 查找表头
	 * @param conditions
	 * @return
	 */
	public List<Positn> findPositnHead(Map<String,Object> conditions){
		String firmCode=(String) conditions.get("firmCode");
		String firmDes=(String) conditions.get("firmDes");
		List<Positn> positnList=new ArrayList<Positn>();
		if(null!=firmCode && !"".equals(firmCode)){
			List<String> codeList=Arrays.asList(firmCode.split(","));
			List<String> desList=Arrays.asList(firmDes.split(","));
			for (int i = 0; i < codeList.size(); i++) {
				Positn positn=new Positn();
				positn.setCode(codeList.get(i));
				positn.setDes(desList.get(i));
				positnList.add(positn);
			}
		}else{
			Positn positn=new Positn();
			positn.setTyp("1203");
			positnList=positnMapper.findPositn(positn);
		}
		return positnList;
	}
	/**
	 * 生产耗用分析
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public ReportObject<Map<String,Object>> findProCostAna(ProCostAna proCostAna,Page pager){
		mapReportObject.setRows(mapPageManager.selectPage(proCostAna, pager, PrdPrcCostManageMapper.class.getName()+".findProCostAna"));
		List<Map<String,Object>> foot = prdPrcCostManageMapper.findCalForProCostAna(proCostAna);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(pager.getCount());
		return mapReportObject;
	}	
	/**
	 * 毛利率周分析
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findMaoWeekAna(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			conditions.put("bdat", sa.getBdat());
			conditions.put("edat", sa.getEdat());
			conditions.put("positn", (sa.getPositn()==null||sa.getPositn().equals(""))?null:CodeHelper.replaceCode(sa.getPositn()));
			conditions.put("firstDate", DateFormat.getFirstDayOfCurrYear(sa.getBdat()));
			List<Map<String,Object>> foot = prdPrcCostManageMapper.findMaoWeekAnaSum(conditions);
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, PrdPrcCostManageMapper.class.getName()+".findMaoWeekAna"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询毛利周分析期初(年初到指定日期部分的 入减出)
	 * @param conditions
	 * @return
	 */
	public List<Map<String,Object>> findMaoWeekAnaQiChu1(Map<String,Object> conditions){
		return prdPrcCostManageMapper.findMaoWeekAnaQiChu1(conditions);
	}
	
	/**
	 * 查询毛利周分析期初(年初结存)
	 * @param conditions
	 * @return
	 */
	public Map<String,Object> findMaoWeekAnaQiChu2(Map<String,Object> conditions){
		Map<String,Object> resultMap = new HashMap<String, Object>();
		List<Map<String,Object>> list = prdPrcCostManageMapper.findMaoWeekAnaQiChu2(conditions);
		for(Map<String,Object> map : list){
			resultMap.put(map.get("FIRMCODE").toString(), map.get("BPERIOD"));
		}
		return resultMap;
	}
	
	/**
	 * 查询费用类别
	 */
	public List<Grp> findGrp() throws CRUDException{
		try{
			return prdPrcCostManageMapper.findGrp();
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据分店查询类别成本周趋势
	 */
	public ReportObject<Map<String,Object>> findGrpFirm(Map<String,Object> conditions) throws CRUDException{
		try{
			List<Map<String,Object>> foot = prdPrcCostManageMapper.findGrpFirmSum(conditions);
			mapReportObject.setRows(prdPrcCostManageMapper.findGrpFirm(conditions));
			mapReportObject.setFooter(foot);
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 导出分类别
	 * @param os
	 * @param supplyAcct
	 * @return
	 * @throws Exception
	 */
	public boolean exportGrpWeek(OutputStream os,Map<String,Object> condition) throws Exception{
		return true;
	}
	
	/**
	 * 应产率分析
	 */
	public ReportObject<Map<String,Object>> findShouldYieldAna(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct supplyAcct = (SupplyAcct)conditions.get("supplyAcct");
			supplyAcct.setPositn((null==supplyAcct.getPositn()||"".equals(supplyAcct.getPositn()))?null:CodeHelper.replaceCode(supplyAcct.getPositn()));
			supplyAcct.setGrptyp((null==supplyAcct.getGrptyp()||"".equals(supplyAcct.getGrptyp()))?null:CodeHelper.replaceCode(supplyAcct.getGrptyp()));
			supplyAcct.setGrp((null==supplyAcct.getGrp()||"".equals(supplyAcct.getGrp()))?null:CodeHelper.replaceCode(supplyAcct.getGrp()));
			supplyAcct.setTyp((null==supplyAcct.getTyp()||"".equals(supplyAcct.getTyp()))?null:CodeHelper.replaceCode(supplyAcct.getTyp()));
			conditions.put("supplyAcct", supplyAcct);
			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			if(null!=conditions.get("byly")){
				list = mapPageManager.selectPage(conditions, page, PrdPrcCostManageMapper.class.getName()+".findShouldYieldAna");
			}else{
				list = mapPageManager.selectPage(conditions, page, PrdPrcCostManageMapper.class.getName()+".findShouldYieldAnaByHY");
			}
			//如果是按物资编码或者物资名称排序的，相同的才省略
			if(null == conditions.get("sort") || "SP_CODE".equals(conditions.get("sort").toString()) || "SP_NAME".equals(conditions.get("sort").toString())){
				List<Map<String,Object>> rs =new ArrayList<Map<String,Object>>();
				String vcodejs="";
				//循环便利比较数据，加入同一物资，则合并显示
				for(int i=0;i<list.size();i++){
					Map<String,Object> map = list.get(i);
					rs.add(map);
					if (map.containsKey("SP_CODE")){
						if (null != map.get("SP_CODE")){
							if (vcodejs.equals(map.get("SP_CODE").toString())){
								map.put("SP_CODE", "");
								map.put("SP_NAME", "");
								map.put("SP_DESC", "");
								map.put("UNIT", "");
								map.put("STAN_PRDNAME", "");
								map.put("REAL_YIELD", "");
								map.put("REAL_YIELDRATE", "");
								map.put("USE", "");
								map.put("STAN_YIELD", "");
								map.put("STAN_YIELDRATE", "");
								map.put("RATE", "");
								continue;
							}
						}
					}
					vcodejs=map.get("SP_CODE")==null?"":map.get("SP_CODE").toString();
				}
				mapReportObject.setRows(rs);
			}else{
				mapReportObject.setRows(list);
			}
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 加工间综合利润分析
	 */
	public ReportObject<Map<String,Object>> findJGJLiRunAna(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			conditions.put("bdat", sa.getBdat());
			conditions.put("edat", sa.getEdat());
			conditions.put("positn", sa.getPositn());
			List<Map<String,Object>> foot = prdPrcCostManageMapper.findJGJLiRunAnaSum(conditions);
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, PrdPrcCostManageMapper.class.getName()+".findJGJLiRunAna"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 加工产品利润成本
	 */
	public ReportObject<Map<String,Object>> findProductTheoryCost(Map<String,Object> conditions,Page page) throws CRUDException{
		try{
			SupplyAcct sa = ((SupplyAcct)conditions.get("supplyAcct"));
			if(null != sa && null != sa.getPositn() && !"".equals(sa.getPositn())){
				sa.setPositn(CodeHelper.replaceCode(sa.getPositn()));
			}
			if(null != sa && null != sa.getTyp() && !"".equals(sa.getTyp())){
				sa.setTyp(CodeHelper.replaceCode(sa.getTyp()));
			}
			conditions.put("bdat", sa.getBdat());
			conditions.put("edat", sa.getEdat());
			List<Map<String,Object>> foot = prdPrcCostManageMapper.findProductTheoryCostSum(conditions);
			mapReportObject.setRows(mapPageManager.selectPage(conditions, page, PrdPrcCostManageMapper.class.getName()+".findProductTheoryCost"));
			mapReportObject.setFooter(foot);
			mapReportObject.setTotal(page.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
}