package com.choice.scm.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Controller;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.PositnSupply;
import com.choice.scm.domain.Supply;


/**
 * 读取excel 和 判断excel中数据是否存在错误信息 并将读取的信息放入对象中
 * 
 * @author 王吉峰
 * 
 */
@Controller
public class ReadInitExcel {

	public static List<String> errorList;

	/**
	 * List<Supply> 将Supply添加到list中
	 */
	public static List<PositnSupply> psList = new ArrayList<PositnSupply>();
	
	private static Set<String> spcodeSet = new HashSet<String>();
			
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String path, SupplyService supplyservice, String currentFirm) {
		Boolean checkResult = true;
		Workbook book = null;
		errorList = new ArrayList<String>();
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				if (!checkSupply(rowNum, colNum, sheet,supplyservice)) {
					checkResult = false;
				}
			} else {
				checkResult = false;
			}
		} catch (Exception e) {
			checkResult = false;
			e.printStackTrace();
		} finally {
			if (book != null) {
				book.close();
				book = null;
			}

		}
		return checkResult;
	}
	
	/**
	 * 检测 存在错误信息时返回false 如果不存在 返回true
	 * @param rowNum
	 * @param colNum
	 * @param sheet
	 * @return
	 */
	private static boolean checkSupply(int rowNum, int colNum, Sheet sheet,SupplyService supplyservice)
			throws Exception {
		List<Boolean> booleanList = new ArrayList<Boolean>();
		boolean bool = true;
		Cell cell = null;
		psList.clear();
		spcodeSet.clear();
		a:for (int i = 2; i < rowNum; i++) { // 行循环
			PositnSupply ps = new PositnSupply();
			for (int col = 0; col < colNum; col++) {// 列循环
				cell = sheet.getCell(col, i);// 读取单元格
				if(col == 0){
					if (isEmpty(cell.getContents())) {
						errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+",物资编码为空");
						bool = false;
						booleanList.add(bool);
						continue;
					}else{
						String regCode = "^[\\d\\w][\\d\\w-]*";
						if(cell.getContents().matches(regCode)){
							if(null != spcodeSet){
								if(spcodeSet.contains(cell.getContents())){//如果有excel中有重复物资，则忽略
									continue a;
								}
							}
							Supply supply = checkSupply(cell.getContents(),supplyservice);
							if(supply == null){
								errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+"物资编码不存在");
								bool = false;
								booleanList.add(bool);
								continue;
							}else{//，如果编码验证通过，则将里面物资表中的东西都set进去
								spcodeSet.add(supply.getSp_code());
								ps.setSp_code(supply.getSp_code());
								ps.setSp_name(supply.getSp_name());
								ps.setSp_desc(supply.getSp_desc());
//								ps.setSp_init(supply.getSp_init());
								ps.setUnit(supply.getUnit());
								ps.setUnit1(supply.getUnit1());
								//加入转化率，后面用到用标准数量计算参考数量
								ps.setUnitper(supply.getUnitper());
								bool = true;
							}
						}else{
							errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+ "物资编码验证不通过");
							bool = false;
							booleanList.add(bool);
							continue;
						}
					}
				}
				double cnt = 0;
				if(col == 4 || col == 5){
					try{
						cnt = Double.parseDouble(cell.getContents());
						if(cnt < 0){
							errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+ "数量不能为负数！");
							bool = false;
							booleanList.add(bool);
							continue;
						}
					}catch(Exception e){
						cnt = 0;
					}
					if(col == 4){
						ps.setInc0(cnt);
						ps.setIncu0(cnt*ps.getUnitper());
					}if(col == 5){
						ps.setIna0(cnt);
					}
				}
				booleanList.add(bool);
			}
			if(bool){
				psList.add(ps);
			}
		}
		if(booleanList.size() != 0){
			for(Boolean b :booleanList){
				if(!b){
					return false;
				}
			}
		}
		return true;
	}
	
	/***
	 * 校验是不是存在此物资 wjf
	 * @param code
	 * @param supplyservice
	 * @return
	 */
	private static Supply checkSupply(String code,SupplyService supplyservice){
		Supply supply = new Supply();
		supply.setSp_code(code);
		Supply supply1 = new Supply();
		try {
			supply1 = supplyservice.findSupplyById(supply);
		} catch (CRUDException e) {
			e.printStackTrace();
		}
		return supply1;
	}
	
	/**
	 * 判断是否为空
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

}
