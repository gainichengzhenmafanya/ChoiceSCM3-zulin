package com.choice.scm.service;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import net.sourceforge.pinyin4j.PinyinHelper;

import org.springframework.stereotype.Controller;

import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Grp;
import com.choice.scm.domain.GrpTyp;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.Typ;
import com.choice.scm.persistence.CodeDesMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.GrpTypMapper;
import com.choice.scm.persistence.PositnMapper;

/**
 * 读取excel 和 判断excel中数据是否存在错误信息
 * 
 * @author qiaoyj
 * 
 */
@Controller
public class ReadSupplyExcel {
	
	/**
	 * 加载表头信息
	 * 
	 * 
	 */
	
	// Excel表头信息
	static String[] excelColumns = { 
		"是否门店半成品"
		,"是否总部半成品"                    
		,"加工车间"                          
		,"加工间仓位"                        
		,"加工工时"                      
		,"标准价"                            
		,"售价"                              
		,"物资品牌"                              
		,"物资产地"                              
		,"参考类别"                          
		,"参考类别名称"                      
		,"税率"                              
		,"税率名称"  
		,"ABC"
		,"是否直拨"                          
		,"是否代销"                          
		,"是否日盘点"                       
		,"是否周盘点"    
		,"是否精品"
		,"是否可退货"            
		,"是否批次管理"
		,"物资属性"
		,"标准产品编码"                      
		,"标准产品名称"                      
		,"最小申购量"                      
		,"有效期"                      
		 };
	// 类中信息
	static String[] supplyColumns = {       
	    "ex"               
	    ,"ex1"                
	    ,"positnex"          
		,"positnexdes"       
		,"extim.Double"
		,"sp_price.Double"
		,"pricesale.Double"
		,"sp_mark"           
		,"sp_addr"           
		,"typoth"            
		,"typothdes"         
		,"tax.Double"
		,"taxdes"            
		,"abc"           
		,"inout"             
		,"yndx"              
		,"yndaypan"          
		,"ynweekpan"   
		,"sp_cost" 
		,"ynth" 
		,"ynbatch"
		,"attribute"
		,"vcode"             
		,"vname"             
		,"mincnt"           
		,"sp_per1.Double"
};
	/**
	 * Map<String, String> 将错误信息存放到map中 用于前台显示使用
	 */
	public static Map<String, String> map = new HashMap<String, String>();

	/**
	 * List<Supply> 将Supply添加到list中
	 */
	public static List<Supply> SupplyList = new ArrayList<Supply>();
	
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String path, GrpTypMapper grpTypMapper,
			PositnMapper positnMapper, DeliverMapper deliverMapper,CodeDesMapper codeDesMapper) {
		List<Boolean> booleanList = new ArrayList<Boolean>();
		Cell cell = null;
		if(map!=null && map.size()!=0){
			map.clear();
		}
		SupplyList.clear();
		Workbook book = null;
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				for (int i = 1; i < rowNum; i++) {// 行循环
					Supply supply = new Supply();
					for (int col = 0; col < colNum; col++) {// 列循环
						cell = sheet.getCell(col, i);// 读取单元格
						String cellVal = null == cell.getContents() ? "" : cell.getContents().trim();
						if(col == 0){//帐套
							if(isEmpty(cell.getContents())) {
								supply.setAcct("1");
							}else{
								supply.setAcct(cellVal);
							}
							continue;
						}
						if(col == 1 ||col == 2 || col == 3 || col == 4 || col == 6 || col == 8 || col == 13 || col == 15){//物资编码，名称，缩写  三级类别，默认仓位，默认供应商，报货分类，标准单位   
							if(isEmpty(cell.getContents())) {//标红的不能为空 ，否则继续下一条
								map.put((i+1) + "_" + (col + 1), "标红的列不能为空");
								booleanList.add(false);
								continue;
							}
							if(col == 1){
								supply.setSp_code1(cellVal);
							}
							if(col == 2){//验证编码
								String regCode = "^[\\d\\w].*[\\d\\w-]*$";;
								if(cellVal.matches(regCode)){
									supply.setSp_code(cellVal);
								}else{
									map.put((i+1) + "_" + (col + 1), "物资编码验证不通过");
									booleanList.add(false);
								}
								continue;
							}
							if(col == 3){//set名称
								supply.setSp_name(cellVal);
								//supply.setSp_init(ChineseToPinYin.toJP(supply.getSp_name()));//缩写直接自己生成
								
								//add by jin shuai at 20160309
								//物资名称生成缩写
								String sp_name = cellVal;
								String sp_init = "";
								/*for(int iii=0; iii<sp_name.length(); iii++){
									char sn = sp_name.charAt(iii);
									int sp_nameAsc = (int)sn;
									if(sp_nameAsc >=19968 && sp_nameAsc <= 171941){//是汉字
										sp_init+= UperSp_code.getInit(sp_name.substring(iii, iii+1));//缩写转为大写
									}else{
										sp_init+=sp_name.substring(iii, iii+1).toUpperCase();
									}
								}*/
								
								//update by lbh at 2017-07-31
								for(int j = 0; j < sp_name.length(); j++){
									char word = sp_name.charAt(j);
									String[] pyArray = PinyinHelper.toHanyuPinyinStringArray(word);
									if (pyArray != null) {
										sp_init += pyArray[0].charAt(0);
									} else {
										sp_init += word;
									}
								}
								supply.setSp_init(sp_init);								
								
								continue;
							}
							if(col == 4){//验证类型
								List<Typ> obj = grpTypMapper.findTypByCode(cellVal, supply.getAcct());//查询三级类别
								if(obj == null || obj.size() == 0){
									map.put((i+1) + "_" + (col + 1), "验证不通过!没有此物资类别!");
									booleanList.add(false);
								}else{
									Typ typ = (Typ)obj.get(0);
									supply.setSp_type(typ.getCode());
									supply.setTypdes(typ.getDes());//设置三级类别名称
									supply.setGrp(typ.getGrp());//设置二级级别
									List<Grp> grps = grpTypMapper.findGrpByCode(typ.getGrp(), supply.getAcct());//查询2级类别
									Grp grp = (Grp)grps.get(0);
									supply.setGrpdes(grp.getDes());//设置二级级别名称
									supply.setGrptyp(typ.getGrptyp());//设置一级级别
									Object obj1 = grpTypMapper.findGrpTypByCode(typ.getGrptyp(), supply.getAcct());//查询1级类别
									GrpTyp grptyp = (GrpTyp)obj1;
									supply.setGrptypdes(grptyp.getDes());//设置1级级别名称
								}
								continue;
							}
							if(col == 6){//验证默认仓位
								Positn positn = new Positn();
								positn.setAcct(supply.getAcct());
								positn.setCode(cellVal);
								positn = positnMapper.findPositnByCode(positn);
								if(positn == null){
									map.put((i+1) + "_" + (col + 1), "默认仓位验证不通过!没有此仓位!");
									booleanList.add(false);
								}else{
									supply.setPositndes(positn.getDes());
									supply.setSp_position(cellVal);
								}
								continue;
							}
							if(col == 8){//验证默认仓位
								//验证默认供应商
								Deliver deliver = new Deliver();
								deliver.setCode(cellVal);
								deliver = deliverMapper.findDeliverByCode(deliver);
								if(deliver == null){
									map.put((i+1) + "_" + (col + 1), "默认供应商验证不通过!没有此供应商!");
									booleanList.add(false);
								}else{
									supply.setDeliver(cellVal);
									supply.setDeliverdes(deliver.getDes());
								}
								continue;
							}
							if(col == 13){//验证报货分类
								CodeDes code = new CodeDes();
								code.setTyp("11");
								code.setCode(cellVal);
								List<CodeDes> codes = codeDesMapper.findCodeDes(code);
								if(codes.size() == 0){
									map.put((i+1) + "_" + (col + 1), "报货分类验证不通过!没有此类型!");
									booleanList.add(false);
								}else{
									supply.setTyp_eas(cellVal);
								}
								continue;
							}
							if(col == 15){//set 标准单位
								supply.setUnit(cellVal);
								continue;
							}
						}
						if(col == 10){//规格
							supply.setSp_desc(cellVal);
							continue;
						}
						if(col == 11){//状态 是否禁用
							if(!"Y".equals(cellVal))
								cellVal = "N";
							supply.setSta(cellVal);
							continue;
						}
						if(col == 12){//是否已用
							if(!"Y".equals(cellVal))
								cellVal = "N";
							supply.setLocked(cellVal);
							continue;
						}
						if(col == 16){//参考单位
							if("".equals(cellVal))
								cellVal = supply.getUnit();
							supply.setUnit1(cellVal);
							continue;
						}
						if(col == 17){//标准参考转换率
							if("".equals(cellVal))
								cellVal = "1";
							supply.setUnitper(Double.parseDouble(cellVal));
							continue;
						}
						if(col == 18){//成本单位
							if("".equals(cellVal))
								cellVal = supply.getUnit();
							supply.setUnit2(cellVal);
							continue;
						}
						if(col == 19){//标准成本转换率
							if("".equals(cellVal))
								cellVal = "1";
							supply.setUnitper2(Double.parseDouble(cellVal));
							continue;
						}
						if(col == 20){//采购单位
							if("".equals(cellVal))
								cellVal = supply.getUnit();
							supply.setUnit3(cellVal);
							continue;
						}
						if(col == 21){//采购参考转换率
							if("".equals(cellVal))
								cellVal = "1";
							supply.setUnitper3(Double.parseDouble(cellVal));
							continue;
						}
						if(col == 22){//库存单位
							if("".equals(cellVal))
								cellVal = supply.getUnit();
							supply.setUnit4(cellVal);
							continue;
						}
						if(col == 23){//库存参考转换率
							if("".equals(cellVal))
								cellVal = "1";
							supply.setUnitper4(Double.parseDouble(cellVal));
							continue;
						}
						if(col >= 24){
							if(!"".equals(cellVal))
								reflexMethod(supply,supplyColumns[col-24].trim(),cellVal);
						}
					}
					supply.setDatsto(0.0);//采购周期
					supply.setYnCheck("Y");//是否总部审核
					supply.setStomax1(0);//分店单次限量
					supply.setYhrate("0");//物资验货比
					supply.setChrate("0");//物资出货比
					supply.setUpper("0");//物资涨价比
					supply.setLower("0");//物资降价比
					SupplyList.add(supply);
				}
				if(!map.isEmpty()){//如果map不为空 则存在错误信息，将错误信息返回前台页面 wangjie 2015年3月6日 11:13:00
					return false;
				}
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (book != null) {
				book.close();
				book = null;
			}
		}
	}

	/**
	 * 判断是否为空
	 * 
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

	/**
	 * 反射机制set参数
	 * 
	 * @param objParam
	 * @param propertyMethod
	 * @param param
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	private static void reflexMethod(Object objParam, String propertyMethod,
			String param) throws Exception {

		if (propertyMethod.contains(".")) {
			// 获得.之前的get方法
			String getCode = propertyMethod.substring(0,
					propertyMethod.indexOf("."));
			StringBuffer getStringBuff = new StringBuffer("get");
			getStringBuff.append(toUpOfFirst(getCode));
			Method methodOne = objParam.getClass().getMethod(
					getStringBuff.toString(), new Class[] {});
			Object obj = new Object();
			String setCode = propertyMethod.substring(propertyMethod
					.indexOf(".") + 1);
			if("Double".equals(setCode)){
				obj = Double.parseDouble(param);
			}else if("Integer".equals(setCode)){
				obj = Integer.parseInt(param);
			}else if("Date".equals(setCode)){
				obj = new Date(param);
			}
			
//
//			// 获得get方法之后获得其返回类型，然后实例化该对象
//			String methodType = methodOne.getReturnType().toString();
//			Class objClass = Class.forName(methodType.substring(methodType
//					.indexOf(" ") + 1));
//			Object obj = objClass.newInstance();
//
//			// 截取.后面的方法名称，并获得其set方法
//			String setCode = propertyMethod.substring(propertyMethod
//					.indexOf(".") + 1);
//			StringBuffer setStringBuff = new StringBuffer("set");
//			setStringBuff.append(toUpOfFirst(setCode));
//			Method setMethodOne = obj.getClass().getMethod(
//					setStringBuff.toString(), param.getClass());
//			// 把需要设置的参数设置到get方法返回类型的对象当中
//			setMethodOne.invoke(obj, new Object[] { param });
//
			// 然后把对象传入到要保存的对象当中
			getStringBuff.replace(0, 1, "s");
			Method setMethodTwo = objParam.getClass().getMethod(
					getStringBuff.toString(), methodOne.getReturnType());
			setMethodTwo.invoke(objParam, new Object[] { obj });
		} else {
			String method = "set" + toUpOfFirst(propertyMethod);
			Method methodEl = objParam.getClass().getMethod(method,
					param.getClass());
			methodEl.invoke(objParam, new Object[] { param });
		}
	}

	/**
	 * 将首字母转换为大写
	 * 
	 * @param str
	 * @return
	 */
	private static String toUpOfFirst(String str) {
		char[] chars = str.toCharArray();
		if ((chars[0] >= 97) && (chars[0] <= 122)) {
			chars[0] = (char) (chars[0] - 32);
		}
		return new String(chars);
	}

}
