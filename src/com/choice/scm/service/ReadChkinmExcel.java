package com.choice.scm.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Controller;

import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.wsyd.util.DateFormat;


/**
 * 读取excel 和 判断excel中数据是否存在错误信息 并将读取的信息放入对象中
 * 
 * @author 王吉峰
 * 
 */
@Controller
public class ReadChkinmExcel {
	
	// Excel表头信息
	static String[] excelColumns = { 
		"物资编码"
		,"物资名称"
		,"入库数量"
		,"生产日期"
		,"批次号"
		,"备注"
	};
	// 类中信息
	static String[] supplyColumns = { 
		"sp_code"
		,"sp_name"
	    ,"amount.Double"
	    ,"dued"
	    ,"pcno"
		,"memo"
	};
	
	public static List<String> errorList;
	/**
	 * List<Supply> 将Supply添加到list中
	 */
	public static List<Chkind> chkindList = new ArrayList<Chkind>();
	
	public static Chkinm chkinm;
	
	private static Chkind chkind;
	
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String accountId, String path, SupplyMapper supplyMapper, PositnMapper positnMapper,DeliverMapper deliverMapper) {
		Boolean checkResult = true;
		Workbook book = null;
		chkinm = new Chkinm();
		errorList = new ArrayList<String>();
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				//0.判断单据类型
				Cell cell0 = sheet.getCell(1, 0);
				if(isEmpty(cell0.getContents())){
					errorList.add("单据类型不能为空");
					checkResult = false;
				}else if(!checkDJtype(cell0.getContents())){
					errorList.add("单据类型不存在");
					checkResult = false;
				}else{
					chkinm.setTyp(cell0.getContents());
				}
				//1.判断日期格式对不对
				Cell cell1 = sheet.getCell(3, 0);
				if(isEmpty(cell1.getContents())){
					errorList.add("填制日期不能为空");
					checkResult = false;
				}else if(!checkDate(cell1.getContents())){
					errorList.add("填制日期格式不对");
					checkResult = false;
				}else{
					chkinm.setMaded(DateFormat.getDateByString(cell1.getContents(), "yyyy-MM-dd"));
				}
				//2.判断申购仓位有没有
				Cell cell2 = sheet.getCell(1,1);
				if(isEmpty(cell2.getContents())){
					errorList.add("入库仓位不能为空");
					checkResult = false;
				}else{
					Positn positn = checkPositn(cell2.getContents(),positnMapper);
					if(positn == null){
						errorList.add("入库仓位不正确");
						checkResult = false;
					}else{
						chkinm.setPositn(positn);
					}
				}
				//3.判断供应商
				Cell cell3 = sheet.getCell(1,2);
				if(isEmpty(cell3.getContents())){
					errorList.add("供应商不能为空");
					checkResult = false;
				}else{
					Deliver deliver = checkDeliver(cell3.getContents(),deliverMapper);
					if(deliver == null){
						errorList.add("供应商不正确");
						checkResult = false;
					}else{
						chkinm.setDeliver(deliver);
					}
				}
				//4.得到摘要
				Cell cell4 = sheet.getCell(1,3);
				if(!isEmpty(cell4.getContents())){
					chkinm.setMemo(cell4.getContents());
				}
				//5.判断物资
				if (!checkSupply(accountId,rowNum, colNum, sheet,supplyMapper)) {
					checkResult = false;
				}
			} else {
				checkResult = false;
			}
		} catch (Exception e) {
			checkResult = false;
			e.printStackTrace();
		} finally {
			if (book != null) {
				book.close();
				book = null;
			}

		}
		return checkResult;
	}
	
	/***
	 * 校验单据类型 wjf
	 * 9900	正常入库
		9901	盘盈入库
		9902	赠品入库
		9903	回收入库
		9904	代销入库
		9905	产品入库
		9907	入库冲消
	 * @return
	 */
	private static boolean checkDJtype(String dj){
		if(dj.equals("9900") || dj.equals("9901") ||dj.equals("9902") ||dj.equals("9903") 
				||dj.equals("9904") ||dj.equals("9905") ||dj.equals("9906") ||dj.equals("9907")){
			return true;
		}
		return false;
	}
	
	/***
	 * 校验供应商 wjf
	 * @param contents
	 * @param deliverMapper
	 * @return
	 */
	private static Deliver checkDeliver(String contents,
			DeliverMapper deliverMapper) {
		Deliver deliver = new Deliver();
		deliver.setCode(contents);
		List<Deliver> delivers = deliverMapper.findDeliver(deliver);
		if(delivers.size() == 0){
			return null;
		}else{
			return delivers.get(0);
		}
	}
	
	
	/***
	 * 判断日期对不对 wjf
	 */
	private static boolean checkDate(String date){
		java.text.SimpleDateFormat format=new java.text.SimpleDateFormat("yyyy-MM-dd");
		try{
			format.parse(date);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	/***
	 * 判断是否有此仓位
	 * @param positncode
	 * @param positnMapper
	 * @return
	 */
	private static Positn checkPositn(String positncode,PositnMapper positnMapper){
		Positn positn = new Positn();
		positn.setCode(positncode);
		Positn positn1 = positnMapper.findPositnByCode(positn);
		if(positn1 == null){
			return null;
		}
		return positn1;
	}

	/**
	 * 检测 存在错误信息时返回false 如果不存在 返回true
	 * 
	 * @param rowNum
	 * @param colNum
	 * @param sheet
	 * @return
	 */
	private static boolean checkSupply(String accountId,int rowNum, int colNum, Sheet sheet,SupplyMapper supplyMapper)
			throws Exception {
		List<Boolean> booleanList = new ArrayList<Boolean>();
		boolean bool = true;
		Cell cell = null;
		chkindList.clear();
		for (int i = 5; i < rowNum; i++) { // 行循环
			chkind = new Chkind();
			for (int col = 0; col < colNum; col++) {// 列循环
				cell = sheet.getCell(col, i);// 读取单元格
				if (col == 0 || col == 2) { // 判断必填字段是否为空
					if (isEmpty(cell.getContents())) {
						errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "为空");
						bool = false;
						continue;
					}
				}
				//验证
				if (!("").equals(cell.getContents())) {
					bool = validateColumns(cell.getContents(),i,col,supplyMapper);
				}
				if(bool){
					if (!("").equals(cell.getContents())) {// 如果不为""时将物资信息保存到实体类中
						if(col == 2){//放数量
							chkind.setAmount(Double.parseDouble(cell.getContents()));
						}else if(col == 4){//放批次号
							chkind.setPcno(cell.getContents());
						}else if(col == 5){//放备注
							chkind.setMemo(cell.getContents());
						}
					}
				}
				booleanList.add(bool);
			}
			if(bool){
				//1.参考数量需要算出来。。
				if(chkind.getSupply() != null && chkinm.getPositn() != null && chkinm.getMaded() != null){
					chkind.setAmount1(chkind.getAmount()*chkind.getSupply().getUnitper());
					//2.单价取报价，没有报价取标准价
					Spprice sprice = new Spprice();
					sprice.setAcct(accountId);
					sprice.setSp_code(chkind.getSupply().getSp_code());
					sprice.setArea(chkinm.getPositn().getCode());
					sprice.setMadet(DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd"));
					sprice = supplyMapper.findBprice(sprice);
					if (sprice == null){
						chkind.setPrice(chkind.getSupply().getSp_price());//没有就取标准价
						chkind.setTotalamt(chkind.getAmount()*chkind.getPrice());
					}else{
						chkind.setPrice(sprice.getPrice().doubleValue());
						chkind.setTotalamt(chkind.getAmount()*chkind.getPrice());
					}
				}
				chkindList.add(chkind);
			}
		}
		if(booleanList.size() != 0){
			for(Boolean b :booleanList){
				if(!b){
					return false;
				}
			}
		}
		return true;
	}
	private static boolean validateColumns(String cell, int i,int col,SupplyMapper supplyMapper) {
		boolean bool=true;
		switch (col) {
		case 0:
			//验证编码
			String regCode = "^[\\d\\w][\\d\\w-]*";;
			if(cell.matches(regCode)){
				Supply supply = checkSupply(cell,supplyMapper);
				if(supply == null){
					bool = false;
					errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "不存在");
				}else{//，如果编码验证通过，则将里面物资表中的东西都set进去
					chkind.setSupply(supply);
					bool = true;
				}
			}else{
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				bool = false;
			}
			break;
		case 2:
			//验证报货数量
			try{
				Double.parseDouble(cell);
				bool = true;
				break;
			}catch(Exception e){
				bool = false;
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				break;
			}
		case 3:
			//验证生产日期
			if(!checkDate(cell)){
				errorList.add("第"+(i+1)+"行第"+(col+1)+"列"+excelColumns[col] + "验证不通过");
				bool = false;
				break;
			}else{
				chkind.setDued(DateFormat.getDateByString(cell, "yyyy-MM-dd"));
				bool = true;
				break;
			}
		default:
			break;
		}
		return bool;
	}
	
	/***
	 * 校验是不是存在此物资 wjf
	 * @param code
	 * @param supplyMapper
	 * @return
	 */
	private static Supply checkSupply(String code,SupplyMapper supplyMapper){
		Supply supply = new Supply();
		supply.setSp_code(code);
		Supply supply1 = supplyMapper.findSupplyById(supply);
		return supply1;
	}
	
	/**
	 * 判断是否为空
	 * 
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

}
