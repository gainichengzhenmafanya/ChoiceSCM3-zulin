package com.choice.scm.service;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.AccountRole;
import com.choice.framework.domain.system.AccountSupply;
import com.choice.framework.domain.system.Role;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountRoleMapper;
import com.choice.framework.persistence.system.AccountSupplyMapper;
import com.choice.framework.persistence.system.RoleMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.UperSp_code;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Costbom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.PositnSpcode;
import com.choice.scm.domain.Spprice;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyUnit;
import com.choice.scm.domain.Tax;
import com.choice.scm.domain.Typoth;
import com.choice.scm.persistence.CodeDesMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.ExplanMapper;
import com.choice.scm.persistence.FirmSupplyMapper;
import com.choice.scm.persistence.GrpTypMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.PublicExportExcel;

@Service
public class SupplyService {

	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private AccountSupplyMapper accountSupplyMapper;
	@Autowired
	private FirmSupplyService firmSupplyService;
	@Autowired
	private PageManager<Supply> pageManager;
	@Autowired 
	private GrpTypMapper grpTypMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private CodeDesMapper codeDesMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private AccountRoleMapper accountRoleMapper;
	@Autowired
	private FirmSupplyMapper firmSupplyMapper; /*wangjie 2014年12月9日 14:36:33*/
	@Autowired
	private ExplanMapper explanMapper;//判断是否查出品加工间还是查物资表
	private final transient Log log = LogFactory.getLog(SupplyService.class);
	/**
	 * 根据左侧的类别树    查询全部物资编码信息
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findAllSupplyByLeftGrpTyp(Supply  supply,String level,String code,Page page) throws CRUDException {
		try { 
			if(null!=supply && null!=supply.getSp_init()){
				supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			}
			if (null!=level) {
				if(level.equals("1")){   //大类
					supply.setGrptyp(code);
				}else if(level.equals("2")){//中类
					supply.setGrp(code);
				}else if(level.equals("3")){//小类
					supply.setSp_type(code);
				}	
			}
		//	return supplyMapper.findAllSupply(supply);
		//分页方法
			if(null!=supply && null!=supply.getIs_supply_x()&&"Y_group".equals(supply.getIs_supply_x())){//虚拟物资编码的          分组                方法
				return pageManager.selectPage(supply, page, SupplyMapper.class.getName()+".selectAllSupply_x");
			}
			
			return pageManager.selectPage(supply, page, SupplyMapper.class.getName()+".findAllSupply");
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据左侧的类别树    查询全部物资编码信息   用于虚拟物资模板
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findAllSupplyByLeftGrpTyp_x(Supply  supply,String level,String code,Page page) throws CRUDException {
		try { 
			if(null!=supply && null!=supply.getSp_init()){
				supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			}
			if (null!=level) {
				if(level.equals("1")){   //大类
					supply.setGrptyp(code);
				}else if(level.equals("2")){//中类
					supply.setGrp(code);
				}else if(level.equals("3")){//小类
					supply.setSp_type(code);
				}	
			}
		//	return supplyMapper.findAllSupply(supply);
		//分页方法
				return pageManager.selectPage(supply, page, SupplyMapper.class.getName()+".selectAllSupply_x");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据左侧的类别树    查询全部物资编码信息
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findAllSupplyByLeftGrpTyp(Supply  supply,String level,String code) throws CRUDException {
		try { 
			if(null!=supply && null!=supply.getSp_init()){
				supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			}
			if (null!=level) {
				if(level.equals("1")){   //大类
					supply.setGrptyp(code);
				}else if(level.equals("2")){//中类
					supply.setGrp(code);
				}else if(level.equals("3")){//小类
					supply.setSp_type(code);
				}	
			}
			return supplyMapper.findAllSupply(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询单个物资编码信息
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public Supply findSupplyById(Supply supply) throws CRUDException {
		try {
			//是否是  虚拟物料
			return supplyMapper.findSupplyById(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
//	public list
	
	/**
	 * 查询单个物资编码信息
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public Supply findSupplyById_x(Supply supply) throws CRUDException {
		try {
			//是否是  虚拟物料
			return supplyMapper.findSupplyById_x(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public Supply findSupplyById_x1(Supply supply) throws CRUDException {
		try {
			return supplyMapper.findSupplyById_x1(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 多条添加虚拟物资到模板中
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public Supply findSupplyById_x2(Supply supply) throws CRUDException {
		try {
			return supplyMapper.findSupplyById_x2(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *根据虚拟物料的编码查询对应的所有实际物料 
	 * @param supply
	 * @return List<Supply>
	 * throws CRUDException
	 */
	public List<Supply> findSupplyBySp_code_x_add(Supply supply)throws CRUDException{
		try {
			return supplyMapper.findAllSupplySXS(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *根据虚拟物料的编码查询对应的所有实际物料 
	 * @param supply
	 * @return List<Supply>
	 * throws CRUDException
	 */
	public List<Supply> findSupplyBySp_code_x_edit(Costbom costbom)throws CRUDException{
		try {
			return supplyMapper.findAllSupplySXS_edit(costbom);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询全部物资编码信息
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findAllSupply(Supply supply) throws CRUDException {
		try {
			return supplyMapper.findAllSupply(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询全部物资编码信息
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findAllSupplySXS(Supply supply) throws CRUDException {
		try {
			return supplyMapper.findAllSupplySXS(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的物资编码信息，并标记当前账户已关联的物资编码，用于界面显示
	 * @param accountId
	 * @return
	 */
	public List<Supply> findSupplyList(String accountId) throws CRUDException {
		try {
			return supplyMapper.findSupplyList(accountId);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据的传入的条件查询前n条记录
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findSupplyListTopN(Supply supply) throws CRUDException {
		try {
		//	Supply supply=new Supply();
		//	supply.setSp_init(inputValue);
			if(null!=supply && "Y".equals(supply.getIs_supply_x())){//虚拟物料专用  不熟悉务改
				return supplyMapper.findSupplyListTopN_x(supply);
			}else{
				return supplyMapper.findSupplyListTopN(supply);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据输入的条件
	 * 查询供应商供应物资前十条记录
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findGysSpListTopN(Supply supply) throws CRUDException {
		try {
			return supplyMapper.findGysSpListTopN(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据的传入的条件查询前n条记录
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findSupplyListTopN_x(Supply supply) throws CRUDException {
		try {
			return supplyMapper.findSupplyListTopN_x(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据条件查询售价
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public SppriceSale findSprice(SppriceSale spprice) throws CRUDException {
		try {
			return supplyMapper.findSprice(spprice);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据条件查询报价
	 * @param spprice
	 * @return
	 * @throws CRUDException
	 */
	public Spprice findBprice(Spprice spprice) throws CRUDException {
		try {
			return supplyMapper.findBprice(spprice);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 将文件上传到temp文件夹下
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request,HttpServletResponse response) throws IOException {
		return PublicExportExcel.uploadToTemp(request, response);
	}
	/**
	 * 对execl进行验证
	 */
	public List<String> check(String path, String accountId) throws CRUDException {
		boolean checkResult = ReadSupplyExcel.check(path, grpTypMapper,positnMapper,deliverMapper,codeDesMapper);
		Map<String, String> mapMsg = new HashMap<String, String>();
		if (checkResult) {
			List<Supply> supplyList = ReadSupplyExcel.SupplyList;
//			System.out.println("账套:"+supplyList.get(0).getAcct()+" 编码:"+supplyList.get(0).getSp_code()+" 名称:"+supplyList.get(0).getSp_name()+"  缩写:"+supplyList.get(0).getSp_init());
			saveSupplyList(supplyList, accountId);
		} else {
			mapMsg = ReadSupplyExcel.map;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
//		ReadSupplyExcel.map.clear();//清空错误信息
		return listError(mapMsg);
	}
	/**
	 * 返回导入的错误结果信息
	 */
	public List<String> listError(Map<String, String> map) {
		List<String> listError = new ArrayList<String>();
		if (map.size() != 0) {
			for (String key : map.keySet()) {
				listError.add(key);
				listError.add(map.get(key));
			}
		}
		return listError;
	}
	/**
	 * 读取操作文档详细列表
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public List<Map<String,Object>> showHelpFiles(HttpServletResponse response,HttpServletRequest request) throws IOException {
		String filePath = request.getSession().getServletContext().getRealPath("/")+ "\\" + "template\\";//如果是服务器上linux 换成/
		File f = new File(filePath);// 建立当前目录中文件的File对象
		File[] files = f.listFiles();// 取得目录中所有文件的File对象数组
		DecimalFormat df = new DecimalFormat("#.00"); 
		List<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		if(null != files){
			String len="";
			for(File file : files) {
				Map<String,Object> map = new HashMap<String,Object>();
				// 目录下的文件：
				if(file.isFile()) {
					//计算文件大小
					if(file.length()<1024){
						len = file.length()+"B"; 
					}else if(file.length() < 1048576){
						len = df.format((double)file.length()/1024)+"KB";
					}else if(file.length() < 1073741824){
						len = df.format((double)file.length()/1024/1024)+"MB";
					}else{
						len = df.format((double)file.length()/1024/1024/1024)+"GB";	
					}
					map.put("name",file.getName());
					map.put("len",len);
				}else if (file.isDirectory()) { 	// 目录下的目录：
				}
				fileList.add(map);
			}
		}
		return fileList;
	}
	
	/**
	 * 下载模板信息
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response,HttpServletRequest request) throws IOException {
		//下载模板，调用公用方法，适用windows和linux，templete文件夹下文件的下载
		PublicExportExcel.downloadTemplate(response, request, "物资编码导入模板.xls");
	}
	/**
	 * 批量保存物资信息
	 * 
	 * @param teacherInfoList
	 * @throws CRUDException
	 */
	private void saveSupplyList(List<Supply> supplyList, String accountId)
			throws CRUDException {
		for (Supply supply : supplyList) {
			Supply supply1 = new Supply();
			supply1.setSp_code(supply.getSp_code());
			if(!isSupplyCodeRepeated(supply1)){//数据库中已经存在此编码的物资，则改为更新 wjf
				updateSupply(supply);
			}else{
				saveSupply(supply, accountId,null);
				//保存到supplyunit表
				List<String> unitList = removeCo(supply);
				for(int i=0;i<unitList.size(); i++){
					String unitstr = unitList.get(i);
					SupplyUnit supplyUnit = new SupplyUnit();
					supplyUnit.setId(Util.getUUID());
					supplyUnit.setSp_code(supply.getSp_code());
					supplyUnit.setUnit(unitstr.split(",")[0]);
					supplyUnit.setUnitper(Double.parseDouble(unitstr.split(",")[1]));
					supplyUnit.setOrderNo(i+1);
					supplyUnit.setSequence(i+1);
					
					supplyMapper.saveSupplyUnit(supplyUnit);
				}
			}
		}
	}
	
	/**
	 * 保存物资编码信息
	 * @param supply
	 * @throws CRUDException
	 */
	public void saveSupply(Supply supply, String accountId,String firmCodes) throws CRUDException {
		try {
//			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			//添加该账号的物资权限
			AccountSupply accountSupply = new AccountSupply();
			accountSupply.setId(CodeHelper.createUUID());
			accountSupply.setSp_code(supply.getSp_code());
			accountSupply.setAccountId(accountId);
			accountSupplyMapper.saveAccountSupply(accountSupply);
			//添加系统管理员角色账号对改物资的权限
			Role role = new Role();
			role.setName("系统管理员");
			role.setDeleteFlag("T");
			List<Role> listRole = roleMapper.findAllRole(role);
			if(listRole!=null && listRole.size()>0){//存在该角色
				String roleId ="";
				for(Role ro : listRole){
					if("系统管理员".equals(ro.getName())){
						roleId = ro.getId();
						break;
					}
				}
				if(roleId!=null && !"".equals(roleId)){
					//查询该角色下账号信息
					AccountRole accountRole = new AccountRole();
					accountRole.setRoleId(roleId);
					List<AccountRole> listAccountRole = accountRoleMapper.findAccountRole(accountRole);
					if(listAccountRole!=null && listAccountRole.size()>0){
						AccountSupply admin = new AccountSupply();
						admin.setId(CodeHelper.createUUID());
						admin.setSp_code(supply.getSp_code());
						for(AccountRole aRole:listAccountRole){
							admin.setAccountId(aRole.getAccountId());
							accountSupplyMapper.saveAccountSupply(admin);
						}
					}
				}
				
				
			}
			
			//如果页面没有传物资缩写，后台根据物资名称自动生成（针对excel导入的物资）wj 15.01.12
			if(supply.getSp_init()==null || "".equals(supply.getSp_init())){
				String sp_name = supply.getSp_name();//物资名称
				String sp_init = "";
				for(int i=0; i<sp_name.length(); i++){
					char sn = sp_name.charAt(i);
					int sp_nameAsc = (int)sn;
					if(sp_nameAsc >=19968 && sp_nameAsc <= 171941){//是汉字
						sp_init+= UperSp_code.getInit(sp_name.substring(i, i+1));//缩写转为大写
					}else{
						sp_init+=sp_name.substring(i, i+1).toUpperCase();
					}
				}
				supply.setSp_init(sp_init);
			}else{
				supply.setSp_init(supply.getSp_init().toUpperCase());//小写转大写
			}
			
			//标准单位（其他4个单位为空的情况下 默认和标准单位相同  且转换率为1.0）
			if(supply.getUnit() != null && !"".equals(supply.getUnit())){
				supply.setUnitper0(1.0);
				if(supply.getUnit1() == null || "".equals(supply.getUnit1())){
					supply.setUnit1(supply.getUnit());
					supply.setUnitper(1.0);
				}
				if(supply.getUnit2() == null || "".equals(supply.getUnit2())){
					supply.setUnit2(supply.getUnit());
					supply.setUnitper2(1.0);
				}
				if(supply.getUnit3() == null || "".equals(supply.getUnit3())){
					supply.setUnit3(supply.getUnit());
					supply.setUnitper3(1.0);
				}
				if(supply.getUnit4() == null || "".equals(supply.getUnit4())){
					supply.setUnit4(supply.getUnit());
					supply.setUnitper4(1.0);
				}
			}
			
			supplyMapper.saveSupply_new(supply);
			if(firmCodes!=null&&""!=firmCodes){
				String[] firmCodeStr = firmCodes.split(",");
				for(int i=0;i<firmCodeStr.length;i++){
					PositnSpcode positnSpcode = new PositnSpcode();
					positnSpcode.setSp_code(supply.getSp_code());
					positnSpcode.setSp_desc(supply.getSp_desc());
					positnSpcode.setSp_init(supply.getSp_init());
					positnSpcode.setSp_max1(supply.getSp_max1());
					positnSpcode.setSp_min1(supply.getSp_min1());
					positnSpcode.setSp_name(supply.getSp_name());
					positnSpcode.setSp_price(supply.getSp_price());
					positnSpcode.setUnit(supply.getUnit());
					positnSpcode.setPositn(firmCodeStr[i]);
					firmSupplyService.saveByAddOnly(positnSpcode);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存物资编码信息
	 * @param supply
	 * @throws CRUDException
	 */
	public void saveSupply_new(Supply supply, String accountId,String firmCodes,String orderno,String disunit) throws CRUDException {
		try {
//			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			//添加该账号的物资权限
			AccountSupply accountSupply = new AccountSupply();
			accountSupply.setId(CodeHelper.createUUID());
			accountSupply.setSp_code(supply.getSp_code());
			accountSupply.setAccountId(accountId);
			accountSupplyMapper.saveAccountSupply(accountSupply);
			//添加系统管理员角色账号对改物资的权限
			Role role = new Role();
			role.setName("系统管理员");
			role.setDeleteFlag("T");
			List<Role> listRole = roleMapper.findAllRole(role);
			if(listRole!=null && listRole.size()>0){//存在该角色
				String roleId ="";
				for(Role ro : listRole){
					if("系统管理员".equals(ro.getName())){
						roleId = ro.getId();
						break;
					}
				}
				if(roleId!=null && !"".equals(roleId)){
					//查询该角色下账号信息
					AccountRole accountRole = new AccountRole();
					accountRole.setRoleId(roleId);
					List<AccountRole> listAccountRole = accountRoleMapper.findAccountRole(accountRole);
					if(listAccountRole!=null && listAccountRole.size()>0){
						AccountSupply admin = new AccountSupply();
						admin.setId(CodeHelper.createUUID());
						admin.setSp_code(supply.getSp_code());
						for(AccountRole aRole:listAccountRole){
							admin.setAccountId(aRole.getAccountId());
							accountSupplyMapper.saveAccountSupply(admin);
						}
					}
				}
				
				
			}
			if(supply.getSp_init()==null || "".equals(supply.getSp_init())){//如果页面没有传物资缩写，后台根据物资名称自动生成（针对excel导入的物资）wj 15.01.12
				supply.setSp_init(UperSp_code.getInit(supply.getSp_name()));//缩写转为大写
			}
			supply.setUnitper2(supply.getUnitper2()/supply.getUnitper0());
			supply.setUnitper3(supply.getUnitper3()/supply.getUnitper0());
			supply.setUnitper4(supply.getUnitper4()/supply.getUnitper0());
			
			supplyMapper.saveSupply_new(supply);//保存物资编码
			
			/**保存物资单位，规格，配送单位**/
			List<String> string = Arrays.asList(orderno.split("#"));//单位list
			List<String> disList = Arrays.asList(disunit.split("#"));//配送单位list
			HashMap<String,String> allUnit = new HashMap<String,String>();//合并后的单位
			//1.将单位和配送单位合并
			for(String unit : string){
				allUnit.put(unit.split(",")[0], unit);
			}
			for(String unit : disList){
				allUnit.put(unit.split(",")[0], unit);
			}
			Set<Entry<String,String>> set = allUnit.entrySet();
			for(Object unit : set){
				@SuppressWarnings("unchecked")
				Map.Entry<String,String> u = (Map.Entry<String,String>)unit;
				SupplyUnit supplyUnit = new SupplyUnit();
				supplyUnit.setId(Util.getUUID());
				supplyUnit.setSp_code(supply.getSp_code());
				supplyUnit.setUnit((String)u.getKey());
				String[] value = ((String)u.getValue()).split(",");
				supplyUnit.setUnitper(Double.parseDouble(value[1]));
				supplyUnit.setSequence(Integer.parseInt(value[2]));
				supplyUnit.setOrderNo(Integer.parseInt(value[3]));
				if(value.length >= 5){
					supplyUnit.setDismincnt(Double.parseDouble(value[4]));
					supplyUnit.setYnDisunit("Y");
				}else{
					supplyUnit.setYnDisunit("N");
				}
				supplyMapper.saveSupplyUnit(supplyUnit);
			}
			
//			for(int i=0;i<string.size(); i++){
//				String unitstr = string.get(i);
//				SupplyUnit supplyUnit = new SupplyUnit();
//				supplyUnit.setId(Util.getUUID());
//				supplyUnit.setSp_code(supply.getSp_code());
//				supplyUnit.setUnit(unitstr.split(",")[0]);
//				supplyUnit.setUnitper(Double.parseDouble(unitstr.split(",")[1]));
//				supplyUnit.setSequence(Integer.parseInt(unitstr.split(",")[2]));
//				supplyUnit.setOrderNo(i+1);
//				supplyUnit.setYnPsUnit(unitstr.split(",")[3]);
//				supplyMapper.saveSupplyUnit(supplyUnit);
//			}
			
			if(firmCodes!=null&&""!=firmCodes){
				String[] firmCodeStr = firmCodes.split(",");
				for(int i=0;i<firmCodeStr.length;i++){
					PositnSpcode positnSpcode = new PositnSpcode();
					positnSpcode.setSp_code(supply.getSp_code());
					positnSpcode.setSp_desc(supply.getSp_desc());
					positnSpcode.setSp_init(supply.getSp_init());
					positnSpcode.setSp_max1(supply.getSp_max1());
					positnSpcode.setSp_min1(supply.getSp_min1());
					positnSpcode.setSp_name(supply.getSp_name());
					positnSpcode.setSp_price(supply.getSp_price());
					positnSpcode.setUnit(supply.getUnit());
					positnSpcode.setPositn(firmCodeStr[i]);
					firmSupplyService.saveByAddOnly(positnSpcode);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public void saveSupply(Supply supply) throws CRUDException {
		try {
//			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			supply.setSp_init(UperSp_code.getInit(supply.getSp_name()));//缩写转为大写
			supplyMapper.saveSupply(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public void saveSupplyNew(Supply supply) throws CRUDException {
		try {
//			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			if(null!=supply && "".equals(supply.getSp_init())){
				supply.setSp_init(UperSp_code.getInit(supply.getSp_name()));//缩写转为大写				
			}

			supplyMapper.saveSupply_new(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新物资编码信息
	 * @param supply
	 * @throws CRUDException
	 */
	public void updateSupply(Supply supply) throws CRUDException {
		try {
			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			supplyMapper.updateSupply(supply);
			//物资设为禁用状态时候，设置分店物资属性里的是否可申购为否
			if ("N".equals(supply.getSta())){
				supplyMapper.updatePositnSpcode(supply);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新物资编码信息
	 * @param supply
	 * @throws CRUDException
	 */
	public void updateSupply_new(Supply supply,String orderno,String disunit) throws CRUDException {
		try {
			supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			if(supply.getUnitper2()!=null && !"".equals(supply.getUnitper2())){
				supply.setUnitper2(supply.getUnitper2()/supply.getUnitper0());
			}
			if(supply.getUnitper3()!=null && !"".equals(supply.getUnitper3())){
				supply.setUnitper3(supply.getUnitper3()/supply.getUnitper0());			
			}
			if(supply.getUnitper4()!=null && !"".equals(supply.getUnitper4())){
				supply.setUnitper4(supply.getUnitper4()/supply.getUnitper0());
			}
			
			supplyMapper.updateSupply_new(supply);//修改物资信息
			//修改物资对应的 supplyunit表
			supplyMapper.deleteByIds(supply.getSp_code());
			
			/**保存物资单位，规格，配送单位**/
			List<String> string = Arrays.asList(orderno.split("#"));//单位list
			List<String> disList = Arrays.asList(disunit.split("#"));//配送单位list
			HashMap<String,String> allUnit = new HashMap<String,String>();//合并后的单位
			//1.将单位和配送单位合并
			for(String unit : string){
				allUnit.put(unit.split(",")[0], unit);
			}
			for(String unit : disList){
				allUnit.put(unit.split(",")[0], unit);
			}
			Set<Entry<String,String>> set = allUnit.entrySet();
			for(Object unit : set){
				@SuppressWarnings("unchecked")
				Map.Entry<String,String> u = (Map.Entry<String,String>)unit;
				SupplyUnit supplyUnit = new SupplyUnit();
				supplyUnit.setId(Util.getUUID());
				supplyUnit.setSp_code(supply.getSp_code());
				supplyUnit.setUnit((String)u.getKey());
				String[] value = ((String)u.getValue()).split(",");
				supplyUnit.setUnitper(Double.parseDouble(value[1]));
				supplyUnit.setSequence(Integer.parseInt(value[2]));
				supplyUnit.setOrderNo(Integer.parseInt(value[3]));
				if(value.length >= 5){
					supplyUnit.setDismincnt(Double.parseDouble(value[4]));
					supplyUnit.setYnDisunit("Y");
					//更新的同时要更新positnspcode表的转换率和最小申购量
					supplyMapper.updatePsDisunit(supplyUnit);
				}else{
					supplyUnit.setYnDisunit("N");
				}
				supplyMapper.saveSupplyUnit(supplyUnit);
			}
			
//			for(int i=0;i<string.size(); i++){
//				String unitstr = string.get(i);
//				SupplyUnit supplyUnit = new SupplyUnit();
//				supplyUnit.setId(Util.getUUID());
//				supplyUnit.setSp_code(supply.getSp_code());
//				supplyUnit.setUnit(unitstr.split(",")[0]);
//				supplyUnit.setUnitper(Double.parseDouble(unitstr.split(",")[1]));
//				supplyUnit.setSequence(Integer.parseInt(unitstr.split(",")[2]));
//				supplyUnit.setOrderNo(i+1);
//				supplyUnit.setYnPsUnit(unitstr.split(",")[3]);
//				supplyMapper.saveSupplyUnit(supplyUnit);
//			}
			
			//物资设为禁用状态时候，设置分店物资属性里的是否可申购为否
			if ("N".equals(supply.getSta())){
				supplyMapper.updatePositnSpcode(supply);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除前查询是否已用
	 * @param ids
	 * @return
	 * @throws CRUDException
	 */
	public String findAllSupplySfyyByIds(String ids) throws CRUDException {
		try { 
			String result="";
			List<String> listId = Arrays.asList(ids.split(","));
			List<Supply> supplyList= supplyMapper.findAllSupplySfyyByIds(listId);
			for (int i = 0; i < supplyList.size(); i++) {
				Supply supply=supplyList.get(i);
				if("Y".equals(supply.getLocked())){
					result+=supply.getSp_name()+",";
				}
			}
			if(!"".equals(result)){
				result+=result+" 的使用状态为Y不能删除！";
			}
			return result;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除物资编码，并且级联删除物资编码对应权限，物资编码对应权限操作范围
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteSupply(List<String> listId) throws CRUDException {
		try {
			supplyMapper.deleteSupply(listId);
			//删除supplyunit表
			for(String sp_code:listId){
				supplyMapper.deleteByIds(sp_code);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除物资编码
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteAllSupply() throws CRUDException {
		try {
			supplyMapper.deleteAllSupply();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取小类中物资的最大编码+1返回
	 * @param code
	 * @return
	 */
	public String getMaxSpcode(String code) throws CRUDException {
		try {
            	return supplyMapper.getMaxSpcode(code);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询参考类别
	 */
	public List<Typoth> findTypoth(String code,String des) throws CRUDException{
		try {
			return supplyMapper.findTypoth(code,des);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public List<Tax> findTax(Integer id) throws CRUDException{
		try {
			return supplyMapper.findTax(id);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public boolean isSupplyCodeRepeated(Supply supply) throws CRUDException{
		try {
			return supplyMapper.SupplyCodeCount(supply)<1;
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *获取所有的虚拟物料 信息 ====用于物资编码，虚拟物料查询
	 *@author wangchao
	 */
	public List<Supply> selectSupply_x(Supply supply) throws CRUDException{
		try {
			return supplyMapper.selectSupply_x(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 跳转到虚拟物料修改页面
	 * @param supply
	 * @return
	 * @throws CRUDException
	 */
	public Supply toUpdateSupply_x(Supply supply) throws CRUDException{
		try {
			return supplyMapper.toUpdateSupply_x(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 虚拟物料修改
	 * @param supply
	 * @throws CRUDException
	 */
	public void saveByUpdateSupply_x(Supply supply) throws CRUDException{
		try {
			supplyMapper.saveByUpdateSupply_x(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *获取所有的虚拟物料 信息 
	 */
	public List<Supply> selectAllSupply_x(Supply supply) throws CRUDException{
		try {
			return supplyMapper.selectAllSupply_x(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 *获取所有的实际物料 信息 
	 */
	public List<Supply> selectAllSupply(Supply supply) throws CRUDException{
		try {
			return supplyMapper.selectAllSupply(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}


	/***
	 * 导出物资资料电子表格
	 * @param supply
	 * @param level
	 * @param code
	 * @param fromnum
	 * @param tonum
	 * @return
	 * @throws CRUDException
	 */
	public List<Supply> findAllSupplyExport(Supply supply, String level,
			String code, int fromnum,int tonum) throws CRUDException{
		try { 
			if(null!=supply && null!=supply.getSp_init()){
				supply.setSp_init(supply.getSp_init().toUpperCase());//缩写转为大写
			}
			if (null!=level) {
				if(level.equals("1")){   //大类
					supply.setGrptyp(code);
				}else if(level.equals("2")){//中类
					supply.setGrp(code);
				}else if(level.equals("3")){//小类
					supply.setSp_type(code);
				}	
			}
			HashMap<String,Object> contions = new HashMap<String, Object>(); 
			contions.put("supply", supply);
			contions.put("fromnum",fromnum );
			contions.put("tonum", tonum);
		//分页方法
			if(null!=supply && null!=supply.getIs_supply_x()&&"Y_group".equals(supply.getIs_supply_x())){//虚拟物资编码的          分组                方法
				return supplyMapper.selectAllSupply_x_export(contions);
			}else{
				return supplyMapper.findAllSupply_export(contions);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 检查一种虚拟物资对应多种实际物资，成本单位是否一样
	 * @param sp_code_x
	 * @return
	 */
	public String checkUnit(String sp_code_x,String sp_code)throws CRUDException{
		try {
			Supply supply = new Supply();
			supply.setSp_code(sp_code);
			supply.setSp_code_x(sp_code_x);
			String result;
			List<Supply> list = supplyMapper.checkUnit(supply);
			if (list.size() == 0) {
				result = null;
			} else {
				result = list.get(0).getUnit2();
			}
			return result;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检查虚拟物资编码和名称是否重复
	 * @param sp_code_x
	 * @param sp_name_x
	 * @return
	 * @throws CRUDException
	 */
	public int checkSupply_x(String sp_code_x,String sp_name_x)throws CRUDException{
		try {
			Supply supply = new Supply();
			supply.setSp_name_x(sp_name_x);
			supply.setSp_code_x(sp_code_x);
			return supplyMapper.checkSupply_x(supply);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据选中的虚拟物资  查询实际的物资
	 * @param supply
	 * @return
	 */
	public List<Supply> downSupply(Supply supply){
		return supplyMapper.downSupply(supply);
	}

	/***
	 * 加入分店
	 * @param acct
	 * @param ids
	 * @param codes
	 */
	public String joinPositn(String acct, String ids, String codes) {
		String result = "OK";
		Map<String,String> map = new HashMap<String,String>();
		map.put("acct", acct);
		map.put("ids", ids);
		map.put("codes", codes);
		supplyMapper.joinPositn(map);
		if(!"0".equals(String.valueOf(map.get("pr")))){
			result = "FAIL";
		}
		return result;
	}

	/***
	 * 加入分店（洞庭专用）wangjie 2014年12月9日 14:15:44
	 * @param acct
	 * @param ids
	 * @param codes
	 */
	public String joinPositnForDT(String acct, String ids, String codes) {
		String result = "OK";
//		int count = 0;
		String[] idList = ids.split(",");//物资
		String[] codeList = codes.split(",");//分店
		
		for(String vpositn:idList){
			for(String code:codeList){
				PositnSpcode spcode = new PositnSpcode();
				spcode.setPositn(code);
				spcode.setSp_code(vpositn);
				
				List<PositnSpcode> spcodeList = firmSupplyMapper.selectFirmSupplyForDT(spcode);
				if(spcodeList!=null && spcodeList.size()==0){
					firmSupplyMapper.insertSupplyForDT(spcode);
				}
			}
		}
//		if(count>0){
//			result = "FAIL";
//		}
		return result;
	}
	
	/***
	 * 关联用户 wjf
	 * @param acct
	 * @param ids
	 * @param accountids
	 * @return
	 */
	public String joinAccount(String acct, String ids, String accountids) {
		String[] supplyIdArray = ids.split(",");
		String[] accountidArray = accountids.split(",");
		for(String supplyId:supplyIdArray){
			for(String accountid:accountidArray){
				AccountSupply newAccountSupply = new AccountSupply();
				newAccountSupply.setId(CodeHelper.createUUID());
				newAccountSupply.setSp_code(supplyId);
				newAccountSupply.setAccountId(accountid);
				int i = accountSupplyMapper.findOneAccountSupply(newAccountSupply);
				if(i == 0){//如果没有，才插入数据
					accountSupplyMapper.saveAccountSupply(newAccountSupply);
				}
			}
		}
		return "OK";
	}
	
	/**
	 * 查询物资多单位
	 * @param positn 
	 * @param Positn
	 * @throws CRUDException 
	 */
	public List<SupplyUnit> findAllSupplyUnit(SupplyUnit supplyUnit) throws CRUDException{
		try{
			return supplyMapper.findAllSupplyUnit(supplyUnit);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	

	public void saveSupplyUnit(SupplyUnit supplyUnit) throws CRUDException {
		try {
			supplyUnit.setId(CodeHelper.createUUID());
			supplyMapper.saveSupplyUnit(supplyUnit);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新物资编码信息
	 * @param supplyUnit
	 * @throws CRUDException
	 */
	public void updateSupplyUnit(SupplyUnit supplyUnit) throws CRUDException {
		try {
			supplyMapper.updateSupplyUnit(supplyUnit);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 通过id查询物资单位
	 * @param supplyUnit
	 * @return
	 * @throws CRUDException
	 */
	public SupplyUnit findSupplyUnitById(SupplyUnit supplyUnit) throws CRUDException {
		try {
			//是否是  虚拟物料
			return supplyMapper.findSupplyUnitById(supplyUnit);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除物资单位信息
	 * @param listPositn deleteByIds
	 * @throws CRUDException
	 */
	public void deleteByIds(String code) throws CRUDException{
		try{
			supplyMapper.deleteByIds(CodeHelper.replaceCode(code));
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 物资单位信息
	 * @param listPositn deleteByIds
	 * @throws CRUDException
	 */
//	public List<SupplyUnit> findSupplyUnit(SupplyUnit supplyUnit)
//			throws CRUDException {
//		try {
//			return grpTypMapper.findAllGrpA(supplyUnit);
//		} catch (Exception e) {
//			log.error(e);
//			throw new CRUDException(e);
//		}
//	}
	
	/**
	 * 新增参考类别
	 */
	public String insertTypoth(Typoth typoth) throws CRUDException{
		try{
			List<Typoth> list = findTypoth(null,typoth.getDes().trim());
			//名称已存在
			if(list!=null && list.size()!=0){
				return "desError";
			}
			list = supplyMapper.findTypoth(typoth.getCode(),null);
			if(list!=null && list.size()!=0){
				return "codeError";
			}else{
				Map<String, Typoth> map = new HashMap<String, Typoth>();
				map.put("typoth", typoth);
				supplyMapper.insertTypoth(map);
				return "";
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改参考类别
	 * @param typoth
	 * @return
	 * @throws CRUDException
	 */
	public String updateTypoth(Typoth typoth) throws CRUDException{
		String str = "succ";
		try{
			List<Typoth> list = findTypoth(null,typoth.getDes().trim());
			//名称已存在
			if(list!=null&&list.size()>0){
				for(int i=0,len=list.size();i<len;i++){
					Typoth th = list.get(i);
					if(th!=null&&th.getCode()!=null&&typoth.getCode()!=null&&!th.getCode().equals(typoth.getCode())){
						str = "desError";
						break;
					}
				}
			}else{
				Map<String, Typoth> map = new HashMap<String, Typoth>();
				map.put("typoth", typoth);
				supplyMapper.updateTypoth(map);
			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return str;
	}
	
	public void deleteTypoth(Typoth typoth) throws CRUDException{
		try{
			Map<String, Typoth> map = new HashMap<String, Typoth>();
			map.put("typoth", typoth);
			supplyMapper.deleteTypoth(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量修改物资参考类别
	 */
	public void batchTypoth(Supply supply,String checkedcode) throws CRUDException{
		Map<String,Object> modelMap = new HashMap<String,Object>();
		String typoth = supply.getTypoth();
		if(typoth!=null && !typoth.equals(",")){
			supply.setTypoth(typoth.split(",")[0]);
			supply.setTypothdes(typoth.split(",")[1]);
		}
		modelMap.put("supply", supply);
		if(checkedcode!=null && !"".equals(checkedcode)){//选择需要修改的物资
			String[] sp_code = checkedcode.split(",");
			List<String> codeList = Arrays.asList(sp_code);
			modelMap.put("codeList", codeList);
		}
		supplyMapper.batchTypoth(modelMap);
	}
	
	/**
	 * 移除物资 N个单位及转换率相同的记录
	 * @param list
	 * @return
	 */
	public List<String> removeCo(Supply supply){
		List<String> unitString = new ArrayList<String>();
		unitString.add(supply.getUnit()+","+supply.getUnitper0());
		unitString.add(supply.getUnit1()+","+supply.getUnitper());
		unitString.add(supply.getUnit2()+","+supply.getUnitper2());
		unitString.add(supply.getUnit3()+","+supply.getUnitper3());
		unitString.add(supply.getUnit4()+","+supply.getUnitper4());
		List<String> string = new ArrayList<String>();
		
		 Set<Object> set = new HashSet<Object>();
//	        List newList = new ArrayList();
	        for (Iterator<String> iter = unitString.iterator(); iter.hasNext();) {
	            String element = iter.next();
	            if (set.add(element))
	            	string.add(element);
	        }
	        return string;

//		HashSet h  =   new  HashSet(unitString);
//		string.clear(); 
//		string.addAll(h);
		
//		  for (int i = 0; i < unitString.size(); i++){
//			  string.add(unitString.get(i));
//		         for (int j = i + 1; j < string.size(); j++){
//		             if (unitString.get(i).equals(string.get(j))){
//		            	 continue;
//		             }else{
//		            	 string.add(unitString.get(i));
//		             }
//		         }
//		  }
//		return string;
	}

	/***
	 * 查询出品加工间前十的半成品
	 * @author wjf
	 * @param supply
	 * @return
	 */
	public List<Supply> findSupplyExTop10(Supply supply) {
		//判断是不是走出品加工间的
		int count = explanMapper.findSpcodemodCount();
		if(count == 0){//没有说明只走物资表  默认加工间
			return supplyMapper.findSupplyListTopN(supply);
		}else{
			return supplyMapper.findSupplyExTop10(supply);
		}
	}
	
	/***
	 * 查询指定物资的配送片区默认仓位
	 * @param sp_code
	 * @param mod
	 * @return
	 */
	public Positn findSpcodeMod(String sp_code, String firm) {
		return supplyMapper.findSpcodeMod(sp_code,firm);
	}

	/***
	 * 查询此物资配送单位是不是已经引用
	 * @param su
	 */
	public int findSupplyUnitByPositnspcode(SupplyUnit su) throws CRUDException{
		try{
			return supplyMapper.findSupplyUnitByPositnspcode(su);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 根据供应商可供物资查询税率
	 * @param s
	 * @return
	 */
	public Supply findTaxByDeliverSpcode(Supply s) throws CRUDException {
		try{
			return supplyMapper.findTaxByDeliverSpcode(s);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
