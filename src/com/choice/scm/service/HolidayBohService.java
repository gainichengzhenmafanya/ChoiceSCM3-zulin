package com.choice.scm.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.HolidayBoh;
import com.choice.scm.persistence.HolidayBohMapper;

@Service
public class HolidayBohService {
	
	private static Logger log = Logger.getLogger(HolidayBohService.class);
	
	@Autowired
	private HolidayBohMapper holidayBohMapper;
	@Autowired
	private PageManager<HolidayBoh> pageManager;
	
	/**
	 * 显示所有节假日信息
	 * @param holiday
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<HolidayBoh> list(HolidayBoh holiday, Page page) throws CRUDException{
		try{
		return pageManager.selectPage(holiday, page, HolidayBohMapper.class.getName()+".list");
//		return holidayBohMapper.list(holiday);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 通过节假日主键查询节假日信息
	 * @param pk_holidaydefine
	 * @return 
	 * @throws CRUDException
	 */
	public HolidayBoh findHolidayById(String pk_holidaydefine) throws CRUDException{
		try{
			return holidayBohMapper.fingHolidayById(pk_holidaydefine);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增节假日信息
	 * @param holiday
	 * @throws CRUDException
	 */
	public void saveHoliday(HolidayBoh holiday) throws CRUDException{
			//Holiday ruler = holiday;
		try {
			holiday.setPk_holidaydefine(CodeHelper.createUUID().substring(0, 20).toUpperCase());
			holidayBohMapper.saveHoliday(holiday);
		} catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量新增假日信息
	 * @param holiday
	 * @throws CRUDException
	 */
	public void saveGenerateHoliday(HolidayBoh holiday) throws CRUDException{
			//Holiday ruler = holiday;
		try {
			deleteByVyear(holiday);
			Object[] holidays = getAllWeekendsByYear(Integer.parseInt(holiday.getVyear()));
			for(int i = 0; i < holidays.length; i++) {
				HolidayBoh day = new HolidayBoh();
				day.setVyear(holiday.getVyear());
				day.setVholidaygrade(1);
				day.setVholidayname("周末");
				day.setDholidaydate(holidays[i].toString());
				day.setEnablestate(2);
				day.setVmemo("");
				saveHoliday(day);
			}
		
		} catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改节假日信息
	 * @param holiday
	 * @throws CRUDException
	 */
	public void updateHoliday(HolidayBoh holiday) throws CRUDException{
		 try {
			 holidayBohMapper.updateHoliday(holiday);
		 } catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除节假日信息
	 * @param ids
	 * @throws CRUDException
	 */
	public void deleteHoliday(HolidayBoh holiday) throws CRUDException{
		try {
			List<String> idList=Arrays.asList(holiday.getPk_holidaydefine().split(","));
			for(int i=0;i<idList.size();i++){
				HolidayBoh holi=new HolidayBoh();
				holi.setPk_holidaydefine(idList.get(i));
				holi.setEnablestate(holiday.getEnablestate());
				holidayBohMapper.deleteHoliday(holi);
			}
		} catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 通过年份删除节假日
	 * @param holiday
	 * @throws CRUDException
	 */
	public void deleteByVyear(HolidayBoh holiday) throws CRUDException{
		try {
			holidayBohMapper.deleteByVyear(holiday);
		} catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *生成一年中所有的周六、周天 
	 * @param year
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private Object[] getAllWeekendsByYear(int year) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
		List <String> dates = new ArrayList<String>();
		Date beginDate = null;
		try {
			 // 开始时间
			beginDate = df.parse(year + "-01-01");
			int day = beginDate.getDay();	// 获取当前时间是星期几(0为星期天，6为星期六)
			int startSatOffset = 6 - day;	// 判断一年的第一天隔最近的星期六有几天
			
			// 结束时间
			Calendar calender = Calendar.getInstance();
			calender.setTime(beginDate);
			calender.add(Calendar.YEAR, 1);
			Date endDate = calender.getTime(); 
			int j = 1;
			for (int i = 0; i <= 365 / 7; i++) {

				Date satday = df.parse(year + "-" + j + "-" + (1 + startSatOffset + i * 7));// 星期六日期
				Date sunday = df.parse(year + "-" + j + "-"	+ (1 + startSatOffset + (i * 7 + 1))); // 星期天日期
				// 时间不能超过一年 （最后一个星期六不能超过结束时间）
				// 如果satday比endtime时间早
			
				if (satday.before(endDate)) {
					// 获取该年该月的最后一天的日期
					Date lastDate = df.parse(year + "-" + j + "-1");
					Calendar calendar2 = Calendar.getInstance();
					calendar2.setTime(lastDate);
					calendar2.add(Calendar.MONTH, j);
					calendar2.add(Calendar.DAY_OF_YEAR, -1);
					Date currentMonthLastDate = calendar2.getTime();// 当前月的最后一天
					// 当星期六超过了这个月的最后一天
					if (currentMonthLastDate.before(satday)) {
						j = j + 1;
						satday = df.parse(year + "-" + j + "-" + (1 + startSatOffset + i * 7));// 星期六日期
						sunday = df.parse(year + "-" + j + "-" + (1 + startSatOffset + (i * 7 + 1))); // 星期天日期
					}
					// 当星期六星是本月的最后一天
					if(currentMonthLastDate.equals(satday) && currentMonthLastDate.before(sunday)) {
						satday = df.parse(year + "-" + j + "-" + (1 + startSatOffset + i * 7));// 星期六日期
						j = j + 1;
						sunday = df.parse(year + "-" + j + "-" + (1 + startSatOffset + (i * 7 + 1))); // 星期天日期
					}
				}
				//判断当前的日期有没有超过当前年份
				if(satday.after(endDate) || sunday.after(endDate)) {
					return dates.toArray();
				}
				dates.add(df.format(satday));
				dates.add(df.format(sunday));
			}
		} catch (ParseException e) {
			log.error(e);
		}
		return dates.toArray();
	}
	
	
	/**
	 * 通过日期查询节假日信息  add ygb 20160120
	 * @param pk_holidaydefine
	 * @return 
	 * @throws CRUDException
	 */
	public List<HolidayBoh> fingHolidayByDholidaydate(String dholidaydate) throws CRUDException{
		try{
			return holidayBohMapper.fingHolidayByDholidaydate(dholidaydate);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
