package com.choice.scm.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Explan;
import com.choice.scm.domain.ExplanD;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SpCodeMod;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.CostCutMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.DisMapper;
import com.choice.scm.persistence.ExplanMMapper;
import com.choice.scm.persistence.ExplanMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.persistence.TeShuCaoZuoMapper;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.PublicExportExcel;

@Service

public class ChkinmService {
	
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private DisMapper disMapper;
	@Autowired
	private PageManager<Chkinm> pageManager;
	@Autowired
	private PageManager<Chkind> pageManager1;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private ExplanMapper explanMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private ExplanService explanService;
	@Autowired
	private ExplanMMapper explanMMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private TeShuCaoZuoMapper teShuCaoZuoMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private CostCutMapper costCutMapper;

	private final transient Log log = LogFactory.getLog(ChkinmService.class);
	
	/**
	 * 查询入库单号序列最大值
	 */
	public Integer getMaxChkinno() throws CRUDException {
		return chkinmMapper.getMaxChkinno();
	}
		
	/**
	 * 查询所有的入库单
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinm(String checkOrNot,Chkinm chkinm,Page page,Date madedEnd,String locale) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			if(null!=chkinm && null!=chkinm.getInout() && "zf".equals(chkinm.getInout())){
				chkinm.setInout("inout");//直发
			}else if(null!=chkinm && null!=chkinm.getInout() && "rk".equals(chkinm.getInout())){
				chkinm.setInout("in");//入库
			}
			map.put("locale", locale);
			map.put("chkinm", chkinm);
			map.put("checkOrNot", checkOrNot);
			map.put("madedEnd", madedEnd);//checkOrNot == check 或者 uncheck   时候 模糊查询  已审核或则未审核
			map.put("typ", chkinm.getTyp());
			log.warn("入库单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+
					",checkOrNot:"+map.get("checkOrNot")+","+
					chkinm
					);
			return pageManager.selectPage(map,page,ChkinmMapper.class.getName()+".findAllChkinmByinput");

			
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 查询所有的入库单(供应商结算)
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinmNew(String checkOrNot,Chkinm chkinm,Page page,Date madedEnd,String locale) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			if(null!=chkinm && null!=chkinm.getInout() && "zf".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.conk);
			}else if(null!=chkinm && null!=chkinm.getInout() && "rk".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.in);
			}
            if(chkinm!=null&&chkinm.getPositn()!=null&&chkinm.getPositn().getCode()!=null){
                String codes[]=chkinm.getPositn().getCode().split(",");
                chkinm.getPositn().setListCode(Arrays.asList(codes));
            }
            map.put("locale", locale);
			map.put("chkinm", chkinm);
			map.put("checkOrNot", checkOrNot);
			map.put("madedEnd", madedEnd);//checkOrNot == check 或者 uncheck   时候 模糊查询  已审核或则未审核
			return pageManager.selectPage(map,page,ChkinmMapper.class.getName()+".findAllChkinmByinputNew");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 供应商结算查询所有的入库单汇总
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public Chkinm findAllChkinmTotal(String checkOrNot,Chkinm chkinm,Date madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			if(null!=chkinm && null!=chkinm.getInout() && "zf".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.conk);
			}else if(null!=chkinm && null!=chkinm.getInout() && "rk".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.in);
			}
            if(chkinm!=null&&chkinm.getPositn()!=null&&chkinm.getPositn().getCode()!=null){
                String codes[]=chkinm.getPositn().getCode().split(",");
                chkinm.getPositn().setListCode(Arrays.asList(codes));
            }
			map.put("chkinm", chkinm);
			map.put("checkOrNot", checkOrNot);
			map.put("madedEnd", madedEnd);//checkOrNot == check 或者 uncheck   时候 模糊查询  已审核或则未审核
			log.warn("入库单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+
					",checkOrNot:"+map.get("checkOrNot")+","+
					chkinm
					);
			return chkinmMapper.findAllChkinmTotal(map);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询付款记录
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> findPayData(Chkinm chkinm, String madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			if(null!=chkinm && null!=chkinm.getInout() && "zf".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.conk);
			}else if(null!=chkinm && null!=chkinm.getInout() && "rk".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.in);
			}
            if(chkinm!=null&&chkinm.getPositn()!=null&&chkinm.getPositn().getCode()!=null){
                String codes[]=chkinm.getPositn().getCode().split(",");
                chkinm.getPositn().setListCode(Arrays.asList(codes));
            }
			map.put("chkinm", chkinm);
			map.put("madedEnd", DateFormat.getDateByString(madedEnd, "yyyy-MM-dd"));
			return chkinmMapper.findPayData(map);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询发票记录
	* @param chkinm
	* @param madedEnd
	* @return
	 * @throws CRUDException 
	 */
	public List<Map<String, Object>> findBillData(Chkinm chkinm, String madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			if(null!=chkinm && null!=chkinm.getInout() && "zf".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.conk);
			}else if(null!=chkinm && null!=chkinm.getInout() && "rk".equals(chkinm.getInout())){
				chkinm.setInout(ChkinmConstants.in);
			}
			map.put("chkinm", chkinm);
			map.put("madedEnd", DateFormat.getDateByString(madedEnd, "yyyy-MM-dd"));
			return chkinmMapper.findBillData(map);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 查询所有的入库单   模糊查询           暂时没用到
	 * @param chkinm
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkinm> findAllChkinmByinput(Chkinm chkinm,Page page,Date madedEnd) throws CRUDException {
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("chkinm", chkinm);
			map.put("madedEnd", madedEnd);
			log.warn("入库单查询:\n"+
					"bdat:"+DateFormat.getStringByDate(chkinm.getMaded(), "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(madedEnd, "yyyy-MM-dd")+","+
					chkinm
					);
			return pageManager.selectPage(map,page,ChkinmMapper.class.getName()+".findAllChkinmByinput");
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	/**
	 * 保存入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int saveChkinm(Chkinm chkinm,String inout) throws CRUDException {
		try {
			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			log.warn("添加入库单(主单)：\n"+chkinm);
	 		chkinmMapper.saveChkinm(chkinm);
	 		log.warn("添加入库单(详单)：");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				if(inout.equals("bh")){
					chkind.setInout(chkind.getInout());   //加入子单    标志    
				}else if(inout.equals("in")){
					chkind.setInout("in");   //加入入库标志
				}else if(inout.equals("zb")){
					chkind.setInout("inout");   //加入直发标志
				}
				log.warn(chkind);
	 			chkindMapper.saveChkind(chkind);
			}
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据id查询 入库单
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public Chkinm findChkinmByid(Chkinm chkinm) throws CRUDException {
		try {
			if(null==chkinm.getChkinno()){
				return null;
			}else{
				log.warn("获取入库单：" + chkinm.getChkinno());
				return chkinmMapper.findChkinmByid(chkinm);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改入库单
	 * @param chkinm
	 * @throws CRUDException
	 */
	public int updateChkinm(Chkinm chkinm) throws CRUDException {
		try {
			//验证该入库单是否已被审核
			Chkinm chkinmChec = findChkinmByid(chkinm);
			if(null != chkinmChec.getChecby() && !"".equals(chkinmChec.getChecby())){
				return -1;
			}
			//删除该条记录

			Date date=new Date();
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			
			log.warn("更新入库单(主单):\n"+chkinm);
	 		chkinmMapper.updateChkinm(chkinm);//修改入库单
	 		//修改入库单从表---思路   先删除  再插入
	 		//再添加入数据
	 		log.warn("更新入库单(详单):");
			for (int i = 0; i < chkinm.getChkindList().size(); i++) {
				Chkind chkind=chkinm.getChkindList().get(i);
				chkind.setChkinno(chkinm.getChkinno());
				log.warn(chkind);
	 			chkindMapper.saveChkind(chkind);
			}
			return  chkinm.getPr();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  验收入库方法
	 * @param chkinm
	 * @throws CRUDException
	 */
	public void saveYsrkChkinm(List<Dis>  listDistribution,String acct,String positnIn,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("验收入库：");
			String deliverCode=listDistribution.get(0).getDeliverCode();//供应商code
			String positnCode=listDistribution.get(0).getPositnCode();//仓位code
			ArrayList<Dis>  list=new ArrayList<Dis>();//临时list
			for (int i = 0; i < listDistribution.size(); i++) {//循环数据
				if(listDistribution.get(i).getPositnCode().equals(positnCode)
						&&listDistribution.get(i).getDeliverCode().equals(deliverCode)){
					list.add(listDistribution.get(i));
				}else{//不相等的时候
					saveChkinmByChkstom(list,acct,positnIn,maded,accountName);
					list.clear();//清空
					list.add(listDistribution.get(i));//在list重新加入该条数据
				}
				deliverCode=listDistribution.get(i).getDeliverCode();//把循环外的值换为当前数据
				positnCode=listDistribution.get(i).getPositnCode();  //把循环外的值换为当前数据
			}
			//最后处理  list大小不为0  时候  在作为一条入库单据加入
			if(list.size()!=0){
				saveChkinmByChkstom(list,acct,positnIn,maded,accountName);
			}
			//修改       验收入库成功的标志位
			HashMap<String,Object>  map=new HashMap<String, Object>();
			map.put("chkinSta", "chkinSta");//标志改
			map.put("acct", acct);
			map.put("chkin", "Y");//标志改
			map.put("listDis", listDistribution);//传入要修改的list 主要取ListId
			for (int i = 0; i < listDistribution.size(); i++) {//循环数据
				Dis dis=listDistribution.get(i);
				dis.setChkin("Y");
				String odisId =  dis.getId();//再赋回去，防止页面出现单引号
				dis.setId(CodeHelper.replaceCode(dis.getId()));
				disMapper.updateByIds(dis);
				dis.setId(odisId);
			}
//			distributionMapper.updateChkstodByList(map);//批量修改为                   已入库
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 验收入库转为 入库单的方法！ 
	 */
	public void saveChkinmByChkstom(ArrayList<Dis>  list,String acct,String positnIn,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("验收入库生成入库单:");
			Date date=new Date();
			Chkinm chkinm=new Chkinm();
			String vouno=calChkNum.getNext(CalChkNum.CHKIN,maded);
			int chkinno=getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.getDateByString(DateFormat.getStringByDate(maded,"yyyy-MM-dd"),"yyyy-MM-dd"));
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			Positn positn=new Positn();
			Deliver deliver=new Deliver();
			positn.setCode(positnIn);
			deliver.setCode(list.get(0).getDeliverCode());
			chkinm.setPositn(positn);
			chkinm.setDeliver(deliver);
			chkinm.setTyp("9900");//正常入库
			//chkinm.setMadeby(list.get(0).getChkstod().get);
			chkinm.setTotalamt(list.get(0).getAmountin()*list.get(0).getPricein());
		//	chkinm.setTotalpound(list.get(0).getChkstod().getTotalAmt());
			//chkinm.setInout(list.get(0).getInout());
			chkinm.setInout("in");  //直发单也当成 入库
//			if("直发".equals(list.get(0).getInout())){
//				chkinm.setInout("入库");
//			}else if("直配".equals(list.get(0).getInout())){
//				chkinm.setInout("直发");
//			}
		//	chkinm.setMadedto(list.get(0).getChkstod().get);
		//	chkinm.setTyp(list.get(0).getChkstod().get);
			ArrayList<Chkind> chkindList=new ArrayList<Chkind>();
			for (int j = 0; j < list.size(); j++) {
				Dis dis=(Dis)list.get(j);
				Chkind  chkind=new Chkind();//入库单从表
				if("inout".equals(dis.getInout())){
					chkinm.setTyp("直发入库");
				}
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				chkind.setChkinno(chkinno);
				chkind.setSupply(supply);
				chkind.setAmount(dis.getAmountin());//数量为采购数量 而非报货数量
				chkind.setPrice(dis.getPricein());
				chkind.setMemo(dis.getMemo());
				chkind.setDued(dis.getDued());
				chkind.setInout(dis.getInout());
				chkind.setPound(0);
				chkind.setSp_id(dis.getSp_id());
				chkind.setIndept(dis.getFirmCode());
//				chkind.setChkstono(Integer.parseInt(dis.getId()));
				chkind.setChkstono(dis.getChkstoNo());
				chkind.setAmount1(dis.getAmount1in());//这里改为取amount1in  2014.12.30wjf 也能兼容以前版本
				chkind.setPcno(dis.getPcno());//保存批次号
				chkindList.add(chkind);
			}
			chkinm.setChkindList(chkindList);
			saveChkinm(chkinm,"bh");//保存入库单		
			chkinm.setChecby(accountName);//加入审核人
			checkChkinm_one(chkinm,"bh");//同时审核入库单	bh  来自报货	
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 生产加工  转为入库单
	 * @throws CRUDException
	 */
	public void saveChkinm(List<Explan> explanList,String ids,String acct,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("生产加工生成入库单:");
			/*String positnCode=explanList.get(0).getSupply().getSp_position();//仓位code
			String positnexCode=explanList.get(0).getSupply().getPositnex();//加工间code
			ArrayList<Explan>  list=new ArrayList<Explan>();//临时list
			for (int i = 0; i < explanList.size(); i++) {//循环数据
				if(explanList.get(i).getSupply().getSp_position().equals(positnCode)
						&& explanList.get(i).getSupply().getPositnex().equals(positnexCode)){
					list.add(explanList.get(i));
				}else{//不相等的时候
					saveChkinmByExplan(list,acct,maded,accountName);
					list.clear();//清空
					list.add(explanList.get(i));//在list重新加入该条数据
				}
				positnCode=explanList.get(i).getSupply().getSp_position();//把循环外的值换为当前数据
				positnexCode=explanList.get(i).getSupply().getPositnex();  //把循环外的值换为当前数据
			}
			//最后处理  list大小不为0  时候  在作为一条入库单据加入
			if(list.size()!=0){
				saveChkinmByExplan(list,acct,maded,accountName);
			}*/
			saveChkinmByExplan(explanList,acct,maded,accountName);
			//修改为已经报货	
			HashMap<String, Object> map=new HashMap<String, Object>();
			List<String> listId=Arrays.asList(ids.split(","));
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			map.put("listId", listId);
			map.put("sta", "完成");//已经入库
			map.put("etim", sdf.format(new Date()));
			map.put("ynin", "Y");//已经入库
			explanMapper.updateExplan(map); 
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 生产加工  转为入库单
	 * @throws CRUDException
	 */
	public void saveChkinmM(List<ExplanD> explanList,String ids,String acct,Date maded,String accountName) throws CRUDException {
		try {
			log.warn("生产加工生成入库单:");
			//根据配送片区默认仓位分组，生成入库单
			List<ExplanD> splitList = new ArrayList<ExplanD>();
			String positn = "";
			for(int i=0,len=explanList.size();i<len;i++){
				if(i==0){
					//添加元素
					positn = explanList.get(i).getPositn();
					splitList.add(explanList.get(i));
				}else{
					//相同的仓位，则直接添加元素
					if(positn.equals(explanList.get(i).getPositn())){
						splitList.add(explanList.get(i));
					}else{
						//生成单子
						saveChkinmByExplanM(splitList,acct,maded,accountName,positn);
						//清空集合
						splitList.clear();
						//设置仓位信息
						positn = explanList.get(i).getPositn();
						//添加元素
						splitList.add(explanList.get(i));
					}
				}
				
			}
			//最后一次的集合要生成单子
			if(splitList.size()>0){
				saveChkinmByExplanM(splitList,acct,maded,accountName,positn);
			}
			//saveChkinmByExplanM(explanList,acct,maded,accountName);
			//修改为已经入库
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			ExplanD explanD = new ExplanD();
			explanD.setEtim(sdf.format(new Date()));
			explanD.setYnrk("Y");
			explanD.setIds(CodeHelper.replaceCode(ids));
			explanD.setAcct(acct);
			explanMMapper.updateCntact(explanD); 
			
			//生成入库单同时，自动核减
			
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 生产加工    转为 入库单的方法！ 
	 */
	public void saveChkinmByExplan(List<Explan> list,String acct,Date maded,String accountName) throws CRUDException {
		try {
			Date date=new Date();
			Chkinm chkinm=new Chkinm();
			String vouno=calChkNum.getNext(CalChkNum.CHKIN,maded);
			int chkinno=getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.getDateByString(DateFormat.getStringByDate(maded,"yyyy-MM-dd"),"yyyy-MM-dd"));
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			chkinm.setMadedto(DateFormat.formatDate(date,"yyyy-MM-dd"));
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setTyp(ChkinmConstants.explanIn);//产品入库
				Positn positn=new Positn();
				positn.setCode(list.get(0).getSupply().getSp_position());
			chkinm.setPositn(positn);   //仓位
				Deliver deliver=new Deliver();
				deliver.setPositn(list.get(0).getSupply().getPositnex());
			chkinm.setDeliver(explanService.findDeliverByPositn(deliver)); //供应商   
			chkinm.setMemo("加工生产入库");
			//chkinm.setMadeby(list.get(0).getChkstod().get);
//chkinm.setTotalamt(list.get(0).getChkstod().getTotalAmt());
		//	chkinm.setTotalpound(list.get(0).getChkstod().getTotalAmt());
//			chkinm.setInout(list.get(0).getChkstod().getInout());
		//	chkinm.setMadedto(list.get(0).getChkstod().get);
		//	chkinm.setTyp(list.get(0).getChkstod().get);
			ArrayList<Chkind> chkindList=new ArrayList<Chkind>();
			double amountAll = 0;
			double amount1All = 0;
			for (int j = 0; j < list.size(); j++) {
				Explan explan=(Explan)list.get(j);
				Chkind  chkind=new Chkind();//入库单从表
				chkind.setChkinno(chkinno);
				chkind.setSupply(explan.getSupply());
				chkind.setAmount(explan.getCntact());  //数量
				chkind.setPrice(explan.getSupply().getSp_price());
//				chkind.setMemo(explan.getChkstod().getMemo());
//				chkind.setDued(explan.getChkstod().getDued());
//				chkind.setInout(explan.getChkstod().getInout());
//				chkind.setPound(explan.getChkstod().getPound());
//				chkind.setSp_id(explan.getChkstod().getSp_id());
			//	chkind.setIndept(dis.getChkstod().geti);
//				chkind.setChkstono(explan.getChkstod().getId());
				chkind.setAmount1(explan.getCntact1());
				chkindList.add(chkind);
				amountAll += explan.getCntact();
				amount1All += explan.getCntact1();
			}
			if (amountAll!=0 || amount1All!=0) {
				//标准数量和参考数量至少有一个不为0的时候才生成入库单
				chkinm.setChkindList(chkindList);
				saveChkinm(chkinm,"in");//保存入库单	
			}
			//不用审核
		//	chkinm.setChecby(accountName);//加入审核人
		//	checkChkinm2(chkinm);//同时审核入库单		
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新流程生产加工    转为 入库单的方法！
	 * author: cs
	 * @param list
	 * @param acct
	 * @param maded
	 * @param accountName
	 * @param positn
	 * @throws CRUDException
	 */
	public void saveChkinmByExplanM(List<ExplanD> list,String acct,Date maded,String accountName,String positnCode) throws CRUDException {
		try {
			Date date=new Date();
			Chkinm chkinm=new Chkinm();
			String vouno=calChkNum.getNext(CalChkNum.CHKIN,maded);
			int chkinno=getMaxChkinno();
		  	chkinm.setAcct(acct);
		  	chkinm.setMadeby(accountName);
		  	chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
		  	chkinm.setChkinno(chkinno);
		  	chkinm.setVouno(vouno);
		  	chkinm.setMaded(DateFormat.formatDate(date, "yyyy-MM-dd"));
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			chkinm.setMadedto(DateFormat.formatDate(date,"yyyy-MM-dd"));
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setTyp(ChkinmConstants.explanIn);//产品入库
			Positn positn=new Positn();
			positn.setCode(positnCode);
			chkinm.setPositn(positn);   //仓位
			Deliver deliver=new Deliver();
			deliver.setPositn(list.get(0).getSupply().getPositnex());
			chkinm.setDeliver(explanService.findDeliverByPositn(deliver)); //供应商   
			chkinm.setMemo("加工生产入库");
			ArrayList<Chkind> chkindList=new ArrayList<Chkind>();
			double amountAll = 0;
			double amount1All = 0;
			for (int j = 0; j < list.size(); j++) {
				ExplanD explan=(ExplanD)list.get(j);
				Chkind  chkind=new Chkind();//入库单从表
				chkind.setChkinno(chkinno);
				chkind.setSupply(explan.getSupply());
				chkind.setAmount(explan.getAmountin());  //改为需求数量
				chkind.setPrice(explan.getSupply().getSp_price());
				chkind.setAmount1(explan.getAmountin()*explan.getSupply().getUnitper());
				chkind.setExtim(explan.getExtim());
				chkindList.add(chkind);
				amountAll += explan.getAmount();
				amount1All += explan.getAmount()*explan.getSupply().getUnitper();
			}
			if (amountAll!=0 || amount1All!=0) {
				//标准数量和参考数量至少有一个不为0的时候才生成入库单
				chkinm.setChkindList(chkindList);
				saveChkinm(chkinm,"in");//保存入库单					
				//入库单添加成功之后，自动核减
				// 调用核减存储过程				
			}
			//不用审核
		//	chkinm.setChecby(accountName);//加入审核人
		//	checkChkinm2(chkinm);//同时审核入库单		
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  审核入库单      单条
	 * @param chkinm
	 * @throws CRUDException
	 */
	public void checkChkinm_one(Chkinm chkinm,String from) throws CRUDException {
		try {
			chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//会计月 
			if("bh".equals(from)){
				chkinm.setStatus(chkinm.getInout());
			}else{
				chkinm.setStatus("in");//入库
			}
			chkinmMapper.checkChkinm(chkinm);//审核入库单
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除入库单
	 * @param listId
	 * @throws CRUDException
	 */
	public void deleteChkinm(List<String> listId) throws CRUDException {
		try {
			log.warn("删除入库单:\n"+listId);
			//验证要删除的入库单是否已被审核：n--未审核，y--已审核
			String sta = "n";
			for(String id : listId){
				Chkinm chkinm = new Chkinm();
				chkinm.setChkinno(Integer.parseInt(id));
				Chkinm chkinmChec = findChkinmByid(chkinm);
				if(null != chkinmChec.getChecby() && !"".equals(chkinmChec.getChecby())){
					sta = "y";
				}
			}
			if("y".equals(sta)){
				return;
			}else{
				chkinmMapper.deleteChkinm(listId);
				//删除从表
				chkindMapper.deleteChkindByChkinno(listId);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核入库单   单条或批量审核
	 * @param listId
	 * @throws CRUDException
	 */
	public String checkChkinm(Chkinm chkinm,String chkinnoids) throws CRUDException {
		try {
			Map<String,String> result = new HashMap<String,String>();
			int pr=1;
			log.warn("审核入库单：\n");
			if(null==chkinnoids){
				log.warn(chkinm);
				chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm.getMaded()));//此处所有方法都不在用yearr字段，xml也都已修改 20170607wjf 
				chkinmMapper.checkChkinm(chkinm);//单条审核入库单
				pr=chkinm.getPr();
				if(pr == 1){
					//如果审核成功，判断是否加工间产品入库，需要进行核减
					int count = chkinmMapper.findDeliverTypByChkinno(chkinm);
					if(count == 1){
						costCutMapper.excuteExcostcut(chkinm);
					}
				}
			}else{
				log.warn(chkinnoids);
				List<String> ids = Arrays.asList(chkinnoids.split(","));
				for (int i = 0; i < ids.size(); i++) {
					chkinm.setChkinno(Integer.parseInt(ids.get(i)));
					Chkinm chkinm_ = chkinmMapper.findChkinmByid(chkinm);
					chkinm.setMonth(acctService.getOnlyAccountMonth(chkinm_.getMaded()));//会计月 
					chkinmMapper.checkChkinm(chkinm);//循环批量 审核入库单
					pr=chkinm.getPr();
					if(pr == 1){
						//如果审核成功，判断是否加工间产品入库，需要进行核减
						int count = chkinmMapper.findDeliverTypByChkinno(chkinm);
						if(count == 1){
							costCutMapper.excuteExcostcut(chkinm);
						}
					}
				}
			}
			result.put("pr", pr+"");	
			result.put("checby", chkinm.getChecby());
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询是否必须入到默认仓位
	 * @param chkinm
	 * @throws CRUDException
	 */
	@SuppressWarnings("finally")
	public String chkPositn(SpCodeMod spCodeMod) throws CRUDException {
		String msg = "OK";
		try {
			if ("".equals(spCodeMod.getSp_code())) {
				msg = "入库单据不能为空！";
				return msg;
			}
			spCodeMod.setSp_code(CodeHelper.replaceCode(spCodeMod.getSp_code()));
			Supply supply = new Supply();
			supply.setSp_code(spCodeMod.getSp_code());
			if ("0".equals(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "INOUT_IS_NOT"))) {
				int aaa = chkinmMapper.getPositn(spCodeMod).size();
				int bbb = chkinmMapper.getPositn1(supply).size();
				if (aaa==0 && bbb==0) {
					msg = "物资未设置默认仓位!";
					return msg;
				}
				boolean first = true;
				List<SpCodeMod> spCodeModList = chkinmMapper.chkPositn(spCodeMod);
				for (int i=0; i<spCodeModList.size(); i++) {
					if (first) {
						msg = spCodeModList.get(i).getSp_code();
					}else {
						msg = msg + "," + spCodeModList.get(i).getSp_code();
					}
					first=false;
				}
				if (!"OK".equals(msg)) {
					msg = msg + "的默认仓位不是所选入库仓位!";
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		finally{
			return  msg;
		}
	}
	/**
	 * 检测要保存、删除的入库单据是否被审核
	 * @param chkinm
	 * @return
	 * @throws CRUDException
	 */
	public String chkChect(String active,Chkinm chkinm) throws CRUDException {
		try {
			String showM = "YES",action = ""; 
			if("edit".equals(active)){
				action = "修改保存";
			}else if("delete".equals(active)){
				action = "删除";
			}
			Map<String,Object> map = new HashMap<String, Object>();
			Chkinm chkinm1=new Chkinm();
			chkinm1.setChkinno(chkinm.getChkinno());
			map.put("chkinm", chkinm1);
			List<Chkinm> listChecked = pageManager.selectPage(map,new Page(),ChkinmMapper.class.getName()+".findAllChkinmByinput");
			switch(listChecked.size()){
				case 0 : 
					showM = "凭证号："+chkinm.getVouno()+"的单据已被删除，不能进行"+action+"操作。"; 
					break;
				case 1 :  
					if(null != listChecked.get(0).getChecby() && !"".equals(listChecked.get(0).getChecby())){
						showM = "凭证号："+chkinm.getVouno()+"的单据已被审核，不能进行"+action+"操作。"; 
					}
					break;
				default :
					showM = "凭证号："+chkinm.getVouno()+"的单据数据记录异常，请核对。";
					break;
			}
			return showM;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取入库数据
	 * @param listId
	 * @throws CRUDException
	 */
	public List<Spbatch> addChkinmByCx(Spbatch spbatch) throws CRUDException {
		try {
			return chkinmMapper.addChkinmByCx(spbatch);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取直发冲消数据
	 * @author 2014.11.4 wjf
	 * @param spbatch
	 * @throws CRUDException
	 */
	public List<Spbatch> addChkinmzfByCx(Spbatch spbatch) throws CRUDException {
		try {
			Positn positn1=new  Positn();
			positn1.setCode(spbatch.getPositn());
			Positn positn= positnMapper.findPositnByCode(positn1);
			if("1203".equals(positn.getPcode())){// 分店   冲销   spbatch_x
				return chkinmMapper.addChkinmzfByCx_x(spbatch);
			}else{// 其它   冲销   spbatch
				return chkinmMapper.addChkinmzfByCx(spbatch);
			}
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核结账
	* @param chkinm
	 * @throws CRUDException 
	 */
	public void checkedChkinmBill(String chkinno) throws CRUDException {
		try {
			chkinmMapper.checkedChkinmBill(CodeHelper.replaceCode(chkinno));
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据主键字符串查找数据
	* @param chkinno
	* @return
	 * @throws CRUDException 
	 */
	public List<Chkinm> findChkinmByIds(String chkinno) throws CRUDException {
		try {
			return chkinmMapper.findChkinmByIds(CodeHelper.replaceCode(chkinno));
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据主键字符串查找数据
	* @param chkinno
	* @return
	 * @throws CRUDException 
	 */
	public List<Chkind> findAllChkind(Chkind chkind, Page page) throws CRUDException {
		try {
			return pageManager1.selectPage(chkind, page, ChkindMapper.class.getName()+".findAllChkind");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 付款
	* @param listChkinm
	* @param dat
	* @param memo
	* @param pay
	* @throws CRUDException
	 */
	public void payMoney(List<Chkinm> listChkinm, Date dat, String memo,double pay,String madeby) throws CRUDException {
		try {
            if(listChkinm!=null&&listChkinm.size()>0){
                Chkinm chkinm=new Chkinm();
                chkinm.setMadeby(madeby);//当前操作人
                chkinm.setMaded(dat);
                chkinm.setMemo(memo);
                chkinm.setDeliver(listChkinm.get(0).getDeliver());
                chkinm.setPay1(pay);
                chkinm.setPay(pay);
                Integer v = chkinmMapper.getGen_foliNextVal();//wjf
                chkinm.setFolio(v);//临时借用字段放主键
                chkinmMapper.addFolio(chkinm);
                BigDecimal money=BigDecimal.valueOf(pay);
                
                //存在入库冲销的记录，则先判断付款总金额是否大于等于计算的应该付款的总金额
                double yfsum = 0;
                for(int i=0;i<listChkinm.size();i++){
                    Chkinm chk = listChkinm.get(i);
                    yfsum += (chk.getTotalamt()-chk.getPay());
                }
                //付款的金额大于应付款的
                if(pay>=yfsum){
                    for(int i=0;i<listChkinm.size();i++){
                        Chkinm chk = listChkinm.get(i);
                        Chkinm cm=new Chkinm();
                        cm.setChkinno(chk.getChkinno());
                        cm.setInout(chk.getInout());
                        cm.setFolio(chkinm.getFolio());//临时借用字段放主键
                        
                        //减
						money=money.subtract(BigDecimal.valueOf(chk.getTotalamt()-chk.getPay()));
						cm.setPay(chk.getTotalamt()-chk.getPay());//foliochkm
						cm.setPay1(chk.getTotalamt());//出入库
						
						//最后一个单子的支付金额是否大于0
                        if((i+1)==listChkinm.size()&&money.doubleValue()!=0){
                            cm.setPay(money.add(BigDecimal.valueOf(cm.getPay())).doubleValue());//foliochkm
                            cm.setPay1(money.add(BigDecimal.valueOf(cm.getPay1())).doubleValue());//出入库
                        }
                        //更新数据
                        chkinmMapper.addFolioChkm(cm);
                        chkinmMapper.updateChkinmPay(cm);
                    }
                }
                else{
	                for(int i=0;i<listChkinm.size();i++){
	                    Chkinm chk = listChkinm.get(i);
	                    Chkinm cm=new Chkinm();
	                    cm.setChkinno(chk.getChkinno());
	                    cm.setInout(chk.getInout());
	                    cm.setFolio(chkinm.getFolio());//临时借用字段放主键
	                    if(money.compareTo(BigDecimal.valueOf(chk.getTotalamt()-chk.getPay()))>0){//支付金额是否大于 需支付金额+已支付金额
	                        money=money.subtract(BigDecimal.valueOf(chk.getTotalamt()-chk.getPay()));
	                        cm.setPay(chk.getTotalamt()-chk.getPay());//foliochkm
	                        cm.setPay1(chk.getPay()+(chk.getTotalamt()-chk.getPay()));//出入库
	                    }else if(money.compareTo(BigDecimal.valueOf(chk.getTotalamt()-chk.getPay()))<=0){//支付金额是否小于等于 需支付金额+已支付金额
	                        cm.setPay(money.doubleValue());//foliochkm
	                        cm.setPay1(money.add(BigDecimal.valueOf(chk.getPay())).doubleValue());//出入库
	                        money=BigDecimal.valueOf(0);
	                    }
	                    if((i+1)==listChkinm.size()&&money.doubleValue()!=0){//最后一个单子的支付金额是否大于0
	                        cm.setPay(money.add(BigDecimal.valueOf(cm.getPay())).doubleValue());//foliochkm
	                        cm.setPay1(money.add(BigDecimal.valueOf(cm.getPay1())).doubleValue());//出入库
	                    }
	                    chkinmMapper.addFolioChkm(cm);
	                    chkinmMapper.updateChkinmPay(cm);
	                }
                }
            }
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 添加发票
	* @param deliverCode
	* @param dat
	* @param memo
	* @param pay
	 * @throws CRUDException 
	 */
	public void addBill(Chkinm chkinm) throws CRUDException {
		try {
			chkinmMapper.addBill(chkinm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加供应商未结金额
	* @param deliverCode
	* @param dat
	* @param memo
	* @param pay
	 * @throws CRUDException 
	 */
	public void addDeliverMoney(Chkinm chkinm) throws CRUDException {
		try {
            chkinm.setYearr(teShuCaoZuoMapper.findMain().getYearr());
			chkinmMapper.addDeliverMoney(chkinm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response, HttpServletRequest request) throws IOException {
		PublicExportExcel.downloadTemplate(response, request, "入库单.xls"); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return PublicExportExcel.uploadToTemp(request, response);  //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 对execl进行验证
	 */
	public Object check(String path,String accountId) throws CRUDException {
		boolean checkResult = ReadChkinmExcel.check(accountId,path, supplyMapper,positnMapper,deliverMapper);
//		Map<String, String> map = new HashMap<String, String>();
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			Chkinm chkinm = ReadChkinmExcel.chkinm;
			chkinm.setAcct(accountId);
			List<Chkind> chkindList = ReadChkinmExcel.chkindList;
			chkinm.setChkindList(chkindList);
			return chkinm;
		} else {
//			map = ReadChkstomExcel.map;
			errorList = ReadChkinmExcel.errorList;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}
}
