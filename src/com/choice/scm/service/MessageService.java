package com.choice.scm.service;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Message;
import com.choice.scm.persistence.MessageMapper;

@Service
public class MessageService {

	@Autowired
	private MessageMapper messageMapper;
	@Autowired
	private PageManager<Message> pageManager;
	
	private final transient Log log = LogFactory.getLog(MessageService.class);
	
	/**
	 * 写邮件
	 * @param message
	 */
	public void sendMessage(Message message) throws CRUDException{
		try{
			messageMapper.sendMessage(message);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改邮件状态（修改state由0改为1）
	 * String id
	 */
	public void updateMessage(String id) throws CRUDException{
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("id", id);
		try{
			messageMapper.updateMessage(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询邮件内容
	 * String id
	 */
	public Message findMessageById(String id) throws CRUDException{
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("id", id);
		try{
			return messageMapper.findMessageById(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找所有邮件
	 * String receiver
	 * String searchInfo
	 * String orderby:sender,sendtime desc,state
	 */
	public List<Message> findAllMessage(String receiver,String title,String content,String senderName,Date startTime,Date endTime,Page page) throws CRUDException{
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("receiver", receiver);
		map.put("title", title);
		map.put("content", content);
		map.put("senderName", senderName);
		map.put("startTime", startTime);
		if(endTime!=null){
			Calendar   calendar   =   new   GregorianCalendar(); 
			calendar.setTime(endTime); 
			calendar.add(calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动 
			endTime=calendar.getTime();
		}
		map.put("endTime", endTime);
		try{
			return pageManager.selectPage(map, page, MessageMapper.class.getName()+".findAllMessage");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询未读邮件数
	 * String receiver
	 */
	public int findUnReadMessage(String receiver) throws CRUDException{
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("receiver", receiver);
		try{
			return messageMapper.findUnReadMessage(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除邮件
	 * String receiver
	 */
	public void deleteMessage(String ids) throws CRUDException{
		List<String> list = Arrays.asList(ids.split(","));
		try{
			messageMapper.deleteMessage(list);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询账号信息
	 */
	public List<Message> findAccount(String searchInfo) throws CRUDException{
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("searchInfo", searchInfo);
		try{
			return messageMapper.findAccount(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}