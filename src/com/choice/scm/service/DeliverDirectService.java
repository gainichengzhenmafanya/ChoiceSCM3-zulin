package com.choice.scm.service;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.FirmDeliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.DeliverDirectMapper;
import com.choice.scm.persistence.FirmDeliverMapper;

@Service
public class DeliverDirectService {

	@Autowired
	private DeliverDirectMapper deliverDirectMapper;
	@Autowired
	private FirmDeliverMapper firmDeliverMapper;
	@Autowired
	private PageManager<Positn> pageManager;
	
	/**
	 * 根据 供应商查询仓位信息
	 * @param deliverCode
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> findPositnByDeliver(String deliverCode,Page page) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("deliverCode", deliverCode);
			return pageManager.selectPage(map, page, DeliverDirectMapper.class.getName()+".findPositnByDeliver");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据供应商查询 全部仓位编码
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public String findAllPositnByDeliver(String deliverCode) throws CRUDException{
		StringBuffer result = new StringBuffer();
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("deliverCode", deliverCode);
			List<Positn> list = deliverDirectMapper.findPositnByDeliver(map);
			if(list.size()>0){
				for(Positn positn:list){
					result.append(positn.getCode()+",");
				}
				return result.substring(0, result.length()-1);
			}
			return "";
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据 供应商增加仓位信息
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public void savePositnByDeliver(String acct,String deliver,String positnString) throws CRUDException{
		try{
			FirmDeliver firmDeliver = new FirmDeliver();
			String[] positnArray = positnString.split(",");
			for(String positn:positnArray){
				firmDeliver = new FirmDeliver();
				firmDeliver.setAcct(acct);
				firmDeliver.setFirm(positn);
				firmDeliver.setDeliver(deliver);
				List<FirmDeliver> fd = deliverDirectMapper.findFrimDeliverByFirm(firmDeliver);
				firmDeliver.setInout(1);
				if(fd.size() == 0){//如果没有数据，则新增，有的话直接把inout改为1
					deliverDirectMapper.saveDeliverDirect(firmDeliver);
				}else{
					deliverDirectMapper.updateDeliverDirect(firmDeliver);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 检查是否已经存在该数据
	 */
	public String checkOne(String firm,String deliver) throws CRUDException{
		Map<String,Object> map=new HashMap<String, Object>();
		Deliver del=new Deliver();
		try{
			del.setCode(deliver);
			del.setFirm(firm);
			String inout=firmDeliverMapper.findInout(del);
			if(null!=inout && !"".equals(inout)){
				map.put("pr", "0");
			}else{
				map.put("pr", "1");
			}
			JSONObject rs = JSONObject.fromObject(map);
			return rs.toString();
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据供应商删除仓位信息
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public void deletePositnByDeliver(String acct,String deliverCode,String positnString) throws CRUDException{
		try{
			List<String> positnList = Arrays.asList(positnString.split(","));
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("acct", acct);
			map.put("deliver", deliverCode);
			map.put("positnList", positnList);
			
			deliverDirectMapper.deleteDeliverDirect(map);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/***
	 * 根据门店查询他配置的供应商直配信息（报货单填制提交查询价格前 查询方向用）   wjf
	 * @param firmDeliver
	 * @return
	 */
	public List<FirmDeliver> findFrimDeliverByFirm(FirmDeliver firmDeliver){
		return deliverDirectMapper.findFrimDeliverByFirm(firmDeliver);
	}
}