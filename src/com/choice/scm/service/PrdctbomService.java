package com.choice.scm.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Product;
import com.choice.scm.domain.Spcodeexm;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.PrdctbomMapper;

@Service
public class PrdctbomService {
	private static Logger log = Logger.getLogger(PrdctbomService.class);
	
	@Autowired
	private PrdctbomMapper prdctMapper;
	@Autowired
	private AcctMapper acctMapper;
	
	/**
	 * 模糊查询半成品
	 * @param prdct
	 * @throws CRUDException 
	 */
	public List<Supply> findPrdct(Supply prdct) throws Exception{
		try{
			return prdctMapper.findPrdct(prdct);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 门店模糊查询半成品
	 * @param prdct
	 * @throws CRUDException 
	 */
	public List<Supply> findPrdctFirm(Supply prdct) throws Exception{
		try{
			return prdctMapper.findPrdctFirm(prdct);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据key模糊查询半成品
	 * @param key
	 * @throws CRUDException 
	 */
	public List<Supply> findByKey(String key, String prdctcard, String code, String acct) throws Exception{
		try{
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("key", key);
			map.put("prdctcard", prdctcard);
			map.put("code", code);
			map.put("acct", acct);
			return prdctMapper.findByKey(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据key模糊查询半成品
	 * @param key
	 * @throws CRUDException 
	 */
	public List<Supply> findByKeyFirm(String key, String prdctcard, String code, String acct) throws Exception{
		try{
			HashMap<String, Object> map=new HashMap<String, Object>();
			map.put("key", key);
			map.put("prdctcard", prdctcard);
			map.put("code", code);
			map.put("acct", acct);
			return prdctMapper.findByKeyFirm(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据code查询原材料
	 * @param code
	 * @throws CRUDException 
	 */
	public List<Product> findmaterial(Spcodeexm spcodeexm) throws Exception{
		try{
			return prdctMapper.findmaterial(spcodeexm);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询物资存在的产品以及物资用量
	 * @param code
	 * @throws CRUDException 
	 */
	public List<Product> listResult(Product product) throws Exception{
		try{
			return prdctMapper.listResult(product);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据出入的多条id   查询产品bom
	 * @throws Exception
	 */
	public List<Product> findmaterialBylist(Product product) throws Exception{
		try{
			HashMap<String, Object> map=new HashMap<String, Object>();
			List<String> itemids = Arrays.asList(product.getItem().split(","));
			map.put("product", product);
			map.put("itemids", itemids);
			return prdctMapper.findmaterialBylist(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 生成加工  申购单跳转   分组求和（生成报货单）
	 * @param ids   explan的主键id  的list
	 * @param acct
	 * @return
	 * @throws Exception
	 */
	public List<Product> findSumMaterialBylist(String ids,String acct) throws Exception{
		try{
			HashMap<String, Object> map=new HashMap<String, Object>();
			List<String> idList = Arrays.asList(ids.split(","));
			map.put("idList", idList);
			map.put("acct", acct);
			return prdctMapper.findSumMaterialBylist(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 生成加工  申购单跳转   分组求和(生成出库单)
	 * @param explanno
	 * @param acct
	 * @return
	 * @throws Exception
	 */
	public List<Product> findSumMaterialByExplanno(String ids, String acct,String positn) throws Exception{
		try{
			HashMap<String, Object> map=new HashMap<String, Object>();
			List<String> idList = Arrays.asList(ids.split(","));
			map.put("idList", idList);
			map.put("acct", acct);
			map.put("positn", positn);
			map.put("yearr", acctMapper.findYearrByDate(new Date()));
			return prdctMapper.findSumMaterialByExplanno(map);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 原材料操作
	 * @param pro
	 * @throws CRUDException 
	 */
	public int saveOrUpdateOrDelPrdctbom(Product pro) throws Exception{
		try{
			prdctMapper.saveOrUpdateOrDelPrdctbom(pro);
			return  pro.getPr();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据物资编码获取物资名称
	 * @param supply
	 * @throws CRUDException 
	 */
	public String getItemNm(Supply supply) throws Exception{
		try{
			return prdctMapper.getItemNm(supply);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询已做成本卡Id   list
	 * @param product
	 * @return
	 */
	public List<Product> findAllSpcodeexmIdList(Product product) throws CRUDException {
		try {
			return prdctMapper.findAllSpcodeexmIdList(product);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 导出/gzjj
	 */
	public boolean exportExcel(OutputStream os, List<Product> list) throws CRUDException {   
		WritableWorkbook workBook = null;
		
		try {
			int totalCount=0;
			if (list!=null) {
				totalCount = list.size();
			}
			int sheetNum = totalCount;
			workBook = Workbook.createWorkbook(os);
			for(int i=0;i<sheetNum;i++){
				WritableSheet sheet = workBook.createSheet(list.get(i).getItemDes(), i);
				sheet.addCell(new Label(0, 0,"半成品："+list.get(i).getItem()+"     "+list.get(i).getItemDes()));
				sheet.addCell(new Label(0, 1, "编码")); 
	            sheet.addCell(new Label(1, 1, "名称"));   
	            sheet.addCell(new Label(2, 1, "规格")); 
				sheet.addCell(new Label(3, 1, "成本单位"));  
				sheet.addCell(new Label(4, 1, "成本净用量"));
				sheet.addCell(new Label(5, 1, "成本毛用量"));
				sheet.addCell(new Label(6, 1, "标准单位"));
				sheet.addCell(new Label(7, 1, "标准毛用量"));
				sheet.addCell(new Label(8, 1, "标准单位单价"));
				sheet.addCell(new Label(9, 1, "标准单位金额"));
				sheet.addCell(new Label(10, 1, "参考单位"));
				sheet.addCell(new Label(11, 1, "参考毛用量"));
				sheet.addCell(new Label(12, 1, "取料率"));
	            //遍历list填充表格内容
				Spcodeexm spcodeexm = new Spcodeexm();
				spcodeexm.setItem(list.get(i).getItem());
				spcodeexm.setAcct("1");
				List<Product> spcodeexmList = prdctMapper.findmaterial(spcodeexm);
				int index = 1;
	            for(int j=0; j<spcodeexmList.size(); j++) {
	            	if(j == spcodeexmList.size()){
	            		break;
	            	}
	            	index += 1;
	            	sheet.addCell(new Label(0, index, spcodeexmList.get(j).getSupply().getSp_code()));
	            	sheet.addCell(new Label(1, index, spcodeexmList.get(j).getSupply().getSp_name()));
	            	sheet.addCell(new Label(2, index, spcodeexmList.get(j).getSupply().getSp_desc()));
	            	sheet.addCell(new Label(3, index, spcodeexmList.get(j).getSupply().getUnit2()));
	            	sheet.addCell(new Label(4, index, spcodeexmList.get(j).getExcnt().toString()));
	            	sheet.addCell(new Label(5, index, spcodeexmList.get(j).getCnt2().toString()));
	            	sheet.addCell(new Label(6, index, spcodeexmList.get(j).getSupply().getUnit()));
	            	sheet.addCell(new Label(7, index, spcodeexmList.get(j).getCnt().toString()));
	            	sheet.addCell(new Label(8, index, spcodeexmList.get(j).getSupply().getPricesale().toString()));
	            	sheet.addCell(new Label(9, index, spcodeexmList.get(j).getSale().toString()));
	            	sheet.addCell(new Label(10, index, spcodeexmList.get(j).getSupply().getUnit1()));
	            	sheet.addCell(new Label(11, index, spcodeexmList.get(j).getCnt1().toString()));
	            	sheet.addCell(new Label(12, index, spcodeexmList.get(j).getExrate()+""));
		    	}	            
			}
			workBook.write();
			os.flush();
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	/**
	 * 删除半成品原料实际使用明细
	 * @param mapMx
	 */
	public void deleteSpCodeExPriceCnt(Map<String, Object> map) {
		prdctMapper.deleteSpCodeExPriceCnt(map);
	}
	/**
	 * 新增半成品原料实际使用明细
	 * @param mapMx
	 */
	public void saveSpCodeExPriceCnt(Map<String, Object> map) {
		prdctMapper.saveSpCodeExPriceCnt(map);
	}
	
	/**
	 * 复制单个成本卡----实际物料
	 * @param modelMap
	 * @param item
	 * @param mods
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public void copyPrdct(String item, String ids, String acct) throws CRUDException{
		try {
			Spcodeexm spcodeexm1 = new Spcodeexm();
			spcodeexm1.setItem(item);
			spcodeexm1.setAcct(acct);
			List<Product> listProduct = prdctMapper.findmaterial(spcodeexm1);//获取当前选中类别下所有的bom
			String[] str = ids.split(",");
			for (int i = 0; i < str.length; i++) {
				int a=0;
				for (Product product:listProduct){
					product.setItem(str[i]);
					if (a==0){
						prdctMapper.deleteProductByItemId(product);
						a=1;
					}
					prdctMapper.saveProductByItem(product);//主表
				}				
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
