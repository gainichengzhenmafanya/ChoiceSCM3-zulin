package com.choice.scm.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountDeliver;
import com.choice.framework.domain.system.AccountRole;
import com.choice.framework.domain.system.Role;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountDeliverMapper;
import com.choice.framework.persistence.system.AccountRoleMapper;
import com.choice.framework.persistence.system.RoleMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.AcctMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.DisFenBoMapper;
import com.choice.scm.persistence.DisMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.util.MaterialScopeList;

@Service
public class DeliverService {

	@Autowired
	private AcctMapper acctMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private PageManager<Deliver> pageManager;
	@Autowired
	private PageManager<Dis> pageManager1;
	@Autowired
	private AccountDeliverMapper accountDeliverMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private AccountRoleMapper accountRoleMapper ;
	@Autowired
	private SupplyMapper supplyMapper;
	
	private final transient Log log = LogFactory.getLog(DeliverService.class);
	/**
	 * 保存供应商
	 * @param deliver
	 * @throws CRUDException
	 */
	public String saveDeliver(Deliver deliver,String accountId) throws CRUDException
	{
		String str="";
		try {
			if(findAllDelivers(deliver).size()!=0){
				str= "error";	
			}else{
				//添加该账号的供应商权限
				AccountDeliver accountDeliver = new AccountDeliver();
				accountDeliver.setId(CodeHelper.createUUID());
				accountDeliver.setAccountId(accountId);
				accountDeliver.setDeliverId(deliver.getCode());
				accountDeliverMapper.saveAccountDeliver(accountDeliver);
				//添加系统管理员角色账号对改物资的权限
				Role role = new Role();
				role.setName("系统管理员");
				role.setDeleteFlag("T");
				List<Role> listRole = roleMapper.findAllRole(role);
				if(listRole!=null && listRole.size()>0){//存在该角色
					String roleId ="";
					for(Role ro : listRole){
						if("系统管理员".equals(ro.getName())){
							roleId = ro.getId();
							break;
						}
					}
					if(roleId!=null && !"".equals(roleId)){
						//查询该角色下账号信息
						AccountRole accountRole = new AccountRole();
						accountRole.setRoleId(roleId);
						List<AccountRole> listAccountRole = accountRoleMapper.findAccountRole(accountRole);
						if(listAccountRole!=null && listAccountRole.size()>0){
							AccountDeliver admin = new AccountDeliver();
							admin.setDeliverId(deliver.getCode());
							for(AccountRole aRole:listAccountRole){
								admin.setAccountId(aRole.getAccountId());
								admin.setId(CodeHelper.createUUID());
								accountDeliverMapper.saveAccountDeliver(admin);
							}
						}
					}
				}
				deliver.setInit(deliver.getInit().toUpperCase());//缩写转为大写
				
				deliverMapper.saveDeliver(deliver);
				str="succ";
			}
			return str;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	public String saveDeliver(Deliver deliver) throws CRUDException
	{
		String str="";
		try {
			if(findAllDelivers(deliver).size()!=0){
				str= "error";	
			}else{
				deliver.setInit(deliver.getInit().toUpperCase());//缩写转为大写
				deliverMapper.saveDeliver(deliver);
				str="succ";
			}
			return str;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	} 
	
	/**
	 * 获取供应商中的最大编码+1返回
	 * @param code
	 * @return
	 */
	public String getMaxCode() throws CRUDException {
		try {
			return deliverMapper.getMaxCode();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询供应商信息
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findDeliver(Deliver deliver,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(deliver, page, DeliverMapper.class.getName()+".findDeliver");
			//return deliverMapper.findDeliverByKey(deliver);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询是否被引用----用于删除供应商时候的验证
	 * @param codes
	 * @param types
	 * @return
	 * @throws CRUDException
	 */
	public String deleteyh(String codes) throws CRUDException{
		try {
			int index = 0;//接收验证的返回值
			String deliverCode = "";//用于返回已经被引用的供应商
			String[] code = codes.split(",");
			for (int i = 0; i < code.length; i++) {
				Deliver deliver = new Deliver();
				deliver.setCode(code[i]);
				index = deliverMapper.deleteyh(deliver);
				Deliver del = new Deliver();
				del = deliverMapper.selectDeliverByCode(deliver);//根据编码获取供应商的名称
				if (index > 0) {
					deliverCode += del.getDes()+"\n";
				}
			}
			return deliverCode;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应商类型
	 * @param deliver
	 * @param page
	 * @return
	 * @throws CRUDException
	 * @author Administrator lishuai
	 */
	public List<Deliver> findDeliverType() throws CRUDException
	{
		try {
//			return pageManager.selectPage(map, page, DeliverMapper.class.getName()+".findDeliverType");
			return deliverMapper.findDeliverType();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应商信息
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findDeliverBy(String deliverDirect, String searchInfo, Page page) throws CRUDException
	{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("searchInfo", searchInfo);
			map.put("person", deliverDirect);
			return pageManager.selectPage(map, page, DeliverMapper.class.getName()+".findDeliverBy");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应商信息
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findDeliverBy(String deliverDirect, String searchInfo, Page page, String gysqx, String accountId) throws CRUDException
	{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("searchInfo", searchInfo);
			map.put("gysqx", gysqx);
			map.put("accountId", accountId);
			map.put("person", deliverDirect);
			return pageManager.selectPage(map, page, DeliverMapper.class.getName()+".findDeliverBy");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应商信息（无分页）
	 * @param key
	 * @param sech
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findAllDelivers(Deliver deliver) throws CRUDException
	{
		try {
			return deliverMapper.findDeliver(deliver);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应商信息（含分页）
	 * @param deliver
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findAllPageDelivers(Deliver deliver,Page page) throws CRUDException
	{
		try {
//			return deliverMapper.findDeliver(deliver);
			return pageManager.selectPage(deliver, page, DeliverMapper.class.getName()+".findDeliver");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据方向和分店查询供应商
	 */
	public List<Deliver> findDeliverByInout(String firm,String inout,Deliver deliver) throws CRUDException
	{
		Map<String,Object> map=new HashMap<String, Object>();
//		String inoutNum=null;
		try {
//			if(null!=inout && !"".equals(inout) && "直发".equals(inout)){
//				inoutNum="0";
//			}
//			if(null!=inout && !"".equals(inout) && "直配".equals(inout)){
//				inoutNum="1";
//			}
			map.put("firm", firm);
			map.put("inout", inout);
			map.put("deliver", deliver);
			if ("2".equals(inout)) {//现在改为出库的不能修改供应商  入库的可以改 这里2代表查入库供应商 默认查所有供应商2015.1.8wjf
				return deliverMapper.findDeliverByOut(map);
			}
			return deliverMapper.findDeliverByInout(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据Code 查询供应商
	 * @param deliver
	 * @return
	 * @throws CRUDException
	 */
	public Deliver findDeliverByCode(Deliver deliver) throws CRUDException
	{
		try {
			return deliverMapper.findDeliverByCode(deliver);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 报货分拨、验收，查询供应商，返回js对象
	 */
	public String findDeliverToJS(Deliver deliver) throws CRUDException
	{
		Deliver result=null;
		try {
			//List<Deliver> list= deliverMapper.findDeliver(deliver);
			//result=list.get(0);
			result=deliverMapper.findDeliverByCode(deliver);
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改信息
	 * @param deliver
	 * @throws CRUDException
	 */
	public void updateDeliver(HashMap<String, Object> deliverMap) throws CRUDException
	{
		try {
			deliverMapper.updateDeliver(deliverMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存修改信息====将修改的供应商名称同步到物资表中
	 * @param deliver
	 * @throws CRUDException
	 * @author wangchao
	 */
	public void updateSupply(HashMap<String, Object> deliverMap) throws CRUDException
	{
		try {
			deliverMapper.updateSupply(deliverMap);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改供应商对应仓位编码
	 * @param deliver
	 * @throws CRUDException
	 */
	public void saveDeliverPositn(Deliver deliver) throws CRUDException
	{
		try {
			deliverMapper.saveDeliverPositn(deliver);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除供应商信息
	 * @param codes
	 * @throws CRUDException
	 */
	public void deleteDeliver(List<String> codes) throws CRUDException
	{
		try {
			deliverMapper.deleteDeliver(codes);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 获取供应商类别（已使用）
	 * @return
	 * @throws CRUDException 
	 */
	public List<Deliver> findDeliverCategoryInUse() throws CRUDException{
		try {
			return deliverMapper.findDeliverCategoryInUse();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 打印供应商
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> printDeliver(Deliver deliver) throws CRUDException
	{
		try {
			return deliverMapper.findDeliver(deliver);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
//    供应商配送相关  
	/**
	 * 获取角色名
	 * @return
	 * @throws CRUDException
	 */
	public Role getRoleNm(Account account) throws CRUDException
	{
		try {
			return deliverMapper.getRoleNm(account);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 供应商查询   送哪些物资                    
	 */
	public List<Dis> findAllDis(Dis dis,Page page) throws CRUDException {
		try {
			//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
			Acct acct = acctMapper.findAcctById(dis.getAcct());
			if("Y".equals(acct.getYncgqr())){
				dis.setChk1("Y");
			}else{
				dis.setChk1(null);
			}
			if("Y".equals(acct.getYncgsh())){
				dis.setSta("Y");
			}else{
				dis.setSta(null);
			}
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			if(null!=inout && !"".equals(inout) && "in".equals(inout)){
				dis.setInout(inout);
			}else{
				if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			}
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
//			if(null!=dis.getInout() && !"".equals(dis.getInout())){dis.setInout(CodeHelper.replaceCode(dis.getInout()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=pageManager1.selectPage(dis, page, DisFenBoMapper.class.getName()+".findAllDis"); 				
			//list=disMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 供应商查询   送哪些物资                不分页
	 */
	public List<Dis> findAllDisPage(Dis dis) throws CRUDException {
		try {
			List<Dis> list=null;
			String firmCode=dis.getFirmCode();//记录原始值
			String inout=dis.getInout();
			
			if(null!=dis.getFirmCode() && !"".equals(dis.getFirmCode())){dis.setFirmCode(CodeHelper.replaceCode(dis.getFirmCode()));}
			if(null!=dis.getSp_init() && !"".equals(dis.getSp_init())){dis.setSp_init(dis.getSp_init().toUpperCase());}
			if("1".equals(dis.getYnArrival())){dis.setChksend("N");dis.setChk1("Y");}//确认到货
			if("2".equals(dis.getYnArrival())){dis.setChksend("Y");}//未知到货
			list=deliverMapper.findAllDis(dis);
			dis.setFirmCode(firmCode);//覆盖处理后的值
			dis.setInout(inout);
			return list;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 供应商，采购汇总
	 */
	public List<Dis> findCaiGouTotal(Dis dis,Page page) throws CRUDException {
		try {
			if (page==null) {
				return deliverMapper.findCaiGouTotal(dis);
			}else {
				return pageManager1.selectPage(dis, page, DisMapper.class.getName()+".findCaiGouTotal");
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/***
	 * 供应商确认送货  根据选中的
	 * @param ids
	 */
	public void confirmSh(String ids) {
		deliverMapper.confirmSh(ids);
	}

	/***
	 * 查询某供应商  还有多少个门店未确认送货
	 * @param dis
	 * @return
	 */
	public Integer findDeliverMessage1(Dis dis) {
		//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
		Acct acct = acctMapper.findAcctById(dis.getAcct());
		if("Y".equals(acct.getYncgqr())){
			dis.setChk1("Y");
		}else{
			dis.setChk1(null);
		}
		if("Y".equals(acct.getYncgsh())){
			dis.setSta("Y");
		}else{
			dis.setSta(null);
		}
		return deliverMapper.findDeliverMessage1(dis);
	}

	/***
	 * 查询某供应商   共有多少物资未送
	 * @param dis
	 * @return
	 */
	public Integer findDeliverMessage2(Dis dis) {
		//改为判断是不是非要采购确认 采购审核之后才能查询 wjf
		Acct acct = acctMapper.findAcctById(dis.getAcct());
		if("Y".equals(acct.getYncgqr())){
			dis.setChk1("Y");
		}else{
			dis.setChk1(null);
		}
		if("Y".equals(acct.getYncgsh())){
			dis.setSta("Y");
		}else{
			dis.setSta(null);
		}
		return deliverMapper.findDeliverMessage2(dis);
	}

	/***
	 * 确认某个供应商所有送货
	 * @param dis 
	 */
	public void confirmShAll(Dis dis) {
		deliverMapper.confirmShAll(dis);
	}
	
	/**
	 * 保存供应商物资到期提醒设置 wj
	 * @throws CRUDException
	 */
	public void saveWarmDays(String typCode,String warmDays)throws CRUDException{
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			List<String> codeList = Arrays.asList(typCode.split(","));
			map.put("codeList", codeList);
			map.put("warmDays", Integer.parseInt(warmDays));
			deliverMapper.saveWarmDays(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 跳转到添加物资界面
	 * @param materialScope
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialScope> tosaveDeliverSupply(MaterialScope materialScope) throws CRUDException{
		try {
			return deliverMapper.tosaveDeliverSupply(materialScope);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 分店物资属性添加物资
	 */
	public void saveSupplyByClass(List<Supply> listSupply, String delivercode) throws CRUDException {
		MaterialScope materialScope = new MaterialScope();
		materialScope.setDelivercode(delivercode);
		//删除全部
		deliverMapper.deleteDeliverSupply(materialScope);
		// 新增
		//判断为空
		if(listSupply==null||listSupply.size()==0){
			return;
		}
		for (Supply s:listSupply) {
			materialScope.setDelivercode(delivercode);
			materialScope.setPk_materialscope(CodeHelper.createUUID());
			materialScope.setPk_material(s.getSp_code());
			materialScope.setSp_code(s.getSp_code());
			materialScope.setSp_name(s.getSp_name());
			materialScope.setPk_typ(s.getGrptyp());
			materialScope.setUnit(s.getUnit());
			materialScope.setSp_desc(s.getSp_desc()==null?"":s.getSp_desc());
			materialScope.setSp_price(Double.parseDouble(s.getSp_price().toString()));
			materialScope.setTyp(s.getGrptyp());
			materialScope.setTypdes(s.getGrptypdes());
			materialScope.setAcct(s.getAcct());
			deliverMapper.insertDeliverSupply(materialScope);
		}
	}
	
	/**
	 * 添加供应物资范围
	 * @param delivercode
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public String insertDeliverSupply(String delivercode,List<String> code)throws CRUDException{
		try {
			
			List<Supply> listSupply = supplyMapper.findSupplyBySpcode(code);
			if (listSupply.size()>0) {
				for (Supply supply : listSupply) {
					MaterialScope materialScope = new MaterialScope();
					materialScope.setDelivercode(delivercode);
					materialScope.setPk_materialscope(CodeHelper.createUUID());
					materialScope.setPk_material(supply.getSp_code());
					materialScope.setSp_code(supply.getSp_code());
					materialScope.setSp_name(supply.getSp_name());
					materialScope.setPk_typ(supply.getGrptyp());
					materialScope.setUnit(supply.getUnit());
					materialScope.setSp_desc(supply.getSp_desc()==null?"":supply.getSp_desc());
					materialScope.setSp_price(Double.parseDouble(supply.getSp_price().toString()));
					materialScope.setTyp(supply.getGrptyp());
					materialScope.setTypdes(supply.getGrptypdes());
					materialScope.setAcct(supply.getAcct());
					deliverMapper.deleteDeliverSupply(materialScope);
					deliverMapper.insertDeliverSupply(materialScope);
				}
			}
			return "OK";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除物资
	 * @param materialScope
	 * @throws CRUDException
	 */
	public void deleteDeliverSupply(MaterialScope materialScope)throws CRUDException{
		try {
			deliverMapper.deleteDeliverSupply(materialScope);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新单价
	 * @param materialScopeList
	 * @return
	 * @throws CRUDException
	 */
	public boolean updateDeliverSupply(MaterialScopeList materialScopeList)throws CRUDException{
		try {
			for (int i = 0; i < materialScopeList.getListMaterialScope().size(); i++) {
				MaterialScope materialScope = materialScopeList.getListMaterialScope().get(i);
				deliverMapper.updateDeliverSupply(materialScope);
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新税率 jinshuai 20160425
	 * @param materialScopeList
	 * @return
	 * @throws CRUDException
	 */
	public boolean updateTaxRate(MaterialScopeList materialScopeList)throws CRUDException{
		//无数据 则返回
		if(materialScopeList==null||materialScopeList.getListMaterialScope()==null||materialScopeList.getListMaterialScope().size()==0){
			return true;
		}
		try {
			for (int i = 0; i < materialScopeList.getListMaterialScope().size(); i++) {
				MaterialScope materialScope = materialScopeList.getListMaterialScope().get(i);
				deliverMapper.updateTaxRate(materialScope);
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加关联商城供应商
	 * @param deliver
	 * @param session
	 * @return
	 * @throws Exception
	 */
    public int updateJmuDeliver(Deliver deliver,HttpSession session)throws CRUDException {
    	try {
    		deliverMapper.updateJmuDeliver(deliver);
    		return 1;
		} catch (Exception e) {
			log.error(e);
    		throw new CRUDException(e);
		}
    }
	
	/**
	 * 删除关联商城供应商信息
	 * @param codes
	 * @throws CRUDException
	 */
	public int deleteJumDeliver(List<String> codes) throws CRUDException {
		try {
			deliverMapper.deleteJumDeliver(codes);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
    
    /**
     * 添加关联商城物资
     * @param materialScope
     * @param session
     * @return
     * @throws Exception
     */
    public int updateJmuSupply(MaterialScope materialScope,HttpSession session)throws CRUDException {
    	try {
			deliverMapper.updateJmuSupply(materialScope);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
    }
	
	/**
	 * 删除供应商信息
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	public int deleteJumSupply(MaterialScope materialScope)throws CRUDException {
		try {
			deliverMapper.deleteJumSupply(materialScope);
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询供应物资范围的前十条供应商，包括税率，报价
	 * @return
	 * @throws CRUDException 
	 */
	public List<MaterialScope> findGySpGysTop10(MaterialScope materialScope) throws CRUDException{
		try {
			return deliverMapper.findGySpGysTop10(materialScope);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
