package com.choice.scm.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.scm.constants.ChkinmZbConstants;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Chkoutd;
import com.choice.scm.domain.Chkoutm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spbatch;
import com.choice.scm.domain.Supply;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.ChkindMapper;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.ChkoutdMapper;
import com.choice.scm.persistence.ChkoutmMapper;
import com.choice.scm.persistence.CodeDesMapper;
import com.choice.scm.persistence.DeliverMapper;
import com.choice.scm.persistence.DisChuKuMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SppriceSaleQuickMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.util.CalChkNum;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.PublicExportExcel;

@Service
public class ChkoutService {

	@Autowired
	private ChkoutdMapper chkoutdMapper;
	@Autowired
	private ChkoutmMapper chkoutmMapper;
	@Autowired
	private ChkindMapper chkindMapper;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private DisChuKuMapper disMapper;
	@Autowired
	private PageManager<Chkoutm> pageManager;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	@Autowired
	private DeliverMapper deliverMapper;
	@Autowired
	private CodeDesMapper codeDesMapper;
	@Autowired
	private SppriceSaleQuickMapper SppriceSaleQuickMapper;
	private final transient Log log = LogFactory.getLog(ChkoutService.class);
	
	/**
	 * 分店MIS验收查询
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkoutd> findChkoutFromMis(Map<String,Object> map) throws CRUDException{
		try{
			Supply supply=(Supply) map.get("supply");
			if(null!=supply.getGrp() && !"".equals(supply.getGrp())){
				List<String> grpList = Arrays.asList(supply.getGrp().split(","));
				map.put("grpList", grpList);
			}
			log.warn("分店验收查询：\n"+ 
					"grp:" + map.get("grpList")+
					",deliver:" + map.get("deliver")+
					",sp_code:" + map.get("sp_code")+
					",sp_init:" + map.get("sp_init")
					);
			return chkoutdMapper.findChkoutFromMis(map);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}

	/**
	 * 模糊查询 出库单
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkoutm> findChkoutm(Chkoutm chkoutm,Date bdat,Date edat,Page page,String locale) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			if(null == bdat){
				bdat = new Date();
			}
			if(null == edat){
				edat = new Date();
			}
			map.put("bdat", bdat);
			map.put("locale", locale);
			map.put("edat", edat);
			chkoutm.setChkoutnos(chkoutm.getChkoutno()==null?"":chkoutm.getChkoutno()+"");
			map.put("chkoutm", chkoutm);
			log.warn("出库单模糊查询：\n"+
					"bdat:"+DateFormat.getStringByDate(bdat, "yyyy-MM-dd")+
					",edat:"+DateFormat.getStringByDate(edat, "yyyy-MM-dd")+
					",chkoutm.checkby:"+chkoutm.getChecby()+
					",chkoutm.firm:"+chkoutm.getChecby()+
					",chkoutm.positn:"+chkoutm.getChecby()+
					",chkoutm.chkoutno:"+chkoutm.getChecby()+
					",chkoutm.vouno:"+chkoutm.getChecby()
					);
			return pageManager.selectPage(map, page, ChkoutmMapper.class.getName()+".findChkoutm");
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据id获取出库单信息
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public Chkoutm findChkoutmById(Chkoutm chkoutm) throws CRUDException{
		try{
			log.warn("获取出库单："+chkoutm.getChkoutno());
			return chkoutmMapper.findChkoutmById(chkoutm);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/*
	 * 获取未核减账单
	 */
	public List<Map<String, Object>> findFolioByNoChkoutm(String pk_store, String pk_folio, String dworkdate){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> folio = new HashMap<String, Object>();
		folio.put("pk_store", pk_store);
		folio.put("pk_folio", pk_folio);
		folio.put("dworkdate", dworkdate);
		
		list = chkoutmMapper.findFolioByNoChkoutm(folio);
		
		return list;
	}
	/**
	 * 更新账单表是否已核减状态
	 * 
	 * @param vbcode
	 * @throws CRUDException
	 */
	public void updateFolioIsChked(String vbcode) throws CRUDException {
		chkoutmMapper.updateFolioIsChked(vbcode);
	}
	
	/*
	 * 获取未核减账单明细
	 */
	public List<Map<String, Object>> findOrdrsByNoChkoutm(String vbcode){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("vbcode", vbcode);
		
		list = chkoutmMapper.findOrdrsByNoChkoutm(map);
		
		return list;
	}
	
	/**
	 * 新增出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String saveChkoutm(Chkoutm chkoutm) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			result.put("chkoutno", String.valueOf(chkoutm.getChkoutno()));
//			call add_chkoutm(#{acct},#{yearr},#{chkoutno},#{vouno},#{maded},#{madet},#{positn.code},
//			#{firm.code},#{madeby},#{totalamt},#{typ},#{memo},#{pr,jdbcType=INTEGER,mode=OUT})
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("acct", chkoutm.getAcct());
			map.put("yearr", mainInfoMapper.findYearrList().get(0)+"");
			map.put("chkoutno", chkoutm.getChkoutno());
			map.put("vouno", calChkNum.getNext(CalChkNum.CHKOUT, chkoutm.getMaded()));
			map.put("maded", chkoutm.getMaded());
			map.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			map.put("positn", chkoutm.getPositn().getCode());
			map.put("firm", chkoutm.getFirm().getCode());
			map.put("madeby", chkoutm.getMadeby());
			//修改出库单没有总价  wjf
			double totalAmt = 0;
			if(chkoutm.getChkoutd().size() != 0){
				for(Chkoutd chkoutd : chkoutm.getChkoutd()){
					double amount=chkoutd.getAmount();
					double price=chkoutd.getPrice();
					totalAmt+=amount*price;
				}
			}
			chkoutm.setTotalamt(totalAmt);
			map.put("totalamt", chkoutm.getTotalamt());
			map.put("typ", chkoutm.getTyp());
			map.put("memo", chkoutm.getMemo());
			map.put("pr", 0);
			log.warn("新增出库单(主单):\n"+
					"yearr:" + map.get("yearr")+
					",chkoutno:" + map.get("chkoutno")+
					",vouno:" + map.get("vouno")+
					",maded:" + map.get("maded")+
					",positn:" + map.get("positn")+
					",firm:" + map.get("firm")+
					",madeby:" + map.get("madeby")+
					",totalamt:" + map.get("totalamt")+
					",typ:" + map.get("typ")+
					",memo:" + map.get("memo")
					);
			chkoutmMapper.saveChkoutm(map);
			result.put("pr", map.get("pr").toString());
			if(! (Integer.parseInt(map.get("pr").toString()) == 1)){
				result.put("pr", "-1");
				throw new Exception("出库单"+chkoutm.getVouno()+"添加失败");
			}

			log.warn("新增出库单(详单):");
			if(chkoutm.getChkoutd() != null)
				for(Chkoutd chkoutd : chkoutm.getChkoutd()){
	//			call add_chkoutd(#{chkoutno},#{sp_code.sp_code},#{amount},#{price},#{memo},#{stoid},
	//				#{batchid},#{deliver.code},#{amount1},#{pricesale},#{pr,mode=OUT,jdbcType=INTEGER})
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutm.getChkoutno());///
					m.put("sp_code", chkoutd.getSp_code().getSp_code());
					m.put("amount", chkoutd.getAmount());
					m.put("price", chkoutd.getPrice());
					m.put("memo", chkoutd.getMemo());
					m.put("stoid", chkoutd.getStoid());
					m.put("batchid", chkoutd.getBatchno());
					if(null != chkoutd.getDeliver())
						m.put("deliver", chkoutd.getDeliver().getCode());
					m.put("amount1", chkoutd.getAmount1());
					m.put("pricesale", chkoutd.getPricesale());
					m.put("chkstono", chkoutd.getChkstono());//wjf
					m.put("pr", 0);
					log.warn("sp_code:" + m.get("sp_code")+
							",amount:" + m.get("amount")+
							",price:" + m.get("price")+
							",memo:" + m.get("memo")+
							",stoid:" + m.get("stoid")+
							",batchid:" + m.get("batchid")+
							",deliver:" + m.get("deliver")+
							",amount1:" + m.get("amount1")+
							",pricesale:" + m.get("pricesale")
							);
					chkoutdMapper.saveChkoutd(m);
					
					//int aasdf=10/0;

					result.put("pr", map.get("pr").toString());
					switch(Integer.parseInt(m.get("pr").toString())){
					case -1:
						result.put("pr", "-1");
						throw new Exception("出库单"+chkoutm.getVouno()+"添加失败"+chkoutd.getSp_code().getSp_code());
					case -2:
						result.put("pr", "-2");
						throw new Exception("出库单"+chkoutm.getVouno()+"添加失败"+chkoutd.getSp_code().getSp_code());
					}
				}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String updateChkoutm(Chkoutm chkoutm) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			//检测要保存的出库单据是否被审核    //  去掉凭证号
			Chkoutm  chkoutmchke=new Chkoutm();
			chkoutmchke.setChkoutno(chkoutm.getChkoutno());
			Chkoutm chkoutChec = chkoutmMapper.findChkoutmById(chkoutmchke);
			if(null != chkoutChec.getChecby() && !"".equals(chkoutChec.getChecby())){
				result.put("pr","-2");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}
			result.put("chkoutno", chkoutm.getChkoutno().toString());
			Map<String,Object> chkout = new HashMap<String,Object>();
			chkout.put("chkoutno", chkoutm.getChkoutno().toString());
			chkout.put("vouno", chkoutm.getVouno());
			chkout.put("maded", chkoutm.getMaded());
			chkout.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			chkout.put("positn", chkoutm.getPositn().getCode());
			chkout.put("madeby", chkoutm.getMadeby());
			chkout.put("firm", chkoutm.getFirm().getCode());
			chkout.put("totalamt", chkoutm.getTotalamt());
			chkout.put("typ", chkoutm.getTyp());
			chkout.put("memo", chkoutm.getMemo());
			chkout.put("pr", 0);
			log.warn("更新出库单(主单):\n"+
					"chkoutno:" + chkout.get("chkoutno")+
					",vouno:" + chkout.get("vouno")+
					",maded:" + chkout.get("maded")+
					",madet:" + chkout.get("madet")+
					",positn:" + chkout.get("positn")+
					",madeby:" + chkout.get("madeby")+
					",firm:" + chkout.get("firm")+
					",totalamt:" + chkout.get("totalamt")+
					",typ:" + chkout.get("typ")+
					",memo:" + chkout.get("memo")
					);
			chkoutmMapper.updateChkoutm(chkout);
			result.put("pr", chkout.get("pr").toString());
			if(!(Integer.parseInt(chkout.get("pr").toString()) == 1)) throw new Exception("出库单修改失败");
			log.warn("更新出库单(详单):");
			for(Chkoutd chkoutd : chkoutm.getChkoutd()){
				Map<String,Object> m = new HashMap<String,Object>();
				m.put("chkoutno", chkoutm.getChkoutno());
				m.put("sp_code", chkoutd.getSp_code().getSp_code());
				m.put("amount", chkoutd.getAmount());
				m.put("price", chkoutd.getPrice());
				m.put("memo", chkoutd.getMemo());
				m.put("stoid", chkoutd.getStoid());
				m.put("batchid", chkoutd.getBatchno());
				m.put("deliver", chkoutd.getDeliver());
				m.put("amount1", chkoutd.getAmount1());
				m.put("pricesale", chkoutd.getPricesale());
				m.put("pr", 0);
				log.warn("sp_code:" + m.get("sp_code")+
						",amount:" + m.get("amount")+
						",price:" + m.get("price")+
						",memo:" + m.get("memo")+
						",stoid:" + m.get("stoid")+
						",batchid:" + m.get("batchid")+
						",deliver:" + m.get("deliver")+
						",amount1:" + m.get("amount1")+
						",pricesale:" + m.get("pricesale")
						);
				chkoutdMapper.saveChkoutd(m);
				switch(Integer.parseInt(m.get("pr").toString())){
				case -1:
					result.put("pr", chkout.get("pr").toString());
					throw new Exception("出库单"+chkoutm.getVouno()+"更新失败"+chkoutd.getSp_code().getSp_code());
				case -2:
					result.put("pr", chkout.get("pr").toString());
					throw new Exception("出库单"+chkoutm.getVouno()+"已审核，更新失败"+chkoutd.getSp_code().getSp_code());
				}
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String deleteChkoutm(Chkoutm chkoutm) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			Map<String,Object> chkout = new HashMap<String,Object>();
			String[] no = chkoutm.getVouno().split(",");
			log.warn("删除出库单:\n"+chkoutm.getVouno());
			for(String n : no){
				chkout.put("chkoutno",n);
				chkout.put("pr", 0);
				chkoutmMapper.deleteChkoutm(chkout);
				if(!(Integer.parseInt(chkout.get("pr").toString()) == 1)) throw new Exception("单据已审核，不能删除！");
				result.put("pr", chkout.get("pr").toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 审核出库单信息
	 * @param chkoutm
	 * @throws CRUDException
	 */
	public String updateByAudit(Chkoutm chkoutm, String checby) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			Map<String,Object> map = new HashMap<String,Object>();
			
			//jinshuai at 20160411
			//出库单增加或者修改的时候，读取配置文件，是否可以修改售价，如果可以修改售价
			String chkoutCanUpdatesppricesale = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "chkout_can_updatesppricesale");
			boolean isTrue = (chkoutCanUpdatesppricesale!=null&&chkoutCanUpdatesppricesale.trim().equalsIgnoreCase("Y"))?true:false;
			
			//使用vouno接收chkoutno参数
			if (null==chkoutm.getChkoutnos()) {
				ReadProperties rp = new ReadProperties();
				chkoutm = chkoutmMapper.findChkoutmById(chkoutm);
				chkoutm.setChecby(checby);
				map.put("chkoutno", chkoutm.getChkoutno());
				map.put("month",  acctService.getOnlyAccountMonth(chkoutm.getMaded())+"");
				map.put("emp", checby);
				map.put("sta", "Y");
				map.put("chk1memo", chkoutm.getChk1memo());//审核备注  wjf
				if(chkoutm.getFirm().getCode().equals(CalChkNum.profitDeliver) && "1203".equals(chkoutm.getPositn().getTyp())){//如果使用仓位是盘亏，并且出库仓位是分店类型 ，则sta=P,勿动！2014.12.20wjf
					//门店是先进先出，还是加权平均   勿动！2015.07.16  xlh
					String   sta=ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"firmOutWay");
					if("Y".equals(sta)){					
						map.put("sta", sta);//先进先出
					}else{
						map.put("sta", "P");
					}
				}
				map.put("pr", 0);
				map.put("errdes", "");
				log.warn("\tchkoutno:"+chkoutm.getChkoutno()+"\n"+
						"month:"+map.get("month")+
						",emp:"+map.get("emp")+
						",sta:"+map.get("sta")
						);
				chkoutmMapper.AuditChkout(map);
				chkoutmMapper.updateChkoutAmount(chkoutm);
				
				//update by jinshuai at 20160411
				//如果配置了，把售价更新到售价表
				if(isTrue){
					Map<String,Object> pmap = null;
					String firmCode = chkoutm.getFirm().getCode();
					String acct = chkoutm.getAcct();
					List<Chkoutd> list = chkoutm.getChkoutd();
					if(list!=null){
						for(int j=0,len=list.size();j<len;j++){
							Chkoutd ckd = list.get(j);
							if(ckd!=null){
								String spCode = ckd.getSp_code().getSp_code();
								double priceSale = ckd.getPricesale();
								pmap = new HashMap<String,Object>();
								pmap.put("area", firmCode);
								pmap.put("acct", acct);
								pmap.put("sp_code", spCode);
								pmap.put("price", priceSale);
								//更新物资对分店的最新审核通过的售价
								SppriceSaleQuickMapper.updateNewCheckedSppriceSale(pmap);
							}
						}
					}
				}
				
				result.put("pr", map.get("pr").toString());
				result.put("msg", map.get("errdes") != null ? map.get("errdes").toString() : "");
				switch(Integer.parseInt(map.get("pr").toString())){
					case 0:break;
					case 1:break;
					case -1:break;
					case -3:break;
					case -4:break;
					case -5:break;
					default:
						throw new CRUDException(map.get("errdes").toString());
				}
			}else{
				String[] no = chkoutm.getChkoutnos().split(",");
				log.warn("审核出库单:\n"+chkoutm.getVouno());
				ReadProperties rp = new ReadProperties();
				for(String n : no){
					Chkoutm chkoutm1=new Chkoutm();//原先好大的bug    循环里居然用  一个对象  到底  xlh  2014.11.21
					chkoutm1.setChkoutno(Integer.valueOf(n));
					chkoutm1 = chkoutmMapper.findChkoutmById(chkoutm1);
					map.put("chkoutno", n);
					map.put("month",  acctService.getOnlyAccountMonth(chkoutm1.getMaded())+"");
					map.put("emp", checby);
					map.put("sta", "Y");
					
					if(chkoutm1.getFirm().getCode().equals(CalChkNum.profitDeliver) && "1203".equals(chkoutm1.getPositn().getTyp())){//如果使用仓位是盘亏，并且出库仓位是分店类型 ，则sta=P,勿动！2014.12.20wjf
						//门店是先进先出，还是加权平均   勿动！2015.07.16  xlh
						String   sta=ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SBOH,"firmOutWay");
						if("Y".equals(sta)){					
							map.put("sta", sta);//先进先出
						}else{
							map.put("sta", "P");
						}
					}
					map.put("pr", 0);
					map.put("errdes", "");
					log.warn("\tchkoutno:"+n+"\n"+
							"month:"+map.get("month")+
							",emp:"+map.get("emp")+
							",sta:"+map.get("sta")
							);
					chkoutmMapper.AuditChkout(map);
					chkoutmMapper.updateChkoutAmount(chkoutm1);
					
					//update by jinshuai at 20160411
					//如果配置了，把售价更新到售价表
					if(isTrue){
						Map<String,Object> pmap = null;
						String firmCode = chkoutm1.getFirm().getCode();
						String acct = chkoutm1.getAcct();
						List<Chkoutd> list = chkoutm1.getChkoutd();
						if(list!=null){
							for(int j=0,len=list.size();j<len;j++){
								Chkoutd ckd = list.get(j);
								if(ckd!=null){
									String spCode = ckd.getSp_code().getSp_code();
									double priceSale = ckd.getPricesale();
									pmap = new HashMap<String,Object>();
									pmap.put("area", firmCode);
									pmap.put("acct", acct);
									pmap.put("sp_code", spCode);
									pmap.put("price", priceSale);
									//更新物资对分店的最新审核通过的售价
									SppriceSaleQuickMapper.updateNewCheckedSppriceSale(pmap);
								}
							}
						}
					}
					
					String pr =map.get("pr") != null ? map.get("pr").toString() : "";
					String errdes =map.get("errdes") != null ? map.get("errdes").toString() : "";
					result.put("pr", pr);
					result.put("msg", errdes);
					
					System.out.println(pr+":::::::::::"+errdes);
					switch(Integer.parseInt(map.get("pr").toString())){
					case 0:break;
					case 1:break;
					case -1:break;
					case -3:break;
					case -4:break;
					case -5:break;
					default:
						throw new CRUDException(map.get("errdes").toString());
					}
				}
			}
			result.put("checby", checby);//审核人
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取下一单号
	 * @return
	 * @throws CRUDException
	 */
	public BigDecimal findNextNo() throws CRUDException{
		try{
			return BigDecimal.valueOf(chkoutmMapper.findNextNo());
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 验收出库方法
	 * @throws CRUDException
	 */
	public String saveYsckChkout(List<Dis>  listDistribution,String acct,String accountName,String inoutSta,Date maded) throws CRUDException {
		String str=null;
		try {
//			String positnCode=listDistribution.get(0).getPositnCode();//出库仓位
//			String firmCode=listDistribution.get(0).getFirmCode();//入库仓位
//			ArrayList<Dis>  list=new ArrayList<Dis>();//临时list
			Set<String> set = new HashSet<String>();
			for (int i = 0; i < listDistribution.size(); i++) {//循环数据
				set.add(listDistribution.get(i).getPositnCode()+","+listDistribution.get(i).getFirmCode());
			}
			
			for(String code:set){
				String pcode = code.split(",")[0];
				String fcode = code.split(",")[1];
				List<Dis> listDis = new ArrayList<Dis>();
				for (int j = 0; j < listDistribution.size(); j++) {
					if(listDistribution.get(j).getPositnCode().equals(pcode) 
							&& listDistribution.get(j).getFirmCode().equals(fcode)){
						listDis.add(listDistribution.get(j));
					}
				}
				
				str=saveChkoutmByChkstom(listDis,acct,maded,accountName,inoutSta,pcode,fcode);
			}
			
			//最后处理  list大小不为0  时候  在作为一条出库单据加入
//			if(list.size()!=0){
//				str=saveChkoutmByChkstom(list,acct,maded,accountName,inoutSta,positnCode,firmCode);
//			}
			
			if(null==str || ""==str){
				//修改       验收出库成功的标志位
				HashMap<String,Object>  newmap=new HashMap<String, Object>();
				newmap.put("chkoutSta", "chkoutSta");//标志改
				newmap.put("chkout", "Y");//标志改
				newmap.put("acct", acct);
				newmap.put("listDis", listDistribution);//传入要修改的list 主要取ListId
				for (int i = 0; i < listDistribution.size(); i++) {//循环数据
					Dis dis=listDistribution.get(i);
					dis.setChkout("Y");
					dis.setId(CodeHelper.replaceCode(dis.getId()));
					disMapper.updateByIds(dis);
				}
//				distributionMapper.updateChkstodByList(newmap);//批量修改为                   已出库
			}
			return str;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	/**
	 * 验收出库
	 * @return
	 * @throws CRUDException
	 */
	public String saveChkoutmByChkstom(List<Dis>  listDis,String acct,Date maded,String accountName,String inoutSta,String positnCode,String firmCode) throws CRUDException {
		try {			
			Chkoutm chkoutm = new Chkoutm();
			if("inout".equals(inoutSta)){
				chkoutm.setTyp("9908");//正常出库
				chkoutm.setMemo("申购入库后直发出库");
			}else{
				chkoutm.setTyp("9908");//正常出库
				chkoutm.setMemo("申购后仓库出库");
			}
			log.warn("验收出库:\n"+
					"typ:"+chkoutm.getTyp()+
					"memo:"+chkoutm.getMemo()
					);
			String vouno=calChkNum.getNext(CalChkNum.CHKOUT,maded);
			int chkoutno=chkoutmMapper.findNextNo();
			chkoutm.setChkoutno(chkoutno);
			chkoutm.setVouno(vouno);
			chkoutm.setAcct(acct);
			chkoutm.setMaded(maded);
			Positn positn2=new Positn();
			Positn firm2=new Positn();
			positn2.setCode(positnCode);
			firm2.setCode(firmCode);
			chkoutm.setPositn(positn2);
			chkoutm.setFirm(firm2);
			chkoutm.setMadeby(accountName);
			chkoutm.setTotalamt(listDis.get(0).getAmountin()*listDis.get(0).getPricesale());			

			ArrayList<Chkoutd> chkoutdList=new ArrayList<Chkoutd>();
			for (int j = 0; j < listDis.size(); j++) {
				Dis dis=(Dis)listDis.get(j);
				Chkoutd  chkoutd=new Chkoutd();//出库单从表
				Supply supply=new Supply();
				supply.setSp_code(dis.getSp_code());
				Deliver deliver=new Deliver();
				deliver.setCode(dis.getDeliverCode());
				chkoutd.setSp_code(supply);
				chkoutd.setAmount(dis.getAmountin());
//				chkoutd.setPrice(dis.getPricein());         2014.7.28修改这里很关键 
				chkoutd.setPrice(dis.getPricesale());
				chkoutd.setMemo(dis.getMemo());
				chkoutd.setStoid(BigDecimal.valueOf(Integer.parseInt(dis.getId())));
				chkoutd.setChkstono(new BigDecimal(dis.getChkstoNo()));//wjf
				chkoutd.setDeliver(deliver);
				chkoutd.setAmount1(dis.getAmount1in());//新报货流程需要  老报货流程也不影响  2014.12.31wjf
//				chkoutd.setPricesale(dis.getPrice());         2014.7.28修改这里很关键 
				chkoutd.setPricesale(dis.getPricesale());
				chkoutdList.add(chkoutd);
			}
			chkoutm.setChkoutd(chkoutdList);
			saveChkoutm(chkoutm);//保存出库单		
			chkoutm.setChecby(accountName);//加入审核人
			return checkChkoutm2(chkoutm,inoutSta);//同时审核出库单
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	/**
	 * 生成出库单同时审核
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public String checkChkoutm2(Chkoutm chkoutm,String inoutSta) throws CRUDException{		
		try{
			String chkMsg="1";
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("acct", "1");
			map.put("chkoutno", chkoutm.getChkoutno());
		//	map.put("month", new Date().getMonth()+1);
			map.put("month", acctService.getOnlyAccountMonth(chkoutm.getMaded())+"");
			map.put("emp", chkoutm.getChecby());
			if("inout".equals(inoutSta)){
				map.put("sta", "N");
			}else{
				map.put("sta", "Y");
			}
			chkoutmMapper.AuditChkout(map);
			if(null!=map.get("errdes")){
				chkMsg=map.get("errdes").toString();
			}else{//审核成功
				//1.扣款判断
				Acct acct = acctService.findAcctById(map.get("acct").toString());
				if("香他她".equals(acct.getDes())){
					String vcode = positnMapper.findFirmtypByScode(chkoutm.getFirm().getCode());
					if("2".equals(vcode)){//加盟商
						Chkoutm c = chkoutmMapper.findChkoutmById(chkoutm);
						Map<String,Object> m = positnMapper.checkSppriceSale(chkoutm.getFirm().getCode());
						if(null != m && null != m.get("ISDEDUCT") && null != m.get("MONEY")){
							int iddeduct = Integer.valueOf(m.get("ISDEDUCT").toString());
							if(iddeduct == 1){//需要申购扣款
								double amt = Double.valueOf(m.get("MONEY").toString());
								if(amt-c.getTotalamt()<0){
									throw new CRUDException("报货门店："+c.getFirm().getDes()+"所属加盟商预付款金额不足，不能出库！");
								}
								positnMapper.joiningdeduction(c);
								if(1 != c.getPr()){
									throw new CRUDException("加盟商扣款失败，请联系专业技术人员！");
								}
							}
						}
					}
				}
			}
			return chkMsg;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
	/**
	 * 检测要保存、删除的出库单据是否被审核
	 * @param active
	 * @param chkoutm
	 * @return
	 * @throws CRUDException
	 */
	public String chkChect(String active, Chkoutm chkoutm) throws CRUDException {
		try {
			String showM = "YES",action = ""; 
			if("edit".equals(active)){
				action = "修改保存";
			}else if("delete".equals(active)){
				action = "删除";
			}
			chkoutm.setVouno("");
			Chkoutm listChecked =chkoutmMapper.findChkoutmById(chkoutm);
			if(null == listChecked){
				showM = "单号："+chkoutm.getChkoutno()+"的单据已被删除，不能进行"+action+"操作。"; 
			}else if(null != listChecked.getChecby() && !"".equals(listChecked.getChecby())){
				showM = "凭证号："+listChecked.getVouno()+"的单据已被审核，不能进行"+action+"操作。";
			}
			return showM;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取出库数据
	 * @param listId
	 * @throws CRUDException
	 */
	public List<Spbatch> findChkoutByCx(Spbatch spbatch) throws CRUDException {
		try {
			
			Positn positn1=new  Positn();
			positn1.setCode(spbatch.getPositn());
			Positn positn= positnMapper.findPositnByCode(positn1);
			if("1203".equals(positn.getPcode())){// 分店   冲销   spbatch_x
				return chkoutmMapper.findChkoutByCx_X(spbatch);
			}else{// 其它   冲销   spbatch
				return chkoutmMapper.findChkoutByCx(spbatch);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取批次出库数据
	 * @param listId
	 * @throws CRUDException
	 */
	public List<Spbatch> findChkoutByPc(Spbatch spbatch) throws CRUDException {
		try {
			return chkoutmMapper.findChkoutByPc(spbatch);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response, HttpServletRequest request) throws IOException {
		PublicExportExcel.downloadTemplate(response, request, "出库单.xls"); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return PublicExportExcel.uploadToTemp(request, response); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 对execl进行验证
	 */
	public Object check(String path,String accountId) throws CRUDException {
		boolean checkResult = ReadChkoutExcel.check(accountId,path, supplyMapper,positnMapper,deliverMapper,codeDesMapper);
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			Chkoutm chkoutm = ReadChkoutExcel.chkoutm;
			chkoutm.setAcct(accountId);
			List<Chkoutd> chkoutdList = ReadChkoutExcel.chkoutdList;
			chkoutm.setChkoutd(chkoutdList);
			return chkoutm;
		} else {
			errorList = ReadChkoutExcel.errorList;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}

	/***
	 * 冲消+报损处理
	 * @author wjf
	 * @param disList  要处理的数据
	 * @param acct  	帐套
	 * @param acctName  操作人
	 * @param supplyAcct
	 * @param ids		要处理的supplyacct的id
	 * @return
	 */
	public String dealDiff(String type, List<SupplyAcct> disList,String acct, String acctName, SupplyAcct supplyAcct,String ids) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try{
			if(!"3".equals(type)){
				//1.先填一个冲消单
				Integer chkoutno = chkoutmMapper.findNextNo();//单号
				result.put("chkoutno", String.valueOf(chkoutno));
				Date maded = DateFormat.formatDate(new Date(), "yyyy-MM-dd");//制单日期 今天
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("acct", acct);
				map.put("yearr", mainInfoMapper.findYearrList().get(0)+"");
				map.put("chkoutno", chkoutno);
				map.put("vouno", calChkNum.getNext(CalChkNum.CHKOUT, maded));
				map.put("maded", maded);
				map.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				map.put("positn", supplyAcct.getPositn());
				map.put("firm", supplyAcct.getFirm());
				map.put("madeby", acctName);
				map.put("totalamt", 0);//设为0  审核时会赋值
				map.put("typ", "9914");//出库冲消
				map.put("memo", "验货不符冲消");
				map.put("pr", 0);
				log.warn("新增出库单(主单):\n"+
						"yearr:" + map.get("yearr")+
						",chkoutno:" + map.get("chkoutno")+
						",vouno:" + map.get("vouno")+
						",maded:" + map.get("maded")+
						",positn:" + map.get("positn")+
						",firm:" + map.get("firm")+
						",madeby:" + map.get("madeby")+
						",totalamt:" + map.get("totalamt")+
						",typ:" + map.get("typ")+
						",memo:" + map.get("memo")
						);
				chkoutmMapper.saveChkoutm(map);
				
				result.put("pr", map.get("pr").toString());
				if(Integer.parseInt(map.get("pr").toString()) != 1){//有为就抛异常  回滚 返回
					result.put("pr", "-1");
					result.put("msg", "出库单"+chkoutno+"添加失败");
					throw new CRUDException(JSONObject.fromObject(result).toString());
				}
				//2.冲销单从表  需要回滚
				for(SupplyAcct sa :disList){
					//0.查找当时的spbatch id号    给从表的存储过程
					Spbatch spbatch = new Spbatch();
					spbatch.setAcct(acct);
					spbatch.setChkno(sa.getChkno());
					spbatch.setPositn(supplyAcct.getFirm());
					spbatch.setSp_code(sa.getSp_code());
					spbatch.setAmount(sa.getCntout()-sa.getCntfirm());//这里差异应该是正的
					spbatch.setPrice(sa.getPricesale() == 0?sa.getPriceout():sa.getPricesale());
					int batchid = 0;//先进先出表id,默认为0
					if("1203".equals(sa.getFirmtyp())){// 分店   冲销   spbatch_x
						List<Spbatch> spbatchs = chkoutmMapper.findChkoutByCx_X(spbatch);
						if(spbatchs.size() != 0){
							batchid = spbatchs.get(0).getId();
						}
					}else{// 其它   冲销   spbatch
						List<Spbatch> spbatchs = chkoutmMapper.findChkoutByCx(spbatch);
						if(spbatchs.size() != 0){
							batchid = spbatchs.get(0).getId();
						}
					}
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("chkoutno", chkoutno);
					m.put("sp_code", sa.getSp_code());
					m.put("amount", sa.getCntfirm()-sa.getCntout());//数量是负的
					m.put("price", sa.getPricesale() == 0?sa.getPriceout():sa.getPricesale());//如果有售价，取售价没有售价取出库价
					m.put("memo", sa.getDes());
					m.put("stoid", sa.getId());//cs 代码这么写的
					m.put("batchid", batchid);//先进先出表的id 查出来的
					m.put("deliver", sa.getDelivercode());//cs代码这么写的
					m.put("amount1", sa.getCntfirm1()-sa.getCntout1());//参考数量
					m.put("pricesale", sa.getPricesale() == 0?sa.getPricesale():sa.getPriceout());//如果有售价取出库价，没有售价为0
					m.put("chkstono", 0);//申购单号 后来加的 这里不用
					m.put("pr", 0);
					log.warn("sp_code:" + m.get("sp_code")+
							",amount:" + m.get("amount")+
							",price:" + m.get("price")+
							",memo:" + m.get("memo")+
							",stoid:" + m.get("stoid")+
							",batchid:" + m.get("batchid")+
							",deliver:" + m.get("deliver")+
							",amount1:" + m.get("amount1")+
							",pricesale:" + m.get("pricesale")
							);
					chkoutdMapper.saveChkoutd(m);
					result.put("pr", map.get("pr").toString());
					switch(Integer.parseInt(m.get("pr").toString())){
					case -1:
						result.put("pr", "-1");
						result.put("msg", "出库单"+chkoutno+"添加失败"+sa.getSp_code());
						throw new CRUDException(JSONObject.fromObject(result).toString());
					case -2:
						result.put("pr", "-2");
						result.put("msg", "出库单"+chkoutno+"添加失败"+sa.getSp_code());
						throw new CRUDException(JSONObject.fromObject(result).toString());
					}
				}
				
				//3.审核冲销单 失败回滚
				Map<String,Object> map1 = new HashMap<String,Object>();
				map1.put("chkoutno", chkoutno);
				map1.put("month",  acctService.getOnlyAccountMonth(maded)+"");
				map1.put("emp", acctName);
				map1.put("sta", "Y");
				map1.put("chk1memo", "验货不符冲消审核通过");//审核备注  wjf
				map1.put("pr", 0);
				map1.put("errdes", "");
				log.warn("\tchkoutno:"+chkoutno+"\n"+
						"month:"+map1.get("month")+
						",emp:"+map1.get("emp")+
						",sta:"+map1.get("sta")
						);
				chkoutmMapper.AuditChkout(map1);
				Chkoutm chkoutm = new Chkoutm();
				chkoutm.setChkoutno(chkoutno);
				chkoutmMapper.updateChkoutAmount(chkoutm);//冲消计算总价
				result.put("pr", map1.get("pr").toString());
				result.put("msg", map1.get("errdes") != null ? map1.get("errdes").toString() : "");
				switch(Integer.parseInt(map1.get("pr").toString())){
					case 1:break;
					default:
						result.put("msg",map1.get("errdes").toString());
						throw new CRUDException(JSONObject.fromObject(result).toString());
				}
				//4.更新这个冲消单的supplyacct 状态为2
				SupplyAcct sua = new SupplyAcct();
				sua.setAcct(acct);
				sua.setChkno(chkoutno);
				sua.setChktag(2);//出库
				sua.setChkks(2);//生成的退库单supplyacct的chkks的值，以后不处理差异，门店也不处理
				sua.setChkyh("Y");
				chkoutmMapper.updateChkks(sua);
				//4.更新要冲消报损的单子chkks  状态为3    感觉没必要 因为后面还要改为1.即使失败，也能都回滚
				//。。。
				if("2".equals(type)){
					//5.填报损单
					Integer chkoutno2 = chkoutmMapper.findNextNo();//单号
					result.put("chkoutno", String.valueOf(chkoutno2));
					Map<String,Object> map2 = new HashMap<String,Object>();
					map2.put("acct", acct);
					map2.put("yearr", mainInfoMapper.findYearrList().get(0)+"");
					map2.put("chkoutno", chkoutno2);
					map2.put("vouno", calChkNum.getNext(CalChkNum.CHKOUT, maded));
					map2.put("maded", maded);
					map2.put("madet", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					map2.put("positn", supplyAcct.getPositn());
					map2.put("firm", "1999");//入库仓位为盘亏
					map2.put("madeby", acctName);
					map2.put("totalamt", 0);//设为0  审核时会赋值
					map2.put("typ", "9915");//出库报废
					map2.put("memo", "验货不符冲消后报损");
					map2.put("pr", 0);
					log.warn("新增出库单(主单):\n"+
							"yearr:" + map2.get("yearr")+
							",chkoutno:" + map2.get("chkoutno")+
							",vouno:" + map2.get("vouno")+
							",maded:" + map2.get("maded")+
							",positn:" + map2.get("positn")+
							",firm:" + map2.get("firm")+
							",madeby:" + map2.get("madeby")+
							",totalamt:" + map2.get("totalamt")+
							",typ:" + map2.get("typ")+
							",memo:" + map2.get("memo")
							);
					chkoutmMapper.saveChkoutm(map2);
					
					result.put("pr", map2.get("pr").toString());
					if(Integer.parseInt(map2.get("pr").toString()) != 1){
						result.put("pr", "-1");
						result.put("msg","出库单"+chkoutno2+"添加失败");
						throw new CRUDException(JSONObject.fromObject(result).toString());
					}
					//6.报损单从表  需要回滚
					for(SupplyAcct sa :disList){
						//0.查找当时的spbatch id号    给从表的存储过程
						Spbatch spbatch = new Spbatch();
						spbatch.setAcct(acct);
						spbatch.setChkno(sa.getChkno());
						spbatch.setPositn(supplyAcct.getFirm());
						spbatch.setSp_code(sa.getSp_code());
						spbatch.setAmount(sa.getCntout()-sa.getCntfirm());
						spbatch.setPrice(sa.getPricesale() == 0?sa.getPriceout():sa.getPricesale());
						int batchid = 0;//先进先出表id,默认为0
						if("1203".equals(sa.getFirmtyp())){// 分店   冲销   spbatch_x
							List<Spbatch> spbatchs = chkoutmMapper.findChkoutByCx_X(spbatch);
							if(spbatchs.size() != 0){
								batchid = spbatchs.get(0).getId();
							}
						}else{// 其它   冲销   spbatch
							List<Spbatch> spbatchs = chkoutmMapper.findChkoutByCx(spbatch);
							if(spbatchs.size() != 0){
								batchid = spbatchs.get(0).getId();
							}
						}
						Map<String,Object> m = new HashMap<String,Object>();
						m.put("chkoutno", chkoutno2);///
						m.put("sp_code", sa.getSp_code());
						m.put("amount", sa.getCntout()-sa.getCntfirm());//数量是正的
						m.put("price", sa.getPricesale() == 0?sa.getPriceout():sa.getPricesale());//如果有售价，取售价没有售价取出库价
						m.put("memo", sa.getDes());
						m.put("stoid", sa.getId());
						m.put("batchid", batchid);
						m.put("deliver", sa.getDelivercode());
						m.put("amount1", sa.getCntout1()-sa.getCntfirm1());//参考数量
						m.put("pricesale", sa.getPricesale() == 0?sa.getPricesale():sa.getPriceout());//如果有售价取出库价，没有售价为0
						m.put("chkstono", 0);
						m.put("pr", 0);
						log.warn("sp_code:" + m.get("sp_code")+
								",amount:" + m.get("amount")+
								",price:" + m.get("price")+
								",memo:" + m.get("memo")+
								",stoid:" + m.get("stoid")+
								",batchid:" + m.get("batchid")+
								",deliver:" + m.get("deliver")+
								",amount1:" + m.get("amount1")+
								",pricesale:" + m.get("pricesale")
								);
						chkoutdMapper.saveChkoutd(m);
						result.put("pr", map.get("pr").toString());
						switch(Integer.parseInt(m.get("pr").toString())){
						case -1:
							result.put("pr", "-1");
							result.put("msg","出库单"+chkoutno2+"添加失败"+sa.getSp_code());
							throw new CRUDException(JSONObject.fromObject(result).toString());
						case -2:
							result.put("pr", "-2");
							result.put("msg","出库单"+chkoutno2+"添加失败"+sa.getSp_code());
							throw new CRUDException(JSONObject.fromObject(result).toString());
						}
					}
					
					//7.审核报损单 失败回滚
					Map<String,Object> map3 = new HashMap<String,Object>();
					map3.put("chkoutno", chkoutno2);
					map3.put("month",  acctService.getOnlyAccountMonth(maded)+"");
					map3.put("emp", acctName);
					map3.put("sta", "Y");
					map3.put("chk1memo", "验货不符冲消加报损审核通过");//审核备注  wjf
					map3.put("pr", 0);
					map3.put("errdes", "");
					log.warn("\tchkoutno:"+chkoutno2+"\n"+
							"month:"+map3.get("month")+
							",emp:"+map3.get("emp")+
							",sta:"+map3.get("sta")
							);
					chkoutmMapper.AuditChkout(map3);
					chkoutm = new Chkoutm();
					chkoutm.setChkoutno(chkoutno2);
					chkoutmMapper.updateChkoutAmount(chkoutm);//冲消计算总价
					result.put("pr", map3.get("pr").toString());
					result.put("msg", map3.get("errdes") != null ? map3.get("errdes").toString() : "");
					switch(Integer.parseInt(map3.get("pr").toString())){
						case 1:break;
						default:
							result.put("msg",map3.get("errdes").toString());
							throw new CRUDException(JSONObject.fromObject(result).toString());
					}
				}
			}else{
				//生成报盈补入到门店的直发单 供应商是1999 门店无需验货
				Chkinm chkinm = new Chkinm();
				chkinm.setAcct(acct);
				Date maded = DateFormat.formatDate(new Date(), "yyyy-MM-dd");//制单日期 今天
				chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
				int chkinno = chkinmMapper.getMaxChkinno();
				chkinm.setChkinno(chkinno);
				chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKZB, maded));
				chkinm.setMaded(maded);
				chkinm.setMadet(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				Deliver deliver = new Deliver();
				deliver.setCode(CalChkNum.profitDeliver);
				chkinm.setDeliver(deliver);
				Positn positn = new Positn();
				positn.setCode(supplyAcct.getPositn());
				chkinm.setPositn(positn);
				chkinm.setTotalamt(0);
				chkinm.setInout(ChkinmZbConstants.conk);
				chkinm.setTyp("9916");//正常直发
				chkinm.setMemo("报盈补入到门店的直发单");
				log.warn("添加直发单(主单)：\n"+chkinm);
		 		chkinmMapper.saveChkinm(chkinm);
				//2.冲销单从表  需要回滚
				for(SupplyAcct sa :disList){
					Chkind chkind = new Chkind();
					chkind.setChkinno(chkinno);
					Supply supply = new Supply();
					supply.setSp_code(sa.getSp_code());
					chkind.setSupply(supply);
					chkind.setAmount(sa.getCntfirm()-sa.getCntout());
					chkind.setPrice(sa.getPricesale() == 0?sa.getPriceout():sa.getPricesale());
					chkind.setMemo(sa.getDes());
					chkind.setInout("inout");   //加入直发标志
					chkind.setPound(0);
					chkind.setIndept(supplyAcct.getFirm());
					chkind.setAmount1(sa.getCntfirm1()-sa.getCntout1());
					chkindMapper.saveChkind(chkind);
				}
				//审核
				chkinm.setMonth(acctService.getOnlyAccountMonth(maded));//会计月 
				chkinm.setChecby(acctName);
				chkinm.setStatus("inout");
				chkinm.setChk1memo("报盈补入到门店的直发单审核通过");
				chkinmMapper.checkChkinm(chkinm);//审核入库单
				result.put("pr", chkinm.getPr()+"");
				result.put("msg", "直发单："+chkinno+"添加成功。");
				//4.更新这个直发单的supplyacct 状态为2
				SupplyAcct sua = new SupplyAcct();
				sua.setAcct(acct);
				sua.setChkno(chkinno);
				sua.setChktag(3);//出库
				sua.setChkks(2);//生成的退库单supplyacct的chkks的值，以后不处理差异，门店也不处理
				sua.setChkyh("Y");
				chkoutmMapper.updateChkks(sua);
			}
			//7.更新要冲消报损的单子chkks  状态为1 
			supplyAcct.setAcct(acct);
			supplyAcct.setChkks(1);
			supplyAcct.setChkstoNo(ids);//借用这个字段
			chkoutmMapper.updateChkks(supplyAcct);
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(CRUDException e){
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
}