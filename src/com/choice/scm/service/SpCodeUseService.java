package com.choice.scm.service;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkstod;
import com.choice.scm.domain.Chkstom;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.ScheduleStore;
import com.choice.scm.domain.SpCodeUse;
import com.choice.scm.persistence.ChkstodMapper;
import com.choice.scm.persistence.ChkstomMapper;
import com.choice.scm.persistence.InventoryMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.SpCodeUseMapper;

@Service
public class SpCodeUseService {
	
	@Autowired
	private SpCodeUseMapper spCodeUseMapper;
	@Autowired
	private PageManager<SpCodeUse> pageManager;
	@Autowired
	private ChkstomMapper chkstomMapper;
	@Autowired
	private ChkstodMapper chkstodMapper;
	@Autowired
	private InventoryMapper inventoryMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	
	private final transient Log log = LogFactory.getLog(SpCodeUseService.class);
	
	/**
	 * 判断是否做过盘点
	 * @return
	 * @throws CRUDException
	 */
	public String getStatus_pd(SpCodeUse spCodeUse) throws CRUDException
	{
		try {
			Map<String,Object> condition = new HashMap<String, Object>();
			condition.put("positnCd", spCodeUse.getFirm());
			condition.put("dat", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
			if(inventoryMapper.getStatus_pd(condition).size()==0) {
				return "N";
			}
			return null;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取配送班表对应的该分店的可报货的报货分类
	 * @return
	 * @throws CRUDException
	 */
//	public List<GrpTyp> findGrpTypByPsbb(ScheduleStore scheduleStore) throws CRUDException {
//		try {
//			return spCodeUseMapper.findGrpTypByPsbb(scheduleStore);
//		} catch (Exception e) {
//			log.error(e);
//			throw new CRUDException(e);
//		}
//	}
	
	/**
	 * 获取配送班表对应的该分店的报货日期和到货日期
	 * @return
	 * @throws CRUDException
	 */
	public SpCodeUse findDateByPsbb(ScheduleStore scheduleStore) throws CRUDException {
		try {
			return spCodeUseMapper.findDateByPsbb(scheduleStore);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 判断是审核所有进货单
	 * @return
	 * @throws CRUDException
	 */
//	public String getStatus_sh(SpCodeUse spCodeUse) throws CRUDException
//	{
//		try {
//			Dis dis = new Dis();
//			dis.setFirmCode(spCodeUse.getFirm());
//			dis.setInout("入库,直配");
//			dis.setChkin("N");
//			dis.setCheckinSelect("Y");
//			dis.setInd(DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
//			//未验收
//			if(disMapper.findAllDisPage(dis).size()>0) {
//				return "NG";
//			}
//			return null;
//		} catch (Exception e) {
//			log.error(e);
//			throw new CRUDException(e);
//		}
//	}
	
	/**
	 * 查找千人万元用量预估
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSpCodeUse(SpCodeUse spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMapper.listSpCodeUse(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 查询营业额
	 * @return
	 * @throws CRUDException
	 */
	public SpCodeUse getAmt(SpCodeUse spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMapper.getAmt(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public SpCodeUse getAmt_pos(SpCodeUse spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMapper.getAmt_pos(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public SpCodeUse getAmt_boh(SpCodeUse spCodeUse) throws CRUDException
	{
		try {
			return spCodeUseMapper.getAmt_boh(spCodeUse);	
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除所有菜品点击率
	 * @param
	 * @throws CRUDException
	 */
	public void deleteAll(SpCodeUse spCodeUse) throws CRUDException{
		try{
			spCodeUseMapper.deleteAll(spCodeUse);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算物资用量
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> calculate(SpCodeUse spCodeUse) throws CRUDException
	{
		try {
			//查找预估物资的使用量信息
			return spCodeUseMapper.listSupplyAcct(spCodeUse);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 重新计算物资用量
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSupplyAcctCk(SpCodeUse spCodeUse) throws CRUDException
	{
		try {
			//查找预估物资的使用量信息
			List<SpCodeUse> listSpCodeUse = spCodeUseMapper.listSupplyAcctCk(spCodeUse);
			List<SpCodeUse> listScu=new ArrayList<SpCodeUse>();
			for(int i=0;i<listSpCodeUse.size();i++){
				SpCodeUse spCode=new SpCodeUse();
				spCode=listSpCodeUse.get(i);
//				forecastcnt = forecastcnt.setScale(2,BigDecimal.ROUND_HALF_UP);  
				BigDecimal prorequi = new BigDecimal(spCode.getProrequi()); 
				prorequi = prorequi.setScale(2, BigDecimal.ROUND_HALF_UP); 
//				spCode.setForecastcnt(forecastcnt);
				spCode.setProrequi(Double.parseDouble(prorequi.toString()));
				if (prorequi.compareTo(BigDecimal.valueOf(0.0))==1){
					listScu.add(spCode);
				}
			}
			return listScu;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 获取点击率表达方式（单数，营业额，人数）
	 * @return
	 * @throws CRUDException
	 */
	public String getPlanTyp(String acct) throws CRUDException
	{
		try {
			return spCodeUseMapper.getPlanTyp(acct);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
		
	/**
	 * 保存修改调整量(菜品点击率)
	 * @param itemUse
	 * @throws CRUDException
	 */
	public void update(SpCodeUse spCodeUse) throws CRUDException {
		try {
			for (int i = 0; i < spCodeUse.getSpCodeUseList().size(); i++) {//批量修改
//				SpCodeUse spCodeUse_ = new SpCodeUse();
//				spCodeUse_.setSp_code(spCodeUse.getSpCodeUseList().get(i).getSp_code());
//				spCodeUse_.setCnt(spCodeUse.getSpCodeUseList().get(i).getCnt());
//				spCodeUse_.setCntold(spCodeUse.getSpCodeUseList().get(i).getCntold());
//				spCodeUse_.setMemo(spCodeUse.getSpCodeUseList().get(i).getMemo());
//				spCodeUse_.setFirm(spCodeUse.getSpCodeUseList().get(i).getFirm());
				spCodeUseMapper.insertSpCodeUse(spCodeUse.getSpCodeUseList().get(i));
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询预估营业额
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listPossalePlan(SpCodeUse spCodeUse)  throws CRUDException {
		try{
			return spCodeUseMapper.listPossalePlan(spCodeUse);	
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询物资
	 * @param spCodeUse
	 * @return
	 */
	public List<SpCodeUse> listSpSupply(SpCodeUse spCodeUse,Page page) throws CRUDException{
		try{
			return pageManager.selectPage(spCodeUse, page, SpCodeUseMapper.class.getName()+".listSpSupply");	
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 原始的预估报货物资耗用计算
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSpSupplyCalculate(SpCodeUse spCodeUse) throws CRUDException{
		try{
			List<SpCodeUse> listScu=new ArrayList<SpCodeUse>();
			List<SpCodeUse> listSpCodeUse=spCodeUseMapper.listSpSupply(spCodeUse);
			for(int i=0;i<listSpCodeUse.size();i++){
				SpCodeUse spCode=new SpCodeUse();
				spCode=listSpCodeUse.get(i);
				double forecastcnt = (spCodeUse.getTotal()/10000)*listSpCodeUse.get(i).getForecastcnt();   
//				forecastcnt = forecastcnt.setScale(2,BigDecimal.ROUND_HALF_UP);  
				BigDecimal prorequi = new BigDecimal((forecastcnt-spCode.getSupply().getCnt()-spCode.getCntzt())*spCodeUse.getSafetystock()); 
				prorequi = prorequi.setScale(2,BigDecimal.ROUND_HALF_UP); 
				spCode.setForecastcnt(forecastcnt);
				spCode.setProrequi(Double.parseDouble(prorequi.toString()));
				if (prorequi.compareTo(BigDecimal.valueOf(0.0))==1){
					listScu.add(spCode);
				}
			}
			return listScu;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 按照bom的预估报货物资耗用计算（门店）
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSupplyCalculateByBom(SpCodeUse spCodeUse) throws CRUDException{
		try{
			List<SpCodeUse> listScu=new ArrayList<SpCodeUse>();
			List<SpCodeUse> listSpCodeUse=spCodeUseMapper.listSupplyByBom(spCodeUse);
			for(int i=0;i<listSpCodeUse.size();i++){
				SpCodeUse spCode = listSpCodeUse.get(i);
				BigDecimal prorequi = new BigDecimal(spCode.getForecastcnt()-spCode.getSupply().getCnt()-spCode.getCntzt()); 
				prorequi = prorequi.setScale(2,BigDecimal.ROUND_HALF_UP); 
				spCode.setForecastcnt(spCode.getForecastcnt());
				spCode.setProrequi(Double.parseDouble(prorequi.toString()));
				if (prorequi.compareTo(BigDecimal.valueOf(0.0))==1){
					listScu.add(spCode);
				}
			}
			return listScu;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 按照bom的预估报货物资耗用计算（门店）
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSupplyCalculate(SpCodeUse spCodeUse) throws CRUDException{
		try{
			List<SpCodeUse> listScu=new ArrayList<SpCodeUse>();
			List<SpCodeUse> listSpCodeUse=spCodeUseMapper.listSupplyByMW(spCodeUse);
			for(int i=0;i<listSpCodeUse.size();i++){
				SpCodeUse spCode = listSpCodeUse.get(i);
				BigDecimal prorequi = new BigDecimal(spCode.getForecastcnt()-spCode.getSupply().getCnt()-spCode.getCntzt()); 
				prorequi = prorequi.setScale(2,BigDecimal.ROUND_HALF_UP); 
				spCode.setForecastcnt(spCode.getForecastcnt());
				spCode.setProrequi(Double.parseDouble(prorequi.toString()));
				if (prorequi.compareTo(BigDecimal.valueOf(0.0))==1){
					listScu.add(spCode);
				}
			}
			return listScu;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 按照bom的预估报货物资耗用计算（总部）
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSupplyCalculateZb(SpCodeUse spCodeUse) throws CRUDException{
		try{
			List<SpCodeUse> listScu=new ArrayList<SpCodeUse>();
			List<SpCodeUse> listSpCodeUse=spCodeUseMapper.listSupplyByMWZB(spCodeUse);
			for(int i=0;i<listSpCodeUse.size();i++){
				SpCodeUse spCode = listSpCodeUse.get(i);
				BigDecimal prorequi = new BigDecimal(spCode.getForecastcnt()-spCode.getSupply().getCnt()-spCode.getCntzt()); 
				prorequi = prorequi.setScale(2,BigDecimal.ROUND_HALF_UP); 
				spCode.setForecastcnt(spCode.getForecastcnt());
				spCode.setProrequi(Double.parseDouble(prorequi.toString()));
				if (prorequi.compareTo(BigDecimal.valueOf(0.0))==1){
					listScu.add(spCode);
				}
			}
			return listScu;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 按照bom的预估报货物资耗用计算（加工间）
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSupplyCalculateJGJ(SpCodeUse spCodeUse) throws CRUDException{
		try{
			List<SpCodeUse> listScu=new ArrayList<SpCodeUse>();
			List<SpCodeUse> listSpCodeUse=spCodeUseMapper.listSupplyByMWJGJ(spCodeUse);
			for(int i=0;i<listSpCodeUse.size();i++){
				SpCodeUse spCode = listSpCodeUse.get(i);
				BigDecimal prorequi = new BigDecimal(spCode.getForecastcnt()); 
				prorequi = prorequi.setScale(2,BigDecimal.ROUND_HALF_UP); 
				spCode.setForecastcnt(spCode.getForecastcnt());
				spCode.setProrequi(Double.parseDouble(prorequi.toString()));
				if (prorequi.compareTo(BigDecimal.valueOf(0.0))==1){
					listScu.add(spCode);
				}
			}
			return listScu;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 按照bom的预估报货物资耗用计算（总部）
	 * @param spCodeUse
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<SpCodeUse> listSupplyCalculateByBomZb(SpCodeUse spCodeUse) throws CRUDException{
		try{
			List<SpCodeUse> listScu=new ArrayList<SpCodeUse>();
			List<SpCodeUse> listSpCodeUse=spCodeUseMapper.listSupplyByBomZb(spCodeUse);
			for(int i=0;i<listSpCodeUse.size();i++){
				SpCodeUse spCode = listSpCodeUse.get(i);
				BigDecimal prorequi = new BigDecimal(spCode.getForecastcnt()-spCode.getSupply().getCnt()-spCode.getCntzt()); 
				prorequi = prorequi.setScale(2,BigDecimal.ROUND_HALF_UP); 
				spCode.setForecastcnt(spCode.getForecastcnt());
				spCode.setProrequi(Double.parseDouble(prorequi.toString()));
				if (prorequi.compareTo(BigDecimal.valueOf(0.0))==1){
					listScu.add(spCode);
				}
			}
			return listScu;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	//生成报货单数量进1
	public List<SpCodeUse> listSpSupplyCal(List<SpCodeUse> listSpSupplyCal){
		List<SpCodeUse> listSupply=new ArrayList<SpCodeUse>();
		if(listSpSupplyCal.size() !=0){
			for(int i=0;i<listSpSupplyCal.size();i++){
				SpCodeUse spcodeuse=new SpCodeUse();
				spcodeuse=listSpSupplyCal.get(i);
				spcodeuse.setProrequi(Math.ceil(listSpSupplyCal.get(i).getProrequi()));
				listSupply.add(spcodeuse);
			}
		}
		return listSupply;
	}
	
	/**
	 * 添加或修改报货单
	 * @param chkstom
	 * @throws CRUDException
	 */
	public String saveOrUpdateChk(Chkstom chkstom) throws CRUDException
	{
		String result="1";
		try {
			
			chkstom.setChkstoNo(chkstomMapper.getMaxChkstono());
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			chkstom.setYearr(mainInfoMapper.findYearrList().get(0));
			chkstom.setMadet(new SimpleDateFormat("HH:mm:ss").format(new Date()));
			Positn positn=new Positn();
			positn.setCode(chkstomMapper.findMainPositnByFirm(chkstom.getFirm()));
			chkstom.setPositn(positn);
			if(chkstom.getChkstodList() != null){
				for(Chkstod chkstod : chkstom.getChkstodList()){
					double amount=chkstod.getAmount();
					double price=chkstod.getPrice();
					totalAmt+=amount*price;
				}
			}
			chkstom.setTotalAmt(totalAmt);
			chkstom.setPr(0);
			//添加报货单日志
			log.warn("添加报货单(主单)：\n" + chkstom);
			chkstomMapper.saveNewChkstom(chkstom);
			result=chkstom.getPr().toString();
			if(chkstom.getChkstodList() != null){
				//保存报货单日志
				log.warn("保存报货单(详单)：");
				for(Chkstod chkstod : chkstom.getChkstodList()){
					chkstod.setChkstoNo(chkstom.getChkstoNo());
					//保存报货单日志
					log.warn(chkstod);
					chkstodMapper.saveNewChkstodMis(chkstod);
					if(chkstod.getPr()>=1){
						result="1";
					}
				}
			}
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
}