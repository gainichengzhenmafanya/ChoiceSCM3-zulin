package com.choice.scm.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.ChkdemoConstants;
import com.choice.scm.domain.Chkdemo;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.ChkdemoMapper;

@Service
public class ChkdemoService {
	@Autowired
	private ChkdemoMapper chkdemoMapper;
	@Autowired
	private PageManager<Chkdemo> pageManager;

	private final transient Log log = LogFactory.getLog(ChkdemoService.class);
	
	/**
	 * 查找当前最大顺序号
	 * @return
	 * @throws CRUDException
	 */
	public String findMaxId(Chkdemo chkdemo) throws CRUDException
	{
		try {
			return chkdemoMapper.findMaxId(chkdemo);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查找所有的盘点标题
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkdemo> findAllTitle(Chkdemo chkdemo) throws CRUDException
	{
		try {
			return chkdemoMapper.findAllTitle(chkdemo);			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * ajax查询
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkdemo> findChkdemoAjax(Chkdemo chkdemo) throws CRUDException
	{
		try {
			return chkdemoMapper.findAllTitle(chkdemo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 添加盘点
	 * @param sp_codes
	 * @throws CRUDException
	 */
	public void saveNewChkdemo(Chkdemo chkdemo) throws CRUDException
	{
		try {
			chkdemo.setTyp(ChkdemoConstants.PAN_DIAN);
			int id=chkdemo.getId();
			List<String> codeList=Arrays.asList(chkdemo.getSupply().getSp_code().split(","));
			for (int i = 0; i < codeList.size(); i++) {
				Supply supply=new Supply();
				supply.setSp_code(codeList.get(i));
				chkdemo.setSupply((supply));
				chkdemo.setId(id);
				id++;
				chkdemoMapper.saveNewChkdemo(chkdemo);
			}			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 查询信息
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkdemo> findChkdemo(Chkdemo chkdemo,Page page) throws CRUDException
	{
		try {
			return pageManager.selectPage(chkdemo, page, ChkdemoMapper.class.getName()+".findChkdemo");		
			//return chkdemoMapper.findChkdemo(chkdemo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 模板数据查询
	 * @return
	 * @throws CRUDException
	 */
	public String findChkdemoByRec(List<String> recList,List<String> cntList,List<String> cnt1List,String acct) throws CRUDException
	{
		Map<String,Object> result = new HashMap<String,Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("recList", recList);
		map.put("acct", acct);
		try {
			List<Chkdemo> chkdemoList=chkdemoMapper.findChkdemoByRec(map);
			for (int i = 0; i < chkdemoList.size(); i++) {
				Chkdemo newChkdemo=new Chkdemo();
				newChkdemo=chkdemoList.get(i);
				chkdemoList.set(i, newChkdemo);
			}
			result.put("chkdemoList", chkdemoList);
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除盘点信息
	 * @throws CRUDException
	 */
	public void deleteChkdemo(Chkdemo chkdemo,String acct) throws CRUDException
	{
		try {
			for (int i = 0; i < chkdemo.getChkdemoList().size(); i++) {
				Chkdemo chkde=new Chkdemo();
				chkde.setAcct(acct);
				chkde.setTitle(chkdemo.getChkdemoList().get(i).getTitle());
				chkde.setId(chkdemo.getChkdemoList().get(i).getId());
				chkdemoMapper.deleteChkdemoByTitleAndId(chkde);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除盘点信息
	 * @param sp_codes
	 * @throws CRUDException
	 */
	public void deleteChkdemo(Map<String,Object> map) throws CRUDException
	{
		try {
			chkdemoMapper.deleteChkdemo(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.equals(e);
			throw new CRUDException(e);
		}
	}
}
