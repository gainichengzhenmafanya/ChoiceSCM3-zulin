package com.choice.scm.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.SDList;
import com.choice.scm.domain.SppriceDemo;
import com.choice.scm.persistence.SppriceDemoMapper;

@Service
public class SppriceDemoService {

	@Autowired
	private SppriceDemoMapper sppriceDemoMapper;
	@Autowired
	private PageManager<SppriceDemo> pageManager;
	
	/**
	 * 查询价格模板信息
	 * @param demo
	 * @return
	 * @throws Exception
	 */
	public List<SppriceDemo> findSppriceDemo(SppriceDemo demo,Page page) throws Exception{
		try{
			return demo.getDeliver() == null || demo.getDeliver().equals("") ? null : pageManager.selectPage(demo, page, SppriceDemoMapper.class.getName()+".findSppriceDemo");
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新价格模板信息
	 * @param demo
	 * @throws Exception
	 */
	public void updateSppriceDemo(SppriceDemo demo) throws Exception{
		try{
			sppriceDemoMapper.updateSppriceDemo(demo);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量更新价格模板信息
	 * @param demos
	 * @throws Exception
	 */
	public void updateSppriceDemoBatch(SDList demos) throws Exception{
		try{
			String str = (new SimpleDateFormat("yyyy-MM-dd  kk:mm:ss ")).format(new Date());
			for(SppriceDemo demo : demos.getDemos()){
				demo.setCzdate(str);
				sppriceDemoMapper.updateSppriceDemo(demo);
			}
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存价格模板信息
	 * @param demo
	 * @throws Exception
	 */
	public void saveSppriceDemo(SppriceDemo demo) throws Exception{
		try{
			List<String> firms = Arrays.asList(demo.getFirm().split(","));
			for(String firm : firms){
				demo.setFirm(firm);
				sppriceDemoMapper.saveSppriceDemo(demo);
			}
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量添加价格模板信息
	 * @param demo
	 * @throws Exception
	 */
	public void saveBatchDemo(SppriceDemo demo) throws Exception{
		try{
			SppriceDemo cur = new SppriceDemo();
			cur.setAcct(demo.getAcct());
			cur.setDeliver(demo.getDeliver());
			cur.setBdat(new Date());
			cur.setEdat(new Date());
			List<String> supplys = new ArrayList<String>(new HashSet<String>(Arrays.asList(demo.getSp_code().split(","))));
			List<String> positns = new ArrayList<String>(new HashSet<String>(Arrays.asList(demo.getFirm().split(","))));
			
			for(String supply : supplys)
				for(String positn : positns){
					cur.setSp_code(supply);
					cur.setFirm(positn);
					sppriceDemoMapper.saveSppriceDemo(cur);
				}
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除价格模板信息
	 * @param demoList
	 * @throws Exception
	 */
	public void deleteSppriceDemo(SDList demos) throws Exception{
		try{
			for(SppriceDemo demo : demos.getDemos())
				sppriceDemoMapper.deleteSppriceDemo(demo);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	public void updatePrice(SppriceDemo demo) throws Exception{
		try{
			sppriceDemoMapper.updatePrice(demo);
	}catch(Exception e){
		throw new CRUDException(e);
	}
	}
}
