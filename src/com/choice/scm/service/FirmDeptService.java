package com.choice.scm.service;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.FirmDept;
import com.choice.scm.persistence.FirmDeptMapper;

@Service
public class FirmDeptService {

	@Autowired
	private FirmDeptMapper firmDeptMapper;
	
	private final transient static Log log = LogFactory.getLog(FirmDeptService.class);
	
	/**
	 * 查询分店部门
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public List<FirmDept> findFirmDept(FirmDept firmDept) throws CRUDException{
		try{
			return firmDeptMapper.findFirmDept(firmDept);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
}