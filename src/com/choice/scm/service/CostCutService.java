package com.choice.scm.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DateFormat;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.CostCut;
import com.choice.scm.domain.CostItem;
import com.choice.scm.domain.DateList;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.CostCutMapper;
import com.choice.scm.persistence.MainInfoMapper;

@Service
public class CostCutService {
	private static Logger log = Logger.getLogger(CostCutService.class);

	@Autowired
	private CostCutMapper costcutMapper;
	@Autowired
	private PageManager<Chkinm> pageManager;
	@Autowired
	private PageManager<CostCut> pageManager1;
	private String driver = "";
	private String url = "";
	private String uname = "";
	private String password = "";
	@Autowired
	private MainInfoMapper mainInfoMapper;
	
	@Autowired
	private AcctService acctService;

	/**
	 * 销售数据库连接获取（决策，总部boh，老总部快餐）
	 * 
	 * @return
	 * @throws CRUDException
	 */
	public void getJdbcConn() throws CRUDException, IOException {
		InputStream inputStream = this.getClass().getClassLoader()
				.getResourceAsStream("jdbc.properties");
		Properties properties = new Properties();
		properties.load(inputStream);
		driver = properties.getProperty("jdbc.driver_boh");
		url = properties.getProperty("jdbc.url_boh");
		uname = properties.getProperty("jdbc.username_boh");
		password = properties.getProperty("jdbc.password_boh");
	}

	/**
	 * 物流库连接获取
	 * 
	 * @return
	 * @throws CRUDException
	 */
	public void getJdbcConn_scm() throws CRUDException, IOException {
		InputStream inputStream = this.getClass().getClassLoader()
				.getResourceAsStream("jdbc.properties");
		Properties properties = new Properties();
		properties.load(inputStream);
		driver = properties.getProperty("jdbc.driver");
		url = properties.getProperty("jdbc.url");
		uname = properties.getProperty("jdbc.username");
		password = properties.getProperty("jdbc.password");
	}

	/******************************************************** 菜品理论成本核减start *************************************************/

	/**
	 * 查询所有门店
	 * 
	 * @param prdct
	 * @throws CRUDException
	 */
	public List<Positn> findPositn() throws Exception {
		try {
			return costcutMapper.findPositn();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询已核减数据分页
	 * @param costCut
	 * @param page
	 * @return
	 */
	public List<CostCut> findCostItemByCostCut(CostCut costCut, Page page) throws CRUDException {
		try {
			return pageManager1.selectPage(costCut,page,CostCutMapper.class.getName()+".findCostItemByCostCut");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据门店编码查询门店类型
	 * add by wangkai 2016-1-27 
	 * @return
	 */
	public String findVfoodSignByVcode(Positn positn)  throws CRUDException {
		try{
			return costcutMapper.findVfoodSignByVcode(positn);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/***
	 * 查询营业数据快餐
	 * 20160421wjf
	 * @param code
	 * @param startDate
	 * @param endDate
	 * @param ynUseDept
	 * @return
	 */
	public List<Map<String, Object>> findDatSubtract(String code,String startDate, String endDate, String ynUseDept) {
		return costcutMapper.findDatSubtract(code,startDate,endDate,ynUseDept);
	}
	
	/***
	 * 查询营业数据中餐
	 * 20160421wjf
	 * @param code
	 * @param startDate
	 * @param endDate
	 * @param ynUseDept
	 * @return
	 */
	public List<Map<String, Object>> findDatSubtract_cn(String code,String startDate, String endDate, String ynUseDept) {
		return costcutMapper.findDatSubtract_cn(code,startDate,endDate,ynUseDept);
	}
	
	/**
	 * 成本核减 - -   审核
	 * @author 文清泉
	 * @param 2015年4月28日 下午2:22:22
	 * @param startDate 开始时间
	 * @param endDate 结束时间
	 * @param session 
	 * @return 
	 * @throws CRUDException 
	 */
	public int Subtract(String startDate, String endDate, Positn positn, List<Map<String,Object>> listMap) throws CRUDException {
		try {
			antiReduction(startDate,endDate,positn);
			//查询供应链档口放到map
			List<Positn> pfList = costcutMapper.findPositnfirmByPositn(positn);
			HashMap<String,String> pfMap = new HashMap<String,String>();
			for(Positn pf : pfList){
				pfMap.put(pf.getOldcode(), pf.getCode());
			}
			String acct = positn.getAcct();
			List<CostItem> list = new ArrayList<CostItem>();
			for (Map<String, Object> map : listMap) {
				CostItem cost = new CostItem();
				cost.setAcct(acct);
				cost.setAmt(Double.parseDouble(map.get("AMT").toString()));
				cost.setChkcost("N");//默认未做成本卡，存储过程会改回来
				cost.setTx("A");//现在核减只查的是正常菜
				cost.setCnt(Double.parseDouble(map.get("CNT").toString()));
				cost.setDat(DateFormat.getDateByString(map.get("DWORKDATE").toString(),"yyyy-MM-dd"));
				cost.setDept(null == map.get("DEPT") ? "":map.get("DEPT").toString());
				cost.setDeptdes(null == map.get("DEPTDES") ? "":map.get("DEPTDES").toString());
				String dept1 = pfMap.get(cost.getDept());
				cost.setDept1(dept1);
				cost.setFirm(positn.getCode());
				cost.setDes(null == map.get("VPNAME") ? "":map.get("VPNAME").toString());
				cost.setFirmdes(positn.getDes());
				cost.setItem(map.get("VPCODE").toString());
				if(Double.parseDouble(map.get("CNT").toString()) >0){
					cost.setItprice(Double.parseDouble(map.get("AMT").toString())/Double.parseDouble(map.get("CNT").toString()));
				}else{
					cost.setItprice(0);
				}
				cost.setYnunit("N");//默认不是双单位 如果第二单位数量有值 则改为Y
				cost.setNum(Double.parseDouble(map.get("NUM").toString()));
				if(cost.getNum() > 0)
					cost.setYnunit("Y");
				cost.setUnit(null == map.get("VUNITS") ? "":map.get("VUNITS").toString());
				cost.setTyp(null == map.get("VCCLEASSNAME") ? "":map.get("VCCLEASSNAME").toString());
				cost.setPositn(null != dept1 && !"".equals(dept1) ? dept1 : positn.getCode());
				cost.setDisc(null != map.get("DISC") ? Double.valueOf(map.get("DISC").toString()):0);//折扣
				cost.setSvc(null != map.get("SVC") ? Double.valueOf(map.get("SVC").toString()):0);//服务费
				cost.setTax(null != map.get("TAX") ? Double.valueOf(map.get("TAX").toString()):0);//税金
				cost.setRefund(null != map.get("REFUND") ? Double.valueOf(map.get("REFUND").toString()):0);//免项
				cost.setPickdisc(null != map.get("PICKDISC") ? Double.valueOf(map.get("PICKDISC").toString()):0);//套餐优惠
				cost.setTotal(null != map.get("TOTAL") ? Double.valueOf(map.get("TOTAL").toString()):0);//实收额
				list.add(cost);
			}
			String yearr = mainInfoMapper.findYearrList().get(0)+"";
			//核减存储接收按日期来核减
			Date bdat = DateFormat.getDateByString(startDate, "yyyy-MM-dd");
			Date edat = DateFormat.getDateByString(endDate, "yyyy-MM-dd");
			long day = (edat.getTime() - bdat.getTime())/(3600 * 24 * 1000);
			for (long i = 0; i <= day; i++) {
				CostItem cost = new CostItem();
				cost.setAcct(acct);
				cost.setYear(yearr);
				cost.setFirm(positn.getCode());
				Date date = new Date(bdat.getTime() + i*(3600 * 24 * 1000));
				cost.setDat(date);
				int month = acctService.getOnlyAccountMonth2(date);
				cost.setMonth(month);//应该是当前日期所在会计月
				costcutMapper.excutecostcut(cost);
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 描述：反核减
	 * @author 马振
	 * 创建时间：2015-1-1 下午5:32:06
	 * @param startDate
	 * @param endDate
	 * @param session
	 * @throws CRUDException
	 */
	public int antiReduction(String startDate, String endDate, Positn positn) throws CRUDException{
		int sta = -3;
		try{
			String acct = positn.getAcct();
			String yearr = mainInfoMapper.findYearrList().get(0)+"";
			//核减存储接收按日期来核减
			Date bdat = DateFormat.getDateByString(startDate, "yyyy-MM-dd");
			Date edat = DateFormat.getDateByString(endDate, "yyyy-MM-dd");
			long day = (edat.getTime() - bdat.getTime())/(3600 * 24 * 1000);
			for (long i = 0; i <= day; i++) {
				CostItem cost = new CostItem();
				cost.setAcct(acct);
				cost.setYear(yearr);
				cost.setFirm(positn.getCode());
				Date date = new Date(bdat.getTime() + i*(3600 * 24 * 1000));
				cost.setDat(date);
				int month = acctService.getOnlyAccountMonth2(date);
				cost.setMonth(month);//应该是当前日期所在会计月
				costcutMapper.excutecostcut_re(cost);
			}
			sta = 2;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return sta;
	}
	
	

	/**
	 * 查询已核减信息
	 * 
	 * @param prdct
	 * @throws CRUDException
	 */
	public List<CostItem> queryByDate(CostItem costItem) throws Exception {
		try {
			return costcutMapper.queryByDate(costItem);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据缩写码查询分店
	 * 
	 * @param key
	 * @throws CRUDException
	 */
	public List<Positn> findByKey(String key) throws Exception {
		try {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("key", key);
			return costcutMapper.findByKey(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 判断是否已核减
	 * 
	 * @param codeList
	 * @param acct
	 * @param startdate
	 * @param enddate
	 * @param costItem
	 * @throws CRUDException
	 */
	public HashMap<String, Object> chkOrNot(CostCut costCut) throws Exception {
		try {
			HashMap<String, Object> returnMap = new HashMap<String, Object>();
			List<CostItem> costItemList = costcutMapper.chkOrNot(costCut);
			if (costItemList.size() > 0) {
				returnMap.put("firm", costItemList.get(0).getFirm());
				returnMap.put("date", DateFormat.getStringByDate(costItemList
						.get(0).getDat(), "yyyy-MM-dd"));
				return returnMap;
			}
			return null;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 判断是否已核减
	 * 
	 * @param codeList
	 * @param acct
	 * @param startdate
	 * @param enddate
	 * @param costItem
	 * @throws CRUDException
	 */
	public HashMap<String, Object> excutecostcutAnti(CostCut costCut) throws Exception {
		try {
			HashMap<String, Object> returnMap = new HashMap<String, Object>();
			costcutMapper.excutecostcutAnti(costCut);
			costcutMapper.deleteCostitemSpcode(costCut);
			returnMap.put("status", "ok");
			return returnMap;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 核减理论成本数据
	 * 
	 * @param codeList
	 * @param acct
	 * @param startdate
	 * @param enddate
	 * @param costItem
	 * @throws CRUDException
	 */
	public List<CostCut> getCostcut(CostCut costCut, String mis)
			throws Exception {
		try {
			List<String> codeList = costCut.getCodeList();
			String startdate = costCut.getBdate();
			String enddate = costCut.getEdate();
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("bdate", startdate);
			map.put("edate", enddate);
			List<Integer> codelist = new ArrayList<Integer>();
			for (int i = 0; i < codeList.size(); i++) {
				codelist.add(Integer.parseInt(codeList.get(i)));
			}
			if ("2".equals(mis)) {
				map.put("deptList", codelist);
				map.put("firmId", costCut.getFirmid());
			} else {
				map.put("firmList", codelist);
			}
			return costcutMapper.getCostcut(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public List<CostCut> getCostcutBoh(CostCut costCut) throws Exception {
		try {
			return costcutMapper.getCostcutBoh(costCut);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询boh核减分页方法
	 */
	public List<CostCut> getCostcutBoh(CostCut costCut,Page page) throws Exception {
		try {
			return pageManager1.selectPage(costCut,page,CostCutMapper.class.getName()+".getCostcutBoh");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新版本对接中餐  查询    广豹给的   boh对接中餐语句 查询 菜品   xlh   2015.1.23
	 * @param costCut
	 * @return
	 * @throws Exception
	 */
	public List<CostCut> getCostcutBoh_tele(CostCut costCut) throws Exception {
		try {
			return costcutMapper.getCostcutBoh_tele(costCut);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 核减理论成本数据
	 */
	public void excutecostcut(List<CostCut> costcutList, CostCut costCut) throws Exception {
		try {
			List<String> codeList = costCut.getCodeList();   // 分店名称
			String acct = costCut.getAcct();          
			String startdate = costCut.getBdate();
			String enddate = costCut.getEdate();
			if (costcutList.size() > 0) {
				for (int j = 0; j < costcutList.size(); j++) {
					costcutList.get(j).setAcct(acct);
					Positn positn = new Positn();
					positn.setCode(costcutList.get(j).getFirmid());
					costcutList.get(j).setFirmdes(costcutMapper.getFirmdes(positn));
//					costcutList.get(j).setDat(startdate);
					costcutList.get(j).setRec(costcutMapper.getLastRec());
					// 数据插入物流库costItem表
					insertCostItem(costcutList.get(j));
				}
			}
			CostItem costItem = new CostItem();
			costItem.setAcct(acct);
			Date edat = DateFormat.getDateByString(enddate, "yyyy-MM-dd");
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("acct", acct);
			for (int i = 0; i < codeList.size(); i++) {//循环分店 
				// 按分店核减
				Date bdat = DateFormat.getDateByString(startdate, "yyyy-MM-dd");
				costItem.setFirm(codeList.get(i));
				costItem.setDat(bdat);
				costItem.setYear(startdate.substring(0, 4));
				map.put("date", startdate);
				costItem.setMonth(getMonth(map));
				excutecostcut(costItem);//调用核减  
				if (bdat.getTime() != edat.getTime()) {
					bdat = new Date(bdat.getTime() + 24 * 3600 * 1000);
					costItem.setDat(bdat);
					startdate = DateFormat.getStringByDate(bdat, "yyyy-MM-dd");
					map.put("date", startdate);
					map.put("firm", codeList.get(i));
					costItem.setYear(startdate.substring(0, 4));
					costItem.setMonth(getMonth(map));
					excutecostcut(costItem);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public int excutecostcut(CostItem costItem) throws Exception {
		try {
			costcutMapper.excutecostcut(costItem);
			return costItem.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 插入CostItem表
	 * 
	 * @param costcut
	 * @throws CRUDException
	 */
	public void insertCostItem(CostCut costCut) throws Exception {
		try {
			costcutMapper.insertCostItem(costCut);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	public int getMonth(HashMap<String, Object> map) throws Exception {

		int num = 0;
		String date = (String) map.get("date");
		map.put("year", date.substring(0, 4));
		try {
			Date bdat = DateFormat.getDateByString(date, "yyyy-MM-dd");
			map.remove("date");
			DateList dateList = costcutMapper.selectmonth(map);
			if (bdat.getTime() > dateList.getBdat1().getTime()
					&& bdat.getTime() < dateList.getEdat1().getTime()) {
				num = 1;
			} else if (bdat.getTime() > dateList.getBdat2().getTime()
					&& bdat.getTime() < dateList.getEdat2().getTime()) {
				num = 2;
			} else if (bdat.getTime() > dateList.getBdat3().getTime()
					&& bdat.getTime() < dateList.getEdat3().getTime()) {
				num = 3;
			} else if (bdat.getTime() > dateList.getBdat4().getTime()
					&& bdat.getTime() < dateList.getEdat4().getTime()) {
				num = 4;
			} else if (bdat.getTime() > dateList.getBdat5().getTime()
					&& bdat.getTime() < dateList.getEdat5().getTime()) {
				num = 5;
			} else if (bdat.getTime() > dateList.getBdat6().getTime()
					&& bdat.getTime() < dateList.getEdat6().getTime()) {
				num = 6;
			} else if (bdat.getTime() > dateList.getBdat7().getTime()
					&& bdat.getTime() < dateList.getEdat7().getTime()) {
				num = 7;
			} else if (bdat.getTime() > dateList.getBdat8().getTime()
					&& bdat.getTime() < dateList.getEdat8().getTime()) {
				num = 8;
			} else if (bdat.getTime() > dateList.getBdat9().getTime()
					&& bdat.getTime() < dateList.getEdat9().getTime()) {
				num = 9;
			} else if (bdat.getTime() > dateList.getBdat10().getTime()
					&& bdat.getTime() < dateList.getEdat10().getTime()) {
				num = 10;
			} else if (bdat.getTime() > dateList.getBdat11().getTime()
					&& bdat.getTime() < dateList.getEdat11().getTime()) {
				num = 11;
			} else if (bdat.getTime() > dateList.getBdat12().getTime()
					&& bdat.getTime() < dateList.getEdat12().getTime()) {
				num = 12;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		return num;
	}

	/******************************************************** 菜品理论成本核减end *************************************************/
	/******************************************************** 加工间理论成本核减start *************************************************/

	/**
	 * 查询入库单以及对应的供应商信息
	 * 
	 * @param prdct
	 * @throws CRUDException
	 */
	public List<Chkinm> findChkinm(Chkinm chkinm, Page page) throws Exception {
		try {
			return pageManager.selectPage(chkinm, page,
					CostCutMapper.class.getName() + ".findChkinm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 加工间理论成本数据核减
	 * 
	 * @param codeList
	 * @param acct
	 * @param startdate
	 * @param enddate
	 * @param costItem
	 * @throws CRUDException
	 */
	public int excuteExcostcut(Chkinm chkinm) throws Exception {
		try {
			// 清除本月已核减数据
			costcutMapper.deleteExcostspcode(chkinm);
			// 更新数据为未核减状态
			costcutMapper.updatePositnSupply(chkinm);
			// 查询该月所有单号对应的加工数据
			List<Chkinm> chkinmList = costcutMapper.findChkinm(chkinm);
			chkinm.setStatus("Y");
			for (int i = 0; i < chkinmList.size(); i++) {
				chkinm.setChkinno(chkinmList.get(i).getChkinno());
				// 调用核减存储过程
				costcutMapper.excuteExcostcut(chkinm);
				if (chkinm.getPr() == 0) {
					return 0;
				}
			}
			return 1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/******************************************************** 加工间理论成本核减end *************************************************/

}
