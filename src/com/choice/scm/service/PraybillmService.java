package com.choice.scm.service;


import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Praybilld;
import com.choice.scm.domain.Praybillm;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.persistence.PraybilldMapper;
import com.choice.scm.persistence.PraybillmMapper;

@Service

public class PraybillmService {

	@Autowired
	private PageManager<Praybillm> pageManager;
	@Autowired
	private PraybillmMapper praybillmMapper;
	@Autowired
	private PraybilldMapper praybilldMapper;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	private final transient Log log = LogFactory.getLog(PraybillmService.class);

	/**
	 * 查询当前最大单号
	 * @return
	 * @throws CRUDException
	 */
	public int getMaxPraybillmno() throws CRUDException{
		try {
			return praybillmMapper.getMaxPraybillmno();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 按单号查询
	 * @param praybillm
	 * @return
	 * @throws CRUDException
	 */
	public Praybillm findByPraybillmNo(Praybillm praybillm) throws CRUDException {
		try {
			return praybillmMapper.findByPraybillmNo(praybillm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加或修改报货单
	 * @param praybillm
	 * @throws CRUDException
	 */
	public String saveOrUpdateChk(Praybillm praybillm, String sta) throws CRUDException {
		String result="1";
		try {
//			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");  
//			Properties properties = new Properties();
//			properties.load(inputStream);
			//定义一个变量计算新增一个报货单时的总金额
			double totalAmt=0;
			praybillm.setYearr(mainInfoMapper.findYearrList().get(0));
			praybillm.setMaded(new Date());
			Positn positn=new Positn();
			positn.setCode("1000");
			praybillm.setPositn(positn);
			if(praybillm.getPraybilldList() != null){//计算总价
				for(Praybilld praybilld : praybillm.getPraybilldList()){
					double amount=praybilld.getAmount();
					double price=praybilld.getPrice();
					totalAmt+=amount*price;
				}
			}
			praybillm.setTotalAmt(totalAmt);
			if(null!=sta && !"".equals(sta) && "add".equals(sta)){
				praybillm.setPrclass("普通请购");
				praybillm.setPrfrom("自制请购单");
				//添加报货单日志
				log.warn("添加请购单(主单)：\n" + praybillm);
				praybillmMapper.saveNewPraybillm(praybillm);
			}else{
//				HashMap<Object,Object>  map = new HashMap<Object,Object> ();
//				Chkstom chkstomN = new Chkstom();
//				chkstomN.setChkstoNo(praybillm.getChkstoNo());
//				map.put("chkstom", chkstomN);
//				praybillm.setVouno(chkstomMapper.findByKey(map).get(0).getVouno());
				//修改报货单日志
				log.warn("修改报货单(详单)：\n" + praybillm);
				praybillmMapper.updatePraybillm(praybillm);
			}
			if(praybillm.getPraybilldList() != null){
				//保存报货单日志
				log.warn("保存请购单(详单)：");
				for(Praybilld praybilld : praybillm.getPraybilldList()){
					praybilld.setPraybillmNo(praybillm.getPraybillmNo());
					praybilldMapper.saveNewPraybilld(praybilld);
					result = praybilld.getPr().toString();
					if(Integer.parseInt(result)>=1){
						result = "1";
					}
				}
			}
			return result;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 关键字查找请购单
	 * @param map
	 * @return
	 * @throws CRUDException
	 */
	public List<Praybillm> findByKey(HashMap<String,Object> map,Page page) throws CRUDException{
		try {
			return pageManager.selectPage(map, page, PraybillmMapper.class.getName()+".findByKey");			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除
	 */
	public String deletePraybillm(Praybillm praybillm,String praybillmNos) throws CRUDException {
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			//删除报货单日志
			log.warn("删除报货单:\n" + praybillm + "\n" + praybillmNos);
			if(null!=praybillmNos && !"".equals(praybillmNos)){
				ids=Arrays.asList(praybillmNos.split(","));
			}else{
				ids=Arrays.asList(praybillm.getPraybillmNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Praybillm p = new Praybillm(); 
				p.setPraybillmNo(Integer.parseInt(ids.get(i)));
				praybillmMapper.deletePraybill(p);
				result.put("pr", p.getPr().toString());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 审核
	 */
	public String checkChk(Praybillm praybillm, String praybillmNos) throws CRUDException {
		List<String> ids=null;
		Map<String,String> result = new HashMap<String,String>();
		try {
			//审核报货单日志
			log.warn("审核报货单:\n" + praybillm + "\n" + praybillmNos);
			if(null!=praybillmNos && !"".equals(praybillmNos)){
				ids=Arrays.asList(praybillmNos.split(","));
			}else{
				ids=Arrays.asList(praybillm.getPraybillmNo().toString().split(","));
			}
			for (int i = 0; i < ids.size(); i++) {
				Praybillm p = new Praybillm();
				p.setPraybillmNo(Integer.parseInt(ids.get(i)));
				p.setChecby(praybillm.getChecby());
				praybillmMapper.checkPraybill(p);
				result.put("pr", p.getPr().toString());	
				result.put("checby", p.getChecby());
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.equals(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加物资前验证
	 */
	public double checkSppriceSale(String firmid) throws CRUDException {
		try {
			return praybillmMapper.checkSppriceSale(firmid);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
}
