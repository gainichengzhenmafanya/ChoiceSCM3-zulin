package com.choice.scm.service.reportAnaMis;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.ReportObject;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.persistence.FirmMisReportMapper;
import com.choice.scm.persistence.reportMis.SupplyAnaMisMapper;

@Service
public class JhJiageBijiaoMisService {

	@Autowired
	private ReportObject<Map<String,Object>> mapReportObject;
	@Autowired 
	private PageManager<Map<String,Object>> mapPageManager;
	@Autowired 
	private SupplyAnaMisMapper supplyAnaMisMapper;
		
	/**
	 * 进货价格分析
	 * @param condition
	 * @return
	 * @throws CRUDException
	 */
	public ReportObject<Map<String,Object>> findFirmStockPrice(Map<String,Object> condition,Page pager) throws CRUDException{
		try{
			mapReportObject.setRows(mapPageManager.selectPage(condition, pager, SupplyAnaMisMapper.class.getName()+".findFirmStockPrice"));
			mapReportObject.setTotal(pager.getCount());
			return mapReportObject;
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 分店菜品利润明细
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findFirmFoodProfit(Map<String,Object> conditions,Page page) throws CRUDException{
		SupplyAcct supplyAcct = (SupplyAcct) conditions.get("supplyAcct");
		if(null!=supplyAcct && !"".equals(supplyAcct)){
			if(null!=supplyAcct.getFirm()&&!"".equals(supplyAcct.getFirm())){
				supplyAcct.setFirm(CodeHelper.replaceCode(supplyAcct.getFirm()));
			}
			if(null!=supplyAcct.getTyp()&&!"".equals(supplyAcct.getTyp())){
				supplyAcct.setTyp(CodeHelper.replaceCode(supplyAcct.getTyp()));
			}
		}
		mapReportObject.setRows(mapPageManager.selectPage(conditions, page, FirmMisReportMapper.class.getName()+".findFirmFoodProfit"));
		List<Map<String,Object>> foot = supplyAnaMisMapper.findCalForFirmFoodProfit(conditions);
		mapReportObject.setFooter(foot);
		mapReportObject.setTotal(page.getCount());
		return mapReportObject;
	}
	
	/**
	 * 导出分店物资进货价格比较
	 * @param os
	 * @param supplyAcct
	 * @return
	 * @throws Exception
	 */
	public boolean exportSupplyInPriceCompare(OutputStream os,SupplyAcct supplyAcct) throws Exception{
		return true;
	}
	
	/**
	 * 分店物资进货价格比较
	 * @param conditions
	 * @param page
	 * @return
	 * @throws CRUDException 
	 */
	public ReportObject<Map<String,Object>> findSupplyInPriceCompare(Map<String,Object> conditions,Page page) throws CRUDException{
		return null;
	}
}
