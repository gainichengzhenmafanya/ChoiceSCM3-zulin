package com.choice.scm.service;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.FirmDeliver;
import com.choice.scm.persistence.FirmDeliverMapper;

@Service
public class FirmDeliverService {

	@Autowired
	private FirmDeliverMapper firmDeliverMapper;
	@Autowired
	private PageManager<Deliver> pageManager;
	
	/**
	 * 报货分拨修改供应商，查找方向
	 */
	public String findInout(Deliver deliver,String oldCode) throws CRUDException{
		Map<String,Object> map=new HashMap<String, Object>();
		String newInout=null;
		String oldInout=null;
		try{
			newInout=firmDeliverMapper.findInout(deliver);
			if(null!=newInout && !"".equals(newInout)){
				Deliver d=new Deliver();
				d.setFirm(deliver.getFirm());
				d.setCode(oldCode);
				oldInout=firmDeliverMapper.findInout(d);
			}
			if("1".equals(newInout)){//方向等于直配
				if("1".equals(oldInout)){
					map.put("inout", newInout);
					map.put("sta", "0");
				}else{
					map.put("sta", "1");
				}
			}else{
				if(!"1".equals(oldInout)){
					map.put("inout", newInout);
					map.put("sta", "0");
				}else{
					map.put("sta", "1");
				}
			}
			JSONObject rs = JSONObject.fromObject(map);
			return rs.toString();
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据仓位查询 供应商信息
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findDeliverByPositn(String positnCode,Page page) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("positnCode", positnCode);
			return pageManager.selectPage(map, page, FirmDeliverMapper.class.getName()+".findDeliverByPositn");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据 供应商查询分店仓位信息
	 * @param deliver
	 * @param page
	 * @return deliverList
	 * @throws CRUDException
	 */
	public List<Deliver> findPositnByDeliver(String deliver,Page page) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("deliver", deliver);
			return pageManager.selectPage(map, page, FirmDeliverMapper.class.getName()+".findPositnByDeliver");
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据仓位查询 供应商信息   不分页
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public List<Deliver> findDeliverByPositn(String positnCode) throws CRUDException{
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("positnCode", positnCode);
			return   firmDeliverMapper.findDeliverByPositn(map);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据仓位查询 全部供应商编码
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public String findAllDeliverByPositn(String positnCode) throws CRUDException{
		StringBuffer result = new StringBuffer();
		try{
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("positnCode", positnCode);
			List<Deliver> list = firmDeliverMapper.findDeliverByPositn(map);
			if(list.size()>0){
				for(Deliver deliver:list){
					result.append(deliver.getCode()+",");
				}
				return result.substring(0, result.length()-1);
			}
			return "";
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
	
	/**
	 * 根据仓位增加 供应商信息
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public String saveDeliverByPositn(String acct,String firm,String deliverString) throws CRUDException{
		try{
			FirmDeliver firmDeliver = new FirmDeliver();
			String[] deliverArray = deliverString.split(",");
			String saveSta=null;
			for(String deliver:deliverArray){
				firmDeliver = new FirmDeliver();
				firmDeliver.setAcct(acct);
				firmDeliver.setFirm(firm);
				firmDeliver.setDeliver(deliver);
				firmDeliver.setInout(0);
				firmDeliverMapper.saveFirmDeliver(firmDeliver);
			}
			return saveSta;
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检查是否已经存在该数据
	 */
	public String checkOne(String acct, String firm,String deliverString) throws CRUDException{
		Deliver del=new Deliver();
		try{
			String[] deliverArray = deliverString.split(",");
			String inout=null;
			String saveSta=null;
			for(String deliver:deliverArray){
				del.setCode(deliver);
				del.setFirm(firm);
				inout=firmDeliverMapper.findInout(del);
				if("1".equals(inout)){
					saveSta=deliver+"分店下的供应商已经添加为直配，不能再次添加为直发！";
					return saveSta;
				}else if("0".equals(inout)){
					saveSta=deliver+"已经分配给该分店！";
					return saveSta;
				}
			}
			saveSta = "OK";
			return saveSta;
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据仓位删除供应商信息
	 * @param announcement
	 * @return
	 * @throws CRUDException
	 */
	public void deleteDeliverByPositn(String acct,String firm,String deliverString) throws CRUDException{
		try{
			List<String> deliverList = Arrays.asList(deliverString.split(","));
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("acct", acct);
			map.put("firm", firm);
			map.put("deliverList", deliverList);
			
			firmDeliverMapper.deleteFirmDeliver(map);
		}catch(Exception e){
			e.printStackTrace();
			throw new CRUDException(e);
		}
		
	}
}