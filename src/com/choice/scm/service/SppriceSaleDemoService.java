package com.choice.scm.service;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.SppriceSale;
import com.choice.scm.domain.SppriceSaleDemo;
import com.choice.scm.domain.SppriceSaleDemoList;
import com.choice.scm.domain.SppriceSaleList;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.PositnMapper;
import com.choice.scm.persistence.SppriceSaleDemoMapper;
import com.choice.scm.persistence.SupplyMapper;
import com.choice.scm.util.FileWorked;
import com.choice.scm.util.PublicExportExcel;

@Service
public class SppriceSaleDemoService {

	@Autowired
	private SppriceSaleDemoMapper sppriceSaleDemoMapper;
	@Autowired
	private PageManager<SppriceSaleDemo> sppriceSaleDemoPageManager;
	@Autowired
	private PageManager<SppriceSale> sppriceSalePageManager;
	@Autowired
	private SupplyMapper supplyMapper;
	@Autowired
	private PositnMapper positnMapper;
	private final transient static Log log = LogFactory.getLog(SppriceSaleDemoService.class);
	
	/**
	 * 查询售价单模板
	 */
	public List<SppriceSaleDemo> findSppriceSaleDemo(SppriceSaleDemo sppriceSaleDemo,Page page) throws CRUDException{
		try{
			return sppriceSaleDemoPageManager.selectPage(sppriceSaleDemo, page, SppriceSaleDemoMapper.class.getName()+".findSppriceSaleDemo");
//			return sppriceSaleDemoMapper.findSppriceSaleDemo(sppriceSaleDemo);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询售价单模板 不分页   
	 */
	public List<SppriceSaleDemo> findSppriceSaleDemoNoPage(SppriceSaleDemo sppriceSaleDemo) throws CRUDException{
		try{
			return sppriceSaleDemoMapper.findSppriceSaleDemo(sppriceSaleDemo);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询单条记录
	 */
	public SppriceSaleDemo findSppriceSaleDemoOne(SppriceSaleDemo sppriceSaleDemo) throws CRUDException{
		try{
			return sppriceSaleDemoMapper.findSppriceSaleDemoOne(sppriceSaleDemo);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询售价
	 */
	public List<SppriceSale> findSppriceSale(SppriceSale sppriceSale,Page page) throws CRUDException{
		try {
			return sppriceSalePageManager.selectPage(sppriceSale, page, SppriceSaleDemoMapper.class.getName()+".findSppriceSale");
//			return sppriceSaleDemoMapper.findSppriceSale(sppriceSale);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询售价不带分页
	 */
	public List<SppriceSale> findSppriceSaleNoPage(SppriceSale sppriceSale) throws CRUDException{
		try {
			return sppriceSaleDemoMapper.findSppriceSale(sppriceSale);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 保存新增售价单模板
	 * @param sppriceSaleDemo
	 * @throws CRUDException
	 */
	public String saveByAddSppriceSaleDemo(SppriceSaleDemo sppriceSaleDemo,String h_sp_code,String h_sp_name) throws CRUDException{
		String pr="1";
		List<String> spcodeList=null;
		List<String> spnameList=null;
		try{
			if(null!=h_sp_code && !"".equals(h_sp_code)){
				spcodeList=Arrays.asList(h_sp_code.split(","));
				spnameList=Arrays.asList(h_sp_name.split(","));
				sppriceSaleDemo.setBdat(new Date());
				sppriceSaleDemo.setEdat(new Date());
				sppriceSaleDemo.setPrice(Double.parseDouble("0"));
			}else{
				spcodeList=Arrays.asList(sppriceSaleDemo.getSupply().getSp_code().split(","));
				spnameList=Arrays.asList(sppriceSaleDemo.getSupply().getSp_name().split(","));
			}
			for (int i = 0; i < spcodeList.size(); i++) {
				Supply supply=new Supply();
				supply.setSp_code(spcodeList.get(i));
				supply.setSp_name(spnameList.get(i));
				sppriceSaleDemo.setSupply(supply);
				if(sppriceSaleDemoMapper.findSppriceSaleDemo(sppriceSaleDemo).size()!=0){
					pr="1";//已存在
				}else{
					sppriceSaleDemoMapper.saveByAddSppriceSaleDemo(sppriceSaleDemo);
					pr="2";//添加成功
				}
			}
			return pr;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 保存修改
	 */
	public void saveByUpdateSppriceSaleDemo(SppriceSaleDemo sppriceSaleDemo) throws CRUDException{
		try {
			sppriceSaleDemoMapper.updateSppriceSaleDemo(sppriceSaleDemo);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 批量添加售价单模板
	 * @param sppriceSaleDemo
	 */
	public String saveByAddSppriceSaleDemoMore(SppriceSaleDemo sppriceSaleDemo,String h_sp_code,String h_sp_name) throws CRUDException{
		List<String> spcodeList=null;
		List<String> spnameList=null;
		Map<String,Object> result=new HashMap<String, Object>();
		try{
			if(h_sp_code!=null && !"".equals(h_sp_code) && h_sp_name!=null &&!"".equals(h_sp_name)){
				spcodeList=Arrays.asList(h_sp_code.split(","));
				spnameList=Arrays.asList(h_sp_name.split(","));
				sppriceSaleDemo.setBdat(new Date());
				sppriceSaleDemo.setEdat(new Date());
				sppriceSaleDemo.setPrice(Double.parseDouble("0"));
				for (int i = 0; i < spcodeList.size(); i++) {
					Supply supply=new Supply();
					supply.setSp_code(spcodeList.get(i));
					supply.setSp_name(spnameList.get(i));
					sppriceSaleDemo.setSupply(supply);
					if(sppriceSaleDemoMapper.findSppriceSaleDemo(sppriceSaleDemo).size()!=0){
						result.put("pr", "1");//已存在
					}else{
						sppriceSaleDemoMapper.saveByAddSppriceSaleDemo(sppriceSaleDemo);
						result.put("pr", "2");//添加成功
					}
				}
			}else{
				result.put("pr", "3");//无数据
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 保存新增售价单
	 * @param sppriceSale
	 * @throws CRUDException
	 */
	public String saveByAddSppriceSale(Map<String,Object> map) throws CRUDException{
		List<String> firmList=Arrays.asList(map.get("firm").toString().split(","));
		SppriceSaleDemoList sppriceSaleDemoList=(SppriceSaleDemoList) map.get("sppriceSaleDemoList");
		Map<String,Object> result=new HashMap<String, Object>();
		try{
			for (int j = 0; j < firmList.size(); j++) {
				for (int i = 0; i < sppriceSaleDemoList.getSppriceSaleDemoList().size(); i++) {
					SppriceSale ss=new SppriceSale();
					Supply supply=new Supply();
					Positn area=new Positn();
					ss.setAcct(map.get("acct").toString());
					ss.setEmp(map.get("emp").toString());
					supply.setSp_code(sppriceSaleDemoList.getSppriceSaleDemoList().get(i).getSupply().getSp_code());
					ss.setSupply(supply);
					ss.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					area.setCode(firmList.get(j));
					ss.setArea(area);
					ss.setPrice(sppriceSaleDemoList.getSppriceSaleDemoList().get(i).getPrice());
					ss.setPriceold(sppriceSaleDemoList.getSppriceSaleDemoList().get(i).getPriceold());
					ss.setBdat(sppriceSaleDemoList.getSppriceSaleDemoList().get(i).getBdat());
					ss.setEdat(sppriceSaleDemoList.getSppriceSaleDemoList().get(i).getEdat());
					ss.setSta("N");
					Integer rec = sppriceSaleDemoMapper.getMaxRec(ss);
					if(null != rec){//说明用的oracle,反之 直接回存到ss的rec中wjf
						ss.setRec(rec.toString());
					}
					sppriceSaleDemoMapper.saveByAddSppriceSale(ss);
					result.put("pr", "2");
				}
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 批量更新--售价单模板
	 * @param sppriceSaleDemoList
	 * @throws Exception
	 */
	public void updateSppriceSaleDemoBatch(SppriceSaleDemoList sppriceSaleDemoList) throws Exception{
		try{
			for(SppriceSaleDemo sppriceSaleDemo : sppriceSaleDemoList.getSppriceSaleDemoList())
				sppriceSaleDemoMapper.updateSppriceSaleDemo(sppriceSaleDemo);
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}
	/**
	 * 批量更新--售价单
	 * @param sppriceSaleDemoList
	 * @throws Exception
	 */
	public void updateSppriceSaleBatch(SppriceSaleList sppriceSaleList) throws Exception{
		try{
			for(SppriceSale sppriceSale : sppriceSaleList.getSppriceSaleList()){
				sppriceSaleDemoMapper.updateSppriceSale(sppriceSale);
			}
		}catch(Exception e){
			throw new CRUDException(e);
		}
	}	

	/**
	 * 删除售价单模板
	 * @param codes
	 * @param types
	 * @throws CRUDException
	 */
	public String delete(List<String> spCodeList,List<String> firmList) throws CRUDException
	{
		Map<String,Object> result=new HashMap<String, Object>();
		try{
			for (int i = 0; i < spCodeList.size(); i++) {
				SppriceSaleDemo sppriceSaleDemo=new SppriceSaleDemo();
				Supply supply=new Supply();
				supply.setSp_code(spCodeList.get(i));
				sppriceSaleDemo.setSupply(supply);
				sppriceSaleDemo.setFirm(firmList.get(i));
				sppriceSaleDemoMapper.delete(sppriceSaleDemo);
				result.put("pr", "1");
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 删除未审核的报价
	 */
	public String deleteUncheckSpprice(SppriceSale sppriceSale) throws CRUDException
	{
		int succNum=0;
		int failNum=0;
		List<String> recList=Arrays.asList(sppriceSale.getRec().toString().split(","));
		List<String> areaList=Arrays.asList(sppriceSale.getArea().getCode().toString().split(","));
		Map<String,Object> result=new HashMap<String, Object>();
		try{
			for (int i = 0; i < recList.size(); i++) {
				sppriceSale.setRec(recList.get(i));
				Positn area=new Positn();
				area.setCode(areaList.get(i));
				sppriceSale.setArea(area);
				List<SppriceSale> list = sppriceSaleDemoMapper.findSppriceSale(sppriceSale);
				if(list.size()==0){
					failNum++;
				}else{
					sppriceSaleDemoMapper.deleteUncheckSpprice(sppriceSale);
					succNum++;
				}
				result.put("succNum", succNum);
				result.put("failNum", failNum);
				result.put("pr", "1");
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 审核售价单
	 * @param sppriceSale
	 * @return
	 * @throws CRUDException
	 */
	public String checkSppriceSale(SppriceSale sppriceSale,String type) throws CRUDException
	{
		List<String> recList=Arrays.asList(sppriceSale.getRec().split(","));
		List<String> areaCodeList=Arrays.asList(sppriceSale.getArea().getCode().split(","));
		Map<String,Object> result=new HashMap<String, Object>();
		try{
			for (int i = 0; i < recList.size(); i++) {
				SppriceSale ss=new SppriceSale();
				Positn area=new Positn();
				ss.setRec(recList.get(i));
				area.setCode(areaCodeList.get(i));
				ss.setArea(area);
				if(null!=type && !"".equals(type) && "uncheck".equals(type)){
					ss.setSta("uncheck");
				}else{
					ss.setSta("check");
					ss.setChecby(sppriceSale.getChecby());
					ss.setChect(sppriceSale.getChect());
				}
				sppriceSaleDemoMapper.checkSppriceSale(ss);
				result.put("pr", "1");
			}
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 全部反审核所有的售价
	 * @param sppriceSale
	 * @return
	 * @throws CRUDException
	 */
	public String reverseCheckSppriceSale(SppriceSale sppriceSale) throws CRUDException{
		try {
			Map<String,Object> result=new HashMap<String, Object>();
			sppriceSaleDemoMapper.reverseCheckSppriceSale(sppriceSale);
			result.put("pr", "1");
			return JSONObject.fromObject(result).toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 全部审核
	 */
	public String checkAll(SppriceSale sppriceSale) throws CRUDException
	{
		Map<String,Object> result=new HashMap<String, Object>();
		try{
			//优化这个方法wjf2014.11.25
			sppriceSaleDemoMapper.checkAllSppriceSale(sppriceSale);
//			List<SppriceSale> list=findSppriceSaleNoPage(sppriceSale);
//			if(list.size()!=0){
//				for (int i = 0; i < list.size(); i++) {
//					SppriceSale ss=new SppriceSale();
//					Positn area=new Positn();
//					ss.setRec(list.get(i).getRec());
//					area.setCode(list.get(i).getArea().getCode());
//					ss.setArea(area);
//					ss.setSta("check");
//					ss.setChecby(sppriceSale.getChecby());
//					ss.setChect(sppriceSale.getChect());
//					sppriceSaleDemoMapper.checkSppriceSale(ss);
//				}
//			}
			result.put("pr", "1");
			JSONObject rs = JSONObject.fromObject(result);
			return rs.toString();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 物资售价综合查询
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public List<SppriceSale> findSppriceSaleByReport(SppriceSale sppriceSale,String nodate,Page page) throws CRUDException{
		try {
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("sppriceSale", sppriceSale);
			map.put("nodate", nodate);
			return sppriceSalePageManager.selectPage(map, page, SppriceSaleDemoMapper.class.getName()+".findSppriceSaleByReport");
//			return sppriceSaleDemoMapper.findSppriceSale(sppriceSale);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 物资售价综合查询--查询所有的售价用于全部反审核
	 * @param conditions
	 * @param pager
	 * @return
	 */
	public List<SppriceSale> findSppriceSaleByReport1(SppriceSale sppriceSale,String nodate) throws CRUDException{
		try {
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("sppriceSale", sppriceSale);
			map.put("nodate", nodate);
			return sppriceSaleDemoMapper.findSppriceSaleByReport(map);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 下载模板信息 wjf
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response, HttpServletRequest request) throws IOException {
		PublicExportExcel.downloadTemplate(response, request, "售价单.xls"); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 将文件上传到temp文件夹下wjf
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return PublicExportExcel.uploadToTemp(request, response); //改为调用公用方法  by lbh 2017-07-07
	}
	
	/**
	 * 对execl进行验证
	 */
	public List<String> check(String path,String accountId,String acct,String madeby) throws CRUDException {
		boolean checkResult = ReadSppriceSaleExcel.check(accountId,path,supplyMapper,positnMapper);
		List<String> errorList = new ArrayList<String>();
		if (checkResult) {
			List<SppriceSale> sppriceSaleList = ReadSppriceSaleExcel.sppriceSaleList;
			//将读出来的数据插进表 变成未审核的
			for(SppriceSale spriceSale : sppriceSaleList){
				//将门店取出来
				List<Positn> positns = spriceSale.getPositnList();
				if(positns.size() > 0){
					for(Positn positn :positns){
						SppriceSale ss=new SppriceSale();
						ss.setAcct(acct);
						ss.setEmp(madeby);
						ss.setSupply(spriceSale.getSupply());
						ss.setMadet(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						ss.setArea(positn);
						ss.setPrice(spriceSale.getPrice());
						ss.setPriceold(0.0);//先设成0，，，，，等等再赋值
						ss.setBdat(spriceSale.getBdat());
						ss.setEdat(spriceSale.getEdat());
						ss.setSta("N");
						Integer rec = sppriceSaleDemoMapper.getMaxRec(ss);
						if(null != rec){//说明用的oracle,反之 直接回存到ss的rec中wjf
							ss.setRec(rec.toString());
						}
						sppriceSaleDemoMapper.saveByAddSppriceSale(ss);
					}
				}
			}
		} else {
			errorList = ReadSppriceSaleExcel.errorList;
		}
		FileWorked.deleteFile(path);//删除上传后的的文件
		return errorList;
	}
}