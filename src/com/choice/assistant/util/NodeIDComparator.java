package com.choice.assistant.util;

import java.util.Comparator;

public class NodeIDComparator  implements Comparator<Object>  {
		// 按照节点编号比较
		public int compare(Object o1, Object o2) {
			String j1 = ((Node) o1).name;
			String j2 = ((Node) o2).name;
			return (j1.compareTo(j2)<0 ? -1 : (j1.compareTo(j2)==0 ? 0 : 1));
		}

}
