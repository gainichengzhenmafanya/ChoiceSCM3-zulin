package com.choice.assistant.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.choice.framework.exception.CRUDException;
/**
 * ztree数据构建工具类
 * @author ZGL
 * @Date 2014-11-09 15:36:11
 *
 */
public class ZtreeUtil {
	
	public static Node getZtreeList(List<Map<String,Object>> dataList) throws CRUDException{
		// 根节点
		Node root = null;
		try{
		// 节点列表（映射表，用于临时存储节点对象）
		HashMap<String,Object> nodeList = new HashMap<String,Object>();
		// 将结果集存入映射表（后面将借助映射表构造多叉树）
		for (Iterator<Map<String, Object>> it = dataList.iterator(); it.hasNext();) {
			Map<String,Object> dataRecord = (Map<String, Object>) it.next();
			Node node = new Node();
			node.id = (String) dataRecord.get("id");
			node.name = (String) dataRecord.get("name");
			node.parentId = (String) dataRecord.get("parentId");
			node.sequence= dataRecord.get("sequence").toString();
			node.click= (String) dataRecord.get("click");
			nodeList.put(node.id, node);
		}
		// 构造无序的多叉树
		Set<Entry<String, Object>> entrySet = nodeList.entrySet();
		for (Iterator<Entry<String, Object>> it = entrySet.iterator(); it.hasNext();) {
			@SuppressWarnings("rawtypes")
			Node node = (Node) ((Map.Entry) it.next()).getValue();
			if (node.parentId == null || node.parentId.equals("")) {
				root = node;
			} else {
				if(nodeList.get(node.parentId) != null && !"".equals(nodeList.get(node.parentId))){
					((Node) nodeList.get(node.parentId)).addChild(node);
				}
			}
		}
		// 输出无序的树形结构的JSON字符串
//		System.out.println(root);
		// 对多叉树进行横向排序
		root.sortChildrenBySequence();
		// 输出有序的树形结构的JSON字符串
//		System.out.println(root);
		}catch(Exception e){
			System.out.println(e);
			throw new CRUDException(e);
		}
		return root;
	}
}
