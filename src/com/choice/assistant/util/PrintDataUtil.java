package com.choice.assistant.util;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

public class PrintDataUtil {

	/**
	 * 打印
	 * @param modelMap
	 * @param request
	 * @param session
	 * @param domainClass --主表实体类
	 * @param type --报表展示类型
	 * @param reportName --报表名称
	 * @param listResult --子表list
	 * @param ptintUrl --ireport文件路径
	 * @return
	 * @throws Exception
	 */
	public static ModelAndView printData(ModelMap modelMap,HttpServletRequest request,HttpSession session,Map<String,Object> mapRes,
			String type,	String reportName,List<?> listResult,String ptintUrl) throws Exception{
		List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(listResult);
		//设置分页，查询所有数据
		modelMap.put("actionMap", mapRes);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
	 	Map<String,Object>  parameters = mapRes;
	    parameters.put("report_name", reportName);
	    modelMap.put("report_name", reportName);//导出excel、pdf用到了
	    parameters.put("madeby", session.getAttribute("accountName").toString());
        modelMap.put("parameters", parameters);//ireport表头参数
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",dataList);
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,ptintUrl);//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 打印-Listmap
	 * @param modelMap
	 * @param request
	 * @param session
	 * @param domainClass --主表实体类
	 * @param type --报表展示类型
	 * @param reportName --报表名称
	 * @param listResult --子表list
	 * @param ptintUrl --ireport文件路径
	 * @return
	 * @throws Exception
	 */
	public static ModelAndView printDataMap(ModelMap modelMap,HttpServletRequest request,HttpSession session,Map<String,Object> mapRes,
			String type,	String reportName,List<Map<String,Object>> listResult,String ptintUrl) throws Exception{
		//设置分页，查询所有数据
		modelMap.put("actionMap", mapRes);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
		Map<String,Object>  parameters = mapRes;
		parameters.put("report_name", reportName);
	    modelMap.put("report_name", reportName);//导出excel、pdf用到了
		parameters.put("madeby", session.getAttribute("accountName").toString());
		modelMap.put("parameters", parameters);//ireport表头参数
		modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",listResult);
		Map<String,String> rs = ReadReportUrl.redReportUrl(type,ptintUrl);//判断跳转路径
		modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
}
