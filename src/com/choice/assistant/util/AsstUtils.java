package com.choice.assistant.util;

import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.choice.assistant.domain.util.AuthMall;
import com.choice.assistant.util.web.AESEncrypt;
import com.choice.assistant.util.web.MallAssitUtil;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.MD5;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.scm.domain.Acct;

public class AsstUtils {
	private static Logger log = Logger.getLogger(AsstUtils.class);
	public static String selectbeforedate="",formatLen="",assitWebserviceHttp="";
	public static String updatefilename="",currversion="";
	public static String asst_to_jmu_basis_time="",asst_to_jmu_basis_interval="",asst_to_jmu_order_time="",asst_to_jmu_order_interval="";
	public static int threadSize=3;//每次创建的线程条数
	public static Map<String,Object> itfMap = new HashMap<String,Object>();//存储接口登录参数{"acct":"AssistItfDomain"}
	public static String errmsg = "";//接口登录错误信息
	public static String emailHost = "",sendEmail="",emailPwd = "";
	private static Random randGen = null;
	private static char[] numbersAndLetters = null;
	public static String ftpIp="",ftpPort="",ftpUserName="",ftpUserPwd="";
	public static String isruning="0";
	
	/**
	 * 平台authSalt
	 */
	public static String authSalt="534589a768a089c1b1a062cbdd455fef";
	/**
	 * 平台通信秘钥
	 */
	public static String authKey="f3647526e596c5b309f105550cc13ded";
	/**
	 * ERP通信秘钥
	 */
	public static String authERPKey="73c94d7264f741a8a11d279f22edba66";
	
	static{
		ReadProperties rp = new ReadProperties();
		//是否可以选择以前的日期0-不可以、1-可以
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("selectbeforedate"))){
			selectbeforedate = rp.getStrByParam("selectbeforedate");
		}
		//商城接口地址
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("assitWebserviceHttp"))){
			assitWebserviceHttp = rp.getStrByParam("assitWebserviceHttp");
		}
		//发送邮件的邮箱smtp主机
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("emailHost"))){
			emailHost = rp.getStrByParam("emailHost");
		}
		//发送邮件的邮箱
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("sendEmail"))){
			sendEmail = rp.getStrByParam("sendEmail");
		}
		//发送邮件的邮箱密码
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("emailPwd"))){
			emailPwd = rp.getStrByParam("emailPwd");
		}
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("threadSize"))){
			try{
				threadSize = Integer.parseInt(rp.getStrByParam("threadSize"));
			}catch (Exception e) {
				threadSize = 3;
			}
		}
		//服务器FTP IP
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("ftpIp"))){
			ftpIp = rp.getStrByParam("ftpIp");
		}
		//服务器FTP端口
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("ftpPort"))){
			ftpPort = rp.getStrByParam("ftpPort");
		}
		//服务器FTP用户名
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("ftpUserName"))){
			ftpUserName = rp.getStrByParam("ftpUserName");
		}
		//服务器FTP密码
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("ftpUserPwd"))){
			ftpUserPwd = rp.getStrByParam("ftpUserPwd");
		}
		//--------------------------server start-------------------------------------
		//更新文件名
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("updatefilename"))){
			updatefilename = rp.getStrByParam("updatefilename");
		}
		//当前版本号
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("currversion"))){
			currversion = rp.getStrByParam("currversion");
		}
		//基础资料同步时间段
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("asst_to_jmu_basis_time"))){
			asst_to_jmu_basis_time = rp.getStrByParam("asst_to_jmu_basis_time");
		}
		//基础资料同步时间间隔（分钟）
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("asst_to_jmu_basis_interval"))){
			asst_to_jmu_basis_interval = rp.getStrByParam("asst_to_jmu_basis_interval");
		}
		//订单信息同步时间段
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("asst_to_jmu_order_time"))){
			asst_to_jmu_order_time = rp.getStrByParam("asst_to_jmu_order_time");
		}
		//订单信息同步时间间隔（分钟）
		if(ValueCheck.IsNotEmpty(rp.getStrByParam("asst_to_jmu_order_interval"))){
			asst_to_jmu_order_interval = rp.getStrByParam("asst_to_jmu_order_interval");
		}
		//--------------------------server end-------------------------------------
	}
	
	/**
	 * 格式化数字
	 * @param num  需要格式化的数字
	 * @param len  格式化的长度:<0 为默认2;  >=0 为格式化的小数位数
	 * @return
	 */
	public static String formatDoubleLength(String num,int len){
		Double formatNum = 0.00;
		if(ValueCheck.IsEmpty(num)){
			num = "0";
		}
		try{//如果输入的字符是数字则转换，不是直接返回该字符串
			formatNum=Double.valueOf(num);
		}catch (Exception e) {
			log.error(e);
			return num;
		}
		String result = "";//存放最终格式化后的数字
		String model="#0";//数字格式
		int length=2;//数字格式的小数位数
		if(len>=0){//判断如果输入的长度>=0则将长度修改<0则去系统配置的长度参数名：formatLen
			length = len;
		}else{
			if(ValueCheck.IsNotEmpty(formatLen)){
				try{
					length=Integer.parseInt(formatLen);
				}catch (Exception e) {
					log.error(e);
				}
			}
		}
		if(length>1){
			model+=".";
		}
		//循环小数位数格式
		for(int i=0;i<length;i++){
			model+="0";
		}
		DecimalFormat df = new DecimalFormat(model);
		df.setRoundingMode(RoundingMode.HALF_UP);//设置五入 默认为HALF_EVEN 2，4，6，8的不进位
		result = df.format(formatNum);
		return result;
	}
	/**
	 * 格式化list集合中的数字（将空值格式化为0.00）
	 * @param listResult
	 * @param cols
	 * @param len
	 * @return
	 * @author zgl
	 * @Date 2014-03-03 10:06:35
	 */
	public static List<Map<String,Object>> formatNumForListResult(List<Map<String,Object>> listResult,String cols,int len){
		String num = "0";
		if(len>0){
			num +=".";
			for(int i=0;i<len;i++){
				num += "0";
			}
		}
		if(listResult != null && listResult.size()>0){
			String[] colsList = cols.split(",");
			for(Map<String,Object> map : listResult){
				//循环map并取里面的值
				for(String col : colsList){
					map.put(col,null==map.get(col)?num:formatDoubleLength(map.get(col).toString(),len));
				}
			}
		}
		return listResult;
	}
	/**
	 * 格式化list集合中的数字(忽略空值,不格式化空值)
	 * @param listResult
	 * @param cols
	 * @param len
	 * @return
	 * @author zgl
	 * @Date 2014-03-03 10:06:35
	 */
	public static List<Map<String,Object>> formatNumForListResultIgnoreNull(List<Map<String,Object>> listResult,String cols,int len){
		if(listResult != null && listResult.size()>0){
			String[] colsList = cols.split(",");
			for(Map<String,Object> map : listResult){
				//循环map并取里面的值
				for(String col : colsList){
					map.put(col,null==map.get(col)?"":formatDoubleLength(map.get(col).toString(),len));
				}
			}
		}
		return listResult;
	}
	/**
	 * 数字与字符类型的数字相加
	 * @param t
	 * @param obj
	 * @return
	 */
	public static Double stringPlusDouble(Object pl,Object obj){
		Double t = 0.00;
		try{
			String model="#0.00";//数字格式
			DecimalFormat df = new DecimalFormat(model);
			df.setRoundingMode(RoundingMode.HALF_UP);//设置五入 默认为HALF_EVEN 2，4，6，8的不进位
			if(ValueCheck.IsNotEmpty(pl)){
				t = Double.parseDouble(pl.toString());
			}
			if(ValueCheck.IsNotEmpty(obj)){
				t = t+Double.parseDouble(obj.toString());
			}
			t=Double.parseDouble(df.format(t));
		}catch(Exception e){
			log.error(e);
		}
		return t;
	}
	/**
	 * 数字与字符类型的数字相除
	 * @param t
	 * @param obj
	 * @return
	 */
	public static String dividedNum(Object pl,Object obj,int len){
		Double t = 0.00;
		try{
			if(ValueCheck.IsNotEmpty(pl)){
				t = Double.parseDouble(pl.toString());
			}
			if(ValueCheck.IsNotEmpty(obj)){
				if(Double.parseDouble(pl.toString())==0){
					return formatDoubleLength(t.toString(),len);
				}
				//计算
				if(!obj.equals("0")){
					t = t/Double.parseDouble(obj.toString());
				}
			}
		}catch(Exception e){
			log.error(e);
		}
		return formatDoubleLength(t.toString(),len);
	}
	/**
	 * 数字与字符类型的数字相除 以百分比型式返回
	 * @param t
	 * @param obj
	 * @return
	 */
	public static String dividedNumBfb(Object pl,Object obj,int len){
		Double t = 0.00;
		try{
			if(ValueCheck.IsNotEmpty(pl)){
				t = Double.parseDouble(pl.toString());
			}
			if(ValueCheck.IsNotEmpty(obj)){
				if(Double.parseDouble(pl.toString())==0){
					return formatDoubleLength(t.toString(),len)+"%";
				}
				//计算
				if(!obj.equals("0")){
					t = t/Double.parseDouble(obj.toString())*100;
				}
			}
		}catch(Exception e){
			log.error(e);
		}
		return formatDoubleLength(t.toString(),len)+"%";
	}
	/**
	 * 数字与字符类型的数字相减
	 * @param t
	 * @param obj
	 * @return
	 */
	public static String subtractNum(Object pl,Object obj,int len){
		Double t = 0.00;
		try{
			if(ValueCheck.IsNotEmpty(pl)){
				t = Double.parseDouble(pl.toString());
			}
			if(ValueCheck.IsNotEmpty(obj)){
				t = t-Double.parseDouble(obj.toString());
			}
		}catch(Exception e){
			log.error(e);
		}
		return formatDoubleLength(t.toString(),len);
	}

	/**
	 * 将“,”替换为“','”并且在字符串两端加“'”
	 * @param param
	 * @return
	 */
	public static String StringCodeReplace(String param){
		if(ValueCheck.IsEmpty(param)) {
			return ""; 
		}else{
			//处理字符串前先将”','“替换成“,”;
			param=param.replace("'", "");
			return "'"+param.replace(",", "','")+"'";
		}
	}
	/**
	 * webservice权限验证
	 * @param md5key
	 * @param pk_group
	 * @param dateStr
	 * @return
	 */
	public static Boolean checkmd5keyForWebservice(String md5key,String pk_group,String dateStr){
		if(ValueCheck.IsEmpty(md5key)){
			return false;
		}else{
			if(md5key.equals(MD5.md5((dateStr+"@"+pk_group)))){
				return true;
			}
		}
		return false;
	}
	/**
	 * 获取公用验证字符串
	 * @param authMall
	 * @return
	 * @throws CRUDException
	 */
	public static String getAsstAuth(AuthMall authMall) throws CRUDException{
		String authStr = "";
		try{
			if(ValueCheck.IsNotEmpty(authMall)){
				authMall.setCrtTime(String.valueOf(System.currentTimeMillis()/1000));
				authMall.setToken(MD5.md5(authSalt+(authMall.getSessionId()==null?"":authMall.getSessionId())+authMall.getCrtTime()));
				authStr = "\"auth.token\":\""+authMall.getToken()+"\"," +
						"\"auth.sessionId\":\""+(authMall.getSessionId()==null?"":authMall.getSessionId())+"\"," +
						"\"auth.platformId\":\""+authMall.getPlatformId()+"\"," +
						"\"auth.userId\":\""+(authMall.getUserId()==null?"":authMall.getUserId())+"\"," +
						"\"auth.crtTime\":\""+authMall.getCrtTime()+"\"";

			}
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return authStr;
	}
	
	/**
	 * 获取当前登录信息，如果没有登录就先登录然后将登录信息拼接为验证参数
	 * @param company
	 * @return
	 * @throws CRUDException
	 */
	public static String getParams(Acct acct)throws CRUDException{
        String params = "";
		try {
			AsstUtils.errmsg = "";//将错误信息置空
			AuthMall authMall = new AuthMall();
			//如果没有登录，调用登录接口
	        if(!MallAssitUtil.getAsstSession(acct)){
	        	
	        	AssistItfDomain assistItfDomain = new AssistItfDomain();
				//如果获取到登录接口要用的信息
				if(ValueCheck.IsNotEmpty(AsstUtils.itfMap.get(acct.getCode()))){
					assistItfDomain = (AssistItfDomain)AsstUtils.itfMap.get(acct.getCode());
				}else{//如果没有获取到登录接口要用的信息，将登录用户名、密码放到缓存中，登录商城接口用
					if(ValueCheck.IsEmpty(acct.getJmuusername()) || ValueCheck.IsEmpty(acct.getJmupassword())){
						System.out.println("===================接口登录名、密码为空。");
						AsstUtils.errmsg="接口登录名、密码为空。";
						return null;
					}
					assistItfDomain.setAsstUserName(acct.getJmuusername());
					assistItfDomain.setAsstPas(acct.getJmupassword());
				}
				String res = MallAssitUtil.login(authMall, assistItfDomain.getAsstUserName(),assistItfDomain.getAsstPas(), acct);
				if(ValueCheck.IsNotEmpty(res)){
					System.out.println("===================登录失败");
					return null;
				}
	        }
	        if(ValueCheck.IsEmpty(AsstUtils.itfMap.get(acct.getCode()))){
	        	return null;
	        }
	        if(ValueCheck.IsEmpty(((AssistItfDomain)AsstUtils.itfMap.get(acct.getCode())).getAsstSessionId())){
	        	System.out.println("=============================没有登录");
	        	return null;
	        }
	        AssistItfDomain assistItf = (AssistItfDomain)AsstUtils.itfMap.get(acct.getCode());
	    	authMall.setSessionId(assistItf.getAsstSessionId());
	    	authMall.setUserId(assistItf.getAsstUserId());
//	    	authMall.setCrtTime(System.currentTimeMillis());
	        params += AsstUtils.getAsstAuth(authMall);
	        return params;
		} catch (CRUDException e) {
			log.error(e);
			e.printStackTrace();
		}
		return params;
	}
	
	/**
	 * 产生随机字符串
	 * @param length
	 * @return
	 */
	public static final String randomString(int length) {
         if (length < 1) {
             return null;
         }
         if (randGen == null) {
                randGen = new Random();
                numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz" +
                   "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                 }
         char [] randBuffer = new char[length];
         for (int i=0; i<randBuffer.length; i++) {
             randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
         }
         return new String(randBuffer);
	}
	public static void main(String[] args) throws UnsupportedEncodingException, CRUDException {
		String p = "FD433E507E2CF9E7F4D8A4D04A702F142C75F07689240C6441B47D61F75021C562E9E45680822DE068F84FCC9E6CFA3019EBF4E3244AEB1A730AFE649B1D654D2FCD11275725747838302B485225EBE4BD151ABCE2B921EB999C5094FFCC757B7F038C4F613F11CE8F1AE13FE83B9A35C6E7FBA3E9E69A7D5854B3A89E076A72756ADBC2AB2666B5A3829B15CF247E4527C20B07102F897A40C89374264CBD46636DDBBF9AF5FFC33C2364B4C61A0967DDE928355A8EE9E1EC8F7467D8BFE954";
		byte[] decryptFrom = AESEncrypt.parseHexStr2Byte(p);  
		byte[] decryptResult  = AESEncrypt.decrypt(decryptFrom, authKey);
//		byte[] decryptResult  = AESEncrypt.decrypt(decryptFrom,  "69a79f8fd09b4b1f936fc557d8188b06");
		p = new String(decryptResult,"utf-8");
//		Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
// 		childrenClassMap.put("data", ResultBillInfoData.class);
// 		childrenClassMap.put("goods", Map.class);
// 		AssitBillInfoData assitBillInfoData = null;
//			assitBillInfoData = (AssitBillInfoData)JsonFormatUtil.formatAggObjectJson(str, AssitBillInfoData.class,childrenClassMap);
			
		JSONObject json=JSONObject.fromObject(p);
//
		System.out.println(json);
//		byte[] bits = AESEncrypt.encrypt(str,  "2c36da08301a4e61a170fccfd04dc529");
//		//将2进制转换为16进制
//		String encryptResultStr = AESEncrypt.parseByte2HexStr(bits);  
//		System.out.println(encryptResultStr);
//
//		Map<String,String> stateMap = new HashMap<String,String>();
//		stateMap.put("0", "新建,1");
//		stateMap.put("100", "待付款,2");
//		stateMap.put("200", "待收货(待备货),2");
//		stateMap.put("210", "待收货(备货中),2");
//		stateMap.put("220", "待收货(备货完成),2");
//		stateMap.put("230", "待收货(已发货),3");
//		stateMap.put("300", "已签收,4");
//		stateMap.put("350", "已签收 (返修/退换货中),6");
//		stateMap.put("900", "已完成,7");
//		stateMap.put("400", "已取消,-1");
//		
//		System.out.println(stateMap.get("400"));
//		Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
// 		childrenClassMap.put("data", ResultBillInfoData.class);
// 		childrenClassMap.put("order", AssitBillInfoOrder.class);
// 		childrenClassMap.put("spec", AssitBillInfoSpec.class);
// 		childrenClassMap.put("goods", Map.class);
// 		AssitBillInfoData assitBillInfoData = null;
//		try {
//			//解析返回的数据
//			assitBillInfoData = (AssitBillInfoData)JsonFormatUtil.formatAggObjectJson(str, AssitBillInfoData.class,childrenClassMap);
//		} catch (CRUDException e) {
//			log.error(e);
//			e.printStackTrace();
//		}
	}
}
