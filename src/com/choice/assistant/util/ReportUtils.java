package com.choice.assistant.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.choice.assistant.constants.report.ReportStringConstant;
import com.choice.assistant.domain.common.ReportObject;

@Component
public class ReportUtils implements ApplicationContextAware{

	@Resource
	private ApplicationContext context;
	
	//将bean类中非static，非空属性放入map并返回
	public static<T> Map<String,String> convertToMap(T obj) throws Exception{
		Map<String,String> result = new HashMap<String,String>();
		Class<?> objClass = obj.getClass();
		Field[] fields = objClass.getDeclaredFields();
		for(Field f : fields){
			if((f.getModifiers()&Modifier.STATIC) > 0 || f.isAnnotationPresent(NoUse.class))continue;
			boolean b = f.isAccessible();
			if(!b)f.setAccessible(true);
			if(null != f.get(obj)){
				Object cur = f.get(obj);
				if("java.util.Date".equals(cur.getClass().getName())){
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					result.put(f.getName(), sdf.format(cur));
				}else
					result.put(f.getName(), f.get(obj).toString());
			}
			f.setAccessible(b);
		}
		return result;
	}
	/**
	 * 动态执行类中的方法
	 * @param condition
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ReportObject<Map<String,Object>> execMethod(String className,String reportName,Object... condition){
		ReportObject<Map<String,Object>> result = null;
		int len = condition.length;
		Class<?>[] params = new Class[condition.length];
		for(int i = 0 ; i < len ; i ++){
			params[i] = condition[i].getClass();
		}
		String method = ReadReportConstants.getReportMethod(ReportStringConstant.class, reportName);
		try {
			Method m = Class.forName(className).getMethod(method,params);
			String serviceSpringName = className.substring(className.lastIndexOf(".")+1, className.length());
			Object service = context.getBean(serviceSpringName);
			result =  (ReportObject<Map<String, Object>>) m.invoke(service, condition);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@Override
	public void setApplicationContext(ApplicationContext c) throws BeansException {
			context = c;
	}
}
