package com.choice.assistant.util.web;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

import com.choice.assistant.domain.util.AssitResult;
import com.choice.assistant.domain.util.AssitResultForOne;
import com.choice.assistant.domain.util.AuthMall;
import com.choice.assistant.domain.util.PageCondition;
import com.choice.assistant.domain.util.ResultData;
import com.choice.assistant.domain.util.asstBillList.AssitBillData;
import com.choice.assistant.domain.util.asstBillList.AssitBillResult;
import com.choice.assistant.domain.util.asstBillList.ResultBillData;
import com.choice.assistant.domain.util.asstGoods.AssitResultGoods;
import com.choice.assistant.domain.util.asstGoods.AssitResultHspec;
import com.choice.assistant.domain.util.asstGoods.AsstGoods;
import com.choice.assistant.domain.util.asstGoods.AsstHspec;
import com.choice.assistant.domain.util.asstGoods.AsstStore;
import com.choice.assistant.domain.util.asstGoods.ResultDataGoods;
import com.choice.assistant.domain.util.asstGoods.ResultDataHspec;
import com.choice.assistant.domain.util.asstSupplier.AssitSupplierResult;
import com.choice.assistant.domain.util.asstSupplier.AsstSupCompany;
import com.choice.assistant.domain.util.asstSupplier.ResultSupplierData;
import com.choice.assistant.domain.util.baseList.AssitResultList;
import com.choice.assistant.domain.util.baseList.ResultDataList;
import com.choice.assistant.domain.util.uploadBill.AssitUploadBillData;
import com.choice.assistant.domain.util.uploadBill.AssitUploadBillResult;
import com.choice.assistant.domain.util.uploadBill.ResultOrder;
import com.choice.assistant.domain.util.uploadBill.UploadResultBillData;
import com.choice.assistant.util.AssistItfDomain;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.LogUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Acct;

@Controller
public class MallAssitUtil {
	
	private final transient static Log log = LogFactory.getLog(MallAssitUtil.class);
    private final transient static Logger logger = Logger.getLogger(MallAssitUtil.class);
	/**
	 * 登录方法
	 * @param authMall
	 * @param userName
	 * @param password
	 * @return
	 * @throws CRUDException
	 */
	public static String login(AuthMall authMall,String userName,String password,Acct acct) throws CRUDException{
		AssitResultForOne assitResult = new AssitResultForOne();
		try{
			String params="{";
			params += AsstUtils.getAsstAuth(authMall);
			params += ",\"userName\":\""+userName+"\"," +
					"\"password\":\""+password+"\"}";
			//加密
			byte[] bits = AESEncrypt.encrypt(params, AsstUtils.authKey);
			//将2进制转换为16进制
			String encryptResultStr = AESEncrypt.parseByte2HexStr(bits);  
			params = "paramData="+encryptResultStr;
			//调用接口方法
			String resultStr = CallWebService.httpCallWebService("account/login.action", params);
			//将16进制转为2进制以便解密
			byte[] decryptFrom = AESEncrypt.parseHexStr2Byte(resultStr);  
			//解密
			byte[] decryptResult = AESEncrypt.decrypt(decryptFrom, AsstUtils.authKey);
			//将字节转为字符串
			String str = new String(decryptResult,"utf-8");
			JSONObject json=JSONObject.fromObject(str);
			System.out.println("=========================登录错误信息errmsg="+json.get("errmsg"));
			logger.error(DateFormat.getTs()+"登录错误信息errmsg="+json.get("errmsg"));
			if(ValueCheck.IsNotEmpty(json.get("errmsg"))){//如果登录失败，返回失败描述信息
				System.out.println(DateFormat.getTs()+"===================接口登录失败==="+json.get("errmsg"));
//				if("username or password error".indexOf(AsstUtils.errmsg)>-1){
					AsstUtils.errmsg = "登录商城接口时登录名或密码错误。";
//					throw new CRUDException(new Exception("=========="+AsstUtils.errmsg));
//				}
				return AsstUtils.errmsg;
			}
			//转为字符串
			assitResult = formatResult(str);
			System.out.println("=============登录返回信息========================="+str);
			//如果登录成功，将登录信息更新到缓存中
			AssistItfDomain assistItf = new AssistItfDomain();
			if(ValueCheck.IsNotEmpty(assitResult) && "000000".equals(assitResult.getCode())){
				if(ValueCheck.IsNotEmpty(AsstUtils.itfMap.get(acct.getCode()))){
					assistItf = (AssistItfDomain)AsstUtils.itfMap.get(acct.getCode());
				}else{
					assistItf.setAsstUserName(userName);
					assistItf.setAsstPas(password);
				}
				assistItf.setAsstSessionId(assitResult.getData().get("sessionId").toString());
				assistItf.setAsstUserId(assitResult.getData().get("userId").toString());
				AsstUtils.itfMap.put(acct.getCode(), assistItf);
//				session.setAttribute("asstSessionId", assitResult.getData().get("sessionId"));
//				session.setAttribute("asstUserId", assitResult.getData().get("userId"));
			}
		}catch(Exception e) {
			e.printStackTrace();
			log.error(e+"=========="+assitResult.getErrmsg());
		}
		return null;
	}
	/**
	 * 格式化接口返回结果-list多条数据
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitResult formatResultList(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", ResultData.class);
			childrenClassMap.put("result", Map.class);
			childrenClassMap.put("order", Map.class);
			childrenClassMap.put("pageCondition", PageCondition.class);
			AssitResult assitResult = (AssitResult)JsonFormatUtil.formatAggObjectJson(resultStr, AssitResult.class,childrenClassMap);
//			if("session invalid".equals(assitResult.getErrmsg())){
//				Company company = new Company();
//				login(new AuthMall(),"","",company);
//			}
			return assitResult;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 格式化接口返回结果-商品列表
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitResultGoods formatGoodsResultList(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", ResultDataGoods.class);
			childrenClassMap.put("result", AsstGoods.class);
			childrenClassMap.put("store", AsstStore.class);
			childrenClassMap.put("seller", Map.class);
			childrenClassMap.put("pageCondition", PageCondition.class);
			AssitResultGoods assitResult = (AssitResultGoods)JsonFormatUtil.formatAggObjectJson(resultStr, AssitResultGoods.class,childrenClassMap);
			return assitResult;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 格式化接口返回结果-规格列表
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitResultHspec formatHspecResultList(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", ResultDataHspec.class);
			childrenClassMap.put("result", AsstHspec.class);
			childrenClassMap.put("goods", Map.class);
			childrenClassMap.put("store", Map.class);
			childrenClassMap.put("seller", Map.class);
			childrenClassMap.put("pageCondition", PageCondition.class);
			AssitResultHspec assitResult = (AssitResultHspec)JsonFormatUtil.formatAggObjectJson(resultStr, AssitResultHspec.class,childrenClassMap);
			return assitResult;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 格式化接口返回结果-list多条数据-供应商
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitSupplierResult formatSupplierResultList(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", ResultSupplierData.class);
			childrenClassMap.put("result", AsstSupCompany.class);
			childrenClassMap.put("company", Map.class);
			childrenClassMap.put("pageCondition", PageCondition.class);
			AssitSupplierResult assitSupplierResult = (AssitSupplierResult)JsonFormatUtil.formatAggObjectJson(resultStr, AssitSupplierResult.class,childrenClassMap);
			return assitSupplierResult;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 格式化接口返回结果-一条结果
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitResultForOne formatResult(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", Map.class);
			AssitResultForOne assitResult = (AssitResultForOne)JsonFormatUtil.formatAggObjectJson(resultStr, AssitResultForOne.class,childrenClassMap);
			return assitResult;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 调用webservice接口公用方法并格式化返回数据字符串
	 * @param method
	 * @param params
	 * @return
	 * @throws CRUDException
	 * @author ZGL
	 * @throws UnsupportedEncodingException 
	 * @Date 2014-11-02 21:38:58
	 */
	public static String getResult(String method,String params,Acct acct) throws CRUDException, UnsupportedEncodingException{
		try{
			if(ValueCheck.IsEmpty(AsstUtils.itfMap.get(acct.getCode()))){
				System.out.println("还未登录。");
				return null;
			}
			if(ValueCheck.IsEmpty(method)){
				System.out.println("调用方法为空。");
				return null;
			}
			if(ValueCheck.IsEmpty(params)){
				System.out.println("参数为空。");
				return null;
			}
			//加密
//			System.out.println("加密=============================================="+DateFormat.getTs());
			byte[] bits = AESEncrypt.encrypt("{"+params+"}", AsstUtils.authKey);
			//将2进制转换为16进制
			String encryptResultStr = AESEncrypt.parseByte2HexStr(bits);  
			params = "paramData="+encryptResultStr;
			//调用接口方法
			String resultStr = CallWebService.httpCallWebService(method, params);
			//将16进制转为2进制以便解密
			byte[] decryptFrom = AESEncrypt.parseHexStr2Byte(resultStr);  
			//解密
			//转为字符串
			String str= "";
			JSONObject json = new JSONObject();
			if(ValueCheck.IsNotEmpty(decryptFrom)){
				byte[] decryptResult  = AESEncrypt.decrypt(decryptFrom,  AsstUtils.authKey);
				str = new String(decryptResult,"utf-8");
				json=JSONObject.fromObject(str);
				LogUtil.writeToTxt(LogUtil.EXTERNALINTERFACECALL, "MallAssitUtil.class:"+new Throwable().getStackTrace()[0].getLineNumber()+"行,调用接口"+method+"出错，加密后参数"+params+"，返回错误信息："+json.get("errmsg"));
			}
			
			//如果检测到登录超时，再执行一次登录后调用本次查询接口
			if("session invalid".equals(json.get("errmsg"))){
				AssistItfDomain assistItfDomain = new AssistItfDomain();
				//获取登录参数
				if(ValueCheck.IsNotEmpty(AsstUtils.itfMap.get(acct.getCode()))){
					assistItfDomain = (AssistItfDomain)AsstUtils.itfMap.get(acct.getCode());
				}
				//登录接口
				String res = login(new AuthMall(),assistItfDomain.getAsstUserName(),assistItfDomain.getAsstPas(),acct);
				if(ValueCheck.IsEmpty(res)){
					LogUtil.writeToTxt(LogUtil.EXTERNALINTERFACECALL, "MallAssitUtil.class:"+new Throwable().getStackTrace()[0].getLineNumber()+"行,登录出错");
					return null;
				}
				//调用接口方法
				resultStr = CallWebService.httpCallWebService(method, params);
				//将16进制转为2进制以便解密
				decryptFrom = AESEncrypt.parseHexStr2Byte(resultStr);  
				//解密
				//转为字符串
				if(ValueCheck.IsNotEmpty(decryptFrom)){
					byte[] decryptResult  = AESEncrypt.decrypt(decryptFrom,  AsstUtils.authKey);
					str = new String(decryptResult,"utf-8");
				}
			}
//			System.out.println("解密=============================================="+DateFormat.getTs());
			return str;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 商城订单列表方法
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitBillData formatBillListResult(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", ResultBillData.class);
			childrenClassMap.put("result", AssitBillResult.class);
			childrenClassMap.put("orderInfo", Map.class);
			childrenClassMap.put("goodsOrder", Map.class);
			AssitBillData assitResult = (AssitBillData)JsonFormatUtil.formatAggObjectJson(resultStr, AssitBillData.class,childrenClassMap);
			return assitResult;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 订单上传商城返回结果格式方法
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitUploadBillData formatUploadBillResult(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", UploadResultBillData.class);
			childrenClassMap.put("result", AssitUploadBillResult.class);
			childrenClassMap.put("order", ResultOrder.class);
			AssitUploadBillData assitUploadBillData = (AssitUploadBillData)JsonFormatUtil.formatAggObjectJson(resultStr, AssitUploadBillData.class,childrenClassMap);
			return assitUploadBillData;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 接口返回数据基本List格式方法
	 * @param resultStr
	 * @return
	 * @throws CRUDException
	 */
	public static AssitResultList formatBaseListResult(String resultStr) throws CRUDException{
		try{
			if(ValueCheck.IsEmpty(resultStr)){
				return null;
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", ResultDataList.class);
			childrenClassMap.put("result", Map.class);
			AssitResultList assitResultList = (AssitResultList)JsonFormatUtil.formatAggObjectJson(resultStr, AssitResultList.class,childrenClassMap);
			return assitResultList;
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	public static void main(String[] args) throws UnsupportedEncodingException {
		String res = "{\"auth.token\":\"888ae933a5f5f492124224cdb3302bbf\",\"auth.sessionId\":\"\",\"auth.platformId\":\"4\",\"auth.userId\":\"\",\"auth.crtTime\":\"2014110315370732\",\"userName\":\"wanchao01\",\"password\":\"000000\"}";
		AuthMall authMall = new AuthMall();
		String ts = DateFormat.getTs();
		authMall.setToken("be009eb35a1bca943c53652600508ad6");
		authMall.setUserId("1");
		authMall.setCrtTime(ts);
		authMall.setPlatformId("4");
		authMall.setSessionId("0es85ef7u6a2kalsjipeiraqh1");
		String params="";
//		params += "{\"auth.token\":\""+authMall.getToken()+"\"," +
//				"\"auth.sessionId\":\""+authMall.getSessionId()+"\"," +
//				"\"auth.platformId\":\"4\"," +
//				"\"auth.userId\":\""+authMall.getUserId()+"\"," +
//				"\"auth.crtTime\":\""+DateFormat.getStringByDate(new Date(), "yyyyMMddHHmmssS")+"\"," +
//				"\"userName\":\"wanchao01\"," +
//				"\"password\":\"000000\"}";
		byte[] bits = AESEncrypt.encrypt(res, AsstUtils.authKey);
		String encryptResultStr = AESEncrypt.parseByte2HexStr(bits);  
		params = "paramData="+encryptResultStr;
		String resultStr = CallWebService.httpCallWebService("account/login.action", params);
		byte[] decryptFrom = AESEncrypt.parseHexStr2Byte(resultStr);  
		byte[] decryptResult = AESEncrypt.decrypt(decryptFrom,  AsstUtils.authKey);
		String str = new String(decryptResult);
//		formatResult(resultStr);
		System.out.println(str);
	}
	/**
	 * 验证是否已经登录过了
	 * 	true-已经登录
	 * 	false-没有登录
	 * @param 
	 * @return
	 */
	public static Boolean getAsstSession(Acct acct){
		Object obj = AsstUtils.itfMap.get(acct.getCode());
		if(ValueCheck.IsNotEmpty(obj)){
			AssistItfDomain asstInfo = (AssistItfDomain)obj;
			if(ValueCheck.IsEmpty(asstInfo.getAsstSessionId()) || ValueCheck.IsEmpty(asstInfo.getAsstUserId())){
				return false;
			}else{
				return true;
			}
		}
		return false;
	}
}
