package com.choice.assistant.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtil {
	/**
	 * 获取cookie中指定的值--acctidjmu--企业id
	 * @param request
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static String getCookie(HttpServletRequest request, String name) throws Exception {

		String value = "";
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie c = cookies[i];
				if (c.getName().equalsIgnoreCase(name)) {
					value = c.getValue();
					value = new String(value.getBytes("ISO8859_1"), "gbk");
					System.out.println(value);
				}
			}
		} 
		return value;
	}
}
