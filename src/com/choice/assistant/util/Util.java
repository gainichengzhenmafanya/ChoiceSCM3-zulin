package com.choice.assistant.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * @title: Util.java
 * @package com.choice.osp.util
 * @description: TODO
 * @copyright: JAVA开发部 (c) com.choice.www
 * @author qinyong
 * @date 2014-3-11
 * @version 1.0
 */
public class Util {


    /**
     * @Method: uploadFile
     * @Description: 上传文件方法
     * @param key 前台标签name属性的值,
     * @param url 已存在文件的地址(不需填写项目路径),
     * @param uploadUrl (不需填写项目路径) 文件的上传地址
     * @param customName 自定义文件的名称(不带文件的后缀名)
     * @return 返回文件的上传路径
     * @throws
     * @author qinyong
     */
    public static String uploadFile(HttpServletRequest request, String key, String url, String uploadUrl, String customName) throws Exception{

        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = multipartRequest.getFile(key);
        //获取上传路径
        String fileuploadPath = request.getSession().getServletContext().getRealPath(uploadUrl)+"/";
        //文件真实名称
        String realName = "";
        //如果传入的自定义名称为"",则默认取文件的名称
        if("".equals(customName)){
            realName = file.getOriginalFilename();
        }else{
            realName = customName+file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
        }
        //文件真实路径
        String realPath = fileuploadPath+realName;

        //如果已存在文件，则删除
        if(!"".equals(url) && null != url){
            File oldFile = new File(request.getSession().getServletContext().getRealPath(url));
            oldFile.delete();
        }

        //上传文件夹不存在时创建路径
        File dirPath = new File(fileuploadPath);
        if (!dirPath.exists()) {
            dirPath.mkdir();
        }
        //上传图片
        File uploadFile = new File(realPath);
        file.transferTo(uploadFile);
        return uploadUrl+"/"+realName;
    }

    /**
     * @Method: download
     * @Description: 下载文件
     * @param storeName 要下载的文件名称  realName网页下载的现实名称 filePath文件路径，相对路径不带文件名  contentType头类型
     * @return
     * @throws
     * @author qinyong
     */
    public static void download(HttpServletRequest request,
                                HttpServletResponse response, String storeName, String contentType,
                                String realName, String filePath) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        String ctxPath = request.getSession().getServletContext().getRealPath(filePath);
        //如果文件路径中已存在文件名称,则storename为空
        String downLoadPath = "".equals(storeName)?ctxPath:ctxPath +"/"+ storeName;

        long fileLength = new File(downLoadPath).length();

        String agent = request.getHeader("User-Agent");
        boolean isMSIE = (agent != null && agent.indexOf("MSIE") != -1);
        if (isMSIE) {
            realName = URLEncoder.encode(realName, "UTF-8");
        } else {
            realName = new String(realName.getBytes("UTF-8"), "ISO-8859-1");
        }
        response.setContentType(contentType);
        response.setHeader("Content-disposition", "attachment; filename=" +realName);
        response.setHeader("Content-Length", String.valueOf(fileLength));

        bis = new BufferedInputStream(new FileInputStream(downLoadPath));
        bos = new BufferedOutputStream(response.getOutputStream());
        byte[] buff = new byte[2048];
        int bytesRead;
        while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
            bos.write(buff, 0, bytesRead);
        }
        bis.close();
        bos.close();
    }
}
