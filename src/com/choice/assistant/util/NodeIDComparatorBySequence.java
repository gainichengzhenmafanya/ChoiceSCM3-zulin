package com.choice.assistant.util;

import java.util.Comparator;

public class NodeIDComparatorBySequence  implements Comparator<Object>  {
		// 按照节点排序号比较
		public int compare(Object o1, Object o2) {
			String j1 = ((Node) o1).sequence;
			String j2 = ((Node) o2).sequence;
			return (j1.compareTo(j2)<0 ? -1 : (j1.compareTo(j2)==0 ? 0 : 1));
		}

}
