package com.choice.assistant.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public class ExcelUtil {

	public static final String PROPERTY = "property";
	public static final String TITLE = "title";
	public static final String NAME = "name";
	public static final String DATALIST = "dataList";
	
	/** 
     * @Method: download 
     * @Description: 下载文件
     * @param storeName要下载的文件名称  realName网页下载的现实名称 filePath文件路径，相对路径不带文件名  contentType头类型
     * @return 
     * @throws
     * @author qinyong 
     */
    public static void download(HttpServletRequest request,  
            HttpServletResponse response, String storeName, String contentType,  
            String realName, String filePath) throws Exception {  
        response.setContentType("text/html;charset=UTF-8");  
        request.setCharacterEncoding("UTF-8");  
        BufferedInputStream bis = null;  
        BufferedOutputStream bos = null;  
  
        String ctxPath = request.getSession().getServletContext()  
                .getRealPath(filePath);
        //如果文件路径中已存在文件名称,则storename为空
        String downLoadPath = "".equals(storeName)?ctxPath:ctxPath +"/"+ storeName;  
        
        long fileLength = new File(downLoadPath).length();  
  
        String agent = request.getHeader("User-Agent");
        boolean isMSIE = (agent != null && agent.indexOf("MSIE") != -1);
        if (isMSIE) {
            realName = URLEncoder.encode(realName, "UTF-8");
        } else {
        	realName = new String(realName.getBytes("UTF-8"), "ISO-8859-1");
        }
        response.setContentType(contentType);  
        response.setHeader("Content-dispositn", "attachment; filename=" +realName);
        response.setHeader("Content-Length", String.valueOf(fileLength));  
  
        bis = new BufferedInputStream(new FileInputStream(downLoadPath));  
        bos = new BufferedOutputStream(response.getOutputStream());  
        byte[] buff = new byte[2048];  
        int bytesRead;  
        while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {  
            bos.write(buff, 0, bytesRead);  
        }  
        bis.close();  
        bos.close();  
    }  
    
    /** 
	 * @Method: uploadFile 
	 * @Description: 上传文件方法
	 * @param key 前台标签name属性的值,url 已存在文件的地址(不需填写项目路径), uploadUrl(不需填写项目路径) 文件的上传地址
	 * @param customName 自定义文件的名称(不带文件的后缀名)
	 * @return 返回文件的上传路径
	 * @throws
	 * @author qinyong 
	 */
	public static String uploadFile(HttpServletRequest request, String key, String uploadUrl) throws Exception{
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartRequest.getFile(key);
		//获取上传路径
		String fileuploadPath = request.getSession().getServletContext().getRealPath(uploadUrl)+File.separator;
		//文件真实名称
		String realName = "";
		//如果传入的自定义名称为"",则默认取文件的名称
		realName = file.getOriginalFilename();
		//文件真实路径
//		String realPath = fileuploadPath+realName;

		//如果已存在文件，则删除
		if(!"".equals(file.getOriginalFilename()) && null != file.getOriginalFilename()){
			File oldFile = new File(fileuploadPath+file.getOriginalFilename());
			oldFile.delete();
		}
		
		//上传文件夹不存在时创建路径
		File dirPath = new File(fileuploadPath);
		if (!dirPath.exists()) {
			dirPath.mkdir();
		}
		//上传
		File uploadFile = new File(fileuploadPath+realName);
		if(!uploadFile.exists()){
			file.transferTo(uploadFile);
			return fileuploadPath+realName;
		}else{
			File newfile = new  File(fileuploadPath+"1"+realName);
			file.transferTo(newfile);
			return fileuploadPath+"1"+realName;
		}
	}
	
	/**
	 * 导出Excel通用方法
	 * @param request
	 * @param response
	 * @param datalist
	 * @param propertiesArray
	 * @param nameArray
	 * @param title
	 * @param fileName
	 */
	public static void  exportExcel(HttpServletRequest request,HttpServletResponse response,List<?> objectlist,
			String[] propertiesArray,String[] nameArray,String title,String fileName){
		List<Map<String,Object>> datalist = MapBeanConvertUtil.convertBeanList(objectlist);
		
		WritableWorkbook workBook = null;
		OutputStream os = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);
		try {
			titleStyle.setAlignment(Alignment.CENTRE);
			response.setContentType("application/msexcel; charset=UTF-8");
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
			    //IE  
			    fileName = URLEncoder.encode(fileName, "UTF-8");              
			}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
			    //firefox  
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}else{                
			    // other          
			    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
			}   
			response.setHeader("Content-dispositn", "attachment; filename="  
	                + fileName + ".xls");
			os = response.getOutputStream();
			workBook = Workbook.createWorkbook(os);
			WritableSheet sheet = workBook.createSheet(title, 0);
			Label label = new Label(0,0,title,titleStyle);
            sheet.addCell(label);
            //设置表格表头
            for(int i = 0 ; i < nameArray.length;i ++){
            	Label head = new Label(i,1,nameArray[i],contentStyle);
            	sheet.addCell(head);
            }
            sheet.mergeCells(0, 0, nameArray.length-1, 0);
            //遍历list填充表格内容
            for(int i = 0 ; i < datalist.size() ; i ++ ){
            	for(int j = 0 ; j < propertiesArray.length ; j ++){
            		Map<String,Object> map = datalist.get(i);
            		String result = "";
            		
            		String property = propertiesArray[j];
            		if(property.indexOf(".")!=-1){
            			Object object = map.get(property.substring(0, property.indexOf(".")));
                        if(object==null){
                            result=null;
                        }else {
                            Map<String, Object> typemap = MapBeanConvertUtil.convertBean(object);
                            Object curobj = typemap.get(property.substring(property.indexOf(".") + 1));
                            result = null == curobj ? "" : curobj.toString();
                        }
            		}else{
            			Object curValue = map.get(propertiesArray[j]);
                		curValue = curValue == null ? "":curValue;
    					result = curValue.toString();
            		}
        			Label lab = new Label(j,2+i,result,contentStyle);
            		sheet.addCell(lab);
            	}
            }
            workBook.write();
            os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				workBook.close();
				os.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 导出多个子表的excel
	 * @param request
	 * @param response
	 * @param title
	 * @param fileName
	 * @param maplist
	 */
	public static void exportExcelChildrenList(HttpServletRequest request,HttpServletResponse response,
			String title,String fileName,List<Map<String,Object>> maplist){
		WritableWorkbook workBook = null;
		OutputStream os = null;
		WritableFont titleFont = new WritableFont(WritableFont.TIMES, 16,  
	            WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableFont contentFont = new WritableFont(WritableFont.TIMES, 10,  
	            WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,  
	            Colour.BLACK);
		WritableCellFormat titleStyle = new WritableCellFormat(titleFont);
		WritableCellFormat contentStyle = new WritableCellFormat(contentFont);
		try {
				titleStyle.setAlignment(Alignment.CENTRE);
				response.setContentType("application/msexcel; charset=UTF-8");
				if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0){                
				    //IE  
				    fileName = URLEncoder.encode(fileName, "UTF-8");              
				}else if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {  
				    //firefox  
				    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
				}else{                
				    // other          
				    fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");              
				}   
				response.setHeader("Content-dispositn", "attachment; filename="  
		                + fileName + ".xls");
				os = response.getOutputStream();
				workBook = Workbook.createWorkbook(os);
				int sheetNo = 0;
				for(Map<String,Object> sheetmap:maplist){
					String sheetname = sheetmap.get(TITLE).toString();
					String[] propertyArray = (String[])sheetmap.get(PROPERTY);
					String[] nameArray = (String[])sheetmap.get(NAME);
					List<Map<String,Object>> datalist = MapBeanConvertUtil.convertBeanList((List<?>)sheetmap.get(DATALIST));
					
					WritableSheet sheet = workBook.createSheet(sheetname, sheetNo++);
					Label label = new Label(0,0,title,titleStyle);
		            sheet.addCell(label);
		            //设置表格表头
		            for(int i = 0 ; i < nameArray.length;i ++){
		            	Label head = new Label(i,1,nameArray[i],contentStyle);
		            	sheet.addCell(head);
		            }
		            sheet.mergeCells(0, 0, nameArray.length-1, 0);
		            //遍历list填充表格内容
		            for(int i = 0 ; i < datalist.size() ; i ++ ){
		            	for(int j = 0 ; j < propertyArray.length ; j ++){
		            		Map<String,Object> map = datalist.get(i);
		            		String result = "";
		            		
		            		String property = propertyArray[j];
		            		if(property.indexOf(".")!=-1){
		            			Object object = map.get(property.substring(0, property.indexOf(".")));
		            			Map<String,Object> typemap = MapBeanConvertUtil.convertBean(object);
		            			Object curobj = typemap.get(property.substring(property.indexOf(".")+1));
		    					result = null==curobj?"":curobj.toString();
		            		}else{
		            			Object curValue = map.get(propertyArray[j]);
		                		curValue = curValue == null ? "":curValue;
		    					result = curValue.toString();
		            		}
		        			Label lab = new Label(j,2+i,result,contentStyle);
		            		sheet.addCell(lab);
		            	}
		            }
				}
				workBook.write();
		        os.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					workBook.close();
					os.close();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	}
}
