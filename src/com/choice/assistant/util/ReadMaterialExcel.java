package com.choice.assistant.util;


import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.springframework.stereotype.Controller;

import com.choice.assistant.domain.material.Material;
import com.choice.assistant.domain.material.MaterialType;
import com.choice.assistant.persistence.material.MaterialMapper;


/**
 * 读取excel 和 判断excel中数据是否存在错误信息
 * 
 * @author qiaoyj
 * 
 */
@Controller
public class ReadMaterialExcel {
	
	/**
	 * 加载表头信息
	 * 
	 * 
	 */
	
	// Excel表头信息
	static String[] excelColumns = { 
		"企业ID"
		,"编码"
		,"名称"
		,"缩写"
		,"物资类别"
		,"缩写"
		,"规格"
		,"启用状态"
		,"标准计量单位"
		,"进货价"
		,"售价"
		,"品牌"
		,"产地"
		,"税率"
		,"保质期"
		,"是否可退货"
		,"验货差异率"
		,"最小采购量"
		,"采购周期"
		,"条码"
		,"采购单位"
		,"销售单位"
		,"库存单位"
		,"采购单位转换率"
		,"销售单位转换率" 
		,"库存单位转换率"                    
		,"备注"

		 };
	// 类中信息
	static String[] supplyColumns = { 
		"acct"
		,"vcode"
		,"vname"
		,"materialtype"
		,"vinit"
		,"vspecfication"
		,"enablestate"
		,"vunitname"
		,"ninprice"
		,"nsaleprice"
		,"vbrand"
		,"vaddr"
		,"ntax"
		,"ishelflife"
		,"bisback"
		,"ncheckdiffrate"
		,"nminamt"
		,"ineeddate"
		,"vbarcode"
		,"cgunit"
		,"xsunit"
		,"kcunit"
		,"cgnater"
		,"xsnater"
		,"kcnater"
		,"vmemo"   
};
	/**
	 * Map<String, String> 将错误信息存放到map中 用于前台显示使用
	 */
	public static Map<String, String> map = new HashMap<String, String>();

	/**
	 * List<Material> 将Material添加到list中
	 */
	public static List<Material> MaterialList = new ArrayList<Material>();
	
	private static Material material;
	
	/**
	 * 对excel中的数据判断 并把错误的信息存放到Map信息中 存在错误信息时返回false 如果不存在错误信息 返回true
	 * 
	 * @param path
	 *            上传的excel路径
	 * @return
	 */
	public static Boolean check(String path,MaterialMapper materialMapper) {
		Boolean checkResult = true;
		Workbook book = null;
		try {
			book = Workbook.getWorkbook(new File(path));
			// 获取工作表
			Sheet sheet = book.getSheet(0);
			int colNum = sheet.getColumns();// 列数
			int rowNum = sheet.getRows();// 行数
			if (rowNum > 0) {
				if (!checkResult(rowNum, colNum, sheet,materialMapper)) {
					checkResult = false;
				}
			} else {
				checkResult = false;
			}
		} catch (Exception e) {
			checkResult = false;
			e.printStackTrace();

		} finally {
			if (book != null) {
				book.close();
				book = null;
			}

		}
		return checkResult;
	}

	/**
	 * 检测 存在错误信息时返回false 如果不存在 返回true
	 * 
	 * @param rowNum
	 * @param colNum
	 * @param sheet
	 * @return
	 */
	private static Boolean checkResult(int rowNum, int colNum, Sheet sheet,MaterialMapper materialMapper)
			throws Exception {
		Boolean bool = true;
		Cell cell = null;
		MaterialList.clear();
		for (int i = 1; i < rowNum; i++) {
			// 行循环
			material = new Material();
			for (int col = 0; col < colNum; col++) {// 列循环
				cell = sheet.getCell(col, i);// 读取单元格
				
				if (col <= 3) { // 判断必填字段是否为空
					if (isEmpty(cell.getContents())) {
						map.put((i+1) + "_" + (col + 1), excelColumns[col] + "为空");
						bool = false;
						continue;
					}
				}
				//验证
				if (!("").equals(cell.getContents())) {
					bool = validateColumns(cell.getContents(),i,col, materialMapper);
				}
				if(bool){
					if (!("").equals(cell.getContents())) {// 如果不为""时将物资信息保存到实体类中
						setSupply(cell, material, col);
					}
				}
			}
			if(bool){
				MaterialList.add(material);
			}
		}
		return bool;
	}
	private static boolean validateColumns(String cell, int i,int col,MaterialMapper materialMapper) {
		boolean bool=true;
		switch (col) {
		case 1:
			//验证编码
			String regCode = "^[\\d\\w][\\d\\w-]*";;
			bool=validate(cell,regCode,i,col);
			break;
//		case 2:
//			//验证名称
//			String regName = "[\u4e00-\u9fa5]*";
//			bool=validate(cell,regName,i,col);
//			break;
//		case 3:
//			//验证缩写
//			String reginit = "^[0-9A-Za-z]*$";
//			bool=validate(cell,reginit,i,col);
//			break;
		case 4:
			//验证类别
			try {
				List<MaterialType> obj = materialMapper.queryMaterialType(cell, material.getAcct());//查询三级类别
				if(obj == null){
					map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过!没有此类别!");
					bool = false;
				}else{
					MaterialType materialType = (MaterialType)obj.get(0);
					bool = true;
				}
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		case 5:
			bool = false;
			break;
		case 6:
			bool = false;
			break;
		case 7:
			bool = false;
			break;
		case 8:
			bool = false;
			break;
		case 9:
			bool = false;
			break;
		default:
			break;
		}
		return bool;
	}
	private static boolean validate(String cell,String regEx, int i,int col){
		if(cell.matches(regEx)){
			return true;
		}else{
			map.put((i+1) + "_" + (col + 1), excelColumns[col] + "验证不通过");
			return false;
		}
	}
	

	private static void setSupply(Cell cell,Material material,
			int col) throws Exception {

		reflexMethod(material,
				supplyColumns[col].trim(),
				cell.getContents());
		
	}

	/**
	 * 判断是否为空
	 * 
	 * @param contents
	 * @return
	 */
	private static boolean isEmpty(String contents) {
		if ("".equals(contents.trim()))
			return true;
		else
			return false;
	}

	/**
	 * 反射机制set参数
	 * 
	 * @param objParam
	 * @param propertyMethod
	 * @param param
	 * @throws Exception
	 */
	private static void reflexMethod(Object objParam, String propertyMethod,
			String param) throws Exception {

		if (propertyMethod.contains(".")) {
			// 获得.之前的get方法
			String getCode = propertyMethod.substring(0,
					propertyMethod.indexOf("."));
			StringBuffer getStringBuff = new StringBuffer("get");
			getStringBuff.append(toUpOfFirst(getCode));
			Method methodOne = objParam.getClass().getMethod(
					getStringBuff.toString(), new Class[] {});
			Object obj = new Object();
			String setCode = propertyMethod.substring(propertyMethod
					.indexOf(".") + 1);
			if("Double".equals(setCode)){
				obj = Double.parseDouble(param);
			}else if("Integer".equals(setCode)){
				obj = Integer.parseInt(param);
			}else if("Date".equals(setCode)){
				obj = new Date(param);
			}
			getStringBuff.replace(0, 1, "s");
			Method setMethodTwo = objParam.getClass().getMethod(
					getStringBuff.toString(), methodOne.getReturnType());
			setMethodTwo.invoke(objParam, new Object[] { obj });
		} else {
			String method = "set" + toUpOfFirst(propertyMethod);
			Method methodEl = objParam.getClass().getMethod(method,
					param.getClass());
			methodEl.invoke(objParam, new Object[] { param });
		}
	}

	/**
	 * 将首字母转换为大写
	 * 
	 * @param str
	 * @return
	 */
	private static String toUpOfFirst(String str) {
		char[] chars = str.toCharArray();
		if ((chars[0] >= 97) && (chars[0] <= 122)) {
			chars[0] = (char) (chars[0] - 32);
		}
		return new String(chars);
	}

}
