package com.choice.assistant.util;
/**
 * 商城接口返回类
 * @author ZGL
 *
 */
public class AssistItfDomain {

	private String asstSessionId;	//商城返回sessionId
	private String asstUserId;		//接口登录返回的userId
	private String asstUserName;	//接口登录名
	private String asstPas;			//接口登录密码
	
	public String getAsstSessionId() {
		return asstSessionId;
	}
	public void setAsstSessionId(String asstSessionId) {
		this.asstSessionId = asstSessionId;
	}
	public String getAsstUserId() {
		return asstUserId;
	}
	public void setAsstUserId(String asstUserId) {
		this.asstUserId = asstUserId;
	}
	public String getAsstUserName() {
		return asstUserName;
	}
	public void setAsstUserName(String asstUserName) {
		this.asstUserName = asstUserName;
	}
	public String getAsstPas() {
		return asstPas;
	}
	public void setAsstPas(String asstPas) {
		this.asstPas = asstPas;
	}
}
