package com.choice.assistant.service.settlement;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.settlement.PaymentConstants;
import com.choice.assistant.constants.settlement.ProsettlementConstants;
import com.choice.assistant.domain.settlement.Payment;
import com.choice.assistant.domain.settlement.Payment_d;
import com.choice.assistant.domain.settlement.ProBill;
import com.choice.assistant.domain.settlement.ProPayment;
import com.choice.assistant.domain.settlement.Prosettlement;
import com.choice.assistant.persistence.settlement.ProsettlementMappler;
import com.choice.assistant.service.bill.PuprOrderService;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * Created by mc on 14-10-29.
 */
@Service
public class ProsettlementService {
    @Autowired
    private ProsettlementMappler prosettlementMappler;
    //@Autowired
    //private CoderuleService coderuleService;
    @Autowired
    private PageManager<Prosettlement> pageManager;

    private final transient Log log = LogFactory.getLog(ProsettlementService.class);
    /**
     * 获取结算信息
     * @param prosettlement
     * @return
     */
    public List<Prosettlement> listPro(Prosettlement prosettlement,String group,Page page) throws CRUDException {
        try {
            prosettlement.setAcct(group);
            return pageManager.selectPage(prosettlement, page, ProsettlementMappler.class.getName() + ".listPro");
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 获取未、已结算的单据
     * @param prosettlement
     * @return
     * @throws CRUDException
     */
    public List<ProBill> listProBill(Prosettlement prosettlement) throws CRUDException {
        try {
            return prosettlementMappler.listProBill(prosettlement);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 生成付款单
     * @param group
     * @param proPayment
     */
    public String addPro(String group,String accountName,ProPayment proPayment) throws CRUDException {
        try {
            if(proPayment==null||proPayment.getType()==null||proPayment.getType()==""||proPayment.getCodes()==null||proPayment.getCodes()==""){
                throw new CRUDException("上传的单据信息为空！");
            }
            List<String> list=Arrays.asList(proPayment.getCodes().split(","));
            String codes = "";//入库单主键
            String pks = "";//其他应付款主键
            Pattern p = Pattern.compile("\\d+");
            for(String pk_id:list){
            	if(p.matcher(pk_id).matches()){//是数字
            		codes = codes + "'"+pk_id+"',";
            	}else{
            		pks = pks + "'"+pk_id+"',";
            	}
            }
            
            if(codes.length()>0){
            	codes = codes.substring(0,codes.lastIndexOf(","));
            }
            if(pks.length()>0){
            	pks = pks.substring(0,pks.lastIndexOf(","));
            }
            
            proPayment.setCodes(codes);
            proPayment.setPks(pks);
            proPayment.setAcct(group);
            List<ProBill> proBills=prosettlementMappler.findBillByPk(proPayment);
            List<Payment_d> paymentDs=new ArrayList<Payment_d>();
            String pk= CodeHelper.createUUID();
            for(ProBill proBill:proBills){//循环 采购结算子表
                Payment_d payment_d=new Payment_d();
                payment_d.setAcct(group);
                payment_d.setPk_payment(pk);
                payment_d.setCreator(accountName);
                payment_d.setNypaymoney(proBill.getNmoney());
                payment_d.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
                payment_d.setPk_paymentd(CodeHelper.createUUID());
                payment_d.setPk_bills(proBill.getPk_bill());
                payment_d.setVbilltype(proBill.getBillType());
                payment_d.setDr(0);
                payment_d.setVbillnumber(proBill.getVbillno());
                paymentDs.add(payment_d);
                prosettlementMappler.updateState(setMap(proBill.getPk_bill(),proBill.getBillType(),group));
            }//生成付款单主表数据丅
            BigDecimal bigDecimal;
            bigDecimal=new BigDecimal(0);
            Payment payment=new Payment();
            payment.setAcct(group);
            payment.setPk_payment(pk);
            //payment.setVpaymentcode(coderuleService.getCode(CoderuleConstants.PAYMENT,group));
            payment.setVpaymentcode(this.getNextVpaymentcode(new Date(), group));
            payment.setPk_supplier(proPayment.getSupplier());
            payment.setDelivercode(proPayment.getSupplier());
            payment.setDbilldat(proPayment.getBilldate());
            payment.setNypaymoney(bigDecimal.add(BigDecimal.valueOf(proPayment.getNmoney())).doubleValue());
            payment.setNxpaymoney(bigDecimal.add(BigDecimal.valueOf(proPayment.getNmoney())).doubleValue());
            payment.setNinvoicemoney(bigDecimal.add(BigDecimal.valueOf(proPayment.getImoney())).doubleValue());
            payment.setIisorinvoive(PaymentConstants.UNIISORINVOIVE);
            payment.setIpaymentstate(PaymentConstants.UNIPAYMENTSTATE);
            payment.setCreator(accountName);
            payment.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
            prosettlementMappler.addPayment(payment);
            prosettlementMappler.addPaymnetD(paymentDs);
            return payment.getVpaymentcode();
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    
	/**
	 * 生成付款单号js20161219
	 * @param date
	 * @return
	 * @throws CRUDException
	 */
	public String getNextVpaymentcode(Date date,String acct)throws CRUDException{
		try {
			Payment payment = new Payment();
			payment.setAcct(acct);
			payment.setDbilldat(DateFormat.getStringByDate(date,"yyyy-MM-dd"));
			//得到下一个单号
			String vbillno = prosettlementMappler.getNextVpaymentcode(payment);
			if(vbillno==null){
				vbillno = "0001";
			}
			else if(vbillno.length()==1){
				vbillno = "000"+vbillno;
			}
			else if(vbillno.length()==2){
				vbillno = "00"+vbillno;
			}
			else if(vbillno.length()==3){
				vbillno = "0"+vbillno;
			}
			SimpleDateFormat sif = new SimpleDateFormat("yyyyMMdd");
			vbillno = "FK"+sif.format(date)+PuprOrderService.getRandomStr(3)+vbillno;
			return vbillno;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

    public Map<String,Object> setMap(final String code, final int billtype,String acct){
        Map<String,Object> map=new HashMap<String, Object>();
        switch(billtype){
            case ProsettlementConstants.BACKBILL:
                map.put("table", "chkinm");
                map.put("billWhere"," chkinno='"+code+"' and acct = '"+acct+"'");
                map.put("setValue"," istate=2");
                break;
            case ProsettlementConstants.INSPECT:
                map.put("table", "chkinm");
                map.put("billWhere"," chkinno='"+code+"' and acct = '"+acct+"'");
                map.put("setValue"," istate=2");
                break;
            case ProsettlementConstants.RESTSPAYMENT:
                map.put("table", "cscm_otherpayables_m");
                map.put("billWhere"," pk_otherpayables='"+code+"' and acct = '"+acct+"'");
                map.put("setValue"," istate=3");
                break;
            default:
                return null;
        }
        return map;
    }
}
