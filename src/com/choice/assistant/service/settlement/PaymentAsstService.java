package com.choice.assistant.service.settlement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.settlement.PaymentConstants;
import com.choice.assistant.domain.settlement.Applyinvoice;
import com.choice.assistant.domain.settlement.Collinvoice;
import com.choice.assistant.domain.settlement.Paydetails;
import com.choice.assistant.domain.settlement.Payment;
import com.choice.assistant.persistence.settlement.PaymentMappler;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * Created by mc on 14-10-31.
 */
@Service
public class PaymentAsstService {
    @Autowired
    private PaymentMappler paymentMappler;
    @Autowired
    private PageManager<Payment> pageManager;

    private final transient Log log = LogFactory.getLog(PaymentAsstService.class);

    /**
     *
     * @param payment
     * @param page
     * @return
     * @throws CRUDException
     */
    public List<Payment> findPayment(Payment payment,String group,Page page) throws CRUDException {
        try{
            payment.setAcct(group);
            return pageManager.selectPage(payment, page, PaymentMappler.class.getName() + ".findPayment");
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 获取子表数据
     * @param payment
     * @return
     * @throws CRUDException
     */
    public Payment findPaymentd(Payment payment) throws CRUDException {
        try{
        	if(ValueCheck.IsNotEmpty(payment.getPk_payment())){
	            payment.setCollinvoices(paymentMappler.findCollinvoice(payment));
	            payment.setPaymentDList(paymentMappler.findPaymentd(payment));
	            payment.setApplyinvoices(paymentMappler.findApplyinvoice(payment));
	            payment.setPaydateiles(paymentMappler.findPaydetails(payment));
        	}
            return payment;
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 保存发票申请
     * @param payment
     * @param group
     * @param creator
     * @return
     * @throws CRUDException
     */
    public String addApp(Payment payment,String group,String creator) throws CRUDException {
        try{
            if(payment.getApplyinvoices()!=null&&payment.getApplyinvoices().size()>0){
                if(payment.getCodes()!=null) {
                    List<String> codelist = Arrays.asList(payment.getCodes().split(","));
                    if(codelist!=null && codelist.size()>0) {
                        for(String pk:codelist) {
                            List<Applyinvoice> list=new ArrayList<Applyinvoice>();
                            for (Applyinvoice applyinvoice : payment.getApplyinvoices()) {
                                applyinvoice.setPk_applyinvoice(CodeHelper.createUUID());
                                applyinvoice.setPk_payment(pk);
                                applyinvoice.setAcct(group);
                                applyinvoice.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
                                applyinvoice.setCreator(creator);
                                applyinvoice.setVapplyaccount(creator);
                                list.add(applyinvoice);
                            }
                            paymentMappler.addApp(list);
                        }
                    }else{
                        return "4";//传入的数据为空
                    }
                }else {
                    return "4";//传入的数据为空
                }
            }else {
                return "2";//发票申请数据为空
            }
            return "3";//成功
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 关联发票
     * @param group
     * @param collinvoice
     * @return
     */
    public String addColl(String group,String name,Collinvoice collinvoice){
        if(collinvoice!=null&&collinvoice.getCodes()!=null){
            List<String> list=Arrays.asList(collinvoice.getCodes().split(","));
            if(list!=null&&list.size()>0){
                for(String code:list){
                    collinvoice.setPk_collinvoice(CodeHelper.createUUID());
                    collinvoice.setAcct(group);
                    collinvoice.setPk_payment(code);
                    collinvoice.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
                    if(collinvoice.getCreator()==null||collinvoice.getCreator()==""){
                        collinvoice.setCreator(name);
                    }
                    if(collinvoice.getDdat()==null||collinvoice.getDdat()==""){
                        collinvoice.setDdat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
                    }
                    paymentMappler.addColl(collinvoice);
                    Payment payment=new Payment();
                    payment.setPk_payment(code);
                    payment=paymentMappler.findPaymentByPk(payment);
                    payment.setIisorinvoive(PaymentConstants.IISORINVOIVE);//设置发票标示
                    paymentMappler.updatePayment(payment);
                }
                return "ok";
            }else{
                return "获取付款单错误！";
            }
        }else{
            return "获取付款单错误！";
        }
    }

    /**
     * 保存付款
     * @param group
     * @param paydetails
     * @param payment
     * @return
     */
    public String addPaydetails(String group,Paydetails paydetails,Payment payment){
        if(payment!=null&&payment.getCodes()!=null){
            List<String> list=Arrays.asList(payment.getCodes().split(","));
            if(list!=null&&list.size()>0){
                for(String code:list){
                    payment.setPk_payment(code);
                    Payment p=paymentMappler.findPaymentByPk(payment);//根据PK获取单据
                    p.setNwpaymoney((p.getNwpaymoney()==null?0:p.getNwpaymoney())+(paydetails.getNpaymoney()==null?0:paydetails.getNpaymoney()));//计算已付款金额
                    /*if(p.getNwpaymoney()<p.getNypaymoney()){
                        p.setIpaymentstate(payment.getIpaymentstate());
                    }else{
                        p.setIpaymentstate(PaymentConstants.IPAYMENTSTATE);
                    }*/  // 采购付款时，不要修改为已结算状态， 否则无法进行审核结算  2017-07-17  by lbh
                    paydetails.setAcct(group);
                    paydetails.setPk_payment(code);
                    paydetails.setPk_paydetails(CodeHelper.createUUID());
                    paymentMappler.addPaydetails(paydetails);
                    paymentMappler.updatePayment(p);
                }
                //update by lq at 2015.5.12  change puprorder state by paymentd
                if(payment.getIpaymentstate()==2){
//                	for(String code:list){
//                    	payment.setPk_payment(code);
//                        List<Payment_d> dlist=paymentMappler.findPaymentd(payment);//根据PK获取单据
//                        for(Payment_d pd:dlist){
//                        	paymentMappler.updatePuprorderState(pd);
//                        }
//                    }
                	//更新订单状态js20161219
                	paymentMappler.updatePuprorderIstate2(payment);
                	//更新验货的状态
                	paymentMappler.updateInspectmIstate2(payment);
                }
                
                return "1";//保存成功
            }else{
                return "2";//获取账单数据错误！
            }
        }else{
            return "2";
        }
    }
    
    /**
     * 审核结账
     * @param acct
     * @param payment
     * @return
     * @throws Exception 
     */
    public String checkEnd(String acct,Payment payment) throws Exception{
    	try{
			//更新状态
			paymentMappler.checkEnd(payment);
	    	//更新订单状态js20161219
	    	paymentMappler.updatePuprorderIstate2(payment);
	    	//更新验货的状态
	    	paymentMappler.updateInspectmIstate2(payment);
	    	return "OK";
    	}catch(Exception e){
    		throw new Exception(e.getMessage());
    	}
    }
    
    /**
    *	根据主键查询付款单
    * @param payment
    * @param page
    * @return
    * @throws CRUDException
    */
   public Payment findPaymentByPk(Payment payment) throws CRUDException {
       try{
    	   return paymentMappler.findPaymentByPk(payment);//根据PK获取单据
       } catch (Exception e) {
           log.error(e);
           throw new CRUDException(e);
       }
   }
}
