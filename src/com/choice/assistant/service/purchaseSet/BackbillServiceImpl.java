package com.choice.assistant.service.purchaseSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.PurchaseSet.Backbilld;
import com.choice.assistant.domain.PurchaseSet.Backbillm;
import com.choice.assistant.persistence.purchaseSet.BackbillMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 退货单管理
 * @author zgl
 *
 */
@Service
public class BackbillServiceImpl {
	
	@Autowired
	private BackbillMapper backbillMapper;
	@Autowired
	private PageManager<Backbillm> pageManager;
	
	private final transient Log log = LogFactory.getLog(BackbillServiceImpl.class);
	
	/**
	 * 查询所有的退货单主表数据
	 * @return
	 */
	public List<Backbillm> queryAllBackbillm(Backbillm backbillm,Page page) throws CRUDException{
		try {
			if(ValueCheck.IsNotEmpty(backbillm.getPk_backbill())){
				backbillm.setPk_backbill(AsstUtils.StringCodeReplace(backbillm.getPk_backbill()));
			}
			return pageManager.selectPage(backbillm, page, BackbillMapper.class.getName()+".queryAllBackbillm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的退货单子表数据
	 * @return
	 */
	public List<Backbilld> queryAllBackbilld(Backbillm backbillm) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(backbillm.getPk_backbill())){
				return null;
			}
			return backbillMapper.queryAllBackbilld(backbillm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 *  根据主键查询指定的退货单主表、子表
	 * @return
	 */
	public List<Backbillm> queryAllBackbillmByPk(Backbillm backbillm) throws CRUDException{
		try {
			return backbillMapper.queryAllBackbillmByPk(backbillm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 新增、修改保存退货单
	 * @param backbillmJson
	 * @param acct
	 * @param accountId
	 * @return
	 * @throws CRUDException
	 */
	public String saveBackbill(String backbillJson,String acct,String accountId) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//组织不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("backbilldList", Backbilld.class);
			Backbillm backbillm = (Backbillm)JsonFormatUtil.formatAggObjectJson(backbillJson, Backbillm.class,childrenClassMap);
			backbillm.setAcct(acct);
			backbillm.setCreator(accountId);
			
			if(ValueCheck.IsNotEmpty(backbillm)){//保存主表
				if(ValueCheck.IsEmpty(backbillm.getPk_backbill())){//如果主键为空则为新增主表
					backbillm.setPk_backbill(Util.getUUID());
					backbillm.setDr(0);
					backbillm.setTs(DateFormat.getTs());
					backbillm.setCreationtime(DateFormat.getTs());
					backbillMapper.saveBackbillm(backbillm);
				}else{//如果主键不为空则为修改主表
					List<Backbillm> list = backbillMapper.queryAllBackbillm(backbillm);
					if(list.size()>0){
						if(!"0".equals(list.get(0).getIstate())){
							return "-2";
						}
					}
					backbillMapper.updateBackbillm(backbillm);
				}
				//更新子表
				updateBackbilld(backbillm);
			}
			return "1";
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改保存退货单
	 * @param backbillmJson
	 * @param acct
	 * @param accountId
	 * @return
	 * @throws CRUDException
	 */
	public String updateBackbill(String backbillJson,String acct,String accountId) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(backbillJson)){
				return "0";
			}
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//组织不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("backbilldList", Backbilld.class);
			Backbillm backbillm = (Backbillm)JsonFormatUtil.formatAggObjectJson(backbillJson, Backbillm.class,childrenClassMap);
			backbillm.setAcct(acct);
			backbillm.setCreator(accountId);
			
			if(ValueCheck.IsNotEmpty(backbillm)){//保存主表
				if(ValueCheck.IsNotEmpty(backbillm.getPk_backbill())){//如果主键不为空则为修改主表
					List<Backbillm> list = backbillMapper.queryAllBackbillm(backbillm);
					if(list.size()>0){
						if(!"0".equals(list.get(0).getIstate())){
							return "-2";
						}
					}
					backbillMapper.updateBackbillm(backbillm);
				}else{
					return "-3";
				}
				//更新子表
				updateBackbilld(backbillm);
			}
			return "1";
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 先删除子表、再新增子表
	 * @param backbillm
	 * @throws CRUDException
	 */
	public void updateBackbilld(Backbillm backbillm) throws CRUDException{
		if(ValueCheck.IsEmpty(backbillm.getPk_backbill())){
			return;
		}
		backbillMapper.deleteBackbilld(backbillm);
		if(ValueCheck.IsNotEmpty(backbillm.getBackbilldList())){//保存子表
			for(Backbilld backbilld : backbillm.getBackbilldList()){
				backbilld.setPk_backbilld(Util.getUUID());
				backbilld.setPk_backbill(backbillm.getPk_backbill());
				backbilld.setAcct(backbillm.getAcct());
				backbilld.setDr(0);
				backbilld.setTs(DateFormat.getTs());
				backbillMapper.saveBackbilld(backbilld);
			}
		}
	}
	/**
	 * 删除退货单
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public String deleteBackbill(String pk_backbill) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(pk_backbill)){
				return "-1";
			}
			Backbillm backbillm = new Backbillm();
			backbillm.setPk_backbill(AsstUtils.StringCodeReplace(pk_backbill));
			List<Backbillm> list = backbillMapper.queryAllBackbillm(backbillm);
			if(list.size()>0){
				for(Backbillm backbill : list){
					if(!"0".equals(backbill.getIstate())){
						return "-2";
					}
				}
			}
			backbillm.setTs(DateFormat.getTs());
			backbillMapper.deleteBackbill(backbillm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	

}
