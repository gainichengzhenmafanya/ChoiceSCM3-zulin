package com.choice.assistant.service.purchaseSet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.PurchaseSet.Backbilld;
import com.choice.assistant.domain.PurchaseSet.Backbillm;
import com.choice.assistant.domain.PurchaseSet.Inspectm;
import com.choice.assistant.persistence.purchaseSet.BackbillMapper;
import com.choice.assistant.persistence.purchaseSet.InspectMapper;
import com.choice.assistant.service.bill.PuprOrderService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.AcctConstants;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.util.CalChkNum;

/**
 * 退货单管理
 * @author zgl
 *
 */
@Service
public class BackbillService {
	
	@Autowired
	private BackbillMapper backbillMapper;
	@Autowired
	private PageManager<Backbillm> pageManager;
	@Autowired
	private CoderuleService coderuleService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private InspectMapper inspectMapper;
	@Autowired
	private InspectService inspectService;
	@Autowired
	private AcctService acctService;
	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	private CalChkNum calChkNum;
	private final transient Log log = LogFactory.getLog(BackbillService.class);
	
	/**
	 * 查询所有的退货单主表数据
	 * @return
	 */
	public List<Backbillm> queryAllBackbillm(Backbillm backbillm,Page page) throws CRUDException{
		try {
			if(ValueCheck.IsNotEmpty(backbillm.getPk_backbill())){
				backbillm.setPk_backbill(AsstUtils.StringCodeReplace(backbillm.getPk_backbill()));
			}
			return pageManager.selectPage(backbillm, page, BackbillMapper.class.getName()+".queryAllBackbillm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的退货单子表数据
	 * @return
	 */
	public List<Backbilld> queryAllBackbilld(Backbillm backbillm) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(backbillm.getPk_backbill())){
				return null;
			}
			return backbillMapper.queryAllBackbilld(backbillm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 校验单据号是否重复
	 * @param backbillm
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean findBackbillByCode(Backbillm backbillm) throws CRUDException{
		try {
			if (backbillMapper.findBackbillByCode(backbillm) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增退货单
	 * @param backbillm
	 * @return
	 * @throws CRUDException
	 */
	public String saveBackbill(Backbillm backbillm) throws CRUDException{
		try {
			double money = 0.0;
			backbillm.setPk_backbill(CodeHelper.createUUID());
			backbillm.setDr(0);
			backbillm.setTs(DateFormat.getTs());
			backbillm.setIstate(2);
			backbillm.setPk_inspect(backbillm.getPk_inspectlist());
			backbillm.setVinspectbillno(backbillm.getVinspectvouno());
			Inspectm inspectm  = new Inspectm();
			inspectm.setPk_inspect("'"+backbillm.getPk_inspectlist()+"'");
			inspectm.setAcct(backbillm.getAcct());
			backbillm.setPositncode(inspectService.queryAllInspectm(inspectm, new Page()).get(0).getPositncode());
			
			for (Backbilld backbilld : backbillm.getBackbilldList()) {
				if (backbilld.getNbacknum() == 0.0) {
					continue;
				}
				backbilld.setAcct(backbillm.getAcct());
				backbilld.setPk_backbilld(CodeHelper.createUUID());
				backbilld.setPk_backbill(backbillm.getPk_backbill());
				backbilld.setTs(DateFormat.getTs());
				if (backbilld.getNmoney() != 0.0) {
					backbilld.setNprice(backbilld.getNmoney() / backbilld.getNbacknum());
				} else {
					backbilld.setNmoney(backbilld.getNbacknum() * backbilld.getNprice());
				}
				money += backbilld.getNmoney();
				backbillMapper.saveBackbilld(backbilld);
			}
			backbillm.setNmoney(money);
			backbillMapper.saveBackbillm(backbillm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 跳转到修改页面
	 * @param backbillm
	 * @return
	 * @throws CRUDException
	 */
	public Backbillm toUpdateBackbill(Backbillm backbillm)throws CRUDException{
		try {
			Backbillm backbill = backbillMapper.toUpdateBackbill(backbillm);
			List<Backbilld> backbilldList = backbillMapper.toUpdateBackbilld(backbillm);
			backbill.setBackbilldList(backbilldList);
			return backbill;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改退货单
	 * @param session
	 * @param backbillm
	 * @return
	 * @throws Exception
	 */
	public String updateBackbill(Backbillm backbillm)throws CRUDException{
		try {
			double money = 0.0;
			backbillm.setTs(DateFormat.getTs());
			backbillMapper.deleteBackbilld(backbillm);
			for (Backbilld backbilld : backbillm.getBackbilldList()) {
				backbilld.setAcct(backbillm.getAcct());
				backbilld.setTs(DateFormat.getTs());
				backbilld.setPk_backbill(backbillm.getPk_backbill());
				backbilld.setPk_backbilld(CodeHelper.createUUID());
				backbilld.setTs(DateFormat.getTs());
				if (backbilld.getNmoney() != 0.0) {
					backbilld.setNprice(backbilld.getNmoney() / backbilld.getNbacknum());
				} else {
					backbilld.setNmoney(backbilld.getNbacknum() * backbilld.getNprice());
				}
				money += backbilld.getNmoney();
				backbillMapper.saveBackbilld(backbilld);
			}
			backbillm.setNmoney(money);
			backbillMapper.updateBackbillm(backbillm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除退货单
	 * @param backbillm
	 * @throws CRUDException
	 */
	public void deleteBackBillm(Backbillm backbillm) throws CRUDException{
		try {
			backbillm.setTs(DateFormat.getTs());
			backbillMapper.deleteBackBillm(backbillm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 *  根据主键查询指定的退货单主表、子表
	 * @return
	 */
	public List<Backbillm> queryAllBackbillmByPk(Backbillm backbillm) throws CRUDException{
		try {
			return backbillMapper.queryAllBackbillmByPk(backbillm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 新增、修改保存退货单
	 * @param backbillmJson
	 * @param acct
	 * @param accountId
	 * @return
	 * @throws CRUDException
	 */
	public String saveBackbill(String backbillJson,String acct,String accountId) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//企业ID
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("backbilldList", Backbilld.class);
			Backbillm backbillm = (Backbillm)JsonFormatUtil.formatAggObjectJson(backbillJson, Backbillm.class,childrenClassMap);
			backbillm.setAcct(acct);
			backbillm.setCreator(accountId);
			
			if(ValueCheck.IsNotEmpty(backbillm)){//保存主表
				backbillm.setPk_backbill(Util.getUUID());
				backbillm.setDr(0);
				backbillm.setTs(DateFormat.getTs());
				backbillm.setCreationtime(DateFormat.getTs());
				backbillMapper.saveBackbillm(backbillm);
				//更新子表
				updateBackbilld(backbillm);
			}
			return "1";
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改保存退货单
	 * @param backbillmJson
	 * @param acct
	 * @param accountId
	 * @return
	 * @throws CRUDException
	 */
	public String updateBackbill(String backbillJson,String acct,String accountId) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(backbillJson)){
				return "0";
			}
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//组织不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("backbilldList", Backbilld.class);
			Backbillm backbillm = (Backbillm)JsonFormatUtil.formatAggObjectJson(backbillJson, Backbillm.class,childrenClassMap);
			backbillm.setAcct(acct);
			backbillm.setCreator(accountId);
			
			if(ValueCheck.IsNotEmpty(backbillm)){//保存主表
				if(ValueCheck.IsNotEmpty(backbillm.getPk_backbill())){//如果主键不为空则为修改主表
					List<Backbillm> list = backbillMapper.queryAllBackbillm(backbillm);
					if(list.size()>0){
						if(!"0".equals(list.get(0).getIstate())){
							return "-2";
						}
					}
					backbillMapper.updateBackbillm(backbillm);
				}else{
					return "-3";
				}
				//更新子表
				updateBackbilld(backbillm);
			}
			return "1";
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 先删除子表、再新增子表
	 * @param backbillm
	 * @throws CRUDException
	 */
	public void updateBackbilld(Backbillm backbillm) throws CRUDException{
		if(ValueCheck.IsEmpty(backbillm.getPk_backbill())){
			return;
		}
		backbillMapper.deleteBackbilld(backbillm);
		if(ValueCheck.IsNotEmpty(backbillm.getBackbilldList())){//保存子表
			for(Backbilld backbilld : backbillm.getBackbilldList()){
				backbilld.setPk_backbilld(Util.getUUID());
				backbilld.setPk_backbill(backbillm.getPk_backbill());
				backbilld.setAcct(backbillm.getAcct());
				backbilld.setDr(0);
				backbilld.setTs(DateFormat.getTs());
				backbillMapper.saveBackbilld(backbilld);
			}
		}
	}
	/**
	 * 删除退货单
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public String deleteBackbill(String pk_backbill) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(pk_backbill)){
				return "-1";
			}
			Backbillm backbillm = new Backbillm();
			backbillm.setPk_backbill(AsstUtils.StringCodeReplace(pk_backbill));
			List<Backbillm> list = backbillMapper.queryAllBackbillm(backbillm);
			if(list.size()>0){
				for(Backbillm backbill : list){
					if(!"0".equals(backbill.getIstate())){
						return "-2";
					}
				}
			}
			backbillm.setTs(DateFormat.getTs());
			backbillMapper.deleteBackbill(backbillm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取退货单的最大单号,按照月流水进行
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String selectMaxVbillno(Backbillm backbillm,String group)throws CRUDException{
		try {
			backbillm.setBdate(DateFormat.getStringByDate(DateFormat.getFirstDayOfCurrMonth(),"yyyy-MM-dd"));
			backbillm.setEdate(DateFormat.getStringByDate(DateFormat.getEndDayOfCurrMonth(new Date()), "yyyy-MM-dd"));
			String djno = coderuleService.getCode(CoderuleConstants.BACKBILL,group);
			if (djno == "") {
				List<Backbillm> listBackbillm = backbillMapper.selectMaxVbillno(backbillm);
				if (listBackbillm.size() == 0) {
					return "THDJ"+DateFormat.getStringByDate(new Date(), "yyyyMMdd")+DateFormat.getStringByDate(new Date(), "yyyy-MM-dd").substring(5, 7)+"0001";
				}else {
					String lastValue = (Integer.parseInt(listBackbillm.get(0).getVbillno().substring(listBackbillm.get(0).getVbillno().length()-4)) + 1) +"";
					String value = "";
					if (lastValue.length() == 1) {
						value += "000"+lastValue;
					} else if (lastValue.length() == 2) {
						value += "00"+lastValue;
					} else if (lastValue.length() == 3) {
						value += "0"+lastValue;
					} else if (lastValue.length() == 4) {
						value += lastValue;
					}
					return listBackbillm.get(0).getVbillno().substring(0,listBackbillm.get(0).getVbillno().length() - 4)+value;
				}
				
			}else {
				return djno;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取退货的下一个单号,按照天进行js20161221
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String getNextTHVbillno(Date date,String acct)throws CRUDException{
		try {
			Backbillm backbillm = new Backbillm();
			backbillm.setAcct(acct);
			backbillm.setDbilldate(DateFormat.getStringByDate(date,"yyyy-MM-dd"));
			//得到下一个采购单号
			String vbillno = backbillMapper.getNextTHVbillno(backbillm);
			if(vbillno==null){
				vbillno = "0001";
			}
			else if(vbillno.length()==1){
				vbillno = "000"+vbillno;
			}
			else if(vbillno.length()==2){
				vbillno = "00"+vbillno;
			}
			else if(vbillno.length()==3){
				vbillno = "0"+vbillno;
			}
			SimpleDateFormat sif = new SimpleDateFormat("yyyyMMdd");
			vbillno = "TH"+sif.format(date)+PuprOrderService.getRandomStr(3)+vbillno;
			return vbillno;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据验货单查询所有退货单
	 * @param backbillm
	 * @return
	 */
	public List<String> queryBackbillmInspect(Backbillm backbillm) throws CRUDException{
		try {
			return backbillMapper.queryBackbillmInspect(backbillm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 生成入库单并审核
	 * @param backbillm
	 * @return
	 */
	public String saveInorderAdCk(Backbillm backbillm, String accountName) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try {
			Backbillm backbill = backbillMapper.toUpdateBackbill(backbillm);
			List<Backbilld> backbilldList = backbillMapper.toUpdateBackbilld(backbillm);
			//得到退货入库单号
			String pk_inspect = backbill.getPk_inspect();
			Inspectm insm = new Inspectm();
			insm.setAcct(backbill.getAcct());
			insm.setPk_inspect(pk_inspect);
			insm = inspectMapper.toUpdateInspectm(insm);
			int thdh = insm.getChkno();
			Chkind cdc = new Chkind();
			cdc.setChkno(thdh);
			Supply spc = new Supply();
			cdc.setSupply(spc);
			//日期，制单
			Date date=new Date();
			//入库日期
			Date rkDate = DateFormat.getDateByString(backbill.getDbilldate(), "yyyy-MM-dd");
			Chkinm chkinm = new Chkinm();
			chkinm.setAcct(backbill.getAcct());
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMapper.getMaxChkinno());
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,rkDate));
			chkinm.setMaded(rkDate);
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			Positn pos = new Positn();
			pos.setCode(insm.getPositncode());
			chkinm.setPositn(pos);
			Deliver del = new Deliver();
			del.setCode(backbill.getDelivercode());
			chkinm.setDeliver(del);
			chkinm.setMadeby(accountName);
			chkinm.setTotalamt(new Double(backbill.getNmoney()));
			//chkinm.setPount(0);
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setMadedto(date);
			chkinm.setTyp("9944");//采购退货
//			chkinm.setTyp(ChkinmConstants.pur);
			chkinm.setMemo(backbill.getVmemo());
			//未审核
			chkinm.setIstate(1);
			log.warn("添加入库单(主单)：\n"+chkinm);
			inspectMapper.saveChkinmCg(chkinm);
	 		log.warn("添加入库单(详单)：");
			for (int i = 0; i < backbilldList.size(); i++) {
				Backbilld bbd = backbilldList.get(i);
				Chkind chkind = new Chkind();
//				chkind.setAcct(inspectms.getAcct());
//				chkind.setYearr(chkinm.getYearr());
				chkind.setChkinno(chkinm.getChkinno());
				Supply sup = new Supply();
				sup.setSp_code(bbd.getSp_code());
				chkind.setSupply(sup);
				chkind.setAmount(new Double(bbd.getNbacknum()));
				if(bbd.getTax()==null){
					bbd.setTax(0.0);
				}
				chkind.setPrice(new Double(bbd.getNprice()));
				chkind.setMemo(bbd.getVmemo());
				chkind.setDued(date);
				chkind.setInout(ChkinmConstants.in);//采购入库标识
				chkind.setPound(0);
				chkind.setBatchid(0);
				chkind.setFirm("");
				chkind.setChkstono(0);
				chkind.setAmount1(0);
				chkind.setExtim(0);
				chkind.setPcno(bbd.getIbackbatch().toString());
				cdc.getSupply().setSp_code(bbd.getSp_code());
				chkind.setSp_id(backbillMapper.getSpidByChknoAndSp(cdc));
				chkind.setQstate(2);
				chkind.setTax(bbd.getTax());
				chkind.setTaxdes(bbd.getTaxdes());
				chkind.setChkno(thdh);
				//chkind.setTotalamt(new Double(inspectdlist.get(i).getNmoney()));
				//chkind.setQstate(backbilldList.get(i).getQstate());合格状态 暂时没有
				//chkind.setNoreason(backbilldList.get(i).vinspectvouno);验货单单号主键 暂时没用
				log.warn(chkind);
				inspectMapper.saveChkindCg(chkind);
			}
			//return chkinm.getPr();
			if(chkinm.getPr()==0){
				result.put("pr", "-11");	
				JSONObject rs = JSONObject.fromObject(result);
				throw new CRUDException(rs.toString());
			}
			//判断
			try {
				acctService.getOnlyAccountMonth(chkinm.getMaded());//  此字段当成月份使用    加入会计月 
			} catch (Exception e) {
				 //未年结转
				 if(e.getMessage().equals(AcctConstants.NOT_NIAN_JIE_ZHUAN)){
					result.put("pr", "-9");	
					JSONObject rs = JSONObject.fromObject(result);
					throw new CRUDException(rs.toString());
				 }
				 //不能操作当前会计月之前的数据
				 if(e.getMessage().equals(AcctConstants.CAN_NOT_CHECK_BEFORE_DATA)){
					result.put("pr", "-4");	
					JSONObject rs = JSONObject.fromObject(result);
					throw new CRUDException(rs.toString());
				 }
			}
			//审核入库单
			chkinm.setChecby(accountName);
			String checkResult = chkinmService.checkChkinm(chkinm,null);
			//判断审核是否成功
			JSONObject obj = JSONObject.fromObject(checkResult);
			String checkPr = String.valueOf(obj.get("pr"));
			//审核成功
			//返回
			//result.put("pr", "1");
			//JSONObject rs = JSONObject.fromObject(result);
			//return rs.toString();
			if("1".equals(checkPr)){
				//回写退货入库单号和入库凭证号和入库状态
				Backbillm bbm = new Backbillm();
				bbm.setVouno(chkinm.getVouno());
				bbm.setChkno(chkinm.getChkinno());
				bbm.setPk_backbill(backbill.getPk_backbill());
				bbm.setVbillno(backbill.getVbillno());
				bbm.setAcct(backbill.getAcct());
				backbillMapper.updateBackbillmVounoAndChkno(bbm);
				//返回
				result.put("pr", "1");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}else{
				return checkResult;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}
	}
}
