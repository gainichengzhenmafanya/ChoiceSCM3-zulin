package com.choice.assistant.service.purchaseSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.PurchaseSet.Inspectd;
import com.choice.assistant.domain.PurchaseSet.Inspectm;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.persistence.purchaseSet.InspectMapper;
import com.choice.assistant.service.bill.PuprOrderService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.constants.AcctConstants;
import com.choice.scm.constants.ChkinmConstants;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.persistence.ChkinmMapper;
import com.choice.scm.persistence.MainInfoMapper;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.ChkinmService;
import com.choice.scm.util.CalChkNum;

/**
 * 验货单管理
 * @author zgl
 *
 */
@Service
public class InspectService {
	
	@Autowired
	private InspectMapper inspectMapper;
	@Autowired
	private PageManager<Inspectm> pageManager;
	@Autowired
	private CoderuleService coderuleService;
	@Autowired
	private MainInfoMapper mainInfoMapper;
	@Autowired
	private ChkinmMapper chkinmMapper;
	@Autowired
	private ChkinmService chkinmService;
	@Autowired
	private AcctService acctService;
	@Autowired
	private CalChkNum calChkNum;
	@Autowired
	private PuprOrderService puprOrderService;
	private final transient Log log = LogFactory.getLog(InspectService.class);
	
	/**
	 * 查询所有的验货单主表数据
	 * @return
	 */
	public List<Inspectm> queryAllInspectm(Inspectm inspectm,Page page) throws CRUDException{
		try {
			if(ValueCheck.IsNotEmpty(inspectm.getPk_inspect())){
				inspectm.setPk_inspect(AsstUtils.StringCodeReplace(inspectm.getPk_inspect()));
			}
			return pageManager.selectPage(inspectm, page, InspectMapper.class.getName()+".queryAllInspectm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询所有的验货单主表数据（未确认到货）
	 * @return
	 */
	public List<Inspectm> queryAllInspectmDo(Inspectm inspectm,Page page) throws CRUDException{
		try {
			if(ValueCheck.IsNotEmpty(inspectm.getPk_inspect())){
				inspectm.setPk_inspect(AsstUtils.StringCodeReplace(inspectm.getPk_inspect()));
			}
			return pageManager.selectPage(inspectm, page, InspectMapper.class.getName()+".queryAllInspectmDo");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据订单查询验货单
	 * @return
	 */
	public List<String> queryInspectmPuprorder(Inspectm inspectm) throws CRUDException{
		try {
			return inspectMapper.queryInspectmPuprorder(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的验货单子表数据
	 * @return
	 */
	public List<Inspectd> queryAllInspectd(Inspectm inspectm) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(inspectm.getPk_inspect())){
				return null;
			}
			return inspectMapper.queryAllInspectd(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 *  根据主键查询指定的验货单主表、子表
	 * @return
	 */
	public List<Inspectm> queryAllInspectmByPk(Inspectm inspectm) throws CRUDException{
		try {
			return inspectMapper.queryAllInspectmByPk(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 校验单据号是否重复
	 * @param inspectm
	 * @return
	 * @throws CRUDException
	 */
	public boolean findInspectByCode(Inspectm inspectm) throws CRUDException{
		try {
			if (inspectMapper.findInspectByCode(inspectm) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增保存验货单
	 * @param inspectJson
	 * @param acct
	 * @param accountId
	 * @throws CRUDException
	 */
	public String saveInspect(Inspectm inspectm) throws CRUDException{
		try {
			double money = 0.0;
			inspectm.setPk_inspect(CodeHelper.createUUID());
			inspectm.setDr(0);
			inspectm.setIstate(2);
			inspectm.setTs(DateFormat.getTs());
			for (Inspectd inspectd : inspectm.getInspectdList()) {
				if (inspectd.getNinsnum() == 0.0) {
					continue;
				}
				inspectd.setPk_inspect(inspectm.getPk_inspect());
				inspectd.setPk_inspectd(CodeHelper.createUUID());
				inspectd.setAcct(inspectm.getAcct());
				inspectd.setTs(DateFormat.getTs());
				if (inspectd.getNmoney() != 0) {
					inspectd.setNprice(inspectd.getNmoney() / inspectd.getNinsnum());
					inspectd.setNdiffnum(inspectd.getNpurnum() - inspectd.getNinsnum());
					inspectd.setNdiffmoney(inspectd.getNdiffnum() * inspectd.getNprice());
				} else {
					inspectd.setNmoney(inspectd.getNinsnum() * inspectd.getNprice());
				}
				money += inspectd.getNmoney();
				inspectMapper.saveInspectd(inspectd);
			}
			inspectm.setNmoney(money);
			inspectMapper.saveInspectm(inspectm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 删除验货单
	 * @param inspectm
	 * @return
	 * @throws CRUDException
	 */
	public String deleteInspectm(Inspectm inspectm) throws CRUDException{
		try {
			inspectMapper.deleteInspectm(inspectm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 跳转到修改验货单的页面===根据主键查询主表信息
	 * @param inspectm
	 * @return
	 * @throws CRUDException
	 */
	public Inspectm toUpdateInspectm(Inspectm inspectm) throws CRUDException{
		try {
			return inspectMapper.toUpdateInspectm(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改验货单
	 * @param inspectm
	 * @throws CRUDException
	 */
	public String updateInspectm(Inspectm inspectm)throws CRUDException{
		try {
			List<String> pklist = new ArrayList<String>();
			double money = 0.0;
			inspectm.setTs(DateFormat.getTs());
			inspectMapper.deleteInspectd(inspectm);
			for (Inspectd inspectd : inspectm.getInspectdList()) {
				inspectd.setAcct(inspectm.getAcct());
				inspectd.setTs(DateFormat.getTs());
				inspectd.setPk_inspectd(CodeHelper.createUUID());
				inspectd.setPk_inspect(inspectm.getPk_inspect());
				inspectd.setTs(DateFormat.getTs());
				if (inspectd.getNmoney() != 0) {
					inspectd.setNprice(inspectd.getNmoney() / inspectd.getNinsnum());
					inspectd.setNdiffnum(inspectd.getNpurnum() - inspectd.getNinsnum());
					inspectd.setNdiffmoney(inspectd.getNdiffnum() * inspectd.getNprice());
				} else {
					inspectd.setNmoney(inspectd.getNinsnum() * inspectd.getNprice());
				}
				pklist.add(inspectd.getPk_puprorder());
				money += inspectd.getNmoney();
				inspectMapper.saveInspectd(inspectd);
			}
			inspectm.setNmoney(money);
			inspectMapper.updateInspectm(inspectm);
//			//根据验货单主表获取子表中采购订单的主键
//			PuprOrder puprOrder = new PuprOrder();
//			puprOrder.setPklsit(pklist);
//			puprOrder.setAcct(inspectm.getAcct());
//			puprOrderService.updatePuprOrdermStat(puprOrder);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改保存验货单
	 * @param inspectJson
	 * @param acct
	 * @throws CRUDException
	 */
	public String updateInspect(String inspectJson,String acct) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(inspectJson)){
				return "0";//传入参数为空
			}
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//企业Id不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("inspectdList", Inspectd.class);
			Inspectm inspectm = (Inspectm)JsonFormatUtil.formatAggObjectJson(inspectJson, Inspectm.class,childrenClassMap);
			inspectm.setAcct(acct);
			inspectm.setTs(DateFormat.getTs());
			if(ValueCheck.IsNotEmpty(inspectm)){//保存主表
				if(ValueCheck.IsNotEmpty(inspectm.getPk_inspect())){
					//如果主键不为空则为修改主表
					List<Inspectm> list = inspectMapper.queryAllInspectm(inspectm);
					if(list.size()>0){
						if(!"0".equals(list.get(0).getIstate())){
							return "-2";//验货单如果已经提交不让修改
						}
					}
					inspectMapper.updateInspectm(inspectm);
				}else{
					return "-3";//主键为空
				}
				//更新子表
				updateInspectd(inspectm);
			}else{
				return "0";//传入参数为空
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 先删除子表、再新增子表
	 * @param inspectm
	 * @throws CRUDException
	 */
	public void updateInspectd(Inspectm inspectm) throws CRUDException{
		if(ValueCheck.IsEmpty(inspectm.getPk_inspect())){
			return;
		}
		inspectMapper.deleteInspectd(inspectm);
		if(ValueCheck.IsNotEmpty(inspectm.getInspectdList())){//保存子表
			for(Inspectd inspectd : inspectm.getInspectdList()){
				inspectd.setPk_inspectd(Util.getUUID());
				inspectd.setPk_inspect(inspectm.getPk_inspect());
				inspectd.setAcct(inspectm.getAcct());
				inspectd.setDr(0);
				inspectd.setTs(DateFormat.getTs());
				inspectMapper.saveInspectd(inspectd);
			}
		}
	}
	/**
	 * 删除验货单
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public String deleteInspect(String pk_inspect) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(pk_inspect)){
				return "0";
			}
			Inspectm inspectm = new Inspectm();
			inspectm.setPk_inspect(AsstUtils.StringCodeReplace(pk_inspect));
			inspectm.setTs(DateFormat.getTs());
			inspectMapper.deleteInspect(inspectm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据验货单主键集合查询子表信息
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public List<Inspectd> selectInspectdByPK(Inspectm inspectm)throws CRUDException{
		try {
			return inspectMapper.selectInspectdByPK(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取验货单的最大单号,按照月流水进行
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String selectMaxVbillno(Inspectm inspectm,String group)throws CRUDException{
		try {
			inspectm.setBdate(DateFormat.getStringByDate(DateFormat.getFirstDayOfCurrMonth(),"yyyy-MM-dd"));
			inspectm.setEdate(DateFormat.getStringByDate(DateFormat.getEndDayOfCurrMonth(new Date()), "yyyy-MM-dd"));
			String djno = coderuleService.getCode(CoderuleConstants.INSPECT,group);
			if (djno == "") {
				List<Inspectm> listInspectm = inspectMapper.selectMaxVbillno(inspectm);
				if (listInspectm.size() == 0) {
					return "YHDJ"+DateFormat.getStringByDate(new Date(), "yyyyMMdd")+DateFormat.getStringByDate(new Date(), "yyyy-MM-dd").substring(5, 7)+"0001";
				}else {
					String lastValue = (Integer.parseInt(listInspectm.get(0).getVbillno().substring(listInspectm.get(0).getVbillno().length()-4)) + 1) +"";
					String value = "";
					if (lastValue.length() == 1) {
						value += "000"+lastValue;
					} else if (lastValue.length() == 2) {
						value += "00"+lastValue;
					} else if (lastValue.length() == 3) {
						value += "0"+lastValue;
					} else if (lastValue.length() == 4) {
						value += lastValue;
					}
					return listInspectm.get(0).getVbillno().substring(0,listInspectm.get(0).getVbillno().length() - 4)+value;
				}
			}else {
				return djno;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取下一个验货单号js20161217
	 * @param date
	 * @return
	 * @throws CRUDException
	 */
	public String getNextVbillno(Date date,String acct)throws CRUDException{
		try {
			Inspectm inspectm = new Inspectm();
			inspectm.setAcct(acct);
			inspectm.setDbilldate(DateFormat.getStringByDate(date,"yyyy-MM-dd"));
			//得到下一个验货单号
			String vbillno = inspectMapper.getNextVbillno(inspectm);
			if(vbillno==null){
				vbillno = "0001";
			}
			else if(vbillno.length()==1){
				vbillno = "000"+vbillno;
			}
			else if(vbillno.length()==2){
				vbillno = "00"+vbillno;
			}
			else if(vbillno.length()==3){
				vbillno = "0"+vbillno;
			}
			SimpleDateFormat sif = new SimpleDateFormat("yyyyMMddHH");
			vbillno = "YHDJ"+sif.format(date)+vbillno;
			return vbillno;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询前10条验货单
	 * @return
	 */
	public List<Inspectm> findInspectmTop(Inspectm inspectm) throws CRUDException{
		try {
			return inspectMapper.findInspectmTop(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 根据采购订单主键查询验货单明细
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public List<Inspectd> queryInspectdByPuprorder(Inspectd inspectd) throws CRUDException{
		try {
			return inspectMapper.queryInspectdByPuprorder(inspectd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 生成入库单，选择入库日期
	 * @param inspectm
	 * @param accountName
	 * @return
	 * @throws CRUDException
	 */
	public int saveinorder(Inspectm inspectm, String accountName) throws CRUDException{
		try {
			Inspectm inspectms = inspectMapper.toUpdateInspectm(inspectm);
			List<Inspectd> inspectdlist = inspectMapper.selectInspectdByPKS(inspectms.getAcct(),inspectms.getPk_inspect());
			//得到选择的日期
			String dateStr=inspectm.getDbilldate();
			Date date = new Date();
			if(dateStr!=null&&dateStr.length()>0){
				date = DateFormat.getDateByString(dateStr, "yyyy-MM-dd");
			}
			Chkinm chkinm = new Chkinm();
			chkinm.setAcct(inspectms.getAcct());
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMapper.getMaxChkinno());
			chkinm.setVouno(inspectms.getVbillno());
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(date,"yyyy-MM-dd HH:mm:ss"));
			Positn pos = new Positn();
			pos.setCode(inspectms.getPositncode());
			chkinm.setPositn(pos);
			Deliver del = new Deliver();
			del.setCode(inspectms.getDelivercode());
			chkinm.setDeliver(del);
			chkinm.setMadeby(accountName);
			chkinm.setTotalamt(new Double(inspectms.getNmoney()));
			//chkinm.setPount(0);
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setMadedto(date);
			chkinm.setTyp("9928");
//			chkinm.setTyp(ChkinmConstants.pur);
			chkinm.setMemo(inspectms.getVmemo());
			//未审核
			chkinm.setIstate(1);
			log.warn("添加入库单(主单)：\n"+chkinm);
			inspectMapper.saveChkinmCg(chkinm);
	 		log.warn("添加入库单(详单)：");
	 		Inspectd inspectdd;
			for (int i = 0; i < inspectdlist.size(); i++) {
				inspectdd = inspectdlist.get(i);
				Chkind chkind = new Chkind();
//				chkind.setAcct(inspectms.getAcct());
//				chkind.setYearr(chkinm.getYearr());
				//入库单号
				chkind.setChkinno(chkinm.getChkinno());
				Supply sup = new Supply();
				sup.setSp_code(inspectdd.getSp_code());
				chkind.setSupply(sup);
				chkind.setAmount(new Double(inspectdd.getNinsnum()));
				chkind.setPrice(new Double(inspectdd.getNprice()));
				chkind.setMemo(inspectdd.getVmemo());
				chkind.setDued(date);
				chkind.setInout(ChkinmConstants.in);//采购入库标识
				chkind.setPound(0);
				chkind.setBatchid(0);
				chkind.setFirm("");
				chkind.setChkstono(0);
				chkind.setAmount1(0);
				chkind.setExtim(0);
				chkind.setPcno(inspectdd.getPcno());
				//chkind.setTotalamt(new Double(inspectdlist.get(i).getNmoney()));
				chkind.setQstate(inspectdd.getQstate());
				chkind.setNoreason(inspectdd.getVinspectname());
				//税率
				chkind.setTax(inspectdd.getTax());
				chkind.setTaxdes(inspectdd.getTaxdes());
				log.warn(chkind);
				inspectMapper.saveChkindCg(chkind);
			}
			return chkinm.getPr();
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 生成入库单，并审核
	 * @param inspectm
	 * @param accountName
	 * @return
	 * @throws CRUDException
	 */
	public String saveInorderAndCheck(Inspectm inspectm, String accountName) throws CRUDException{
		Map<String,String> result = new HashMap<String,String>();
		try {
			//更新入库状态
			//this.updateInspectmIstate(inspectm);
			//得到主表信息
			Inspectm inspectms = inspectMapper.toUpdateInspectm(inspectm);
			List<Inspectd> inspectdlist = inspectMapper.selectInspectdByPKS(inspectms.getAcct(),inspectms.getPk_inspect());
			//得到选择的日期
			String dateStr=inspectm.getDbilldate();
			Date date = new Date(),date1 = new Date();
			if(dateStr!=null&&dateStr.length()>0){
				date = DateFormat.getDateByString(dateStr, "yyyy-MM-dd");
			}
			Chkinm chkinm = new Chkinm();
			chkinm.setAcct(inspectms.getAcct());
			chkinm.setYearr(mainInfoMapper.findYearrList().get(0)+"");
			chkinm.setChkinno(chkinmMapper.getMaxChkinno());
			//chkinm.setVouno(inspectms.getVbillno());
			//生成入库凭证号
			chkinm.setVouno(calChkNum.getNext(CalChkNum.CHKIN,date));
			chkinm.setMaded(date);
			chkinm.setMadet(DateFormat.getStringByDate(date1,"yyyy-MM-dd HH:mm:ss"));
			Positn pos = new Positn();
			pos.setCode(inspectms.getPositncode());
			chkinm.setPositn(pos);
			Deliver del = new Deliver();
			del.setCode(inspectms.getDelivercode());
			chkinm.setDeliver(del);
			chkinm.setMadeby(accountName);
			chkinm.setTotalamt(new Double(inspectms.getNmoney()));
			//chkinm.setPount(0);
			chkinm.setInout(ChkinmConstants.in);
			chkinm.setMadedto(date);
			chkinm.setTyp("9928");
//			chkinm.setTyp(ChkinmConstants.pur);
			chkinm.setMemo(inspectms.getVmemo());
			//未审核
			chkinm.setIstate(1);
			log.warn("添加入库单(主单)：\n"+chkinm);
			inspectMapper.saveChkinmCg(chkinm);
	 		log.warn("添加入库单(详单)：");
	 		Inspectd inspectdd;
			for (int i = 0; i < inspectdlist.size(); i++) {
				inspectdd = inspectdlist.get(i);
				Chkind chkind = new Chkind();
//				chkind.setAcct(inspectms.getAcct());
//				chkind.setYearr(chkinm.getYearr());
				//入库单号
				chkind.setChkinno(chkinm.getChkinno());
				Supply sup = new Supply();
				sup.setSp_code(inspectdd.getSp_code());
				chkind.setSupply(sup);
				chkind.setAmount(new Double(inspectdd.getNinsnum()));
				chkind.setPrice(new Double(inspectdd.getNprice()));
				chkind.setMemo(inspectdd.getVmemo());
				chkind.setDued(date);
				chkind.setInout(ChkinmConstants.in);//采购入库标识
				chkind.setPound(0);
				chkind.setBatchid(0);
				chkind.setFirm("");
				chkind.setChkstono(0);
				chkind.setAmount1(0);
				chkind.setExtim(0);
				chkind.setPcno(inspectdd.getPcno());
				//chkind.setTotalamt(new Double(inspectdlist.get(i).getNmoney()));
				chkind.setQstate(inspectdd.getQstate());
				chkind.setNoreason(inspectdd.getVinspectname());
				//税率
				chkind.setTax(inspectdd.getTax());
				chkind.setTaxdes(inspectdd.getTaxdes());
				log.warn(chkind);
				inspectMapper.saveChkindCg(chkind);
			}
			//return chkinm.getPr();
			if(chkinm.getPr()==0){
				result.put("pr", "-11");	
				JSONObject rs = JSONObject.fromObject(result);
				throw new CRUDException(rs.toString());
			}
			//判断
			try {
				acctService.getOnlyAccountMonth(chkinm.getMaded());//  此字段当成月份使用    加入会计月 
			} catch (Exception e) {
				 //未年结转
				 if(e.getMessage().equals(AcctConstants.NOT_NIAN_JIE_ZHUAN)){
					result.put("pr", "-9");	
					JSONObject rs = JSONObject.fromObject(result);
					throw new CRUDException(rs.toString());
				 }
				 //不能操作当前会计月之前的数据
				 if(e.getMessage().equals(AcctConstants.CAN_NOT_CHECK_BEFORE_DATA)){
					result.put("pr", "-4");	
					JSONObject rs = JSONObject.fromObject(result);
					throw new CRUDException(rs.toString());
				 }
			}
			//审核入库单
			chkinm.setChecby(accountName);
			String checkResult = chkinmService.checkChkinm(chkinm,null);
			//判断审核是否成功
			JSONObject obj = JSONObject.fromObject(checkResult);
			String checkPr = String.valueOf(obj.get("pr"));
			//审核成功
			if("1".equals(checkPr)){
				//回写采购入库单号和入库凭证号和入库状态
				Inspectm ins = new Inspectm();
				ins.setVouno(chkinm.getVouno());
				ins.setChkno(chkinm.getChkinno());
				ins.setPk_inspect(inspectms.getPk_inspect());
				ins.setVbillno(inspectms.getVbillno());
				ins.setAcct(inspectms.getAcct());
				inspectMapper.updateInspectmVounoAndChkno(ins);
				//更新订单状态
				PuprOrder puprOrder = new PuprOrder();
				puprOrder.setPk_puprorder(inspectms.getPk_puprorder());
				puprOrder.setIstate(6);
				puprOrder.setTs(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
				puprOrderService.updatePuprOrdersState(puprOrder);
				//返回
				result.put("pr", "1");
				JSONObject rs = JSONObject.fromObject(result);
				return rs.toString();
			}else{
				return checkResult;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e.getMessage());
		}

	}
	/**
	 * 根据入库单主键集合查询子表信息
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkind> selectInOrderByPK(Chkinm chkinm)throws CRUDException{
		try {
			return inspectMapper.selectInOrderByPK(chkinm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据订单号查询仓位（采购组织）信息
	 * @return
	 */
	public String queryOrderPos(String acct,String vbillno) throws CRUDException{
		try {
			return inspectMapper.queryOrderPos(acct,vbillno);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 根据订单号查询单价信息
	 * @return
	 */
	public String queryOrderPrice(String acct,String vbillno,String sp_code) throws CRUDException{
		try {
			return inspectMapper.queryOrderPrice(acct,vbillno,sp_code);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改验货单验货数量与金额
	 * @param inspectm
	 * @throws CRUDException
	 */
	public String updateInspectdnum(Inspectd inspectd)throws CRUDException{
		try {
			
			inspectMapper.updateInspectdnum(inspectd);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改验货单状态
	 * @throws CRUDException
	 */
	public void updateInspectmIstate(Inspectm inspectm)throws CRUDException{
		try {
			inspectMapper.updateInspectmIstate(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
