package com.choice.assistant.service.purchaseSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.PurchaseSet.Inspectd;
import com.choice.assistant.domain.PurchaseSet.Inspectm;
import com.choice.assistant.persistence.purchaseSet.InspectMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 验货单管理
 * @author zgl
 *
 */
@Service
public class InspectServiceImpl {
	
	@Autowired
	private InspectMapper inspectMapper;
	@Autowired
	private PageManager<Inspectm> pageManager;
	
	private final transient Log log = LogFactory.getLog(InspectServiceImpl.class);
	
	/**
	 * 查询所有的验货单主表数据
	 * @return
	 */
	public List<Inspectm> queryAllInspectm(Inspectm inspectm,Page page) throws CRUDException{
		try {
			if(ValueCheck.IsNotEmpty(inspectm.getPk_inspect())){
				inspectm.setPk_inspect(AsstUtils.StringCodeReplace(inspectm.getPk_inspect()));
			}
			return pageManager.selectPage(inspectm, page, InspectMapper.class.getName()+".queryAllInspectm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的验货单子表数据
	 * @return
	 */
	public List<Inspectd> queryAllInspectd(Inspectm inspectm) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(inspectm.getPk_inspect())){
				return null;
			}
			return inspectMapper.queryAllInspectd(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 *  根据主键查询指定的验货单主表、子表
	 * @return
	 */
	public List<Inspectm> queryAllInspectmByPk(Inspectm inspectm) throws CRUDException{
		try {
			return inspectMapper.queryAllInspectmByPk(inspectm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 新增保存验货单
	 * @param inspectJson
	 * @param acct
	 * @param accountId
	 * @throws CRUDException
	 */
	public String saveInspect(String inspectJson,String acct,String accountId) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(inspectJson)){
				return "0";
			}
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//组织不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("inspectdList", Inspectd.class);
			Inspectm inspectm = (Inspectm)JsonFormatUtil.formatAggObjectJson(inspectJson, Inspectm.class,childrenClassMap);
			inspectm.setAcct(acct);
			inspectm.setCreator(accountId);
			if(ValueCheck.IsNotEmpty(inspectm)){//保存主表
				inspectm.setPk_inspect(Util.getUUID());
				inspectm.setDr(0);
				inspectm.setTs(DateFormat.getTs());
				inspectm.setCreationtime(DateFormat.getTs());
				inspectMapper.saveInspectm(inspectm);
				//更新子表
				updateInspectd(inspectm);
			}else{
				return "0";
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	/**
	 * 修改保存验货单
	 * @param inspectJson
	 * @param acct
	 * @throws CRUDException
	 */
	public String updateInspect(String inspectJson,String acct) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(inspectJson)){
				return "0";//传入参数为空
			}
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//组织不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("inspectdList", Inspectd.class);
			Inspectm inspectm = (Inspectm)JsonFormatUtil.formatAggObjectJson(inspectJson, Inspectm.class,childrenClassMap);
			inspectm.setAcct(acct);
			inspectm.setTs(DateFormat.getTs());
			if(ValueCheck.IsNotEmpty(inspectm)){//保存主表
				if(ValueCheck.IsNotEmpty(inspectm.getPk_inspect())){
					//如果主键不为空则为修改主表
					List<Inspectm> list = inspectMapper.queryAllInspectm(inspectm);
					if(list.size()>0){
						if(!"0".equals(list.get(0).getIstate())){
							return "-2";//验货单如果已经提交不让修改
						}
					}
					inspectMapper.updateInspectm(inspectm);
				}else{
					return "-3";//主键为空
				}
				//更新子表
				updateInspectd(inspectm);
			}else{
				return "0";//传入参数为空
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 先删除子表、再新增子表
	 * @param inspectm
	 * @throws CRUDException
	 */
	public void updateInspectd(Inspectm inspectm) throws CRUDException{
		if(ValueCheck.IsEmpty(inspectm.getPk_inspect())){
			return;
		}
		inspectMapper.deleteInspectd(inspectm);
		if(ValueCheck.IsNotEmpty(inspectm.getInspectdList())){//保存子表
			for(Inspectd inspectd : inspectm.getInspectdList()){
				inspectd.setPk_inspectd(Util.getUUID());
				inspectd.setPk_inspect(inspectm.getPk_inspect());
				inspectd.setAcct(inspectm.getAcct());
				inspectd.setDr(0);
				inspectd.setTs(DateFormat.getTs());
				inspectMapper.saveInspectd(inspectd);
			}
		}
	}
	/**
	 * 删除验货单
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public String deleteInspect(String pk_inspect) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(pk_inspect)){
				return "0";
			}
			Inspectm inspectm = new Inspectm();
			inspectm.setPk_inspect(AsstUtils.StringCodeReplace(pk_inspect));
			inspectm.setTs(DateFormat.getTs());
			inspectMapper.deleteInspect(inspectm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	

}
