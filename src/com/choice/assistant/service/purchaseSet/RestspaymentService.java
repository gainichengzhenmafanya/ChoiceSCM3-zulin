package com.choice.assistant.service.purchaseSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.PurchaseSet.Restspaymentd;
import com.choice.assistant.domain.PurchaseSet.Restspaymentm;
import com.choice.assistant.persistence.purchaseSet.RestspaymentMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 其他应付款管理
 * @author zgl
 *
 */
@Service
public class RestspaymentService {
	
	@Autowired
	private RestspaymentMapper restspaymentMapper;
	@Autowired
	private PageManager<Restspaymentm> pageManager;
	
	private final transient Log log = LogFactory.getLog(RestspaymentService.class);
	
	/**
	 * 查询所有的其他应付款主表数据
	 * @return
	 */
	public List<Restspaymentm> queryAllRestspaymentm(Restspaymentm restspaymentm,Page page) throws CRUDException{
		try {
			if(ValueCheck.IsNotEmpty(restspaymentm.getPk_restspayment())){
				restspaymentm.setPk_restspayment(AsstUtils.StringCodeReplace(restspaymentm.getPk_restspayment()));
			}
			return pageManager.selectPage(restspaymentm, page, RestspaymentMapper.class.getName()+".queryAllRestspaymentm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的其他应付款子表数据
	 * @return
	 */
	public List<Restspaymentd> queryAllRestspaymentd(Restspaymentm restspaymentm) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(restspaymentm.getPk_restspayment())){
				return null;
			}
			return restspaymentMapper.queryAllRestspaymentd(restspaymentm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 *  根据主键查询指定的其他应付款主表、子表
	 * @return
	 */
	public List<Restspaymentm> queryAllRestspaymentmByPk(Restspaymentm restspaymentm) throws CRUDException{
		try {
			return restspaymentMapper.queryAllRestspaymentmByPk(restspaymentm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 新增、修改保存其他应付款
	 * @return
	 */
	public String saveRestspayment(String restspaymentJson,String acct,String accountId) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(restspaymentJson)){
				return "0";
			}
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//组织不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("restspaymentdList", Restspaymentd.class);
			Restspaymentm restspaymentm = (Restspaymentm)JsonFormatUtil.formatAggObjectJson(restspaymentJson, Restspaymentm.class,childrenClassMap);
			restspaymentm.setAcct(acct);
			restspaymentm.setCreator(accountId);
			restspaymentm.setTs(DateFormat.getTs());
			if(ValueCheck.IsNotEmpty(restspaymentm)){//保存主表
				restspaymentm.setPk_restspayment(Util.getUUID());
				restspaymentm.setDr(0);
				restspaymentm.setTs(DateFormat.getTs());
				restspaymentm.setCreationtime(DateFormat.getTs());
				restspaymentMapper.saveRestspaymentm(restspaymentm);
				//更新子表
				updateRestspaymentd(restspaymentm);
			}else{
				return "0";
			}
			return "1";
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 修改保存其他应付款
	 * @return
	 */
	public String updateRestspayment(String restspaymentJson,String acct) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(restspaymentJson)){
				return "0";
			}
			if(ValueCheck.IsEmpty(acct)){
				return "-1";//组织不能为空
			}
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("restspaymentdList", Restspaymentd.class);
			Restspaymentm restspaymentm = (Restspaymentm)JsonFormatUtil.formatAggObjectJson(restspaymentJson, Restspaymentm.class,childrenClassMap);
			restspaymentm.setAcct(acct);
			restspaymentm.setTs(DateFormat.getTs());
			
			if(ValueCheck.IsNotEmpty(restspaymentm)){//保存主表
				if(ValueCheck.IsNotEmpty(restspaymentm.getPk_restspayment())){//如果主键不为空则为修改主表
					restspaymentMapper.updateRestspaymentm(restspaymentm);
				}else{
					return "-3";
				}
				//更新子表
				updateRestspaymentd(restspaymentm);
			}else{
				return "0";
			}
			return "1";
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 先删除子表、再新增子表
	 * @param restspaymentm
	 * @throws CRUDException
	 */
	public void updateRestspaymentd(Restspaymentm restspaymentm) throws CRUDException{
		if(ValueCheck.IsEmpty(restspaymentm.getPk_restspayment())){
			return;
		}
		restspaymentMapper.deleteRestspaymentd(restspaymentm);
		if(ValueCheck.IsNotEmpty(restspaymentm.getRestspaymentdList())){//保存子表
			for(Restspaymentd restspaymentd : restspaymentm.getRestspaymentdList()){
				restspaymentd.setPk_restspayment_sub(Util.getUUID());
				restspaymentd.setPk_restspayment(restspaymentm.getPk_restspayment());
				restspaymentd.setAcct(restspaymentm.getAcct());
				restspaymentd.setDr(0);
				restspaymentd.setTs(DateFormat.getTs());
				restspaymentMapper.saveRestspaymentd(restspaymentd);
			}
		}
	}
	/**
	 * 删除其他应付款
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public String deleteRestspayment(String pk_restspayment) throws CRUDException{
		try {
			if(ValueCheck.IsEmpty(pk_restspayment)){
				return "-1";
			}
			Restspaymentm restspaymentm = new Restspaymentm();
			restspaymentm.setTs(DateFormat.getTs());
			restspaymentm.setPk_restspayment(AsstUtils.StringCodeReplace(pk_restspayment));
			restspaymentMapper.deleteRestspayment(restspaymentm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	

}
