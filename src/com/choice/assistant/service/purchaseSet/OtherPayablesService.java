package com.choice.assistant.service.purchaseSet;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.PurchaseSet.OtherPayablesd;
import com.choice.assistant.domain.PurchaseSet.OtherPayablesm;
import com.choice.assistant.persistence.purchaseSet.OtherPayablesMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Chkinm;

/**
 * 其他应付款管理
 * @author wangchao
 *
 */
@Service
public class OtherPayablesService {

	@Autowired
	private OtherPayablesMapper otherPayablesmapper;
	@Autowired
	private PageManager<OtherPayablesm> pageManager;
	@Autowired
	private PageManager<Chkinm> pageManagers;
	
	private final transient Log log = LogFactory.getLog(OtherPayablesService.class);
	
	/**
	 * 查询所有的其他应付款主表
	 * @param otherPayablesm
	 * @return
	 */
	public List<OtherPayablesm> queryAllOtherPayablesm(OtherPayablesm otherPayablesm,Page page)throws CRUDException{
		try {
			return pageManager.selectPage(otherPayablesm, page, OtherPayablesMapper.class.getName()+".queryAllOtherPayablesm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主表ID查询其他应付款子表
	 * @param otherPayablesd
	 * @return
	 */
	public List<OtherPayablesd> queryAllOtherPayablesd(OtherPayablesd otherPayablesd)throws CRUDException{
		try {
			return otherPayablesmapper.queryAllOtherPayablesd(otherPayablesd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加其他应付款
	 * @param otherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public String addOtherPayablesm(OtherPayablesm otherPayablesm)throws CRUDException{
		try {
			otherPayablesm.setPk_otherpayables(CodeHelper.createUUID());
			otherPayablesm.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			otherPayablesm.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			otherPayablesm.setDr(0);
			otherPayablesm.setIstate(1);
			otherPayablesmapper.addOtherPayablesm(otherPayablesm);
			for (OtherPayablesd otherPayablesd : otherPayablesm.getOtherpayablesdList()) {
				otherPayablesd.setPk_otherpayablesd(CodeHelper.createUUID());
				otherPayablesd.setPk_otherpayables(otherPayablesm.getPk_otherpayables());
				otherPayablesd.setDr(0);
				otherPayablesd.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				addOtherPayablesd(otherPayablesd);
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加其他应付款子表
	 * @param otherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void addOtherPayablesd(OtherPayablesd otherPayablesd)throws CRUDException{
		try {
			otherPayablesmapper.addOtherPayablesd(otherPayablesd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除其他应付款
	 * @param ids
	 * @throws CRUDException
	 */
	public void deleteOtherPayablesm(OtherPayablesm otherPayablesm)throws CRUDException{
		try {
			otherPayablesmapper.deleteOtherPayablesm(otherPayablesm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主键查询其他应付款主表信息
	 * @param otherPayablesm
	 * @return
	 */
	public OtherPayablesm queryOtherpayablesmByPK(OtherPayablesm otherPayablesm)throws CRUDException{
		try {
			return otherPayablesmapper.queryOtherpayablesmByPK(otherPayablesm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改其他应付款
	 * @param otherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public String updateOtherPayablesm(OtherPayablesm otherPayablesm)throws CRUDException{
		try {
			otherPayablesm.setModifedtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			otherPayablesm.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			otherPayablesmapper.deleteOtherpayablesd(otherPayablesm);
			for (OtherPayablesd otherPayablesd : otherPayablesm.getOtherpayablesdList()) {
				otherPayablesd.setPk_otherpayablesd(CodeHelper.createUUID());
				otherPayablesd.setPk_otherpayables(otherPayablesm.getPk_otherpayables());
				otherPayablesd.setAcct(otherPayablesm.getAcct());
				otherPayablesmapper.addOtherPayablesd(otherPayablesd);
			}
			otherPayablesmapper.updateOtherPayablesm(otherPayablesm);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 提交其他付款单
	 * @param chkstom
	 * @throws CRUDException
	 */
	public void commitPayablesm(OtherPayablesm otherPayablesm) throws CRUDException{
		try {
			otherPayablesm.setIstate(2);
			otherPayablesmapper.updateOtherPayablesm(otherPayablesm);
		} catch (Exception e) {
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询所有的入库单主表数据
	 * @return
	 */
	public List<Chkinm> queryAllInOrder(Chkinm chkinm,Page page) throws CRUDException{
		try {
			if(ValueCheck.IsNotEmpty(chkinm.getChkinno())){
				chkinm.setChkinno(chkinm.getChkinno());
			}
			return pageManagers.selectPage(chkinm, page, OtherPayablesMapper.class.getName()+".queryAllInOrder");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询前10条入库单
	 * @return
	 */
	public List<Chkinm> findInOrderTop(Chkinm chkinm) throws CRUDException{
		try {
			return otherPayablesmapper.findInOrderTop(chkinm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
}
