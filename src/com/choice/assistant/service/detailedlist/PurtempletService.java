package com.choice.assistant.service.detailedlist;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.detailedlist.Purtempletd;
import com.choice.assistant.domain.detailedlist.Purtempletm;
import com.choice.assistant.domain.material.Material;
import com.choice.assistant.persistence.detailedlist.PurtempletMapper;
import com.choice.assistant.persistence.material.MaterialMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 采购清单管理
 * @author wangchao
 *
 */
@Service
public class PurtempletService {

	@Autowired
	private PurtempletMapper purtempletmapper;
	@Autowired
	private MaterialMapper materialMapper;
	@Autowired
	private PageManager<Purtempletm> pageManager;
	
	private final transient Log log = LogFactory.getLog(PurtempletService.class);
	
	/**
	 * 查询所有的采购清单主表
	 * @param purtempletm
	 * @return
	 */
	public List<Purtempletm> queryAllPurtempletm(Purtempletm purtempletm,Page page)throws CRUDException{
		try {
			return pageManager.selectPage(purtempletm, page, PurtempletMapper.class.getName()+".queryAllPurtempletm");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主表ID查询物料清单子表
	 * @param purtempletd
	 * @return
	 */
	public List<Purtempletd> queryAllPurtempletd(Purtempletd purtempletd)throws CRUDException{
		List<Purtempletd> listPurtempletd = null;
		try {
			listPurtempletd = purtempletmapper.queryAllPurtempletd(purtempletd);
			if((null!=purtempletd.getPositncode() && !"".equals(purtempletd.getPositncode()))|| 
					(null!=purtempletd.getDelivercode() && !"".equals(purtempletd.getDelivercode()))){
				Material condition = new Material();
				condition.setAcct(purtempletd.getAcct());
				condition.setDelivercode(purtempletd.getDelivercode());
				condition.setPositncode(purtempletd.getPositncode());
				condition.setDdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
				List<Material> mlist = materialMapper.findAllMaterialPrice(condition);
				Map<String,Double> pricemap = new HashMap<String,Double>();
				for(Material m:mlist){
					pricemap.put(m.getSp_code()+"-"+m.getPositncode()+"-"+m.getDelivercode(), m.getSp_price());
				}
				
				for(Purtempletd pd:listPurtempletd){
					String key = pd.getSp_code()+"-"+purtempletd.getPositncode()+"-"+pd.getDelivercode();
					if(null!=pricemap.get(key)){
						pd.setSp_price(pricemap.get(key));
						pd.setBeditprice(1);
					}
				}
			}
			
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		return listPurtempletd;
	}
	
	/**
	 * 根据主表ID查询物料清单子表 == 根据供应商分组
	 * @param purtempletd
	 * @return
	 */
	public List<Purtempletd> queryPurtempletdBySupplier(Purtempletd purtempletd)throws CRUDException{
		try {
			return purtempletmapper.queryPurtempletdBySupplier(purtempletd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加采购清单
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public String addPurtempletm(Purtempletm purtempletm)throws CRUDException{
		try {
			purtempletm.setPk_purtemplet(CodeHelper.createUUID());
			purtempletm.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			purtempletm.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			purtempletm.setDr(0);
			purtempletmapper.addPurtempletm(purtempletm);
			for (Purtempletd purtempletd : purtempletm.getListPurtempletd()) {
				purtempletd.setPk_purtempletd(CodeHelper.createUUID());
				purtempletd.setPk_purtemplet(purtempletm.getPk_purtemplet());
				purtempletd.setPk_material(purtempletd.getSp_code());
				purtempletd.setDr(0);
				purtempletd.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				addPurtempletd(purtempletd);
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加采购清单子表
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void addPurtempletd(Purtempletd purtempletd)throws CRUDException{
		try {
			purtempletmapper.addPurtempletd(purtempletd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除采购清单
	 * @param ids
	 * @throws CRUDException
	 */
	public void deletePurtempletm(Purtempletm purtempletm)throws CRUDException{
		try {
			purtempletmapper.deletePurtempletm(purtempletm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主键查询采购清单主表信息
	 * @param purtempletm
	 * @return
	 */
	public Purtempletm queryPurtempletmByPK(Purtempletm purtempletm)throws CRUDException{
		try {
			return purtempletmapper.queryPurtempletmByPK(purtempletm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改采购清单
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public String updatePurtempletm(Purtempletm purtempletm)throws CRUDException{
		try {
			purtempletm.setModifedtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			purtempletm.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			purtempletmapper.updatePurtempletm(purtempletm);
			purtempletmapper.deletePurtempletd(purtempletm);
			for (Purtempletd purtempletd : purtempletm.getListPurtempletd()) {
				purtempletd.setPk_purtemplet(purtempletm.getPk_purtemplet());
				purtempletd.setAcct(purtempletm.getAcct());
				purtempletd.setPk_purtempletd(CodeHelper.createUUID());
				purtempletd.setPk_material(purtempletd.getSp_code());
				purtempletmapper.addPurtempletd(purtempletd);
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取采购清单的下拉框
	 * @param purtempletm
	 * @return
	 * @throws CRUDException
	 */
	public List<Purtempletm> getPurtempletmName(Purtempletm purtempletm)throws CRUDException{
		try {
			return purtempletmapper.getPurtempletmName(purtempletm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
}
