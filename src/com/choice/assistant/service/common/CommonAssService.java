package com.choice.assistant.service.common;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.common.CommonAssMethod;
import com.choice.assistant.persistence.common.CommonAssMapper;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;

/**
 * BOH后台公共调用方法
 * @author 董志鹏
 * 2013年11月4日 上午9:27:47
 */
@Service("CommonAssService")
public class CommonAssService {
	
	private static Logger log = Logger.getLogger(CommonAssService.class);
	@Autowired
	private CommonAssMapper commonMapper;
	
	public String validateVcode(CommonAssMethod commonMethod) {
		try {
			return commonMapper.validateVcode(commonMethod);
		} catch (Exception e) {
			log.error(e);
			return "error";
		}
	}

	public String getSortNo(CommonAssMethod commonMethod) {
		try {
			return commonMapper.getSortNo(commonMethod);
		} catch (Exception e) {
			log.error(e);
			return "error";
		}
	}
	
	/**
	 * 描述:是否启用功能
	 * 作者:mmw
	 * 日期:2013-11-13 下午5:08:59
	 * @param ids
	 * @param type
	 * @return
	 * @throws CRUDException
	 */
	public String IsEnable(CommonAssMethod commonMethod)throws CRUDException {
		try{
			commonMapper.IsEnable(commonMethod);
			return "1";
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}

	
	/**
	 * 描述：验证排序重复
	 * @param commonMethod
	 * @return
	 * author:spt
	 * 日期：2014-4-21
	 */
	public Integer validateSortNo(CommonAssMethod commonMethod) {
		try {
			return commonMapper.validateSortNo(commonMethod);
		} catch (Exception e) {
			log.error(e);
			return -1;
		}
	}

	/**
	 * 验证数据是否被引用,vtablename:引用的表，vcodename:引用的字段，vcode:被引用的值
	 * @author wk
	 * 2014年5月15日 
	 * @return
	 */
	public String checkQuote(CommonAssMethod commonMethod) {
		try {
			return commonMapper.checkQuote(commonMethod);
		} catch (Exception e) {
			log.error(e);
			return "-1";
		}
	}
	
	/**
	 * 验证数据是否被引用, vcodename字段中含有多个值的
	 * @author wk
	 * 2014年5月15日 
	 * @return
	 */
	public String checkMultiCodeQuote(CommonAssMethod commonMethod) {
		try {
			return commonMapper.checkMultiCodeQuote(commonMethod);
		} catch (Exception e) {
			log.error(e);
			return "-1";
		}
	}
	/**
	 * 动态执行新增语句
	 * @param delSql
	 * @return
	 */
	public String dynExecDelete(String delSql) {
		try {
			if(ValueCheck.IsNotEmpty(delSql)){
				commonMapper.dynExecDelete(delSql);
				return "1";
			}else{
				return "0";
			}
		} catch (Exception e) {
			log.error(e);
			return "-1";
		}
	}
	/**
	 * 动态执行删除语句
	 * @param insertSql
	 * @return
	 */
	public String dynExecInsert(String insertSql) {
		try {
			if(ValueCheck.IsNotEmpty(insertSql)){
				commonMapper.dynExecInsert(insertSql);
				return "1";
			}else{
				return "0";
			}
		} catch (Exception e) {
			log.error(e);
			return "-1";
		}
	}
}
