package com.choice.assistant.service.tableMain;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.TableMain.OrdrIstate;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.assistant.domain.detailedlist.Purtempletd;
import com.choice.assistant.domain.detailedlist.Purtempletm;
import com.choice.assistant.domain.supplier.Purcontract;
import com.choice.assistant.domain.system.MaterialMatch;
import com.choice.assistant.persistence.tableMain.TableMainOrdrMapper;
import com.choice.framework.exception.CRUDException;

/**
 * 首页订单
 * @author zhb
 *
 */
@Service
public class TableMainOrdrService {
	
	@Autowired
	private TableMainOrdrMapper tableMainOrdrMapper;
	
	private final transient Log log = LogFactory.getLog(TableMainOrdrService.class);
	
	/**
	 * 查询订单号所对应的首页订单
	 * @return
	 */
	public PuprOrder codeordrlist(PuprOrder puprOrder) throws CRUDException{
		try {
				return tableMainOrdrMapper.codeordrlist(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 查询订单号所对应的明细信息
	 * @return
	 */
	public List<PuprOrderd> codeordrDetailList(PuprOrder puprOrder) throws CRUDException{
		try {
			return tableMainOrdrMapper.codeordrDetailList(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 最新发生的订单
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<PuprOrder> newPuprOrder(PuprOrder puprOrder) throws CRUDException{
		try {
				return tableMainOrdrMapper.newPuprOrder(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 物料匹配
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> queryMaterialMatch(MaterialMatch materialMatch) throws CRUDException{
		try {
				return tableMainOrdrMapper.queryMaterialMatch(materialMatch);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 订单跟踪状态
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public OrdrIstate newordristate(OrdrIstate ordrIstate) throws CRUDException{
		try {
				return tableMainOrdrMapper.newordristate(ordrIstate);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 待确认
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<OrdrIstate> dqrnewordristate(OrdrIstate ordrIstate) throws CRUDException{
		try {
				return tableMainOrdrMapper.dqrnewordristate(ordrIstate);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 待收货
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<OrdrIstate> dshnewordristate(OrdrIstate ordrIstate) throws CRUDException{
		try {
				return tableMainOrdrMapper.dshnewordristate(ordrIstate);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 待入库
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<OrdrIstate> drknewordristate(OrdrIstate ordrIstate) throws CRUDException{
		try {
				return tableMainOrdrMapper.drknewordristate(ordrIstate);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 待结算
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<OrdrIstate> djsnewordristate(OrdrIstate ordrIstate) throws CRUDException{
		try {
				return tableMainOrdrMapper.djsnewordristate(ordrIstate);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 采购清单最新
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<Purtempletm> cgqdnewordristate(Purtempletm purtempletm) throws CRUDException{
		try {
				return tableMainOrdrMapper.cgqdnewordristate(purtempletm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 采购清单对应的具体单据主表
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public Purtempletm cgqdlist(Purtempletm purtempletm) throws CRUDException{
		try {
				return tableMainOrdrMapper.cgqdlist(purtempletm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 采购清单对应的具体单据子表
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<Purtempletd> cgqdlists(Purtempletd purtempletd) throws CRUDException{
		try {
				return tableMainOrdrMapper.cgqdlists(purtempletd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 合约
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<Purcontract> hy(Purcontract purcontract) throws CRUDException{
		try {
				return tableMainOrdrMapper.hy(purcontract);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
		
	}
	/**
	 * 合约提醒
	 * @param OrdrIstate
	 * @return
	 * @throws CRUDException
	 */
	public List<Purcontract> hytx(String ss, String acct) throws CRUDException {
		try {
			Purcontract purcontract = new Purcontract();
			purcontract.setPk_purcontract(ss);
			purcontract.setAcct(acct);
			return tableMainOrdrMapper.hytx(purcontract);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
