package com.choice.assistant.service.system;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.Condition;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.util.AssitResultForOne;
import com.choice.assistant.domain.util.asstBillList.AssitBillData;
import com.choice.assistant.domain.util.asstBillList.AssitBillResult;
import com.choice.assistant.domain.util.asstBillList.ResultBillData;
import com.choice.assistant.persistence.system.AssitWebserviceMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.assistant.util.web.MallAssitUtil;
import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Positn;
import com.choice.scm.persistence.AcctMapper;

@Service
public class AssitWebserviceService {

	private final transient Log log = LogFactory.getLog(AssitWebserviceService.class);
	@Autowired
	private AssitWebserviceMapper assitWebserviceMapper;
	@Autowired
	private AcctMapper acctMapper;
	
	/**
	 * 
	 * 手动上传订单到商城查询选择的订单主表信息
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String,Object>> queryAllPuprOrders(PuprOrder puprOrder) throws CRUDException{
		if(ValueCheck.IsEmpty(puprOrder.getPk_puprorder())){
			return null;
		}
		puprOrder.setPk_puprorder(AsstUtils.StringCodeReplace(puprOrder.getPk_puprorder()));
		return assitWebserviceMapper.queryAllPuprOrders(puprOrder);
	}
	
	/**
     * 门店地址上传接口
     * @param supplier
     * @param session
     * @return
     * @throws CRUDException
     */
	public String uploadAddress(Positn positn) throws CRUDException {
    	try{
 	        String params = "";
     		String method = "receiptAddress/create.action";
     		Acct acct = acctMapper.findAcctById(positn.getAcct());
 	        params += AsstUtils.getParams(acct);
 	        if(ValueCheck.IsEmpty(params)){
 	        	System.out.println("=========================================================接口登录失败。");
 	        	log.error("接口登录失败。");
 	        	return null;
 	        }
        	if(ValueCheck.IsNotEmpty(positn)){
        		String str = "";
        		str += ",\"entity.area.id\":\"1\"," +//区域编号
        				"\"entity.companyName\":\""+positn.getDes()+"\","+//公司名称
        				"\"entity.address\":\""+positn.getVaddr()+"\","+//详细地址
        				"\"entity.zipCode\":\""+positn.getVzipcode()+"\","+//邮编
        				"\"entity.consignee\":\""+positn.getVcontact()+"\","+//联系人姓名
        				"\"entity.tel\":\""+positn.getVtel()+"\","+//电话 tel：最长20
        				"\"entity.def\":\"1\"";//是否为默认地址1：是；0：否
        		//调用接口
 	 	       	AssitResultForOne result = MallAssitUtil.formatResult(MallAssitUtil.getResult(method, params+str, acct));
 	 	        if(ValueCheck.IsNotEmpty(result)){
 	 	        	if(ValueCheck.IsNotEmpty(result.getData())&& !result.getData().isEmpty()){
 	 	        		positn.setVmalladdrid(result.getData().get("id").toString());
 	 	        		assitWebserviceMapper.uploadAddress(positn);
 	 	        	}
 	 	        }
 	        }
    		return "1";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
	/**
	 * 获取商城地址id
	 * @param positn
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String getAddressId(Positn positn, Acct acct) throws CRUDException {
    	try{
 	        String params = "";
     		String method = "receiptAddress/create.action";
 	        params += AsstUtils.getParams(acct);
 	        if(ValueCheck.IsEmpty(params)){
 	        	System.out.println("=========================================================接口登录失败。");
 	        	log.error("接口登录失败。");
 	        	return null;
 	        }
        	if(ValueCheck.IsNotEmpty(positn)){
        		String str = "";
        		str += ",\"entity.area.id\":\"1\"," +//区域编号
        				"\"entity.companyName\":\""+positn.getDes()+"\","+//公司名称
        				"\"entity.address\":\""+positn.getVaddr()+"\","+//详细地址
        				"\"entity.zipCode\":\"123456\","+//邮编
        				"\"entity.consignee\":\""+positn.getVcontact()+"\","+//联系人姓名
        				"\"entity.tel\":\""+positn.getVtel()+"\","+//电话 tel：最长20
        				"\"entity.def\":\"1\"";//是否为默认地址1：是；0：否
        		//调用接口
 	 	       	AssitResultForOne result = MallAssitUtil.formatResult(MallAssitUtil.getResult(method, params+str, acct));
 	 	        if(ValueCheck.IsNotEmpty(result)){
 	 	        	if(ValueCheck.IsNotEmpty(result.getData())&& !result.getData().isEmpty()){
 	 	        		return result.getData().get("id").toString();
 	 	        	}
 	 	        }
 	        }
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
		return null;
    }
	
	/**
	 * 获取商城订单列表
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String getMallOrderList(Condition condition, HttpSession session)  throws CRUDException {
    	try{
 	        String params = "";
 	        Acct acct = new Acct();
			acct.setCode(session.getAttribute("ChoiceAcct").toString());
 	        params += AsstUtils.getParams(acct);
 	        if(ValueCheck.IsEmpty(params)){
 	        	System.out.println("=========================================================接口登录失败。");
 	        	log.error("接口登录失败。");
 	        	return null;
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getQuerySnAst())){
 	        	params += ",\"queryOutsideSn\":\""+URLDecoder.decode(condition.getQuerySnAst(),"UTF-8")+"\"";
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getBdat())){
 	        	params += ",\"queryCreatedOrderTimeStart\":\""+condition.getBdat()+"\"";
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getEdat())){
 	        	params += ",\"queryCreatedOrderTimeEnd\":\""+condition.getEdat()+"\"";
 	        }
 	       String str = MallAssitUtil.getResult("order/query/listForBuyer.action", params, acct);
			Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
     		childrenClassMap.put("data", ResultBillData.class);
     		childrenClassMap.put("result", AssitBillResult.class);
     		childrenClassMap.put("goods", Map.class);
     		childrenClassMap.put("seller", Map.class);
     		@SuppressWarnings("unused")
			AssitBillData assitBillResult = null;
			try {
				//解析返回的数据
				assitBillResult = (AssitBillData)JsonFormatUtil.formatAggObjectJson(str, AssitBillData.class,childrenClassMap);
			} catch (CRUDException e) {
				log.error(e);
				e.printStackTrace();
			}
    		return str;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
	/**
	 * 获取商城订单详细信息
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	public String getMallOrderDetail(Condition condition,HttpSession session)   throws CRUDException {
    	try{
 	        String params = "";
 	        Acct acct = new Acct();
			acct.setCode(session.getAttribute("ChoiceAcct").toString());
 	        params += AsstUtils.getParams(acct);
 	        if(ValueCheck.IsEmpty(params)){
 	        	System.out.println("=========================================================接口登录失败。");
 	        	log.error("接口登录失败。");
 	        	return null;
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getId())){
 	        	params += ",\"entity.id\":\""+URLDecoder.decode(condition.getId(),"UTF-8")+"\"";
 	        }
 	        
    		return MallAssitUtil.getResult("order/query/info.action", params, acct);
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
	
	/**
	 * 查询订单明细
	 * @param puprOrderd
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> queryPuprdByPk(PuprOrder puprOrderd) throws CRUDException {
		try{
			return  assitWebserviceMapper.queryPuprdByPk(puprOrderd);
		}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
	}
	/**
	 * 更新订单状态
	 * @param puprOrderdd
	 * @throws CRUDException
	 */
	public void updatePuprOrdr(PuprOrder puprOrderdd) throws CRUDException {
		try{
			 assitWebserviceMapper.updatePuprOrdr(puprOrderdd);
		}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
	}
}
