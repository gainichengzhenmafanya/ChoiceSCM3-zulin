package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Tender;
import com.choice.assistant.persistence.system.TenderMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 招标类型设置
 * Created by 臧国良 on 2014.10.24
 */
@Service
public class TenderService {

	@Autowired
    private TenderMapper tenderMapper;
	@Autowired
	private PageManager<Tender> pageManager;

    private final transient Log log = LogFactory.getLog(TenderService.class);

    /**
     * 查询全部招标类型设置
     * @return
     * @throws CRUDException
     */
    public List<Tender> findTenderListByPage(Tender tender,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(tender, page,TenderMapper.class.getName()+".findTenderList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部招标类型设置
     * @return
     * @throws CRUDException
     */
    public List<Tender> findTenderList(Tender tender) throws CRUDException {
        try{
            List<Tender> list=tenderMapper.findTenderList(tender);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定招标类型设置单条
     * @return
     * @throws CRUDException
     */
    public Tender findTenderByPk(Tender tender) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(tender.getPk_tender())){
    			return null;
    		}
    		Tender list=tenderMapper.findTenderByPk(tender);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存招标类型
     * @param tender
     * @throws CRUDException
     */
    public String saveTender(Tender tender) throws CRUDException {
        try{
        	tender.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(tender.getPk_tender())){
        		tender.setDr(0);
	        	tender.setPk_tender(CodeHelper.createUUID());
	            tenderMapper.addTender(tender);
        	}else{
                tenderMapper.updateTender(tender);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用招标类型
     * @param tender
     * @throws CRUDException
     */
    public String enableTender(Tender tender) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(tender.getPk_tender())){
    			return null;
    		}else{
    			tender.setPk_tender(AsstUtils.StringCodeReplace(tender.getPk_tender()));
    		}
    		tender.setTs(DateFormat.getTs());
			tenderMapper.enableTender(tender);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除招标类型
     * @param tender
     * @throws CRUDException
     */
    public String deleteTender(Tender tender) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(tender.getPk_tender())){
    			return null;
    		}else{
    			tender.setPk_tender(AsstUtils.StringCodeReplace(tender.getPk_tender()));
    		}
    		tender.setTs(DateFormat.getTs());
    		tenderMapper.deleteTender(tender);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
