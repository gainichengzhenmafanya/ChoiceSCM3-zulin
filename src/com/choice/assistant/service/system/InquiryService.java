package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Inquiry;
import com.choice.assistant.persistence.system.InquiryMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 询价类型设置
 * Created by 臧国良 on 2014.10.24
 */
@Service
public class InquiryService {

	@Autowired
    private InquiryMapper inquiryMapper;
	@Autowired
	private PageManager<Inquiry> pageManager;

    private final transient Log log = LogFactory.getLog(InquiryService.class);

    /**
     * 查询全部询价类型设置
     * @return
     * @throws CRUDException
     */
    public List<Inquiry> findInquiryListByPage(Inquiry inquiry,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(inquiry, page,InquiryMapper.class.getName()+".findInquiryList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部询价类型设置
     * @return
     * @throws CRUDException
     */
    public List<Inquiry> findInquiryList(Inquiry inquiry) throws CRUDException {
        try{
            List<Inquiry> list=inquiryMapper.findInquiryList(inquiry);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定询价类型设置单条
     * @return
     * @throws CRUDException
     */
    public Inquiry findInquiryByPk(Inquiry inquiry) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(inquiry.getPk_inquiry())){
    			return null;
    		}
    		Inquiry list=inquiryMapper.findInquiryByPk(inquiry);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存询价类型
     * @param inquiry
     * @throws CRUDException
     */
    public String saveInquiry(Inquiry inquiry) throws CRUDException {
        try{
        	inquiry.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(inquiry.getPk_inquiry())){
        		inquiry.setDr(0);
	        	inquiry.setPk_inquiry(CodeHelper.createUUID());
	            inquiryMapper.addInquiry(inquiry);
        	}else{
                inquiryMapper.updateInquiry(inquiry);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用询价类型
     * @param inquiry
     * @throws CRUDException
     */
    public String enableInquiry(Inquiry inquiry) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(inquiry.getPk_inquiry())){
    			return null;
    		}else{
    			inquiry.setPk_inquiry(AsstUtils.StringCodeReplace(inquiry.getPk_inquiry()));
    		}
    		inquiry.setTs(DateFormat.getTs());
			inquiryMapper.enableInquiry(inquiry);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除询价类型
     * @param inquiry
     * @throws CRUDException
     */
    public String deleteInquiry(Inquiry inquiry) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(inquiry.getPk_inquiry())){
    			return null;
    		}else{
    			inquiry.setPk_inquiry(AsstUtils.StringCodeReplace(inquiry.getPk_inquiry()));
    		}
    		inquiry.setTs(DateFormat.getTs());
    		inquiryMapper.deleteInquiry(inquiry);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
