package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.PayMethod;
import com.choice.assistant.persistence.system.PayMethodMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 付款方式
 * Created by 臧国良 on 2014.12.07
 */
@Service
public class PayMethodService {

	@Autowired
    private PayMethodMapper payMethodMapper;
	@Autowired
	private PageManager<PayMethod> pageManager;

    private final transient Log log = LogFactory.getLog(PayMethodService.class);

    /**
     * 查询全部其他应付款设置
     * @return
     * @throws CRUDException
     */
    public List<PayMethod> findPayMethodListByPage(PayMethod payMethod,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(payMethod, page,PayMethodMapper.class.getName()+".findPayMethodList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部其他应付款设置
     * @return
     * @throws CRUDException
     */
    public List<PayMethod> findPayMethodList(PayMethod payMethod) throws CRUDException {
        try{
            List<PayMethod> list=payMethodMapper.findPayMethodList(payMethod);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询前10条其他应付款设置
     * @return
     * @throws CRUDException
     */
    public List<PayMethod> findTopPayMethod(PayMethod payMethod) throws CRUDException {
        try{
            List<PayMethod> list=payMethodMapper.findTopPayMethod(payMethod);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定其他应付款设置单条
     * @return
     * @throws CRUDException
     */
    public PayMethod findPayMethodByPk(PayMethod payMethod) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(payMethod.getPk_paymethod())){
    			return null;
    		}
    		PayMethod list=payMethodMapper.findPayMethodByPk(payMethod);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存其他应付款
     * @param payMethod
     * @throws CRUDException
     */
    public String savePayMethod(PayMethod payMethod) throws CRUDException {
        try{
        	payMethod.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(payMethod.getPk_paymethod())){
        		payMethod.setDr(0);
	        	payMethod.setPk_paymethod(CodeHelper.createUUID());
	            payMethodMapper.addPayMethod(payMethod);
        	}else{
                payMethodMapper.updatePayMethod(payMethod);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用其他应付款
     * @param payMethod
     * @throws CRUDException
     */
    public String enablePayMethod(PayMethod payMethod) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(payMethod.getPk_paymethod())){
    			return null;
    		}else{
    			payMethod.setPk_paymethod(AsstUtils.StringCodeReplace(payMethod.getPk_paymethod()));
    		}
    		payMethod.setTs(DateFormat.getTs());
			payMethodMapper.enablePayMethod(payMethod);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除其他应付款
     * @param payMethod
     * @throws CRUDException
     */
    public String deletePayMethod(PayMethod payMethod) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(payMethod.getPk_paymethod())){
    			return null;
    		}else{
    			payMethod.setPk_paymethod(AsstUtils.StringCodeReplace(payMethod.getPk_paymethod()));
    		}
    		payMethod.setTs(DateFormat.getTs());
    		payMethodMapper.deletePayMethod(payMethod);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
