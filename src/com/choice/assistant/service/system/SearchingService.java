package com.choice.assistant.service.system;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.Condition;
import com.choice.assistant.domain.material.ReviMaterial;
import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.util.AssitResultForSpInfo;
import com.choice.assistant.domain.util.AsstGoods;
import com.choice.assistant.domain.util.AsstSpInfo;
import com.choice.assistant.domain.util.asstGoods.AssitResultGoods;
import com.choice.assistant.domain.util.asstSupplier.AssitSupplierResult;
import com.choice.assistant.persistence.system.SearchingMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.JsonFormatUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.assistant.util.web.MallAssitUtil;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.Acct;
import com.choice.scm.service.AcctService;

@Service
public class SearchingService {

    private final transient Log log = LogFactory.getLog(SearchingService.class);
    private final transient Logger logger = Logger.getLogger(SearchingService.class);
    @Autowired
    private SearchingMapper searchingMapper;
    @Autowired
    private AcctService acctService;
    /**
     * 查询商城物资列表
     * @return
     * @throws CRUDException
     */
    public AssitResultGoods findAsstMaterialList(Condition condition,HttpSession session) throws CRUDException {
    	try{
 	        String params = "";
 	        Acct acct = new Acct();
 	        acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
 	        params += AsstUtils.getParams(acct);
 	        if(ValueCheck.IsEmpty(params)){
 	        	System.out.println("=========================================================接口登录失败。");
 	        	log.error("接口登录失败。");
 	        	return null;
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getVname())){
 	        	params += ",\"entity.name\":\""+URLDecoder.decode(condition.getVname(),"UTF-8")+"\"";
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getVcode())){
 	        	params += ",\"entity.SN\":\""+URLDecoder.decode(condition.getVcode(),"UTF-8")+"\"";
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getVdelivername())){
 	        	params += ",\"querySellerCompanyName\":\""+URLDecoder.decode(condition.getVdelivername(),"UTF-8")+"\"";
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getPager())){
 	        	params += ",\"pageInfo.pageCondition.pageNo\":\""+condition.getPager().getNowPage()+"\"";
 	        	params += ",\"pageInfo.pageCondition.pageSize\":\""+condition.getPager().getPageSize()+"\"";
 	        }
 	        
    		return MallAssitUtil.formatGoodsResultList(MallAssitUtil.getResult("goods/list.action", params, acct));
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 查询商城指定物资
     * @return
     * @throws CRUDException
     */
    public AssitResultForSpInfo findAsstMaterialByPk(Condition condition,HttpSession session) throws CRUDException {
    	try{
 	        String params = "";
 	       	Acct acct = new Acct();
	        acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
	        params += AsstUtils.getParams(acct);
 	        if(ValueCheck.IsEmpty(params)){
 	        	System.out.println("=========================================================接口登录失败。");
 	        	log.error("接口登录失败。");
 	        	return null;
 	        }
	        params += ",\"entity.id\":\""+condition.getVcode()+"\"";
	        String reStr = MallAssitUtil.getResult("goods/goodsInfo.action", params, acct);
	        Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
			childrenClassMap.put("data", AsstSpInfo.class);
			childrenClassMap.put("goods", AsstGoods.class);
			childrenClassMap.put("spec", Map.class);
			childrenClassMap.put("image", Map.class);
			childrenClassMap.put("specName", Map.class);
    	       
    		return (AssitResultForSpInfo)JsonFormatUtil.formatAggObjectJson(reStr, AssitResultForSpInfo.class,childrenClassMap);
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 查询商城供应商列表
     * @return
     * @throws CRUDException
     */
    public AssitSupplierResult findAsstSupplierList(Condition condition,HttpSession session) throws CRUDException {
    	try{
 	        String params = "";
 	        Acct acct = new Acct();
	        acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
	        params += AsstUtils.getParams(acct);
 	        if(ValueCheck.IsEmpty(params)){
 	        	System.out.println("=========================================================接口登录失败。");
 	        	log.error("接口登录失败。");
 	        	logger.error("接口登录失败。");
 	        	return null;
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getVname())){
 	        	params += ",\"queryCompanyName\":\""+URLDecoder.decode(condition.getVname(),"UTF-8")+"\"";
 	        }
 	        if(ValueCheck.IsNotEmpty(condition.getPager())){
 	        	params += ",\"pageInfo.pageCondition.pageNo\":\""+condition.getPager().getNowPage()+"\"";
 	        	params += ",\"pageInfo.pageCondition.pageSize\":\""+condition.getPager().getPageSize()+"\"";
 	        }
    		return MallAssitUtil.formatSupplierResultList(MallAssitUtil.getResult("hsellerQuery/list.action", params, acct));
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
	 
    /**
     * 检测该商品是否已经存到待审核物资表中
     * @param reviMaterial
     * @return
     * @throws CRUDException
     * @author ZGL
     */
	public String findReviMaterialByPk(ReviMaterial reviMaterial) throws CRUDException {
		try{
			return searchingMapper.findReviMaterialByPk(reviMaterial);
		 }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
	}
	/**
	 * 把物资加入到待审核物资表
	 * @param reviMaterial
	 * @return
	 * @throws CRUDException
	 */
	public String saveReviMaterial(ReviMaterial reviMaterial) throws CRUDException {
		try{
			if(ValueCheck.IsNotEmpty(reviMaterial.getPk_material())){
				reviMaterial.setPk_revimaterial(CodeHelper.createUUID());
				reviMaterial.setDr(0);
				reviMaterial.setTs(DateFormat.getTs());
				searchingMapper.saveReviMaterial(reviMaterial);
				return "1";
			}else{
				return "0";
			}
		 }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
	}
	/**
	 * 检查该供应商是否已经存在
	 * @param auditsupplier
	 * @return
	 * @throws CRUDException
	 */
	public String findAuditMaterialByPk(Auditsupplier auditsupplier) throws CRUDException {
		try{
			return searchingMapper.findAuditMaterialByPk(auditsupplier);
		 }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
	}
	/**
	 * 保存供应商到待审核供应商表
	 * @param auditsupplier
	 * @return
	 * @throws CRUDException
	 */
	public String saveAuditMaterial(Auditsupplier auditsupplier) throws CRUDException {
		try{
			if(ValueCheck.IsNotEmpty(auditsupplier.getPk_supplier())){
				auditsupplier.setPk_auditsupplier(CodeHelper.createUUID());
				auditsupplier.setDr("0");
				auditsupplier.setTs(DateFormat.getTs());
				searchingMapper.saveAuditMaterial(auditsupplier);
				return "1";
			}else{
				return "0";
			}
		 }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
	}
}
