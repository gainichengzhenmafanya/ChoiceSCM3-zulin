package com.choice.assistant.service.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Company;
import com.choice.assistant.domain.system.MaterialMatch;
import com.choice.assistant.domain.util.asstGoods.AsstGoods;
import com.choice.assistant.persistence.system.AutoSaveMaterialMatchMapper;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 物料匹配后台任务方法
 * @author ZGL
 * @Date 2015-01-11 15:34:27
 *
 */
@Service
public class AutoSaveMaterialMatchService {
	
	@Autowired
	private AutoSaveMaterialMatchMapper autoSaveMaterialMatchMapper;
	@Autowired
	private PageManager<MaterialMatch> pageManager;
	
	/**
	 * 查询该企业采购数量前5的物资名称、编码
	 * @param comp
	 * @return
	 * @throws CRUDException
	 */
	public List<Map<String, Object>> queryTotalMax5Material(Company comp) throws CRUDException{
		if(ValueCheck.IsEmpty(comp.getAcct())){
			return null;
		}
		return autoSaveMaterialMatchMapper.queryTotalMax5Material(comp);
	}
	/**
	 * 删除之前匹配的物资匹配数据
	 * @param comp
	 * @throws CRUDException
	 */
	public void deleteMaterialMatchByPkgroup(Company comp) throws CRUDException{
		if(ValueCheck.IsEmpty(comp.getAcct())){
			return;
		}
		autoSaveMaterialMatchMapper.deleteMaterialMatchByPkgroup(comp);
	}
	/**
	 * 插入查询出的数据
	 * @param materialMap
	 * @param listMaterial 
	 * @throws CRUDException
	 */
	public void insertMaterialMatch(Map<String, Object> materialMap, List<AsstGoods> listMaterial) throws CRUDException{
		if(ValueCheck.IsEmpty(materialMap.get("acct"))){
			return;
		}
		List<Map<String,Object>> listInsert = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String,Object>();
		for(AsstGoods asstGoods : listMaterial){
			map = new HashMap<String,Object>();
			map.put("pk_materialmatch",CodeHelper.createUUID());
			map.put("acct",materialMap.get("acct"));
			map.put("vcode",materialMap.get("vmaterialcode"));
			map.put("vname",materialMap.get("vmaterialname"));
			map.put("vmallcode",asstGoods.getSN());
			map.put("vmallname",asstGoods.getName());
			map.put("nleast",asstGoods.getLeast());
			map.put("vunit",asstGoods.getUnit());
			map.put("nprice",asstGoods.getDefPrice());
			map.put("delivercode",asstGoods.getStore().getSeller().get("id"));
			map.put("delivername",asstGoods.getStore().getSeller().get("companyName"));
			map.put("vsuppliercontract",asstGoods.getStore().getSeller().get("companyContact"));
			map.put("vsuppliertel",asstGoods.getStore().getSeller().get("companyPhone"));
			map.put("vsupplieraddr",asstGoods.getStore().getSeller().get("companyAddress"));
			listInsert.add(map);
		}
		if(!listInsert.isEmpty() && listInsert.size()>0){
			autoSaveMaterialMatchMapper.insertMaterialMatch(listInsert);
		}
	}
	/**
	 * 查询物资匹配列表-物资列表
	 * @param materialMatch
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialMatch> queryMaterialMatchMaterialList(MaterialMatch materialMatch,Page page)throws CRUDException{
		if(ValueCheck.IsEmpty(materialMatch.getAcct())){
			return null;
		}
		return pageManager.selectPage(materialMatch, page, AutoSaveMaterialMatchMapper.class.getName()+".queryMaterialMatchMaterialList");
	}
	/**
	 * 查询物资匹配列表-供应商列表
	 * @param materialMatch
	 * @param page
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialMatch> queryMaterialMatchSupplierList(MaterialMatch materialMatch,Page page)throws CRUDException{
		if(ValueCheck.IsEmpty(materialMatch.getAcct())){
			return null;
		}
		return pageManager.selectPage(materialMatch, page, AutoSaveMaterialMatchMapper.class.getName()+".queryMaterialMatchSupplierList");
	}
	

}
