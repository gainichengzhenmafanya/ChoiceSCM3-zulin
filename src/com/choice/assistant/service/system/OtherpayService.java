package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Otherpay;
import com.choice.assistant.persistence.system.OtherpayMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 其他应付款设置
 * Created by 臧国良 on 2014.10.27
 */
@Service
public class OtherpayService {

	@Autowired
    private OtherpayMapper otherpayMapper;
	@Autowired
	private PageManager<Otherpay> pageManager;

    private final transient Log log = LogFactory.getLog(OtherpayService.class);

    /**
     * 查询全部其他应付款设置
     * @return
     * @throws CRUDException
     */
    public List<Otherpay> findOtherpayListByPage(Otherpay otherpay,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(otherpay, page,OtherpayMapper.class.getName()+".findOtherpayList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部其他应付款设置
     * @return
     * @throws CRUDException
     */
    public List<Otherpay> findOtherpayList(Otherpay otherpay) throws CRUDException {
        try{
            List<Otherpay> list=otherpayMapper.findOtherpayList(otherpay);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询前10条其他应付款设置
     * @return
     * @throws CRUDException
     */
    public List<Otherpay> findTopOtherpay(Otherpay otherpay) throws CRUDException {
        try{
            List<Otherpay> list=otherpayMapper.findTopOtherpay(otherpay);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定其他应付款设置单条
     * @return
     * @throws CRUDException
     */
    public Otherpay findOtherpayByPk(Otherpay otherpay) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(otherpay.getPk_otherpay())){
    			return null;
    		}
    		Otherpay list=otherpayMapper.findOtherpayByPk(otherpay);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存其他应付款
     * @param otherpay
     * @throws CRUDException
     */
    public String saveOtherpay(Otherpay otherpay) throws CRUDException {
        try{
        	otherpay.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(otherpay.getPk_otherpay())){
        		otherpay.setDr(0);
	        	otherpay.setPk_otherpay(CodeHelper.createUUID());
	            otherpayMapper.addOtherpay(otherpay);
        	}else{
                otherpayMapper.updateOtherpay(otherpay);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用其他应付款
     * @param otherpay
     * @throws CRUDException
     */
    public String enableOtherpay(Otherpay otherpay) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(otherpay.getPk_otherpay())){
    			return null;
    		}else{
    			otherpay.setPk_otherpay(AsstUtils.StringCodeReplace(otherpay.getPk_otherpay()));
    		}
    		otherpay.setTs(DateFormat.getTs());
			otherpayMapper.enableOtherpay(otherpay);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除其他应付款
     * @param otherpay
     * @throws CRUDException
     */
    public String deleteOtherpay(Otherpay otherpay) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(otherpay.getPk_otherpay())){
    			return null;
    		}else{
    			otherpay.setPk_otherpay(AsstUtils.StringCodeReplace(otherpay.getPk_otherpay()));
    		}
    		otherpay.setTs(DateFormat.getTs());
    		otherpayMapper.deleteOtherpay(otherpay);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
