package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.OrgStructure;
import com.choice.assistant.persistence.system.OrgstructureMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 组织类型设置
 * Created by 臧国良 on 2014.10.24
 */
@Service
public class OrgstructureService {

	@Autowired
    private OrgstructureMapper orgstructureMapper;
	@Autowired
	private PageManager<OrgStructure> pageManager;

    private final transient Log log = LogFactory.getLog(OrgstructureService.class);

    /**
     * 查询全部组织类型设置
     * @return
     * @throws CRUDException
     */
    public List<OrgStructure> findOrgStructureListByPage(OrgStructure orgStructure,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(orgStructure, page,OrgstructureMapper.class.getName()+".findOrgStructureList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部组织类型设置
     * @return
     * @throws CRUDException
     */
    public List<OrgStructure> findOrgStructureList(OrgStructure orgStructure) throws CRUDException {
        try{
            List<OrgStructure> list=orgstructureMapper.findOrgStructureList(orgStructure);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定组织类型设置单条
     * @return
     * @throws CRUDException
     */
    public OrgStructure findOrgStructureByPk(OrgStructure orgStructure) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(orgStructure.getPk_orgstructure())){
    			return null;
    		}
    		OrgStructure list=orgstructureMapper.findOrgStructureByPk(orgStructure);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存组织类型
     * @param orgStructure
     * @throws CRUDException
     */
    public String saveOrgstructure(OrgStructure orgStructure) throws CRUDException {
        try{
        	orgStructure.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(orgStructure.getPk_orgstructure())){
        		orgStructure.setDr(0);
	        	orgStructure.setPk_orgstructure(CodeHelper.createUUID());
	            orgstructureMapper.addOrgStructure(orgStructure);
        	}else{
                orgstructureMapper.updateOrgStructure(orgStructure);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用组织类型
     * @param orgStructure
     * @throws CRUDException
     */
    public String enableOrgStructure(OrgStructure orgStructure) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(orgStructure.getPk_orgstructure())){
    			return null;
    		}else{
    			orgStructure.setPk_orgstructure(AsstUtils.StringCodeReplace(orgStructure.getPk_orgstructure()));
    		}
    		orgStructure.setTs(DateFormat.getTs());
			orgstructureMapper.enableOrgStructure(orgStructure);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除组织类型
     * @param orgStructure
     * @throws CRUDException
     */
    public String deleteOrgStructure(OrgStructure orgStructure) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(orgStructure.getPk_orgstructure())){
    			return null;
    		}else{
    			orgStructure.setPk_orgstructure(AsstUtils.StringCodeReplace(orgStructure.getPk_orgstructure()));
    		}
    		orgStructure.setTs(DateFormat.getTs());
    		orgstructureMapper.deleteOrgStructure(orgStructure);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     *  根据条件完全匹配搜索
     * @return
     * @throws CRUDException
     */
    public OrgStructure selectOrgstructureByCondition(OrgStructure orgStructure) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(orgStructure.getVcode()) && ValueCheck.IsEmpty(orgStructure.getVname())){
    			return null;
    		}
    		OrgStructure list=orgstructureMapper.selectOrgstructureByCondition(orgStructure);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
