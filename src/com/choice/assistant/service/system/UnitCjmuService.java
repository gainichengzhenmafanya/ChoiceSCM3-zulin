package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Unit;
import com.choice.assistant.persistence.system.UnitCjmuMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 单位设置
 * Created by 臧国良 on 2014.10.24
 */
@Service
public class UnitCjmuService {

	@Autowired
    private UnitCjmuMapper unitCjmuMapper;
	@Autowired
	private PageManager<Unit> pageManager;

    private final transient Log log = LogFactory.getLog(UnitCjmuService.class);

    /**
     * 查询全部单位设置
     * @return
     * @throws CRUDException
     */
    public List<Unit> findUnitListByPage(Unit unit,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(unit, page,UnitCjmuMapper.class.getName()+".findUnitList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部单位设置
     * @return
     * @throws CRUDException
     */
    public List<Unit> findUnitist(Unit unit) throws CRUDException {
        try{
            List<Unit> list=unitCjmuMapper.findUnitList(unit);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定单位设置单条
     * @return
     * @throws CRUDException
     */
    public Unit findUnitByPk(Unit unit) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(unit.getPk_unit())){
    			return null;
    		}
    		Unit list=unitCjmuMapper.findUnitByPk(unit);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存单位
     * @param unit
     * @throws CRUDException
     */
    public String saveUnitCjmu(Unit unit) throws CRUDException {
        try{
        	unit.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(unit.getPk_unit())){
        		unit.setDr(0);
	        	unit.setPk_unit(CodeHelper.createUUID());
	            unitCjmuMapper.addUnit(unit);
        	}else{
                unitCjmuMapper.updateUnit(unit);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用单位
     * @param unit
     * @throws CRUDException
     */
    public String enableUnit(Unit unit) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(unit.getPk_unit())){
    			return null;
    		}else{
    			unit.setPk_unit(AsstUtils.StringCodeReplace(unit.getPk_unit()));
    		}
    		unit.setTs(DateFormat.getTs());
			unitCjmuMapper.enableUnit(unit);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除单位
     * @param unit
     * @throws CRUDException
     */
    public String deleteUnit(Unit unit) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(unit.getPk_unit())){
    			return null;
    		}else{
    			unit.setPk_unit(AsstUtils.StringCodeReplace(unit.getPk_unit()));
    		}
    		unit.setTs(DateFormat.getTs());
    		unitCjmuMapper.deleteUnit(unit);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
