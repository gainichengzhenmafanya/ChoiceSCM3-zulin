package com.choice.assistant.service.system;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.system.Coderule;
import com.choice.assistant.persistence.system.CoderuleMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;

/**
 * 编码规则
 * Created by mc on 14-10-18.
 */
@Service
public class CoderuleService {
    private static String NULL = "0";     //0无
    private static String FIGURE = "1";  //1数字
    private static String LETTER = "2";  //2字母
    private static String DATE = "3";    //3日期
    private static String R_WATER = "4"; //4流水
    private static String TYPE = "5";    //5类别
    private static String D_WATER = "41";//41日流水
    private static String M_WATER = "42";//42月流水
    private static String Y_WATER = "43";//43年流水
    private static String WATER = "44";//流水

    private static String vcetFlag = "0";//流水日期标志为0是没有变 不等于0是变了
    private static String oldVcet = "";//更新前流水日期标志

    @Autowired
    private CoderuleMapper coderuleMapper;

    private final transient Log log = LogFactory.getLog(CoderuleService.class);
    //物资编码静态编码存放池
	private static Map<String,Object> materialCodeMap = new HashMap<String,Object>();
	//供应商编码静态编码存放池
	private static Map<String,Object> supplierCodeMap = new HashMap<String,Object>();
	//日期流水号标志
	private static Map<String,Object> vcetFlagMap = new HashMap<String,Object>();

    /**
     * 查询所有编码规则
     *
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    public List<Coderule> findcoderuleList(Coderule coderule) throws CRUDException {
        try {
            List<Coderule> list = coderuleMapper.findCoderuleOrgList(coderule);
            if (list == null || list.size() <= 0) {
                list = coderuleMapper.findCoderuleList(null);
                for (Coderule cr : list) {
                    cr.setVpreview(getFormat(cr));
                    cr.setAcct(coderule.getAcct());
                }
                coderuleMapper.saveCoderule(list);
            }
            return list;
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 修改编码规则
     *
     * @param coderule
     * @throws com.choice.framework.exception.CRUDException
     */
    public String updateCoderule(Coderule coderule) throws CRUDException {
        try {
            tirmData(coderule);
            coderuleMapper.updateCoderule(coderule);
            return getCode(coderule);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 初始化编码规则 并计算总长度
     *
     * @param coderule
     */
    private void tirmData(Coderule coderule) {
        if (ValueCheck.IsEmpty(coderule.getIlength1())) {
            coderule.setIlength1(null);
            coderule.setItype1(null);
            coderule.setVvalue1(null);
            coderule.setVcet1(null);
            coderule.setVmax1(null);
        } else {
            coderule.setItotallength(coderule.getItotallength() + coderule.getIlength1());
        }
        if (ValueCheck.IsEmpty(coderule.getIlength2())) {
            coderule.setIlength2(null);
            coderule.setItype2(null);
            coderule.setVvalue2(null);
            coderule.setVcet2(null);
            coderule.setVmax2(null);
        } else {
            coderule.setItotallength(coderule.getItotallength() + coderule.getIlength2());
        }
        if (ValueCheck.IsEmpty(coderule.getIlength3())) {
            coderule.setIlength3(null);
            coderule.setItype3(null);
            coderule.setVvalue3(null);
            coderule.setVcet3(null);
            coderule.setVmax3(null);
        } else {
            coderule.setItotallength(coderule.getItotallength() + coderule.getIlength3());
        }
        if (ValueCheck.IsEmpty(coderule.getIlength4())) {
            coderule.setIlength4(null);
            coderule.setItype4(null);
            coderule.setVvalue4(null);
            coderule.setVcet4(null);
            coderule.setVmax4(null);
        } else {
            coderule.setItotallength(coderule.getItotallength() + coderule.getIlength4());
        }
        if (ValueCheck.IsEmpty(coderule.getIlength5())) {
            coderule.setIlength5(null);
            coderule.setItype5(null);
            coderule.setVvalue5(null);
            coderule.setVcet5(null);
            coderule.setVmax5(null);
        } else {
            coderule.setItotallength(coderule.getItotallength() + coderule.getIlength5());
        }
        if (ValueCheck.IsEmpty(coderule.getIlength6())) {
            coderule.setIlength6(null);
            coderule.setItype6(null);
            coderule.setVvalue6(null);
            coderule.setVcet6(null);
            coderule.setVmax6(null);
        } else {
            coderule.setItotallength(coderule.getItotallength() + coderule.getIlength6());
        }
        if (ValueCheck.IsEmpty(coderule.getIlength7())) {
            coderule.setIlength7(null);
            coderule.setItype7(null);
            coderule.setVvalue7(null);
            coderule.setVcet7(null);
            coderule.setVmax7(null);
        } else {
            coderule.setItotallength(coderule.getItotallength() + coderule.getIlength7());
        }
    }

    //------------------------------编码组合--------------------------

    /**
     * 根据规则组合编码
     *
     * @return
     */
    public String getCode(Coderule cr) throws Exception {
        if (cr != null && cr.getAcct() != null && !"".equals(cr.getAcct())) {
            Coderule coderule = getCR(cr);
            if (coderule == null) {
                return "";
            }
            //如果是基础档案 
            //将数据放到一个变量中 企业号、类别、流水号 每次去变量中取没有的话新增 有的话获取值+1返回并更新变量值
            if ("2".equals(coderule.getIsrule() == null ? null : coderule.getIsrule().toString())) {
                coderule.setTypecode(cr.getTypecode());
                coderule.setParentcode(cr.getParentcode());
                coderule.setLvl(cr.getLvl());
                coderule.setVmaxvalue(getCodeValue(coderule));
                coderule.setVpreview(getFormat(coderule.getPk_coderule(), coderule.getAcct()));
                coderuleMapper.updateMaxValue(coderule);
                return coderule.getVmaxvalue();
            } else {//如果是单据
                coderule.setVmaxvalue(getCodeValue(coderule));
                coderule.setVpreview(getFormat(coderule.getPk_coderule(), coderule.getAcct()));
                coderuleMapper.updateMaxValue(coderule);
                return coderule.getVmaxvalue();
            }
        }
        return "";
    }

    /**
     * 根据规则组合编码
     *
     * @return
     */
    public String getCode(String pk, String group) throws Exception {
        Coderule coderule = new Coderule();
        coderule.setAcct(group);
        coderule.setPk_coderule(pk);
        return getCode(coderule);
    }

    /**
     * 获取编码规则
     *
     * @param coderule
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    private String getCodeValue(Coderule coderule) throws CRUDException {
        String oCode = "";
        int sumLen = 0;
        //如果编码级数为0 表没有设置编码规则
        if (coderule != null &&"1".equals(coderule.getIsrule())&& "0".equals(coderule.getIcodeseries() == null ? null : coderule.getIcodeseries().toString())) {
            try {
                String cet = coderule.getVcet8();
                if (cet == null || cet == "" || !DateFormat.getStringByDate(new Date(), "MMdd").equals(cet)) {
                    coderule.setVmax8("0001");//保存最大值
                    coderule.setVcet8(DateFormat.getStringByDate(new Date(), "MMdd"));//保存日期
                    return CoderuleConstants.INIT.valueOf("MC" + coderule.getPk_coderule()).getValue() +
                            DateFormat.getStringByDate(new Date(), "yyyyMMdd") + "0001";
                }
                String code = getCodeHandle(coderule.getVmax8(), 4);
                coderule.setVmax8(code);//保存最大值
                return CoderuleConstants.INIT.valueOf("MC" + coderule.getPk_coderule()).getValue() +
                        DateFormat.getStringByDate(new Date(), "yyyyMMdd") + code;
            }catch (Exception e){
                e.printStackTrace();
            }
            return "";
        }
        //根据编码规则生成编码方法
        Class c = coderule.getClass();
        if (coderule.getLvl() == null || coderule.getLvl().intValue() <= 0) {
            for (int i = 1; i <= coderule.getIcodeseries(); i++) {//反射 循环 对象数据
                try {
                    Integer len = (Integer) c.getMethod("getIlength" + i).invoke(coderule);
                    String type = (String) c.getMethod("getItype" + i).invoke(coderule);
                    String value = (String) c.getMethod("getVvalue" + i).invoke(coderule);
                    String max = (String) c.getMethod("getVmax" + i).invoke(coderule);
                    String cet = (String) c.getMethod("getVcet" + i).invoke(coderule);
                    if (TYPE.equals(type)) {
                    	len = coderule.getTypecode().length();
                    	//如果是类别，长度不补0处理了
//                        coderule.setTypecode(lenBeHandle(coderule.getTypecode(), len, "0"));
                    }
                    sumLen += len;//计算按照规则生成的编码长度
                    coderule.setIlength(len);
                    coderule.setItype(type);
                    coderule.setVvalue(value);
                    coderule.setVmax(max);
                    coderule.setVcet(cet);
                    oCode += typeValue(coderule);
                    c.getMethod("setVmax" + i, String.class).invoke(coderule, coderule.getVmax());
                    c.getMethod("setVcet" + i, String.class).invoke(coderule, coderule.getVcet());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                oCode = coderule.getParentcode();
                Integer len = (Integer) c.getMethod("getIlength" + coderule.getLvl()).invoke(coderule);
                String type = (String) c.getMethod("getItype" + coderule.getLvl()).invoke(coderule);
                String value = (String) c.getMethod("getVvalue" + coderule.getLvl()).invoke(coderule);
                String max = (String) c.getMethod("getVmax" + coderule.getLvl()).invoke(coderule);
                String cet = (String) c.getMethod("getVcet" + coderule.getLvl()).invoke(coderule);
                coderule.setIlength(len);
                coderule.setItype(type);
                coderule.setVvalue(value);
                coderule.setVmax(max);
                coderule.setVcet(cet);
                oCode += typeValue(coderule);
                c.getMethod("setVmax" + coderule.getLvl(), String.class).invoke(coderule, coderule.getVmax());
                c.getMethod("setVcet" + coderule.getLvl(), String.class).invoke(coderule, coderule.getVcet());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        if (sumLen > 50 || oCode.length() > 50) {
            throw new CRUDException("编码格式过长！最多只支持50位！");
        }
        return oCode;
    }

    /**
     * 根据类型组合编码
     *
     * @return
     */
    public String typeValue(Coderule coderule) throws CRUDException {
        if (LETTER.equals(coderule.getItype()) || NULL.equals(coderule.getItype())) {//字母和无
            return lenHandle(coderule.getVvalue(), coderule.getIlength(), "x");
        } else if (FIGURE.equals(coderule.getItype())) {//数字
            try {
                String sql = CoderuleConstants.INIT.valueOf("MC" + coderule.getPk_coderule()).getValue();
                if (sql != null && !"".equals(sql)) {
                    String maxcode=coderuleMapper.executeSQL(String.format(sql,coderule.getAcct(),coderule.getParentcode().length()+coderule.getIlength(),coderule.getParentcode()));
                    if(maxcode==null){
                        return getCodeHandle("0",coderule.getIlength());
                    }
                    return getCodeHandle(maxcode.substring(coderule.getParentcode().length(),maxcode.length()),coderule.getIlength());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return lenHandle(coderule.getVvalue(), coderule.getIlength(), "0");
        } else if (R_WATER.equals(coderule.getItype())) {//流水
            String water = "";
            if (D_WATER.equals(coderule.getVvalue())) {//日流水
                water = dateFormat("MMdd");
            } else if (M_WATER.equals(coderule.getVvalue())) {//月流水
                water = dateFormat("MM");
            } else if (Y_WATER.equals(coderule.getVvalue())) {//年流水
                water = dateFormat("yyyy");
            }
            //判断当前的日期是否等于数据库中存储的日期，不等于返回当前日期加生成的从1开始的流水号
            if (!water.equals(coderule.getVcet())) {
            	oldVcet = coderule.getVcet(); //更新为之前的日期标志
                coderule.setVcet(water);
                String code = getCodeHandle("0", coderule.getIlength());
                coderule.setVmax(code.substring(0, code.length() - 1) + "1");
                //清空中间变量
                materialCodeMap = new HashMap<String,Object>();
                //将构建的流水号方法静态变量中以备下次获取编码用而不用每次都查库
            	materialCodeMap.put(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode(),coderule.getVmax());
                return coderule.getVmax();
            }
            //物资编码、供应商编码获取问题，读取日期配置标志map 如果为空 获取当前第一位流水号编码
            if(CoderuleConstants.MATERIAL.equals(coderule.getPk_coderule()) 
            		|| CoderuleConstants.SUPPLIER.equals(coderule.getPk_coderule())){
	            if(!water.equals(oldVcet) 
	            		&& ValueCheck.IsEmpty(materialCodeMap.get(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode()))){
	                String code = getCodeHandle("0", coderule.getIlength());
	                coderule.setVmax(code.substring(0, code.length() - 1) + "1");
	            	//将构建的流水号方法静态变量中以备下次获取编码用而不用每次都查库
	            	materialCodeMap.put(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode(),coderule.getVmax());
	                return coderule.getVmax();
	            }
            }
            //物资编码
            if(CoderuleConstants.MATERIAL.equals(coderule.getPk_coderule())){
            	String materialCode = "";
            	if(ValueCheck.IsEmpty(materialCodeMap.get(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode()))){
            		materialCode = coderuleMapper.getMaxVcodeForMaterial(coderule);
            		materialCode = getNumFromStr(materialCode,coderule);
//            		if(!"0".equals(materialCode)){
//            			materialCode = materialCode.substring(materialCode.indexOf(coderule.getTypecode())+coderule.getTypecode().length());
//            		}
            	}else{
            		materialCode = materialCodeMap.get(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode())+"";
            	}
            	coderule.setVmax(materialCode);
            }else if(CoderuleConstants.SUPPLIER.equals(coderule.getPk_coderule())){//供应商编码
            	String supplierCode = "";
            	if(ValueCheck.IsEmpty(materialCodeMap.get(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode()))){
            		supplierCode = coderuleMapper.getMaxVcodeForSupplier(coderule);
            		supplierCode = getNumFromStr(supplierCode,coderule);
//            		if(!"0".equals(supplierCode)){
//            			supplierCode = supplierCode.substring(supplierCode.indexOf(coderule.getTypecode())+coderule.getTypecode().length());
//            		}
            	}else{
            		supplierCode = materialCodeMap.get(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode())+"";
            	}
            	coderule.setVmax(supplierCode);
            }
            //构建流水号,补位--位数不足前面用0补齐
        	coderule.setVmax(getCodeHandle(coderule.getVmax(), coderule.getIlength()));
        	//将构建的流水号方法静态变量中以备下次获取编码用而不用每次都查库
        	materialCodeMap.put(coderule.getAcct()+coderule.getPk_coderule()+coderule.getTypecode(),coderule.getVmax());
            return coderule.getVmax();
        } else if (DATE.equals(coderule.getItype())) {//日期格式
            return lenHandle(dateFormat(coderule.getVvalue()), coderule.getIlength(), "");
        } else if (TYPE.equals(coderule.getItype())) {
            if (coderule.getTypecode() != null) {
                return coderule.getTypecode();
            }
        }
        return "";
    }

    /**
     * 生成流水号
     *
     * @return
     */
    public String getCodeHandle(String code, int codeLen) {
        if (code == null || "".equals(code)) {
            code = "0";
        }
        String c = (Integer.valueOf(code) + 1) + "";
        if (codeLen > c.length()) {
            int len = codeLen - c.length();
            for (int i = 0; i < len; i++) {
                c = "0" + c;
            }
        }
        return c;
    }

    /**
     * 日期格式化
     *
     * @param dataType 日期格式
     * @return
     */
    public String dateFormat(String dataType) {
        SimpleDateFormat df = new SimpleDateFormat(dataType);
        String dateStr = df.format(new Date());
        return dateStr == null ? "" : dateStr;
    }

    /**
     * 长度处理，不足在前面补充
     *
     * @param value 编码值
     * @param len   编码长度
     * @param ex    补充内容
     * @return
     */
    public String lenHandle(String value, int len, String ex) {
        if (value == null || value == "") {
            value = "";
            for (int i = 0; i < len; i++) {
                value = ex + value;
            }
            return value;
        } else if (value.length() == len) {
            return value;
        } else {
            int l = len - value.length();
            if (l > 0) {
                for (int i = 0; i < l; i++) {
                    value = ex + value;
                }
            }
            return value;
        }
    }

    /**
     * 长度处理，不足在后面补充
     *
     * @param value 编码值
     * @param len   编码长度
     * @param ex    补充内容
     * @return
     */
    public String lenBeHandle(String value, int len, String ex) {
        if (value == null || value == "") {
            value = "";
            for (int i = 0; i < len; i++) {
                value = value + ex;
            }
            return value;
        } else if (value.length() == len) {
            return value;
        } else {
            int l = len - value.length();
            if (l > 0) {
                for (int i = 0; i < l; i++) {
                    value = value + ex;
                }
            }
            return value;
        }
    }

    /**
     * 判定编码格式是否正确
     *
     * @param codeFormat 编码规则PK
     * @param code       编码
     * @param lv         编码级别
     * @return
     */
    public String codeFormat(String codeFormat, String code, String parentCode, Integer lv, String pk_group) {
        if (codeFormat != null && code != null) {
            Coderule coderule = new Coderule();
            coderule.setAcct(pk_group);
            coderule.setPk_coderule(codeFormat);
            coderule = getCR(coderule);
            if (coderule == null || "0".equals(coderule.getIcodeseries() == null ? null : coderule.getIcodeseries().toString())) {
                return "ok";
            }
//            if (parentCode == null || parentCode == "") {//上级编码为空
//                if (!"1".equals(lv != null ? lv.toString() : null)) {//看看输入的是不是第一级编码
//                    return "error";
//                }
//            }
            if (code == null || code == "") {
                return "error";
            }
            if (lv == null || lv.equals("")) {
                return "error";
            }
            int series = lv;
            if (series == 0) {
                series = coderule.getIcodeseries();
            }
            int length = 0;
            if (coderule.getItotallength() >= code.length() || "2".equals(coderule.getIsrule().toString())) {
                for (int i = 1; i <= series; i++) {
                    try {
                        Class c = coderule.getClass();
                        coderule.setIlength((Integer) c.getMethod("getIlength" + i).invoke(coderule));
                        coderule.setItype((String) c.getMethod("getItype" + i).invoke(coderule));
                        coderule.setVvalue((String) c.getMethod("getVvalue" + i).invoke(coderule));
                        if (lv == 0 || "0".equals(lv)) {
                            if (!typeValueFormat(length, coderule, code, parentCode)) {
                                return "error";
                            }
                        } else {
                            if (parentCode == "" || parentCode == null) {//如果上级编码为空 证明是code为第一级编码
                                if (!typeValueFormat(length, coderule, code, null)) {
                                    return "error";
                                }
                            } else {
                                if ("".equals(coderule.getVvalue()) || coderule.getVvalue() == null) {
                                    if(coderule.getIlength()==null){
                                        return "error";
                                    }
                                    int exLen = length + coderule.getIlength();
                                    if (parentCode.length() >= coderule.getIlength()) {
                                        if (code.length() >= exLen) {
                                            if (!code.substring(length, length + coderule.getIlength()).equals(parentCode.substring(0, coderule.getIlength()))) {
                                                return "error";
                                            } else {
                                                if (typeValueFormat(length, coderule, code, null)) {
                                                    parentCode = parentCode.substring(coderule.getIlength(), parentCode.length());
                                                }
                                            }
                                        }
                                    } else {
                                        if (!typeValueFormat(length, coderule, code, null)) {
                                            return "error";
                                        }
                                    }

                                }
                            }
                        }
                        length += coderule.getIlength();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
                if (length != code.length()) {
                    return "error";
                }
            } else {
                return "error";
            }
        } else {
            return "error";
        }
        return "ok";
    }

    /**
     * 判定编码格式是否正确
     *
     * @param codeFormat 编码规则PK
     * @param code       编码
     * @return
     */
    public String codeFormat(String codeFormat, String code, String pk_group) {
        if (codeFormat != null && code != null) {
            Coderule coderule = new Coderule();
            coderule.setAcct(pk_group);
            coderule.setPk_coderule(codeFormat);
            coderule = getCR(coderule);
            if (coderule == null || "0".equals(coderule.getIcodeseries() == null ? null : coderule.getIcodeseries().toString())) {
                return "ok";
            }
            if (code == null || code == "") {
                return "error";
            }
            int length = 0;
            if (coderule.getItotallength() >= code.length()) {
                for (int i = 1; i <= coderule.getIcodeseries(); i++) {//反射循环对象 获取数据
                    try {
                        Class c = coderule.getClass();
                        coderule.setIlength((Integer) c.getMethod("getIlength" + i).invoke(coderule));
                        coderule.setItype((String) c.getMethod("getItype" + i).invoke(coderule));
                        coderule.setVvalue((String) c.getMethod("getVvalue" + i).invoke(coderule));
                        if (!typeValueFormat(length, coderule, code, null)) {//判定是否符合编码规则
                            return "error";
                        }
                        length += coderule.getIlength();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
                if (length != code.length()) {
                    return "error";
                }
            } else {
                return "error";
            }
        } else {
            return "error";
        }
        return "ok";
    }

    /**
     * 根据类型组合编码
     *
     * @param sumLen 目前级别编码总长度
     * @return
     */
    public boolean typeValueFormat(int sumLen, Coderule coderule, String code, String parentCode) {
        if (sumLen + coderule.getIlength() > code.length()) {
            return false;
        }
        String vsub = code.substring(sumLen, sumLen + coderule.getIlength());
        if (NULL.equals(coderule.getItype())) {//无
            return isvalue(coderule.getVvalue(), vsub);
        } else if (LETTER.equals(coderule.getItype())) {//字母
            if (vsub.matches("[a-zA-Z]+")) {
                return isvalue(coderule.getVvalue(), vsub);
            }
            return false;
        } else if (FIGURE.equals(coderule.getItype())) {//数字
            if (vsub.matches("^[0-9]*$")) {
                return isvalue(coderule.getVvalue(), vsub);
            }
            return false;
        } else if (R_WATER.equals(coderule.getItype())) {//流水
            if (vsub.matches("^[0-9]*$")) {
                return true;
            }
        } else if (DATE.equals(coderule.getItype())) {//日期
            if (vsub.matches("((^((1[8-9]\\d{2})|([2-9]\\d{3}))(10|12|0?[13578])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\\d{2})|([2-9]\\d{3}))(11|0?[469])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\\d{2})|([2-9]\\d{3}))(0?2)(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)(0?2)(29)$)|(^([3579][26]00)(0?2)(29)$)|(^([1][89][0][48])(0?2)(29)$)|(^([2-9][0-9][0][48])(0?2)(29)$)|(^([1][89][2468][048])(0?2)(29)$)|(^([2-9][0-9][2468][048])(0?2)(29)$)|(^([1][89][13579][26])(0?2)(29)$)|(^([2-9][0-9][13579][26])(0?2)(29)$))")) {
                return true;
            }
        } else if (TYPE.equals(coderule.getItype())) {//类别
            if (code.length() >= parentCode.length()) {
                if (parentCode.equals(code.substring(sumLen, parentCode.length()))) {
                    coderule.setIlength(parentCode.length());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判定值是否相同
     *
     * @param value
     * @param vsub
     * @return
     */
    public boolean isvalue(String value, String vsub) {
        if (value != null && !"".equals(value)) {
            if (!value.equals(vsub)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 获取编码规则样式
     *
     * @param codeFormat
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    public String getFormat(String codeFormat, String pk_group) throws CRUDException {
        if (codeFormat == null) {
            throw new CRUDException("编码规则编码不能为空！");
        }
        Coderule coderule = new Coderule();
        coderule.setAcct(pk_group);
        coderule.setPk_coderule(codeFormat);
        coderule = getCR(coderule);
        if (coderule == null) {
            return "";
        }
        String format = "";
        for (int i = 1; i <= coderule.getIcodeseries(); i++) {//反射循环对象 获取数据
            try {
                Class c = coderule.getClass();
                Integer len = (Integer) c.getMethod("getIlength" + i).invoke(coderule);
                String type = (String) c.getMethod("getItype" + i).invoke(coderule);
                format += getMark(len, type) + "-";
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return format.length() > 1 ? format.substring(0, format.length() - 1) : "";
    }

    /**
     * 获取编码规则样式
     *
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    public String getFormat(Coderule coderule) throws CRUDException {
        if (coderule == null) {
            return "";
        }
        String format = "";
        for (int i = 1; i <= coderule.getIcodeseries(); i++) {//反射循环对象 获取数据
            try {
                Class c = coderule.getClass();
                Integer len = (Integer) c.getMethod("getIlength" + i).invoke(coderule);
                String type = (String) c.getMethod("getItype" + i).invoke(coderule);
                format += getMark(len, type) + "-";
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return format.length() > 1 ? format.substring(0, format.length() - 1) : "";
    }

    /**
     * 生成编码格式 样式
     *
     * @param vlen
     * @param type
     * @return
     */
    public String getMark(int vlen, String type) {
//        if(LETTER.equals(type)){//字母
//            return lenBeHandle("",vlen,"X");
//        }else if(FIGURE.equals(type)){//数字
//            return lenBeHandle("",vlen,"X");
//        }else if(R_WATER.equals(type)){//流水
//            return lenBeHandle("", vlen, "X");
//        }else if(DATE.equals(type)){//时间
//            return lenBeHandle("",vlen,"X");
//        }
        return lenBeHandle("", vlen, "X");
    }

    /**
     * 获取coderule
     *
     * @param cr
     * @return
     */
    public Coderule getCR(Coderule cr) {
    	//获取编码规则详细信息
        Coderule coderule = coderuleMapper.findCoderuleOrgByPk(cr);
        if (coderule == null) {
            coderule = coderule == null ? coderuleMapper.findCoderuleByPk(cr) : null;
            if (coderule != null) {
                try {
                    findcoderuleList(cr);
                } catch (CRUDException e) {
                    e.printStackTrace();
                }
                coderule = coderuleMapper.findCoderuleOrgByPk(cr);
                if (cr.getAcct() != null && cr.getAcct() != "") {
                    if (coderule.getAcct() == null || coderule.getAcct() == "") {
                        coderule.setAcct(cr.getAcct());
                    }
                }
            }
        }
        return coderule;
    }
    /**
     * 获取字符串后的流水号
     * @param str
     * @param coderule 
     * @return
     */
    public static String getNumFromStr(String str, Coderule coderule){
    	String res = "";
    	int len = coderule.getIlength();
    	if(ValueCheck.IsNotEmpty(str)){
    		for(int i =str.length()-1;i>=0;i--){
    			if(len>0){//如果在设置的编码规则长度内，继续执行
	    			if('0'<=str.charAt(i) && str.charAt(i)<='9'){
	    				res =str.charAt(i)+res;
	    				len--;//编码长度减一
	    			}else{
	    				break; //获取的不为数字，跳出循环
	    			}
    			}else{
    				break; //长度为0 跳出循环
    			}
    		}
    	}else{
    		res = "1";
    	}
    	return res;
    }
    public static void main(String[] args) {
    	Coderule coderule = new Coderule();
    	coderule.setIlength(2);
		System.out.println(getNumFromStr("test001",coderule));
	}
}

