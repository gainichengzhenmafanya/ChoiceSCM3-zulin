package com.choice.assistant.service.system;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Problems;
import com.choice.assistant.persistence.system.ProblemsMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 常见问题
 * Created by mc on 15/2/3.
 */
@Service
public class ProblemsService {
    @Autowired
    private ProblemsMapper problemsMapper;
    @Autowired
    private PageManager<Problems> pageManager;

    private final transient Log log = LogFactory.getLog(ProblemsService.class);

    /**
     * 获取所有常见问题
     * @param problems
     * @return
     */
    public List<Problems> findProblems(Problems problems,Page page){
        try {
            return pageManager.selectPage(problems, page,ProblemsMapper.class.getName()+".findProblems");
        } catch (Exception e) {
            log.error(e);
        }
        return null;
    }
    /**
     * 获取所有常见问题
     * @param problems
     * @return
     */
    public Problems findProblemsById(Problems problems){
        try {
            List<Problems> problemses=problemsMapper.findProblems(problems);
            if(problemses!=null&&problemses.size()>0){
                return problemses.get(0);
            }
        } catch (Exception e) {
            log.error(e);
        }
        return null;
    }

    /**
     * 新政常见问题
     * @param problems
     * @return
     */
    public int addProblems(Problems problems){
        try {
            problems.setPk_problems(CodeHelper.createUUID());
            problems.setTs(DateFormat.getStringByDate(new Date(),DateFormat.YYYYMMDDHHMMSS));
            Integer integer=problemsMapper.addProblems(problems);
            if(integer>0){
                return 1;
            }
        } catch (Exception e) {
            log.error(e);
        }
        return 0;
    }

    /**
     * 删除常见问题
     * @param ids
     * @return
     */
    public int delProblems(String ids){
        try {
            if(ids!=null && !"".equals(ids)) {
                List<String> list=Arrays.asList(ids.split(","));
                if(null!=list&&list.size()>0) {
                    Integer integer = problemsMapper.delProblems(list);
                    if (integer > 0) {
                        return 1;
                    }
                }
            }
        } catch (Exception e) {
            log.error(e);
        }
        return 0;
    }

    /**
     * 修改常见问题
     * @param problems
     * @return
     */
    public int updateProblems(Problems problems) {
        try {
            problems.setTs(DateFormat.getStringByDate(new Date(),DateFormat.YYYYMMDDHHMMSS));
            Integer integer=problemsMapper.updateProblems(problems);
            if(integer>0){
                return 1;
            }
        } catch (Exception e) {
            log.error(e);
        }
        return 0;
    }
}
