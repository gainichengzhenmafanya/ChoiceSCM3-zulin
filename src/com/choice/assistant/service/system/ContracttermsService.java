package com.choice.assistant.service.system;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Contractterms;
import com.choice.assistant.persistence.system.ContracttermsMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 合同条款类型设置
 * Created by 臧国良 on 2014.10.24
 */
@Service
public class ContracttermsService {

	@Autowired
    private ContracttermsMapper contracttermsMapper;
	@Autowired
	private PageManager<Contractterms> pageManager;

    private final transient Log log = LogFactory.getLog(ContracttermsService.class);

    /**
     * 查询全部合同条款类型设置
     * @return
     * @throws CRUDException
     */
    public List<Contractterms> findContracttermsListByPage(Contractterms contractterms,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(contractterms, page,ContracttermsMapper.class.getName()+".findContracttermsList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部合同条款类型设置
     * @return
     * @throws CRUDException
     */
    public List<Contractterms> findContracttermsList(Contractterms contractterms) throws CRUDException {
        try{
            List<Contractterms> list=contracttermsMapper.findContracttermsList(contractterms);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定合同条款类型设置单条
     * @return
     * @throws CRUDException
     */
    public Contractterms findContracttermsByPk(Contractterms contractterms) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(contractterms.getPk_contractterms())){
    			return null;
    		}
    		Contractterms list=contracttermsMapper.findContracttermsByPk(contractterms);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存合同条款类型
     * @param contractterms
     * @throws CRUDException
     */
    public String saveContractterms(Contractterms contractterms) throws CRUDException {
        try{
        	contractterms.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(contractterms.getPk_contractterms())){
        		contractterms.setDr(0);
	        	contractterms.setPk_contractterms(CodeHelper.createUUID());
	            contracttermsMapper.addContractterms(contractterms);
        	}else{
                contracttermsMapper.updateContractterms(contractterms);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用合同条款类型
     * @param contractterms
     * @throws CRUDException
     */
    public String enableContractterms(Contractterms contractterms) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(contractterms.getPk_contractterms())){
    			return null;
    		}else{
    			contractterms.setPk_contractterms(AsstUtils.StringCodeReplace(contractterms.getPk_contractterms()));
    		}
    		contractterms.setTs(DateFormat.getTs());
			contracttermsMapper.enableContractterms(contractterms);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除合同条款类型
     * @param contractterms
     * @throws CRUDException
     */
    public String deleteContractterms(Contractterms contractterms) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(contractterms.getPk_contractterms())){
    			return null;
    		}else{
    			contractterms.setPk_contractterms(AsstUtils.StringCodeReplace(contractterms.getPk_contractterms()));
    		}
    		contractterms.setTs(DateFormat.getTs());
    		contracttermsMapper.deleteContractterms(contractterms);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 公用查询方法
     * @param contractterms
     * @throws CRUDException
     */
    public List<Map<String,Object>> queryInfoList(Map<String,Object> paramMap) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(paramMap)){
    			return null;
    		}else{
    			return contracttermsMapper.queryInfoList(paramMap);
    		}
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
