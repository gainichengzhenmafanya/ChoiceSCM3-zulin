package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Tax;
import com.choice.assistant.persistence.system.TaxRateMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 税率设置
 * Created by 臧国良 on 2014.10.24
 */
@Service
public class TaxRateService {

	@Autowired
    private TaxRateMapper taxMapper;
	@Autowired
	private PageManager<Tax> pageManager;

    private final transient Log log = LogFactory.getLog(TaxRateService.class);

    /**
     * 查询全部税率设置
     * @return
     * @throws CRUDException
     */
    public List<Tax> findTaxRateListByPage(Tax tax,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(tax, page,TaxRateMapper.class.getName()+".findTaxRateList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部税率设置
     * @return
     * @throws CRUDException
     */
    public List<Tax> findTaxRateList(Tax tax) throws CRUDException {
        try{
            List<Tax> list=taxMapper.findTaxRateList(tax);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定税率设置单条
     * @return
     * @throws CRUDException
     */
    public Tax findTaxRateByPk(Tax tax) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(tax.getPk_tax())){
    			return null;
    		}
    		Tax list=taxMapper.findTaxRateByPk(tax);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存税率
     * @param tax
     * @throws CRUDException
     */
    public String saveTaxRate(Tax tax) throws CRUDException {
        try{
        	tax.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(tax.getPk_tax())){
        		tax.setDr(0);
	        	tax.setPk_tax(CodeHelper.createUUID());
	            taxMapper.addTaxRate(tax);
        	}else{
                taxMapper.updateTaxRate(tax);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用税率
     * @param tax
     * @throws CRUDException
     */
    public String enableTaxRate(Tax tax) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(tax.getPk_tax())){
    			return null;
    		}else{
    			tax.setPk_tax(AsstUtils.StringCodeReplace(tax.getPk_tax()));
    		}
    		tax.setTs(DateFormat.getTs());
			taxMapper.enableTaxRate(tax);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除税率
     * @param tax
     * @throws CRUDException
     */
    public String deleteTaxRate(Tax tax) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(tax.getPk_tax())){
    			return null;
    		}else{
    			tax.setPk_tax(AsstUtils.StringCodeReplace(tax.getPk_tax()));
    		}
    		tax.setTs(DateFormat.getTs());
    		taxMapper.deleteTaxRate(tax);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
