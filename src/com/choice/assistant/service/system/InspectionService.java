package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.system.Inspection;
import com.choice.assistant.persistence.system.InspectionMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 验货类型设置
 * Created by 臧国良 on 2014.10.27
 */
@Service
public class InspectionService {

	@Autowired
    private InspectionMapper inspectionMapper;
	@Autowired
	private PageManager<Inspection> pageManager;

    private final transient Log log = LogFactory.getLog(InspectionService.class);

    /**
     * 查询全部验货类型设置
     * @return
     * @throws CRUDException
     */
    public List<Inspection> findInspectionListByPage(Inspection inspection,Page page) throws CRUDException {
        try{
            return pageManager.selectPage(inspection, page,InspectionMapper.class.getName()+".findInspectionList");
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询全部验货类型设置
     * @return
     * @throws CRUDException
     */
    public List<Inspection> findInspectionList(Inspection inspection) throws CRUDException {
        try{
            List<Inspection> list=inspectionMapper.findInspectionList(inspection);
            return list;
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 查询指定验货类型设置单条
     * @return
     * @throws CRUDException
     */
    public Inspection findInspectionByPk(Inspection inspection) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(inspection.getPk_inspection())){
    			return null;
    		}
    		Inspection list=inspectionMapper.findInspectionByPk(inspection);
    		return list;
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 保存验货类型
     * @param inspection
     * @throws CRUDException
     */
    public String saveInspection(Inspection inspection) throws CRUDException {
        try{
        	inspection.setTs(DateFormat.getTs());
        	if(ValueCheck.IsEmpty(inspection.getPk_inspection())){
        		inspection.setDr(0);
	        	inspection.setPk_inspection(CodeHelper.createUUID());
	            inspectionMapper.addInspection(inspection);
        	}else{
                inspectionMapper.updateInspection(inspection);
        	}
            return "T";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 启用、禁用验货类型
     * @param inspection
     * @throws CRUDException
     */
    public String enableInspection(Inspection inspection) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(inspection.getPk_inspection())){
    			return null;
    		}else{
    			inspection.setPk_inspection(AsstUtils.StringCodeReplace(inspection.getPk_inspection()));
    		}
    		inspection.setTs(DateFormat.getTs());
			inspectionMapper.enableInspection(inspection);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }
    /**
     * 删除验货类型
     * @param inspection
     * @throws CRUDException
     */
    public String deleteInspection(Inspection inspection) throws CRUDException {
    	try{
    		if(ValueCheck.IsEmpty(inspection.getPk_inspection())){
    			return null;
    		}else{
    			inspection.setPk_inspection(AsstUtils.StringCodeReplace(inspection.getPk_inspection()));
    		}
    		inspection.setTs(DateFormat.getTs());
    		inspectionMapper.deleteInspection(inspection);
    		return "T";
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

}
