package com.choice.assistant.service.system;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.material.ReviMaterial;
import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.system.CollDeliver;
import com.choice.assistant.domain.system.CollMaterial;
import com.choice.assistant.persistence.system.FavoritesMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 收藏夹
 * @author zhb
 *
 */
@Service
public class FavoritesService {
	
	@Autowired
	private FavoritesMapper favoritesMapper;
	@Autowired
    private PageManager<CollMaterial> pageManager;
	@Autowired
    private PageManager<CollDeliver> pageManagers;
	
	private final transient Log log = LogFactory.getLog(FavoritesService.class);
	/**
	 * 获取所有商品收藏数据
	 */
	public List<CollMaterial> findAllCollMaterial(CollMaterial collMaterial,Page page){
		//return favoritesMapper.findAllCollMaterial(collMaterial);
		return pageManager.selectPage(collMaterial, page, FavoritesMapper.class.getName() + ".findAllCollMaterial");
	}
	/**
	 * 商品具体详情
	 */
	public CollMaterial findAllCollMaterialbyspid(CollMaterial collMaterial){
		
		return favoritesMapper.findAllCollMaterialbyspid(collMaterial);
	}
	/**
	 * 保存商品到收藏夹
	 * @param collMaterial
	 * @return
	 * @throws CRUDException
	 */
	public String saveFavorite(CollMaterial collMaterial) throws CRUDException {
		try{
			if(ValueCheck.IsNotEmpty(collMaterial.getPk_mallmaterial())){
				collMaterial.setPk_collmaterial(CodeHelper.createUUID());
				collMaterial.setDr(0);
				collMaterial.setTs(DateFormat.getTs());
				//处理价格为空的情况
				if(ValueCheck.IsEmpty(collMaterial.getNamount())){
					collMaterial.setNamount("0.00");
				}
				if(ValueCheck.IsEmpty(collMaterial.getNmallprice())){
					collMaterial.setNmallprice("0.00");
				}
				if(ValueCheck.IsEmpty(collMaterial.getNmatprice())){
					collMaterial.setNmatprice("0.00");
				}
				favoritesMapper.saveFavorite(collMaterial);
				return "1";
			}else{
				return "0";
			}
		 }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
	}
	/**
	 * 保存供应商到收藏夹
	 * @param collMaterial
	 * @return
	 * @throws CRUDException
	 */
	public String saveGysFavorite(CollDeliver collDeliver) throws CRUDException {
		try{
			if(ValueCheck.IsNotEmpty(collDeliver.getPk_deliver())){
				collDeliver.setPk_colldeliver(CodeHelper.createUUID());
				collDeliver.setDr(0);
				collDeliver.setTs(DateFormat.getTs());
				favoritesMapper.saveGysFavorite(collDeliver);
				return "1";
			}else{
				return "0";
			}
		 }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
	}
	/**
	 * 删除商品
	 */
	public void deletespFavoritesById(CollMaterial collMaterial){
		if(ValueCheck.IsNotEmpty(collMaterial.getPk_collmaterial())
				|| ValueCheck.IsNotEmpty(collMaterial.getPk_mallmaterial()))
		favoritesMapper.deletespFavoritesById(collMaterial);
	}
	/**
	 * 待审核物资
	 * @param reviMaterial
	 */
	public void saveAuditMaterial(ReviMaterial reviMaterial){
		favoritesMapper.saveAuditMaterial(reviMaterial);
	}
	/**
	 * 获取所有供应商收藏数据
	 */
	public List<CollDeliver> findAllCollDeliver(CollDeliver collDeliver,Page page) {
		//return favoritesMapper.findAllCollDeliver(collDeliver);
		return pageManagers.selectPage(collDeliver, page, FavoritesMapper.class.getName() + ".findAllCollDeliver");
	}
	/**
	 * 供应商具体详情
	 */
	public CollDeliver findAllCollDeliverbygysid(CollDeliver collDeliver) {
		return favoritesMapper.findAllCollDeliverbygysid(collDeliver);
	}
	/**
	 * 删除供应商
	 */
	public void deletegysFavoritesById(CollDeliver collDeliver){
		if(ValueCheck.IsNotEmpty(collDeliver.getPk_colldeliver())
				|| ValueCheck.IsNotEmpty(collDeliver.getPk_deliver()))
		favoritesMapper.deletegysFavoritesById(collDeliver);
	}
	/**
	 * 待审核供应商
	 * @param auditsupplier
	 */
	public void saveAuditDeliver(Auditsupplier auditsupplier){
		favoritesMapper.saveAuditDeliver(auditsupplier);
	}
	/**
	 * 判断加入待审核物资是否重复
	 * @param pk_collmaterial
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public String findAuditbypkss(String pk_collmaterial,String acct) throws CRUDException {
		return	favoritesMapper.findAuditbypkss(pk_collmaterial,acct);
	}
	/**
	 * 判断加入供应商待审核是否重复
	 * @param pk_colldeliver
	 * @return
	 * @throws CRUDException
	 */
	public String findAuditbypks(String pk_colldeliver,String acct) throws CRUDException {
		return	favoritesMapper.findAuditbypks(pk_colldeliver,acct);
	}
}
