package com.choice.assistant.service.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.common.ReportObject;
import com.choice.assistant.domain.report.Condition;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.persistence.Report.PurchaseMapper;
import com.choice.assistant.persistence.bill.PuprOrderMapper;
import com.choice.assistant.persistence.supplier.SupplierMapper;
import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 报表Service
 * Created by mc on 14-11-16.
 */
@Service("PurchaseService")
public class PurchaseService {
    @Autowired
    private PurchaseMapper purchaseMapper;
    @Autowired
    private SupplierMapper supplierMapper;
    @Autowired
    private PuprOrderMapper puprorderMapper;
    @Autowired
	private ReportObject<Map<String,Object>> reportObject=new ReportObject<Map<String, Object>>();
	@Autowired
	private PageManager<Map<String,Object>> pageManager;

    /**
	 * 采购明细查询
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author lq
	 */
	public ReportObject<Map<String, Object>> findPuprOrderDetail(Condition condition) throws Exception {
		try {
			Page pager = condition.getPager();
            ids2list(condition);
			reportObject.setRows(pageManager.selectPage(condition, pager,PurchaseMapper.class.getName()+ ".findPuprOrderDetail"));
			reportObject.setFooter(purchaseMapper.findPuprOrderDetailSum(condition));
			System.out.println(reportObject.getRows().toString());
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 物资汇总查询
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author lq
	 */
	public ReportObject<Map<String, Object>> findMaterialSummary(Condition condition) throws Exception {
		try {
			Page pager = condition.getPager();
            ids2list(condition);
			reportObject.setRows(pageManager.selectPage(condition, pager, PurchaseMapper.class.getName() + ".findMaterialSummary"));
			reportObject.setFooter(purchaseMapper.findMaterialSummarySum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 物资汇总查询
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author lq
	 */
	public ReportObject<Map<String, Object>> findMaterialTypeSummary(Condition condition) throws Exception {
		try {
			Page pager = condition.getPager();
            ids2list(condition);
			reportObject.setRows(pageManager.selectPage(condition, pager,PurchaseMapper.class.getName()+ ".findMaterialTypeSummary"));
			reportObject.setFooter(purchaseMapper.findMaterialTypeSummarySum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 供应商物资类别汇总查询
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author lq
	 */
	public ReportObject<Map<String, Object>> findSupplierMaterialTypeSummary(Condition condition) throws Exception {
		Page pager = condition.getPager();
		int nowPage = pager.getNowPage();
		int pagesize = pager.getPageSize();
		Supplier supplier = new Supplier();
		supplier.setAcct(condition.getAcct());
		supplier.setEnablestate(2);
		supplier.setBegin((nowPage-1)*pagesize+1);
		supplier.setEnd(nowPage*pagesize);
		List<Supplier> supplierlist = supplierMapper.findSupplierPage(supplier);
		
		List<String> supplierpks = new ArrayList<String>();
		for(Supplier s:supplierlist){
			supplierpks.add(s.getPk_supplier());
		}
		condition.setSupplierPks(supplierpks);
		List<Map<String, Object>> preorderlist = purchaseMapper.findMaterialSupplier(condition);
		
		try {
			
			reportObject.setRows(pageManager.selectPage(condition, pager,PurchaseMapper.class.getName()+ ".findMaterialTypeSummary"));
			reportObject.setFooter(purchaseMapper.findMaterialTypeSummarySum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
    /**
     * pks set list pk
     * @param condition
     */
    public void ids2list(Condition condition){
        if(condition!=null){
            condition.setMaterialPks(resultList(condition.getPk_material()));
            condition.setSupplierPks(resultList(condition.getPk_supplier()));
            condition.setMaterialtypePks(resultList(condition.getPk_materialtype()));
            condition.setOrgPks(resultList(condition.getPk_org()));
        }
    }

    /**
     * String to list
     * @param val
     * @return
     */
    private List<String> resultList(String val){
        if(val!=null&&val!=""){
            return Arrays.asList(val.split(","));
        }
        return null;
    }
}
