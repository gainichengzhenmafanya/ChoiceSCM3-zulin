package com.choice.assistant.service.report;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.common.ReportObject;
import com.choice.assistant.domain.report.Condition;
import com.choice.assistant.persistence.Report.InspectReportMapper;
import com.choice.assistant.persistence.Report.PurchaseMapper;
import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 报表Service
 * Created by mc on 14-11-16.
 */
@Service("InspectReportService")
public class InspectReportService {
    @Autowired
    private InspectReportMapper inspectReportMapper;
    @Autowired
	private ReportObject<Map<String,Object>> reportObject;
	@Autowired
	private PageManager<Map<String,Object>> pageManager;

	/**
	 * 验货明细查询
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author lq
	 */
	public ReportObject<Map<String, Object>> findInspectDetail(Condition condition) throws Exception {
		try {
			Page pager = condition.getPager();
            ids2list(condition);
			reportObject.setRows(pageManager.selectPage(condition, pager,InspectReportMapper.class.getName()+ ".findInspectDetail"));
			reportObject.setFooter(inspectReportMapper.findInspectDetailSum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 验货物资汇总查询
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author lq
	 */
	public ReportObject<Map<String, Object>> findInspectMaterialSummary(Condition condition) throws Exception {
		try {
			Page pager = condition.getPager();
            ids2list(condition);
			reportObject.setRows(pageManager.selectPage(condition, pager,InspectReportMapper.class.getName()+ ".findInspectMaterialSummary"));
			reportObject.setFooter(inspectReportMapper.findInspectMaterialSummarySum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * 物资类别统计
	 * @param condition
	 * @return
	 * @throws CRUDException
	 * @author lq
	 */
	public ReportObject<Map<String, Object>> findInspectMaterialTypeSummary(Condition condition) throws Exception {
		try {
			Page pager = condition.getPager();
            ids2list(condition);
			reportObject.setRows(pageManager.selectPage(condition, pager,PurchaseMapper.class.getName()+ ".findInspectMaterialTypeSummary"));
			reportObject.setFooter(inspectReportMapper.findInspectMaterialTypeSummarySum(condition));
			reportObject.setTotal(pager.getCount());
			return reportObject;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

    /**
     * pks set list pk
     * @param condition
     */
    public void ids2list(final Condition condition){
        if(condition!=null){
            condition.setMaterialPks(resultList(condition.getPk_material()));
            condition.setSupplierPks(resultList(condition.getPk_supplier()));
            condition.setMaterialtypePks(resultList(condition.getPk_materialtype()));
            condition.setOrgPks(resultList(condition.getPk_org()));
        }
    }

    /**
     * String to list
     * @param val
     * @return
     */
    private List<String> resultList(String val){
        if(val!=null&&val!=""){
            return Arrays.asList(val.split(","));
        }
        return null;
    }
}
