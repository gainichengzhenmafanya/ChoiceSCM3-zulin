//package com.choice.assistant.service.bill;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.choice.assistant.domain.bill.PuprOrder;
//import com.choice.assistant.domain.bill.PuprOrderd;
//import com.choice.assistant.persistence.bill.PuprOrderMapper;
//import com.choice.assistant.util.JsonFormatUtil;
//import com.choice.webservice.IPuprOrderService;
//
///**
// * 采购订单管理
// * @author LQ
// *
// */
//@Service
//public class PuprOrderServiceImpl implements IPuprOrderService{
//	
//	@Autowired
//	private PuprOrderMapper puprOrderMapper;
//
//	/**
//	 * webservice保存订单
//	 */
//	@Override
//	public String jmu_erp_savepurchasebill(String purchasejson) {
//		Map<String,Class<?>> childrenClassMap = new HashMap<String,Class<?>>();
//		childrenClassMap.put("puprOrderdList", PuprOrderd.class);
//		PuprOrder puprOrder = (PuprOrder)JsonFormatUtil.formatAggObjectJson(purchasejson, PuprOrder.class,childrenClassMap);
//		
//		PuprOrder checkPuprOrder = puprOrderMapper.queryPuprOrderByPk(puprOrder);
//		if(null!=checkPuprOrder){
//			return "-1";
//		}
//		puprOrderMapper.savePuprOrderm(puprOrder);
//		for(PuprOrderd puprOrderd:puprOrder.getPuprOrderdList()){
//			puprOrderMapper.savePuprOrderd(puprOrderd);
//		}
//		return "1";
//	}
//
//	/**
//	 * webservice撤销订单
//	 */
//	@Override
//	public String jmu_erp_cancelpurchasebill(String pk_puprorder) {
//		PuprOrder puprOrder = new PuprOrder();
//		puprOrder.setPk_puprorder(pk_puprorder);
//		puprOrder = puprOrderMapper.queryPuprOrderByPk(puprOrder);
//		if(null==puprOrder){
//			return "-1";
//		}
//		puprOrder.setIstate(-1);
//		puprOrderMapper.updatePuprOrderm(puprOrder);
//		return "1";
//	}
//}
