package com.choice.assistant.service.bill;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.PurchaseSet.Inspectd;
import com.choice.assistant.domain.PurchaseSet.Inspectm;
import com.choice.assistant.domain.bill.PreOrder;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.assistant.persistence.bill.PuprOrderMapper;
import com.choice.assistant.service.purchaseSet.InspectService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 采购订单管理
 * @author LQ
 *
 */
@Service
public class PuprOrderService{
	
	@Autowired
	private PuprOrderMapper puprOrderMapper;
	@Autowired
	private PageManager<PuprOrder> pageManager;
	@Autowired
	private CoderuleService coderuleService;
	@Autowired
	private InspectService inspectService;
	@Autowired
	private PreOrderService preOrderService;
	
	private final transient Log log = LogFactory.getLog(PuprOrderService.class);
	
	/**
	 * 查询所有的订单信息
	 * @param puprOrder
	 * @return
	 */
	public List<PuprOrder> queryAllPuprOrders(PuprOrder puprOrder,Page page) throws CRUDException{
		try {
			return pageManager.selectPage(puprOrder, page, PuprOrderMapper.class.getName()+".queryAllPuprOrders");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询菜都订单子表信息
	 * @param puprOrderd
	 * @return
	 * @throws CRUDException
	 */
	public List<PuprOrderd> queryAllPuprOrderdsByPK(PuprOrderd puprOrderd) throws CRUDException{
		try {
			return puprOrderMapper.queryAllPuprOrderdsByPK(puprOrderd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询菜都订单子表信息
	 * @param puprOrderd
	 * @return
	 * @throws CRUDException
	 */
	public List<PuprOrderd> queryAllPuprOrderdsByPk(PuprOrderd puprOrderd) throws CRUDException{
		try {
//			puprOrderd.setIstate(4);//已经到货状态
			return puprOrderMapper.queryAllPuprOrderdsByPk(puprOrderd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取订单的最大单号,按照月流水进行
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String selectMaxVbillno(PuprOrder puprOrder,String group)throws CRUDException{
		try {
			puprOrder.setBdate(DateFormat.getStringByDate(DateFormat.getFirstDayOfCurrMonth(),"yyyy-MM-dd"));
			puprOrder.setEdate(DateFormat.getStringByDate(DateFormat.getEndDayOfCurrMonth(new Date()), "yyyy-MM-dd"));
			String djno = coderuleService.getCode(CoderuleConstants.PURTEMPLET,group);
			if (djno == "") {
				List<PuprOrder> listPuprOrder = puprOrderMapper.selectMaxVbillno(puprOrder);
				if (listPuprOrder.size() == 0) {
					return "CGDD"+DateFormat.getStringByDate(new Date(), "yyyyMMdd")+DateFormat.getStringByDate(new Date(), "yyyy-MM-dd").substring(5, 7)+"0001";
				}else {
					String lastValue = (Integer.parseInt(listPuprOrder.get(0).getVbillno().substring(listPuprOrder.get(0).getVbillno().length()-4)) + 1) +"";
					String value = "";
					if (lastValue.length() == 1) {
						value += "000"+lastValue;
					} else if (lastValue.length() == 2) {
						value += "00"+lastValue;
					} else if (lastValue.length() == 3) {
						value += "0"+lastValue;
					} else if (lastValue.length() == 4) {
						value += lastValue;
					}
					return listPuprOrder.get(0).getVbillno().substring(0,listPuprOrder.get(0).getVbillno().length() - 4)+value;
				}
				
			}else {
				return djno;
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取订单的下一个单号,按照天进行js20161221
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String getNextCGVbillno(Date date,String acct)throws CRUDException{
		try {
			PuprOrder puprOrder = new PuprOrder();
			puprOrder.setAcct(acct);
			puprOrder.setDbilldate(DateFormat.getStringByDate(date,"yyyy-MM-dd"));
			//得到下一个采购单号
			String vbillno = puprOrderMapper.getNextCGVbillno(puprOrder);
			if(vbillno==null){
				vbillno = "0001";
			}
			else if(vbillno.length()==1){
				vbillno = "000"+vbillno;
			}
			else if(vbillno.length()==2){
				vbillno = "00"+vbillno;
			}
			else if(vbillno.length()==3){
				vbillno = "0"+vbillno;
			}
			SimpleDateFormat sif = new SimpleDateFormat("yyyyMMdd");
			vbillno = "CG"+sif.format(date)+getRandomStr(3)+vbillno;
			return vbillno;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 生成几位的随机数
	 * @param length
	 * @return
	 */
	public static String getRandomStr(int length){
		String retur = "";
		Random random = new Random();
		for(int i=0;i<length;i++){
			retur += random.nextInt(10);
		}
		return retur;
	}
	
	/**
	 * 保存订单
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public String savePuprOrder(PuprOrder puprOrder,String group) throws CRUDException{
		try {
			String vbillno = puprOrder.getVbillno();
            puprOrder.setAcct(group);
			puprOrder.setTs(DateFormat.getTs());
			puprOrder.setDbilldate(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDD));
			//默认值
			boolean fromtemplete = false;//是否来自模板根据子表生成订单
			String checksupplier = puprOrder.getDelivercode();
			if(null==checksupplier){
				fromtemplete = true;
			}
			//区分供应商
			Map<String,List<PuprOrderd>> map = new HashMap<String,List<PuprOrderd>>();
			for(PuprOrderd d:puprOrder.getPuprOrderdList()){
				String delivercode = d.getDelivercode();
				if(null==map.get(delivercode)){
					List<PuprOrderd> list = new ArrayList<PuprOrderd>();
					list.add(d);
					map.put(delivercode, list);
				}else{
					List<PuprOrderd> list = map.get(delivercode);
					list.add(d);
					map.put(delivercode, list);
				}
			}
			//不同供应商，生成不同的单据
			for(String key : map.keySet()){
				List<PuprOrderd> dlist = map.get(key);
				double money = 0.0;
				puprOrder.setPk_puprorder(CodeHelper.createUUID());
				//订单编码
				if(null==vbillno || "".equals(vbillno)){
					puprOrder.setVbillno(getNextCGVbillno(new Date(),group));
				}
				//默认值
				puprOrder.setDr(0);
				//供应商
				String delivercode = "";
				String delivername = "";
				for(PuprOrderd puprOrderd:dlist){
					if (puprOrderd.getNcnt() == 0) {
						continue;
					}
					puprOrderd.setPk_puprorder(puprOrder.getPk_puprorder());
					puprOrderd.setPk_puprorderd(CodeHelper.createUUID());
					puprOrderd.setTs(DateFormat.getTs());
					puprOrderd.setAcct(puprOrder.getAcct());
					if (puprOrderd.getNmoney() != 0.0) {
						puprOrderd.setSp_price(puprOrderd.getNmoney() / puprOrderd.getNcnt());
					} else {
						puprOrderd.setNmoney(puprOrderd.getNcnt() * puprOrderd.getSp_price());
					}
					if (puprOrderd.getPositnname() == null || puprOrderd.getPositnname() == "") {
						puprOrderd.setPositncode(puprOrder.getPositncode());
						puprOrderd.setPositnname(puprOrder.getPositnname());
					}
					money += puprOrderd.getNmoney();
					puprOrderd.setDr(0);
					delivercode = puprOrderd.getDelivercode();
					delivername = puprOrderd.getDelivername();
					puprOrderd.setDhopedate(puprOrder.getDhopedate());
					puprOrderMapper.savePuprOrderd(puprOrderd);
				}
				puprOrder.setNmoney(money);
				//主表存在供应商时不赋值
				if(fromtemplete){
					puprOrder.setDelivercode(delivercode);
					puprOrder.setDelivername(delivername);
				}
				puprOrderMapper.savePuprOrderm(puprOrder);
			}
			//返回值
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存订单，相同的采购组织
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public String savePuprOrder2(PuprOrder puprOrder,String group) throws CRUDException{
		try {
			Date date = new Date();
            puprOrder.setAcct(group);
			puprOrder.setTs(DateFormat.getTs());
			puprOrder.setDbilldate(DateFormat.getStringByDate(date, DateFormat.YYYYMMDD));
			//判断有没有值
			if(puprOrder==null||puprOrder.getPuprOrderdList()==null||puprOrder.getPuprOrderdList().size()==0){
				return "1";
			}
			puprOrder.setDmaketime(DateFormat.getStringByDate(date, DateFormat.YYYYMMDDHHMMSS));
			//设置positncode和positnname
			puprOrder.setPositncode(puprOrder.getPuprOrderdList().get(0).getPositncode());
			puprOrder.setPositnname(puprOrder.getPuprOrderdList().get(0).getPositnname());
			//区分供应商
			Map<String,List<PuprOrderd>> map = new HashMap<String,List<PuprOrderd>>();
			for(PuprOrderd d:puprOrder.getPuprOrderdList()){
				String delivercode = d.getDelivercode();
				if(null==map.get(delivercode)){
					List<PuprOrderd> list = new ArrayList<PuprOrderd>();
					list.add(d);
					map.put(delivercode, list);
				}else{
					List<PuprOrderd> list = map.get(delivercode);
					list.add(d);
					map.put(delivercode, list);
				}
			}
			//不同供应商，生成不同的单据
			for(String key : map.keySet()){
				List<PuprOrderd> dlist = map.get(key);
				double money = 0.0;
				puprOrder.setPk_puprorder(CodeHelper.createUUID());
				//订单编码
				puprOrder.setVbillno(getNextCGVbillno(new Date(),group));
				//默认值
				puprOrder.setDr(0);
				//供应商
				String delivercode = "";
				String delivername = "";
				for(PuprOrderd puprOrderd:dlist){
					if (puprOrderd.getNcnt() == 0) {
						continue;
					}
					puprOrderd.setPk_puprorder(puprOrder.getPk_puprorder());
					puprOrderd.setPk_puprorderd(CodeHelper.createUUID());
					puprOrderd.setTs(DateFormat.getTs());
					puprOrderd.setAcct(puprOrder.getAcct());
					if (puprOrderd.getNmoney() != 0.0) {
						puprOrderd.setSp_price(puprOrderd.getNmoney() / puprOrderd.getNcnt());
					} else {
						puprOrderd.setNmoney(puprOrderd.getNcnt() * puprOrderd.getSp_price());
					}
					if (puprOrderd.getPositnname() == null || puprOrderd.getPositnname() == "") {
						puprOrderd.setPositncode(puprOrder.getPositncode());
						puprOrderd.setPositnname(puprOrder.getPositnname());
					}
					money += puprOrderd.getNmoney();
					puprOrderd.setDr(0);
					delivercode = puprOrderd.getDelivercode();
					delivername = puprOrderd.getDelivername();
					puprOrderd.setDhopedate(puprOrder.getDhopedate());
					puprOrderMapper.savePuprOrderd(puprOrderd);
				}
				puprOrder.setNmoney(money);
				puprOrder.setDelivercode(delivercode);
				puprOrder.setDelivername(delivername);
				puprOrderMapper.savePuprOrderm(puprOrder);
			}
			
			//回写预订单状态
			PreOrder preOrder = new PreOrder();
			preOrder.setAcct(group);
			preOrder.setPk_preorders(puprOrder.getPk_preorders());
			preOrder.setDtype("2");
			preOrderService.updateDtype(preOrder);
			
			//返回值
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 校验单据号是否重复
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public PuprOrder findPuprorderByCode(PuprOrder puprOrder) throws CRUDException{
		try {
			return puprOrderMapper.findPuprorderByCode(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改订单
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public String updatePuprOrderm(PuprOrder puprOrder) throws CRUDException{
		try {
			puprOrder.setTs(DateFormat.getTs());
			String pk_puprorder = puprOrder.getPk_puprorder();
			PuprOrderd puprorderd = new PuprOrderd();
			puprorderd.setPk_puprorder(pk_puprorder);
			puprorderd.setAcct(puprOrder.getAcct());
			puprOrderMapper.deletePuprOrderd(puprorderd);
			double money = 0.0;
			for(PuprOrderd puprOrderd:puprOrder.getPuprOrderdList()){
				puprOrderd.setPk_puprorder(pk_puprorder);
				puprOrderd.setPk_puprorderd(CodeHelper.createUUID());
				puprOrderd.setTs(DateFormat.getTs());
				puprOrderd.setAcct(puprOrder.getAcct());
				puprOrderd.setDhopedate(puprOrder.getDhopedate());
				if (puprOrderd.getNmoney() != 0.0) {
					puprOrderd.setSp_price(puprOrderd.getNmoney() / puprOrderd.getNcnt());
				} else {
					puprOrderd.setNmoney(puprOrderd.getNcnt() * puprOrderd.getSp_price());
				}
				money += puprOrderd.getNmoney();
				puprOrderMapper.savePuprOrderd(puprOrderd);
			}
			puprOrder.setNmoney(money);
			puprOrder.setModifedtime(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDDHHMMSS));
			puprOrderMapper.updatePuprOrderm(puprOrder);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除订单
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void deletePuprorder(PuprOrder puprOrder) throws CRUDException{
		try {
			puprOrderMapper.deletePuprorder(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存订单评价信息
	 * @param puprOrder
	 */
	public void saveEvaluate(PuprOrder puprOrder)throws CRUDException{
		try {
			puprOrderMapper.saveEvaluate(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主键查询采购订单主表信息
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public PuprOrder toUpdatePuprorder(PuprOrder puprOrder) throws CRUDException{
		try {
			return puprOrderMapper.toUpdatePuprorder(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 生成验货单之后修改订单状态，更改为已验货
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void updatePuprOrdermStat(PuprOrder puprOrder)throws CRUDException{
		try {
			puprOrderMapper.updatePuprOrdermStat(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 查询助手中未完成的订单(不包括未提交、已完成、已取消订单)
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<PuprOrder> queryAllPuprOrdersForAutoItf(PuprOrder puprOrder) throws CRUDException{
		try {
			return puprOrderMapper.queryAllPuprOrdersForAutoItf(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 更新订单状态
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void updatePuprOrdersState(PuprOrder puprOrder) throws CRUDException{
		try {
			puprOrder.setPk_puprorder(CodeHelper.replaceCode(puprOrder.getPk_puprorder()));
			puprOrderMapper.updatePuprOrdersState(puprOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 检测选择的订单是不是当前登录用户新建的
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String checkOrderIsMine(PuprOrder puprOrder)  throws CRUDException{
		try {
			String str = "";
			puprOrder.setPk_puprorder(AsstUtils.StringCodeReplace(puprOrder.getPk_puprorder()));
			List<Map<String,Object>> listOrders= puprOrderMapper.checkOrderIsMine(puprOrder);
			if(!listOrders.isEmpty() && listOrders.size()>0){
				for(Map<String,Object> map : listOrders){
					str += ","+map.get("vbillno");
				}
				if(ValueCheck.IsNotEmpty(str)){
					str = str.substring(1);
				}
			}
			return str;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 根据采购订单生成验货单20161216
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void createInspectOrder(PuprOrder puprOrder,Map<String,String> map) throws CRUDException{
		try {
			//查询采购订单主表
			puprOrder.setPk_puprorder(puprOrder.getPk_puprorder().replaceAll("'", ""));
			puprOrder.setIstate(1);
			PuprOrder po = new PuprOrder();
			po.setPk_puprorder(puprOrder.getPk_puprorder());
			List<PuprOrder> puprOrders = this.queryAllPuprOrders(po,new Page());
			if(puprOrders!=null&&puprOrders.size()>0){
				//得到订单主表信息
				puprOrder = puprOrders.get(0);
				//账套
				String acct = map.get("acct");
				//采购订单子表
				PuprOrderd puprOrderd = new PuprOrderd();
				puprOrderd.setPk_puprorder(puprOrder.getPk_puprorder());
				puprOrderd.setAcct(acct);
				//查询采购订单从表
				List<PuprOrderd> list = queryAllPuprOrderdsByPK(puprOrderd);
				//拼装验货单数据主表
				Inspectm inspectm = new Inspectm();
				List<Inspectd> inspectdList = new ArrayList<Inspectd>();
				inspectm.setInspectdList(inspectdList);
				//验货单号为采购单号		
				inspectm.setVbillno(puprOrder.getVbillno());
				inspectm.setPk_puprorder(puprOrder.getPk_puprorder());
				inspectm.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
				inspectm.setCreator(map.get("creator"));
				inspectm.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				inspectm.setDelivercode(puprOrder.getDelivercode());
				inspectm.setDelivername(puprOrder.getDelivername());
				inspectm.setIstate(4);
				inspectm.setNmoney(puprOrder.getNmoney());
				inspectm.setVmemo(puprOrder.getVmemo());
				inspectm.setPositncode(puprOrder.getPositncode());
				inspectm.setVpuproedername(puprOrder.getPositnname());
				inspectm.setAcct(acct);
				//验货单从表
				for(int i=0,len=list.size();i<len;i++){
					PuprOrderd pd = list.get(i);
					Inspectd id = new Inspectd();
					id.setAcct(acct);
					id.setSp_code(pd.getSp_code());
					id.setSp_name(pd.getSp_name());
					id.setSp_desc(pd.getSp_desc());
					//id.setUnit3(pd.getUnit());
					id.setPk_unit(pd.getUnit());
					id.setVpuprorderbillno(pd.getVbillno());
					id.setNpurnum(pd.getNcnt());
					id.setNinsnum(pd.getNcnt());
					id.setNprice(pd.getSp_price());
					id.setNmoney(pd.getNmoney());
					id.setNdiffnum(0.0);
					id.setNdiffmoney(0.0);
					id.setQstate(2);
					id.setVinspectcnt(0.0);
					id.setVmemo(pd.getVmemo());
					id.setNcheckdiffrate(pd.getNcheckdiffrate());
					id.setPk_puprorder(pd.getPk_puprorder());
					id.setTax(pd.getTax());
					id.setTaxdes(pd.getTaxdes());
					id.setPk_inspection("");
					id.setVinspectname("");
					id.setPcno("");
					//放入集合
					inspectdList.add(id);
				}
				//调用方法保存
				String res = inspectService.saveInspect(inspectm);
				System.out.println(res);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
