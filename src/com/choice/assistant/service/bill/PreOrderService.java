package com.choice.assistant.service.bill;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.bill.PreOrder;
import com.choice.assistant.domain.bill.PreOrderd;
import com.choice.assistant.persistence.bill.PreOrderMapper;
import com.choice.framework.exception.CRUDException;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 预订单管理
 * @author Administrator
 *
 */
@Service
public class PreOrderService{
	
	@Autowired
	private PreOrderMapper preOrderMapper;
	@Autowired
	private PageManager<PreOrder> pageManager;
	
	private final transient Log log = LogFactory.getLog(PreOrderService.class);
	
	/**
	 * 查询所有的预订单信息
	 * @param preOrder
	 * @return
	 */
	public List<PreOrder> queryAllPreOrders(PreOrder preOrder,Page page) throws CRUDException{
		try {
			return pageManager.selectPage(preOrder, page, PreOrderMapper.class.getName()+".queryAllPreOrders");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询预订单子表信息
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<PreOrderd> queryAllPreOrderds(PreOrder preOrder) throws CRUDException{
		try {
			return preOrderMapper.queryAllPreOrderds(preOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询预订单子表信息，默认供应商税率等
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<PreOrderd> queryAllPreOrderdsDeliver(PreOrder preOrder) throws CRUDException{
		try {
			return preOrderMapper.queryAllPreOrderdsDeliver(preOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 得到下一个主表的序列的值
	 * @param xlm
	 * @return
	 * @throws CRUDException
	 */
	public int getValByXuLie(String xlm) throws CRUDException{
		try {
			return preOrderMapper.getValByXuLie(xlm);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存预订单
	 * @param preOrder
	 * @throws CRUDException
	 */
	public String savePreOrder(PreOrder preOrder) throws CRUDException{
		try {
			String acct = preOrder.getAcct();
			int pk_preorder = getValByXuLie("CSCM_PREORDER_M_SEQUENCE");
			preOrder.setPk_preorder(pk_preorder);
			preOrderMapper.savePreOrder(preOrder);
			List<PreOrderd> list = preOrder.getPreOrderds();
			for(int i=0,len=list.size();i<len;i++){
				PreOrderd pd = list.get(i);
				pd.setPk_preorder(pk_preorder);
				pd.setAcct(acct);
				preOrderMapper.savePreOrderd(pd);
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存预订单，只保存子表
	 * @param preOrder
	 * @throws CRUDException
	 */
	public String savePreOrder2(PreOrder preOrder) throws CRUDException{
		try {
			int pk_preorder = preOrder.getPk_preorder();
			String acct = preOrder.getAcct();
			//删除子表
			deletePreOrderds(preOrder);
			List<PreOrderd> list = preOrder.getPreOrderds();
			for(int i=0,len=list.size();i<len;i++){
				PreOrderd pd = list.get(i);
				pd.setPk_preorder(pk_preorder);
				pd.setAcct(acct);
				preOrderMapper.savePreOrderd(pd);
			}
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除主子表
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public void delete(PreOrder preOrder) throws CRUDException{
		try {
			preOrderMapper.deletePreOrderds(preOrder);
			preOrderMapper.deletePreOrder(preOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除子表
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public void deletePreOrderds(PreOrder preOrder) throws CRUDException{
		try {
			preOrderMapper.deletePreOrderds(preOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除主表
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public void deletePreOrder(PreOrder preOrder) throws CRUDException{
		try {
			preOrderMapper.deletePreOrder(preOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 更新状态
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public void updateDtype(PreOrder preOrder) throws CRUDException{
		try {
			preOrderMapper.updateDtype(preOrder);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
