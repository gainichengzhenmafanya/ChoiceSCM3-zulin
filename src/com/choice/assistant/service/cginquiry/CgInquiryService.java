package com.choice.assistant.service.cginquiry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.cginquiry.CgInquiry;
import com.choice.assistant.domain.system.Inquiry;
import com.choice.assistant.persistence.cginquiry.CgInquiryMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.ChartsXml;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.CreateChart;
import com.choice.framework.util.Local;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;


/**
 * 采购询价
 * @author xiaoguai
 *
 */
@Service
public class CgInquiryService {

	@Autowired
	private CgInquiryMapper cgInquiryMapper;
	@Autowired
	private PageManager<CgInquiry> pageManager;

	private final transient Log log = LogFactory.getLog(CgInquiryService.class);

	/**
	 * 查询询价列表
	 * @param cgInquiry
	 * @return
	 */
	public List<CgInquiry> tableCgInquiry(CgInquiry cgInquiry,Page page)throws CRUDException{
		try {
			return pageManager.selectPage(cgInquiry, page, CgInquiryMapper.class.getName()+".tableCgInquiry");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的询价类型
	 * @param inquiry
	 * @return
	 * @throws CRUDException
	 */
	public List<Inquiry> selectAllInquiryType(Inquiry inquiry)throws CRUDException{
		try {
			return cgInquiryMapper.selectAllInquiryType(inquiry);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加采购询价
	 * @param cgInquiry
	 * @return
	 * @throws Exception
	 */
	public void saveCgInquiry(CgInquiry cgInquiry)throws CRUDException{
		try {
			
			Inquiry inq = new Inquiry();
			inq.setPk_inquiry(cgInquiry.getPk_inquiry());
			inq.setAcct(cgInquiry.getAcct());
			Inquiry inquiry = cgInquiryMapper.selectAllInquiryType(inq).get(0);
			cgInquiry.setVinquirycode(inquiry.getVcode());
			cgInquiry.setVinquiryname(inquiry.getVname());
			cgInquiry.setDr(0);
			cgInquiry.setTs(DateFormat.getTs());
			cgInquiry.setPk_cginquiry(CodeHelper.createUUID());
			cgInquiryMapper.saveCgInquiry(cgInquiry);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 跳转到修改页面
	 * @param cgInquiry
	 * @return
	 * @throws CRUDException
	 */
	public CgInquiry toUpdateCginquiry(CgInquiry cgInquiry)throws CRUDException{
		try {
			return cgInquiryMapper.toUpdateCginquiry(cgInquiry);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加采购询价
	 * @param cgInquiry
	 * @return
	 * @throws Exception
	 */
	public void updateCgInquiry(CgInquiry cgInquiry)throws CRUDException{
		try {
			
			Inquiry inq = new Inquiry();
			inq.setPk_inquiry(cgInquiry.getPk_inquiry());
			inq.setAcct(cgInquiry.getAcct());
			Inquiry inquiry = cgInquiryMapper.selectAllInquiryType(inq).get(0);
			cgInquiry.setVinquirycode(inquiry.getVcode());
			cgInquiry.setVinquiryname(inquiry.getVname());
			cgInquiry.setDr(0);
			cgInquiry.setTs(DateFormat.getTs());
			cgInquiryMapper.updateCgInquiry(cgInquiry);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除询价
	 * @param cgInquiry
	 * @throws CRUDException
	 */
	public String deleteCginquiry(CgInquiry cgInquiry)throws CRUDException{
		try {
			cgInquiry.setDr(1);
			cgInquiry.setTs(DateFormat.getTs());
			cgInquiryMapper.deleteCginquiry(cgInquiry);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

    public ChartsXml findPriceAnalysisData(HttpServletResponse response, CgInquiry cgInquiry) throws CRUDException {
        try{
            List<Map<String,Object>> list=cgInquiryMapper.findPriceAnalysisData(cgInquiry);
            if(list!=null&&list.size()>0) {//生成折线图用的XML
                List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
                for (int i = 0; i < list.size(); i++) {
                    Map<String, Object> map = list.get(i);
                    Map<String, Object> dataMap = new HashMap<String, Object>();
                    dataMap.put("name", map.get("VINQUIRYNAME")+"-"+map.get("DELIVERNAME"));
                    dataMap.put("value", map.get("NINQUIRYPRICE"));
                    dataMap.put("hoverText", map.get("NINQUIRYPRICE") + Local.show(LanguagesConstants.YUAN));
                    dataList.add(dataMap);
                }
                return CreateChart.createHistogram(response, Local.show(LanguagesConstants.ANALYSIS_OF_MATERIALS)+list.get(0).get("SP_NAME")+"\t\t","", Local.show(LanguagesConstants.PRICE), dataList, null);
            }
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
        return null;
    }
}
