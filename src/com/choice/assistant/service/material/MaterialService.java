package com.choice.assistant.service.material;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONSerializer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.material.Material;
import com.choice.assistant.domain.material.MaterialMall;
import com.choice.assistant.domain.material.MaterialType;
import com.choice.assistant.domain.system.MaterialUnit;
import com.choice.assistant.domain.system.Tax;
import com.choice.assistant.domain.system.Unit;
import com.choice.assistant.persistence.material.MaterialMapper;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;

/**
 * 物资管理
 * @author wangchao
 *
 */
@Service
public class MaterialService {
	
	@Autowired
	private MaterialMapper materialMapper;
	@Autowired
	private PageManager<Material> pageManager;
	@Autowired
	private PageManager<Positn> positnPageManager;
    @Autowired
    private CoderuleService coderuleService;
    
    List<MaterialType> list = null;
	
	private final transient Log log = LogFactory.getLog(MaterialService.class);
	
	/**
	 * 查询所有的供应商类别
	 * @return
	 */
	public List<MaterialType> queryAllMaterialType(MaterialType materialType) throws CRUDException{
		try {
			return materialMapper.queryAllMaterialType(materialType);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的供应商类别
	 * @return
	 */
	public Map<Integer,List<MaterialType>> queryMaterialTypeByGrade(String acct) throws CRUDException{
		Map<Integer,List<MaterialType>> map = new HashMap<Integer, List<MaterialType>>();
		try {
			MaterialType materialType = new MaterialType();
			materialType.setAcct(acct);
			List<String> parentpks = new ArrayList<String>();
			parentpks.add("00000000000000000000000000000000");
			materialType.setParentpks(parentpks);
			List<MaterialType> materialTypeList = materialMapper.queryAllMaterialType(materialType);
			map.put(1, materialTypeList);//一级类别
			//----------------二级类别--------------------------------
			if(null != materialTypeList && materialTypeList.size()!=0){
				parentpks = new ArrayList<String>();
				for(MaterialType mt : materialTypeList){
					parentpks.add(mt.getPk_materialtype());
				}
				materialType.setParentpks(parentpks);
				materialTypeList = materialMapper.queryAllMaterialType(materialType);
				map.put(2, materialTypeList);//二级类别
			}
			//----------------三级类别--------------------------------
			if(null != materialTypeList && materialTypeList.size()!=0){
				parentpks = new ArrayList<String>();
				for(MaterialType mt : materialTypeList){
					parentpks.add(mt.getPk_materialtype());
				}
				materialType.setParentpks(parentpks);
				materialTypeList = materialMapper.queryAllMaterialType(materialType);
				map.put(3, materialTypeList);//三级类别
			}
			return map;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的供应商类别
	 * @return
	 */
	public List<MaterialType> queryMaterialType(String acct,String pk_accountid) throws CRUDException{
		List<MaterialType> resultlist = new ArrayList<MaterialType>();
		try {
			MaterialType materialType = new MaterialType();
			materialType.setAcct(acct);
			materialType.setPk_father("00000000000000000000000000000000");
			materialType.setPk_accountid(pk_accountid);
			List<MaterialType> materialTypeList = materialMapper.findMaterialType(materialType);
			if(null != materialTypeList && materialTypeList.size()!=0){
				for(MaterialType mt : materialTypeList){
					materialType.setPk_materialtype(null);
					materialType.setPk_father(mt.getPk_materialtype());
					List<MaterialType> childlist = materialMapper.findMaterialType(materialType);
					MaterialType parent = new MaterialType();
					parent = mt;
					parent.setChildMaterialtype(childlist);
					resultlist.add(parent);
				}
			}
			return resultlist;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询一个物资类别
	 * @return
	 * @throws CRUDException
	 */
	public MaterialType selectOneMaterialType(MaterialType materialType)throws CRUDException{
		try {
			return materialMapper.queryMaterialTypeByPK(materialType);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据物资类别主键穿物资类别信息
	 * @return
	 */
	public List<MaterialType> queryMaterialTypeById(MaterialType materialType) throws CRUDException{
		try {
			return materialMapper.queryMaterialTypeById(materialType);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增物资类别
	 * @param materialType
	 * @throws CRUDException
	 */
	public String saveMaterialType(MaterialType materialType) throws CRUDException{
		try {
			materialType.setPk_father(materialType.getPk_materialtype());
			materialType.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			materialType.setPk_materialtype(CodeHelper.createUUID());
			materialMapper.saveMaterialType(materialType);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 跳转到修改物资类别页面
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public MaterialType toUpdateMaterialType(MaterialType materialType) throws CRUDException{
		try {
			return materialMapper.toUpdateMaterialType(materialType);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取当前类别下面物资编码的最大值
	 * @param material
	 * @return
	 * @throws CRUDException    123456789
	 */
	public String selectMaxVcodeByType(Material material,String group)throws CRUDException{
		try {
			List<Material> listMaterial = materialMapper.selectMaxVcodeByType(material);
			if (listMaterial.size() < 4) {
				return coderuleService.getCode(CoderuleConstants.MATERIAL,group)+"0001";
			}
			String maxVcode = listMaterial.get(0).getVcode();
			String lastValue = (Integer.parseInt(maxVcode.substring(maxVcode.length()-4))+1)+"";
			String value = "";
			if (lastValue.length() == 1) {
				value += "000"+lastValue;
			} else if (lastValue.length() == 2) {
				value += "00"+lastValue;
			} else if (lastValue.length() == 3) {
				value += "0"+lastValue;
			} else if (lastValue.length() == 4) {
				value += lastValue;
			}
			return maxVcode.substring(0,maxVcode.length()-4) + value;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改物资类别
	 * @param materialType
	 * @throws CRUDException
	 */
	public String updateMaterialType(MaterialType materialType) throws CRUDException{
		try {
			materialType.setPk_father(materialType.getPk_materialtype());
			materialType.setTs(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			materialMapper.updateMaterialType(materialType);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 判断类别是否被引用，修改启用状态时用
	 * @param materialType
	 * @return
	 * @throws CRUDException
	 */
	public boolean findMaterialTypeBypk(MaterialType materialType)throws CRUDException{
		try {
			if (materialMapper.queryMaterialTypeBypk(materialType) > 0) {
				return false;
			} else if (materialMapper.queryMaterialByType(materialType) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除物资类别
	 * @param materialType
	 * @throws CRUDException
	 */
	public String deleteMaterialType(MaterialType materialType) throws CRUDException{
		try {
			materialMapper.deleteMaterialType(materialType);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检测类型名称是否相同
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public boolean ajaxIsSameCode(String vcode,String pk_materialtype,HttpSession session) throws CRUDException{
		try {
			MaterialType materialType = new MaterialType();
			materialType.setVcode(vcode);
			materialType.setPk_materialtype(pk_materialtype);
			materialType.setAcct(session.getAttribute("ChoiceAcct").toString());
			int cnt = materialMapper.ajaxIsSameCode(materialType) ;
			if (cnt> 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检测类型名称是否相同
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public boolean ajaxIsSameName(String vname,HttpSession session,String pk_materialtype) throws CRUDException{
		try {
			MaterialType materialType = new MaterialType();
			materialType.setVname(vname);
			materialType.setPk_materialtype(pk_materialtype);
			materialType.setAcct(session.getAttribute("ChoiceAcct").toString());
			if (materialMapper.ajaxIsSameName(materialType) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检测类型是否被引用
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public boolean findMaterialTypeByName(String pks,HttpSession session) throws CRUDException{
		try {
			List<String> ids = Arrays.asList(pks.split(","));
			for (int i = 0; i < ids.size(); i++) {
				MaterialType materialType = new MaterialType();
				materialType.setPk_materialtype(ids.get(i));
				materialType.setAcct(session.getAttribute("ChoiceAcct").toString());
				if (materialMapper.findMaterialTypeByName(materialType) > 0) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 检测类型是否被引用
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public boolean findMaterialTypeBytypepk(String pks,HttpSession session) throws CRUDException{
		try {
			List<String> ids = Arrays.asList(pks.split(","));
			for (int i = 0; i < ids.size(); i++) {
				MaterialType materialType = new MaterialType();
				materialType.setPk_materialtype(ids.get(i));
				materialType.setAcct(session.getAttribute("ChoiceAcct").toString());
				if (materialMapper.findMaterialTypeBytypepk(materialType) > 0) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 判断当前选中的物资类别是不是末级分类
	 * @param pk_materialtype
	 * @return
	 * @throws Exception
	 */
	public int checkMaterialType(String pk_materialtype,HttpSession session)throws CRUDException{
		try {
			MaterialType materialType = new MaterialType();
			materialType.setPk_materialtype(pk_materialtype);
			materialType.setAcct(session.getAttribute("ChoiceAcct").toString());
			return materialMapper.checkMaterialType(materialType);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有的物资单位
	 * @param unit
	 * @return
	 */
	public List<Unit> queryAllUnit(Unit unit)throws CRUDException{
		try {
			return materialMapper.queryAllUnit(unit);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有的税率值
	 * @param tax
	 * @return
	 * @throws CRUDException
	 */
	public List<Tax> queryAllTax(Tax tax)throws CRUDException{
		try {
			return materialMapper.queryAllTax(tax);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增时检索物资名称是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public boolean findMaterialByName(Material material)throws CRUDException{
		try {
			if(materialMapper.findMaterialByName(material) > 0){
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增时检索物资编码是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public boolean findMaterialByCode(Material material)throws CRUDException{
		try {
			if(materialMapper.findMaterialByCode(material) > 0){
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改时检索物资名称是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public boolean findMaterialByNameUp(Material material)throws CRUDException{
		try {
			if(materialMapper.findMaterialByNameUp(material) > 0){
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改时检索物资编码是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public boolean findMaterialByCodeUp(Material material)throws CRUDException{
		try {
			if(materialMapper.findMaterialByCodeUp(material) > 0){
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	
	/**
	 * 根据物资查询采购组织
	 * @param material
	 * @return
	 */
	public List<Positn> findPositnBySupply(Material material){
		return materialMapper.findPositnBySupplyCG(material);
	}
	
	/**
	 * 校验物资是否被引用
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public boolean checkMaterial(Material material) throws CRUDException{
		try {
			if (materialMapper.checkMaterial(material) > 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 添加单位转换率
	 * @param materialUnitList
	 * @throws CRUDException
	 */
	public void saveMaterialUnit(List<MaterialUnit> materialUnitList)throws CRUDException{
		try {
			
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除单位转换率
	 * @param material
	 * @throws CRUDException
	 */
	public void deleteMaterialUnit(Material material)throws CRUDException{
		try {
			materialMapper.deleteMaterialUnit(material);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	

	/**
	 * 跳转到关联商城物资页面
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public Material toMallMaterial(Material material)throws CRUDException{
		try {
			return materialMapper.queryMaterialById(material);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialMall> toDeleteMallMaterial(Material material)throws CRUDException{
		try {
			return materialMapper.toDeleteMallMaterial(material);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据物资主键查询单位转换率
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialUnit> toUpdateMaterialUnit(Material material)throws CRUDException{
		try {
			return materialMapper.toUpdateMaterialUnit(material);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据转换率查询最小采购量
	 * @param material
	 * @return
	 */
	public double findNmincntByRate(Material material) throws CRUDException{
		try{
			return materialMapper.findNmincntByRate(material);
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有的类别的子类别
	 * @param materialType
	 * @param accountMaterialType 
	 * @return
	 * @throws CRUDException
	 */
	public String queryMaterialType(MaterialType materialType, String accountMaterialType)throws CRUDException{
		try {
			List<MaterialType> listMaterialType = materialMapper.queryMaterialType(materialType);
			list = new ArrayList<MaterialType>();
			backFunction(listMaterialType,materialType);
			String str = "";
			for (int i = 0; i < list.size(); i++) {
				if (str.indexOf(list.get(i).getPk_materialtype()) > 0) {
					continue;
				}
				//如果在授权类别中加到返回参数中
				if(ValueCheck.IsEmpty(accountMaterialType)){
					str += ",'"+list.get(i).getPk_materialtype()+"'";
				}else{
					String[] apks = accountMaterialType.replace("'", "").split(",");
					String[] mpks = list.get(i).getPk_materialtype().split(",");
					for(String apk:apks){
						for(String mpk:mpks){
							if(apk.equals(mpk)){
								str += ",'"+apk+"'";
							}
						}
					}
				}
			}
			return str.substring(1);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 回掉函数
	 * @param listMaterialType 所有的物资类别集合
	 * @param materialType 当前选中的类别实体
	 * @return
	 */
	public void backFunction(List<MaterialType> listMaterialType,MaterialType materialType){
		for (int i = 0; i < listMaterialType.size(); i++) {
			//如果当前选中的类别下面还有别的类别，就调用本身循环
			if (listMaterialType.get(i).getPk_father().equals(materialType.getPk_materialtype())) {
				backFunction(listMaterialType,listMaterialType.get(i));
				list.add(listMaterialType.get(i));
			}
		}
		list.add(materialType);
	}
	
	/**
	 * 查询物资单位转换率
	 * @param materialUnit
	 * @return
	 * @throws CRUDException
	 */
	public MaterialUnit selectMaterialUnit(MaterialUnit materialUnit)throws CRUDException{
		try {
			return materialMapper.selectMaterialUnit(materialUnit);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据物资类别主键穿物资类别信息
	 * @return
	 */
	public MaterialType queryMaterialTypeByPK(MaterialType materialType) throws CRUDException{
		try {
			return materialMapper.queryMaterialTypeByPK(materialType);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有的物资列表
	 * @return
	 * @throws CRUDException
	 */
	public List<Material> queryMaterial(Material material,Page page) throws CRUDException{
		try {
			return pageManager.selectPage(material, page, MaterialMapper.class.getName()+".queryAllMaterial");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 获取所有的物资列表
	 * @return
	 * @throws CRUDException
	 */
	public List<Material> queryAllMaterial(Material material) throws CRUDException{
		try {
			List<Material> materiallist = materialMapper.queryAllMaterial(material);
			return materiallist;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}

	
	/**
	 * 删除物资信息
	 * @param ids
	 */
	public String deleteMaterial(List<String> ids)throws CRUDException{
		try {
			materialMapper.deleteMaterial(ids);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	
	/**
	 * 根据输入的条件查询 前n条记录
	 * @param material
	 * @return
	 * @throws Exception
	 */
	public List<Material> findMaterialTop(Material material)throws CRUDException{
		try {
			return materialMapper.findMaterialTop(material);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询合约报价
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public Material findMaterialNprice(Material material)throws CRUDException{
		try {
			material.setDdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			Material result = materialMapper.findMaterialNprice(material);
			if ( result == null) {
				return new Material();
			}
			return result;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 查询供应物资范围报价
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public Material findMaterialNpricesupplier(Material material)throws CRUDException{
		try {
			material.setDdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			Material result = materialMapper.findMaterialNpricesupplier(material);
			if ( result == null) {
				return new Material();
			}
			return result;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}	
	
	/**
	 * 将文件上传到temp文件夹下
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public String upload(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String realFilePath = "";
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("file");
			String realFileName = file.getOriginalFilename();
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "temp\\";
			String fileuploadPath = ctxPath;
			File dirPath = new File(fileuploadPath);
			if (!dirPath.exists()) {
				dirPath.mkdir();
			}
			realFilePath = fileuploadPath + realFileName;
			File uploadFile = new File(realFilePath);
			FileCopyUtils.copy(file.getBytes(), uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return realFilePath;
	}	
	
	
	/**
	 * 返回导入的错误结果信息
	 */
	public List<String> listError(Map<String, String> map) {
		List<String> listError = new ArrayList<String>();
		if (map.size() != 0) {
			for (String key : map.keySet()) {
				listError.add(key);
				listError.add(map.get(key));
			}
		}
		return listError;
	}
	/**
	 * 读取操作文档详细列表
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public List<Map<String,Object>> showHelpFiles(HttpServletResponse response,HttpServletRequest request) throws IOException {
		String filePath = request.getSession().getServletContext().getRealPath("/")+ "\\" + "template\\";
		File f = new File(filePath);// 建立当前目录中文件的File对象
		File[] files = f.listFiles();// 取得目录中所有文件的File对象数组
		DecimalFormat df = new DecimalFormat("#.00"); 
		List<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		if(null != files){
			String len="";
			for(File file : files) {
				Map<String,Object> map = new HashMap<String,Object>();
				// 目录下的文件：
				if(file.isFile()) {
					//计算文件大小
					if(file.length()<1024){
						len = file.length()+"B"; 
					}else if(file.length() < 1048576){
						len = df.format((double)file.length()/1024)+"KB";
					}else if(file.length() < 1073741824){
						len = df.format((double)file.length()/1024/1024)+"MB";
					}else{
						len = df.format((double)file.length()/1024/1024/1024)+"GB";	
					}
					map.put("name",file.getName());
					map.put("len",len);
				}else if (file.isDirectory()) { 	// 目录下的目录：
				}
				fileList.add(map);
			}
		}
		return fileList;
	}
	
	/**
	 * 下载模板信息
	 * 
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	public void downloadTemplate(HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		OutputStream outp = null;
		FileInputStream in = null;
		try {
			String fileName = "supply.xls";
			String ctxPath = request.getSession().getServletContext()
					.getRealPath("/")
					+ "\\" + "template\\";
			String filedownload = ctxPath + fileName;
			fileName = URLEncoder.encode(fileName, "UTF-8");
			// 要下载的模板所在的绝对路径
			response.reset();
			response.addHeader("Content-Dispositn", "attachment; filename="
					+ fileName);
			response.setContentType("application/octet-stream;charset=UTF-8");
			outp = response.getOutputStream();
			in = new FileInputStream(filedownload);
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
			if (outp != null) {
				outp.close();
				outp = null;
			}
		}
	}
	
	/**
	 * 判断倒入物资是否存在
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public boolean isMaterialCodeRepeated(Material material) throws CRUDException{
		try {
			return materialMapper.MaterialCodeCount(material)<1;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	/**
	 * 跳转到物资类别-授权
	 * @param materialType
	 * @return
	 * @author ZGL
	 */
	public Object queryMaterialTypeSqTree(MaterialType materialType, String pk_check) throws CRUDException {
		if(materialType.getAcct()=="" || "".equals(materialType.getAcct())){
			return null;
		}
		List<MaterialType> materialTypeList = materialMapper.queryAllMaterialType(materialType);
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		if(materialTypeList != null && materialTypeList.size()>0){
			for(MaterialType materialTypes : materialTypeList){
				Map<String,Object> map = new HashMap<String,Object>();
				if(materialTypes.getParentMaterialtype().getPk_materialtype() == "" 
						|| "".equals(materialTypes.getParentMaterialtype().getPk_materialtype())
						||"00000000000000000000000000000000".equals(materialTypes.getParentMaterialtype().getPk_materialtype())){
					map.put("pid","000");
				}else{
					map.put("pid",materialTypes.getParentMaterialtype().getPk_materialtype());
				}
				if(pk_check!=null && pk_check != "" && !"".equals(pk_check)){
					pk_check = AsstUtils.StringCodeReplace(pk_check);
					if(pk_check.indexOf("'"+materialTypes.getPk_materialtype()+"'") >=0){
						map.put("checked","true");
					}else{
						map.put("checked","false");
					}
				}
				if(materialTypes.getChildMaterialtype().size() == 0 || materialTypes.getChildMaterialtype() == null){
					map.put("hasChildren","");
				}else{
					map.put("hasChildren","true");
				}
				map.put("id", materialTypes.getPk_materialtype());
				map.put("VCODE", materialTypes.getVcode());
				map.put("text", materialTypes.getVcode()+"-"+materialTypes.getVname());
				list.add(map);
			}
		}
		// 创建树的顶级节点
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", "000");
		map.put("text", "全部");
		map.put("hasChildren", "ture");
		list.add(0,map);
		// 转换为JSON字符串
		JSONSerializer.toJSON(list);
		return JSONSerializer.toJSON(list).toString();
	}


	/**
	 * 查询组织结构类型为门店的数据列表
	 * @param department
	 * @param type 
	 * @return
	 * @throws CRUDException
	 */
	public List<Positn> queryFirm(Positn positn, Page page) throws CRUDException{
		try{
			return positnPageManager.selectPage(positn, page, MaterialMapper.class.getName()+".findPositnSuper");
		}catch(Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据物资返回供应商
	 * @param material
	 * @return
	 */
	public List<Deliver> findDeliverByMaterial(Material material){
		return materialMapper.findDeliverByMaterial(material);
	}
	
	/**
	 * 根据ID查询物资
	 * @param pk_material
	 * @param pk_group
	 * @return
	 * @throws CRUDException
	 */
	public Material queryMaterialById(String pk_material,String acct)throws CRUDException{
		try {
			Material material = new Material();
			material.setPk_material(pk_material);
			material.setAcct(acct);
			return materialMapper.queryMaterialById(material);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
