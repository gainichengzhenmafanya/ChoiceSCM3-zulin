package com.choice.assistant.service.supplier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.common.CommonAssMethod;
import com.choice.assistant.domain.material.Material;
import com.choice.assistant.domain.material.MaterialMall;
import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.domain.supplier.SupplierBank;
import com.choice.assistant.domain.supplier.SupplierOrg;
import com.choice.assistant.domain.supplier.SupplierType;
import com.choice.assistant.persistence.common.CommonAssMapper;
import com.choice.assistant.persistence.material.MaterialMapper;
import com.choice.assistant.persistence.supplier.SupplierMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.Local;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 供应商管理
 *
 * @author wangchao
 */
@Service
public class SupplierService {

    @Autowired
    private SupplierMapper supplierMapper;
    @Autowired
    private PageManager<Supplier> pageManager;
    @Autowired
    private CommonAssMapper commonMapper;
	@Autowired
	private MaterialMapper materialMapper;

    private final transient Log log = LogFactory.getLog(SupplierService.class);

    private List<SupplierType> newtypes=null;
    /**
     * 查询所有的供应商类别
     *
     * @return
     */
    public List<SupplierType> queryAllSupplierType(SupplierType supplierType) throws CRUDException {
        try {
            if("2".equals(supplierType.getEnablestate())) {
                supplierType.setEnablestate(null);
                List<SupplierType> types=supplierMapper.queryAllSupplierType(supplierType);
                newtypes = new ArrayList<SupplierType>();
                newtypes.addAll(types);
                for (SupplierType type : types) {//递归筛选未启用的和他下面所有节点
                    if (type.getEnablestate() != 2) {
                        remType(types, type);
                    }
                }
                return newtypes;
            }else{
                List<SupplierType> list=supplierMapper.queryAllSupplierType(supplierType);
                return list;
            }
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 编码结构
     * @param supplierType
     * @return
     * @throws CRUDException
     */
    public List<SupplierType> queryLastSupplierType(SupplierType supplierType) throws CRUDException{
        try {
            supplierType.setEnablestate(null);
            List<SupplierType> types=supplierMapper.queryAllSupplierType(supplierType);
            newtypes = new ArrayList<SupplierType>();
            newtypes.addAll(types);
            for (SupplierType type : types) {//递归筛选未启用的和他下面所有节点
                if (type.getEnablestate() != 2) {
                    remType(types, type);
                }
            }
            for(SupplierType type:types){//递归筛选最后一级类别
                typeLast(types,type);
            }
            return newtypes;
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 筛选已停用的分类
     * @param list
     * @param supplierType
     */
    public void remType(List<SupplierType> list,SupplierType supplierType){
        for(SupplierType type:list) {
            if (supplierType.getPk_suppliertype().equals(type.getPk_father())) {
                remType(list,type);
                newtypes.remove(type);
            }
        }
        newtypes.remove(supplierType);
    }
    /**
     * 筛选已停用的分类
     * @param list
     * @param supplierType
     */
    public void typeLast(List<SupplierType> list,SupplierType supplierType){
        for(SupplierType type:list) {
            if (supplierType.getPk_father().equals(type.getPk_suppliertype())) {
                typeLast(list,type);
                newtypes.remove(type);
            }
        }
    }
    /**
     * 判定供应商是否呗引用
     * @param pk
     * @return 如果被引用 返回 false
     */
    public boolean supplierCheckQuote(String pk,String group){
        CommonAssMethod commonMethod=new CommonAssMethod();
        String[] tableNames={"cscm_purcontract_m","cscm_puprorder_m","cscm_Inspect_m","cscm_otherpayables_m","cscm_Backbill_m"};
        for(String tableName:tableNames) {
            commonMethod.setAcct(group);
            commonMethod.setVcode(pk);
            commonMethod.setVcodename("delivercode");
            commonMethod.setVtablename(tableName);
            int reulse= Integer.parseInt(commonMapper.checkQuote(commonMethod));
            if(reulse>0){
                return false;
            }
        }
        return true;
    }

    /**
     * 根据pk获取数据
     *
     * @param supplier
     * @return
     * @throws CRUDException
     */
    public Supplier findSupplierByPk(Supplier supplier) throws CRUDException {
        try {
            return supplierMapper.findSupplicerByPk(supplier);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 根据pk获取数据List
     *
     * @param supplier
     * @return
     * @throws CRUDException
     */
    public List<Supplier> querySupplierListByPk(Supplier supplier) throws CRUDException {
        try {
            return supplierMapper.querySupplierListByPk(supplier);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 添加供应商
     * @param supplier
     * @param group
     * @param pk
     * @return
     * @throws CRUDException
     */
    public String addSupplier(Supplier supplier, String group, String pk) throws CRUDException {
        try {
            supplier.setAcct(group);
            if(supplierMapper.findSupplicerByCode(supplier)!=null){
                return Local.show(LanguagesConstants.CODE_ERROR);
            }
            if (supplier.getMaterialScope() != null && supplier.getMaterialScope().size() > 0) {//供应物资范围是否为空
                for (MaterialScope scope : supplier.getMaterialScope()) {
                    scope.setAcct(group);
                    scope.setPk_materialscope(CodeHelper.createUUID());
                    scope.setDelivercode(pk);
                }
                supplierMapper.addMaterialScope(supplier.getMaterialScope());
            }
            if (supplier.getListMaterialMall() != null && supplier.getListMaterialMall().size() > 0) {
				for (int i = 0; i < supplier.getListMaterialMall().size(); i++) {
					if (supplier.getListMaterialMall().get(i).getVhspec() != null && 
							!"".equals(supplier.getListMaterialMall().get(i).getVhspec()) && 
							supplier.getPk_supplierjum() != null && !"".equals(supplier.getPk_supplierjum())) {
						MaterialMall materialMall = new MaterialMall();
						materialMall = supplier.getListMaterialMall().get(i);
						materialMall.setTs(DateFormat.getTs());
                        materialMall.setDr(0);
						materialMall.setAcct(supplier.getAcct());
                        materialMall.setPk_materialmall(CodeHelper.createUUID());
						materialMapper.mallMaterial(materialMall);
					}
				}
			}
            if (supplier.getSupplierBank() != null && supplier.getSupplierBank().size() > 0) {//供应商银行信息是否为空
                for (SupplierBank bank : supplier.getSupplierBank()) {
                    bank.setAcct(group);
                    bank.setPk_supplierbank(CodeHelper.createUUID());
                    bank.setPk_supplier(pk);
                }
                supplierMapper.addSupplierBank(supplier.getSupplierBank());
            }
            if (supplier.getSupplierOrg() != null && supplier.getSupplierOrg().size() > 0) {//适用门店是否为空
                for (SupplierOrg org : supplier.getSupplierOrg()) {
                    org.setAcct(group);
                    org.setPk_supplierOrg(CodeHelper.createUUID());
                    org.setPk_supplier(pk);
                }
                supplierMapper.addSupplierOrg(supplier.getSupplierOrg());
            }
            supplier.setPk_supplier(pk);
            supplier.setTs(DateFormat.getTs());
            supplierMapper.addSupplier(supplier);
            return "ok";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 获取所有的供应商列
     *
     * @return
     * @throws CRUDException
     */
    public List<Supplier> querySupplier(Supplier supplier, Page page) throws CRUDException {
        try {
        	supplier.setSuppliertype(AsstUtils.StringCodeReplace(supplier.getSuppliertype()));
            return pageManager.selectPage(supplier, page, SupplierMapper.class.getName() + ".queryAllSupplier");
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    
    /**
     * 获取所有的供应商(不分页)
     *
     * @return
     * @throws CRUDException
     */
    public List<Supplier> queryAllSupplier(Supplier supplier) throws CRUDException {
        try {
            return supplierMapper.queryAllSupplier(supplier);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 供应商状态修改
     *
     * @throws CRUDException
     */
    public void updateEnablestate(String enablestate, String ids) throws CRUDException {
        try {
            List<String> list = Arrays.asList(ids.split(","));
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("enablestate", enablestate);
            map.put("list", list);
            supplierMapper.updateEnablestate(map);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 假-删除供应商信息
     *
     * @param ids
     * @throws CRUDException
     */
    public String deleteSupplier(String ids,String group) throws CRUDException {
        try {
            List<String> idlist = Arrays.asList(ids.split(","));
            for(String id:idlist){
                if(!supplierCheckQuote(id,group)){//判定供应商是否呗引用
                    return "error";
                }
            }
            supplierMapper.fDeleteSupplier(idlist);
            return "ok";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 修改供应商信息
     *
     * @param supplier
     * @throws CRUDException
     */
    public String updateSupplier(Supplier supplier, String group) throws CRUDException {
        try {
            supplier.setAcct(group);
            Supplier haveCode=supplierMapper.findSupplicerByCode(supplier);
            if(haveCode!=null&&!haveCode.getPk_supplier().equals(supplier.getPk_supplier())){
                return Local.show(LanguagesConstants.CODE_ERROR);
            }
            if (supplier.getMaterialScope() != null && supplier.getMaterialScope().size() > 0) {//供应物资范围不为空
                for (MaterialScope scope : supplier.getMaterialScope()) {
                    scope.setAcct(group);
                    scope.setPk_materialscope(CodeHelper.createUUID());
                    scope.setDelivercode(supplier.getPk_supplier());
                }
                supplierMapper.deleteMatlBySupplerPk(supplier);
                supplierMapper.addMaterialScope(supplier.getMaterialScope());
            }else{
                supplierMapper.deleteMatlBySupplerPk(supplier);
            }
            Supplier sup = new Supplier();
            //查询之前的关联商城供应商
            sup = supplierMapper.findSupplicerByPk(supplier);
            if (supplier.getListMaterialMall() != null && supplier.getListMaterialMall().size() > 0) {
				for (int i = 0; i < supplier.getListMaterialMall().size(); i++) {
					if (supplier.getListMaterialMall().get(i).getVhspec() != null && 
							!"".equals(supplier.getListMaterialMall().get(i).getVhspec()) && 
							supplier.getPk_supplierjum() != null && !"".equals(supplier.getPk_supplierjum())) {
						MaterialMall materialMall = new MaterialMall();
						materialMall = supplier.getListMaterialMall().get(i);
						materialMall.setTs(DateFormat.getTs());
						materialMall.setDr(0);
						materialMall.setAcct(supplier.getAcct());
                        materialMall.setPk_materialmall(CodeHelper.createUUID());
						materialMapper.mallMaterial(materialMall);
					}
				}
			}
            if (supplier.getSupplierBank() != null && supplier.getSupplierBank().size() > 0) {//供应商银行信息是否为空
                for (SupplierBank bank : supplier.getSupplierBank()) {
                    bank.setAcct(group);
                    bank.setPk_supplierbank(CodeHelper.createUUID());
                    bank.setPk_supplier(supplier.getPk_supplier());
                }
                supplierMapper.deleteBankBySupplerPk(supplier);
                supplierMapper.addSupplierBank(supplier.getSupplierBank());
            }else{
                supplierMapper.deleteBankBySupplerPk(supplier);
            }
            if (supplier.getSupplierOrg() != null && supplier.getSupplierOrg().size() > 0) {//适用门店是否为空
                for (SupplierOrg org : supplier.getSupplierOrg()) {
                    org.setAcct(group);
                    org.setPk_supplierOrg(CodeHelper.createUUID());
                    org.setPk_supplier(supplier.getPk_supplier());
                }
                supplierMapper.deleteOrgBySupplerPk(supplier);
                supplierMapper.addSupplierOrg(supplier.getSupplierOrg());
            }else{
                supplierMapper.deleteOrgBySupplerPk(supplier);
            }
            supplier.setTs(DateFormat.getTs());
            supplierMapper.updateSupplier(supplier);
            return "ok";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 根据pk获取供应商类别
     * @param supplierType
     * @return
     * @throws CRUDException
     */
    public SupplierType findSupplierTypeByPk(SupplierType supplierType) throws CRUDException {
        try {
            return supplierMapper.findSupplierTypeByPk(supplierType);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 添加供应商类别
     * @param supplierType
     * @throws CRUDException
     */
    public String addSupplierType(SupplierType supplierType,String group) throws CRUDException {
        try {
            supplierType.setAcct(group);
            supplierType.setPk_suppliertype(CodeHelper.createUUID());
            if(supplierMapper.findSupplierTypeByCode(supplierType)!=null){
                return Local.show(LanguagesConstants.CODE_ERROR);
            }
            supplierMapper.addSupplierType(supplierType);
            return "ok";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 修改供应商类别
     * @param supplierType
     * @throws CRUDException
     */
    public void updateSupplierType(SupplierType supplierType,String group) throws CRUDException {
        try {
            supplierType.setAcct(group);
            supplierMapper.updateSupplierType(supplierType);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    List<String> delType=null;
    /**
     * 删除供应商类别分类
     * @return
     * @throws CRUDException
     */
    public String delSupplierType(String ids) throws CRUDException {
        try {
            List<SupplierType> types=supplierMapper.queryAllSupplierType(null);
            List<String> list=Arrays.asList(ids.split(","));
            if(list!=null&&list.size()>0) {
                int count=0;
                delType = new ArrayList<String>();
                for(String id:list) {//筛选下级子节点 进行清理
                    delType.add(id);
                    subType(types, id);
                    count+=supplierMapper.haveSupplierType(delType);
                }
                if (count == 0) {
                    supplierMapper.delSupplierTypeDr(delType);
                } else {
                    return Local.show(LanguagesConstants.TREE_ERROR);
                }
            }
            return "ok";
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 筛选下级分类
     * @param list
     */
    public void subType(List<SupplierType> list,String pk){
        for(SupplierType type:list) {
            if (pk.equals(type.getPk_father())) {
                subType(list, type.getPk_suppliertype());
                delType.add(type.getPk_suppliertype());
            }
        }
    }
	/**
	 * 根据输入的条件查询 前n条记录
	 * @return
	 * @throws Exception
	 */
	public List<Supplier> findSupplierTop(Supplier supplier)throws CRUDException {
		try {
			return supplierMapper.findSupplierTop(supplier);
		} catch (Exception e) {
			 log.error(e);
	         throw new CRUDException(e);
		}
	}

	/**
	 * 根据物资查询共应该物资的供应商
	 * @param material
	 * @return
	 */
	public List<Supplier> findSupplierByMaterialTop(Material material)throws CRUDException {
		try {
			return supplierMapper.findSupplierByMaterialTop(material);
		} catch (Exception e) {
			 log.error(e);
	         throw new CRUDException(e);
		}
	}
	
    /**
     * 批量获取是否被引用
     * @param ids
     * @return
     */
    public int haveSupplierType(String ids) throws CRUDException {
        try {
            List<String> list = Arrays.asList(ids.split(","));
            return supplierMapper.haveSupplierType(list);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 修改商城供应商
     * @param supplier
     */
    public void updateJmu(Supplier supplier) throws CRUDException {
        try{
        	//查询更新之前的关联商城物资
        	Supplier sup = new Supplier();
        	sup = supplierMapper.findSupplicerByPk(supplier);
        	//如果之前关联的商城供应商与本次选择商城供应商的一样就不更新物资关联关系了
        	supplierMapper.updateJmu(supplier);
        }catch (Exception e){
            log.error(e);
            throw  new CRUDException(e);
        }
    }

    /**
     * 获取供应物资
     * @param supplier
     * @return
     * @throws CRUDException
     */
    public List<MaterialScope> getMaterial(Supplier supplier) throws CRUDException {
        try {
            return supplierMapper.getMaterial(supplier);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 根据code or pk 获取数据
     * @param supplier
     * @return
     * @throws CRUDException
     */
    public Supplier findSupplicerByCode(Supplier supplier) throws CRUDException {
        try {
            return supplierMapper.findSupplicerByCode(supplier);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 修改供应商供应物资
     * @param supplier
     * @return
     */
    public void updateMaterial(Supplier supplier) throws CRUDException {
        try {
            String pk_material = "";
            if (supplier.getMaterialScope() != null && supplier.getMaterialScope().size() > 0) {//供应物资范围是否为空
                for (MaterialScope scope : supplier.getMaterialScope()) {
                    scope.setAcct(supplier.getAcct());
                    scope.setPk_materialscope(CodeHelper.createUUID());
                    scope.setDelivercode(supplier.getPk_supplier());
                }
                supplierMapper.deleteMatlBySupplerPk(supplier);
                supplierMapper.addMaterialScope(supplier.getMaterialScope());
            } else {
                supplierMapper.deleteMatlBySupplerPk(supplier);
            }
            if(ValueCheck.IsNotEmpty(supplier.getMaterialScope())&&!supplier.getMaterialScope().isEmpty() && supplier.getMaterialScope().size()>0){
            	for(MaterialScope materialScope : supplier.getMaterialScope()){
            		pk_material += ","+materialScope.getPk_material();
            	}
            	supplier.setPk_material(AsstUtils.StringCodeReplace(pk_material.substring(1)));
            }
            if (supplier.getListMaterialMall() != null && supplier.getListMaterialMall().size() > 0) {
                for (int i = 0; i < supplier.getListMaterialMall().size(); i++) {
                    if (supplier.getListMaterialMall().get(i).getVhspec() != null &&
                            !"".equals(supplier.getListMaterialMall().get(i).getVhspec()) &&
                            supplier.getPk_supplierjum() != null && !"".equals(supplier.getPk_supplierjum())) {
                        MaterialMall materialMall = new MaterialMall();
                        materialMall = supplier.getListMaterialMall().get(i);
                        materialMall.setTs(DateFormat.getTs());
                        materialMall.setDr(0);
                        materialMall.setAcct(supplier.getAcct());
                        materialMall.setPk_materialmall(CodeHelper.createUUID());
                        materialMapper.mallMaterial(materialMall);
                    }
                }
            }
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 检测供应商类别引用状态
     * @param supplierType
     * @return
     * @throws CRUDException
     */
	public String checkSupplierTypeQuote(SupplierType supplierType) throws CRUDException {
        try {
        	int res = 0;
        	int cnt = 0;
        	Map<String,Object> map = new HashMap<String,Object>();
        	map.put("pk_id", supplierType.getPk_suppliertype());
        	map.put("acct", supplierType.getAcct());
        	String tables = "cscm_suppliertype,pk_father;cscm_supplier,delivercodetype";
        	for(String table : tables.split(";")){
        		map.put("tableName", table.split(",")[0]);
        		map.put("columnName", table.split(",")[1]);
        		cnt = supplierMapper.checkDataQuote(map);
        		if(ValueCheck.IsNotEmpty(cnt)){
        			res += cnt;
        		}else{
        			res += 0;
        		}
        	}
        	if(res>0){
        		return "true";
        	}else{
        		return "false";
        	}
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
	}
	/**
	 * 检测供应商类别引用状态
	 * @param supplierType
	 * @return
	 * @throws CRUDException
	 */
	public String checkSupplierQuote(Supplier supplier) throws CRUDException {
		String result = "false";
		try {
			int res = 0;
			int cnt = 0;
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("pk_id", supplier.getPk_supplier());
        	map.put("acct", supplier.getAcct());
//        	cscm_materialscope,delivercode;cscm_supplierorg,delivercode;cscm_supplierbank,delivercode;
			String tables = "cscm_puprorder_m,delivercode;cscm_backbill_m,delivercode;cscm_inspect_m,delivercode;" +
					"cscm_payment_m,delivercode;cscm_otherpayables_m,delivercode;cscm_cginquiry,delivercode";
			for(String table : tables.split(";")){
				map.put("tableName", table.split(",")[0]);
				map.put("columnName", table.split(",")[1]);
				cnt = supplierMapper.checkDataQuote(map);
				if(ValueCheck.IsNotEmpty(cnt)){
					res += cnt;
				}else{
					res += 0;
				}
				if(res>0){
					result = "true";
					break;
				}else{
					result = "false";
				}
			}
		}catch (Exception e){
			log.error(e);
			throw new CRUDException(e);
		}
		return result;
	}
        

}
