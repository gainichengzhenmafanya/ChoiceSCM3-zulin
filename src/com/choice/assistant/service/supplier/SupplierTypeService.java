package com.choice.assistant.service.supplier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONSerializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.supplier.SupplierType;
import com.choice.assistant.persistence.supplier.SupplierTypeMapper;
import com.choice.assistant.util.AsstUtils;
import com.choice.framework.exception.CRUDException;
/**
 * 供应商类别
 * @author ZGL
 *
 */
@Service
public class SupplierTypeService {
    @Autowired
    private SupplierTypeMapper supplierTypeMapper;
	/**
	 * 跳转到供应商类别选择界面
	 * @param supplierType
	 * @param pk_check
	 * @return
	 * @throws CRUDException
	 */
	public Object chooseSupplierTypeTree(SupplierType supplierType,String pk_check) throws CRUDException {
				if(supplierType.getAcct()=="" || "".equals(supplierType.getAcct())){
					return null;
				}
				List<SupplierType> supplierTypeList = supplierTypeMapper.queryAllSupplierTypeTree(supplierType);
				List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
				if(supplierTypeList != null && supplierTypeList.size()>0){
					for(SupplierType supplierTypes : supplierTypeList){
						Map<String,Object> map = new HashMap<String,Object>();
						if(supplierTypes.getParentSupplierType().getPk_suppliertype()== ""  
								|| "".equals(supplierTypes.getParentSupplierType().getPk_suppliertype())
								||"00000000000000000000000000000000".equals(supplierTypes.getParentSupplierType().getPk_suppliertype())){
							map.put("pid","000");
						}else{
							map.put("pid",supplierTypes.getParentSupplierType().getPk_suppliertype());
						}
						if(pk_check!=null && pk_check != "" && !"".equals(pk_check)){
							pk_check = AsstUtils.StringCodeReplace(pk_check);
							if(pk_check.indexOf("'"+supplierTypes.getPk_suppliertype()+"'") >=0){
								map.put("checked","true");
							}else{
								map.put("checked","false");
							}
						}
						if(supplierTypes.getChildSupplierType().size()== 0 || supplierTypes.getChildSupplierType() == null){
							map.put("hasChildren","");
						}else{
							map.put("hasChildren","true");
						}
						map.put("id", supplierTypes.getPk_suppliertype());
						map.put("VCODE", supplierTypes.getVcode());
						map.put("text", supplierTypes.getVcode()+"-"+supplierTypes.getVname());
						list.add(map);
					}
				}
				// 创建树的顶级节点
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", "000");
				map.put("text", "全部");
				map.put("hasChildren", "ture");
				list.add(0,map);
				// 转换为JSON字符串
				JSONSerializer.toJSON(list);
				return JSONSerializer.toJSON(list).toString();
			}
	}
