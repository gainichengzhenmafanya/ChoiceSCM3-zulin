package com.choice.assistant.service.supplier;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.choice.assistant.constants.supplier.PurcontractConstants;
import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.supplier.ChangeLog;
import com.choice.assistant.domain.supplier.Contractclau;
import com.choice.assistant.domain.supplier.Contracteven;
import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.assistant.domain.supplier.Purcontract;
import com.choice.assistant.domain.supplier.Suitstore;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.domain.supplier.Transationatta;
import com.choice.assistant.domain.system.Contractterms;
import com.choice.assistant.domain.util.ManagerName;
import com.choice.assistant.persistence.supplier.PurcontractMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.Util;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.domain.system.Department;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.Local;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.FirmDeliver;
import com.choice.scm.domain.Spprice;

/**
 * 采购合约
 * Created by mc on 14-10-24.
 */
@Service
public class PurcontractService {
    @Autowired
    private PurcontractMapper purcontractMapper;
    @Autowired
    private PageManager<Purcontract> pageManager;
    @Autowired
    private PageManager<Spprice> spPageManager;

    private final transient Log log = LogFactory.getLog(PurcontractService.class);

    /**
     * 获取采购合约
     *
     * @param purcontract
     * @return
     */
	public List<Purcontract> findPurcontract(Purcontract purcontract, Page page) throws CRUDException {
        try {
            return pageManager.selectPage(purcontract, page, PurcontractMapper.class.getName() + ".findPurcontract");
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 根据pk获取合约信息
     *
     * @param purcontract
     * @return
     * @throws CRUDException
     */
    public Purcontract findPurcontractByPk(Purcontract purcontract) throws CRUDException {
        try {
        	purcontract = purcontractMapper.findPurcontractByPK(purcontract);
        	if (purcontract.getPk_purcontract() != null && !"".equals(purcontract.getPk_purcontract())) {
        		List<Spprice> listSpprice = purcontractMapper.findSppriceByPK(purcontract);
        		List<Contracteven> listContracteven = purcontractMapper.findContractevenBYPK(purcontract);
        		List<Contractclau> listContractclau = purcontractMapper.findContractclauByPK(purcontract);
        		List<Transationatta> listTransationatta = purcontractMapper.findTransationattaByPK(purcontract);
        		List<ChangeLog> listChangeLog = purcontractMapper.findChangeLogByPK(purcontract);
        		purcontract.setContractclauList(listContractclau);
        		purcontract.setContractevenList(listContracteven);
        		purcontract.setTransationattaList(listTransationatta);
        		purcontract.setChangeLogList(listChangeLog);
        		purcontract.setSppriceList(listSpprice);
			}
            return purcontract;
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 获取适用门店信息
     *
     * @param suitstore
     * @return
     * @throws CRUDException
     */
    public List<Suitstore> findSuitstore(Suitstore suitstore) throws CRUDException {
        try {
            return purcontractMapper.findStore(suitstore);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 获取合约类型
     *
     * @param acct
     * @return
     * @throws CRUDException
     */
    public List<Contractterms> findContractterms(String acct) throws CRUDException {
        try {
            return purcontractMapper.findContractterms(acct);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 获取报价批次
     * @param purcontract
     * @return
     * @throws CRUDException
     */
    public Integer findBatch(Purcontract purcontract) throws CRUDException {
        try {
            Integer bat=purcontractMapper.findBatch(purcontract);
            return bat;
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 采购合约保存
     *
     * @param purcontract
     */
    public String savePurcontract(Purcontract purcontract,String group,String username) throws CRUDException {
        try {
            int count=purcontractMapper.findHaveCode(purcontract.getVcontractcode(),null,group);
            if(count>0){
                return Local.show(LanguagesConstants.CODE_ERROR);
            }
            if(purcontract.getIhintday()==null||"".equals(purcontract.getIhintday())){
                purcontract.setIhintday(0);
            }
            purcontract.setAcct(group);
            if(purcontract.getPk_purcontract()==null||purcontract.getPk_purcontract().trim().equals("")) {
                purcontract.setPk_purcontract(CodeHelper.createUUID());
                if (purcontract.getTransationattaList() != null && purcontract.getTransationattaList().size() > 0) {//判定合约报价是否为空
                    for(Transationatta transationatta:purcontract.getTransationattaList()){
                        transationatta.setPk_purcontract(purcontract.getPk_purcontract());
                        transationatta.setTs(DateFormat.getTs());
                    }
                }
            }
            if(purcontract.getSppriceList()!=null&&purcontract.getSppriceList().size()>0) {//判定合约报价是否为空
            	List<Spprice> sppricelist = new ArrayList<Spprice>();
            	for (Spprice spprice : purcontract.getSppriceList()) {
                    if(spprice.getSuitstoreList()!=null && spprice.getSuitstoreList().size()>0) {//判定适用门店是否为空
                        for (Suitstore suitstore : spprice.getSuitstoreList()) {
                        	Spprice insertsp = (Spprice)BeanUtils.cloneBean(spprice);
                        	insertsp.setAcct(group);//报价group赋值
                        	insertsp.setPk_purcontract(purcontract.getPk_purcontract());//报价对应合约主键
                        	insertsp.setPk_spprice(CodeHelper.createUUID());//报价对应主键
                        	insertsp.setPriceold(new BigDecimal(0));
                        	insertsp.setEmp(username);//添加员工
                        	insertsp.setArea(suitstore.getPositn().getCode());
                        	sppricelist.add(insertsp);
                        }
                    }
                }
            	if(sppricelist.size()>0){
            		purcontractMapper.saveSpprice(sppricelist);
            	}
            }else {
                return Local.show(LanguagesConstants.PURCONTRACT_NULL);
            }
            if(purcontract.getContractevenList()!=null&&purcontract.getContractevenList().size()>0) {//判定合约大事件是否为空
                for (Contracteven contracteven : purcontract.getContractevenList()) {
                    contracteven.setAcct(group);//报价group赋值
                    contracteven.setPk_purcontract(purcontract.getPk_purcontract());//报价对应合约主键
                    contracteven.setPk_contracteven(CodeHelper.createUUID());//报价对应主键
                }
                purcontractMapper.delContracteven(purcontract.getPk_purcontract(),group);
                purcontractMapper.saveContracteven(purcontract.getContractevenList());
            }
            if(purcontract.getContractclauList()!=null&&purcontract.getContractclauList().size()>0) {//判定合约条款是否为空
                for (Contractclau contractclau : purcontract.getContractclauList()) {
                    contractclau.setAcct(group);//报价group赋值
                    contractclau.setPk_purcontract(purcontract.getPk_purcontract());//报价对应合约主键
                    contractclau.setPk_contractclau(CodeHelper.createUUID());//报价对应主键
                }
                purcontractMapper.delContractclau(purcontract.getPk_purcontract(),group);
                purcontractMapper.saveContractclau(purcontract.getContractclauList());
            }
            purcontract.setTs(DateFormat.getTs());
            purcontractMapper.savePurcontract(purcontract);
            return "success";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 修改采购合约
     * @param purcontract
     * @param group
     * @throws CRUDException
     */
    public String updatePurcontract(Purcontract purcontract,String group,String accountId,String accountName) throws CRUDException {
        try {
            int count=purcontractMapper.findHaveCode(purcontract.getVcontractcode(),purcontract.getPk_purcontract(),group);
            if(count>1){
                return Local.show(LanguagesConstants.CODE_ERROR);
            }
            if(purcontract.getIhintday()==null&&"".equals(purcontract.getIhintday())){
                purcontract.setIhintday(0);
            }
            purcontract.setAcct(group);
            Purcontract oldPur=findPurcontractByPk(purcontract);
            if(oldPur!=null){
                StringBuffer stringBuffer= null;
                try {
                    stringBuffer = updateLog(oldPur, purcontract);
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if(stringBuffer!=null){
                    ChangeLog changeLog=new ChangeLog();
                    changeLog.setDr(0);
                    changeLog.setTs(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
                    changeLog.setPk_changelog(CodeHelper.createUUID());
                    changeLog.setVaccount(accountId);
                    changeLog.setVaccountname(accountName);
                    changeLog.setPk_purcontract(purcontract.getPk_purcontract());
                    changeLog.setVchangecontent(stringBuffer.toString());
                    changeLog.setVersion(purcontract.getVcontractversion());
                    changeLog.setVchangedate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
                    purcontractMapper.saveChangeLog(changeLog);
                }
            }
        	if(purcontract.getSppriceList()!=null&&purcontract.getSppriceList().size()>0) {//判定合约报价是否为空
        		    
                	List<Spprice> sppricelist = new ArrayList<Spprice>();
                	for (Spprice spprice : purcontract.getSppriceList()) {
                		spprice.setAcct(purcontract.getAcct());
                        if(spprice.getSuitstoreList()!=null && spprice.getSuitstoreList().size()>0) {//判定适用门店是否为空
                        	purcontractMapper.delSuitstoreDr(spprice);//先删除在添加
                            for (Suitstore suitstore : spprice.getSuitstoreList()) {
                            	Spprice insertsp = (Spprice)BeanUtils.cloneBean(spprice);
                            	insertsp.setAcct(group);//报价group赋值
                            	insertsp.setPk_purcontract(purcontract.getPk_purcontract());//报价对应合约主键
                            	insertsp.setPk_spprice(CodeHelper.createUUID());//报价对应主键
                            	insertsp.setPriceold(new BigDecimal(0));
                            	insertsp.setEmp(accountName);//添加员工
                            	insertsp.setArea(suitstore.getPositn().getCode());
                            	sppricelist.add(insertsp);
                            }
                        }else{
                        	purcontractMapper.updateSpprice(spprice);
                        }
                    }
                	if(sppricelist.size()>0){
                		purcontractMapper.saveSpprice(sppricelist);
                	}
            }
            if(purcontract.getContractevenList()!=null&&purcontract.getContractevenList().size()>0) {//判定合约大事件是否为空
                for (Contracteven contracteven : purcontract.getContractevenList()) {
                    contracteven.setAcct(group);//报价group赋值
                    contracteven.setPk_purcontract(purcontract.getPk_purcontract());//报价对应合约主键
                    contracteven.setPk_contracteven(CodeHelper.createUUID());//报价对应主键
                    contracteven.setTs(DateFormat.getTs());
                }
                purcontractMapper.delContracteven(purcontract.getPk_purcontract(),group);
                purcontractMapper.saveContracteven(purcontract.getContractevenList());
            }
            if(purcontract.getContractclauList()!=null&&purcontract.getContractclauList().size()>0) {//判定合约条款是否为空
                for (Contractclau contractclau : purcontract.getContractclauList()) {
                    contractclau.setAcct(group);//报价group赋值
                    contractclau.setPk_purcontract(purcontract.getPk_purcontract());//报价对应合约主键
                    contractclau.setPk_contractclau(CodeHelper.createUUID());//报价对应主键
                    contractclau.setTs(DateFormat.getTs());
                }
                purcontractMapper.delContractclau(purcontract.getPk_purcontract(),group);
                purcontractMapper.saveContractclau(purcontract.getContractclauList());
            }
            purcontract.setTs(DateFormat.getTs());
            purcontractMapper.updatePurcontract(purcontract);
            return "success";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    @SuppressWarnings("unchecked")
	public StringBuffer updateLog(Object oldPur,Object newPur) throws InvocationTargetException, IllegalAccessException {
        ManagerName cName=oldPur==null?null:oldPur.getClass().getAnnotation(ManagerName.class);
        cName=cName!=null?cName:newPur==null?null:newPur.getClass().getAnnotation(ManagerName.class);
        if(cName==null){
            return null;
        }
        Method[] methods=oldPur!=null?oldPur.getClass().getMethods():newPur==null?null:newPur.getClass().getMethods();
        StringBuffer stringBuffer=null;
        StringBuffer confText=new StringBuffer();
        for(Method method:methods){
            String methodName=method.getName();
            ManagerName aName=method.getAnnotation(ManagerName.class);//获取注解的注释
            if(aName==null){//判断注解是否为空
                continue;
            }
            if(methodName.matches("^get[A-Z]\\w+$")){//判断是不是get方法
                Class<?> cla=method.getReturnType();
                Object oldObj=oldPur==null?null:method.invoke(oldPur);//获取老数据的值
                Object newObj=newPur==null?null:method.invoke(newPur);//获取心数据的值
                String oldValue="";
                String newValue="";
                boolean isEqual=false;
                if(cla==String.class){
                    if (oldObj != null && newObj != null) {//如果获取的String 值不为空时
                        if (!oldObj.toString().trim().equals(newObj.toString().trim())) {//如果两值不想等
                            isEqual = true;
                        }
                    } else if (oldObj != null || newObj != null) {//如果下载值有的空有的不空
                        isEqual = true;
                    }
                    if(isEqual){//两值不想等
                        oldValue = oldObj==null?"":oldObj.toString();
                        newValue = newObj==null?"":newObj.toString();
                    }else{
                        if(!aName.isConfirm()){
                            setConfSB(confText,aName.value(),oldObj==null?"":oldObj.toString(),newObj==null?"":newObj.toString());
                        }
                    }
                }else if(cla==Integer.class||cla==Double.class){
                    if (oldObj != null && newObj != null) {//如果获取的String 值不为空时
                        if (!oldObj.equals(newObj)) {//如果两值不想等
                            if (oldObj != newObj) {
                                isEqual = true;
                            }
                        }
                    } else if (oldObj != null || newObj != null) {//如果下载值有的空有的不空
                        isEqual = true;
                    }
                    if(isEqual&&!aName.isDB()){//两值不想等
                        oldValue = oldObj==null?"":oldObj.toString();
                        newValue = newObj==null?"":newObj.toString();
                    }else if(aName.isDB()){
                        try {
                            oldValue = PurcontractConstants.ISTART.valueOf(aName.enumName() + oldObj).getValue();
                        }catch (Exception e){
                            log.error(e);
                        }
                        try {
                            newValue = PurcontractConstants.ISTART.valueOf(aName.enumName() + newObj).getValue();
                        }catch (Exception e){
                            log.error(e);
                        }
                    }else{
                        if(!aName.isConfirm()){
                            setConfSB(confText,aName.value(),oldObj==null?"":oldObj.toString(),newObj==null?"":newObj.toString());
                        }
                    }
                }else if(cla==Supplier.class){
                    if(oldObj!=null) {
                        oldValue=((Supplier) oldObj).getPk_supplier()!=null?((Supplier) oldObj).getPk_supplier():"";
                    }
                    if(newObj!=null){
                        newValue=((Supplier) newObj).getPk_supplier()!=null?((Supplier) newObj).getPk_supplier():"";
                    }
                    if(!oldValue.equals(newValue)){
                        oldValue=oldObj==null?"":((Supplier) oldObj).getVname();
                        newValue=newObj==null?"":((Supplier) newObj).getVname();
                        isEqual=true;
                    }else{
                        if(!aName.isConfirm()){
                            setConfSB(confText,aName.value(),oldObj==null?"":((Supplier) oldObj).getVname(),newObj==null?"":((Supplier) newObj).getVname());
                        }
                    }
                }else if(cla==Department.class){
                    if (oldObj != null) {
                        oldValue = ((Department) oldObj).getId() != null ? ((Department) oldObj).getId() : "";
                    }
                    if (newObj != null) {
                        newValue = ((Department) newObj).getId() != null ? ((Department) newObj).getId() : "";
                    }
                    if(!oldValue.equals(newValue)){
                        oldValue=oldObj==null?"":((Department) oldObj).getName();
                        newValue=newObj==null?"":((Department) newObj).getName();
                        isEqual=true;
                    }else{
                        if(!aName.isConfirm()){
                            setConfSB(confText,aName.value(),oldObj==null?"":((Department) oldObj).getName(),newObj==null?"":((Department) newObj).getName());
                        }
                    }
                }else if(cla==List.class){
                    List<Object> newList= newObj==null?null:(List<Object>) newObj;
                    List<Object> oldList= oldObj==null?null:(List<Object>) oldObj;
                    if(newList!=null||oldList!=null){
                        int newCount=newList!=null?newList.size():0;
                        int oldCount=oldList!=null?oldList.size():0;
                        int count=newCount>oldCount?newCount:oldCount;
                        for(int i=0;i<count;i++){
                            Object nobj=null;
                            Object oobj=null;
                            if(newCount>i){
                                nobj=newList.get(i);
                            }
                            if(oldCount>i){
                                oobj=oldList.get(i);
                            }
                            StringBuffer log=updateLog(oobj, nobj);
                            if(log!=null) {
                                if(stringBuffer==null){
                                    stringBuffer=new StringBuffer();
                                    stringBuffer.append(cName.value()).append(":[");
                                }
                                stringBuffer.append(log);
                            }
                        }
                    }
                }
                if(isEqual){
                    if(stringBuffer==null){
                        stringBuffer=new StringBuffer();
                        stringBuffer.append(cName.value()).append(":[");
                    }
                    stringBuffer.append(confText).append(aName.value())
                            .append("[").append(oldValue)
                            .append("\t 变更为").append(newValue)
                            .append("]\n");
                }
            }
        }
        if(stringBuffer!=null){
            stringBuffer.append("]");
        }
        return stringBuffer;
    }

    /**
     * 拼接必须数据 如物资名称 物资批次
     * @param str
     * @param name
     * @param oldValue
     * @param newValue
     */
    private void setConfSB(StringBuffer str,String name,String oldValue,String newValue){
        str.append(name)
                .append("[").append(oldValue)
                .append("\t 变更为").append(newValue)
                .append("]\n");
    }
    /**
     * 删除合约
     * @param ids
     * @param group
     * @throws CRUDException
     */
    public String delPurcontract(String ids,String versions,String group,String accountId,String accountName) throws CRUDException {
        try{
            List<String> list= ids==null?null:Arrays.asList(ids.split(","));
            if(list!=null&&list.size()>0) {
                int count = purcontractMapper.findStatePur(list);
                if (count > 0) {//判定合约状态
                    return Local.show(LanguagesConstants.PURCONTRACT_CANT_DEL);
                }
                Map<String, Object> map = new HashMap<String, Object>();
                ChangeLog changeLog=new ChangeLog();
                changeLog.setVaccountname(accountName);
                changeLog.setVaccount(accountId);
                changeLog.setVchangedate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
                changeLog.setVchangecontent("删除合约！");
                List<String> verlist=versions==null?null:Arrays.asList(versions.split(","));
                for(int i=0;i<list.size();i++) {
                    changeLog.setPk_changelog(CodeHelper.createUUID());
                    changeLog.setPk_purcontract(list.get(i));
                    if(verlist!=null&&verlist.size()==list.size()){
                        changeLog.setVersion(verlist.get(i));
                    }
                    purcontractMapper.saveChangeLog(changeLog);
                }
                map.put("list", list);
                map.put("acct", group);
                purcontractMapper.delPurcontractDr(map);
            }
            return "ok";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 修改合约状态
     * @throws CRUDException
     */
    public String updatePurState(String istate,String ids,String versions,String oldistate,String accountId,String accountName) throws CRUDException {
        try{
            List<String> list=ids==null?null:Arrays.asList(ids.split(","));
            if(list!=null&&list.size()>0) {
//                int count = purcontractMapper.findStatePur(list);
//                if(count>0){
//                    if(!"4".equals(istate)) {
//                        return Local.show(LanguagesConstants.PURCONTRACT_AUDIT_ERROR);
//                    }
//                }
                ChangeLog changeLog=new ChangeLog();
                changeLog.setVaccountname(accountName);
                changeLog.setVaccount(accountId);
                changeLog.setVchangedate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
                List<String> verlist=versions==null?null:Arrays.asList(versions.split(","));
                List<String> oldlist=oldistate==null?null:Arrays.asList(oldistate.split(","));
                for(int i=0;i<list.size();i++) {
                    changeLog.setPk_changelog(CodeHelper.createUUID());
                    changeLog.setPk_purcontract(list.get(i));
                    if(verlist!=null&&verlist.size()==list.size()){
                        changeLog.setVersion(verlist.get(i));
                    }
                    try {
                        if(oldlist!=null&&oldlist.size()==list.size()) {
                            changeLog.setVchangecontent("合约状态修改：["+PurcontractConstants.ISTART.valueOf("S" + oldlist.get(i)).getValue()+"\t 变更为" + PurcontractConstants.ISTART.valueOf("S" + istate).getValue() + "]");
                        }else{
                            changeLog.setVchangecontent("合约状态修改：[" + PurcontractConstants.ISTART.valueOf("S" + istate).getValue() + "]");
                        }
                    }catch (Exception e){
                        log.error(e);
                        changeLog.setVchangecontent("合约状态修改");
                    }
                    purcontractMapper.saveChangeLog(changeLog);
                }
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("list", list);
                map.put("istate", istate);
                purcontractMapper.updatePurState(map);
            }
            return "ok";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 把时间到了的合约改为 终止
     */
    public void updatePurcontractState() throws CRUDException {
        try {
            purcontractMapper.updatePurcontractState(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 修改适用门店
     * @param contractQuote
     * @param ids
     * @throws CRUDException
     */
    public String updateStore(Spprice spprice,String ids) throws CRUDException {
        try{
        	List<Spprice> existlist = purcontractMapper.findSpprice(spprice);
        	Spprice insertSpprice = existlist.get(0);
            purcontractMapper.delSuitstoreDr(spprice);//先删除在添加
            String pks[]=ids.split(",");
            if(pks!=null&&pks.length>0){
            	List<Spprice> insertlist = new ArrayList<Spprice>();
            	for(String firmid:pks){
            		Spprice sp = (Spprice)BeanUtils.cloneBean(insertSpprice);
            		sp.setArea(firmid);
            		insertlist.add(sp);
            	}
            	purcontractMapper.saveSpprice(insertlist);
            }else {
                return Local.show(LanguagesConstants.APPLY_STORE_NULL);
            }
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
        return "ok";
    }

    /**
     * 根据供应商获取供应商供应物资
     * @param materialScope
     * @return
     * @throws CRUDException
     */
    public List<MaterialScope> queryMaterial(MaterialScope materialScope) throws CRUDException {
        try {
            return purcontractMapper.findMaterial(materialScope);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 获取适用门店
     * @param org
     * @return
     * @throws CRUDException
     */
    public List<FirmDeliver> queryFirm(FirmDeliver firmDeliver) throws CRUDException {
        try {
            return purcontractMapper.queryFirm(firmDeliver);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 文件上传
     * @param request
     * @return
     * @throws Exception
     */
    public Transationatta upload(HttpServletRequest request,String pk_purcontract) throws CRUDException {
        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("file");
            String customName=request.getSession().getAttribute("ChoiceAcct")+DateFormat.getStringByDate(new Date(),"yyyyMMddHHmmss");
            String uploadFilePath = "upload/"+customName+file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
            String filePath=Util.uploadFile(request, "file", uploadFilePath, "upload", customName);
            Transationatta transationatta=new Transationatta();
            transationatta.setPk_transationatta(CodeHelper.createUUID());
            transationatta.setAcct(request.getSession().getAttribute("ChoiceAcct").toString());
            transationatta.setDr(0);
            transationatta.setVattafile(filePath);
            transationatta.setVattaname(file.getOriginalFilename());
            transationatta.setPk_purcontract(pk_purcontract);
            transationatta.setTs(DateFormat.getTs());
            purcontractMapper.saveTransationatta(transationatta);
            return transationatta;
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 根据附件pk获取附件信息
     * @param pk
     * @return
     * @throws CRUDException
     */
    public Transationatta findTransationattaByPk(String pk) throws CRUDException {
        try{
            return purcontractMapper.findTransationattaByPk(pk);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 删除附件
     * @param ids
     * @throws CRUDException
     */
    public void delTta(String ids) throws CRUDException {
        try {
            List<String> list = Arrays.asList(ids.split(","));
            purcontractMapper.delTransationatta(list);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

	public List<Spprice> getContractQuoteByPur_id(Purcontract purcontract,Page page) throws CRUDException {
        try {
            return spPageManager.selectPage(purcontract, page, PurcontractMapper.class.getName() + ".findSppriceByPK");
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 检测一个采购组织、一个供应商 采购合约是否时间重叠，重叠返回-1 不重叠返回1 
     * @param purcontract
     * @return
     * @throws CRUDException
     */
	public String checkPurcontract(Purcontract purcontract) throws CRUDException {
        try {
        	Purcontract purcontractdd= purcontractMapper.checkPurcontract(purcontract);
        	if(ValueCheck.IsNotEmpty(purcontractdd)){
        		return "采购合约计划生效、终止日期与"+purcontractdd.getVcontractname()+"("+purcontractdd.getVcontractcode()+")有交叉，请重新定义。";
        	}else{
        		return "1";
        	}
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
	}
}
