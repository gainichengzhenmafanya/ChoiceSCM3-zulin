package com.choice.assistant.service.supplier;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.supplier.SupplierRM;
import com.choice.assistant.domain.supplier.SupplierTO;
import com.choice.assistant.persistence.supplier.SupplierRecruitmentMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.Local;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

/**
 * 供应商招募
 *
 * @author zhb
 */
@Service
public class SupplierRecruitmentService {

    @Autowired
    private SupplierRecruitmentMapper supplierRMMapper;
    @Autowired
    private PageManager<SupplierRM> pageManager;
    
    @Autowired
	private PageManager<SupplierTO> pageManagerd;

    private final transient Log log = LogFactory.getLog(SupplierRecruitmentService.class);
   /**
    * 获取所有的供应商招募
    * @param supplierRM
    * @param page
    * @return
    * @throws CRUDException
    */
    public List<SupplierRM> querySupplierRM(SupplierRM supplierRM, Page page) throws CRUDException {
        try {
            return pageManager.selectPage(supplierRM, page, SupplierRecruitmentMapper.class.getName() + ".querySupplierRM");
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 根据供应商招募pk获取数据-未删除的
     * @param supplierRM
     * @return
     * @throws CRUDException
     */
    public List<SupplierTO> findSupplierRMByPk(SupplierTO supplierTO) throws CRUDException {
        try {
            return supplierRMMapper.findSupplierRMByPk(supplierTO);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 根据供应商招募pkS获取数据-被删除的
     * @param supplierRM
     * @return
     * @throws CRUDException
     */
    public List<SupplierTO> findSupplierRMByPkS(SupplierTO supplierTO) throws CRUDException {
        try {
            return supplierRMMapper.findSupplierRMByPkS(supplierTO);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 根据供应商招募pk获取主数据
     * @param supplierRM
     * @return
     * @throws CRUDException
     */
    public SupplierRM findSupplierRMZHUByPk(SupplierRM supplierRM) throws CRUDException {
        try {
            return supplierRMMapper.findSupplierRMZHUByPk(supplierRM);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 供应商招募主表新增
     * @param supplierRM
     * @param company
     * @param pk
     * @return
     * @throws Exception 
     */
    public String addSupplierRM(SupplierRM supplierRM, String acct, String pk) throws Exception {
        try {
        	supplierRM.setAcct(acct);
            if(supplierRMMapper.findSupplierRMByCode(supplierRM)!=null){
            	return Local.show(LanguagesConstants.CODE_ERROR);
                //return "编码重复！";
            }
            supplierRM.setPk_recruit(pk);
            supplierRM.setDr(0);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            supplierRM.setTs(df.format(new Date()));//当前时间
            supplierRMMapper.addSupplierRM(supplierRM);
            return "ok";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 供应商招募主表修改
     *
     * @param supplierRM
     * @throws CRUDException
     */
    public void updateSupplierRM(SupplierRM supplierRM) throws CRUDException {
        try {
        	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            supplierRM.setTs(df.format(new Date()));//当前时间
            supplierRMMapper.updateSupplierRM(supplierRM);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 供应商应募申请删除，dr的值变为1
     *
     * @param supplierRM
     * @throws CRUDException
     */
    public void updateyminfo(String code,String ts) throws CRUDException {
        try {
            supplierRMMapper.updateyminfo(code,ts);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 供应商应募恢复，dr的值变为0
     *
     * @param supplierRM
     * @throws CRUDException
     */
    public void hfupdateyminfo(String code,String ts) throws CRUDException {
        try {
            supplierRMMapper.hfupdateyminfo(code,ts);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 删除供应商招募信息
     *
     * @param ids
     * @throws CRUDException
     */
    public void deleteSupplierRM(String ids) throws CRUDException {
        try {
            List<String> idlist = Arrays.asList(ids.split(","));
            supplierRMMapper.deleteSupplierRM(idlist);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 供应商招募状态修改
     *
     * @throws CRUDException
     */
    public void updateEnablestate(String enablestate, String ids) throws CRUDException {
        try {
            List<String> list = Arrays.asList(ids.split(","));
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("enablestate", enablestate);
            map.put("list", list);
            supplierRMMapper.updateEnablestate(map);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
	 * 查询选中的应募信息
	 */
	public SupplierTO findAllSupplierTObygysid(SupplierTO supplierTO) {
		return supplierRMMapper.findAllSupplierTObygysid(supplierTO);
	}
	/**
	 * 判断加入供应商待审核是否重复
	 * @param delivercode
	 * @return
	 * @throws CRUDException
	 */
	public String findAuditbypks(String delivercode,String acct) throws CRUDException {
		return	supplierRMMapper.findAuditbypks(delivercode,acct);
	}
	
	/**
	 * 插入招募书应募信息
	 * @param supplierTOd
	 * @throws CRUDException
	 */
//	public void addEnlist(SupplierTO supplierTOd)  throws CRUDException {
//		try{
//			supplierRMMapper.addEnlist(supplierTOd);
//		} catch (Exception e) {
//	        log.error(e);
//	        throw new CRUDException(e);
//	    }
//	}
	
	/**
	 * 查询招募书应募信息
	 * @param supplierTO
	 * @return
	 */
	public List<SupplierTO> queryAllSupplierTO(SupplierRM supplierRM, Page page) throws CRUDException{
		try {
			return pageManagerd.selectPage(supplierRM, page, SupplierRecruitmentMapper.class.getName()+".queryAllSupplierTO");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询招募书应募信息
	 * @param supplierTO
	 * @return
	 */
	public List<SupplierTO> queryAllSupplierTO(SupplierTO supplierTO) throws CRUDException{
		try {
			return supplierRMMapper.queryAllSupplierTOByCode(supplierTO);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增应募信息
	 * @param supplierTO
	 * @throws CRUDException
	 */
	public String insertSupplierTO(SupplierTO supplierTO) throws CRUDException{
		try {
			supplierTO.setDr("0");
			supplierTO.setPk_enlist(CodeHelper.createUUID());
			supplierTO.setTs(DateFormat.getTs());
//			cgtenderd.setVdatetime(DateFormat.getTs());
//			CgTender cgtender = new CgTender();
//			cgtender.setPk_cgtender(cgtenderd.getPk_cgtender());
//			cgtender = supplierRMMapper.queryCgTenderByID(cgtender);
//			cgtenderd.setVtendermemo(cgtender.getVtendermemo());
//			cgtenderd.setVtendermemo(cgtender.getVtendermemo());
//			cgtenderd.setPk_paymethod(cgtender.getPk_paymethod());
//			cgtenderd.setVpaytypecode(cgtender.getVpaytypecode());
//			cgtenderd.setVpaytypename(cgtender.getVpaytypename());
			supplierRMMapper.addEnlist(supplierTO);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改应募书
	 * @param session
	 * @param supplierTO
	 * @return
	 * @throws Exception
	 */
	public void updateSupplierTO(HttpSession session, SupplierTO supplierTO)throws CRUDException{
		try {
			supplierTO.setAcct(session.getAttribute("ChoiceAcct").toString());
			supplierTO.setTs(DateFormat.getTs());
			supplierRMMapper.updateSupplierTO(supplierTO);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除应募书
	 * @param supplierTO
	 * @throws CRUDException
	 */
	public String deleteSupplierTO(SupplierTO supplierTO)throws CRUDException{
		try {
			supplierRMMapper.deleteSupplierTO(supplierTO);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主键查询应募
	 * @param supTo
	 * @return
	 * @throws CRUDException
	 */
	public SupplierTO querySupplierTOByID(SupplierTO supplierTO)throws CRUDException{
		try {
			SupplierTO supTo = supplierRMMapper.querySupplierTOByID(supplierTO);
			return supTo;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
