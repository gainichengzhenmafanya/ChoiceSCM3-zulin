package com.choice.assistant.service.supplier;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.supplier.Suppliereval;
import com.choice.assistant.domain.supplier.Supplierevald;
import com.choice.assistant.domain.system.Evalitem;
import com.choice.assistant.persistence.supplier.SupplierEvaluateMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.Local;

/**
 * 供应商评价
 * Created by mc on 14-12-4.
 */
@Service
public class SupplierEvaluateService {
    @Autowired
    private SupplierEvaluateMapper supplierEvaluateMapper;

    private final transient Log log = LogFactory.getLog(SupplierEvaluateService.class);

    /**
     * 获取评价项
     * @param evalitem
     * @return
     * @throws CRUDException
     */
    public List<Evalitem> queryEvalitem(Evalitem evalitem,String acct) throws CRUDException {
        try {
            evalitem.setAcct(acct);
            return supplierEvaluateMapper.queryEvalitem(evalitem);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    public List<Suppliereval> listbottom(Suppliereval suppliereval,String acct) throws CRUDException{
        try {
            suppliereval.setAcct(acct);
            return supplierEvaluateMapper.querySuppliereval(suppliereval);
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 保存评价项目
     * @param evalitem
     * @param acct
     * @throws CRUDException
     */
    public void saveEvalitem(Evalitem evalitem,String acct) throws CRUDException {
        try{
            supplierEvaluateMapper.delEvalitem(acct);
            if(evalitem.getEvalitems()!=null&&evalitem.getEvalitems().size()>0){
                for(Evalitem eva:evalitem.getEvalitems()){
                    eva.setAcct(acct);
                    eva.setPk_evalitem(CodeHelper.createUUID());
                }
                supplierEvaluateMapper.saveEvalitem(evalitem.getEvalitems());
            }
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    public String addEvaluation(Suppliereval suppliereval,String acct) throws CRUDException {
        try {
            suppliereval.setAcct(acct);
            suppliereval.setPk_suppliereval(CodeHelper.createUUID());
            suppliereval.setDevaldate(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
            suppliereval.setTs(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
            if(suppliereval.getSupplierevalds()!=null&&suppliereval.getSupplierevalds().size()>0){
                for(Supplierevald supplierevald:suppliereval.getSupplierevalds()){
                    supplierevald.setPk_suppliereval(suppliereval.getPk_suppliereval());
                    supplierevald.setAcct(acct);
                    supplierevald.setPk_supplierevald(CodeHelper.createUUID());
                    supplierevald.setTs(suppliereval.getTs());
                    if(supplierevald.getNscore()>100){
                        return Local.show(LanguagesConstants.NSCORE_ERROR);
                    }
                }
                supplierEvaluateMapper.saveSupplierevald(suppliereval.getSupplierevalds());
            }
            if(suppliereval.getNscore()>100){
                return Local.show(LanguagesConstants.NSCORE_ERROR);
            }
            supplierEvaluateMapper.saveSuppliereval(suppliereval);
            return "success";
        } catch (Exception e) {
            log.error(e);
            throw new CRUDException(e);
        }
    }
}
