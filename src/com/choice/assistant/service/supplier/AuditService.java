package com.choice.assistant.service.supplier;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.persistence.material.MaterialMapper;
import com.choice.assistant.persistence.supplier.AuditMapper;
import com.choice.assistant.persistence.supplier.SupplierMapper;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;

/**
 * 供应商审核
 * Created by mc on 14-10-29.
 */
@Service
public class AuditService {
    @Autowired
    private AuditMapper auditMapper;
    @Autowired
    private SupplierMapper supplierMapper;
	@Autowired
	private MaterialMapper materialMapper;

    private final transient Log log = LogFactory.getLog(AuditService.class);

    /**
     * 获得所有待审核供应商
     * @param auditsupplier
     * @return
     * @throws CRUDException
     */
    public List<Auditsupplier> findAudit(Auditsupplier auditsupplier) throws CRUDException {
        try {
            return auditMapper.findAudit(auditsupplier);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 获得所有待审核供应商按主键查询
     * @param auditsupplier
     * @return
     * @throws CRUDException
     */
    public List<Auditsupplier> queryAudit(Auditsupplier auditsupplier) throws CRUDException {
    	try {
    		return auditMapper.queryAudit(auditsupplier);
    	}catch (Exception e){
    		log.error(e);
    		throw new CRUDException(e);
    	}
    }

    /**
     * 删除待审核供应商
     * @param ids
     * @throws CRUDException
     */
    public void delAudit(String ids) throws CRUDException {
        try {
            List<String> list = Arrays.asList(ids.split(","));
            auditMapper.delAudit(list);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 根据供应商的pk删除待审核供应商
     * @param id
     * @throws CRUDException
     */
    public void delOneAudit(String id) throws CRUDException {
        try {
            auditMapper.delOneAudit(id);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }

    /**
     * 关联供应商
     * @param supplier
     * @throws CRUDException
     */
    public void linkSupplier(Supplier supplier) throws CRUDException {
        try {
        	Supplier sup = new Supplier();
        	sup = supplierMapper.findSupplicerByPk(supplier);
        	//如果之前关联的商城供应商与本次选择商城供应商的一样就不更新物资关联关系了
        	if(!supplier.getPk_supplierjum().equals(sup.getPk_supplierjum())){
	        	if(ValueCheck.IsNotEmpty(sup.getPk_supplierjum())){//如果之前关联了商城供应商 清空物资管理商城物资关系
	        	}
        	}
            auditMapper.linkSupplier(supplier);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
    /**
     * 检测当前供应商是否已经关联了本地供应商
     * @param id
     * @param acct
     * @return
     * @throws CRUDException
     */
    public int isLink(String id, String acct) throws CRUDException {
        try{
        	if(ValueCheck.IsEmpty(acct)){
        		return 1;
        	}
            return auditMapper.isLink(id,acct);
        }catch (Exception e){
            log.error(e);
            throw new CRUDException(e);
        }
    }
}
