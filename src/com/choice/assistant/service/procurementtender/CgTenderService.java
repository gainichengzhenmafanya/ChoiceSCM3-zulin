package com.choice.assistant.service.procurementtender;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.assistant.domain.procurementtender.CgTender;
import com.choice.assistant.domain.procurementtender.CgTenderd;
import com.choice.assistant.domain.system.PayMethod;
import com.choice.assistant.domain.system.Tender;
import com.choice.assistant.persistence.procurementtender.CgTenderMapper;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.CodeHelper;
import com.choice.orientationSys.persistence.system.impl.PageManager;
import com.choice.orientationSys.util.Page;

@Service
public class CgTenderService {

	@Autowired
	private CgTenderMapper cgTenderMapper;
	@Autowired
	private PageManager<CgTender> pageManager;
	@Autowired
	private PageManager<CgTenderd> pageManagerd;

	private final transient Log log = LogFactory.getLog(CgTenderService.class);

	/**
	 * 查询所有的招标
	 * @param cgtender
	 * @return
	 */
	public List<CgTender> queryAllCgTender(CgTender cgtender,Page page)throws CRUDException{
		try {
			return pageManager.selectPage(cgtender, page, CgTenderMapper.class.getName()+".queryAllCgTender");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的招标类型
	 * @param tender
	 * @return
	 * @throws CRUDException
	 */
	public List<Tender> queryAllTender(Tender tender)throws CRUDException{
		try {
			return cgTenderMapper.queryAllTender(tender);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询所有的付款方式
	 * @param paytype
	 * @return
	 * @throws CRUDException
	 */
	public List<PayMethod> queryAllPayType(PayMethod paymethod)throws CRUDException{
		try {
			return cgTenderMapper.queryAllPayType(paymethod);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 保存采购招标
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	public String saveCgTender(HttpSession session,CgTender cgtender)throws CRUDException{
		try {
			cgtender.setAcct(session.getAttribute("ChoiceAcct").toString());
			cgtender.setPk_cgtender(CodeHelper.createUUID());
			cgtender.setDr(0);
			cgtender.setTs(DateFormat.getTs());
			Tender ten = new Tender();
			ten.setAcct(session.getAttribute("ChoiceAcct").toString());
			ten.setPk_tender(cgtender.getPk_tender());
			Tender tender = cgTenderMapper.queryAllTender(ten).get(0);
			cgtender.setVtendercode(tender.getVcode());
			cgtender.setVtendername(tender.getVname());
			if(ValueCheck.IsNotEmpty(cgtender.getPk_paymethod())){
				PayMethod pmth = new PayMethod();
				pmth.setAcct(session.getAttribute("ChoiceAcct").toString());
				pmth.setPk_paymethod(cgtender.getPk_paymethod());
				PayMethod payMethod = cgTenderMapper.queryAllPayType(pmth).get(0);
				cgtender.setVpaytypecode(payMethod.getVcode());
				cgtender.setVpaytypename(payMethod.getVname());
			}else{
				cgtender.setVpaytypecode("");
				cgtender.setVpaytypename("");
			}
			cgtender.setVcreatetime(DateFormat.getTs());
			cgtender.setIsstat("1");
			cgtender.setVreceiptaddr(cgtender.getCountry()+"&&&"+cgtender.getProvince()+"&&&"+cgtender.getCity()+"&&&"+cgtender.getXxdzaddr());
			cgtender.setVreleasetime("");
			cgtender.setVstoptime("");
			cgTenderMapper.saveCgTender(cgtender);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主键查询招标
	 * @param cgtender
	 * @return
	 * @throws CRUDException
	 */
	public CgTender queryCgTenderByID(CgTender cgtender)throws CRUDException{
		try {
			CgTender cgten = cgTenderMapper.queryCgTenderByID(cgtender);
			String[] addr = cgten.getVreceiptaddr().split("&&&");
			if(addr[0] != null && addr[0] != ""){
				cgten.setCountry(addr[0]);
			}
			if(addr[1] != null && addr[1] != ""){
				cgten.setProvince(addr[1]);
			}
			if(addr[2] != null && addr[2] != ""){
				cgten.setCity(addr[2]);
			}
			if(addr.length==4){
				if(addr[3] != null && addr[3] != ""){
					cgten.setXxdzaddr(addr[3]);
				}
			}
			return cgten;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改招标书
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	public String updateCgTender(HttpSession session,CgTender cgtender)throws CRUDException{
		try {
			cgtender.setAcct(session.getAttribute("ChoiceAcct").toString());
			cgtender.setTs(DateFormat.getTs());
			Tender ten = new Tender();
			ten.setAcct(session.getAttribute("ChoiceAcct").toString());
			ten.setPk_tender(cgtender.getPk_tender());
			Tender tender = cgTenderMapper.queryAllTender(ten).get(0);
			cgtender.setVtendercode(tender.getVcode());
			cgtender.setVtendername(tender.getVname());
			if(ValueCheck.IsNotEmpty(cgtender.getPk_paymethod())){
				PayMethod pmth = new PayMethod();
				pmth.setAcct(session.getAttribute("ChoiceAcct").toString());
				pmth.setPk_paymethod(cgtender.getPk_paymethod());
				PayMethod payMethod = cgTenderMapper.queryAllPayType(pmth).get(0);
				cgtender.setVpaytypecode(payMethod.getVcode());
				cgtender.setVpaytypename(payMethod.getVname());
			}else{
				cgtender.setVpaytypecode("");
				cgtender.setVpaytypename("");
			}
			cgtender.setVreceiptaddr(cgtender.getCountry()+"&&&"+cgtender.getProvince()+"&&&"+cgtender.getCity()+"&&&"+cgtender.getXxdzaddr());
			cgTenderMapper.updateCgTender(cgtender);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除招标书
	 * @param cgtender
	 * @throws CRUDException
	 */
	public String deleteCgTender(CgTender cgtender)throws CRUDException{
		try {
			cgTenderMapper.deleteCgTender(cgtender);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询招标书应标信息
	 * @param cgtender
	 * @return
	 */
	public List<CgTenderd> queryAllCgTenderd(CgTender cgtender,Page page) throws CRUDException{
		try {
			return pageManagerd.selectPage(cgtender, page, CgTenderMapper.class.getName()+".queryAllCgTenderd");
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 查询招标书应标信息
	 * @param cgtender
	 * @return
	 */
	public List<CgTenderd> queryAllCgTenderd(CgTenderd cgtenderd) throws CRUDException{
		try {
			return cgTenderMapper.queryAllCgTenderdByCode(cgtenderd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 新增应标信息
	 * @param cgtenderd
	 * @throws CRUDException
	 */
	public String insertCgTenderd(CgTenderd cgtenderd) throws CRUDException{
		try {
			cgtenderd.setDr(0);
			cgtenderd.setPk_cgtenderd(CodeHelper.createUUID());
			cgtenderd.setTs(DateFormat.getTs());
			cgtenderd.setVdatetime(DateFormat.getTs());
			CgTender cgtender = new CgTender();
			cgtender.setPk_cgtender(cgtenderd.getPk_cgtender());
			cgtender = cgTenderMapper.queryCgTenderByID(cgtender);
			cgtenderd.setVtendermemo(cgtender.getVtendermemo());
			cgtenderd.setVtendermemo(cgtender.getVtendermemo());
			cgtenderd.setPk_paymethod(cgtender.getPk_paymethod());
			cgtenderd.setVpaytypecode(cgtender.getVpaytypecode());
			cgtenderd.setVpaytypename(cgtender.getVpaytypename());
			cgTenderMapper.insertCgTenderd(cgtenderd);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 修改招标书
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	public void updateCgTenderd(HttpSession session,CgTenderd cgtenderd)throws CRUDException{
		try {
			cgtenderd.setAcct(session.getAttribute("ChoiceAcct").toString());
			cgtenderd.setTs(DateFormat.getTs());
			cgTenderMapper.updateCgTenderd(cgtenderd);
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 删除应标书
	 * @param cgtenderd
	 * @throws CRUDException
	 */
	public String deleteCgTenderd(CgTenderd cgtenderd)throws CRUDException{
		try {
			cgTenderMapper.deleteCgTenderd(cgtenderd);
			return "1";
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
	
	/**
	 * 根据主键查询应标
	 * @param cgtender
	 * @return
	 * @throws CRUDException
	 */
	public CgTenderd queryCgTenderdByID(CgTenderd cgtenderd)throws CRUDException{
		try {
			CgTenderd cgten = cgTenderMapper.queryCgTenderdByID(cgtenderd);
			return cgten;
		} catch (Exception e) {
			log.error(e);
			throw new CRUDException(e);
		}
	}
}
