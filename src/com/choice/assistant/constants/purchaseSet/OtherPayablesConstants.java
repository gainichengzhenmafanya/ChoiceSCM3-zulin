package com.choice.assistant.constants.purchaseSet;

public class OtherPayablesConstants {

	public static final String SELECTOTHERPAYABLESM = "assistant/otherpayables/selectOtherpayablesm";
	
	public static final String LISTOTHERPAYABLESM = "assistant/otherpayables/otherpayablesmList";
	
	public static final String LISTOTHERPAYABLESD = "assistant/otherpayables/otherpayablesdList";
	
	public static final String ADDOTHERPAYABLESD = "assistant/otherpayables/addOtherpayablesm";
	
	public static final String UPDATEOTHERPAYABLESD = "assistant/otherpayables/updateOtherpayablesm";
	
	public static final String SELECTINORDER = "assistant/otherpayables/selectinorder";

}
