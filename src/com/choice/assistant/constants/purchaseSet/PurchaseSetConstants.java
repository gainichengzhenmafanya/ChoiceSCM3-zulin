package com.choice.assistant.constants.purchaseSet;

public class PurchaseSetConstants {

	public static final String TABLEINSPECTM = "assistant/purchaseSet/tableinspectm";

	public static final String TABLEINSPECTD = "assistant/purchaseSet/tableinspectd";

	public static final String TOADDINSPECTD = "assistant/purchaseSet/saveinspectm";

	public static final String TOADDINSPECTDORDER = "assistant/purchaseSet/saveinspectmorder";

	public static final String UPDATEINSPECTDORDER = "assistant/purchaseSet/updateinspect";

	public static final String ENTERINSPECTDORDER = "assistant/purchaseSet/enterinspect";
	
	public static final String ENTERINORDR = "assistant/purchaseSet/enterinordr";

	public static final String SELECTINSPECTM = "assistant/purchaseSet/selectinspectm";

	public static final String TOADDINSPECTDORDERFROMINVOICE = "assistant/purchaseSet/saveinspectmorderByfahuo";
	
}
