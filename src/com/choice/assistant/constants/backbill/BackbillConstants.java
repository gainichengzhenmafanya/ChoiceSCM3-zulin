package com.choice.assistant.constants.backbill;

public class BackbillConstants {
	
	

	public static final String TABLEBACKBILLM = "assistant/backbill/tablebackbillm";

	public static final String TABLEBACKBILLD = "assistant/backbill/tablebackbilld";

	public static final String TOSAVEBACKBILL = "assistant/backbill/savebackbill";

	public static final String TOSAVEBACKBILLBYYH = "assistant/backbill/savebackbillbyyh";

	public static final String TOUPDATEACKBILL = "assistant/backbill/updatebackbill";

	public static final String TOENTERACKBILL = "assistant/backbill/enterbackbill";
	
}
