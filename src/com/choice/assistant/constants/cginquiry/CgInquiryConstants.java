package com.choice.assistant.constants.cginquiry;

public class CgInquiryConstants {

	public static final String TABLE_CGINQUIRY = "assistant/cginquiry/tablecginquiry";
	
	public static final String SAVE_CGINQUIRY = "assistant/cginquiry/savecgInquiry";
	
	public static final String UPDATE_CGINQUIRY = "assistant/cginquiry/updatecgInquiry";

	public static final String PRICE_ANALYSIS_CHART = "assistant/cginquiry/priceAnalysisChart";

}
