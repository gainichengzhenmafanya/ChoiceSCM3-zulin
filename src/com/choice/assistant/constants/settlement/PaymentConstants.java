package com.choice.assistant.constants.settlement;

/**
 * 付款单
 * Created by mc on 14-10-31.
 */
public class PaymentConstants {
    /**
     * 已开发票
     */
    public static final int IISORINVOIVE=2;
    /**
     * 未开发票
     */
    public static final int UNIISORINVOIVE=1;
    /**
     * 已付款
     */
    public static final int IPAYMENTSTATE=2;
    /**
     * 未付款
     */
    public static final int UNIPAYMENTSTATE=1;
    /**
     * 支付未结束
     */
    public static final int UNENDIPAYMENTSTATE=3;


    /**
     * 付款单主界面
     */
    public static final String LIST="assistant/settlement/payment/list";
    /**
     * 付款单子界面
     */
    public static final String BOTTOM="assistant/settlement/payment/bottom";
    /**
     * 发票申请
     */
    public static final String APPLYINVOICE="assistant/settlement/payment/applyinvoice";
    /**
     * 发票录入
     */
    public static final String COLLINVOICE="assistant/settlement/payment/collinvoice";
    /**
     * 付款
     */
    public static final String PAYMENT="assistant/settlement/payment/payment";
}
