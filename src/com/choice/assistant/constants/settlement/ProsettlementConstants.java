package com.choice.assistant.constants.settlement;


/**
 * 采购结算
 * Created by mc on 14-10-29.
 */
public class ProsettlementConstants {
    /**
     * 退货单
     */
    public static final int BACKBILL=1;
    /**
     * 验货单
     */
    public static final int INSPECT=2;
    /**
     * 其他付款单
     */
    public static final int RESTSPAYMENT=3;

    /**
     * 采购结算主界面
     */
    public static final String LIST="assistant/settlement/Prosettlement/list";
    /**
     * 主界面下面部分
     */
    public static final String BOTTOM="assistant/settlement/Prosettlement/bottom";
    /**
     * 采购界面弹窗
     */
    public static final String POPWIN="assistant/settlement/Prosettlement/popwin";
}
