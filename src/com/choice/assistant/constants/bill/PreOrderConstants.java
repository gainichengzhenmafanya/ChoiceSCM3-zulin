package com.choice.assistant.constants.bill;

public class PreOrderConstants {

	public static final String ADD="assistant/prebill/addpreorder";
	
	public static final String UPDATE="assistant/prebill/updatepreorder";
	
	public static final String LIST="assistant/prebill/listpreorder";
	
	public static final String LIST2="assistant/prebill/listpreorder2";
	
	public static final String LIST_PREORDERD="assistant/prebill/listpreorderd";
	
}
