package com.choice.assistant.constants.bill;

public class PuprOrderConstants {

	public static final String TOADDPUPRORDER="assistant/bill/savepuprorder";
	
	public static final String TOADDPUPRORDERBULU="assistant/bill/savepuprorderbulu";

	public static final String TOUPDATEPUPRORDER="assistant/bill/updatepuprorder";
	
	public static final String TOUPDATEPUPRORDERBULU="assistant/bill/updatepuprorderbulu";

	public static final String TABLE_PUPRORDER="assistant/bill/tablepuprorder";
	
	public static final String TABLE_PUPRORDERD="assistant/bill/tablepuprorderd";
	
	public static final String TOSAVEPUPRORDERD="assistant/detailedlist/tosavepuprorder";
	
	public static final String TOSAVEPUPRORDERDLIST="assistant/bill/savepuprorderlist";
	
	public static final String TOSAVEPREORDERDLIST="assistant/bill/savepre2puprorder";
	
	public static final String SELECTPUPRORDERDLIST="assistant/bill/selectpuprorder";
	
	public static final String DISFENBO="assistant/bill/disfenbo";
	
}
