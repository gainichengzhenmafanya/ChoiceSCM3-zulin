package com.choice.assistant.constants.procurementtender;

public class CgTenderConstants {

	public static final String TABLE_CGTENDER = "assistant/procurementtender/tableprocurementtender";

	public static final String SAVE_CGTENDER = "assistant/procurementtender/saveprocurementtender";

	public static final String UPDATE_CGTENDER = "assistant/procurementtender/updateprocurementtender";
	//查看采购招标详情
	public static final String VIEW_CGTENDER = "assistant/procurementtender/viewprocurementtender";
	//采购招标应标详情
	public static final String LIST_CGTENDERYBINFO = "assistant/procurementtender/listProcurementTenderYb";
	//添加应标
	public static final String ADD_CGTENDERD = "assistant/procurementtender/addTenderD";
	//修改应标
	public static final String UPDATE_CGTENDERD = "assistant/procurementtender/updateTenderD";
}
