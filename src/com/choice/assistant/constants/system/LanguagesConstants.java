package com.choice.assistant.constants.system;

/**
 * 多语言处理类
 * Created by mc on 14-11-13.
 */
public class LanguagesConstants {
    /**
     * L 删除错误
     */
    public static final String DELERROR="删除失败,刪除失敗,delete error";
    /**
     * 编码重复
     */
    public static final String CODE_ERROR="编码重复！,編碼重複！,Coding repeat!";
    /**
     * 该类别或下级类别已被引用不能删除
     */
    public static final String TREE_ERROR="该类别或下级类别已被引用不能删除!,該類別或下級類別已被引用不能刪除,The category or the lower category has been quoted cannot be deleted!";
    /**
     * 报价数据为空
     */
    public static final String PURCONTRACT_NULL="报价数据为空！,報價數據為空！,Quotation data is empty!";

    /**
     * 保存出错
     */
    public static final String UPDATE_ERROR="修改出错！,修改出錯！,update the error!";
    /**
     * 保存出错
     */
    public static final String SAVE_ERROR="保存出错！,保存出錯！,Save the error!";
    /**
     * 合约已生效不能删除
     */
    public static final String PURCONTRACT_CANT_DEL="合约已生效不能删除,合約已生效不能刪除,The contract has been take effect cannot be deleted";
    /**
     *合约已生效不能审核
     */
    public static final String PURCONTRACT_AUDIT_ERROR="合约已生效不能审核，只能终止！,合約已生效不能審核，只能終止！,Contract has not audit, can only be terminated!";
    /**
     *适用门店为空
     */
    public static final String APPLY_STORE_NULL= "适用门店为空,適用門店不能為空,Apply to store is empty";
    /**
     * 供应商已被引用不能删除
     */
    public static final String SUPPLIER_QUOTED="供应商已被引用不能删除!,供應商已被引用不能刪除！,Supplier has been quoted cannot be deleted！";
    /**
     * 报价日期
     */
    public static final String QUOTATION_DATE="报价日期,報價日期,quotation date";
    /**
     * 单价
     */
    public static final String PRICE="单价,單價,price";
    /**
     * 合约报价趋势分析
     */
    public static final String CONTRACT_PRICE="合约报价趋势分析,合約報價趨勢分析,Contract price trend analysis";
    /**
     * 元
     */
    public static final String YUAN="元,元,yuan";
    /**
     * 全部
     */
    public static String ALL = "全部,全部,All";
    /**
     * 未审核
     */
    public static final String TO_AUDIT="未审核,未審核,Not audit";
    /**
     * 已审核
     */
    public static final String AUDIT="已审核,已審核,The approved";
    /**
     * 生效
     */
    public static final String TAKE_EFFECT="生效,生效,To take effect";
    /**
     * 终止
     */
    public static final String TERMINATION_OF="终止,終止,The End";

    public static final String STOP_OF="停用,停用,The End";

    public static final String DOES_NOT_HELP_INFORMATION="该功能没有帮助信息,該功能沒有幫助信息,The function does not help information";

    public static final String JMU_CUSTOMER_SERVICE="如需要帮助请咨询北京辰森世纪科技有限公司,如需要幫助請諮詢北京辰森世纪科技有限公司,If in doubt please consult ChoiceSoft customer service";

    public static final String HANDLE_FAILURE="操作失败,操作失敗,Handle Failure";

    public static final String NSCORE_ERROR="单项评分不能超过100,單項評分不能超過100,Single item grading cannot surpass 100";

    public static final String ANALYSIS_OF_MATERIALS="分析物资：,分析物資：,Analysis of materials:";
}
