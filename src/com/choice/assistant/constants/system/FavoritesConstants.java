package com.choice.assistant.constants.system;


/**
 * Created by zhb
 */
public class FavoritesConstants {
    public static final String LIST_FASTSEARCH = "assistant/favoritesJsp/listFavorites";
    public static final String SHANGPIN = "assistant/favoritesJsp/subPage/listShangPin";
	public static final String GONGYINGSHANG = "assistant/favoritesJsp/subPage/listGys";
	public static final String FSHANGPININFO = "assistant/favoritesJsp/subPage/inAuditMaterial";
	public static final String FGONGYINGSHANGINFO = "assistant/favoritesJsp/subPage/inAuditDeliver";

}
