package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class TaxRateConstants {
    public static final String LIST_TAXRATE="assistant/system/datadict/taxRate/listTaxRate";
    public static final String SAVE_TAXRATE="assistant/system/datadict/taxRate/saveTaxRate";
	public static final String CHOOSE_TAX = "assistant/system/datadict/taxRate/chooseTaxRate";

}
