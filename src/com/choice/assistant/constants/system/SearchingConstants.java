package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class SearchingConstants {
    public static final String LIST_FASTSEARCH = "assistant/searchJsp/listFastSearch";
    public static final String SHANGPIN = "assistant/searchJsp/subPage/listShangPin";
	public static final String GONGYINGSHANG = "assistant/searchJsp/subPage/listGys";
	public static final String SHANGPININFO = "assistant/searchJsp/subPage/listSpInfo";
	public static final String GONGYINGSHANGINFO = "assistant/searchJsp/subPage/listGysInfo";
	public static final String CHOOSEMALLMATERIAL = "assistant/searchJsp/chooseMallMaterial";
	public static final String APPLYTODSH = "assistant/searchJsp/applyToDshPage";

}
