package com.choice.assistant.constants.system;


/**
 * Created by mc on 14-10-18.
 */
public class CoderuleConstants {
    /**
     * 首页
     */
    public static final String LIST_CODERULE="assistant/system/coderule/list";
    /**
     * 单据号规则
     */
    public static final String CODE="assistant/system/coderule/code";
    /**
     * 编码约束规则
     */
    public static final String ORDER="assistant/system/coderule/order";

    /**
     * 招募书
     */
    public static final String RECRUIT="00000000000000000000000000000001";
    /**
     * 付款单
     */
    public static final String PAYMENT="00000000000000000000000000000002";
    /**
     * 退货单
     */
    public static final String BACKBILL="00000000000000000000000000000003";
    /**
     * 验货单
     */
    public static final String INSPECT="00000000000000000000000000000004";
    /**
     * 采购清
     */
    public static final String PURTEMPLET="00000000000000000000000000000005";
    /**
     * 采购合约
     */
    public static final String PURCONTRACT="00000000000000000000000000000006";
    /*
     *员工
     */
    /*public static final String ACCOUNT="00000000000000000000000000000007";*/
    /**
     * 组织
     */
    public static final String ORGSTRUCTURE="00000000000000000000000000000008";
    /**
     * 供应商
     */
    public static final String SUPPLIER="00000000000000000000000000000009";
    /**
     * 供应商类别
     */
    public static final String SUPPLIER_TYPE="00000000000000000000000000000010";
    /**
     * 物资信息
     */
    public static final String MATERIAL="00000000000000000000000000000011";
    /**
     * 物资类别
     */
    public static final String MATERIAL_TYPE="00000000000000000000000000000012";
    /**
     * 到货单
     */
    public static final String ARRIVAL="00000000000000000000000000000013";
    /**
     * 其他应付款
     */
    public static final String OTHERPAYABLES="00000000000000000000000000000014";
    /**
     * 报货单
     */
    public static final String CHKSTOM="00000000000000000000000000000015";

    public static enum INIT{
        MC00000000000000000000000000000001("ZM"),//招募书
        MC00000000000000000000000000000002("FK"),//付款单
        MC00000000000000000000000000000003("TH"),//退款单
        MC00000000000000000000000000000004("YH"),//验货单
        MC00000000000000000000000000000005("CG"),//采购单
        MC00000000000000000000000000000006("HY"),//采购合约
        MC00000000000000000000000000000008(" select max(viewCode) from department where acct='%s' and length(viewCode)=%s and viewCode like '%s%%'"),//物资类别
        MC00000000000000000000000000000013("DH"),//到货单
        MC00000000000000000000000000000014("QT"),//其他已付款
        MC00000000000000000000000000000015("BH"),//报货单
        MC00000000000000000000000000000012(" select max(vcode) from cscm_materialtype where acct='%s' and length(vcode)=%s and vcode like '%s%%'"),//物资类别
        MC00000000000000000000000000000010(" select max(vcode) from cscm_suppliertype where acct='%s' and length(vcode)=%s and vcode like '%s%%'");//供应商类别
        //私有成员变量，保存名称
        private String value;
        public String getValue() {
            return value;
        }
        //带参构造函数
        INIT(String value) {
            this.value = value;
        }
    }
}
