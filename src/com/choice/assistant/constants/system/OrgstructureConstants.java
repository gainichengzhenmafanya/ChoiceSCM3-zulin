package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class OrgstructureConstants {
	public static final String LIST_DATADICT="assistant/system/datadict/listDataDict";
    public static final String LIST_ORGSTRUCTURE="assistant/system/datadict/orgstructure/listOrgstructure";
    public static final String SAVE_ORGSTRUCTURE="assistant/system/datadict/orgstructure/saveOrgstructure";
	public static final String CHOOSE_ORGSTRUCTURE = "assistant/system/datadict/orgstructure/chooseOrgstructure";

}
