package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class InspectionConstants {
    public static final String LIST_INSPECTION="assistant/system/datadict/inspection/listInspection";
    public static final String SAVE_INSPECTION="assistant/system/datadict/inspection/saveInspection";
	public static final String CHOOSE_INSPECTION = "assistant/system/datadict/inspection/chooseInspection";

}
