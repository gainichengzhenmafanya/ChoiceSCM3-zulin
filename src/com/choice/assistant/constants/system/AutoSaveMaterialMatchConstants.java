package com.choice.assistant.constants.system;


/**
 * 物资匹配明细界面
 * Created by zgl
 */
public class AutoSaveMaterialMatchConstants {
    public static final String SHOWMOREMATERIALMATCH="assistant/system/materialMatch/showMoreMaterialMatch";
    public static final String SHOWMATERIALMATCH="assistant/system/materialMatch/showMaterialMatch";
    public static final String SHOWSUPPIERMATCH="assistant/system/materialMatch/showSuppierMatch";

}
