package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class CompanyConstants {
    public static final String LIST_CONTRACTTERMS="assistant/system/asstSet/listAsstSet";
    public static final String LIST_COMPANY="assistant/system/asstSet/subPage/listCompany";
	public static final String ADMINPSW = "assistant/system/asstSet/subPage/adminPsw";
	public static final String KUAIJIQI = "assistant/system/asstSet/subPage/kuaijiqi";
	public static final String UPDATEITFINFO = "assistant/system/asstSet/updateItfInfo";
	public static final String REGISTERJSP = "assistant/system/asstSet/registerJsp";
	public static final String FTPDOWNLOADJSP = "assistant/system/ftpFileDownLoad";
	public static final String TOLISTCOMPANY = "assistant/system/asstSet/listCompanyParams";
	public static final String LISTCOMPANY = "assistant/system/asstSet/listCompanyByPage";
	
	//采购员预置权限数据
	public static final String caigouyuanRoleOperateSql = "insert into role_operate(id,operateid,acct,roleid,dr,ts)";
	public static final String caigouyuanRoleOperateData = 
			"select 'role_operate','760ddfe4e644431294813e0a5c29513b','acct','pk_role','0','tstime'"+
			"union select 'role_operate','9bc8b334fa97495b9dc03717542f8de7','acct','pk_role','0','tstime'"+
			"union select 'role_operate','3c83b082692f4451ba18bba09dc77ed4','acct','pk_role','0','tstime'"+
			"union select 'role_operate','a3c3b53a86e04a93a740e96524fa1470','acct','pk_role','0','tstime'"+
			"union select 'role_operate','702886f2f52846839f8990a31c84bdc5','acct','pk_role','0','tstime'"+
			"union select 'role_operate','e7c21933274a49c196867b52b9f09fe2','acct','pk_role','0','tstime'"+
			"union select 'role_operate','ce05318d09af4e3896f30c0d135983b8','acct','pk_role','0','tstime'"+
			"union select 'role_operate','ac54bd5f93f5484bb4c84094e2f6f77b','acct','pk_role','0','tstime'"+
			"union select 'role_operate','220a846579d3444b83bdbb019716c676','acct','pk_role','0','tstime'"+
			"union select 'role_operate','358646bd7a094216b0bbcba891d17c61','acct','pk_role','0','tstime'"+
			"union select 'role_operate','731e9d58c3164df39b5e7485b43a4844','acct','pk_role','0','tstime'"+
			"union select 'role_operate','e631aca7a9c449f7a2ec7dd6e6a93902','acct','pk_role','0','tstime'"+
			"union select 'role_operate','b1efc754e6a34d008efd27cd8aacb199','acct','pk_role','0','tstime'"+
			"union select 'role_operate','4dc28c0187d34fe9a36e92adbc20438d','acct','pk_role','0','tstime'"+
			"union select 'role_operate','bde77ff4808341c3b2d070a1ce9d7426','acct','pk_role','0','tstime'"+
			"union select 'role_operate','b8e1e426dcde4e34abe97ef567084577','acct','pk_role','0','tstime'"+
			"union select 'role_operate','eca96598b6cd4bb0a9706afcd4141298','acct','pk_role','0','tstime'"+
			"union select 'role_operate','2500e488acca482eb02dc7bd65e26217','acct','pk_role','0','tstime'"+
			"union select 'role_operate','92829812be1d4ab9b509a2a0c595ac72','acct','pk_role','0','tstime'"+
			"union select 'role_operate','d2357e0b371143e0a144ed840897f478','acct','pk_role','0','tstime'"+
			"union select 'role_operate','7345c90cf602464fb8555645dbc61f12','acct','pk_role','0','tstime'"+
			"union select 'role_operate','62863a7d35df49ddbf9c0626fbdcec27','acct','pk_role','0','tstime'"+
			"union select 'role_operate','64f97d0ced3341d293669b60e1d28203','acct','pk_role','0','tstime'"+
			"union select 'role_operate','812783d35b7243cf804c29bf4c6cdbbf','acct','pk_role','0','tstime'"+
			"union select 'role_operate','37921d9d75d14ada936bcf96753ceed8','acct','pk_role','0','tstime'"+
			"union select 'role_operate','41acf45c11994192ad80bb5efc7c6759','acct','pk_role','0','tstime'"+
			"union select 'role_operate','acafe90f13264d45a3b7f2ac0c390ac0','acct','pk_role','0','tstime'";
		//采购经理预置权限数据
		public static final String caigoujingliRoleOperateSql = "insert into role_operate(id,operateid,acct,roleid,dr,ts)";
		public static final String caigoujingliRoleOperateData = 
				"select 'role_operate','111960e19e5e4dd5bdba24229b3acd32','acct','pk_role','0','tstime'"+
						"union select 'role_operate','b1efc754e6a34d008efd27cd8aacb199','acct','pk_role','0','tstime'"+
						"union select 'role_operate','9bc8b334fa97495b9dc03717542f8de7','acct','pk_role','0','tstime'"+
						"union select 'role_operate','04a56521a4cd402e852dd68ab9b271e4','acct','pk_role','0','tstime'"+
						"union select 'role_operate','731e9d58c3164df39b5e7485b43a4844','acct','pk_role','0','tstime'"+
						"union select 'role_operate','20047907914a471695ef11189026d021','acct','pk_role','0','tstime'"+
						"union select 'role_operate','3c83b082692f4451ba18bba09dc77ed4','acct','pk_role','0','tstime'"+
						"union select 'role_operate','a00cc31daae146338c382f4b2eaa838b','acct','pk_role','0','tstime'"+
						"union select 'role_operate','4dc28c0187d34fe9a36e92adbc20438d','acct','pk_role','0','tstime'"+
						"union select 'role_operate','b8e1e426dcde4e34abe97ef567084577','acct','pk_role','0','tstime'"+
						"union select 'role_operate','3f16675cf0844c5a80ef67c992ed681b','acct','pk_role','0','tstime'"+
						"union select 'role_operate','760ddfe4e644431294813e0a5c29513b','acct','pk_role','0','tstime'"+
						"union select 'role_operate','e631aca7a9c449f7a2ec7dd6e6a93902','acct','pk_role','0','tstime'"+
						"union select 'role_operate','84f0aaa27601422bb39ad3473fd85ac1','acct','pk_role','0','tstime'"+
						"union select 'role_operate','43748dbe9b8d47bfa0a3cc0f1beeddfe','acct','pk_role','0','tstime'"+
						"union select 'role_operate','579b60c8e3f5424ab2c474638a0d527f','acct','pk_role','0','tstime'"+
						"union select 'role_operate','ac54bd5f93f5484bb4c84094e2f6f77b','acct','pk_role','0','tstime'"+
						"union select 'role_operate','ee5a2bdc424a4a4b8f2d0030a08f7260','acct','pk_role','0','tstime'"+
						"union select 'role_operate','4ecc0cd1139a43ee9d8d45fbc880966a','acct','pk_role','0','tstime'"+
						"union select 'role_operate','62f36e04cd7747ed8a99a476c457045d','acct','pk_role','0','tstime'"+
						"union select 'role_operate','8b25a3f90a8d4fb4bca51627f6bc762f','acct','pk_role','0','tstime'"+
						"union select 'role_operate','d2ed90ca09ba4fbea4a6d9e5521e8c1f','acct','pk_role','0','tstime'"+
						"union select 'role_operate','d4c9819579d54185a79372d0610acf1a','acct','pk_role','0','tstime'"+
						"union select 'role_operate','03a9d11e82f940aea665f4315f96c7cd','acct','pk_role','0','tstime'"+
						"union select 'role_operate','90c06dea0a264cf9943efb5332f335aa','acct','pk_role','0','tstime'"+
						"union select 'role_operate','2ad0968b35f440c79d8601fc2e854b47','acct','pk_role','0','tstime'"+
						"union select 'role_operate','4c6f607a179b47dabbccb9e9d3a49692','acct','pk_role','0','tstime'"+
						"union select 'role_operate','e7c21933274a49c196867b52b9f09fe2','acct','pk_role','0','tstime'"+
						"union select 'role_operate','2bcdb688fe3f40fc874b3a2ebb0ba290','acct','pk_role','0','tstime'"+
						"union select 'role_operate','bc001c47c80f4310ab223666216f247f','acct','pk_role','0','tstime'"+
						"union select 'role_operate','a34ff6458768479fac49a843cff61701','acct','pk_role','0','tstime'"+
						"union select 'role_operate','220a846579d3444b83bdbb019716c676','acct','pk_role','0','tstime'"+
						"union select 'role_operate','6af0277c2e8f4798886c09d91fbf1fc2','acct','pk_role','0','tstime'"+
						"union select 'role_operate','fafa18d6881544009cd754b0d46d9ae5','acct','pk_role','0','tstime'"+
						"union select 'role_operate','e2347f6f33584b5e930d6ce5d5166fba','acct','pk_role','0','tstime'"+
						"union select 'role_operate','b97095a499a4439a9e0739c0ddcdfe75','acct','pk_role','0','tstime'"+
						"union select 'role_operate','9ad0af795e9d4d338c1d47ebe3d7f62a','acct','pk_role','0','tstime'"+
						"union select 'role_operate','a93f3fb1dcba42ed816cbfd60dc28ca7','acct','pk_role','0','tstime'"+
						"union select 'role_operate','62ea93b7d2374482b64e58a93ed7fc58','acct','pk_role','0','tstime'"+
						"union select 'role_operate','b2864fcab4d14b489a722d640620a6a1','acct','pk_role','0','tstime'"+
						"union select 'role_operate','18b879f5b7734a3fb60c2d5bea61574a','acct','pk_role','0','tstime'"+
						"union select 'role_operate','0b3780c9661043aeaa861bbe22132063','acct','pk_role','0','tstime'"+
						"union select 'role_operate','eca96598b6cd4bb0a9706afcd4141298','acct','pk_role','0','tstime'"+
						"union select 'role_operate','146662ecd40d46ee8196d350f811db3c','acct','pk_role','0','tstime'"+
						"union select 'role_operate','89a5f107274a49a5b8d914ac1063ad3e','acct','pk_role','0','tstime'"+
						"union select 'role_operate','acaac461a4914a7b91f73ab3058cb58f','acct','pk_role','0','tstime'"+
						"union select 'role_operate','a3c3b53a86e04a93a740e96524fa1470','acct','pk_role','0','tstime'"+
						"union select 'role_operate','28e24b0be31949739ce4b0fd18bcc199','acct','pk_role','0','tstime'"+
						"union select 'role_operate','d029dd8cf403441582775c3bf86358c2','acct','pk_role','0','tstime'"+
						"union select 'role_operate','2500e488acca482eb02dc7bd65e26217','acct','pk_role','0','tstime'"+
						"union select 'role_operate','92829812be1d4ab9b509a2a0c595ac72','acct','pk_role','0','tstime'"+
						"union select 'role_operate','d2357e0b371143e0a144ed840897f478','acct','pk_role','0','tstime'"+
						"union select 'role_operate','7345c90cf602464fb8555645dbc61f12','acct','pk_role','0','tstime'"+
						"union select 'role_operate','62863a7d35df49ddbf9c0626fbdcec27','acct','pk_role','0','tstime'"+
						"union select 'role_operate','64f97d0ced3341d293669b60e1d28203','acct','pk_role','0','tstime'"+
						"union select 'role_operate','812783d35b7243cf804c29bf4c6cdbbf','acct','pk_role','0','tstime'"+
						"union select 'role_operate','37921d9d75d14ada936bcf96753ceed8','acct','pk_role','0','tstime'"+
						"union select 'role_operate','41acf45c11994192ad80bb5efc7c6759','acct','pk_role','0','tstime'"+
						"union select 'role_operate','acafe90f13264d45a3b7f2ac0c390ac0','acct','pk_role','0','tstime'"+
						"union select 'role_operate','50b9bc82651f4e9e8916c096b70726f1','acct','pk_role','0','tstime'"+
						"union select 'role_operate','28d3c9324c9a4c508f5c730d1a2bf79d','acct','pk_role','0','tstime'"+
						"union select 'role_operate','54fe39bdfca64788bd6664df1f206989','acct','pk_role','0','tstime'"+
						"union select 'role_operate','8cd3d79755c14f8b9e63b0b73634f616','acct','pk_role','0','tstime'"+
						"union select 'role_operate','ac6fe3a359f9447a87bdf2c71644ddfb','acct','pk_role','0','tstime'"+
						"union select 'role_operate','aec2c179c6ba4d4690ba0d0a63eba1ee','acct','pk_role','0','tstime'"+
						"union select 'role_operate','df30b47507f249d688df09873e24bfdf','acct','pk_role','0','tstime'"+
						"union select 'role_operate','12d9bbc65c11430fb537b5dedc4f224e','acct','pk_role','0','tstime'"+
						"union select 'role_operate','8c6d4d2eb8ad4ab7bb4b3ec7c27d0875','acct','pk_role','0','tstime'"+
						"union select 'role_operate','b34b32f7514d4a319dbd2055afd56f42','acct','pk_role','0','tstime'"+
						"union select 'role_operate','408d331fcc1f4ac8a3676d82085e5626','acct','pk_role','0','tstime'"+
						"union select 'role_operate','15498dd6a54a4eb5bbb99bd624363a74','acct','pk_role','0','tstime'"+
						"union select 'role_operate','6402588c392b4d48b4d0bee8edb16df9','acct','pk_role','0','tstime'"+
						"union select 'role_operate','bde77ff4808341c3b2d070a1ce9d7426','acct','pk_role','0','tstime'"+
						"union select 'role_operate','ce05318d09af4e3896f30c0d135983b8','acct','pk_role','0','tstime'"+
						"union select 'role_operate','702886f2f52846839f8990a31c84bdc5','acct','pk_role','0','tstime'"+
						"union select 'role_operate','358646bd7a094216b0bbcba891d17c61','acct','pk_role','0','tstime'";
}
