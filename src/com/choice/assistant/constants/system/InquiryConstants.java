package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class InquiryConstants {
    public static final String LIST_INQUIRY="assistant/system/datadict/inquiry/listInquiry";
    public static final String SAVE_INQUIRY="assistant/system/datadict/inquiry/saveInquiry";
	public static final String CHOOSE_INQUIRY = "assistant/system/datadict/inquiry/chooseInquiry";

}
