package com.choice.assistant.constants.system;

public class SysParamConstants {
	
	// 系统参数
	public static final String SYS = "0";
	// 企业参数
	public static final String SCM = "1";
	// 门店参数
	public static final String SBOH = "2";
	// 用户参数
	public static final String USER = "3";
}
