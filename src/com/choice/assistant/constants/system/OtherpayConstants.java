package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class OtherpayConstants {
    public static final String LIST_OTHERPAY="assistant/system/datadict/otherpay/listOtherpay";
    public static final String SAVE_OTHERPAY="assistant/system/datadict/otherpay/saveOtherpay";
	public static final String CHOOSE_OTHERPAY = "assistant/system/datadict/otherpay/chooseOtherpay";

}
