package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class ContracttermsConstants {
    public static final String LIST_CONTRACTTERMS="assistant/system/datadict/contractterms/listContractterms";
    public static final String SAVE_CONTRACTTERMS="assistant/system/datadict/contractterms/saveContractterms";
	public static final String CHOOSE_CONTRACTTERMS = "assistant/system/datadict/contractterms/chooseContractterms";

}
