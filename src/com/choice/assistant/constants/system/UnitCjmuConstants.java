package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class UnitCjmuConstants {
    public static final String LIST_UNITCJMU="assistant/system/datadict/unitCjmu/listUnitCjmu";
    public static final String SAVE_UNITCJMU="assistant/system/datadict/unitCjmu/saveUnitCjmu";
	public static final String CHOOSE_UNITCJMU = "assistant/system/datadict/unitCjmu/chooseUnitCjmu";

}
