package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class PayMethodConstants {
    public static final String LIST_PAYMETHOD="assistant/system/datadict/payMethod/listPayMethod";
    public static final String SAVE_PAYMETHOD="assistant/system/datadict/payMethod/savePayMethod";
	public static final String CHOOSE_PAYMETHOD = "assistant/system/datadict/payMethod/choosePayMethod";

}
