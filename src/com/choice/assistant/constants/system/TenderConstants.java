package com.choice.assistant.constants.system;


/**
 * Created by zgl
 */
public class TenderConstants {
    public static final String LIST_INQUIRY="assistant/system/datadict/tender/listTender";
    public static final String SAVE_INQUIRY="assistant/system/datadict/tender/saveTender";
	public static final String CHOOSE_Tender = "assistant/system/datadict/tender/chooseTender";

}
