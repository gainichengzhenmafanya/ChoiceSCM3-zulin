package com.choice.assistant.constants.system;

/**
 * 常见问题
 * Created by mc on 15/2/3.
 */
public class ProblemsConstants {
    /**
     * 常见问题显示界面
     */
    public static final String PROBLEMS_LIST="assistant/system/problems/list";
    /**
     * 常见问题 新政修改界面
     */
    public static final String PROBLEMS_ADD_OR_UPDATE="assistant/system/problems/updateProblems";
    /**
     * 常见问题 显示
     */
    public static final String PROBLEMS_SHOW="assistant/system/problems/showProblems";
}
