package com.choice.assistant.constants.detailedlist;

public class PurtempletConstants {
	
	public static final String SELECTPURTEMPLETM = "assistant/detailedlist/selectPurtempletm";
	
	public static final String LISTPURTEMPLETM = "assistant/detailedlist/listPurtempletm";
	
	public static final String LISTPURTEMPLETD = "assistant/detailedlist/listPurtempletd";
	
	public static final String ADDPURTEMPLETD = "assistant/detailedlist/addPurtempletm";
	
	public static final String UPDATEPURTEMPLETD = "assistant/detailedlist/updatePurtempletm";
	
}
