package com.choice.assistant.constants.material;

/**
 * 供应商管理
 * @author wangchao
 *
 */
public class MaterialConstants {

	public static final String LIST_MATERIALTYPE="assistant/material/tablematerialtype";
	
	public static final String TABLE_MATERIALTYPE="assistant/material/listmaterialtype";
	
	public static final String ADD_MATERIALTYPE="assistant/material/savematerialtype";
	
	public static final String UPDATE_MATERIALTYPE="assistant/material/updatematerialtype";

	public static final String LIST_MATERIAL="assistant/material/tablematerial";

	public static final String LIST_LINK_MATERIAL="assistant/material/jmumaterial/tablematerial";

	public static final String TABLE_MATERIAL="assistant/material/listmaterial";

	public static final String TABLE_LINK_MATERIAL="assistant/material/jmumaterial/listmaterial";

	public static final String ADD_MATERIAL="assistant/material/savematerial";
	
	public static final String UPDATE_MATERIAL="assistant/material/updatematerial";
	
	public static final String TOSAVEMATERIAL="assistant/material/jmumaterial/tosavematerial";

	public static final String SELECT_MATERIAL="assistant/material/selectmaterial";

	public static final String SELECT_MATERIAL_JMU="assistant/material/selectjmumaterial";

	public static final String JMUMATERIALLIST="assistant/material/jmumaterial/jmumaterial";
	
	public static final String IMPORT_SUPPLY="assistant/material/importSupply";//导入页面
	
	public static final String IMPORT_RESULT="orientation/orientationStudentManagement/importResult";//导入结果
	
	public static final String TOMALLMATERIAL = "assistant/material/tomallmaterial";
	
	public static final String SELECTMATERIALTYPE="assistant/material/materialtree";//物资选择树
	
	//测试物资选择框
	public static final String MATERIAL="assistant/material/material";
	//物资类别授权页面
	public static final String CHOOSE_MATERIALTYPESQ = "assistant/material/materialTypeSq";
	
	public static final String MATERIALIREPORT = "/report/assistant/basedata/material.jasper";
	//翻页选择物资主页面
	public static final String SELECTMATERIALBYPAGE = "assistant/material/selectMaterialByPage/tablematerial";
	public static final String SELECTMATERIALBYPAGELIST = "assistant/material/selectMaterialByPage/listmaterial";
	//翻页批量选择物资页面
	public static final String ADD_SUPPLYBATCH="assistant/material/selectMaterialByPage/addSupplyBatch";
	//普通选择物资页面
	public static final String SELECT_SUPPLY = "assistant/material/selectSupply";
	
	public static final String SELECT_TABLE_SUPPLY="assistant/material/selectTableSupply";
	
}
