package com.choice.assistant.constants.supplier;
/**
 * 供应商招募
 * @author zhb
 *
 */
public class SupplierRecruitmentConstants {
	/**
     * 供应商招募
     */
	public static final String SRMLIST="assistant/supplier/SupplierRecruitment/srmlist";
	/**
	 * 供应商招募pk
	 */
	public static final String SRMCHILDLIST="assistant/supplier/SupplierRecruitment/srmchildlist";
	/**
	 * 供应商招募页面跳转
	 */
	public static final String RMINSERTORUPDATE="assistant/supplier/SupplierRecruitment/rminsertorupdate";
	
	//采购招募应募详情
	public static final String LIST_SUPPLIERTOINFO = "assistant/supplier/SupplierRecruitment/listSupplierTO";
	//添加应募
	public static final String ADD_SUPPLIERTO = "assistant/supplier/SupplierRecruitment/addSupplierTO";
	//修改应募
	public static final String UPDATE_SUPPLIERTO = "assistant/supplier/SupplierRecruitment/updateSupplierTO";
}
