package com.choice.assistant.constants.supplier;

/**
 * 供应商审核
 */
public class AuditConstants {
    /**
     * 主界面
     */
    public static final String LIST="assistant/supplier/audit/list";
    /**
     * 关联界面下部
     */
    public static final String LINK_SUPPLIERBOTTOM="assistant/supplier/audit/listbottom";
    /**
     * 关联界面
     */
    public static final String LINK_SUPPLIER="assistant/supplier/audit/linkSupplier";
    /**
     * 添加供应商
     */
    public static final String ADD_SUPPLIER="assistant/supplier/skip";
}
