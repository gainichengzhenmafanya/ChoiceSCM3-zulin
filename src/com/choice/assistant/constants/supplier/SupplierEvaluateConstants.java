package com.choice.assistant.constants.supplier;

/**
 * 供应商评价
 * Created by mc on 14-12-4.
 */
public class SupplierEvaluateConstants {
    /**
     * 供应商评价 显示
     */
    public static final String LIST="assistant/supplier/evaluate/list";
    /**
     * 供应商评价 下面
     */
    public static final String LISTBOTTOM="assistant/supplier/evaluate/listbottom";
    /**
     * 供应商评价体系
     */
    public static final String ADDEVALUATE="assistant/supplier/evaluate/addEvaluate";
    /**
     * 供应商评价界面
     */
    public static final String EVALUATE="assistant/supplier/evaluate/evaluate";
}
