package com.choice.assistant.constants.supplier;

/**
 * 供应商管理
 * @author wangchao
 *
 */
public class SupplierConstants {

    /**
     * 供应商列表
     */
	public static final String LIST="assistant/supplier/list";
    /**
     * 供应商跳转
     */
	public static final String SKIP="assistant/supplier/skip";
    /**
     * 页签界面
     */
	public static final String LISTBOTTOM="assistant/supplier/listbottom";
    /**
     * 供应商类别
     */
	public static final String TYPE="assistant/supplier/type/type";
    /**
     * 供应商类别修改
     */
	public static final String TYPEEDIT="assistant/supplier/type/typeEdit";
    /**
     * 供应商类别新增界面
     */
	public static final String TYPEADD="assistant/supplier/type/typeAdd";
    /**
     * 供应商显示
     */
	public static final String TYPSHOW="assistant/supplier/type/typeShow";

    /**
     * 共用供应商
     */
	public static final String SELECT_DELIVER="assistant/supplier/selectSupplier";

    /**
     * 共用供应商
     */
	public static final String SELECT_DELIVER_FROMJMU="assistant/supplier/selectjmuSupplier";

    /**
     * 共用供应商测试页
     */
    public static final String LIST_DELIVER="assistant/supplier/supplier";
    /**
     * 关联物资
     */
    public static final String RELEVANCE_MATERIAL="assistant/supplier/relevanceMaterial";
    /**
     * 供应商列表打印
     */
	public static final String SUPPLIERIREPORT = "/report/assistant/basedata/supplierList.jasper";
	/**
	 * 供应商类别树
	 */
	public static final String CHOOSE_SUPPLIERTYPETREE = "assistant/supplier/type/supplierTypeTree";
	public static final String SELECT_ONEDELIVERS2 = "assistant/supplier/selectOneDeliver2";//选择供应商新页面，左右分栏
	public static final String TABLE_DELIVERS2="assistant/supplier/tableDeliver2";//选择供应商右侧栏

}
