package com.choice.assistant.constants.supplier;

import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.framework.util.Local;

/**
 * 采购合约
 * Created by mc on 14-10-24.
 */
public class PurcontractConstants {
    /**
     * 未审核
     */
    public static final int TO_AUDIT=1;
    /**
     * 已审核
     */
    public static final int AUDIT=2;
    /**
     * 生效
     */
    public static final int TAKE_EFFECT=3;
    /**
     * 终止
     */
    public static final int TERMINATION_OF=4;


    /**
     * 新增界面
     */
    public static final String SKIP="assistant/supplier/purcontract/skip";
    /**
     * 上面界面
     */
    public static final String LIST="assistant/supplier/purcontract/list";
    /**
     * 主界面下面部分
     */
    public static final String LIST_BOTTOM="assistant/supplier/purcontract/listbottom";
    /**
     * 快速选择物品
     */
    public static final String MATERIAL="assistant/supplier/purcontract/quickMal";
    /**
     * 显示所有物料
     */
    public static final String ALLMAL="assistant/supplier/purcontract/allMal";
    /**
     * 门店界面
     */
    public static final String ALLSTORE="assistant/supplier/purcontract/allStore";
    /**
     * 显示适用门店
     */
    public static final String STORELIST="assistant/supplier/purcontract/storelist";
    /**
     * 显示适用门店
     */
    public static final String CONTRACTQUOTELIST="assistant/supplier/purcontract/contractQuoteList";
    /**
     * 采购合约-基本信息
     */
	public static final String BASEIREPORT = "/report/assistant/Purcontract/PurcontractBase.jasper";
	/**
	 * 采购合约-合约条款
	 */
	public static final String CLAUIREPORT =  "/report/assistant/Purcontract/PurcontractClau.jasper";
	/**
	 * 采购合约-合约大事记
	 */
	public static final String EVENTIERIREPORT =  "/report/assistant/Purcontract/PurcontractEvent.jasper";

    public enum ISTART{
        S1(Local.show(LanguagesConstants.TO_AUDIT)),
        S2(Local.show(LanguagesConstants.TAKE_EFFECT)),
        S3(Local.show(LanguagesConstants.TAKE_EFFECT)),
        S4(Local.show(LanguagesConstants.TERMINATION_OF)),
        E1(Local.show(LanguagesConstants.TO_AUDIT)),
        E2(Local.show(LanguagesConstants.AUDIT)),
        E3(Local.show(LanguagesConstants.STOP_OF));
        private String value;
        public String getValue() {
            return value;
        }
        ISTART(String value) {
            this.value = value;
        }
    }
}
