package com.choice.assistant.constants.report;

public class ReportUrlConstant {
	
	/***************************************采购明细分析*********************************************/
	public static final String REPORT_SHOW_PUPRORDERDETAIL="assistant/report/purchase/puprOrderDetail";
	public static final String REPORT_URL_PUPRORDERDETAIL="/report/assistant/report/CaiGouMingXiChaXun.jasper";
	/***************************************物资汇总查询*********************************************/
	public static final String REPORT_SHOW_MATERIALSUMMARY="assistant/report/purchase/materialSummary";
	public static final String REPORT_URL_MATERIALSUMMARY="/report/assistant/report/MaterialSummary.jasper";
	/***************************************物资类别汇总*********************************************/
	public static final String REPORT_SHOW_MATERIALTYPESUMMARY="assistant/report/purchase/materialTypeSummary";
	public static final String REPORT_URL_MATERIALTYPESUMMARY="";
	/***************************************验货明细分析*********************************************/
	public static final String REPORT_SHOW_INSPECTDETAIL="assistant/report/inspect/inspectDetail";
	public static final String REPORT_URL_INSPECTDETAIL="/report/assistant/report/YanHuoMingXiChaXun.jasper";
	/***************************************验货物资汇总查询*********************************************/
	public static final String REPORT_SHOW_INSPECTMATERIALSUMMARY="assistant/report/inspect/inspectMaterialSummary";
	public static final String REPORT_URL_INSPECTMATERIALSUMMARY="/report/assistant/report/YanHuoHuiZongChaXun.jasper";
	/***************************************验货物资类别汇总*********************************************/
	public static final String REPORT_SHOW_INSPECTMATERIALTYPESUMMARY="assistant/report/inspect/inspectMaterialTypeSummary";
	public static final String REPORT_URL_INSPECTMATERIALTYPESUMMARY="";
    /***************************************合约报价趋势分析*********************************************/
    /**
     * 合约价格趋势分析 主界面
     */
    public static final String CONTRACTPRICETREND="assistant/report/analysis/contractPriceTrend";
    /**
     * 物资界面显示
     */
    public static final String MATERIAL="assistant/report/analysis/listmaterial";
    /**
     * 供应商显示界面
     */
    public static final String FIND_SUPPLIER="assistant/report/analysis/listSupplier";
    /***************************************价格异动分析*********************************************/
    /**
     * 多选树
     */
    public static final String MSELECTTREE="assistant/report/analysis/mSelectTree";
    /**
     * 价格预警分析 主界面
     */
    public static final String REPORT_SHOW_ANALYSISOFPRICECHANGES="assistant/report/analysis/analysisOfPriceChanges";
	public static final String REPORT_URL_ANALYSISOFPRICECHANGES="/report/assistant/report/AnalysisOfPriceChanges.jasper";
    /***************************************不合格进货分析*********************************************/
    /**
    * 不合格进货分析 主界面
    */
    public static final String REPORT_SHOW_UNQUALIFIEDSTOCKANALYSIS="assistant/report/analysis/unqualifiedStockAnalysis";
    public static final String REPORT_URL_UNQUALIFIEDSTOCKANALYSIS="/report/assistant/report/UnqualifiedStockAnalysis.jasper";
    /***************************************采购到货分析*********************************************/
    /**
     * 采购到货分析 主界面
     */
    public static final String REPORT_SHOW_PURCHASINGARRIVALGOODS="assistant/report/analysis/purchasingArrivalGoods";
    public static final String REPORT_URL_PURCHASINGARRIVALGOODS="/report/assistant/report/PurchasingArrivalGoods.jasper";
}
