package com.choice.assistant.constants.report;

public final class ReportStringConstant {
	private ReportStringConstant(){}
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
	
	/*************************************采购明细start******************************************************/
	public static final String BASICINFO_REPORT_PUPRORDERDETAIL = "1,2,3,4,5,6,7,8,9,10,11";  //默认选择列
	public static final String REPORT_NAME_PUPRORDERDETAIL = "caigoumingxi";  //选择表名
	public static final String REPORT_NAME_CN_PUPRORDERDETAIL = "采购明细";  //中文名
	public static final String BASICINFO_REPORT_PUPRORDERDETAIL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_PUPRORDERDETAIL = "findPuprOrderDetail";
	/*************************************采购物资汇总查询start******************************************************/
	public static final String BASICINFO_REPORT_MATERIALSUMMARY = "16,17,18,19,20,21,22,23";  //默认选择列
	public static final String REPORT_NAME_MATERIALSUMMARY = "wuzihuizong";  //选择表名
	public static final String REPORT_NAME_CN_MATERIALSUMMARY = "物资汇总查询";  //中文名
	public static final String BASICINFO_REPORT_MATERIALSUMMARY_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_MATERIALSUMMARY = "findMaterialSummary";
	/*************************************采购物资类别汇总查询start******************************************************/
	public static final String BASICINFO_REPORT_MATERIALTYPESUMMARY = "31,32,33,34,35";  //默认选择列
	public static final String REPORT_NAME_MATERIALTYPESUMMARY = "wuzileibietongji";  //选择表名
	public static final String REPORT_NAME_CN_MATERIALTYPESUMMARY = "物资类别汇总";  //中文名
	public static final String BASICINFO_REPORT_MATERIALTYPESUMMARY_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_MATERIALTYPESUMMARY = "findMaterialTypeSummary";
	/*************************************验货明细start******************************************************/
	public static final String BASICINFO_REPORT_INSPECTDETAIL = "151,152,153,154,155,156,157,158,159,160";  //默认选择列
	public static final String REPORT_NAME_INSPECTDETAIL = "yanhuomingxi";  //选择表名
	public static final String REPORT_NAME_CN_INSPECTDETAIL = "验货明细";  //中文名
	public static final String BASICINFO_REPORT_INSPECTDETAIL_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_INSPECTDETAIL = "findInspectDetail";
	/*************************************验货物资汇总查询start******************************************************/
	public static final String BASICINFO_REPORT_INSPECTMATERIALSUMMARY = "181,182,183,184,185,186,187,188";  //默认选择列
	public static final String REPORT_NAME_INSPECTMATERIALSUMMARY = "yanhuowuzihuizong";  //选择表名
	public static final String REPORT_NAME_CN_INSPECTMATERIALSUMMARY = "验货物资汇总查询";  //中文名
	public static final String BASICINFO_REPORT_INSPECTMATERIALSUMMARY_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_INSPECTMATERIALSUMMARY = "findInspectMaterialSummary";
	/*************************************验货物资类别汇总查询start******************************************************/
	public static final String BASICINFO_REPORT_INSPECTMATERIALTYPESUMMARY = "211,212,213,214,215";  //默认选择列
	public static final String REPORT_NAME_INSPECTMATERIALTYPESUMMARY = "yanhuowuzileibietongji";  //选择表名
	public static final String REPORT_NAME_CN_INSPECTMATERIALTYPESUMMARY = "验货物资类别汇总";  //中文名
	public static final String BASICINFO_REPORT_INSPECTMATERIALTYPESUMMARY_FROZEN = "";
	public static final String REPORT_QUERY_METHOD_INSPECTMATERIALTYPESUMMARY = "findInspectMaterialTypeSummary";
    /*************************************价格预警分析start******************************************************/
    public static final String BASICINFO_REPORT_ANALYSISOFPRICECHANGES = "61,62,63,64,65,66,67,68,69,70,71";  //默认选择列
    public static final String REPORT_NAME_ANALYSISOFPRICECHANGES = "AnalysisOfPriceChanges";  //选择表名
    public static final String REPORT_NAME_CN_ANALYSISOFPRICECHANGES = "采购价格异动";  //中文名
    public static final String BASICINFO_REPORT_ANALYSISOFPRICECHANGES_FROZEN = "";
    public static final String REPORT_QUERY_METHOD_ANALYSISOFPRICECHANGES = "findAnalysisOfPriceChanges";
    /*************************************不合格进货分析start******************************************************/
    public static final String BASICINFO_REPORT_UNQUALIFIEDSTOCKANALYSIS = "91,92,93,94,95,96,97";  //默认选择列
    public static final String REPORT_NAME_UNQUALIFIEDSTOCKANALYSIS = "UnqualifiedStockAnalysis";  //选择表名
    public static final String REPORT_NAME_CN_UNQUALIFIEDSTOCKANALYSIS = "不合格进货分析";  //中文名
    public static final String BASICINFO_REPORT_UNQUALIFIEDSTOCKANALYSIS_FROZEN = "";
    public static final String REPORT_QUERY_METHOD_UNQUALIFIEDSTOCKANALYSIS = "findUnqualifiedStockAnalysis";
    /*************************************供应商申购到货分析 start******************************************************/
    public static final String BASICINFO_REPORT_PURCHASINGARRIVALGOODS = "121,122,123,124,125,126,127,128";  //默认选择列
    public static final String REPORT_NAME_PURCHASINGARRIVALGOODS = "PurchasingArrivalGoods";  //选择表名
    public static final String REPORT_NAME_CN_PURCHASINGARRIVALGOODS = "供应商申购到货分析";  //中文名
    public static final String BASICINFO_REPORT_PURCHASINGARRIVALGOODS_FROZEN = "";
    public static final String REPORT_QUERY_METHOD_PURCHASINGARRIVALGOODS = "findPurchasingArrivalGoods";
	
	/**----------------------------------------报表start-------------------------------------------------------**/
	public static final String IREPORT_HTML = "share/ireport/mapSource/html";  //html页面
	public static final String IREPORT_EXCEL = "share/ireport/mapSource/excel";  //excel页面
	public static final String IREPORT_PDF = "share/ireport/mapSource/pdf";  //pdf页面
	public static final String IREPORT_WORD = "share/ireport/mapSource/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL = "share/ireport/mapSource/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF = "share/ireport/mapSource/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD = "share/ireport/mapSource/printWord";  //直接打印word页面
	//bean
	public static final String IREPORT_HTML_BEAN = "share/ireport/html";  //html页面
	public static final String IREPORT_EXCEL_BEAN = "share/ireport/excel";  //excel页面
	public static final String IREPORT_PDF_BEAN = "share/ireport/pdf";  //pdf页面
	public static final String IREPORT_WORD_BEAN = "share/ireport/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL_BEAN = "share/ireport/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF_BEAN = "share/ireport/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD_BEAN = "share/ireport/printWord";  //直接打印word页面
	/**---------------------------------------------报表end------------------------------------------------------**/
}