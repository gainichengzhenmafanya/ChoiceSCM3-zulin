package com.choice.assistant.persistence.system;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.material.ReviMaterial;
import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.system.CollDeliver;
import com.choice.assistant.domain.system.CollMaterial;

/**
 * 收藏夹接口
 * Created by zhb on 14-10-18.
 */
public interface FavoritesMapper {
    /**
     * 获取所有商品收藏数据
     * @return
     */
	public List<CollMaterial> findAllCollMaterial(CollMaterial collMaterial);
	/**
	 * 商品具体详情
	 * @param spid
	 * @return
	 */
	public CollMaterial findAllCollMaterialbyspid(CollMaterial collMaterial);
	/**
	 * 删除商品
	 * @param collMaterial
	 * @return
	 */
	public void deletespFavoritesById(CollMaterial collMaterial);
	/**
	 * 待审核物资
	 * @param reviMaterial
	 */
	public void saveAuditMaterial(ReviMaterial reviMaterial);

    /**
     * 获取所有供应商收藏数据
     * @return
     */
	public List<CollDeliver> findAllCollDeliver(CollDeliver collDeliver);
	/**
	 * 供应商具体详情
	 * @param gysid
	 * @return
	 */
	public CollDeliver findAllCollDeliverbygysid(CollDeliver collDeliver);
	/**
	 * 删除供应商
	 * @param collDeliver
	 * @return
	 */
	public void deletegysFavoritesById(CollDeliver collDeliver);
	/**
	 * 待审核供应商
	 * @param collDeliver
	 */
	public void saveAuditDeliver(Auditsupplier auditsupplier);
	/**
	 * 保存商品到收藏夹
	 * @param collMaterial
	 */
	public void saveFavorite(CollMaterial collMaterial);
	/**
	 * 保存供应商到收藏夹
	 * @param collDeliver
	 */
	public void saveGysFavorite(CollDeliver collDeliver);
	/**
	 * 判断加入待审核物资是否重复
	 * @param pk_collmaterial
	 * @param acct
	 * @return
	 */
	public String findAuditbypkss(@Param("pk_collmaterial")String pk_collmaterial,@Param("acct")String acct);
	/**
	 * 判断加入供应商待审核是否重复
	 * @param pk_colldeliver
	 * @return
	 */
	public String findAuditbypks(@Param("pk_colldeliver")String pk_colldeliver,@Param("acct")String acct); 

}
