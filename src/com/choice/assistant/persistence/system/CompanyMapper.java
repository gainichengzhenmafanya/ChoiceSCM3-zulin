package com.choice.assistant.persistence.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.system.AccountPeri;
import com.choice.assistant.domain.system.Company;
import com.choice.framework.domain.system.AccountRole;

public interface CompanyMapper {
	/**
	 * 查询全部企业信息设置
	 * @param company
	 * @return
	 */
	public List<Company> findCompanyList(Company company);
	/**
	 *  新增企业信息
	 * @param company
	 */
	public void addCompany(Company company);
	/**
	 *  修改企业信息
	 * @param company
	 */
	public void updateCompany(Company company);
	/**
	 *  启用、禁用企业信息
	 * @param company
	 */
	public void enableCompany(Company company);
	/**
	 *  删除企业信息
	 * @param company
	 */
	public void deleteCompany(Company company);
	/**
	 * 查询指定企业信息设置单条
	 * @param company
	 * @return
	 */
	public Company findCompanyByPk(Company company);
	/**
	 * 查询指定企业信息设置单条
	 * @param company
	 * @return
	 */
	public Company findCompanyByPkStatic(Company company);
	/**
	 * 验证密码是否正确
	 * @param param
	 * @return
	 */
	public String validatePwd(Map<String, Object> param);
	/**
	 * 修改密码
	 * @param param
	 */
	public void updatePwd(Map<String, Object> param);
	/**
	 * 查询会计期
	 * @param accountPeri
	 * @return
	 */
	public List<AccountPeri> queryAccountPeriList(AccountPeri accountPeri);
	/**
	 * 保存会计期
	 * @param accountPeri
	 */
	public void saveAccountPeri(AccountPeri accountPeri);
	/**
	 * 删除会计期
	 * @param company
	 */
	public void deleteAccountPeri(Company company);
	/**
	 * 修改企业登录接口的用户名、密码
	 * @param company
	 */
	public void updateIftUserInfo(Company company);
	/**
	 * 获取当前企业号最大值
	 */
	public String findMaxVcode();
	/**
	 * 新增预置角色
	 * @param sql
	 * @return
	 */
	public void saveDataYz(@Param(value="sql")String sql);
	/**
	 * admin关联默认的全部权限角色
	 * @param accountRole
	 * @return
	 */
	public void saveAccountRole(AccountRole accountRole);
	/**
	 *  修改接口登录密码
	 * @param company
	 */
	public void updateItfPwd(Company company);
	/**
	 * 根据主卡号查询企业信息
	 * @param company
	 * @return
	 */
	public Company findCompanyByVmcn(Company company);

}
