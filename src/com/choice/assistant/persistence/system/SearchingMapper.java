package com.choice.assistant.persistence.system;

import com.choice.assistant.domain.material.ReviMaterial;
import com.choice.assistant.domain.supplier.Auditsupplier;

public interface SearchingMapper {
	/**
	 * 检测该商品是否已经存到待审核物资表中
	 * @param reviMaterial
	 * @return
	 */
	public String findReviMaterialByPk(ReviMaterial reviMaterial);
	/**
	 * 把物资加入到待审核物资表
	 * @param reviMaterial
	 */
	public void saveReviMaterial(ReviMaterial reviMaterial);
	/**
	 * 检查该供应商是否已经存在
	 * @param auditsupplier
	 * @return
	 */
	public String findAuditMaterialByPk(Auditsupplier auditsupplier);
	/**
	 * 保存供应商到待审核供应商表
	 * @param auditsupplier
	 */
	public void saveAuditMaterial(Auditsupplier auditsupplier);

}
