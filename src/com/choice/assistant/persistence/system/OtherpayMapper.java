package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.Otherpay;

public interface OtherpayMapper {
	/**
	 * 查询全部其他应付款设置
	 * @param otherpay
	 * @return
	 */
	public List<Otherpay> findOtherpayList(Otherpay otherpay);
	/**
	 * 查询前10条其他应付款
	 * @param otherpay
	 * @return
	 */
	public List<Otherpay> findTopOtherpay(Otherpay otherpay);
	/**
	 *  新增其他应付款
	 * @param otherpay
	 */
	public void addOtherpay(Otherpay otherpay);
	/**
	 *  修改其他应付款
	 * @param otherpay
	 */
	public void updateOtherpay(Otherpay otherpay);
	/**
	 *  启用、禁用其他应付款
	 * @param otherpay
	 */
	public void enableOtherpay(Otherpay otherpay);
	/**
	 *  删除其他应付款
	 * @param otherpay
	 */
	public void deleteOtherpay(Otherpay otherpay);
	/**
	 * 查询指定其他应付款设置单条
	 * @param otherpay
	 * @return
	 */
	public Otherpay findOtherpayByPk(Otherpay otherpay);

}
