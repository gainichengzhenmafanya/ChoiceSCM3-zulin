package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.Unit;

public interface UnitCjmuMapper {
	/**
	 * 查询全部单位设置
	 * @param unit
	 * @return
	 */
	public List<Unit> findUnitList(Unit unit);
	/**
	 *  新增单位
	 * @param unit
	 */
	public void addUnit(Unit unit);
	/**
	 *  修改单位
	 * @param unit
	 */
	public void updateUnit(Unit unit);
	/**
	 *  启用、禁用单位
	 * @param unit
	 */
	public void enableUnit(Unit unit);
	/**
	 *  删除单位
	 * @param unit
	 */
	public void deleteUnit(Unit unit);
	/**
	 * 查询指定单位设置单条
	 * @param unit
	 * @return
	 */
	public Unit findUnitByPk(Unit unit);

}
