package com.choice.assistant.persistence.system;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.system.Contractterms;

public interface ContracttermsMapper {
	/**
	 * 查询全部合同条款类型设置
	 * @param contractterms
	 * @return
	 */
	public List<Contractterms> findContracttermsList(Contractterms contractterms);
	/**
	 *  新增合同条款类型
	 * @param contractterms
	 */
	public void addContractterms(Contractterms contractterms);
	/**
	 *  修改合同条款类型
	 * @param contractterms
	 */
	public void updateContractterms(Contractterms contractterms);
	/**
	 *  启用、禁用合同条款类型
	 * @param contractterms
	 */
	public void enableContractterms(Contractterms contractterms);
	/**
	 *  删除合同条款类型
	 * @param contractterms
	 */
	public void deleteContractterms(Contractterms contractterms);
	/**
	 * 查询指定合同条款类型设置单条
	 * @param contractterms
	 * @return
	 */
	public Contractterms findContracttermsByPk(Contractterms contractterms);
	/**
	 * 公用查询方法
	 * @param paramMap
	 * @return
	 */
	public List<Map<String, Object>> queryInfoList(Map<String, Object> paramMap);

}
