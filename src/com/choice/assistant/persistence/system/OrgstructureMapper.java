package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.OrgStructure;

public interface OrgstructureMapper {
	/**
	 * 查询全部组织类型设置
	 * @param orgStructure
	 * @return
	 */
	public List<OrgStructure> findOrgStructureList(OrgStructure orgStructure);
	/**
	 *  新增组织类型
	 * @param orgStructure
	 */
	public void addOrgStructure(OrgStructure orgStructure);
	/**
	 *  修改组织类型
	 * @param orgStructure
	 */
	public void updateOrgStructure(OrgStructure orgStructure);
	/**
	 *  启用、禁用组织类型
	 * @param orgStructure
	 */
	public void enableOrgStructure(OrgStructure orgStructure);
	/**
	 *  删除组织类型
	 * @param orgStructure
	 */
	public void deleteOrgStructure(OrgStructure orgStructure);
	/**
	 * 查询指定组织类型设置单条
	 * @param orgStructure
	 * @return
	 */
	public OrgStructure findOrgStructureByPk(OrgStructure orgStructure);
	/**
	 *  根据条件完全匹配搜索
	 * @param orgStructure
	 * @return
	 */
	public OrgStructure selectOrgstructureByCondition(OrgStructure orgStructure);

}
