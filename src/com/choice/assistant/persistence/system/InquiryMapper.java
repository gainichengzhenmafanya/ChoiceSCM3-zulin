package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.Inquiry;

public interface InquiryMapper {
	/**
	 * 查询全部询价类型设置
	 * @param inquiry
	 * @return
	 */
	public List<Inquiry> findInquiryList(Inquiry inquiry);
	/**
	 *  新增询价类型
	 * @param inquiry
	 */
	public void addInquiry(Inquiry inquiry);
	/**
	 *  修改询价类型
	 * @param inquiry
	 */
	public void updateInquiry(Inquiry inquiry);
	/**
	 *  启用、禁用询价类型
	 * @param inquiry
	 */
	public void enableInquiry(Inquiry inquiry);
	/**
	 *  删除询价类型
	 * @param inquiry
	 */
	public void deleteInquiry(Inquiry inquiry);
	/**
	 * 查询指定询价类型设置单条
	 * @param inquiry
	 * @return
	 */
	public Inquiry findInquiryByPk(Inquiry inquiry);

}
