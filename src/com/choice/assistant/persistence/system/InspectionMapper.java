package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.Inspection;

public interface InspectionMapper {
	/**
	 * 查询全部验货类型设置
	 * @param inspection
	 * @return
	 */
	public List<Inspection> findInspectionList(Inspection inspection);
	/**
	 *  新增验货类型
	 * @param inspection
	 */
	public void addInspection(Inspection inspection);
	/**
	 *  修改验货类型
	 * @param inspection
	 */
	public void updateInspection(Inspection inspection);
	/**
	 *  启用、禁用验货类型
	 * @param inspection
	 */
	public void enableInspection(Inspection inspection);
	/**
	 *  删除验货类型
	 * @param inspection
	 */
	public void deleteInspection(Inspection inspection);
	/**
	 * 查询指定验货类型设置单条
	 * @param inspection
	 * @return
	 */
	public Inspection findInspectionByPk(Inspection inspection);

}
