package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.Tender;

public interface TenderMapper {
	/**
	 * 查询全部招标类型设置
	 * @param tender
	 * @return
	 */
	public List<Tender> findTenderList(Tender tender);
	/**
	 *  新增招标类型
	 * @param tender
	 */
	public void addTender(Tender tender);
	/**
	 *  修改招标类型
	 * @param tender
	 */
	public void updateTender(Tender tender);
	/**
	 *  启用、禁用招标类型
	 * @param tender
	 */
	public void enableTender(Tender tender);
	/**
	 *  删除招标类型
	 * @param tender
	 */
	public void deleteTender(Tender tender);
	/**
	 * 查询指定招标类型设置单条
	 * @param tender
	 * @return
	 */
	public Tender findTenderByPk(Tender tender);

}
