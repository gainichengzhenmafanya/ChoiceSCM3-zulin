package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.Tax;

public interface TaxRateMapper {
	/**
	 * 查询全部税率设置
	 * @param tax
	 * @return
	 */
	public List<Tax> findTaxRateList(Tax tax);
	/**
	 *  新增税率
	 * @param tax
	 */
	public void addTaxRate(Tax tax);
	/**
	 *  修改税率
	 * @param tax
	 */
	public void updateTaxRate(Tax tax);
	/**
	 *  启用、禁用税率
	 * @param tax
	 */
	public void enableTaxRate(Tax tax);
	/**
	 *  删除税率
	 * @param tax
	 */
	public void deleteTaxRate(Tax tax);
	/**
	 * 查询指定税率设置单条
	 * @param tax
	 * @return
	 */
	public Tax findTaxRateByPk(Tax tax);

}
