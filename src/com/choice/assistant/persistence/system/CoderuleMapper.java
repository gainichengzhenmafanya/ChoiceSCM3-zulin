package com.choice.assistant.persistence.system;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.system.Coderule;

/**
 * 编码规则
 * Created by mc on 14-10-18.
 */
public interface CoderuleMapper {
    /**
     * 获取所有编码规则
     * @return
     */
    public List<Coderule> findCoderuleList(Coderule coderule);

    /**
     * 获取编码规则
     * @param coderule
     * @return
     */
    public List<Coderule> findCoderuleOrgList(Coderule coderule);

    /**
     * 根据PK获取数据
     * @return
     */
    public Coderule findCoderuleByPk(Coderule coderule);
    /**
     * 根据PK获取数据
     * @return
     */
    public Coderule findCoderuleOrgByPk(Coderule coderule);

    /**
     * 修改编码规则
     * @param coderule
     */
    public void updateCoderule(Coderule coderule);

    /**
     * 修改最大编码规则
     * @param coderule
     */
    public void updateMaxValue(Coderule coderule);

    /**
     * 保存编码规则至org表
     * @param list
     */
    public void saveCoderule(List<Coderule> list);

    public String executeSQL(@Param("sql") String sql);
    /**
     * 获取物资编码的最大值
     * @param coderule
     * @return 
     */
	public String getMaxVcodeForMaterial(Coderule coderule);
	/**
	 * 获取供应商编码的最大值
	 * @param coderule
	 * @return
	 */
	public String getMaxVcodeForSupplier(Coderule coderule);
}
