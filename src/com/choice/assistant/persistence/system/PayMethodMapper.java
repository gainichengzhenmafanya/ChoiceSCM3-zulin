package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.PayMethod;

public interface PayMethodMapper {
	/**
	 * 查询全部付款方式设置
	 * @param payMethod
	 * @return
	 */
	public List<PayMethod> findPayMethodList(PayMethod payMethod);
	/**
	 * 查询前10条付款方式
	 * @param payMethod
	 * @return
	 */
	public List<PayMethod> findTopPayMethod(PayMethod payMethod);
	/**
	 *  新增付款方式
	 * @param payMethod
	 */
	public void addPayMethod(PayMethod payMethod);
	/**
	 *  修改付款方式
	 * @param payMethod
	 */
	public void updatePayMethod(PayMethod payMethod);
	/**
	 *  启用、禁用付款方式
	 * @param payMethod
	 */
	public void enablePayMethod(PayMethod payMethod);
	/**
	 *  删除付款方式
	 * @param payMethod
	 */
	public void deletePayMethod(PayMethod payMethod);
	/**
	 * 查询指定付款方式设置单条
	 * @param payMethod
	 * @return
	 */
	public PayMethod findPayMethodByPk(PayMethod payMethod);

}
