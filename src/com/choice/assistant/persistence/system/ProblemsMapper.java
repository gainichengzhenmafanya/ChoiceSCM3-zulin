package com.choice.assistant.persistence.system;

import java.util.List;

import com.choice.assistant.domain.system.Problems;

/**
 * 常见问题
 * Created by mc on 15/2/3.
 */
public interface ProblemsMapper {
    /**
     * 查询常见问题
     * @param problems
     * @return
     */
    public List<Problems> findProblems(Problems problems);

    /**
     * 常见问题新增
     * @param problems
     * @return
     */
    public Integer addProblems(Problems problems);

    /**
     * 常见问题删除
     * @param ids
     * @return
     */
    public Integer delProblems(List<String> ids);

    /**
     * 修改常见问题
     * @param problems
     * @return
     */
    public Integer updateProblems(Problems problems);
}
