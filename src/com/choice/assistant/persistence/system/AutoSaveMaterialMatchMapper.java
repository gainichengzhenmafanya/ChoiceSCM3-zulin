package com.choice.assistant.persistence.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.system.Company;

public interface AutoSaveMaterialMatchMapper {
	
	/**
	 * 查询该企业采购数量前5的物资名称、编码
	 * @param comp
	 * @return
	 */
	public List<Map<String, Object>> queryTotalMax5Material(Company comp);
	/**
	 *  删除之前匹配的物资匹配数据
	 * @param comp
	 */
	public void deleteMaterialMatchByPkgroup(Company comp);
	/**
	 *  插入查询出的数据
	 * @param listInsert
	 */
	public void insertMaterialMatch(@Param(value="list")List<Map<String, Object>> listInsert);

}
