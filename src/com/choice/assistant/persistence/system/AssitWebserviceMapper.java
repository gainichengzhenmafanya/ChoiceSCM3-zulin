package com.choice.assistant.persistence.system;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.material.Material;
import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.scm.domain.Positn;

public interface AssitWebserviceMapper {

	
	/**
	 * 调用接口成功后更新供应商信息
	 * @param supplier
	 */
	public void updateSupplierCjmu(Supplier supplier);
	/**
	 * 调用接口成功后更新物资
	 * @param materiald
	 */
	public void updateMaterialCjmu(MaterialScope materiald);

	/**
	 * 根据物资查询物资对应供应商
	 * @param material
	 * @return
	 */
	public List<Map<String, Object>> queryAllMaterial(Material material);
	/**
	 * 查询所有未上传的订单--手动
	 * @param puprOrder
	 * @return
	 */
	public List<Map<String, Object>> queryAllPuprOrders(PuprOrder puprOrder);
	/**
	 * 根据订单主键查询订单明细
	 * @param puprOrder
	 * @return
	 */
	public List<Map<String, Object>> queryPuprdByPk(PuprOrder puprOrder);
	/**
	 * 更新订单状态
	 * @param puprOrderdd
	 */
	public void updatePuprOrdr(PuprOrder puprOrderdd);
	/**
	 * 门店地址上传接口
	 * @param departmentd
	 */
	public void uploadAddress(Positn positn);
	/**
	 * 查询erp上传的订单明细及对应商城物资关系
	 * @param puprOrder
	 * @return
	 */
	public List<Map<String, Object>> queryPuprdByPkAuto(PuprOrder puprOrder);

}
