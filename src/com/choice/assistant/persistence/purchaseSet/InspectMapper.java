package com.choice.assistant.persistence.purchaseSet;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.PurchaseSet.Inspectd;
import com.choice.assistant.domain.PurchaseSet.Inspectm;
import com.choice.assistant.domain.material.MaterialType;
import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Chkind;
import com.choice.scm.domain.Chkinm;


/**
 * 物资管理
 * @author wangchao
 *
 */
public interface InspectMapper {
	
	/**
	 * 查询所有的验货单主表数据
	 * @return
	 */
	public List<Inspectm> queryAllInspectm(Inspectm inspectm);
	/**
	 * 查询所有的验货单主表数据(未确认到货)
	 * @return
	 */
	public List<Inspectm> queryAllInspectmDo(Inspectm inspectm);
	
	/**
	 * 根据订单查询验货单
	 * @return
	 */
	public List<String> queryInspectmPuprorder(Inspectm inspectm);
	
	
	/**
	 * 查询验货单子表数据
	 * @return
	 */
	public List<Inspectd> queryAllInspectd(Inspectm inspectm);
	
	/**
	 * 修改验货单
	 * @param inspectm
	 * @throws CRUDException
	 */
	public void updateInspectm(Inspectm inspectm);
	
	/**
	 * 回写验货单验货凭证号和入库单号
	 * @param inspectm
	 * @throws CRUDException
	 */
	public void updateInspectmVounoAndChkno(Inspectm inspectm);
	
	/**
	 * 删除验货单
	 * @param inspectm
	 * @return
	 * @throws CRUDException
	 */
	public MaterialType deleteInspect(Inspectm inspectm);

	/**
	 * 新增验货单主表
	 * @param inspectm
	 */
	public void saveInspectm(Inspectm inspectm);

	/**
	 * 新增验货单子表
	 * @param inspectd
	 */
	public void saveInspectd(Inspectd inspectd);

	/**
	 * 删除验货单
	 * @param inspectm
	 * @return
	 * @throws CRUDException
	 */
	public void deleteInspectm(Inspectm inspectm);

	/**
	 * 根据主键查询指定的验货单主表、子表
	 * @param inspectm
	 * @return
	 */
	public List<Inspectm> queryAllInspectmByPk(Inspectm inspectm);
	
	/**
	 * 校验单据号是否重复
	 * @param inspectm
	 * @return
	 * @throws CRUDException
	 */
	public int findInspectByCode(Inspectm inspectm);

	/**
	 * 删除子表数据
	 * @param inspectm
	 */
	public void deleteInspectd(Inspectm inspectm);
	
	/**
	 * 跳转到修改验货单的页面===根据主键查询主表信息
	 * @param inspectm
	 * @return
	 * @throws CRUDException
	 */
	public Inspectm toUpdateInspectm(Inspectm inspectm);
	
	/**
	 * 根据验货单主键集合查询子表信息
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public List<Inspectd> selectInspectdByPK(Inspectm inspectm);
	/**
	 * 根据验货单主键查询子表信息
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public List<Inspectd> selectInspectdByPKS(@Param("acct")String acct,@Param("pk_inspect")String pk_inspect);
	
	/**
	 * 获取验货单的最大单号,按照月流水进行
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<Inspectm> selectMaxVbillno(Inspectm inspectm);
	
	/**
	 * 获取下一个验货单号
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String getNextVbillno(Inspectm inspectm);
	
	/**
	 * 查询前10条验货单
	 * @return
	 */
	public List<Inspectm> findInspectmTop(Inspectm inspectm);
	
	/**
	 * 根据验货单主表获取子表中采购订单的主键
	 * @param inspectm
	 * @return
	 */
	public List<Inspectd> queryInspectd(Inspectm inspectm);

	/**
	 * 根据采购订单主键查询验货单明细
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public List<Inspectd> queryInspectdByPuprorder(Inspectd inspectd);
	/**
	 * 根据入库单主键集合查询子表信息
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public List<Chkind> selectInOrderByPK(Chkinm chkinm);
	/**
	 * 添加入库单主表(采购系统)
	 * @param chkinm
	 */
	public void saveChkinmCg(Chkinm chkinm);
	/**
	 * 添加入库单从表(采购系统)
	 * @param chkind
	 */
	public void saveChkindCg(Chkind chkind);
	/**
	 * 根据订单号查询仓位（采购组织）信息
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public String queryOrderPos(@Param("acct")String acct,@Param("vbillno")String vbillno);
	/**
	 * 根据订单号查询单价信息
	 * @param inspectd
	 * @return
	 * @throws CRUDException
	 */
	public String queryOrderPrice(@Param("acct")String acct,@Param("vbillno")String vbillno,@Param("sp_code")String sp_code);
	/**
	 * 修改验货单验货数量与金额
	 * @param materialType
	 * @throws CRUDException
	 */
	public void updateInspectdnum(Inspectd inspectd);
	/**
	 * 修改验货单状态
	 * @param inspectm
	 * @throws CRUDException
	 */
	public void updateInspectmIstate(Inspectm inspectm);
}
