package com.choice.assistant.persistence.purchaseSet;

import java.util.List;

import com.choice.assistant.domain.PurchaseSet.Restspaymentd;
import com.choice.assistant.domain.PurchaseSet.Restspaymentm;
import com.choice.assistant.domain.material.MaterialType;
import com.choice.framework.exception.CRUDException;


/**
 * 其他应付款管理
 * @author zgl
 *
 */
public interface RestspaymentMapper {
	
	/**
	 * 查询所有的其他应付款主表数据
	 * @return
	 */
	public List<Restspaymentm> queryAllRestspaymentm(Restspaymentm restspaymentm);
	
	
	/**
	 * 查询其他应付款子表数据
	 * @return
	 */
	public List<Restspaymentd> queryAllRestspaymentd(Restspaymentm restspaymentm);
	
	/**
	 * 修改其他应付款
	 * @param materialType
	 * @throws CRUDException
	 */
	public void updateRestspaymentm(Restspaymentm restspaymentm);
	
	/**
	 * 删除其他应付款
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public MaterialType deleteRestspayment(Restspaymentm restspaymentm);

	/**
	 * 新增其他应付款主表
	 * @param restspaymentm
	 */
	public void saveRestspaymentm(Restspaymentm restspaymentm);

	/**
	 * 新增其他应付款子表
	 * @param inspectd
	 */
	public void saveRestspaymentd(Restspaymentd inspectd);

	/**
	 * 根据主键查询指定的其他应付款主表、子表
	 * @param restspaymentm
	 * @return
	 */
	public List<Restspaymentm> queryAllRestspaymentmByPk(Restspaymentm restspaymentm);

	/**
	 * 删除子表数据
	 * @param restspaymentm
	 */
	public void deleteRestspaymentd(Restspaymentm restspaymentm);
	
	
}
