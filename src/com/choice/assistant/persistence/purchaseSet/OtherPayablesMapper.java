package com.choice.assistant.persistence.purchaseSet;

import java.util.List;

import com.choice.assistant.domain.PurchaseSet.OtherPayablesd;
import com.choice.assistant.domain.PurchaseSet.OtherPayablesm;
import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Chkinm;

/**
 * 其他应付款管理
 * @author wangchao
 *
 */
public interface OtherPayablesMapper {

	/**
	 * 查询所有的其他应付款主表
	 * @param OtherPayablesm
	 * @return
	 */
	public List<OtherPayablesm> queryAllOtherPayablesm(OtherPayablesm OtherPayablesm);
	
	/**
	 * 根据主表ID查询其他应付款子表
	 * @param OtherPayablesd
	 * @return
	 */
	public List<OtherPayablesd> queryAllOtherPayablesd(OtherPayablesd OtherPayablesd);
	
	/**
	 * 添加其他应付款
	 * @param OtherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void addOtherPayablesm(OtherPayablesm OtherPayablesm);
	
	/**
	 * 添加其他应付款子表
	 * @param OtherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void addOtherPayablesd(OtherPayablesd OtherPayablesd);
	
	/**
	 * 删除其他应付款
	 * @param ids
	 * @throws CRUDException
	 */
	public void deleteOtherPayablesm(OtherPayablesm OtherPayablesm);
	
	/**
	 * 根据主键查询其他应付款主表信息
	 * @param OtherPayablesm
	 * @return
	 */
	public OtherPayablesm queryOtherpayablesmByPK(OtherPayablesm OtherPayablesm);
	
	/**
	 * 修改其他应付款
	 * @param OtherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void updateOtherPayablesm(OtherPayablesm OtherPayablesm);
	
	/**
	 * 删除其他应付款子表
	 * @param ids
	 * @throws CRUDException
	 */
	public void deleteOtherpayablesd(OtherPayablesm OtherPayablesm);
	/**
	 * 查询所有的入库单主表数据
	 * @return
	 */
	public List<Chkinm> queryAllInOrder(Chkinm chkinm);
	/**
	 * 查询前10条入库单
	 * @return
	 */
	public List<Chkinm> findInOrderTop(Chkinm chkinm);
	
}
