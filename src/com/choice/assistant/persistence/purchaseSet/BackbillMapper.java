package com.choice.assistant.persistence.purchaseSet;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.PurchaseSet.Backbilld;
import com.choice.assistant.domain.PurchaseSet.Backbillm;
import com.choice.assistant.domain.material.MaterialType;
import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Chkind;


/**
 * 物资管理
 * @author wangchao
 *
 */
public interface BackbillMapper {
	
	/**
	 * 查询所有的退货单主表数据
	 * @return
	 */
	public List<Backbillm> queryAllBackbillm(Backbillm backbillm);
	
	/**
	 * 根据验货单查询所有退货单
	 * @param backbillm
	 * @return
	 */
	public List<String> queryBackbillmInspect(Backbillm backbillm);
	
	/**
	 * 查询退货单子表数据
	 * @return
	 */
	public List<Backbilld> queryAllBackbilld(Backbillm backbillm);
	
	/**
	 * 修改退货单
	 * @param materialType
	 * @throws CRUDException
	 */
	public void updateBackbillm(Backbillm backbillm);
	
	/**
	 * 删除退货单
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public MaterialType deleteBackbill(Backbillm backbillm);

	/**
	 * 新增退货单主表
	 * @param backbillm
	 */
	public void saveBackbillm(Backbillm backbillm);

	/**
	 * 新增退货单子表
	 * @param inspectd
	 */
	public void saveBackbilld(Backbilld backbilld);
	
	/**
	 * 校验单据号是否重复
	 * @param backbillm
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public int findBackbillByCode(Backbillm backbillm);
	
	/**
	 * 跳转到修改页面==查询主表信息
	 * @param backbillm
	 * @return
	 * @throws CRUDException
	 */
	public Backbillm toUpdateBackbill(Backbillm backbillm);
	
	/**
	 * 跳转到修改页面==查询子表信息
	 * @param backbillm
	 * @return
	 * @throws CRUDException
	 */
	public List<Backbilld> toUpdateBackbilld(Backbillm backbillm);
	
	/**
	 * 删除退货单
	 * @param backbillm
	 * @throws CRUDException
	 */
	public void deleteBackBillm(Backbillm backbillm);
	
	/**
	 * 根据主键查询指定的退货单主表、子表
	 * @param backbillm
	 * @return
	 */
	public List<Backbillm> queryAllBackbillmByPk(Backbillm backbillm);

	/**
	 * 删除子表数据
	 * @param backbillm
	 */
	public void deleteBackbilld(Backbillm backbillm);
	
	/**
	 * 获取退货单的最大单号,按照月流水进行
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<Backbillm> selectMaxVbillno(Backbillm backbillm);

	/**
	 * 查询退货单明细中的验货单主键
	 * @param backbillmd
	 * @return
	 */
	public List<Map<String, Object>> selectBackbilldForPkinspect(Backbillm backbillmd);
	
	/**
	 * 查询验货单明细中的订单主键、订单号
	 * @param string 
	 * @param pk_
	 * @return
	 */
	public List<Map<String, Object>> selectMallSn(@Param(value="pk_inspect")String pk_inspect, @Param(value="acct")String acct);

	/**
	 * 查询退货明细
	 * @param map
	 * @return 
	 */
	public List<Map<String, Object>> queryBackMaterial(Map<String, Object> map);

	/**
	 * 得到spbatch表id，根据单号和物资编码
	 * @param inspectm
	 * @throws CRUDException
	 */
	public int getSpidByChknoAndSp(Chkind chkind);
	/**
	 * 获取下一个退货单号
	 * @param backbillm
	 * @return
	 * @throws CRUDException
	 */
	public String getNextTHVbillno(Backbillm backbillm);
	/**
	 * 回写验货单验货凭证号和入库单号
	 * @param backbillm
	 * @throws CRUDException
	 */
	public void updateBackbillmVounoAndChkno(Backbillm backbillm);
}
