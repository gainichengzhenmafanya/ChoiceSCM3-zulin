package com.choice.assistant.persistence.settlement;

import java.util.List;

import com.choice.assistant.domain.settlement.Applyinvoice;
import com.choice.assistant.domain.settlement.Collinvoice;
import com.choice.assistant.domain.settlement.Paydetails;
import com.choice.assistant.domain.settlement.Payment;
import com.choice.assistant.domain.settlement.Payment_d;

/**
 * 付款单
 * Created by mc on 14-10-31.
 */
public interface PaymentMappler {
    /**
     * 获取所有付款单
     * @param payment
     * @return
     */
    public List<Payment> findPayment(Payment payment);

    /**
     * 根据PK获取付款单
     * @param payment
     * @return
     */
    public Payment findPaymentByPk(Payment payment);

    /**
     * 获取付款单-关联发票
     * @param payment
     * @return
     */
    public List<Collinvoice> findCollinvoice(Payment payment);

    /**
     * 获取付款单-发票申请
     * @param payment
     * @return
     */
    public List<Applyinvoice> findApplyinvoice(Payment payment);

    /**
     * 付款单子表
     * @param payment
     * @return
     */
    public List<Payment_d> findPaymentd(Payment payment);

    /**
     * 支付详情
     * @param payment
     * @return
     */
    public List<Paydetails> findPaydetails(Payment payment);

    /**
     * 保存发票申请
     * @param applyinvoice
     */
    public void addApp(List<Applyinvoice> applyinvoice);

    /**
     * 关联发票
     * @param collinvoice
     */
    public void addColl(Collinvoice collinvoice);

    /**
     * 保存付款单子表
     * @param paydetails
     */
    public void addPaydetails(Paydetails paydetails);

    /**
     * 修单据
     * @param payment
     */
    public void updatePayment(Payment payment);

    /**
     * 修改单据状态
     * @param payment
     */
    public void updatePaymentState(Payment payment);
    
    /**
     * 修改订单状态
     * @param payment
     */
    public void updatePuprorderState(Payment_d paymentd);
    
    /**
     * 修改订单状态
     * @param payment
     */
    public void updatePuprorderIstate2(Payment payment);
    
    /**
     * 修改验货的状态
     * @param payment
     */
    public void updateInspectmIstate2(Payment payment);
    
    /**
     * 审核结账
     * @param payment
     */
    public void checkEnd(Payment payment);
    
}
