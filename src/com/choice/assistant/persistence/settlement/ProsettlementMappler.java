package com.choice.assistant.persistence.settlement;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.settlement.Payment;
import com.choice.assistant.domain.settlement.Payment_d;
import com.choice.assistant.domain.settlement.ProBill;
import com.choice.assistant.domain.settlement.ProPayment;
import com.choice.assistant.domain.settlement.Prosettlement;
import com.choice.framework.exception.CRUDException;

/**
 * 采购结算
 * Created by mc on 14-10-29.
 */
public interface ProsettlementMappler {
    /**
     * 获取结算信息
     * @param prosettlement
     * @return
     */
    public List<Prosettlement> listPro(Prosettlement prosettlement);

    /**
     * 根据日期，供应商获取 账单数据
     * @param prosettlement
     * @return
     */
    public List<ProBill> listProBill(Prosettlement prosettlement);
    /**
     * 根据PK获取未付款单据
     * @return
     */
    public List<ProBill> findBillByPk(ProPayment proPayment);

    /**
     * 保存付款的主表
     * @param payment
     */
    public void addPayment(Payment payment);

    /**
     * 保存付款单子表
     * @param list
     */
    public void addPaymnetD(List<Payment_d> list);

    /**
     * 修改单据状态
     * @param map
     */
    public void updateState(Map<String,Object> map);
    
	/**
	 * 得到下一个付款单号
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String getNextVpaymentcode(Payment payment);
}
