package com.choice.assistant.persistence.common;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.common.CommonAssMethod;

/**
 * BOH后台公共调用方法
 * @author zgl
 * @version 创建时间：2014-10-27 21:50:33
 */
public interface CommonAssMapper {

	public String validateVcode(CommonAssMethod commonMethod);

	public String getSortNo(CommonAssMethod commonMethod);
	/**
	 * 描述:启用
	 * 作者:zgl
	 * 日期:2014-10-27 22:11:14
	 * @param ids
	 */
	public void IsEnable(CommonAssMethod commonMethod);


	/**
	 * 描述：验证排序重复性
	 * @param commonMethod
	 * @return
	 * author:zgl
	 * 日期：2014-10-27
	 */
	public Integer validateSortNo(CommonAssMethod commonMethod);
	/**
	 * 验证数据是否被引用
	 * @param commonMethod
	 * @return
	 */
	public String checkQuote(CommonAssMethod commonMethod);
	
	public String checkMultiCodeQuote(CommonAssMethod commonMethod);
	/**
	 * 动态执行删除语句
	 * @param delSql
	 * @return
	 */
	public void dynExecDelete(@Param(value="delSql")String delSql);
	/**
	 * 动态执行新增语句
	 * @param insertSql
	 * @return
	 */
	public void dynExecInsert(@Param(value="insertSql")String insertSql);

}
