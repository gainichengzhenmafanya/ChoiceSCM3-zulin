package com.choice.assistant.persistence.material;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.material.Material;
import com.choice.assistant.domain.material.MaterialMall;
import com.choice.assistant.domain.material.MaterialType;
import com.choice.assistant.domain.material.ReviMaterial;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.domain.system.MaterialUnit;
import com.choice.assistant.domain.system.Tax;
import com.choice.assistant.domain.system.Unit;
import com.choice.framework.exception.CRUDException;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;


/**
 * 物资管理
 * @author wangchao
 *
 */
public interface MaterialMapper {
	
	/**
	 * 查询所有的物资信息
	 * @return
	 */
	public List<Material> queryAllMaterial(Material material);
	
	/**
	 * 查询所有的物资类别
	 * @return
	 */
	public List<MaterialType> queryAllMaterialType(MaterialType materialType);
	
	/**
	 * 查询所有的物资类别(单个)
	 * @return
	 */
	public List<MaterialType> findMaterialType(MaterialType materialType);
	
	/**
	 * 根据物资类别主键穿物资类别信息
	 * @return
	 */
	public List<MaterialType> queryMaterialTypeById(MaterialType materialType);
	
	/**
	 * 获取所有的类别的子类别
	 * @param materialType
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialType> queryMaterialType(MaterialType materialType);
	
	/**
	 * 获取所有的类别的子类别==导入
	 * @param materialType
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialType> queryMaterialType(@Param(value="vcode")String vcode,@Param(value="acct")String acct);
	
	/**
	 * 新增物资类别
	 * @param materialType
	 * @throws CRUDException
	 */
	public void saveMaterialType(MaterialType materialType);
	
	/**
	 * 跳转到修改物资类别页面
	 * @param vcode
	 * @return
	 * @throws CRUDException
	 */
	public MaterialType toUpdateMaterialType(MaterialType materialType);
	
	/**
	 * 获取当前类别下面物资编码的最大值
	 * @param material
	 * @return
	 * @throws CRUDException    123456789
	 */
	public List<Material> selectMaxVcodeByType(Material material);
	
	/**
	 * 修改物资类别
	 * @param materialType
	 * @throws CRUDException
	 */
	public void updateMaterialType(MaterialType materialType);
	
	/**
	 * 判断类别表中指定的类别有没有被引用
	 * @param materialType
	 * @return
	 */
	public int queryMaterialTypeBypk(MaterialType materialType);
	
	/**
	 * 判断指定的类别在物资表中有没有被引用
	 * @param materialType
	 * @return
	 */
	public int queryMaterialByType(MaterialType materialType);
	
	/**
	 * 删除物资类别
	 * @param materialType
	 * @throws CRUDException
	 */
	public void deleteMaterialType(MaterialType materialType);

	
	/**
	 * 检测类型编码是否相同
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public int ajaxIsSameCode(MaterialType materialType);
	
	/**
	 * 检测类型名称是否相同
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public int ajaxIsSameName(MaterialType materialType);
	
	/**
	 * 检测类型是否被引用
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public int findMaterialTypeByName(MaterialType materialType);
	
	/**
	 * 检测类型是否被引用
	 * @param vname
	 * @return
	 * @throws Exception
	 */
	public int findMaterialTypeBytypepk(MaterialType materialType);
	
	/**
	 * 判断当前选中的物资类别是不是末级分类
	 * @param pk_materialtype
	 * @return
	 * @throws Exception
	 */
	public int checkMaterialType(MaterialType materialType);
	
	/**
	 * 获取所有的物资单位
	 * @param unit
	 * @return
	 */
	public List<Unit> queryAllUnit(Unit unit);
	
	
	/**
	 * 获取所有的税率值
	 * @param tax
	 * @return
	 * @throws CRUDException
	 */
	public List<Tax> queryAllTax(Tax tax);
	
	/**
	 * 新增时检索物资名称是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public int findMaterialByName(Material material);
	
	/**
	 * 新增时检索物资编码是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public int findMaterialByCode(Material material);
	
	/**
	 * 修改时检索物资名称是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public int findMaterialByNameUp(Material material);
	
	/**
	 * 修改时检索物资编码是否相同
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public int findMaterialByCodeUp(Material material);
	
	/**
	 * 新增物资
	 * @param materialType
	 * @throws CRUDException
	 */
	public void saveMaterial(Material material);
	
	/**
	 * 校验物资是否被引用
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public int checkMaterial(Material material);
	
	/**
	 * 添加单位转换率
	 * @param materialUnitList
	 * @throws CRUDException
	 */
	public void saveMaterialUnit(MaterialUnit materialUnit);
	
	/**
	 * 删除单位转换率
	 * @param material
	 * @throws CRUDException
	 */
	public void deleteMaterialUnit(Material material);
	
	/**
	 * 根据转换率查询最小采购量
	 * @param material
	 * @return
	 */
	public double findNmincntByRate(Material material);
	
	/**
	 * 根据ID查询物资
	 * @param pk_material
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public Material queryMaterialById(Material material);
	
	/**
	 * 根据物资主键查询关联供应商关系
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialMall> toDeleteMallMaterial(Material material);

	/**
	 * 根据物资主键查询单位转换率
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public List<MaterialUnit> toUpdateMaterialUnit(Material material);

	/**
	 * 查询物资单位转换率
	 * @param materialUnit
	 * @return
	 * @throws CRUDException
	 */
	public MaterialUnit selectMaterialUnit(MaterialUnit materialUnit);

	/**
	 * 根据物资类别主键穿物资类别信息
	 * @return
	 */
	public MaterialType queryMaterialTypeByPK(MaterialType materialType);

	/**
	 * 修改物资信息
	 * @param modelMap
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public void updateMaterial(Material material);
	
	/**
	 * 删除物资信息
	 * @param ids
	 */
	public void deleteMaterial(List<String> ids);
	
	/**
	 * 关联商城供应商
	 * @param material
	 * @return
	 * @throws CRUDException
	 */
	public void mallMaterial(MaterialMall materialMall);
	
	/**
	 * 查询当前商城供应商有没有关联本地供应商
	 * @param supplier
	 * @return
	 */
	public List<Supplier> queryMallSupplier(Supplier supplier);
	
	/**
	 * 删除关联物资
	 * @param materialMall
	 */
	public void deleteMallMaterial(Material material);
	
	/**
	 * 根据输入的条件查询 前n条记录
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public List<Material> findMaterialTop(Material material);
	
	/**
	 * 查询报价
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public Material findMaterialNprice(Material material);
	
	/**
	 * 查询报价
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public Material findMaterialNpricesupplier(Material material);
	
	/***
	 * 获取所有物资（导出）
	 * @param contions
	 * @return
	 */
	public List<Material> findAllMaterial_export(HashMap<String, Object> contions);
	
	/**
	 * 判断倒入物资是否存在
	 * @param material
	 * @return
	 */
	public Integer MaterialCodeCount(Material material);

	/**
	 * 查询待审核物资列表
	 * @param modelMap
	 * @param material
	 * @param page
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public List<ReviMaterial> jmuMaterial(ReviMaterial reviMaterial);
	/**
	 * 查询指定的物资列表
	 * @param material
	 * @return
	 */
	public List<Map<String,Object>> queryMaterialSupplierList(Material material);
	
	/**
	 * 根据待审核物资主键查询待审核物资
	 * @param reviMaterial
	 * @return
	 */
	public ReviMaterial queryReviMaterialById(ReviMaterial reviMaterial);

	/**
	 * 删除待审核物资
	 * @param reviMaterial
	 * @throws CRUDException
	 */
	public void deleteReviMaterial(ReviMaterial reviMaterial);
    /**
     * 删除待审核物资
     * @param pk
     * @throws CRUDException
     */
	public void deleteReviMaterialOne(@Param("pk") String pk, @Param("acct") String acct);

	/**
	 * 跳转到加入到物料表
	 * @param reviMaterial
	 * @return
	 * @throws CRUDException
	 */
	public Material toUpdateReviMaterial(ReviMaterial reviMaterial);
	
	/**
	 * 查询所有物资合约报价
	 * @param material
	 * @return
	 */
	public List<Material> findAllMaterialPrice(Material material);
	
	/**
	 * 查询所有的物资报价
	 * @param material
	 * @return
	 */
	public Material findMaterialScope(Material material);
	
	/**
	 * 根据物资查询采购组织(前10条)
	 * @param material
	 * @return
	 */
	public List<Positn> findPositnBySupplyCG(Material material);
	
	/**
	 * 根据物资查询采购组织(弹窗)
	 * @param material
	 * @return
	 */
	public List<Positn> findPositnSuper(Material material);
	
	/**
	 * 根据物资返回供应商
	 * @param material
	 * @return
	 */
	public List<Deliver> findDeliverByMaterial(Material material);
	
}
