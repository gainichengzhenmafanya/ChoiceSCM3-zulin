package com.choice.assistant.persistence.procurementtender;

import java.util.List;

import com.choice.assistant.domain.procurementtender.CgTender;
import com.choice.assistant.domain.procurementtender.CgTenderd;
import com.choice.assistant.domain.system.PayMethod;
import com.choice.assistant.domain.system.Tender;
import com.choice.framework.exception.CRUDException;

public interface CgTenderMapper {

	/**
	 * 查询所有的招标
	 * @param cgtender
	 * @return
	 */
	public List<CgTender> queryAllCgTender(CgTender cgtender);
	
	/**
	 * 查询所有的招标类型
	 * @param tender
	 * @return
	 * @throws CRUDException
	 */
	public List<Tender> queryAllTender(Tender tender);
	
	/**
	 * 查询所有的付款方式
	 * @param paytype
	 * @return
	 * @throws CRUDException
	 */
	public List<PayMethod> queryAllPayType(PayMethod PayMethod);
	
	/**
	 * 保存采购招标
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	public void saveCgTender(CgTender cgtender);
	
	/**
	 * 根据主键查询招标
	 * @param cgtender
	 * @return
	 * @throws CRUDException
	 */
	public CgTender queryCgTenderByID(CgTender cgtender);
	
	/**
	 * 修改招标书
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	public void updateCgTender(CgTender cgtender);
	
	/**
	 * 删除招标书
	 * @param cgtender
	 * @throws CRUDException
	 */
	public void deleteCgTender(CgTender cgtender);
	
	/**
	 * 终止招标书
	 * @param cgtender
	 * @throws CRUDException
	 */
	public void stopCgTender(CgTender cgtender);
	
	/**
	 * 终止招标书
	 * @param cgtender
	 * @throws CRUDException
	 */
	public void releaseCgTender(CgTender cgtender);
	/**
	 * 新增应标信息
	 * @param cgtenderd
	 */
	public void insertCgTenderd(CgTenderd cgtenderd);
	
	/**
	 * 删除应标书
	 * @param cgtenderd
	 * @throws CRUDException
	 */
	public void deleteCgTenderd(CgTenderd cgtenderd);
	
	/**
	 * 查询所有的应标
	 * @param cgtender
	 * @return
	 */
	public List<CgTender> queryAllCgTenderd(CgTender cgtender);
	
	/**
	 * 查询所有的应标
	 * @param cgtenderd
	 * @return
	 */
	public List<CgTenderd> queryAllCgTenderdByCode(CgTenderd cgtenderd);
	
	/**
	 * 根据主键查询应标
	 * @param cgtender
	 * @return
	 * @throws CRUDException
	 */
	public CgTenderd queryCgTenderdByID(CgTenderd cgtenderd);
	
	/**
	 * 修改应标书
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	public void updateCgTenderd(CgTenderd cgtenderd);
	
}
