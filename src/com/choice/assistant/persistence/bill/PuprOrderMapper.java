package com.choice.assistant.persistence.bill;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.framework.exception.CRUDException;
/**
 * 采购订单
 * @author LQ
 *
 */
public interface PuprOrderMapper {
	
	/**
	 * 查询所有的订单信息
	 * @param puprOrder
	 * @return
	 */
	public List<PuprOrder> queryPuprOrders(PuprOrder puprOrder);
	
	/**
	 * 查询所有的订单信息
	 * @param puprOrder
	 * @return
	 */
	public List<PuprOrder> queryAllPuprOrders(PuprOrder puprOrder);
	
	/**
	 * 查询订单子表信息
	 * @param puprOrderd
	 * @return
	 * @throws CRUDException
	 */
	public List<PuprOrderd> queryAllPuprOrderdsByPK(PuprOrderd puprOrderd);
	
	/**
	 * 查询菜都订单子表信息
	 * @param puprOrderd
	 * @return
	 * @throws CRUDException
	 */
	public List<PuprOrderd> queryAllPuprOrderdsByPk(PuprOrderd puprOrderd);
	
	/**
	 * 根据主键查询单条订单信息
	 * @param puprOrder
	 * @return
	 */
	public PuprOrder queryPuprOrderByPk(PuprOrder puprOrder);
	
	/**
	 * 获取订单的最大单号,按照月流水进行
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<PuprOrder> selectMaxVbillno(PuprOrder puprOrder);
	
	/**
	 * 保存订单主表
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void savePuprOrderm(PuprOrder puprOrder);
	
	/**
	 * 保存订单评价信息
	 * @param puprOrder
	 */
	public void saveEvaluate(PuprOrder puprOrder);
	
	/**
	 * 保存订单子表
	 * @param puprOrderd
	 * @throws CRUDException
	 */
	public void savePuprOrderd(PuprOrderd puprOrderd);
	
	/**
	 * 根据主键查询供应商列表
	 * @param puprOrder
	 * @return
	 */
	public List<Supplier> querySupplierByPK(PuprOrder puprOrder);
	
	/**
	 * 校验单据号是否重复
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public PuprOrder findPuprorderByCode(PuprOrder puprOrder);
	
	/**
	 * 修改订单主表
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void updatePuprOrderm(PuprOrder puprOrder);
	
	/**
	 * 删除订单主表
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void deletePuprorder(PuprOrder puprOrder);
	
	/**
	 * 根据主表主键删除订单子表
	 * @param puprOrderd
	 * @throws CRUDException
	 */
	public void deletePuprOrderd(PuprOrderd puprorderd);
	
	/**
	 * 根据主键查询采购订单主表信息
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public PuprOrder toUpdatePuprorder(PuprOrder puprOrder);
	
	/**
	 * 生成验货单之后修改订单状态，更改为已验货
	 * @param puprOrder
	 * @throws CRUDException
	 */
	public void updatePuprOrdermStat(PuprOrder puprOrder);
	/**
	 * 查询助手中未完成的订单(不包括未提交、已完成、已取消订单)
	 * @param puprOrder
	 * @return 
	 */
	public List<PuprOrder> queryAllPuprOrdersForAutoItf(PuprOrder puprOrder);
	/**
	 * 更新订单状态
	 * @param puprOrder
	 * @return
	 */
	public void updatePuprOrdersState(PuprOrder puprOrder);
	/**
	 * 检测选择的订单是不是当前登录用户新建的
	 * @param puprOrder
	 * @return
	 */
	public List<Map<String, Object>> checkOrderIsMine(PuprOrder puprOrder);
	
	/**
	 * 获取下一个采购单号
	 * @param puprOrder
	 * @return
	 * @throws CRUDException
	 */
	public String getNextCGVbillno(PuprOrder puprOrder);
}
