package com.choice.assistant.persistence.bill;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.bill.PreOrder;
import com.choice.assistant.domain.bill.PreOrderd;
import com.choice.framework.exception.CRUDException;
/**
 * 预订单
 * @author Administrator
 *
 */
public interface PreOrderMapper {
	
	/**
	 * 查询所有的预订单信息
	 * @param preOrder
	 * @return
	 */
	public List<PreOrder> queryAllPreOrders(PreOrder preOrder);
	
	/**
	 * 查询预订单子表信息
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<PreOrderd> queryAllPreOrderds(PreOrder preOrder);
	
	/**
	 * 查询预订单子表信息，默认供应商税率等
	 * @param preOrder
	 * @return
	 * @throws CRUDException
	 */
	public List<PreOrderd> queryAllPreOrderdsDeliver(PreOrder preOrder);
	
	/**
	 * 查询所有的预订单信息，未生成的
	 * @param preOrder
	 * @return
	 */
	public List<PreOrder> queryPreOrdersToPuprorders(PreOrder preOrder);
	
	/**
	 * 插入主表
	 * @param preOrder
	 * @return
	 */
	public void savePreOrder(PreOrder preOrder);
	
	/**
	 * 插入从表
	 * @param preOrderd
	 * @return
	 */
	public void savePreOrderd(PreOrderd preOrderd);
	
	/**
	 * 删除从表
	 * @param preOrder
	 * @return
	 */
	public void deletePreOrderds(PreOrder preOrder);
	
	/**
	 * 删除从表
	 * @param preOrder
	 * @return
	 */
	public void deletePreOrder(PreOrder preOrder);
	
	/**
	 * 得到下一个主表的序列的值
	 * @param xlm
	 * @return
	 */
	public int getValByXuLie(@Param(value="xlm")String xlm);
	
	/**
	 * 更新预订单状态
	 * @param preOrder
	 * @return
	 */
	public void updateDtype(PreOrder preOrder);
}
