package com.choice.assistant.persistence.detailedlist;

import java.util.List;

import com.choice.assistant.domain.detailedlist.Purtempletd;
import com.choice.assistant.domain.detailedlist.Purtempletm;
import com.choice.framework.exception.CRUDException;

/**
 * 采购清单管理
 * @author wangchao
 *
 */
public interface PurtempletMapper {

	/**
	 * 查询所有的采购清单主表
	 * @param purtempletm
	 * @return
	 */
	public List<Purtempletm> queryAllPurtempletm(Purtempletm purtempletm);
	
	/**
	 * 根据主表ID查询物料清单子表
	 * @param purtempletd
	 * @return
	 */
	public List<Purtempletd> queryAllPurtempletd(Purtempletd purtempletd);
	
	/**
	 * 根据主表ID查询物料清单子表 == 根据供应商分组
	 * @param purtempletd
	 * @return
	 */
	public List<Purtempletd> queryPurtempletdBySupplier(Purtempletd purtempletd);
	
	/**
	 * 添加采购清单
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void addPurtempletm(Purtempletm purtempletm);
	
	/**
	 * 添加采购清单子表
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void addPurtempletd(Purtempletd purtempletd);
	
	/**
	 * 删除采购清单
	 * @param ids
	 * @throws CRUDException
	 */
	public void deletePurtempletm(Purtempletm purtempletm);
	
	/**
	 * 根据主键查询采购清单主表信息
	 * @param purtempletm
	 * @return
	 */
	public Purtempletm queryPurtempletmByPK(Purtempletm purtempletm);
	
	/**
	 * 修改采购清单
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	public void updatePurtempletm(Purtempletm purtempletm);
	
	/**
	 * 删除采购清单子表
	 * @param ids
	 * @throws CRUDException
	 */
	public void deletePurtempletd(Purtempletm purtempletm);
	
	/**
	 * 获取采购清单的下拉框
	 * @param purtempletm
	 * @return
	 * @throws CRUDException
	 */
	public List<Purtempletm> getPurtempletmName(Purtempletm purtempletm);
}
