package com.choice.assistant.persistence.cginquiry;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.cginquiry.CgInquiry;
import com.choice.assistant.domain.system.Inquiry;
import com.choice.framework.exception.CRUDException;

/**
 * 采购询价
 * @author xiaoguai
 *
 */
public interface CgInquiryMapper {
	
	/**
	 * 查询询价列表
	 * @param cgInquiry
	 * @return
	 */
	public List<CgInquiry> tableCgInquiry(CgInquiry cgInquiry);
	
	/**
	 * 查询所有的询价类型
	 * @param inquiry
	 * @return
	 * @throws CRUDException
	 */
	public List<Inquiry> selectAllInquiryType(Inquiry inquiry);
	
	/**
	 * 添加采购询价
	 * @param cgInquiry
	 * @return
	 * @throws Exception
	 */
	public void saveCgInquiry(CgInquiry cgInquiry);
	
	/**
	 * 跳转到修改页面
	 * @param cgInquiry
	 * @return
	 * @throws CRUDException
	 */
	public CgInquiry toUpdateCginquiry(CgInquiry cgInquiry);
	
	/**
	 * 修改采购询价
	 * @param cgInquiry
	 * @return
	 * @throws Exception
	 */
	public void updateCgInquiry(CgInquiry cgInquiry);
	
	/**
	 * 删除询价
	 * @param cgInquiry
	 * @throws CRUDException
	 */
	public void deleteCginquiry(CgInquiry cgInquiry);

    /**
     * 获取统计所用统计信息
     * @param cgInquiry
     */
    public List<Map<String,Object>> findPriceAnalysisData(CgInquiry cgInquiry);
}
