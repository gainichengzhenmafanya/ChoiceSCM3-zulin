package com.choice.assistant.persistence.Report;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.report.Condition;

/**
 * 报表Mapper
 * Created by mc on 14-11-16.
 */
public interface AnalysisMapper {
    /**
     * 合约价格走势分析
     * @param delivercode
     * @param pk_material
     * @param date
     * @return
     */
    public List<Map<String,Object>> findContractPriceTrend(@Param("delivercode") String delivercode, @Param("pk_material") String pk_material, @Param("date") String date);

    /**
     * 采购价格异动分析
     * @param condition
     * @return
     */
    public List<Map<String,Object>> findAnalysisOfPriceChanges(Condition condition);
    /**
     * 不合格进货分析
     * @param condition
     * @return
     */
    public List<Map<String,Object>> findUnqualifiedStockAnalysis(Condition condition);

    /**
     * 采购到货分析分析
     * @param condition
     * @return
     */
    public List<Map<String,Object>> findPurchasingArrivalGoods(Condition condition);
}
