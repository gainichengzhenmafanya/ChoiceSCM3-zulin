package com.choice.assistant.persistence.Report;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.report.Condition;

/**
 * 验货报表Mapper
 * Created by lq on 14-11-18.
 */
public interface InspectReportMapper {
	
	//验货明细查询
	public List<Map<String,Object>> findInspectDetail(Condition condition);
	//验货明细查询
	public List<Map<String,Object>> findInspectDetailSum(Condition condition);
	
	//验货物资汇总查询
	public List<Map<String,Object>> findInspectMaterialSummary(Condition condition);
	//验货物资汇总查询合计
	public List<Map<String,Object>> findInspectMaterialSummarySum(Condition condition);
	
	//验货物资类别汇总
	public List<Map<String,Object>> findInspectMaterialTypeSummary(Condition condition);
	//验货物资类别汇总合计
	public List<Map<String,Object>> findInspectMaterialTypeSummarySum(Condition condition);
}
