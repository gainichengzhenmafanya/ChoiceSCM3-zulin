package com.choice.assistant.persistence.Report;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.report.Condition;

/**
 * 采购报表Mapper
 * Created by lq on 14-11-18.
 */
public interface PurchaseMapper {
	
	//采购明细查询
	public List<Map<String,Object>> findPuprOrderDetail(Condition condition);
	//采购明细查询
	public List<Map<String,Object>> findPuprOrderDetailSum(Condition condition);
	
	//物资汇总查询
	public List<Map<String,Object>> findMaterialSummary(Condition condition);
	//物资汇总查询合计
	public List<Map<String,Object>> findMaterialSummarySum(Condition condition);
	
	//物资类别汇总
	public List<Map<String,Object>> findMaterialTypeSummary(Condition condition);
	//物资类别汇总合计
	public List<Map<String,Object>> findMaterialTypeSummarySum(Condition condition);
	
	//按物资供应商汇总
	public List<Map<String,Object>> findMaterialSupplier(Condition condition);
	
	//验货明细查询
	public List<Map<String,Object>> findInspectDetail(Condition condition);
	
	//验货物资汇总查询
	public List<Map<String,Object>> findInspectMaterialSummary(Condition condition);
	//验货物资汇总查询合计
	public List<Map<String,Object>> findInspectMaterialSummarySum(Condition condition);
	
	//验货物资类别汇总
	public List<Map<String,Object>> findInspectMaterialTypeSummary(Condition condition);
	//验货物资类别汇总合计
	public List<Map<String,Object>> findInspectMaterialTypeSummarySum(Condition condition);
}
