package com.choice.assistant.persistence.supplier;

import java.util.List;

import com.choice.assistant.domain.supplier.SupplierType;

public interface SupplierTypeMapper {
	/**
	 * 查询供应商类别树
	 * @param supplierType
	 * @return
	 */
	public List<SupplierType> queryAllSupplierTypeTree(SupplierType supplierType);

}
