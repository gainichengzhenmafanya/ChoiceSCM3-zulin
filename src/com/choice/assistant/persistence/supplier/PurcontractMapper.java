package com.choice.assistant.persistence.supplier;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.supplier.ChangeLog;
import com.choice.assistant.domain.supplier.ContractQuote;
import com.choice.assistant.domain.supplier.Contractclau;
import com.choice.assistant.domain.supplier.Contracteven;
import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.assistant.domain.supplier.Purcontract;
import com.choice.assistant.domain.supplier.Suitstore;
import com.choice.assistant.domain.supplier.Transationatta;
import com.choice.assistant.domain.system.Contractterms;
import com.choice.scm.domain.FirmDeliver;
import com.choice.scm.domain.Spprice;

/**
 * 采购合约
 * Created by mc on 14-10-24.
 */
public interface PurcontractMapper {

    /**
     * 根据PK获取采购合约
     * @param purcontract
     */
    public Purcontract findPurcontractByPk(Purcontract purcontract);

    /**
     * 根据合约主键查询合约主表信息
     * @param purcontract
     * @author 王超
     * @return
     */
    public Purcontract findPurcontractByPK(Purcontract purcontract);
    
    /**
     * 根据合约主键获取合约基本的信息
     * @author 王超
     * @param purcontract
     * @return
     */
    public List<Spprice> findSppriceByPK(Purcontract purcontract);
    
    /**
     * 根据合约主键查询合约条款的信息
     * @author 王超
     * @param purcontract
     * @return
     */
    public List<Contractclau> findContractclauByPK(Purcontract purcontract);
    
    /**
     * 根据合约主键查询合约大事件的信息
     * @author 王超
     * @param purcontract
     * @return
     */
    public List<Contracteven> findContractevenBYPK(Purcontract purcontract);
    
    /**
     * 根据合约主键查询更改记录
     * @author 王超
     * @param purcontract
     * @return
     */
    public List<ChangeLog> findChangeLogByPK(Purcontract purcontract);
    
    /**
     * 根据合约主键查询合约附件操作记录
     * @author 王超
     * @param purcontract
     * @return
     */
    public List<Transationatta> findTransationattaByPK(Purcontract purcontract);
    
    /**
     * 获取所有采购合约
     * @param purcontract
     * @return
     */
    public List<Purcontract> findPurcontract(Purcontract purcontract);

    /**
     * 获取适用门店
     * @param suitstore
     * @return
     */
    public List<Suitstore> findStore(Suitstore suitstore);

    /**
     * 获得所有合约类型
     * @return
     */
    public List<Contractterms> findContractterms(@Param("acct")String acct);

    /**
     * 获取该合约最大报价批次
     * @param purcontract
     * @return
     */
    public Integer findBatch(Purcontract purcontract);
    //============================新增，修改，删除=====================================
    /**
     * 保存采购合约
     * @param purcontract
     */
    public void savePurcontract(Purcontract purcontract);

    /**
     * 保存采购合约变更信息
     * @param changeLog
     */
    public void saveChangeLog(ChangeLog changeLog);

    /**
     * 保存合约条款
     * @param contractclau
     */
    public void saveContractclau(List<Contractclau> contractclau);

    /**
     * 保存采购合约大事记
     * @param contracteven
     */
    public void saveContracteven(List<Contracteven> contracteven);

    /**
     * 保存合约报价
     * @param contractQuote
     */
    public void saveSpprice(List<Spprice> sppricelist);
    
    /**
     * 删除采购报价
     */
    public void deleteContractQuote(Spprice spprice);
    
    /**
     * 保存适用门店
     * @param suitstore
     */
    public void saveSuitstore(List<Suitstore> suitstore);

    /**
     * 保存采购合约附件
     * @param transationatta
     */
    public void saveTransationatta(Transationatta transationatta);

    /**
     * 修改采购合约
     * @param purcontract
     */
    public void updatePurcontract(Purcontract purcontract);

    /**
     * 删除采购合约
     */
    public void delPurcontract(Map<String,Object> map);
    
   
    /**
     * 删除采购合约-假
     */
    public void delPurcontractDr(Map<String,Object> map);

    /**
     * 删除合约条款
     */
    public void delContractclau(@Param("pk_purcontract") String pk_purcontract, @Param("acct") String acct);

    /**
     * 删除采购合约大事记
     */
    public void delContracteven(@Param("pk_purcontract") String pk_purcontract, @Param("acct") String acct);

    /**
     * 删除合约报价
     * @param pk_purcontract
     */
    public void delContractQuote(@Param("pk_purcontract")String pk_purcontract,@Param("pk_contractquote")String pk_contractquote);
    /**
     * 删除合约报价-假
     * @param pk_purcontract
     */
    public void delContractQuoteDr(@Param("pk_purcontract")String pk_purcontract,@Param("pk_contractquote")String pk_contractquote);

    /**
     * 删除采购合约附件
     * @param list
     */
    public void delTransationatta(List<String> list);

    /**
     * 删除适用门店
     * @param pk_purcontract
     */
    public void delSuitstore(@Param("pk_purcontract")String pk_purcontract,@Param("pk_contractquote")String pk_contractquote);
    /**
     * 删除适用门店-假
     * @param pk_purcontract
     */
    public void delSuitstoreDr(Spprice spprice);

    /**
     * 修改合约状态
     */
    public void updatePurState(Map<String,Object> map);


    /**
     * 修改合约报价
     */
    public void updateSpprice(Spprice spprice);

    /**
     * 修改已到期合约
     */
    public void updatePurcontractState(@Param("dat")String dat);

    /**
     * 获取供应商 供应物资
     * @param materialScope
     * @return
     */
    public List<MaterialScope> findMaterial(MaterialScope materialScope);

    /**
     * 获取 供应商 适用门店
     * @param org
     * @return
     */
    public List<FirmDeliver> queryFirm(FirmDeliver firmDeliver);

    /**
     * 获得不可以删除的合约
     * @param list
     * @return
     */
    public int findStatePur(List<String> list);

    /**
     * 判定获取编码重复
     * @param code
     * @param group 
     * @return
     */
    public int findHaveCode(@Param("code")String code, @Param("pk_purcontract") String pk_purcontract, @Param("acct")String group);

    /**
     * 根据主键获取附件
     * @param pk
     * @return
     */
    public Transationatta findTransationattaByPk(@Param("pk") String pk);

    /**
     * 修改附件的合约主键
     * @param list
     */
    public void updateTransationattaPK(@Param("list") List<Transationatta> list, @Param("pk_purcontract") String pk_purcontract);

    /**
     * 获取合约报价明细
     * @param purcontract
     * @return
     */
    public List<ContractQuote> getContractQuoteByPurid(Purcontract purcontract);
    /**
     * 检测一个采购组织、一个供应商 采购合约是否时间重叠，重叠返回-1 不重叠返回1 
     * @param purcontract
     * @return
     */
	public Purcontract checkPurcontract(Purcontract purcontract);
	
	/**
	 * 查询已经存在的
	 * @param spprice
	 * @return
	 */
	public List<Spprice> findSpprice(Spprice spprice);
}
