package com.choice.assistant.persistence.supplier;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.supplier.SupplierRM;
import com.choice.assistant.domain.supplier.SupplierTO;
import com.choice.framework.exception.CRUDException;


/**
 * 供应商招募
 * @author zhb
 *
 */
public interface SupplierRecruitmentMapper {
	/**
	 * 查询所有的供应商招募信息
	 * @param supplierRM
	 * @return
	 */
	public List<SupplierRM> querySupplierRM(SupplierRM supplierRM);
	/**
     * 根据供应商招募PK获取数据-未删除的
     * @param supplierRM
     * @return
     */
    public List<SupplierTO> findSupplierRMByPk(SupplierTO supplierTO);
    /**
     * 根据供应商招募PKS获取数据-被删除的
     * @param supplierRM
     * @return
     */
    public List<SupplierTO> findSupplierRMByPkS(SupplierTO supplierTO);
    /**
     * 根据供应商招募PK获取主数据
     * @param supplierRM
     * @return
     */
    public SupplierRM findSupplierRMZHUByPk(SupplierRM supplierRM);
    /**
     * 根据Code获取主数据
     * @param supplierRM
     * @return
     */
    public SupplierRM findSupplierRMByCode(SupplierRM supplierRM);
    /**
     * 供应商招募新增
     * @param supplierRM
     */
    public void addSupplierRM(SupplierRM supplierRM);
    /**
     * 供应商招募修改
     * @param supplierRM
     * @return
     */
    public void updateSupplierRM(SupplierRM supplierRM);
    /**
     * 供应商应募申请删除，dr的值变为1
     * @param supplierRM
     * @return
     */
    public void updateyminfo(@Param("code")String code,@Param("ts")String ts);
    /**
     * 供应商应募恢复，dr的值变为0
     * @param supplierRM
     * @return
     */
    public void hfupdateyminfo(@Param("code")String code,@Param("ts")String ts);
    /**
     * 删除供应商招募
     * @param idlist
     */
    public void deleteSupplierRM(List<String> idlist);
    /**
     * 供应商招募状态修改
     * @param map
     */
    public void updateEnablestate(Map<String,Object> map);
    /**
	 * 查询选中的应募信息
	 */
	public SupplierTO findAllSupplierTObygysid(SupplierTO supplierTO);
	/**
	 * 判断加入供应商待审核是否重复
	 */
	public String findAuditbypks(@Param("delivercode")String delivercode,@Param("acct")String acct);
	/**
	 * 发布上传供应商招募
	 * @param supplierRM
	 */
	public void updateSupplierRMMallId(SupplierRM supplierRM);
	/**
	 *  修改供应商招募状态为已完成
	 * @param supplierRM
	 */
	public void stopSupplierRM(SupplierRM supplierRM);
	/**
	 * 插入招募书应募信息
	 * @param supplierTOd
	 */
	public void addEnlist(SupplierTO supplierTOd);
	
	/**
	 * 删除应募书
	 * @param supplierTO
	 * @throws CRUDException
	 */
	public void deleteSupplierTO(SupplierTO supplierTO);
	
	/**
	 * 查询所有的应募
	 * @param supplierRM
	 * @return
	 */
	public List<SupplierTO> queryAllSupplierTO(SupplierRM supplierRM);
	
	/**
	 * 查询所有的应募
	 * @param supplierTO
	 * @return
	 */
	public List<SupplierTO> queryAllSupplierTOByCode(SupplierTO supplierTO);
	
	/**
	 * 根据主键查询应募
	 * @param supplierTO
	 * @return
	 * @throws CRUDException
	 */
	public SupplierTO querySupplierTOByID(SupplierTO supplierTO);
	
	/**
	 * 修改应募书
	 * @param session
	 * @param supplierTO
	 * @return
	 * @throws Exception
	 */
	public void updateSupplierTO(SupplierTO supplierTO);
}
