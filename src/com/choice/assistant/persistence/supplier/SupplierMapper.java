package com.choice.assistant.persistence.supplier;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.material.Material;
import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.domain.supplier.SupplierBank;
import com.choice.assistant.domain.supplier.SupplierOrg;
import com.choice.assistant.domain.supplier.SupplierType;


/**
 * 供应商管理
 * @author wangchao
 *
 */
public interface SupplierMapper {

    /**
     * 根据PK获取数据
     * @param supplier
     * @return
     */
    public Supplier findSupplicerByPk(Supplier supplier);

    /**
     * 根据Code获取数据
     * @param supplier
     * @return
     */
    public Supplier findSupplicerByCode(Supplier supplier);

	
	/**
	 * 查询所有的供应商信息
	 * @return
	 */
	public List<Supplier> queryAllSupplier(Supplier supplier);
	
	/**
	 * 查询供应商分页
	 * @param supplier
	 * @return
	 */
	public List<Supplier> findSupplierPage(Supplier supplier);
	
	/**
	 * 查询所有的供应商类别
	 * @return
	 */
	public List<SupplierType> queryAllSupplierType(SupplierType supplierType);

    /**
     * 供应商新增
     * @param supplier
     */
    public void addSupplier(Supplier supplier);

    /**
     * 供应物资范围
     * @param materialScope
     */
    public void addMaterialScope(List<MaterialScope> materialScope);

    /**
     * 银行卡号
     * @param supplierBank
     */
    public void addSupplierBank(List<SupplierBank> supplierBank);

    /**
     * 添加供应商适用门店
     * @param supplierOrg
     */
    public void addSupplierOrg(List<SupplierOrg> supplierOrg);

    /**
     * 供应商修改
     * @param supplier
     * @return
     */
    public void updateSupplier(Supplier supplier);

    /**
     * 供应商状态修改
     * @param map
     */
    public void updateEnablestate(Map<String,Object> map);
    /**
     * 假-删除供应商
     * @param idlist
     * @return
     */
    public void fDeleteSupplier(List<String> idlist);

    /**
     * 真-删除供应商
     * @param idlist
     */
    public void deleteSupplier(List<String> idlist);
    /**
     * 删除供应商评价
     * @param supplier
     * @return
     */
    public void deleteEvalBySupplerPk(Supplier supplier);

    /**
     * 删除适用门店
     * @param supplier
     * @return
     */
    public void deleteOrgBySupplerPk(Supplier supplier);

    /**
     * 删除供应物资范围
     * @param supplier
     * @return
     */
    public void deleteMatlBySupplerPk(Supplier supplier);

    /**
     * 删除银行信息
     * @param supplier
     * @return
     */
    public void deleteBankBySupplerPk(Supplier supplier);

    /**
     * 根据pk获取供应商类别
     * @param supplierType
     * @return
     */
    public SupplierType findSupplierTypeByPk(SupplierType supplierType);

    /**
     * 根据code获取数据
     * @param supplierType
     * @return
     */
    public SupplierType findSupplierTypeByCode(SupplierType supplierType);

    /**
     * 供应商类别添加
     * @param supplierType
     */
    public void addSupplierType(SupplierType supplierType);

    /**
     * 供应商类别修改
     * @param supplierType
     */
    public void updateSupplierType(SupplierType supplierType);
	
	/**
	 * 根据输入的条件查询 前n条记录
	 * @return
	 * @throws Exception
	 */
	public List<Supplier> findSupplierTop(Supplier supplier);

	/**
	 * 根据物资查询共应该物资的供应商
	 * @param material
	 * @return
	 */
	public List<Supplier> findSupplierByMaterialTop(Material material);
	
    /**
     * 删除分类
     * @param list
     */
    public void delSupplierType(List<String> list);

    /**
     * 删除分类-假
     * @param list
     */
    public void delSupplierTypeDr(List<String> list);

    /**
     * 判定类别是否被引用
     * @param list
     */
    public Integer haveSupplierType(List<String> list);
	/**
	 * 根据输入的条件查询 前n条记录
	 * @return
	 * @throws Exception
	 */
	public List<Supplier> querySupplierListByPk(Supplier supplier);

    /**
     * 根据物资获取 供应这个物质的供应商
     * @param materialScope
     * @return
     */
    public List<Supplier> findSupplierByMatl(MaterialScope materialScope);

    /**
     * 根据PK获取供应物资
     * @param materialScope
     * @return
     */
    public int findMaterialScope(MaterialScope materialScope);

    /**
     * 修改商城供应商
     * @param supplier
     */
    public void updateJmu(Supplier supplier);

    /**
     * 获取适用物资范围
     * @param supplier
     * @return
     */
    public List<MaterialScope> getMaterial(Supplier supplier);
    /**
     * 检测数据引用状态公用方法
     * @param map
     * @return
     */
	public int checkDataQuote(Map<String, Object> map);
}
