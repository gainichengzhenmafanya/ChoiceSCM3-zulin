package com.choice.assistant.persistence.supplier;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.supplier.Suppliereval;
import com.choice.assistant.domain.supplier.Supplierevald;
import com.choice.assistant.domain.system.Evalitem;

/**
 * 供应商评价
 * Created by mc on 14-12-4.
 */
public interface SupplierEvaluateMapper {
    /**
     * 获取评价体系
     * @return
     */
    public List<Evalitem> queryEvalitem(Evalitem evalitem);

    /**
     * 删除评价项
     * @param acct
     */
    public void delEvalitem(@Param("acct") String acct);

    /**
     * 保存评价项
     * @param evalitems
     */
    public void saveEvalitem(List<Evalitem> evalitems);

    /**
     * 添加供应商审核明显
     * @param supplierevalds
     */
    public void saveSupplierevald(List<Supplierevald> supplierevalds);

    /**
     * 添加供应商审核信息
     * @param suppliereval
     */
    public void saveSuppliereval(Suppliereval suppliereval);

    /**
     * 获取评价信息
     * @param suppliereval
     * @return
     */
    public List<Suppliereval> querySuppliereval(Suppliereval suppliereval);
}