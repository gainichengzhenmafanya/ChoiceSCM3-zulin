package com.choice.assistant.persistence.supplier;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.supplier.Supplier;

/**
 * 供应商审核
 * Created by mc on 14-10-29.
 */
public interface AuditMapper {
    /**
     * 获取待审核供应商
     * @param auditsupplier
     * @return
     */
    public List<Auditsupplier> findAudit(Auditsupplier auditsupplier);

    /**
     * 删除待审核供应商
     * @param ids
     */
    public void delAudit(List<String> ids);

    /**
     * 根据待审核供应商的供应商PK删除
     * @param id
     */
    public void delOneAudit(@Param("id")String id);

    /**
     * 关联供应商
     * @param supplier
     */
    public void linkSupplier(Supplier supplier);

    /**
     * 判定该供应商是否被关联
     * @param id
     * @param acct 
     * @return
     */
    public int isLink(@Param("id") String id, @Param("acct") String acct);
    /**
     *  获得所有待审核供应商按主键查询
     * @param auditsupplier
     * @return
     */
	public List<Auditsupplier> queryAudit(Auditsupplier auditsupplier);
}
