package com.choice.assistant.persistence.tableMain;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.TableMain.OrdrIstate;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.assistant.domain.detailedlist.Purtempletd;
import com.choice.assistant.domain.detailedlist.Purtempletm;
import com.choice.assistant.domain.supplier.Purcontract;
import com.choice.assistant.domain.system.MaterialMatch;

public interface TableMainOrdrMapper {
	/**
	 * 查询订单号所对应的首页订单
	 * @return
	 */
	public PuprOrder codeordrlist(PuprOrder puprOrder);
	/**
	 * 查询订单号所对应的明细
	 * @return
	 */
	public List<PuprOrderd> codeordrDetailList(PuprOrder puprOrder);
	/**
	 * 最新发生的订单
	 * @param puprOrder
	 * @return
	 */
	public List<PuprOrder> newPuprOrder(PuprOrder puprOrder);
	/**
	 * 物料匹配
	 * @param puprOrder
	 * @return
	 */
	public List<Map<String, Object>> queryMaterialMatch(MaterialMatch materialMatch);
	/**
	 * 订单跟踪状态
	 * @param OrdrIstate
	 * @return
	 */
	public OrdrIstate newordristate(OrdrIstate ordrIstate);
	/**
	 * 待确认
	 * @param OrdrIstate
	 * @return
	 */
	public List<OrdrIstate> dqrnewordristate(OrdrIstate ordrIstate);
	/**
	 * 待收货
	 * @param OrdrIstate
	 * @return
	 */
	public List<OrdrIstate> dshnewordristate(OrdrIstate ordrIstate);
	/**
	 * 待入库
	 * @param OrdrIstate
	 * @return
	 */
	public List<OrdrIstate> drknewordristate(OrdrIstate ordrIstate);
	/**
	 * 待结算
	 * @param OrdrIstate
	 * @return
	 */
	public List<OrdrIstate> djsnewordristate(OrdrIstate ordrIstate);
	/**
	 * 采购清单
	 * @param OrdrIstate
	 * @return
	 */
	public List<Purtempletm> cgqdnewordristate(Purtempletm purtempletm);
	/**
	 * 采购清单对应的具体单据主表
	 * @param OrdrIstate
	 * @return
	 */
	public Purtempletm cgqdlist(Purtempletm purtempletm);
	/**
	 * 采购清单对应的具体单据子表
	 * @param OrdrIstate
	 * @return
	 */
	public List<Purtempletd> cgqdlists(Purtempletd purtempletd);
	/**
	 *  合约
	 * @param OrdrIstate
	 * @return
	 */
	public List<Purcontract> hy(Purcontract purcontract);
	/**
	 *  合约提醒
	 * @param OrdrIstate
	 * @return
	 */
	public List<Purcontract> hytx(Purcontract purcontract);
}
