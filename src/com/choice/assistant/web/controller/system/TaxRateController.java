package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.TaxRateConstants;
import com.choice.assistant.domain.system.Tax;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.TaxRateService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 税率设置
 * Created by zgl
 */
@Controller
@RequestMapping(value = "taxRate")
public class TaxRateController {
    @Autowired
    private TaxRateService taxService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取税率设置
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findTaxRate(ModelMap modelMap,Tax tax,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        tax.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("taxList", taxService.findTaxRateListByPage(tax,page));
		modelMap.put("tax", tax);
		modelMap.put("pageobj", page);
        return new ModelAndView(TaxRateConstants.LIST_TAXRATE, modelMap);
    }
    /**
     * 获取税率设置列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findTaxRateList")
    @ResponseBody
    public List<Tax> findTaxRateList(ModelMap modelMap,Tax tax,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tax.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return taxService.findTaxRateList(tax);
    }
    /**
     * 跳转新增、修改税率设置页面
     * @param tax
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditTaxRate")
    public ModelAndView toEditTaxRate(ModelMap modelMap,Tax tax,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tax.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(tax.getPk_tax())){
    		modelMap.put("tax", taxService.findTaxRateByPk(tax));
    	}
    	return new ModelAndView(TaxRateConstants.SAVE_TAXRATE, modelMap);
    }
    /**
     * 保存税率设置
     * @param tax
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveTaxRate")
    public ModelAndView saveTaxRate(ModelMap modelMap,Tax tax,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tax.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(tax.getPk_tax())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增税率设置："+tax.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改税率设置："+tax.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        taxService.saveTaxRate(tax);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用税率设置
     * @param tax
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableTaxRate")
    public ModelAndView enableTaxRate(ModelMap modelMap,Tax tax,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tax.setEnablestate("2");
    	tax.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_tax");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_tax");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(tax.getPk_tax()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	taxService.enableTaxRate(tax);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用税率设置:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用税率设置
     * @param tax
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableTaxRate")
    public ModelAndView disableTaxRate(ModelMap modelMap,Tax tax,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tax.setEnablestate("3");
    	tax.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_tax");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_tax");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(tax.getPk_tax()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	taxService.enableTaxRate(tax);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用税率设置:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除税率设置
     * @param tax
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteTaxRate")
    @ResponseBody
    public String deleteTaxRate(Tax tax,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return taxService.deleteTaxRate(tax);
    }
	/**
	 * 跳转到税率设置界面list
	 * @param modelMap
	 * @param tax
	 * @param page
	 * @param session
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param type
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseTax")
	public ModelAndView chooseTax(ModelMap modelMap, Tax tax,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		tax.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("taxList", taxService.findTaxRateListByPage(tax,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(TaxRateConstants.CHOOSE_TAX, modelMap);
	}
}
