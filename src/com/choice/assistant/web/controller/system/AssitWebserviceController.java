package com.choice.assistant.web.controller.system;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choice.assistant.domain.Condition;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.util.uploadBill.AssitUploadBillData;
import com.choice.assistant.domain.util.uploadBill.AssitUploadBillResult;
import com.choice.assistant.service.system.AssitWebserviceService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.LogUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.assistant.util.web.MallAssitUtil;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.domain.Acct;
import com.choice.scm.domain.Positn;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.PositnService;
/**
 * 商城接口调用
 * @author ZGL
 *
 */
@Controller
@RequestMapping(value = "assitWebservice")
public class AssitWebserviceController {
	@Autowired
	private AssitWebserviceService assitWebserviceService;
	@Autowired
	private PositnService positnService;
	@Autowired
	private AcctService acctService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private CodeDesService codeDesService;
	
	/**
	 *  手动上传订单
	 * @param puprOrder
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value="/uploadPuprOrdr")
	@ResponseBody
	public String uploadPuprOrdr(PuprOrder puprOrder,HttpSession session) throws CRUDException, UnsupportedEncodingException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		Acct acct = acctService.findAcctById(session.getAttribute("ChoiceAcct").toString());
		String params = "",str = "";
		//获取该企业的接口登录参数
		params = AsstUtils.getParams(acct);
//        if(ValueCheck.IsEmpty(params)){
//        	System.out.println("=========================================================接口登录失败。");
//        	str= AsstUtils.errmsg;
//        	AsstUtils.errmsg = "";
//        	return str;
//        }
	    //查询上传订单数据
//		List<Map<String,Object>> listPuprOrder = assitWebserviceService.queryAllPuprOrders(puprOrder);
		//调用订单上传方法
//		str = uploadBill(listPuprOrder,params,acct,session);
		return str;
	}
	/**
	 * 订单上传接口公共方法
	 * @param listPuprOrder
	 * @param params
	 * @param acct
	 * @return
	 * @throws CRUDException
	 */
	public String uploadBill(List<Map<String,Object>> listPuprOrder,String params,Acct acct,HttpSession session) throws CRUDException{
		//接口调用方法
		String method = "order/create.action";

		String str = "";
		if(!listPuprOrder.isEmpty()){
			for(Map<String,Object> puprOrderMap : listPuprOrder){
				//判断订单供应商是否关联商城供应商
				if(ValueCheck.IsEmpty(puprOrderMap.get("JMUDELIVERCODE"))){
					LogUtil.writeToTxt(LogUtil.AUTOUPLOADBILL, "AssitWebserviceController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"===订单===="+puprOrderMap.get("vbillno")+",所选择供应商没有关联商城供应商。"+System.getProperty("line.separator"));
					return "请先关联商城供应商。";
				}
				
				int i = 1;//重置计数器，记录店铺id的标志
				String strMsg = "",finalStr="",specStr="",startStr="";
				startStr = ",\"data\":{store:[";
				PuprOrder puprOrderd = new PuprOrder();
				puprOrderd.setPk_puprorder(puprOrderMap.get("PK_PUPRORDER").toString());
				puprOrderd.setAcct(puprOrderMap.get("ACCT").toString());
				puprOrderd.setDelivercode(puprOrderMap.get("JMUDELIVERCODE").toString());//获取商城主键
				//根据订单查询订单明细
				List<Map<String,Object>> listPuprOrderd =  assitWebserviceService.queryPuprdByPk(puprOrderd);
				if(!listPuprOrderd.isEmpty()){//物资、供应商需要对应商城的编码
					String vhstore="";
					i = 1;//重置计数器
					Positn qrypositn = new Positn();
					for(Map<String,Object> puprOrderMapd : listPuprOrderd){//循环子表信息
						//查询每个物资对应的地址信息
						
						qrypositn.setCode(puprOrderMapd.get("POSITNCODE").toString());
						Positn positn = positnService.findPositnByCode(qrypositn);
						if(ValueCheck.IsNotEmpty(positn)){
							if(ValueCheck.IsNotEmpty(positn.getVmalladdrid())){
								puprOrderMapd.put("VADDRID", positn.getVmalladdrid());//将地址主键暂存到对象中
								
								if(vhstore.equals(puprOrderMapd.get("VHSTORE"))){//如果店铺id没有变
									if(ValueCheck.IsEmpty(puprOrderMapd.get("VHSPEC"))){
										LogUtil.writeToTxt(LogUtil.AUTOUPLOADBILL, "AssitWebserviceController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"订单"+puprOrderMap.get("vbillno")+"，物资："+puprOrderMapd.get("vmaterialname")+"===没有关联商城物资。"+System.getProperty("line.separator"));
										return "订单"+puprOrderMap.get("VBILLNO")+"，物资："+puprOrderMapd.get("SP_NAME")+"请先关联商城物资。";
									}
									specStr += ",{cartId:'','specId':'"+puprOrderMapd.get("VHSPEC")+"','price':'"+puprOrderMapd.get("SP_PRICE")+"'," +//商品规格id、价格
													"'amount':'"+puprOrderMapd.get("NCNT").toString()+"'," +//采购数量
													"'goodsTotalPrice':'"+puprOrderMapd.get("NMONEY").toString()+"'," +//商品总价
													"'orderTotalPrice':'"+puprOrderMapd.get("NMONEY").toString()+"'," +//订单总价
													"'receiptAddressId':'"+puprOrderMapd.get("VADDRID").toString()+"'," +//收货地址编号
													"'shippingId':''," +//配送方式编号
													"'freight':'0'," +//运费
													"'expectTime':'"+ValueCheck.formatStringForBlank(puprOrderMapd.get("DHOPEDATE"))+"'," +//期望到达时间
													"'realTime':''," +//实际收货时间
													"'description':'"+puprOrderMapd.get("VMEMO")+"'}";
								}else{//如果店铺id变了,重新生成一个订单对象并将订单金额置空
									if(i!=1){//初始第一次不加结束字符串
										strMsg += specStr+"]}";
									}
									if(ValueCheck.IsEmpty(puprOrderMapd.get("VHSTORE"))){
										str = "商品:"+puprOrderMapd.get("SP_NAME")+"没有关联商城物资，该订单不能提交到商城。";
										LogUtil.writeToTxt(LogUtil.AUTOUPLOADBILL, "AssitWebserviceController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+str+System.getProperty("line.separator"));
										return str;
									}
									vhstore = puprOrderMapd.get("VHSTORE").toString();
									finalStr += strMsg;
									specStr = "";
									strMsg = ",{id:'"+puprOrderMapd.get("VHSTORE")+"'" +//店铺id
											",outsideSn:'"+puprOrderMap.get("VBILLNO")+"'"+//订单编码（采购助手）
											",buyMsg:'"+puprOrderMap.get("VMEMO")+"'"+//采购商留言
											",period:'"+puprOrderMap.get("IACCTTYPE")+"'"+//账期天数
											",invoiceId:''" +//发票
											",client:'AST'" +//平台代码
//											",shippingId:''" +
											",'spec':[";
									if(ValueCheck.IsEmpty(puprOrderMapd.get("VHSPEC"))){
										LogUtil.writeToTxt(LogUtil.AUTOUPLOADBILL, "AssitWebserviceController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"订单"+puprOrderMap.get("vbillno")+"，物资："+puprOrderMapd.get("vmaterialname")+"===关联的商城供应商与供应商关联的商城供应商不对应，不能上传到商城。"+System.getProperty("line.separator"));
										return "订单"+puprOrderMap.get("VBILLNO")+"，物资："+puprOrderMapd.get("SP_NAME")+"关联的商城供应商与供应商关联的商城供应商不对应，不能上传到商城。";
									}
									specStr = "{cartId:'','specId':'"+puprOrderMapd.get("VHSPEC")+"','price':'"+puprOrderMapd.get("SP_PRICE")+"'," +//商品规格id、价格
										"'amount':'"+puprOrderMapd.get("NCNT").toString()+"'," +//采购数量
										"'goodsTotalPrice':'"+puprOrderMapd.get("NMONEY").toString()+"'," +//商品总价
										"'orderTotalPrice':'"+puprOrderMapd.get("NMONEY").toString()+"'," +//订单总价
										"'receiptAddressId':'"+puprOrderMapd.get("VADDRID").toString()+"'," +//收货地址编号
										"'shippingId':''," +//配送方式编号
										"'freight':'0'," +//运费
										"'expectTime':'"+ValueCheck.formatStringForBlank(puprOrderMapd.get("DHOPEDATE"))+"'," +//期望到达时间
										"'realTime':''," +//实际收货时间
										"'description':'"+puprOrderMapd.get("VMEMO")+"'}";
									i++;
								}
							}else{
								str = "没有上传地址，请到组织结构中修改相应组织保存以上传地址。";
								LogUtil.writeToTxt(LogUtil.AUTOUPLOADBILL, "AssitWebserviceController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+str+System.getProperty("line.separator"));
								break;
							}
						}else{
							str = "订单===="+puprOrderMap.get("vbillno")+",没有组织，请确认该订单组织已启用或存在。";
							LogUtil.writeToTxt(LogUtil.AUTOUPLOADBILL, "AssitWebserviceController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+str+System.getProperty("line.separator"));
							break;
						}
					}
					//最后的购物车商品字符串加结束符号]}
					finalStr += strMsg +specStr+"]}";
					//整个订单加结束字符串]}
					finalStr = startStr+finalStr.substring(1)+"]}";
					System.out.println(finalStr);
					//调用接口
					AssitUploadBillData result = new AssitUploadBillData();
					try {
						result = MallAssitUtil.formatUploadBillResult(MallAssitUtil.getResult(method, params+finalStr, acct));
					} catch (UnsupportedEncodingException e) {
						str = "调用订单上传接口出错，详情请联系众美联集团处理。";
						e.printStackTrace();
						LogUtil.writeToTxt(LogUtil.AUTOUPLOADBILL, "AssitWebserviceController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+str+e.getMessage()+System.getProperty("line.separator"));
					}
				   if(ValueCheck.IsNotEmpty(result)){//获取返回数据并根据返回数据进行下一步操作
					   if(ValueCheck.IsEmpty(result.getErrmsg())){
						   if(ValueCheck.IsNotEmpty(result.getData())){
							   if(!result.getData().getResult().isEmpty() && result.getData().getResult().size()>0){
								   AssitUploadBillResult assitUploadBillResult = result.getData().getResult().get(0);
								   if(ValueCheck.IsNotEmpty(assitUploadBillResult)){
									   if(!assitUploadBillResult.getOrder().isEmpty() && assitUploadBillResult.getOrder().size()>0){
										   PuprOrder puprOrderdd = new PuprOrder();
											puprOrderdd.setIstate(8);//待收货(待备货)
											//更新助手订单状态
											puprOrderdd.setPk_puprorder(puprOrderMap.get("PK_PUPRORDER").toString());
											puprOrderdd.setVbillsn(assitUploadBillResult.getOrder().get(0).getId());
					 	 	        		puprOrderdd.setIsdeal(1);
					 	 	        		puprOrderdd.setTs(DateFormat.getTs());
					 	 	        		puprOrderdd.setAcct(puprOrderMap.get("ACCT").toString());
					 	 	        		assitWebserviceService.updatePuprOrdr(puprOrderdd);
									   }else{
										   return "返回的订单数据为空";
									   }
								   }else{
									   return "返回的订单数据为空";
								   }
							   }else{
								   return "返回的订单数据为空";
							   }
						   }else{
						   return "返回的订单数据为空";
						   }
					   }else{
						   String errmsg = result.getErrmsg();
		 	 	        	if("SYS ERROR".equals(errmsg)){
		 	 	        		return "调用商城接口出错，请联系商城解决。";
		 	 	        	}else{
		 	 	        		if(errmsg.indexOf("【错误】")>=0){
		 	 	        			errmsg = errmsg.substring(errmsg.indexOf("【错误】")+4);
		 	 	        		}
		 	 	        		return errmsg;
		 	 	        	}
					   }
					   str = "1";
				   }else{
						   return "返回的订单数据为空";
					   }
				}else{
					   return "订单明细为空";
			   }
			}
		}else{
			   return "请选择要上传的数据";
	   }
		return str;
	}
	/**
	 *  获取商城订单列表
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/getMallOrder")
	@ResponseBody
	public String getMallOrder(Condition condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return assitWebserviceService.getMallOrderList(condition,session);
	}
	/**
	 *  获取商城订单详细信息
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/getMallOrderDetail")
	@ResponseBody
	public String getMallOrderDetail(Condition condition,HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(ValueCheck.IsNotEmpty(condition.getId())){
			return assitWebserviceService.getMallOrderDetail(condition,session);
		}else{
			return null;
		}
	}

	
}