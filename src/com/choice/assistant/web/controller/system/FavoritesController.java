package com.choice.assistant.web.controller.system;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.FavoritesConstants;
import com.choice.assistant.domain.Condition;
import com.choice.assistant.domain.material.ReviMaterial;
import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.system.CollDeliver;
import com.choice.assistant.domain.system.CollMaterial;
import com.choice.assistant.service.system.FavoritesService;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.Page;

/**
 * 查询
 * Created by zhb
 */
@Controller
@RequestMapping(value = "favorites")
public class FavoritesController {
    @Autowired
    private FavoritesService favoritesService;

    /**
     * 获取查询
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     * @throws UnsupportedEncodingException 
     */
    @RequestMapping(value = "/list")
    public ModelAndView findInquiry(ModelMap modelMap,Condition condition,Page page,HttpSession session) throws CRUDException, UnsupportedEncodingException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("condition", condition);
        return new ModelAndView(FavoritesConstants.LIST_FASTSEARCH, modelMap);
    }
	/**
	 * 跳转到具体查询详细页面
	 * @param modelMap
	 * @return 
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	@RequestMapping("/loadSubPage")
	public ModelAndView loadSubPage(ModelMap modelMap,CollMaterial collMaterial,CollDeliver collDeliver,Condition condition,String flag,HttpSession session,Page page) throws CRUDException, UnsupportedEncodingException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if("sp".equals(flag)){
			collMaterial = new CollMaterial();
			collMaterial.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<CollMaterial> collMaterialList = favoritesService.findAllCollMaterial(collMaterial,page);
			modelMap.put("collMaterialList", collMaterialList);
			modelMap.put("pageobj", page);
			return new ModelAndView(FavoritesConstants.SHANGPIN, modelMap);
		}else if("gys".equals(flag)){
			collDeliver = new CollDeliver();
			collDeliver.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<CollDeliver> collDeliverList = favoritesService.findAllCollDeliver(collDeliver,page);
			modelMap.put("collDeliverList", collDeliverList);
			modelMap.put("pageobj", page);
			return new ModelAndView(FavoritesConstants.GONGYINGSHANG, modelMap);
		}
		return null;
	}
	/**
	 * 删除所选行商品
	 * @param modelMap
	 * @param collMaterial
	 * @param id
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deletesp")
	@ResponseBody
	public String deletesp(ModelMap modelMap,CollMaterial collMaterial, String id,HttpSession session,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		collMaterial.setPk_collmaterial(id);
		collMaterial.setAcct(session.getAttribute("ChoiceAcct").toString());
		favoritesService.deletespFavoritesById(collMaterial);
		return "T";
	}
	/**
	 * 删除所选行供应商
	 * @param modelMap
	 * @param collDeliver
	 * @param id
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deletegys")
	@ResponseBody
	public String deletegys(ModelMap modelMap,CollDeliver collDeliver, String id,HttpSession session,Page page) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		collDeliver.setPk_colldeliver(id);
		collDeliver.setAcct(session.getAttribute("ChoiceAcct").toString());
		favoritesService.deletegysFavoritesById(collDeliver);
		return "T";
	}
	/**
	 * 跳转商品详情页面
	 * @param modelMap
	 * @param condition
	 * @param session
	 * @return
	 * @throws CRUDException
	 * @throws UnsupportedEncodingException 
	 */
    @RequestMapping(value = "/toFavoritesSpInfo")
    public ModelAndView toFavoritesSpInfo(ModelMap modelMap,CollMaterial collMaterial,String spid,Condition condition,HttpSession session) throws CRUDException, UnsupportedEncodingException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        collMaterial = new CollMaterial();
		collMaterial.setAcct(session.getAttribute("ChoiceAcct").toString());
		collMaterial.setPk_collmaterial(spid);
		CollMaterial collMaterialListbyspid = favoritesService.findAllCollMaterialbyspid(collMaterial);
		modelMap.put("collMaterialListbyspid", collMaterialListbyspid);
		return new ModelAndView(FavoritesConstants.FSHANGPININFO, modelMap);
    }
    /**
     * 跳转供应商详情页面
     * @param modelMap
     * @param condition
     * @param session
     * @return
     * @throws CRUDException
     * @throws UnsupportedEncodingException 
     */
    @RequestMapping(value = "/toFavoritesGysInfo")
    public ModelAndView toFavoritesGysInfo(ModelMap modelMap,CollDeliver collDeliver,String gysid,Condition condition,HttpSession session) throws CRUDException, UnsupportedEncodingException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	collDeliver = new CollDeliver();
		collDeliver.setAcct(session.getAttribute("ChoiceAcct").toString());
		collDeliver.setPk_colldeliver(gysid);
		CollDeliver collDeliverListbygysid = favoritesService.findAllCollDeliverbygysid(collDeliver);
		modelMap.put("collDeliverListbygysid", collDeliverListbygysid);
		return new ModelAndView(FavoritesConstants.FGONGYINGSHANGINFO, modelMap);
    }
    /**
     * 待审核物资
     * @param modelMap
     * @param collMaterial
     * @param reviMaterial
     * @param session
     * @param page
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/addInAuditMaterial")
	public ModelAndView addInAuditMaterial(ModelMap modelMap,String pk_collmaterial,CollMaterial collMaterial, ReviMaterial reviMaterial,HttpSession session,Page page) throws Exception {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	collMaterial = new CollMaterial();
		collMaterial.setAcct(session.getAttribute("ChoiceAcct").toString());
		collMaterial.setPk_collmaterial(pk_collmaterial);
		CollMaterial collMaterialListbyspid = favoritesService.findAllCollMaterialbyspid(collMaterial);
		//判断加入待审核物资是否重复
		if(ValueCheck.IsEmpty(favoritesService.findAuditbypkss(collMaterialListbyspid.getPk_collmaterial(),session.getAttribute("ChoiceAcct").toString()))){
	    	reviMaterial = new ReviMaterial();
	    	reviMaterial.setAcct(session.getAttribute("ChoiceAcct").toString());
	    	reviMaterial.setPk_revimaterial(collMaterialListbyspid.getPk_collmaterial());
	    	reviMaterial.setPk_material(collMaterialListbyspid.getPk_mallmaterial());
	    	reviMaterial.setVcode(collMaterialListbyspid.getVmaterialcode());
	    	reviMaterial.setVname(collMaterialListbyspid.getVmaterialname());
	    	reviMaterial.setVspecfication(collMaterialListbyspid.getVmaterialspec());
	    	reviMaterial.setPk_unit(collMaterialListbyspid.getNmatprice());
	    	reviMaterial.setVdelivername(collMaterialListbyspid.getVmallsupplier());
	    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	    	reviMaterial.setTs(df.format(new Date()));//当前时间
	    	reviMaterial.setDr(0);
	    	favoritesService.saveAuditMaterial(reviMaterial);
	    	return new ModelAndView(StringConstant.ACTION_DONE);
		}
		return new ModelAndView(StringConstant.ERROR_DONE);
//    	collMaterial = new CollMaterial();
//		collMaterial.setAcct(session.getAttribute("ChoiceAcct").toString());
//		List<CollMaterial> collMaterialList = favoritesService.findAllCollMaterial(collMaterial,page);
//		modelMap.put("collMaterialList", collMaterialList);
//		modelMap.put("pageobj", page);
//		return new ModelAndView(FavoritesConstants.SHANGPIN, modelMap);
	}
    /**
     * 待审核供应商
     * @param modelMap
     * @param collDeliver
     * @param auditsupplier
     * @param session
     * @param page
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/addInAuditDeliver")
	public ModelAndView addInAuditMaterial(ModelMap modelMap,String pk_colldeliver,CollDeliver collDeliver, Auditsupplier auditsupplier,HttpSession session,Page page) throws Exception {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	collDeliver = new CollDeliver();
		collDeliver.setAcct(session.getAttribute("ChoiceAcct").toString());
		collDeliver.setPk_colldeliver(pk_colldeliver);
		CollDeliver collDeliverListbygysid = favoritesService.findAllCollDeliverbygysid(collDeliver);
		//判断加入供应商待审核是否重复
		if(ValueCheck.IsEmpty(favoritesService.findAuditbypks(collDeliverListbygysid.getPk_colldeliver(),session.getAttribute("ChoiceAcct").toString()))){
			auditsupplier = new Auditsupplier();
	    	auditsupplier.setAcct(session.getAttribute("ChoiceAcct").toString());
	    	auditsupplier.setPk_auditsupplier(collDeliverListbygysid.getPk_colldeliver());
	    	auditsupplier.setPk_supplier(collDeliverListbygysid.getPk_deliver());
	    	auditsupplier.setVcode(collDeliverListbygysid.getVdelivercode());
	    	auditsupplier.setVname(collDeliverListbygysid.getVdelivername());
	    	auditsupplier.setVlevel(collDeliverListbygysid.getVdeliverlevel());
	    	auditsupplier.setVsuppliertele(collDeliverListbygysid.vdeliverphone);
	    	auditsupplier.setVsupplieraddress(collDeliverListbygysid.getVdeliveraddress());
	    	auditsupplier.setVmemo(collDeliverListbygysid.getVmemo());
	    	auditsupplier.setDr("0");
	    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	    	auditsupplier.setTs(df.format(new Date()));//当前时间
	    	favoritesService.saveAuditDeliver(auditsupplier);
	    	return new ModelAndView(StringConstant.ACTION_DONE);
		}
		return new ModelAndView(StringConstant.ERROR_DONE);
	}
    /**
     * 查询当前物资是不是已经存到收藏夹了
     * @param modelMap
     * @param collMaterial
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value="/getCollMaterial")
    @ResponseBody
    public String getCollMaterial(ModelMap modelMap,CollMaterial collMaterial,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	collMaterial.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String res = "1";
    	if(ValueCheck.IsEmpty(favoritesService.findAllCollMaterialbyspid(collMaterial))){
    		res = "0";
    	}
    	return res;
    }
    /**
     * 查询当前供应商是不是已经存到收藏夹了
     * @param modelMap
     * @param collMaterial
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value="/getCollSupplier")
    @ResponseBody
    public String getCollSupplier(ModelMap modelMap,CollDeliver collDeliver,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	collDeliver.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String res = "1";
    	if(ValueCheck.IsEmpty(favoritesService.findAllCollDeliverbygysid(collDeliver))){
    		res = "0";
    	}
    	return res;
    }
}
