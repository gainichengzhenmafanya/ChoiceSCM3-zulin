package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.ContracttermsConstants;
import com.choice.assistant.domain.system.Contractterms;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 合同条款类型
 * Created by zgl
 */
@Controller
@RequestMapping(value = "contractterms")
public class ContracttermsController {
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取合同条款类型
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findContractterms(ModelMap modelMap,Contractterms contractterms,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        contractterms.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("contracttermsList", contracttermsService.findContracttermsListByPage(contractterms,page));
		modelMap.put("contractterms", contractterms);
		modelMap.put("pageobj", page);
        return new ModelAndView(ContracttermsConstants.LIST_CONTRACTTERMS, modelMap);
    }
    /**
     * 获取合同条款类型列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findContracttermsList")
    @ResponseBody
    public List<Contractterms> findContracttermsList(ModelMap modelMap,Contractterms contractterms,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	contractterms.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return contracttermsService.findContracttermsList(contractterms);
    }
    /**
     * 跳转新增、修改合同条款类型页面
     * @param contractterms
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditContractterms")
    public ModelAndView toEditContractterms(ModelMap modelMap,Contractterms contractterms,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	contractterms.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(contractterms.getPk_contractterms())){
    		modelMap.put("contractterms", contracttermsService.findContracttermsByPk(contractterms));
    	}
    	return new ModelAndView(ContracttermsConstants.SAVE_CONTRACTTERMS, modelMap);
    }
    /**
     * 保存合同条款类型
     * @param contractterms
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveContractterms")
    public ModelAndView saveContractterms(ModelMap modelMap,Contractterms contractterms,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	contractterms.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(contractterms.getPk_contractterms())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增合同条款类型："+contractterms.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改合同条款类型："+contractterms.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        contracttermsService.saveContractterms(contractterms);
        logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用合同条款类型
     * @param contractterms
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableContractterms")
    public ModelAndView enableContractterms(ModelMap modelMap,Contractterms contractterms,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	contractterms.setEnablestate(2);
    	contractterms.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_contractterms");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_contractterms");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(contractterms.getPk_contractterms()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	contracttermsService.enableContractterms(contractterms);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用合同条款类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用合同条款类型
     * @param contractterms
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableContractterms")
    public ModelAndView disableContractterms(ModelMap modelMap,Contractterms contractterms,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	contractterms.setEnablestate(3);
    	contractterms.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_contractterms");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_contractterms");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(contractterms.getPk_contractterms()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	contracttermsService.enableContractterms(contractterms);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用合同条款类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除合同条款类型
     * @param contractterms
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteContractterms")
    @ResponseBody
    public String deleteContractterms(Contractterms contractterms,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return contracttermsService.deleteContractterms(contractterms);
    }
	/**
	 * 跳转到合同条款类型界面list
	 * @param modelMap
	 * @param contractterms
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseContractterms")
	public ModelAndView chooseContractterms(ModelMap modelMap, Contractterms contractterms,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		contractterms.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("contracttermsList", contracttermsService.findContracttermsListByPage(contractterms,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(ContracttermsConstants.CHOOSE_CONTRACTTERMS, modelMap);
	}
}
