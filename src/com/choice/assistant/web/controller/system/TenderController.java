package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.TenderConstants;
import com.choice.assistant.domain.system.Tender;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.TenderService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 招标类型
 * Created by zgl
 */
@Controller
@RequestMapping(value = "tender")
public class TenderController {
    @Autowired
    private TenderService tenderService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取招标类型
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findTender(ModelMap modelMap,Tender tender,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        tender.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("tenderList", tenderService.findTenderListByPage(tender,page));
		modelMap.put("tender", tender);
		modelMap.put("pageobj", page);
        return new ModelAndView(TenderConstants.LIST_INQUIRY, modelMap);
    }
    /**
     * 获取招标类型列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findTenderList")
    @ResponseBody
    public List<Tender> findTenderList(ModelMap modelMap,Tender tender,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tender.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return tenderService.findTenderList(tender);
    }
    /**
     * 跳转新增、修改招标类型页面
     * @param tender
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditTender")
    public ModelAndView toEditTender(ModelMap modelMap,Tender tender,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tender.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(tender.getPk_tender())){
    		modelMap.put("tender", tenderService.findTenderByPk(tender));
    	}
    	return new ModelAndView(TenderConstants.SAVE_INQUIRY, modelMap);
    }
    /**
     * 保存招标类型
     * @param tender
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveTender")
    public ModelAndView saveTender(ModelMap modelMap,Tender tender,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tender.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(tender.getPk_tender())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增招标类型："+tender.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改招标类型："+tender.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        tenderService.saveTender(tender);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用招标类型
     * @param tender
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableTender")
    public ModelAndView enableTender(ModelMap modelMap,Tender tender,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tender.setEnablestate(2);
    	tender.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_tender");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_tender");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(tender.getPk_tender()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	tenderService.enableTender(tender);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用招标类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用招标类型
     * @param tender
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableTender")
    public ModelAndView disableTender(ModelMap modelMap,Tender tender,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	tender.setEnablestate(3);
    	tender.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_tender");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_tender");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(tender.getPk_tender()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	tenderService.enableTender(tender);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用招标类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除招标类型
     * @param tender
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteTender")
    @ResponseBody
    public String deleteTender(Tender tender,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return tenderService.deleteTender(tender);
    }
	/**
	 * 跳转到招标类型界面list
	 * @param modelMap
	 * @param tax
	 * @param page
	 * @param session
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param type
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseTender")
	public ModelAndView chooseTender(ModelMap modelMap, Tender tender,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		tender.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("tenderList", tenderService.findTenderListByPage(tender,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(TenderConstants.CHOOSE_Tender, modelMap);
	}
}
