package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.OtherpayConstants;
import com.choice.assistant.domain.system.Otherpay;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.OtherpayService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 其他应付款
 * Created by zgl
 */
@Controller
@RequestMapping(value = "otherpay")
public class OtherpayController {
    @Autowired
    private OtherpayService otherpayService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取其他应付款
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findOtherpay(ModelMap modelMap,Otherpay otherpay,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("otherpayList", otherpayService.findOtherpayListByPage(otherpay,page));
		modelMap.put("otherpay", otherpay);
		modelMap.put("pageobj", page);
        return new ModelAndView(OtherpayConstants.LIST_OTHERPAY, modelMap);
    }
    /**
     * 获取其他应付款列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findOtherpayList")
    @ResponseBody
    public List<Otherpay> findOtherpayList(ModelMap modelMap,Otherpay otherpay,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return otherpayService.findOtherpayList(otherpay);
    }
    /**
     * 获取其他应付款前10条
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findTopOtherpay")
    @ResponseBody
    public List<Otherpay> findTopOtherpay(ModelMap modelMap,Otherpay otherpay,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return otherpayService.findTopOtherpay(otherpay);
    }
    /**
     * 跳转新增、修改其他应付款页面
     * @param otherpay
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditOtherpay")
    public ModelAndView toEditOtherpay(ModelMap modelMap,Otherpay otherpay,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(otherpay.getPk_otherpay())){
    		modelMap.put("otherpay", otherpayService.findOtherpayByPk(otherpay));
    	}
    	return new ModelAndView(OtherpayConstants.SAVE_OTHERPAY, modelMap);
    }
    /**
     * 保存其他应付款
     * @param otherpay
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveOtherpay")
    public ModelAndView saveOtherpay(ModelMap modelMap,Otherpay otherpay,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(otherpay.getPk_otherpay())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增其他应付款："+otherpay.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改其他应付款："+otherpay.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        otherpayService.saveOtherpay(otherpay);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用其他应付款
     * @param otherpay
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableOtherpay")
    public ModelAndView enableOtherpay(ModelMap modelMap,Otherpay otherpay,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	otherpay.setEnablestate(2);
    	otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_otherpay");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_otherpay");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(otherpay.getPk_otherpay()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	otherpayService.enableOtherpay(otherpay);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用其他应付款:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用其他应付款
     * @param otherpay
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableOtherpay")
    public ModelAndView disableOtherpay(ModelMap modelMap,Otherpay otherpay,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	otherpay.setEnablestate(3);
    	otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_otherpay");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_otherpay");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(otherpay.getPk_otherpay()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	otherpayService.enableOtherpay(otherpay);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用其他应付款:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除其他应付款
     * @param otherpay
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteOtherpay")
    @ResponseBody
    public String deleteOtherpay(Otherpay otherpay,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return otherpayService.deleteOtherpay(otherpay);
    }
	/**
	 * 跳转到其他应付款界面list
	 * @param modelMap
	 * @param otherpay
	 * @param page
	 * @param session
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param type
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseOtherpay")
	public ModelAndView chooseOtherpay(ModelMap modelMap, Otherpay otherpay,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("otherpayList", otherpayService.findOtherpayListByPage(otherpay,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(OtherpayConstants.CHOOSE_OTHERPAY, modelMap);
	}
}
