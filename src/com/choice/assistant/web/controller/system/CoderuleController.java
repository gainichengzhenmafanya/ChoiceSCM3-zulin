package com.choice.assistant.web.controller.system;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.system.Coderule;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;

/**
 * 编码规则
 * Created by mc on 14-10-18.
 */
@Controller
@RequestMapping(value = "coderule")
public class CoderuleController {
    @Autowired
    private CoderuleService coderuleService;

    /**
     * 获取编码规则
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findCoderule(ModelMap modelMap,HttpSession session,Coderule coderule) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        coderule.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("coderuleList", coderuleService.findcoderuleList(coderule));
        return new ModelAndView(CoderuleConstants.LIST_CODERULE, modelMap);
    }

    /**
     * 获取单据规则
     * @param modelMap
     * @param coderule
     * @return
     */
    @RequestMapping("/code")
    public ModelAndView findCode(ModelMap modelMap,HttpSession session,Coderule coderule) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        coderule.setIsrule(1);
        coderule.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("coderuleList", coderuleService.findcoderuleList(coderule));
        return new ModelAndView(CoderuleConstants.CODE,modelMap);
    }

    /**
     * 获取编码规则
     * @param modelMap
     * @param coderule
     * @return
     */
    @RequestMapping(value = "/order")
    public ModelAndView findOrder(ModelMap modelMap,HttpSession session,Coderule coderule) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        coderule.setIsrule(2);
        coderule.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("coderuleList", coderuleService.findcoderuleList(coderule));
        return new ModelAndView(CoderuleConstants.ORDER,modelMap);
    }

    /**
     * 编码规则修改
     * @param coderule
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public String updateCoderule(Coderule coderule,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try{
            coderule.setAcct(session.getAttribute("ChoiceAcct").toString());
            return coderuleService.updateCoderule(coderule);
        }catch (CRUDException e){
            e.printStackTrace();
        }
        return "error";
    }

    /**
     * 根据编码规则生成编码
     * @param coderule
     * @return
     */
    @RequestMapping("/getCode")
    @ResponseBody
    public String getCode(HttpSession session,Coderule coderule){
        Map<String,Object> map=new HashMap<String, Object>();
        try {
            coderule.setAcct(session.getAttribute("ChoiceAcct").toString());
            map.put("code",coderuleService.getCode(coderule));
            map.put("msg","ok");
            String result=JSONObject.fromObject(map).toString();
            return result;
        } catch (Exception e) {
            map.put("msg",e.getMessage());
            return JSONObject.fromObject(map).toString();
        }
    }

    /**
     * 编码规则验证
     * @param codeMark 编码PK CoderuleConstants 内选择
     * @param code 编码
     * @param parent 上级编码
     * @param lvl 级别
     * @return
     */
    @RequestMapping(value = "/format")
    @ResponseBody
    public String codeFormat(String codeMark,String code,String parent,Integer lvl,HttpSession session){
        return coderuleService.codeFormat(codeMark,code,parent,lvl,session.getAttribute("ChoiceAcct").toString());
    }

    /**
     * 单据规则验证
     * @param codeMark
     * @param code
     * @return
     */
    @RequestMapping(value = "/codeformat")
    @ResponseBody
    public String codeFormat(String codeMark,String code,HttpSession session){
        return coderuleService.codeFormat(codeMark,code,session.getAttribute("ChoiceAcct").toString());
    }
    /**
     * 获取指定的编码规则字符串X-XX-XX
     * @param codeMark
     * @param code
     * @return
     * @throws CRUDException 
     */
    @RequestMapping(value = "/getFormat")
    @ResponseBody
    public String getFormat(String pk_id,String code,HttpSession session) throws CRUDException{
    	return coderuleService.getFormat(pk_id,session.getAttribute("ChoiceAcct").toString());
    }

}
