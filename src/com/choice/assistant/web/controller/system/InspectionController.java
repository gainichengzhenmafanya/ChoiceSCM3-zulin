package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.InspectionConstants;
import com.choice.assistant.domain.system.Inspection;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.InspectionService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 验货类型
 * Created by zgl
 */
@Controller
@RequestMapping(value = "inspection")
public class InspectionController {
    @Autowired
    private InspectionService inspectionService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取验货类型
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findInspection(ModelMap modelMap,Inspection inspection,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("inspectionList", inspectionService.findInspectionListByPage(inspection,page));
		modelMap.put("inspection", inspection);
		modelMap.put("pageobj", page);
        return new ModelAndView(InspectionConstants.LIST_INSPECTION, modelMap);
    }
    /**
     * 获取验货类型列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findInspectionList")
    @ResponseBody
    public List<Inspection> findInspectionList(ModelMap modelMap,Inspection inspection,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return inspectionService.findInspectionList(inspection);
    }
    /**
     * 跳转新增、修改验货类型页面
     * @param inspection
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditInspection")
    public ModelAndView toEditInspection(ModelMap modelMap,Inspection inspection,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(inspection.getPk_inspection())){
    		modelMap.put("inspection", inspectionService.findInspectionByPk(inspection));
    	}
    	return new ModelAndView(InspectionConstants.SAVE_INSPECTION, modelMap);
    }
    /**
     * 保存验货类型
     * @param inspection
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveInspection")
    public ModelAndView saveInspection(ModelMap modelMap,Inspection inspection,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(inspection.getPk_inspection())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增验货类型："+inspection.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改验货类型："+inspection.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        inspectionService.saveInspection(inspection);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用验货类型
     * @param inspection
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableInspection")
    public ModelAndView enableInspection(ModelMap modelMap,Inspection inspection,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inspection.setEnablestate(2);
    	inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_inspection");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_inspection");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(inspection.getPk_inspection()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	inspectionService.enableInspection(inspection);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用验货类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用验货类型
     * @param inspection
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableInspection")
    public ModelAndView disableInspection(ModelMap modelMap,Inspection inspection,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inspection.setEnablestate(3);
    	inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_inspection");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_inspection");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(inspection.getPk_inspection()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	inspectionService.enableInspection(inspection);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用验货类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除验货类型
     * @param inspection
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteInspection")
    @ResponseBody
    public String deleteInspection(Inspection inspection,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return inspectionService.deleteInspection(inspection);
    }
	/**
	 * 跳转到验货类型界面list
	 * @param modelMap
	 * @param inquiry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseInspection")
	public ModelAndView chooseInspection(ModelMap modelMap, Inspection inspection,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("inspectionList", inspectionService.findInspectionListByPage(inspection,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(InspectionConstants.CHOOSE_INSPECTION, modelMap);
	}
	
	/**
     * 选择验货类型
     * @param inspection
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/findAllInspection")
    @ResponseBody
    public Object findAllInspection(Inspection inspection,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return inspectionService.findInspectionList(inspection);
    }
}
