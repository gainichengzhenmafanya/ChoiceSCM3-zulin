package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.PayMethodConstants;
import com.choice.assistant.domain.system.PayMethod;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.PayMethodService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 付款方式
 * Created by zgl
 */
@Controller
@RequestMapping(value = "payMethod")
public class PayMethodController {
    @Autowired
    private PayMethodService payMethodService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取其他应付款
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findPayMethod(ModelMap modelMap,PayMethod payMethod,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("payMethodList", payMethodService.findPayMethodListByPage(payMethod,page));
		modelMap.put("payMethod", payMethod);
		modelMap.put("pageobj", page);
        return new ModelAndView(PayMethodConstants.LIST_PAYMETHOD, modelMap);
    }
    /**
     * 获取其他应付款列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findPayMethodList")
    @ResponseBody
    public List<PayMethod> findPayMethodList(ModelMap modelMap,PayMethod payMethod,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return payMethodService.findPayMethodList(payMethod);
    }
    /**
     * 获取其他应付款前10条
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findTopPayMethod")
    @ResponseBody
    public List<PayMethod> findTopPayMethod(ModelMap modelMap,PayMethod payMethod,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return payMethodService.findTopPayMethod(payMethod);
    }
    /**
     * 跳转新增、修改其他应付款页面
     * @param payMethod
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditPayMethod")
    public ModelAndView toEditPayMethod(ModelMap modelMap,PayMethod payMethod,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(payMethod.getPk_paymethod())){
    		modelMap.put("payMethod", payMethodService.findPayMethodByPk(payMethod));
    	}
    	return new ModelAndView(PayMethodConstants.SAVE_PAYMETHOD, modelMap);
    }
    /**
     * 保存其他应付款
     * @param payMethod
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/savePayMethod")
    public ModelAndView savePayMethod(ModelMap modelMap,PayMethod payMethod,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(payMethod.getPk_paymethod())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增其他应付款："+payMethod.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改其他应付款："+payMethod.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        payMethodService.savePayMethod(payMethod);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用其他应付款
     * @param payMethod
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enablePayMethod")
    public ModelAndView enablePayMethod(ModelMap modelMap,PayMethod payMethod,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	payMethod.setEnablestate(2);
    	payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_paymethod");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_paymethod");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(payMethod.getPk_paymethod()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	payMethodService.enablePayMethod(payMethod);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用其他应付款:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用其他应付款
     * @param payMethod
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disablePayMethod")
    public ModelAndView disablePayMethod(ModelMap modelMap,PayMethod payMethod,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	payMethod.setEnablestate(3);
    	payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_paymethod");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_paymethod");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(payMethod.getPk_paymethod()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	payMethodService.enablePayMethod(payMethod);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用其他应付款:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除其他应付款
     * @param payMethod
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deletePayMethod")
    @ResponseBody
    public String deletePayMethod(PayMethod payMethod,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return payMethodService.deletePayMethod(payMethod);
    }
	/**
	 * 跳转到其他应付款界面list
	 * @param modelMap
	 * @param payMethod
	 * @param page
	 * @param session
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param type
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/choosePayMethod")
	public ModelAndView choosePayMethod(ModelMap modelMap, PayMethod payMethod,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		payMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("payMethodList", payMethodService.findPayMethodListByPage(payMethod,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(PayMethodConstants.CHOOSE_PAYMETHOD, modelMap);
	}
}
