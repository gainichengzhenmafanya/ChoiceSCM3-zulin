package com.choice.assistant.web.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.constants.system.ProblemsConstants;
import com.choice.assistant.domain.system.Problems;
import com.choice.assistant.service.system.ProblemsService;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.util.Local;
import com.choice.orientationSys.util.Page;

/**
 * 常见问题
 * Created by mc on 15/2/3.
 */
@Controller
@RequestMapping("problems")
public class ProblemsController {
    @Autowired
    private ProblemsService problemsService;

    /**
     * 获取所有常见问题
     * @param modelMap
     * @param problems
     * @param page
     * @return
     */
    @RequestMapping("/findProblems")
    public ModelAndView findProblems(ModelMap modelMap,Problems problems,Page page,String mark){
        modelMap.put("pageobj",page);
        modelMap.put("problems",problems);
        modelMap.put("problemsList",problemsService.findProblems(problems,page));
        if(mark!=null){
            return new ModelAndView(ProblemsConstants.PROBLEMS_SHOW,modelMap);
        }
        return new ModelAndView(ProblemsConstants.PROBLEMS_LIST,modelMap);
    }

    /**
     * 新政修改界面
     * @param modelMap
     * @param mark
     * @return
     */
    @RequestMapping("getView")
    public ModelAndView upView(ModelMap modelMap,Problems problems,String mark){
        if("update".equals(mark)){
            modelMap.put("problems",problemsService.findProblemsById(problems));
        }
        modelMap.put("mark",mark);
        return new ModelAndView(ProblemsConstants.PROBLEMS_ADD_OR_UPDATE,modelMap);
    }

    /**
     * 新增常见问题
     * @param modelMap
     * @param problems
     * @return
     */
    @RequestMapping("/addProblems")
    public ModelAndView addProblems(ModelMap modelMap,Problems problems){
        if(problemsService.addProblems(problems)==1){
            return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
        }else{
            modelMap.put("msg", Local.show(LanguagesConstants.SAVE_ERROR));
            return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
        }
    }

    /**
     * 修改常见问题
     * @param modelMap
     * @param problems
     * @return
     */
    @RequestMapping("/update")
    public ModelAndView updateProblems(ModelMap modelMap,Problems problems){
        if(problemsService.updateProblems(problems)==1){
            return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
        }else{
            modelMap.put("msg", Local.show(LanguagesConstants.UPDATE_ERROR));
            return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
        }
    }

    /**
     * 删除常见问题
     * @param modelMap
     * @param ids
     * @return
     */
    @RequestMapping("/delProblems")
    public ModelAndView delProblems(ModelMap modelMap,String ids){
        if(problemsService.delProblems(ids)==1){
            return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
        }else{
            modelMap.put("msg", Local.show(LanguagesConstants.DELERROR));
            return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
        }
    }
}
