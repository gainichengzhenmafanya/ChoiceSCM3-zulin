package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.UnitCjmuConstants;
import com.choice.assistant.domain.system.Unit;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.UnitCjmuService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 单位
 * Created by zgl
 */
@Controller
@RequestMapping(value = "unitCjmu")
public class UnitCjmuController {
    @Autowired
    private UnitCjmuService unitCjmuService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取单位类型
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findUnitCjmu(ModelMap modelMap,Unit unitCjmu,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        unitCjmu.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("unitCjmuList", unitCjmuService.findUnitListByPage(unitCjmu,page));
		modelMap.put("unitCjmu", unitCjmu);
		modelMap.put("pageobj", page);
        return new ModelAndView(UnitCjmuConstants.LIST_UNITCJMU, modelMap);
    }
    /**
     * 获取单位类型列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findUnitList")
    @ResponseBody
    public List<Unit> findUnitList(ModelMap modelMap,Unit unitCjmu,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	unitCjmu.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return unitCjmuService.findUnitist(unitCjmu);
    }
    /**
     * 跳转新增、修改单位类型页面
     * @param unitCjmu
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditUnitCjmu")
    public ModelAndView toEditUnitCjmu(ModelMap modelMap,Unit unitCjmu,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	unitCjmu.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(unitCjmu.getPk_unit())){
    		modelMap.put("unitCjmu", unitCjmuService.findUnitByPk(unitCjmu));
    	}
    	return new ModelAndView(UnitCjmuConstants.SAVE_UNITCJMU, modelMap);
    }
    /**
     * 保存单位类型
     * @param unitCjmu
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveUnitCjmu")
    public ModelAndView saveUnitCjmu(ModelMap modelMap,Unit unitCjmu,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	unitCjmu.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(unitCjmu.getPk_unit())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增单位类型："+unitCjmu.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改单位类型："+unitCjmu.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        unitCjmuService.saveUnitCjmu(unitCjmu);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用单位类型
     * @param unitCjmu
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableUnit")
    public ModelAndView enableUnit(ModelMap modelMap,Unit unitCjmu,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	unitCjmu.setEnablestate(2);
    	unitCjmu.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_unit");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_unit");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(unitCjmu.getPk_unit()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	unitCjmuService.enableUnit(unitCjmu);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用单位类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用单位类型
     * @param unitCjmu
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableUnit")
    public ModelAndView disableUnit(ModelMap modelMap,Unit unitCjmu,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	unitCjmu.setEnablestate(3);
    	unitCjmu.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_unit");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_unit");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(unitCjmu.getPk_unit()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	unitCjmuService.enableUnit(unitCjmu);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用单位类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除单位类型
     * @param unitCjmu
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteUnit")
    @ResponseBody
    public String deleteUnit(Unit unitCjmu,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return unitCjmuService.deleteUnit(unitCjmu);
    }
	/**
	 * 跳转到单位类型界面list
	 * @param modelMap
	 * @param tax
	 * @param page
	 * @param session
	 * @param single
	 * @param callBack
	 * @param domId
	 * @param type
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseUnitCjmu")
	public ModelAndView chooseUnitCjmu(ModelMap modelMap, Unit unitCjmu,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		unitCjmu.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("unitCjmuList", unitCjmuService.findUnitListByPage(unitCjmu,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(UnitCjmuConstants.CHOOSE_UNITCJMU, modelMap);
	}
}
