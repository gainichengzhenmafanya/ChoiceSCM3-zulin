package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.InquiryConstants;
import com.choice.assistant.domain.system.Inquiry;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.InquiryService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 询价类型
 * Created by zgl
 */
@Controller
@RequestMapping(value = "inquiry")
public class InquiryController {
    @Autowired
    private InquiryService inquiryService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 获取询价类型
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findInquiry(ModelMap modelMap,Inquiry inquiry,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("inquiryList", inquiryService.findInquiryListByPage(inquiry,page));
		modelMap.put("inquiry", inquiry);
		modelMap.put("pageobj", page);
        return new ModelAndView(InquiryConstants.LIST_INQUIRY, modelMap);
    }
    /**
     * 获取询价类型列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findInquiryList")
    @ResponseBody
    public List<Inquiry> findInquiryList(ModelMap modelMap,Inquiry inquiry,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return inquiryService.findInquiryList(inquiry);
    }
    /**
     * 跳转新增、修改询价类型页面
     * @param inquiry
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditInquiry")
    public ModelAndView toEditInquiry(ModelMap modelMap,Inquiry inquiry,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(inquiry.getPk_inquiry())){
    		modelMap.put("inquiry", inquiryService.findInquiryByPk(inquiry));
    	}
    	return new ModelAndView(InquiryConstants.SAVE_INQUIRY, modelMap);
    }
    /**
     * 保存询价类型
     * @param inquiry
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveInquiry")
    public ModelAndView saveInquiry(ModelMap modelMap,Inquiry inquiry,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(inquiry.getPk_inquiry())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增询价类型："+inquiry.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改询价类型："+inquiry.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        inquiryService.saveInquiry(inquiry);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用询价类型
     * @param inquiry
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableInquiry")
    public ModelAndView enableInquiry(ModelMap modelMap,Inquiry inquiry,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inquiry.setEnablestate(2);
    	inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_inquiry");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_inquiry");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(inquiry.getPk_inquiry()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	inquiryService.enableInquiry(inquiry);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用询价类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用询价类型
     * @param inquiry
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableInquiry")
    public ModelAndView disableInquiry(ModelMap modelMap,Inquiry inquiry,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	inquiry.setEnablestate(3);
    	inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_inquiry");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_inquiry");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(inquiry.getPk_inquiry()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	inquiryService.enableInquiry(inquiry);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用询价类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除询价类型
     * @param inquiry
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteInquiry")
    @ResponseBody
    public String deleteInquiry(Inquiry inquiry,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	return inquiryService.deleteInquiry(inquiry);
    }
	/**
	 * 跳转到询价类型界面list
	 * @param modelMap
	 * @param inquiry
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseInquiry")
	public ModelAndView chooseInquiry(ModelMap modelMap, Inquiry inquiry,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("inquiryList", inquiryService.findInquiryListByPage(inquiry,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(InquiryConstants.CHOOSE_INQUIRY, modelMap);
	}
}
