package com.choice.assistant.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.system.OrgstructureConstants;
import com.choice.assistant.domain.system.OrgStructure;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.OrgstructureService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 组织结构类型
 * Created by zgl
 */
@Controller
@RequestMapping(value = "orgstructure")
public class OrgstructureController {
    @Autowired
    private OrgstructureService orgstructureService;
    @Autowired
    private ContracttermsService contracttermsService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 跳转到数据字典主页面
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/toDataDict")
    public ModelAndView toDataDict(ModelMap modelMap,HttpSession session,String cid,String url) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	modelMap.put("cid", ValueCheck.IsEmpty(cid)==true?"1001":cid);
    	modelMap.put("url", ValueCheck.IsEmpty(url)==true?"orgstructure/list.do":url);
    	return new ModelAndView(OrgstructureConstants.LIST_DATADICT, modelMap);
    }
    /**
     * 获取组织结构类型
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView findOrgstructure(ModelMap modelMap,OrgStructure orgstructure,Page page,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("orgstructureList", orgstructureService.findOrgStructureListByPage(orgstructure,page));
		modelMap.put("orgstructure", orgstructure);
		modelMap.put("pageobj", page);
        return new ModelAndView(OrgstructureConstants.LIST_ORGSTRUCTURE, modelMap);
    }
    /**
     * 获取组织结构类型列表
     * @param modelMap
     * @return
     * @throws com.choice.framework.exception.CRUDException
     */
    @RequestMapping(value = "/findOrgStructureList")
    @ResponseBody
    public List<OrgStructure> findOrgStructureList(ModelMap modelMap,OrgStructure orgstructure,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
    	return orgstructureService.findOrgStructureList(orgstructure);
    }
    /**
     * 跳转新增、修改组织结构类型页面
     * @param orgstructure
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/toEditOrgstructure")
    public ModelAndView toEditOrgstructure(ModelMap modelMap,OrgStructure orgstructure,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
    	if(ValueCheck.IsNotEmpty(orgstructure.getPk_orgstructure())){
    		modelMap.put("orgstructure", orgstructureService.findOrgStructureByPk(orgstructure));
    	}
    	return new ModelAndView(OrgstructureConstants.SAVE_ORGSTRUCTURE, modelMap);
    }
    /**
     * 保存组织结构类型
     * @param orgstructure
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/saveOrgstructure")
    public ModelAndView saveOrgstructure(ModelMap modelMap,OrgStructure orgstructure,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
		//加入日志
    	Logs logs = new Logs();
        if(ValueCheck.IsEmpty(orgstructure.getPk_orgstructure())){
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增组织结构类型："+orgstructure.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }else{
        	logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改组织结构类型："+orgstructure.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        }
        orgstructureService.saveOrgstructure(orgstructure);
		logsMapper.addLogs(logs);
        return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 启用组织结构类型
     * @param orgstructure
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/enableOrgStructure")
    public ModelAndView enableOrgStructure(ModelMap modelMap,OrgStructure orgstructure,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	orgstructure.setEnablestate(2);
    	orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_orgstructure");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","positncodestructure");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(orgstructure.getPk_orgstructure()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	orgstructureService.enableOrgStructure(orgstructure);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用组织结构类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 禁用组织结构类型
     * @param orgstructure
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/disableOrgStructure")
    public ModelAndView disableOrgStructure(ModelMap modelMap,OrgStructure orgstructure,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	orgstructure.setEnablestate(3);
    	orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_orgstructure");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","positncodestructure");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(orgstructure.getPk_orgstructure()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	orgstructureService.enableOrgStructure(orgstructure);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用组织结构类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
    /**
     * 删除组织结构类型
     * @param orgstructure
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/deleteOrgStructure")
    public ModelAndView deleteOrgStructure(ModelMap modelMap,OrgStructure orgstructure,HttpSession session) throws CRUDException {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vname");
    	paramMap.put("tableName","cscm_orgstructure");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","positncodestructure");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(orgstructure.getPk_orgstructure()));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vname");
    	}
    	orgstructureService.deleteOrgStructure(orgstructure);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除组织结构类型:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return  new ModelAndView(StringConstant.ACTION_DONE, modelMap);
    }
	/**
	 * 跳转到组织结构类型界面list
	 * @param modelMap
	 * @param orgstructure
	 * @param session
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseOrgStructure")
	public ModelAndView chooseOrgStructure(ModelMap modelMap, OrgStructure orgstructure,Page page,HttpSession session,String single,String callBack,String domId,String type) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		orgstructure.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("orgstructureList", orgstructureService.findOrgStructureListByPage(orgstructure,page));
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("type", type);
		modelMap.put("domId", domId);
		modelMap.put("pageobj", page);
		return new ModelAndView(OrgstructureConstants.CHOOSE_ORGSTRUCTURE, modelMap);
	}
}
