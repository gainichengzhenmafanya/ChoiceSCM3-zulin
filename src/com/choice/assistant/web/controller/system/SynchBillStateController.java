package com.choice.assistant.web.controller.system;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.util.asstBillList.AssitBillData;
import com.choice.assistant.service.bill.PuprOrderService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.LogUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.assistant.util.web.MallAssitUtil;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.scm.domain.Acct;
import com.choice.scm.service.AcctService;

@Controller
public class SynchBillStateController  extends TimerTask{

	@Autowired
	private AcctService acctService;
	@Autowired
	private PuprOrderService puprOrderService;

	@Override
	public void run() {

		String params = "";
		Map<String,Object> astate = new HashMap<String,Object>();
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Map<String,String> stateMap = new HashMap<String,String>();
		stateMap.put("0", "新建,1");
		stateMap.put("100", "待付款,8");//先对应成刚提交订单时的状态
		stateMap.put("200", "待收货(待备货),8");
		stateMap.put("210", "待收货(备货中),9");
		stateMap.put("220", "待收货(备货完成),10");
		stateMap.put("230", "待收货(已发货),3");
		stateMap.put("300", "已签收,4");
		stateMap.put("350", "已签收 (返修/退换货中),5");
		stateMap.put("900", "已完成,7");
		stateMap.put("400", "已取消,-1");
		Acct aryAcct = new Acct();
		List<Acct> listAcct = null;
		try {
			//查询当前企业
			listAcct = acctService.findAcct(aryAcct);
		} catch (CRUDException e1) {
			LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"==查询企业列表信息为空。错误信息："+e1.getMessage()+System.getProperty("line.separator"));
			e1.printStackTrace();
		}
		//如果不为空
		if(!listAcct.isEmpty() && listAcct.size()>0){
			for(Acct acct : listAcct){
				try {
					params = AsstUtils.getParams(acct);
				} catch (CRUDException e1) {
					LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==获取登录参数为空。错误信息："+e1.getMessage()+System.getProperty("line.separator"));
					e1.printStackTrace();
				}
				if(ValueCheck.IsEmpty(params)|| "登录商城接口时登录名或密码错误。".equals(params)){
					System.out.println("=========================================================接口登录失败。");
					LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==接口登录失败。"+System.getProperty("line.separator"));
				}else{
					DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
					PuprOrder puprOrdera = new PuprOrder();
					puprOrdera.setAcct(acct.getCode());
					List<PuprOrder> listPuprOrdr = null;
					try {
						//查询该企业下的订单(不包括未提交、已完成、已取消订单)
						listPuprOrdr = puprOrderService.queryAllPuprOrdersForAutoItf(puprOrdera);
					} catch (CRUDException e) {
						LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==查询订单失败。错误信息："+e.getMessage()+System.getProperty("line.separator"));
						e.printStackTrace();
					}
					//如果订单数据不为空
					if(!listPuprOrdr.isEmpty() && listPuprOrdr.size()>0){
						DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		     			for(PuprOrder puprOrder : listPuprOrdr){
							try {
								//调用商城接口
								AssitBillData  assitBillData = MallAssitUtil.formatBillListResult(MallAssitUtil.getResult("order/query/listForBuyer.action", params+",\"queryOutsideSn\":\""+puprOrder.getVbillno()+"\"", acct));
								//如果返回的数据不为空
								if(ValueCheck.IsNotEmpty(assitBillData) && ValueCheck.IsEmpty(assitBillData.getErrmsg())){
									//如果返回的数据不为空
						     		if(ValueCheck.IsNotEmpty(assitBillData.getData()) && ValueCheck.IsNotEmpty(assitBillData.getData().getResult()) ){
						     			//如果返回的数据不为空
					     				astate = assitBillData.getData().getResult().get(0).getState();
					     				if(ValueCheck.IsNotEmpty(astate)){
					     					String code = astate.get("code").toString();
					     					if(ValueCheck.IsNotEmpty(stateMap.get(code))){
					     						puprOrder.setIstate(Integer.parseInt(stateMap.get(code).split(",")[1]));
					     						puprOrder.setTs(DateFormat.getTs());
					     						try {
					     							//更新助手中订单状态
					     							puprOrderService.updatePuprOrdersState(puprOrder);
					     						} catch (CRUDException e) {
					     							LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==更新订单("+puprOrder.getVbillno()+")状态失败。错误信息："+e.getMessage()+System.getProperty("line.separator"));
					     							e.printStackTrace();
					     						}
					     					}else{
												LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==更新订单("+puprOrder.getVbillno()+")状态失败。没有查询到对应的商城订单的状态。"+System.getProperty("line.separator"));
											}
						     			}else{
											LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==更新订单("+puprOrder.getVbillno()+")状态失败。没有查询到对应的商城订单的状态。"+System.getProperty("line.separator"));
										}
					     			}else{
										LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==更新订单("+puprOrder.getVbillno()+")状态失败。没有查询到对应的商城订单。"+System.getProperty("line.separator"));
									}
								}else{
									LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==更新订单("+puprOrder.getVbillno()+")状态失败。没有查询到对应的商城订单。"+System.getProperty("line.separator"));
								}
							} catch (UnsupportedEncodingException e) {
								LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==调用商城接口出错，订单("+puprOrder.getVbillno()+")状态失败。错误信息："+e.getMessage()+System.getProperty("line.separator"));
     							e.printStackTrace();
							} catch (CRUDException e) {
								LogUtil.writeToTxt(LogUtil.SYNCHBILLSTATE, "SynchBillStateController.class--"+new Throwable().getStackTrace()[0].getLineNumber()+"行"+"=企业:"+acct.getCode()+"==出错,订单("+puprOrder.getVbillno()+")状态失败。错误信息："+e.getMessage()+System.getProperty("line.separator"));
     							e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}
	
}
