package com.choice.assistant.web.controller.purchaseSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.purchaseSet.OtherPayablesConstants;
import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.PurchaseSet.OtherPayablesd;
import com.choice.assistant.domain.PurchaseSet.OtherPayablesm;
import com.choice.assistant.domain.system.Otherpay;
import com.choice.assistant.service.purchaseSet.OtherPayablesService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.service.system.OtherpayService;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.ReadReportUrl;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Chkinm;
import com.choice.scm.domain.Deliver;

@Controller
@RequestMapping("otherpayables")
public class OtherPayablesController {

	@Autowired
	private OtherPayablesService otherPayablesService;
	@Autowired
	private OtherpayService otherpayService;
	@Autowired
	private CoderuleService coderuleService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 查询所有的其他应付款
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param otherPayablesm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllOtherPayablesm")
	public ModelAndView queryAllOtherPayablesm(ModelMap modelMap,HttpSession session,Page page,OtherPayablesm otherPayablesm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(ValueCheck.IsEmpty(otherPayablesm.getBdat())){
			otherPayablesm.setBdat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}
		if(ValueCheck.IsEmpty(otherPayablesm.getEdat())){
			otherPayablesm.setEdat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}
		List<OtherPayablesm> listOtherPayablesm = otherPayablesService.queryAllOtherPayablesm(otherPayablesm,page);
		String pk_otherPayables = "";
		if (listOtherPayablesm.size()>0) {
			pk_otherPayables = listOtherPayablesm.get(0).getPk_otherpayables();
		}
		modelMap.put("listotherpayablesm", listOtherPayablesm);
		modelMap.put("pk_otherpayables", pk_otherPayables);
		modelMap.put("otherPayablesm", otherPayablesm);
		modelMap.put("pageobj", page);
		return new ModelAndView(OtherPayablesConstants.LISTOTHERPAYABLESM,modelMap);
	}
	
	/**
	 * 查询所有的其他应付款子表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param otherPayablesm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllOtherPayablesd")
	public ModelAndView queryAllOtherPayablesd(ModelMap modelMap,HttpSession session,String pk_otherpayablesm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=pk_otherpayablesm && !"".equals(pk_otherpayablesm)){
			OtherPayablesd otherPayablesd = new OtherPayablesd();
			otherPayablesd.setPk_otherpayables(pk_otherpayablesm);
			otherPayablesd.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<OtherPayablesd> listOtherPayablesd = otherPayablesService.queryAllOtherPayablesd(otherPayablesd);
			double amt = 0;
			for(OtherPayablesd other:listOtherPayablesd){
				amt += other.getNmoney();
			}
			modelMap.put("listotherpayablesd", listOtherPayablesd);
			modelMap.put("length", listOtherPayablesd.size());
			modelMap.put("summoney", amt);
		}
		return new ModelAndView(OtherPayablesConstants.LISTOTHERPAYABLESD,modelMap);
	}
	
	/**
	 * 跳转到添加其他应付款的页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddOtherPayablesm")
	public ModelAndView toAddOtherPayablesm(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		OtherPayablesm otherPayablesm = new OtherPayablesm();
		otherPayablesm.setVbillno(coderuleService.getCode(CoderuleConstants.OTHERPAYABLES,session.getAttribute("ChoiceAcct").toString()));
		otherPayablesm.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("otherpayablesm", otherPayablesm);
		return new ModelAndView(OtherPayablesConstants.ADDOTHERPAYABLESD,modelMap);
	}
	
	/**
	 * 添加其他应付款
	 * @param otherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	@RequestMapping(value="/addOtherPayablesm")
	@ResponseBody
	public String addOtherPayablesm(OtherPayablesm otherPayablesm,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		otherPayablesm.setCreator(session.getAttribute("accountName").toString());
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		otherPayablesm.setDbilldate(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDD));
		otherPayablesm.setIstate(1);
		for (int i = 0; i < otherPayablesm.getOtherpayablesdList().size(); i++) {
			otherPayablesm.getOtherpayablesdList().get(i).setAcct(session.getAttribute("ChoiceAcct").toString());
		}
		String res =  otherPayablesService.addOtherPayablesm(otherPayablesm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增其他应付款:"+otherPayablesm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 删除订单信息
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteOtherPayablesm")
	public ModelAndView deleteOtherPayablesm(ModelMap modelMap,HttpSession session,String pks)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String names = "";
		OtherPayablesm otherPayablesm = new OtherPayablesm();
		List<String> ids = Arrays.asList(pks.split(","));
		otherPayablesm.setPklist(ids);
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<OtherPayablesm> listOtherPayablesms = otherPayablesService.queryAllOtherPayablesm(otherPayablesm, page);
		if(!listOtherPayablesms.isEmpty()){
			for(OtherPayablesm otherPayablesms : listOtherPayablesms){
				names += ","+otherPayablesms.getVbillno();
			}
		}
		otherPayablesService.deleteOtherPayablesm(otherPayablesm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除其他应付款:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 跳转到修改其他应付款的界面
	 * @param modelMap
	 * @param session
	 * @param pk_otherPayables
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdateOtherPayablesm")
	public ModelAndView toUpdateOtherPayablesm(ModelMap modelMap,HttpSession session,String pk_otherpayables) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		OtherPayablesm otherPayablesm = new OtherPayablesm();
		otherPayablesm.setPk_otherpayables(pk_otherpayables);
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("otherpayablesm", otherPayablesService.queryOtherpayablesmByPK(otherPayablesm));
		OtherPayablesd otherPayablesd = new OtherPayablesd();
		otherPayablesd.setPk_otherpayables(pk_otherpayables);
		otherPayablesd.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<OtherPayablesd> listOtherPayablesd = new ArrayList<OtherPayablesd>();
		listOtherPayablesd = otherPayablesService.queryAllOtherPayablesd(otherPayablesd);
		modelMap.put("otherpayablesdlist", listOtherPayablesd);
		Otherpay otherpay = new Otherpay();
		otherpay.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Otherpay> otherpaylist = otherpayService.findOtherpayList(otherpay);
		modelMap.put("otherpaylist", otherpaylist);
		return new ModelAndView(OtherPayablesConstants.UPDATEOTHERPAYABLESD,modelMap);
	}
	
	/**
	 * 修改其他应付款
	 * @param otherPayablesm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	@RequestMapping(value="/updateOtherPayablesm")
	@ResponseBody
	public String updateOtherPayablesm(OtherPayablesm otherPayablesm,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		otherPayablesm.setModifier(session.getAttribute("accountName").toString());
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String res = otherPayablesService.updateOtherPayablesm(otherPayablesm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改其他应付款:"+otherPayablesm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 其他付款单提交
	 * @param modelMap
	 * @param session
	 * @param chksto
	 * @param sta
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/commitPayablesm")
	public ModelAndView commitPayablesm(ModelMap modelMap,HttpSession session,String pks)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		OtherPayablesm otherPayablesm = new OtherPayablesm();
		List<String> pklist = Arrays.asList(pks.split(","));
		otherPayablesm.setPklist(pklist);
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		//查询提交的其他应付款主表详细信息
		OtherPayablesm otherPayablesms = otherPayablesService.queryOtherpayablesmByPK(otherPayablesm);
		otherPayablesService.commitPayablesm(otherPayablesm);
		
		
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"提交其他应付款:"+otherPayablesms.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 查询所有的其他应付款
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param otherPayablesm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/selectAllOtherPayablesm")
	public ModelAndView selectAllOtherPayablesm(ModelMap modelMap,HttpSession session,Page page,OtherPayablesm otherPayablesm,String callBack)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<OtherPayablesm> listOtherPayablesm = otherPayablesService.queryAllOtherPayablesm(otherPayablesm,page);
		String pk_otherPayables = "";
		if (listOtherPayablesm.size()>0) {
			pk_otherPayables = listOtherPayablesm.get(0).getPk_otherpayables();
		}
		modelMap.put("listotherpayablesm", listOtherPayablesm);
		modelMap.put("pk_otherpayables", pk_otherPayables);
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		return new ModelAndView(OtherPayablesConstants.SELECTOTHERPAYABLESM,modelMap);
	}
	
	/**
	 * 打印订单
	 * @param modelMap
	 * @param session
	 * @param pk_chksto
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/printOtherPayables")
	public ModelAndView printOtherPayables(ModelMap modelMap,HttpSession session,HttpServletRequest request,String type,OtherPayablesm otherPayablesm,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		otherPayablesm.setAcct(session.getAttribute("ChoiceAcct").toString());
		page.setPageSize(Integer.MAX_VALUE);
		OtherPayablesm maintable = otherPayablesService.queryAllOtherPayablesm(otherPayablesm,page).get(0);
		OtherPayablesd otherPayablesd = new OtherPayablesd();
		otherPayablesd.setPk_otherpayables(otherPayablesm.getPk_otherpayables());
		otherPayablesd.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<OtherPayablesd> otherPayablesdList = otherPayablesService.queryAllOtherPayablesd(otherPayablesd);
		Map<String,Object> parameter = MapBeanConvertUtil.convertBean(maintable);
		
		List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(otherPayablesdList);
		parameter.put("totalCount", ""+otherPayablesdList.size());
        parameter.put("pk_otherpayables",otherPayablesm.getPk_otherpayables());
		//设置分页，查询所有数据
		modelMap.put("actionMap", parameter);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
	    parameter.put("report_name", "其他应付款");
	    modelMap.put("report_name", "其他应付款");//导出excel、pdf用到了
        modelMap.put("parameters", parameter);//ireport表头参数
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",dataList);
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,"/report/assistant/bill/printotherpayablesm.jasper");//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 入库单弹框
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/selectInOrder")
	public ModelAndView selectInOrder(ModelMap modelMap,Chkinm chkinm,Page page,HttpSession session,
			String callBack,String delivercode,String domId,String single) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Deliver del = new Deliver();
		del.setCode(delivercode);
		chkinm.setDeliver(del);
		List<Chkinm>  chkinmList = otherPayablesService.queryAllInOrder(chkinm,page);
		int chkinno = 0;
		if (chkinmList.size() > 0) {
			chkinno = chkinmList.get(0).getChkinno();
		}
		modelMap.put("pk_inspect", chkinno);
		modelMap.put("inspectm", chkinm);
		modelMap.put("inspectmList", chkinmList);
		modelMap.put("domId", domId);
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("pageobj", page);
		return new ModelAndView(OtherPayablesConstants.SELECTINORDER,modelMap);
	}
	/**
	 * 查询前10条入库单
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findInOrderTop")
	@ResponseBody
	public List<Chkinm> findInOrderTop(ModelMap modelMap,Chkinm chkinm,HttpSession session,String delivercode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Deliver del = new Deliver();
		del.setCode(delivercode);
		chkinm.setDeliver(del);
		return otherPayablesService.findInOrderTop(chkinm);
	}
	
}
