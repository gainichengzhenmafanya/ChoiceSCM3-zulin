package com.choice.assistant.web.controller.purchaseSet;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.material.MaterialConstants;
import com.choice.assistant.domain.PurchaseSet.Restspaymentd;
import com.choice.assistant.domain.PurchaseSet.Restspaymentm;
import com.choice.assistant.service.purchaseSet.RestspaymentService;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

@Controller
@RequestMapping("restspayment")
public class RestspaymentController {
	
	@Autowired
	private RestspaymentService restspaymentService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 查询其他应付款主表
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllRestspaymentm")
	public ModelAndView queryAllRestspaymentm(ModelMap modelMap,Restspaymentm restspaymentm,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		restspaymentm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Restspaymentm>  restspaymentmList = restspaymentService.queryAllRestspaymentm(restspaymentm,page);
		modelMap.put("restspaymentm", restspaymentm);
		modelMap.put("restspaymentmList", restspaymentmList);
		modelMap.put("pageobj", page);
		return new ModelAndView(MaterialConstants.LIST_MATERIALTYPE,modelMap);
	}
	/**
	 * 查询其他应付款子表
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllRestspaymentd")
	public ModelAndView queryAllRestspaymentd(ModelMap modelMap,Restspaymentm restspaymentm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		restspaymentm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Restspaymentd>  restspaymentdList = restspaymentService.queryAllRestspaymentd(restspaymentm);
		modelMap.put("restspaymentm", restspaymentm);
		modelMap.put("restspaymentdList", restspaymentdList);
		return new ModelAndView(MaterialConstants.LIST_MATERIALTYPE,modelMap);
	}
	/**
	 * 保存其他应付款
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveRestspayment")
	@ResponseBody
	public String saveRestspayment(ModelMap modelMap,String restspaymentJson,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String res = restspaymentService.saveRestspayment(restspaymentJson,session.getAttribute("ChoiceAcct").toString(),session.getAttribute("accountId").toString());
		JSONObject json=JSONObject.fromObject(restspaymentJson);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增账号:"+json.get("vcode"),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	/**
	 * 删除其他应付款
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteRestspayment")
	@ResponseBody
	public String deleteRestspayment(ModelMap modelMap,String pk_restspayment,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return restspaymentService.deleteRestspayment(pk_restspayment);
	}
}
