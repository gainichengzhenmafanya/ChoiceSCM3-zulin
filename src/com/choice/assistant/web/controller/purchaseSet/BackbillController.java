package com.choice.assistant.web.controller.purchaseSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.backbill.BackbillConstants;
import com.choice.assistant.domain.PurchaseSet.Backbilld;
import com.choice.assistant.domain.PurchaseSet.Backbillm;
import com.choice.assistant.domain.PurchaseSet.Inspectd;
import com.choice.assistant.domain.PurchaseSet.Inspectm;
import com.choice.assistant.service.purchaseSet.BackbillService;
import com.choice.assistant.service.purchaseSet.InspectService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.ReadReportUrl;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

@Controller
@RequestMapping("backbill")
public class BackbillController {
	
	@Autowired
	private BackbillService backbillService;
	
	@Autowired
	private InspectService inspectService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 查询退货单主表
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllBackbillm")
	public ModelAndView queryAllBackbillm(ModelMap modelMap,Backbillm backbillm,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null==backbillm.getBdate() && null==backbillm.getEdate()){
			backbillm.setBdate(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDD));
			backbillm.setEdate(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDD));
		}
		List<Backbillm> backbillmList = backbillService.queryAllBackbillm(backbillm,page);
		String pk_backbill = "";
		if (backbillmList.size() > 0) {
			pk_backbill = backbillmList.get(0).getPk_backbill();
			modelMap.put("pk_backbill", pk_backbill);
		}
		modelMap.put("backbillm", backbillm);
		modelMap.put("backbillmList", backbillmList);
		modelMap.put("pageobj", page);
		return new ModelAndView(BackbillConstants.TABLEBACKBILLM,modelMap);
	}
	
	/**
	 * 根据验货单查询退货单信息
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryBackbillmInspect")
	@ResponseBody
	public String queryBackbillmInspect(HttpSession session,Backbillm backbillm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<String>  list = backbillService.queryBackbillmInspect(backbillm);
		StringBuffer sb = new StringBuffer();
		if (list.size() > 0) {
			for(String vbillno:list){
				sb.append(vbillno).append(",");
			}
		}
		return sb.length()>0?sb.substring(0, sb.lastIndexOf(",")):"";
	}
	
	/**
	 * 查询退货单子表
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllBackbilld")
	public ModelAndView queryAllBackbilld(ModelMap modelMap,Backbillm backbillm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Backbilld>  backbilldList = backbillService.queryAllBackbilld(backbillm);
		modelMap.put("backbillm", backbillm);
		if (backbillm.getPk_backbill() != null && !"".equals(backbillm.getPk_backbill())) {
			modelMap.put("backbilldList", backbilldList);
		} else {
			modelMap.put("backbilldList", null);
		}
		double summoney = 0.0;
		if (backbilldList != null) {
			for (Backbilld backbilld : backbilldList) {
				summoney += backbilld.getNmoney();
			}
			modelMap.put("length", backbilldList.size());
		} else {
			modelMap.put("length", 0);
		}
		modelMap.put("summoney", summoney);
		return new ModelAndView(BackbillConstants.TABLEBACKBILLD,modelMap);
	}
	
	/**
	 * 跳转到自制新增退货单的界面
	 * @param modelMap
	 * @param backbillm
	 * @param session
	 * @param sta
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddBackBillm")
	public ModelAndView toAddBackBillm(ModelMap modelMap,Backbillm backbillm,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		backbillm.setVbillno(backbillService.selectMaxVbillno(backbillm,session.getAttribute("ChoiceAcct").toString()));
//		backbillm.setVbillno(coderuleService.getCode(CoderuleConstants.BACKBILL));
		backbillm.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		backbillm.setCreator(session.getAttribute("accountName").toString());
		backbillm.setCreationtime(DateFormat.getTs());
		modelMap.put("backbillm", backbillm);
		modelMap.put("sta", sta);
		return new ModelAndView(BackbillConstants.TOSAVEBACKBILL,modelMap);
	}
	
	/**
	 * 跳转到新增退货单的界面====来自验货单(已验过的验货单)
	 * @param modelMap
	 * @param backbillm
	 * @param session
	 * @param sta
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddBackBillByYH")
	public ModelAndView toAddBackBillByYH(ModelMap modelMap,Backbillm backbillm,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		backbillm.setVbillno(backbillService.getNextTHVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
//		backbillm.setVbillno(coderuleService.getCode(CoderuleConstants.BACKBILL));
		backbillm.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		backbillm.setCreator(session.getAttribute("accountName").toString());
		backbillm.setCreationtime(DateFormat.getTs());
		modelMap.put("backbillm", backbillm);
		if (backbillm.getPk_inspectlist() != null && backbillm.getPk_inspectlist() != "") {
			Inspectm inspectm = new Inspectm();
			inspectm.setPklist(Arrays.asList(backbillm.getPk_inspectlist().split(",")));
			inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Inspectd> listInspectd = inspectService.selectInspectdByPK(inspectm);
//			Chkinm chkinm = new Chkinm();
//			chkinm.setPklist(Arrays.asList(backbillm.getPk_inspectlist().split(",")));
//			chkinm.setAcct(session.getAttribute("ChoiceAcct").toString());
//			List<Chkind> listInOrder = inspectService.selectInOrderByPK(chkinm);
			modelMap.put("listInspectd", listInspectd);
		}
		modelMap.put("sta", sta);
		return new ModelAndView(BackbillConstants.TOSAVEBACKBILLBYYH,modelMap);
	}
	
	/**
	 * 保存退货单
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveBackbill")
	@ResponseBody
	public String saveBackbill(ModelMap modelMap,Backbillm backbillm,HttpSession session,String sta) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		for (int i = 0; i < backbillm.getBackbilldList().size(); i++) {
			backbillm.getBackbilldList().get(i).setAcct(session.getAttribute("ChoiceAcct").toString());
		}
		Inspectm inm = new Inspectm();
		inm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inm.setPk_inspect(backbillm.getPk_inspectlist());
		inm = inspectService.toUpdateInspectm(inm);
		//positncode
		backbillm.setPositncode(inm.getPositncode());
		String res = backbillService.saveBackbill(backbillm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增退货单:"+backbillm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 校验单据号是否重复
	 * @param backbillm
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findBackbillByCode")
	@ResponseBody
	public boolean findBackbillByCode(String vbillno,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Backbillm backbillm = new Backbillm();
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		backbillm.setVbillno(vbillno);
		return backbillService.findBackbillByCode(backbillm);
	}
	
	/**
	 * 跳转到修改退货单页面、确认退货单页面
	 * @param modelMap
	 * @param backbillm
	 * @param session
	 * @param sta
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toUpdateBackbill")
	public ModelAndView toUpdateBackbill(ModelMap modelMap,Backbillm backbillm,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Backbillm backbill = backbillService.toUpdateBackbill(backbillm);
		if (sta == "enter" || "enter".equals(sta)) {
			backbill.setModifier(session.getAttribute("accountName").toString());
			backbill.setModifedtime(DateFormat.getTs());
			backbill.setVbacker(session.getAttribute("accountName").toString());
			backbill.setVbacktime(DateFormat.getTs());
		} else {
			backbill.setModifier(session.getAttribute("accountName").toString());
			backbill.setModifedtime(DateFormat.getTs());
		}
		modelMap.put("backbillm", backbill);
		modelMap.put("backbilldList", backbill.getBackbilldList());
		modelMap.put("sta", sta);
		//判断状态是修改还是确认退货
		if (sta == "enter" || "enter".equals(sta)) {
			return new ModelAndView(BackbillConstants.TOENTERACKBILL,modelMap);
		}
		return new ModelAndView(BackbillConstants.TOUPDATEACKBILL,modelMap);
	}
	
	/**
	 * 确认退货单
	 * @param session
	 * @param backbillm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/updateBackbill")
	@ResponseBody
	public String updateBackbill(HttpSession session,Backbillm backbillm,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logs = new Logs();
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String res = "";
		if (sta == "enter" || "enter".equals(sta)) {
			//backbillm.setIstate(1);
			backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
			backbillm = backbillService.toUpdateBackbill(backbillm);
			//修改验货货量与金额
			Inspectd inspectd = new Inspectd();
			inspectd.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Backbilld d : backbillm.getBackbilldList()){
				inspectd.setPk_inspect(d.getPk_inspect());
				inspectd.setSp_code(d.getSp_code());
				inspectd.setNinsnum(d.getNinsnum()+d.getNbacknum());
				inspectd.setNmoney(inspectd.getNinsnum()*d.getNprice());
				inspectService.updateInspectdnum(inspectd);
			}
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"确认退货单:"+backbillm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
			backbillService.updateBackbill(backbillm);
			try{
				//生成入库单并审核
				res = backbillService.saveInorderAdCk(backbillm, session.getAttribute("accountName").toString());
			}catch(Exception e){
				return e.getMessage();
			}
			//加入日志
			Logs logss=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"确认入库单:"+backbillm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
			logsMapper.addLogs(logss);
		}else {
			backbillm.setIstate(2);
			logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改退货单:"+backbillm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
			res = backbillService.updateBackbill(backbillm);
		}
		//加入日志
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 删除退货单
	 * @param session
	 * @param backbillm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/deleteBackBillm")
	public ModelAndView deleteBackBillm(ModelMap modelMap,HttpSession session,Backbillm backbillm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String strs = "",names="";
		for(String str : backbillm.getPks()){
			strs += ","+str;
		}
		backbillm.setPk_backbill(AsstUtils.StringCodeReplace(strs.substring(1)));
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<Backbillm> listBackbillm = backbillService.queryAllBackbillm(backbillm,page);
		for(Backbillm backbillmd : listBackbillm ){
			names += "," +backbillmd.getVbillno();
		}
		backbillService.deleteBackBillm(backbillm);//删除退货单
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除退货单:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 退证该退货单是否可以修改、删除；可以编辑返回--T不能编辑返回--F
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/validateBackbillm")
	@ResponseBody
	public String validateBackbillm(ModelMap modelMap,Backbillm backbillm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		List<Backbillm>  backbillmList  = new ArrayList<Backbillm>();
		if(ValueCheck.IsNotEmpty(backbillm.getPk_backbill())){
			backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
			backbillm.setIstate(0);//确认状态为未确认状态的标志
			backbillmList = backbillService.queryAllBackbillm(backbillm,new Page());
		}
		if(backbillmList.size()>0){
			return "退证该退货单可以修改";
		}else{
			return "退证该退货单不可以修改";
		}
	}
	/**
	 * 删除退货单
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteBackbill")
	@ResponseBody
	public String deleteBackbill(ModelMap modelMap,String pk_backbill,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String names="";
		Backbillm backbillm = new Backbillm();
		backbillm.setPk_backbill(AsstUtils.StringCodeReplace(pk_backbill));
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<Backbillm> listBackbillm = backbillService.queryAllBackbillm(backbillm,page);
		for(Backbillm backbillmd : listBackbillm ){
			names += "," +backbillmd.getVbillno();
		}
		String res = backbillService.deleteBackbill(pk_backbill);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除退货单:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 打印订单
	 * @param modelMap
	 * @param session
	 * @param pk_chksto
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/printBackbill")
	public ModelAndView printBackbill(ModelMap modelMap,HttpSession session,HttpServletRequest request,String type,Backbillm backbillm,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		backbillm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String pk_backbill = backbillm.getPk_backbill();
		page.setPageSize(Integer.MAX_VALUE);
		Backbillm backbill = backbillService.queryAllBackbillm(backbillm, page).get(0);
		backbillm.setPk_backbill(pk_backbill);
		List<Backbilld>  backbilldList = backbillService.queryAllBackbilld(backbillm);
		
		List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(backbilldList);
		Map<String,Object> mapRes = MapBeanConvertUtil.convertBean(backbill);
		//设置分页，查询所有数据
		modelMap.put("actionMap", mapRes);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
	 	Map<String,Object>  parameters = mapRes;
	    parameters.put("report_name", "退货单");
	    modelMap.put("report_name", "退货单");//导出excel、pdf用到了
        modelMap.put("parameters", parameters);//ireport表头参数
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",dataList);
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,"/report/assistant/bill/PrintBackbillm.jasper");//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 根据单号查询退货单的状态
	 * @param backbillm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryBackBillmIstate")
	@ResponseBody
	public int queryBackBillmIstate(ModelMap modelMap,Backbillm backbillm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		backbillm.setAcct(acct);
		int istate = 2;
		//查询
		try{
			backbillm = backbillService.queryAllBackbillm(backbillm,new Page()).get(0);
			istate = backbillm.getIstate();
		}catch(Exception e){
			e.printStackTrace();
		}
		return istate;
	}
}
