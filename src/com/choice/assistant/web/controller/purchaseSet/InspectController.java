package com.choice.assistant.web.controller.purchaseSet;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.purchaseSet.PurchaseSetConstants;
import com.choice.assistant.domain.PurchaseSet.Inspectd;
import com.choice.assistant.domain.PurchaseSet.Inspectm;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.assistant.domain.system.Inspection;
import com.choice.assistant.service.bill.PuprOrderService;
import com.choice.assistant.service.purchaseSet.InspectService;
import com.choice.assistant.service.system.InspectionService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.ReadReportUrl;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.service.CodeDesService;

@Controller
@RequestMapping("inspect")
public class InspectController {
	
	@Autowired
	private InspectService inspectService;
	@Autowired
    private InspectionService inspectionService;
	@Autowired
	private PuprOrderService puprOrderService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private CodeDesService codeDesService;
	/**
	 * 查询验货单主表
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllInspectm")
	public ModelAndView queryAllInspectm(ModelMap modelMap,Inspectm inspectm,Page page,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null==inspectm.getBdate() && null==inspectm.getEdate()){
			inspectm.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			inspectm.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}
		List<Inspectm>  inspectmList = inspectService.queryAllInspectm(inspectm,page);
		String pk_inspect = "";
		if (inspectmList.size() > 0) {
			pk_inspect = inspectmList.get(0).getPk_inspect();
		}
		modelMap.put("pk_inspect", pk_inspect);
		modelMap.put("inspectm", inspectm);
		modelMap.put("inspectmList", inspectmList);
		modelMap.put("pageobj", page);
		return new ModelAndView(PurchaseSetConstants.TABLEINSPECTM,modelMap);
	}
	
	/**
	 * 根据订单查询验货单信息
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryInspectmPuprorder")
	@ResponseBody
	public String queryInspectmPuprorder(HttpSession session,Inspectm inspectm) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<String>  list = inspectService.queryInspectmPuprorder(inspectm);
		StringBuffer sb = new StringBuffer();
		if (list.size() > 0) {
			for(String vbillno:list){
				sb.append(vbillno).append(",");
			}
		}
		return sb.length()>0?sb.substring(0, sb.lastIndexOf(",")):"";
	}
	
	/**
	 * 查询验货单子表
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllInspectd")
	public ModelAndView queryAllInspectd(ModelMap modelMap,Inspectm inspectm,HttpSession session,String pk_inspect) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setPk_inspect(pk_inspect);
		inspectm.setSort("1");
		List<Inspectd> inspectdList = inspectService.queryAllInspectd(inspectm);
//		Inspection inspection = new Inspection();
//		inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
//		List<Inspection> inspectionList = inspectionService.findInspectionList(inspection);
		double summoney = 0.0;
		if (inspectdList != null) {
			for (Inspectd inspectd : inspectdList) {
				summoney += inspectd.getNmoney();
			}
			modelMap.put("length", inspectdList.size());
		}else {
			modelMap.put("length", 0);
		}
		modelMap.put("summoney", summoney);
		modelMap.put("inspectm", inspectm);
		if (pk_inspect != null && !"".equals(pk_inspect)) {
			modelMap.put("inspectdList", inspectdList);
		} else {
			modelMap.put("inspectdList", null);
		}
//		modelMap.put("inspectionList", inspectionList);
		return new ModelAndView(PurchaseSetConstants.TABLEINSPECTD,modelMap);
	}
	
	/**
	 * 跳转到新增验货单的页面
	 * @param modelMap
	 * @param inspectm
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddInspectm")
	public ModelAndView toAddInspectm(ModelMap modelMap,Inspectm inspectm,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setVbillno(inspectService.selectMaxVbillno(inspectm,session.getAttribute("ChoiceAcct").toString()));
//		inspectm.setVbillno(coderuleService.getCode(CoderuleConstants.INSPECT));
		inspectm.setCreator(session.getAttribute("accountName").toString());
		inspectm.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		inspectm.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("inspectm", inspectm);
		modelMap.put("sta", sta);
		return new ModelAndView(PurchaseSetConstants.TOADDINSPECTD,modelMap);
	}
	
	/**
	 * 跳转到新增验货单的页面====来自采购订单
	 * @param modelMap
	 * @param inspectm
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddInspectmOrder")
	public ModelAndView toAddInspectmOrder(ModelMap modelMap,Inspectm inspectm,HttpSession session,String sta,String callBack,int istate)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setVbillno(inspectService.getNextVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
		inspectm.setCreator(session.getAttribute("accountName").toString());
		inspectm.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		inspectm.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("inspectm", inspectm);
		modelMap.put("sta", sta);
		modelMap.put("callBack", callBack);
		Inspection inspection = new Inspection();
		inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Inspection> inspectionlist = inspectionService.findInspectionList(inspection);
		inspection.setVname("");
		inspection.setPk_inspection("");
		inspectionlist.add(0, inspection);
		modelMap.put("inspectionlist",inspectionlist);
		if(inspectm.getPk_puprorderList() != null && inspectm.getPk_puprorderList() != ""){
			PuprOrderd puprOrderd = new PuprOrderd();
			List<String> pklist = Arrays.asList(inspectm.getPk_puprorderList().split(","));
			puprOrderd.setPklist(pklist);
			puprOrderd.setIstate(istate);
			puprOrderd.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<PuprOrderd> listPuprOrderd = puprOrderService.queryAllPuprOrderdsByPk(puprOrderd);
			modelMap.put("listPuprOrderd", listPuprOrderd);
		}
		return new ModelAndView(PurchaseSetConstants.TOADDINSPECTDORDER,modelMap);
	}
	/**
	 * 跳转到新增验货单的页面====来自发货单
	 * @param modelMap
	 * @param inspectm
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddInspectmOrderFromInvoice")
	public ModelAndView toAddInspectmOrderFromInvoice(ModelMap modelMap,Inspectm inspectm,HttpSession session,String sta,String callBack,int istate)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setVbillno(inspectService.selectMaxVbillno(inspectm,session.getAttribute("ChoiceAcct").toString()));
		inspectm.setCreator(session.getAttribute("accountName").toString());
		inspectm.setCreationtime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		inspectm.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("inspectm", inspectm);
		modelMap.put("sta", sta);
		modelMap.put("callBack", callBack);
		Inspection inspection = new Inspection();
		inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Inspection> inspectionlist = inspectionService.findInspectionList(inspection);
		inspection.setVname("");
		inspection.setPk_inspection("");
		inspectionlist.add(0, inspection);
		modelMap.put("inspectionlist",inspectionlist);
		if(inspectm.getPk_puprorderList() != null && inspectm.getPk_puprorderList() != ""){
			PuprOrderd puprOrderd = new PuprOrderd();
			List<String> pklist = Arrays.asList(inspectm.getPk_puprorderList().split(","));
			puprOrderd.setPklist(pklist);
			puprOrderd.setIstate(istate);
			puprOrderd.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<PuprOrderd> listPuprOrderd = puprOrderService.queryAllPuprOrderdsByPk(puprOrderd);
			modelMap.put("listPuprOrderd", listPuprOrderd);
		}
		return new ModelAndView(PurchaseSetConstants.TOADDINSPECTDORDERFROMINVOICE,modelMap);
	}
	
	/**
	 * 校验单据号是否重复
	 * @param inspectm
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findInspectByCode")
	@ResponseBody
	public boolean findInspectByCode(String vbillno,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Inspectm inspectm = new Inspectm();
		inspectm.setVbillno(vbillno);
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		return inspectService.findInspectByCode(inspectm);
	}
	
	/**
	 * 保存验货单
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveInspect")
	@ResponseBody
	public String saveInspect(ModelMap modelMap,Inspectm inspectm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		//根据订单号查询仓位（采购组织）信息
		String positncode = inspectService.queryOrderPos(inspectm.getAcct(),inspectm.getVpuproedername());
		inspectm.setPositncode(positncode);
		String res = inspectService.saveInspect(inspectm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增验货单:"+inspectm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 删除验货单
	 * @param modelMap
	 * @param session
	 * @param pks
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteInspectm")
	public ModelAndView deleteInspectm(ModelMap modelMap,HttpSession session,String pks)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String names = "";
		Inspectm inspectm = new Inspectm();
		List<String> ids = Arrays.asList(pks.split(","));
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		inspectm.setPk_inspect(AsstUtils.StringCodeReplace(pks));
		List<Inspectm> listInspectms = inspectService.queryAllInspectm(inspectm, page);
		for(Inspectm inspectmd : listInspectms){
			names = ","+inspectmd.getVbillno();
		}
		inspectm.setPklist(ids);
		inspectService.deleteInspectm(inspectm);
		
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除验货单:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 跳转到修改验货单界面
	 * @param modelMap
	 * @param session
	 * @param inspectm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdateInspectm")
	public ModelAndView toUpdateInspectm(ModelMap modelMap,HttpSession session,Inspectm inspectm,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Inspectm inspect = inspectService.toUpdateInspectm(inspectm);
		inspect.setModifier(session.getAttribute("accountName").toString());
		inspect.setModifedtime(DateFormat.getTs());
		
		//Inspection inspection = new Inspection();
		//inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
		//List<Inspection> inspectionlist = inspectionService.findInspectionList(inspection);
		//inspection.setVname("");
		//inspection.setPk_inspection("");
		//inspectionlist.add(0, inspection);
		//modelMap.put("inspectionlist",inspectionlist);
		
		//不合格原因
		CodeDes cds = new CodeDes();
		cds.setTyp("21");
		List<CodeDes> reasons = codeDesService.findCodeDes(cds,new Page());
		CodeDes cds1 = new CodeDes();
		cds1.setCode("");
		cds1.setDes("");
		reasons.add(0,cds1);
		modelMap.put("reasons", reasons);
		
		modelMap.put("inspectm", inspect);
		modelMap.put("listInspectd", inspectService.queryAllInspectd(inspectm));
		modelMap.put("sta", sta);
		return new ModelAndView(PurchaseSetConstants.UPDATEINSPECTDORDER,modelMap);
	}
	
	/**
	 * 修改验货单
	 * @param session
	 * @param inspectm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateInspectm")
	@ResponseBody
	public String updateInspectm(HttpSession session,Inspectm inspectm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setIstate(2);
		inspectm.setVinspect(inspectm.getModifier());
		inspectm.setVinspectdate(inspectm.getModifedtime());
		String res = inspectService.updateInspectm(inspectm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改验货单:"+inspectm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 跳转到确认验货页面
	 * @param modelMap
	 * @param session
	 * @param inspectm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toEnterInspectm")
	public ModelAndView toEnterInspectm(ModelMap modelMap,HttpSession session,Inspectm inspectm,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Inspectm inspect =  inspectService.toUpdateInspectm(inspectm);
		inspect.setVinspect(session.getAttribute("accountName").toString());
		inspect.setVinspectdate(DateFormat.getTs());
		inspect.setModifier(session.getAttribute("accountName").toString());
		inspect.setModifedtime(DateFormat.getTs());
		
		Inspection inspection = new Inspection();
		inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Inspection> inspectionlist = inspectionService.findInspectionList(inspection);
		inspection.setVname("");
		inspection.setPk_inspection("");
		inspectionlist.add(0, inspection);
		modelMap.put("inspectionlist",inspectionlist);
		
		modelMap.put("inspectm", inspect);
		modelMap.put("listInspectd", inspectService.queryAllInspectd(inspectm));
		modelMap.put("sta", sta);
		return new ModelAndView(PurchaseSetConstants.ENTERINSPECTDORDER,modelMap);
	}
	/**
	 * 跳转到确认入库页面
	 * @param modelMap
	 * @param session
	 * @param inspectm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toEnterInOrdr")
	public ModelAndView toEnterInOrdr(ModelMap modelMap,HttpSession session,Inspectm inspectm,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		Inspectm inspect =  inspectService.toUpdateInspectm(inspectm);
		inspect.setVinspect(session.getAttribute("accountName").toString());
		inspect.setVinspectdate(DateFormat.getTs());
		inspect.setModifier(session.getAttribute("accountName").toString());
		inspect.setModifedtime(DateFormat.getTs());
//		Inspection inspection = new Inspection();
//		inspection.setAcct(session.getAttribute("ChoiceAcct").toString());
//		List<Inspection> inspectionlist = inspectionService.findInspectionList(inspection);
//		inspection.setVname("");
//		inspection.setPk_inspection("");
//		inspectionlist.add(0, inspection);
//		modelMap.put("inspectionlist",inspectionlist);
		modelMap.put("inspectm", inspect);
//		modelMap.put("listInspectd", inspectService.queryAllInspectd(inspectm));
		modelMap.put("sta", sta);
		return new ModelAndView(PurchaseSetConstants.ENTERINORDR,modelMap);
	}
	
	/**
	 * 确认验货
	 * @param session
	 * @param inspectm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/enterInspectm")
	@ResponseBody
	public String enterInspectm(HttpSession session,Inspectm inspectm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setIstate(1);
		Inspectm inspectms = inspectService.toUpdateInspectm(inspectm);
		String res = inspectService.updateInspectm(inspectm);
		
		Set<String> set = new HashSet<String>();
		for(Inspectd d :inspectm.getInspectdList()){
			set.add(d.getPk_puprorder());
		}
		for(String pk_puprorder:set){
			PuprOrder puprOrder = new PuprOrder();
			puprOrder.setIstate(5);
			puprOrder.setPk_puprorder(pk_puprorder);
			puprOrder.setTs(DateFormat.getTs());
			puprOrderService.updatePuprOrdersState(puprOrder);
		}
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"确认验货单:"+inspectms.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	/**
	 * 确认入库
	 * @param session
	 * @param inspectm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/enterInOrdr")
	@ResponseBody
	public String enterInOrdr(HttpSession session,Inspectm inspectm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
//		inspectm.setIstate(4);
//		Inspectm inspectms = inspectService.toUpdateInspectm(inspectm);
//		inspectService.updateInspectm(inspectm);
//		Set<String> set = new HashSet<String>();
//		for(Inspectd d :inspectm.getInspectdList()){
//			set.add(d.getPk_puprorder());
//		}
//		for(String pk_puprorder:set){
//			PuprOrder puprOrder = new PuprOrder();
//			puprOrder.setIstate(6);
//			puprOrder.setPk_puprorder(pk_puprorder);
//			puprOrder.setTs(DateFormat.getTs());
//			puprOrderService.updatePuprOrdersState(puprOrder);
//		}
		//生成入库单并审核
		String result = "";
		try{
			//加入日志
			Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"生成并审核入库单:"+inspectm.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
			logsMapper.addLogs(logs);
			result = inspectService.saveInorderAndCheck(inspectm, session.getAttribute("accountName").toString());
		}catch(Exception e){
			return e.getMessage();
		}
		return result;
	}
	
	/**
	 * 查询验货单主表====弹出单子选择框（未确认到货）
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/selectAllInspectm")
	public ModelAndView selectAllInspectm(ModelMap modelMap,Inspectm inspectm,Page page,HttpSession session,
			String callBack,String delivercode,String domId,String single) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setDelivercode(delivercode);
		if(null==inspectm.getBdate() && null==inspectm.getEdate()){
			inspectm.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			inspectm.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		}		
		List<Inspectm>  inspectmList = inspectService.queryAllInspectmDo(inspectm,page);
		String pk_inspect = "";
		if (inspectmList.size() > 0) {
			pk_inspect = inspectmList.get(0).getPk_inspect();
		}
		modelMap.put("pk_inspect", pk_inspect);
		modelMap.put("inspectm", inspectm);
		modelMap.put("inspectmList", inspectmList);
		modelMap.put("domId", domId);
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("pageobj", page);
		return new ModelAndView(PurchaseSetConstants.SELECTINSPECTM,modelMap);
	}
	
	/**
	 * 查询前10条验货单
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findInspectmTop")
	@ResponseBody
	public List<Inspectm> findInspectmTop(ModelMap modelMap,Inspectm inspectm,HttpSession session,String delivercode) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectm.setDelivercode(delivercode);
		return inspectService.findInspectmTop(inspectm);
	}
	
	/**
	 * 打印验货单
	 * @param modelMap
	 * @param session
	 * @param pk_chksto
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/printInspectbill")
	public ModelAndView printInspectbill(ModelMap modelMap,HttpSession session,HttpServletRequest request,String type,Inspectm inspectm,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		inspectm.setAcct(session.getAttribute("ChoiceAcct").toString());
		String pk_inspect = inspectm.getPk_inspect();
		page.setPageSize(Integer.MAX_VALUE);
		Inspectm  inspect = inspectService.queryAllInspectm(inspectm,page).get(0);
		inspectm.setPk_inspect(pk_inspect);
		List<Inspectd>  inspectdList = inspectService.queryAllInspectd(inspectm);
		
		List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(inspectdList);
		Map<String,Object> mapRes = MapBeanConvertUtil.convertBean(inspect);
		
		int istate = Integer.parseInt(String.valueOf(mapRes.get("istate")));
		switch(istate){
			case 2 :mapRes.put("vstate", "未入库");break;
			case 4 :mapRes.put("vstate", "已入库");break;
			case 3 :mapRes.put("vstate", "已结算");break;
		}
        BigDecimal summoney = BigDecimal.valueOf(0);
        if (inspectdList != null) {
            for (Inspectd inspectd : inspectdList) {
                summoney=summoney.add(BigDecimal.valueOf(inspectd.getNmoney()));
            }
        }
		//设置分页，查询所有数据
		modelMap.put("actionMap", mapRes);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
	 	Map<String,Object>  parameters = mapRes;
	    parameters.put("report_name", "验货单");
	    modelMap.put("report_name", "验货单");//导出excel、pdf用到了
        parameters.put("summoney", summoney.doubleValue());//合计金额
	    parameters.put("ts", inspect.getTs());
        modelMap.put("parameters", parameters);//ireport表头参数
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",dataList);
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,"/report/assistant/bill/PrintInspectm.jasper");//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	/**
	 * 根据订单号查询单价信息
	 * @param Materialtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryOrderPrice")
	@ResponseBody
	public String queryOrderPrice(ModelMap modelMap,String vpuproedername,String sp_code,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		//根据订单号查询仓位（采购组织）信息
		String price = inspectService.queryOrderPrice(acct,vpuproedername,sp_code);
		return price;
	}
	
	/**
	 * 根据单号查询验货的状态
	 * @param inspectm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryInspectmIstate")
	@ResponseBody
	public int queryInspectmIstate(ModelMap modelMap,Inspectm inspectm,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		inspectm.setAcct(acct);
		//查询
		inspectm = inspectService.toUpdateInspectm(inspectm);
		return inspectm.getIstate();
	}
}
