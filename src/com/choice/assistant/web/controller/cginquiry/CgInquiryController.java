package com.choice.assistant.web.controller.cginquiry;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.cginquiry.CgInquiryConstants;
import com.choice.assistant.domain.cginquiry.CgInquiry;
import com.choice.assistant.domain.system.Inquiry;
import com.choice.assistant.service.cginquiry.CgInquiryService;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.ChartsXml;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 采购询价
 * @author css
 *
 */
@Controller
@RequestMapping("cginquiry")
public class CgInquiryController {

	@Autowired
	private CgInquiryService cgInquiryService;
	@Autowired
	private LogsMapper logsMapper;
    @Autowired
    private ContracttermsService contracttermsService;
	
	/**
	 * 查询所有的询价
	 * @param modelMap
	 * @param puprOrder
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/tableCgInquiry")
	public ModelAndView tableCgInquiry(ModelMap modelMap,CgInquiry cgInquiry,HttpSession session,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgInquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listCgInquiry", cgInquiryService.tableCgInquiry(cgInquiry, page));
		modelMap.put("cgInquiry", cgInquiry);
		modelMap.put("pageobj", page);
		return new ModelAndView(CgInquiryConstants.TABLE_CGINQUIRY,modelMap);
	}
	
	/**
	 * 跳转到新增页面
	 * @param modelMap
	 * @param puprOrder
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toSaveCgInquiry")
	public ModelAndView toSaveCgInquiry(ModelMap modelMap,CgInquiry cgInquiry,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgInquiry.setDinquirybdat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		cgInquiry.setDinquiryedat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("cgInquiry", cgInquiry);
		Inquiry inquiry = new Inquiry();
		inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("inquiryList",cgInquiryService.selectAllInquiryType(inquiry));
		return new ModelAndView(CgInquiryConstants.SAVE_CGINQUIRY,modelMap);
	}
	
	/**
	 * 添加采购询价
	 * @param modelMap
	 * @param cgInquiry
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveCgInquiry")
	public ModelAndView saveCgInquiry(ModelMap modelMap,CgInquiry cgInquiry,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgInquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		cgInquiryService.saveCgInquiry(cgInquiry);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增采购询价，物资名称:"+cgInquiry.getSp_name()+"，供应商名称："+cgInquiry.getDelivername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 跳转到修改页面
	 * @param modelMap
	 * @param pk_cginquiry
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdateCginquiry")
	public ModelAndView toUpdateCginquiry(ModelMap modelMap,String pk_cginquiry,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CgInquiry cgInquiry = new CgInquiry();
		cgInquiry.setPk_cginquiry(pk_cginquiry);
		cgInquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("cginquiry", cgInquiryService.toUpdateCginquiry(cgInquiry));
		Inquiry inquiry = new Inquiry();
		inquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("inquiryList",cgInquiryService.selectAllInquiryType(inquiry));
		modelMap.put("pk_cginquiry", pk_cginquiry);
		return new ModelAndView(CgInquiryConstants.UPDATE_CGINQUIRY,modelMap);
	}
	
	/**
	 * 修改采购询价
	 * @param modelMap
	 * @param cgInquiry
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateCgInquiry")
	public ModelAndView updateCgInquiry(ModelMap modelMap,CgInquiry cgInquiry,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgInquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		cgInquiryService.updateCgInquiry(cgInquiry);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改采购询价，物资名称:"+cgInquiry.getSp_name()+"，供应商名称："+cgInquiry.getDelivername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 删除询价
	 * @param session
	 * @param vcodes
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteCginquiry")
	@ResponseBody
	public String deleteCginquiry(HttpSession session,String vcodes)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CgInquiry cgInquiry = new CgInquiry();
		cgInquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
		cgInquiry.setListpk_cginquiry(Arrays.asList(vcodes.split(",")));
    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","sp_name,delivername");
    	paramMap.put("tableName","cscm_cginquiry");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_cginquiry");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(vcodes));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += "；物资名称："+map.get("sp_name")+"，供应商名称："+map.get("delivername");
    	}
		String res = cgInquiryService.deleteCginquiry(cgInquiry);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除采购询价:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}

    @RequestMapping("/priceAnalysis")
    public ModelAndView priceAnalysis(ModelMap modelMap,String pk_cginquiry){
        modelMap.put("pk_cginquiry",pk_cginquiry);
        return new ModelAndView(CgInquiryConstants.PRICE_ANALYSIS_CHART,modelMap);
    }
    /**
     * 价格分析
     * @param response
     * @param session
     * @param pk_cginquiry
     * @return
     */
    @RequestMapping("/findPriceAnalysisData")
    @ResponseBody
    public Object findPriceAnalysisData(HttpServletResponse response,HttpSession session,String pk_cginquiry){
        try {
            CgInquiry cgInquiry=new CgInquiry();
            cgInquiry.setAcct(session.getAttribute("ChoiceAcct").toString());
            cgInquiry.setPk_cginquiry(pk_cginquiry);
            ChartsXml xml=cgInquiryService.findPriceAnalysisData(response,cgInquiry);
            if(xml!=null) {
                return xml.output();
            }
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return null;
    }
	
}
