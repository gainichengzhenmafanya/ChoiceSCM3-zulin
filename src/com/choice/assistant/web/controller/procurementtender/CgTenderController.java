package com.choice.assistant.web.controller.procurementtender;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.procurementtender.CgTenderConstants;
import com.choice.assistant.domain.procurementtender.CgTender;
import com.choice.assistant.domain.procurementtender.CgTenderd;
import com.choice.assistant.domain.system.PayMethod;
import com.choice.assistant.domain.system.Tender;
import com.choice.assistant.service.procurementtender.CgTenderService;
import com.choice.assistant.util.DateFormat;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.service.CodeDesService;

@Controller
@RequestMapping("cgtender")
public class CgTenderController {
	
	@Autowired
	private CgTenderService cgTenderService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	CodeDesService codeDesService;
	
	/**
	 * 查询招标书列表
	 * @param modelMap
	 * @param cgtender
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllCgTender")
	public ModelAndView queryAllCgTender(ModelMap modelMap,CgTender cgtender,HttpSession session,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgtender.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listCgTender", cgTenderService.queryAllCgTender(cgtender,page));
		modelMap.put("cgtender", cgtender);
		modelMap.put("pageobj", page);
		return new ModelAndView(CgTenderConstants.TABLE_CGTENDER,modelMap);
	}
	
	/**
	 * 跳转到添加页面
	 * @param modelMap
	 * @param cgtender
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toSaveCgTender")
	public ModelAndView toSaveCgTender(ModelMap modelMap,CgTender cgtender,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgtender.setDregistrationedat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		cgtender.setDbidedat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("cgtender", cgtender);
		//查询所有的招标类别
		Tender tender = new Tender();
		tender.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listTender", cgTenderService.queryAllTender(tender));
		//查询所有的付款方式
		PayMethod paymethod = new PayMethod();
		paymethod.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listPayMethod", cgTenderService.queryAllPayType(paymethod));
		return new ModelAndView(CgTenderConstants.SAVE_CGTENDER,modelMap);
	}
	
	/**
	 * 新增招标
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveCgTender")
	@ResponseBody
	public String saveCgTender(HttpSession session,CgTender cgtender)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增采购招标:"+cgtender.getVcgtendername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		String res = cgTenderService.saveCgTender(session,cgtender);
		//加入日志
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 跳转到修改页面
	 * @param modelMap
	 * @param cgtender
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdateCgTender")
	public ModelAndView toUpdateCgTender(ModelMap modelMap,String pk_cgtender,HttpSession session, String status)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CgTender cgten = new CgTender();
		cgten.setPk_cgtender(pk_cgtender);
		cgten.setAcct(session.getAttribute("ChoiceAcct").toString());
		CgTender cgtender = cgTenderService.queryCgTenderByID(cgten);
		modelMap.put("cgtender", cgtender);
		//查询所有的招标类别
		Tender tender = new Tender();
		tender.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listTender", cgTenderService.queryAllTender(tender));
		//查询所有的付款方式
		PayMethod paymethod = new PayMethod();
		paymethod.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listPayMethod", cgTenderService.queryAllPayType(paymethod));
		if ("0".equals(status)){
			return new ModelAndView(CgTenderConstants.VIEW_CGTENDER,modelMap);
		}else{
			return new ModelAndView(CgTenderConstants.UPDATE_CGTENDER,modelMap);
		}
	}
	
	/**
	 * 修改招标书
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateCgTender")
	@ResponseBody
	public String updateCgTender(HttpSession session,CgTender cgtender)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String res = cgTenderService.updateCgTender(session,cgtender);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改采购招标:"+cgtender.getVcgtendername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 删除招标书
	 * @param session
	 * @param vcodes
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteCgTender")
	@ResponseBody
	public String deleteCgTender(HttpSession session,String vcodes)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CgTender cgtender = new CgTender();
		String names = "";
		cgtender.setListPk_cgtenders(Arrays.asList(vcodes.split(",")));
		cgtender.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<CgTender> listCgTender = cgTenderService.queryAllCgTender(cgtender, page);
		if(!listCgTender.isEmpty()){
			for(CgTender cgTenders : listCgTender){
				names += ","+cgTenders.getVcgtendername();
			}
		}
		String res = cgTenderService.deleteCgTender(cgtender);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除采购招标:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 跳转到招标书应标详情页面
	 * @param modelMap
	 * @param cgtender
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toTenderYbinfo")
	public ModelAndView toTenderYbinfo(ModelMap modelMap,CgTender cgtender,HttpSession session,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgtender.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listCgTenderd", cgTenderService.queryAllCgTenderd(cgtender,page));
		modelMap.put("cgtender", cgtender);
		modelMap.put("pageobj", page);
		return new ModelAndView(CgTenderConstants.LIST_CGTENDERYBINFO,modelMap);
	}
	
	/**
	 * 跳转到修改页面
	 * @param modelMap
	 * @param cgtender
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddCgTenderD")
	public ModelAndView toAddCgTenderD(ModelMap modelMap,String pk_cgtender,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		CgTenderd cgTenderd = new CgTenderd();
		cgTenderd.setPk_cgtender(pk_cgtender);
		modelMap.put("cgTenderd", cgTenderd);
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		return new ModelAndView(CgTenderConstants.ADD_CGTENDERD, modelMap);
	}
	
	/**
	 * 新增招标
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveCgtenderDByAdd")
	public ModelAndView saveCgtenderDByAdd(ModelMap modelMap, HttpSession session,CgTenderd cgtenderd)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增采购招标应标:"+cgtenderd.getDelivername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		cgtenderd.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		cgTenderService.insertCgTenderd(cgtenderd);
		//加入日志
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 跳转到修改招标页面
	 * @param modelMap
	 * @param cgtender
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdateCgTenderD")
	public ModelAndView toUpdateCgTenderD(ModelMap modelMap,String pk_cgtenderd,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CgTenderd cgtend = new CgTenderd();
		cgtend.setPk_cgtenderd(pk_cgtenderd);
		CgTenderd cgtenderd = cgTenderService.queryCgTenderdByID(cgtend);
		modelMap.put("cgtenderd", cgtenderd);
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		CodeDes codeDes = new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_PS_AREA));
		modelMap.put("psarea", codeDesService.findCodeDes(codeDes, page));
		codeDes.setTyp(String.valueOf(CodeDes.S_AREA));
		modelMap.put("area", codeDesService.findCodeDes(codeDes, page));
		return new ModelAndView(CgTenderConstants.UPDATE_CGTENDERD, modelMap);
	}
	
	/**
	 * 修改应标书
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateCgTenderd")
	public ModelAndView updateCgTenderd(ModelMap modelMap, HttpSession session,CgTenderd cgtenderd)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		cgTenderService.updateCgTenderd(session, cgtenderd);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改采购招标:"+cgtenderd.getDelivername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
//		return res;
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 删除供应商应标书
	 * @param session
	 * @param vcodes
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteCgTenderD")
	@ResponseBody
	public String deleteCgTenderD(HttpSession session,String vcodes)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		CgTenderd cgtenderd = new CgTenderd();
		String names = "";
		cgtenderd.setListPk_cgtenderds(Arrays.asList(vcodes.split(",")));
		cgtenderd.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<CgTenderd> listCgTender = cgTenderService.queryAllCgTenderd(cgtenderd);
		if(!listCgTender.isEmpty()){
			for(CgTenderd cgTenders : listCgTender){
				names += ","+cgTenders.getDelivername();
			}
		}
		String res = cgTenderService.deleteCgTenderd(cgtenderd);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除供应商应标书:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
}
