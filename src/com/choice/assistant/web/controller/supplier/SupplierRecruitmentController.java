package com.choice.assistant.web.controller.supplier;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.supplier.SupplierRecruitmentConstants;
import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.supplier.SupplierRM;
import com.choice.assistant.domain.supplier.SupplierTO;
import com.choice.assistant.service.supplier.SupplierRecruitmentService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.service.system.FavoritesService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.Local;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

@Controller
@RequestMapping("supplierrm")
public class SupplierRecruitmentController {
	
	@Autowired
	private SupplierRecruitmentService supplierRMService;
    @Autowired
    private CoderuleService coderuleService;
    @Autowired
    private FavoritesService favoritesService;
	@Autowired
	private LogsMapper logsMapper;
    @Autowired
    private ContracttermsService contracttermsService;
	
	/**
	 * 供应商招募
	 * @param modelMap
	 * @param session
	 * @param supplierRM
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/srmlist")
	public ModelAndView srmlist(ModelMap modelMap,HttpSession session,SupplierRM supplierRM,Page page) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplierRM.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<SupplierRM>  supplierRMList = supplierRMService.querySupplierRM(supplierRM,page);
		modelMap.put("supplierRMList", supplierRMList);
        modelMap.put("pageobj", page);
        modelMap.put("vcode", supplierRM.getVcode());
        modelMap.put("vname", supplierRM.getVname());
        modelMap.put("istate", supplierRM.getIstate()==null?0:supplierRM.getIstate());
		return new ModelAndView(SupplierRecruitmentConstants.SRMLIST,modelMap);
	}
	/**
	 * 根据供应商招募pk获取数据
	 * @param modelMap
	 * @param supplierRM
	 * @return
	 * @throws CRUDException
	 */
	  @RequestMapping(value ="/srmchildlist")
	  public ModelAndView srmchildlist(ModelMap modelMap,HttpSession session,SupplierTO supplierTO) throws CRUDException {
	      DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
	      if(supplierTO.getPk_recruit()!=null) {
	    	  supplierTO.setAcct(session.getAttribute("ChoiceAcct").toString());
	          modelMap.put("supplierTO", supplierRMService.findSupplierRMByPk(supplierTO));
	          modelMap.put("supplierTOS", supplierRMService.findSupplierRMByPkS(supplierTO));
	      }
	      return new ModelAndView(SupplierRecruitmentConstants.SRMCHILDLIST,modelMap);
	  }

    /**
     * 供应商招募新增修改跳转界面
     * @param modelMap
     * @param supplier
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/rminsertorupdate")
    public ModelAndView rminsertorupdate(ModelMap modelMap,SupplierRM supplierRM,HttpSession session) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(supplierRM.getPk_recruit()!=null) {
        	modelMap.put("quote",true);
        	supplierRM.setAcct(session.getAttribute("ChoiceAcct").toString());
            modelMap.put("supplierRM", supplierRMService.findSupplierRMZHUByPk(supplierRM));
        }else {
        	supplierRM.setVcode(coderuleService.getCode(CoderuleConstants.RECRUIT,session.getAttribute("ChoiceAcct").toString()));
        	supplierRM.setDreleasedat(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
            modelMap.put("supplierRM", supplierRM);
        }
        return new ModelAndView(SupplierRecruitmentConstants.RMINSERTORUPDATE,modelMap);
    }
    /**
     * 供应商招募主表添加
     * @param supplierRM
     * @return
     */
    @RequestMapping(value = "/addSupplierRM")
    @ResponseBody
    public String addSupplierRM(HttpSession session,SupplierRM supplierRM) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
	        String res = supplierRMService.addSupplierRM(supplierRM, session.getAttribute("ChoiceAcct").toString(),
	                CodeHelper.createUUID());
			//加入日志
			Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增供应商招募:"+supplierRM.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
			logsMapper.addLogs(logs);
			return res;
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.SAVE_ERROR);     
    }
    /**
     * 供应商招募主表修改
     * @param session
     * @param supplierRM
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/updateSupplierRM")
    @ResponseBody
    public String updateSupplierRM(HttpSession session,SupplierRM supplierRM) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
        	supplierRMService.updateSupplierRM(supplierRM);
			//加入日志
			Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改供应商招募:"+supplierRM.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
			logsMapper.addLogs(logs);
        	return "ok";
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.SAVE_ERROR);  
    }
    /**
     * 供应商应募申请删除，dr的值变为1
     * @param session
     * @param supplierRM
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/updateyminfo")
    @ResponseBody
    public String updateyminfo(HttpSession session,String code) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    	String ts=df.format(new Date());//当前时间
		String names = "";
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("colName","delivername");
		paramMap.put("tableName","cscm_enlist");
		paramMap.put("acct",session.getAttribute("ChoiceAcct"));
		paramMap.put("pkCol","pk_enlist");
		paramMap.put("pk_id",AsstUtils.StringCodeReplace(code));
		List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
		for(Map<String,Object> map : listMap){
			names += ","+map.get("delivername");
			}
        supplierRMService.updateyminfo(code,ts);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除供应商应募:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return "ok";
    }
    /**
     * 供应商应募恢复，dr的值变为0
     * @param session
     * @param supplierRM
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/hfupdateyminfo")
    @ResponseBody
    public String hfupdateyminfo(HttpSession session,String code) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    	String ts=df.format(new Date());//当前时间
        supplierRMService.hfupdateyminfo(code,ts);
        return "ok";
    }
    /**
     * 删除供应商招募
     * @param modelMap
     * @param ids
     * @return
     */
    @RequestMapping(value = "/deleteSupplierRM")
    public ModelAndView deleteSupplierRM(ModelMap modelMap,String ids,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String names = "";
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("colName","vname");
		paramMap.put("tableName","cscm_recruit");
		paramMap.put("acct",session.getAttribute("ChoiceAcct"));
		paramMap.put("pkCol","pk_recruit");
		paramMap.put("pk_id",AsstUtils.StringCodeReplace(ids));
		List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
		for(Map<String,Object> map : listMap){
			names += ","+map.get("vname");
			}
        supplierRMService.deleteSupplierRM(ids);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除供应商招募:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
    }
    /**
     * 状态
     * @param enablestate
     * @param ids
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/uEnablestate")
    public ModelAndView updateEnablestate(String enablestate, String ids) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        supplierRMService.updateEnablestate(enablestate, ids);
        return new ModelAndView(StringConstant.ACTION_DONE);
    }
    /**
     * 待审核供应商
     * @param modelMap
     * @param collDeliver
     * @param auditsupplier
     * @param session
     * @param page
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/addymgyslb")
	public String addInAuditMaterial(ModelMap modelMap,String code,SupplierTO supplierTO, Auditsupplier auditsupplier,HttpSession session,Page page) throws Exception {
    	DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	supplierTO = new SupplierTO();
    	supplierTO.setAcct(session.getAttribute("ChoiceAcct").toString());
    	supplierTO.setDelivercode(code);
    	SupplierTO supplierTOListbygysid = supplierRMService.findAllSupplierTObygysid(supplierTO);
		//判断加入供应商待审核是否重复
		if(ValueCheck.IsEmpty(supplierRMService.findAuditbypks(supplierTOListbygysid.getDelivercode(),session.getAttribute("ChoiceAcct").toString()))){
			auditsupplier = new Auditsupplier();
	    	auditsupplier.setAcct(session.getAttribute("ChoiceAcct").toString());
	    	auditsupplier.setPk_auditsupplier(CodeHelper.createUUID());
	    	auditsupplier.setPk_supplier(CodeHelper.createUUID());
	    	auditsupplier.setVcode(supplierTOListbygysid.getDelivercode());
	    	auditsupplier.setVname(supplierTOListbygysid.getDelivername());
//	    	auditsupplier.setVlevel(supplierTOListbygysid.getVdeliverlevel());
	    	auditsupplier.setVsuppliertele(supplierTOListbygysid.getVsupplierphone());
	    	auditsupplier.setVsupplieraddress(supplierTOListbygysid.getVsupplieraddress());
//	    	auditsupplier.setVmemo(supplierTOListbygysid.getVmemo());
	    	auditsupplier.setDr("0");
	    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	    	auditsupplier.setTs(df.format(new Date()));//当前时间
	    	favoritesService.saveAuditDeliver(auditsupplier);
	    	return "ok";
		}
		return "no";
    }
    
	/**
	 * 跳转到招募书应募详情页面
	 * @param modelMap
	 * @param cgtender
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toSupplierTOInfo")
	public ModelAndView toSupplierTOInfo(ModelMap modelMap, SupplierRM supplierRM, HttpSession session, Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplierRM.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("listSupplierTO", supplierRMService.queryAllSupplierTO(supplierRM, page));
//		modelMap.put("supplierRM", supplierRM);
		modelMap.put("pageobj", page);
		return new ModelAndView(SupplierRecruitmentConstants.LIST_SUPPLIERTOINFO,modelMap);
	}
	
	/**
	 * 跳转到新增供应商应募页面
	 * @param modelMap
	 * @param pk_recruit
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddSupplierTO")
	public ModelAndView toAddSupplierTO(ModelMap modelMap, String pk_recruit, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		
		SupplierTO supplierTO = new SupplierTO();
		supplierTO.setPk_recruit(pk_recruit);
		modelMap.put("supplierTO", supplierTO);
		return new ModelAndView(SupplierRecruitmentConstants.ADD_SUPPLIERTO, modelMap);
	}
	
	/**
	 * 新增应募
	 * @param session
	 * @param supplierTO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveSupplierTOByAdd")
	public ModelAndView saveSupplierTOByAdd(ModelMap modelMap, HttpSession session, SupplierTO supplierTO)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增供应商应募:"+supplierTO.getDelivername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		supplierTO.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		supplierRMService.insertSupplierTO(supplierTO);
		//加入日志
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 跳转到修改应募页面
	 * @param modelMap
	 * @param supplierTO
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdateSupplierTO")
	public ModelAndView toUpdateSupplierTO(ModelMap modelMap, String pk_enlist, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		SupplierTO supplierTO = new SupplierTO();
		supplierTO.setPk_enlist(pk_enlist);
		supplierTO.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplierTO = supplierRMService.querySupplierTOByID(supplierTO);
		modelMap.put("supplierTO", supplierTO);
		return new ModelAndView(SupplierRecruitmentConstants.UPDATE_SUPPLIERTO, modelMap);
	}
	
	/**
	 * 修改应募书
	 * @param session
	 * @param cgtender
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateSupplierTO")
	public ModelAndView updateSupplierTO(ModelMap modelMap, HttpSession session,SupplierTO supplierTO)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplierRMService.updateSupplierTO(session, supplierTO);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改供应商应募:"+supplierTO.getDelivername(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
//		return res;
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	/**
	 * 删除供应商应募书
	 * @param session
	 * @param vcodes
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deleteSupplierTO")
	@ResponseBody
	public String deleteSupplierTO(HttpSession session,String vcodes)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		SupplierTO supplierTO = new SupplierTO();
		String names = "";
		supplierTO.setPk_enlists(Arrays.asList(vcodes.split(",")));
		supplierTO.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<SupplierTO> listSupplierTO = supplierRMService.queryAllSupplierTO(supplierTO);
		if(!listSupplierTO.isEmpty()){
			for(SupplierTO supplierTOs : listSupplierTO){
				names += ","+supplierTOs.getDelivername();
			}
		}
		String res = supplierRMService.deleteSupplierTO(supplierTO);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除供应商应募书:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
}
