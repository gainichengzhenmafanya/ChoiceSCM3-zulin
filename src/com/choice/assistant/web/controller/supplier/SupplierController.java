package com.choice.assistant.web.controller.supplier;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.supplier.SupplierConstants;
import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.assistant.domain.material.Material;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.domain.supplier.SupplierType;
import com.choice.assistant.service.supplier.SupplierService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ExcelUtil;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.PrintDataUtil;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.Local;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Deliver;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DeliverService;
/**
 * 供应商
 * @author lenovo
 *
 */
@Controller
@RequestMapping("supplier")
public class SupplierController {
	
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private DeliverService deliverService;
	@Autowired
	private CodeDesService codeDesService;	
	
	/**
	 * 供应商列表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/list")
	public ModelAndView queryDeliver(ModelMap modelMap,HttpSession session,Supplier supplier,Page page,String moduleId) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String delivercodeType = supplier.getSuppliertype();
        supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Supplier>  supplierList = supplierService.querySupplier(supplier,page);
        supplier.setEnablestate(supplier.getEnablestate()==null?0:supplier.getEnablestate());
		modelMap.put("supplierList", supplierList);
        modelMap.put("pageobj", page);
        supplier.setSuppliertype(delivercodeType);//将传进来的主键去掉Service里加的字符
        modelMap.put("supplier", supplier);
        modelMap.put("moduleId", moduleId);
		return new ModelAndView(SupplierConstants.LIST,modelMap);
	}

    /**
     * 新增修改跳转界面
     * @param modelMap
     * @param supplier
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/skip")
    public ModelAndView skip(ModelMap modelMap,Supplier supplier,HttpSession session,String moduleId) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        String group=session.getAttribute("ChoiceAcct").toString();
        if(supplier.getPk_supplier()!=null) {
//            if(!supplierService.supplierCheckQuote(supplier.getPk_supplier(),group)){
                modelMap.put("quote",true);
//            }
            modelMap.put("supplier", supplierService.findSupplierByPk(supplier));
            modelMap.put("operate", 1);//修改
        }else {
//            supplier.setVcode(coderuleService.getCode(CoderuleConstants.SUPPLIER,group));
            modelMap.put("supplier", supplier);
            modelMap.put("operate", 0);//新增
        }
        SupplierType type=new SupplierType();
        type.setAcct(group);
        type.setEnablestate(2);
        modelMap.put("supplierType",supplierService.queryLastSupplierType(type));
        modelMap.put("moduleId", moduleId);
        return new ModelAndView(SupplierConstants.SKIP,modelMap);
    }

    /**
     * 供应商列表详细信息界面
     * @param modelMap
     * @param supplier
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value ="/listbottom")
    public ModelAndView bottom(ModelMap modelMap,Supplier supplier) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(supplier.getPk_supplier()!=null) {
            modelMap.put("supplier", supplierService.findSupplierByPk(supplier));
        }
        return new ModelAndView(SupplierConstants.LISTBOTTOM,modelMap);
    }
    /**
     * 供应商类别
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/typeList")
    public ModelAndView typeList(ModelMap modelMap,SupplierType supplierType,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        SupplierType type=new SupplierType();
        type.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("typeList",supplierService.queryAllSupplierType(type));
        modelMap.put("type",supplierType);
        return new ModelAndView(SupplierConstants.TYPE,modelMap);
    }

    /**
     * 供应商添加
     * @param supplier
     * @return
     */
    @RequestMapping(value = "/addSupplier")
    @ResponseBody
    public String addSupplier(HttpSession session,Supplier supplier){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
        	String res =  supplierService.addSupplier(supplier, session.getAttribute("ChoiceAcct").toString(),
                    CodeHelper.createUUID());
        	if("ok".equals(res)){
	    		//加入日志
	    		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增供应商:"+supplier.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL,session.getAttribute("ChoiceAcct"));
	    		logsMapper.addLogs(logs);
        	}
    		return res;
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.SAVE_ERROR);
    }

    /**
     * 删除供应商
     * @param modelMap
     * @param ids
     * @return
     */
    @RequestMapping(value = "/delete")
    public ModelAndView deleteSupplier(ModelMap modelMap,HttpSession session,String ids){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
        	String names = "";
        	Supplier supplier = new Supplier();
        	supplier.setPk_supplier(AsstUtils.StringCodeReplace(ids));
        	List<Supplier> listSuppliers = supplierService.querySupplierListByPk(supplier);
        	for(Supplier supplierd : listSuppliers){
        		names += ","+supplierd.getVname();
        	}
            String msg=supplierService.deleteSupplier(ids,session.getAttribute("ChoiceAcct").toString());
            if("error".equals(msg)){
                modelMap.put("msg", Local.show(LanguagesConstants.SUPPLIER_QUOTED));
                return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
            }
    		//加入日志
    		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除供应商:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL,session.getAttribute("ChoiceAcct"));
    		logsMapper.addLogs(logs);
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
    }

    /**
     * 修改供应商信息
     * @param session
     * @param supplier
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public String updateSupplier(HttpSession session,Supplier supplier){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
             String res = supplierService.updateSupplier(supplier, session.getAttribute("ChoiceAcct").toString());
    		//加入日志
    		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改供应商:"+supplier.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL,session.getAttribute("ChoiceAcct"));
    		logsMapper.addLogs(logs);
    		return res;
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.SAVE_ERROR);
    }
    /**
     * 启用、禁用供应商
     * @param enablestate
     * @param ids
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/uEnablestate")
    public ModelAndView updateEnablestate(String enablestate, String ids,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
    	String names = "";
    	Logs logs  = new Logs();
    	Supplier supplier = new Supplier();
    	supplier.setPk_supplier(AsstUtils.StringCodeReplace(ids));
    	List<Supplier> listSuppliers = supplierService.querySupplierListByPk(supplier);
    	for(Supplier supplierd : listSuppliers){
    		names += ","+supplierd.getVname();
    	}
    	if("2".equals(enablestate)){
    		logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"启用供应商："+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL,session.getAttribute("ChoiceAcct"));
    	}else{
    		logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"禁用供应商："+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL,session.getAttribute("ChoiceAcct"));
    	}
        supplierService.updateEnablestate(enablestate, ids);
		logsMapper.addLogs(logs);
        return new ModelAndView(StringConstant.ACTION_DONE);
    }
	
	/**
	 * 根据输入的条件查询 前n条记录
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSupplierTop")
	@ResponseBody
	public List<Supplier> findSupplierTop(Supplier supplier, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
		return supplierService.findSupplierTop(supplier);
	}
	
	/**
	 * 根据输入的条件查询 前n条记录
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findSupplierByMaterialTop")
	@ResponseBody
	public List<Supplier> findSupplierByMaterialTop(Material material, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		material.setAcct(session.getAttribute("ChoiceAcct").toString());
//		material.setLimit("N");
		return supplierService.findSupplierByMaterialTop(material);
	}

    //===============================共用部分⬇===========================
	/**
	 * 共用查询供应商列表
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/supplierList")
	public ModelAndView querySupplier(ModelMap modelMap,Supplier supplier,Page page,HttpSession session,String callBack,String domId,boolean single,String pk_material,String delivercode,String pk_purtemplet) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplier.setEnablestate(2);
		supplier.setPk_purtemplet(pk_purtemplet);
		List<Supplier>  supplierList = null;
		if(null!=pk_material && !"".equals(pk_material)){
			Material material = new Material();
//			material.setPk_material(pk_material);
			supplierList = supplierService.findSupplierByMaterialTop(material);
			modelMap.put("pk_material", pk_material);
		}else{
			supplierList = supplierService.querySupplier(supplier,page);
		}
		
        SupplierType type=new SupplierType();
        type.setAcct(supplier.getAcct());
        type.setEnablestate(2);
		List<SupplierType> supplierTypeList = supplierService.queryAllSupplierType(type);
		modelMap.put("supplierList", supplierList);
		modelMap.put("supplierTypeList", supplierTypeList);
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		modelMap.put("domId", domId);
		modelMap.put("single", single);
		modelMap.put("delivercode", delivercode);
		modelMap.put("pk_purtemplet", pk_purtemplet);
		return new ModelAndView(SupplierConstants.SELECT_DELIVER,modelMap);
	}
	
	/**
	 * 导出
	 * @return
	 * @throws CRUDException
	 * @throws IOException 
	 */
	@RequestMapping("/exportExcel")
	@ResponseBody
	public void exportExcel(HttpServletResponse response,HttpServletRequest request,HttpSession session,Supplier supplier) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Supplier> materiallist = supplierService.queryAllSupplier(supplier);
		
		String[] propertiesArray = {"vcode","vname","delivercodetype.vname","vcontact","vtele","vaddr"};
		String[] nameArray = {"供应商编码","供应商名称","供应商类别","联系人","联系方式","地址"};
		ExcelUtil.exportExcel(request, response, materiallist, propertiesArray, nameArray, "供应商信息", "供应商"+DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDD));
	}

    /**
     * 修改商城供应商
     * @param session
     * @param supplier
     */
    @RequestMapping("/updatajmu")
    @ResponseBody
    public String updataJmu(HttpSession session,Supplier supplier){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
            supplierService.updateJmu(supplier);
            supplier = supplierService.findSupplierByPk(supplier);
    		//加入日志
    		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改供应商："+supplier.getVname()+"对应的商城供应商为："+supplier.getVnamejmu(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL,session.getAttribute("ChoiceAcct"));
    		logsMapper.addLogs(logs);
            return "ok";
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return "error";
    }

    /**
     * 获取供应商供应物资
     * @param session
     * @param supplier
     * @return
     */
    @RequestMapping("/getMaterial")
    public ModelAndView getMaterial(ModelMap modelMap,HttpSession session,Supplier supplier){
        try {
            supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
            supplier=supplierService.findSupplicerByCode(supplier);
            modelMap.put("supplier",supplier);
            modelMap.put("materiallist", supplierService.getMaterial(supplier));
            return new ModelAndView(SupplierConstants.RELEVANCE_MATERIAL,modelMap);
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        modelMap.put("msg",Local.show(LanguagesConstants.HANDLE_FAILURE));
        return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
    }

    /**
     * 修改供应商供应物资
     * @param session
     * @param supplier
     * @return
     */
    @RequestMapping("/updataMaterial")
    @ResponseBody
    public String updataMaterial(HttpSession session,Supplier supplier){
        try {
            supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
            supplierService.updateMaterial(supplier);
            return "ok";
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.UPDATE_ERROR);
    }
    

	/**
	 * 打印
	 * @param modelMap
	 * @param vcodes
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/printSupplier")
	public ModelAndView printSupplier(ModelMap modelMap,HttpServletRequest request,HttpSession session,String vcodes,String type,Supplier supplier)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		List<Supplier> listSupplier = supplierService.querySupplier(supplier,page);
		Map<String,Object> mapRes = MapBeanConvertUtil.convertBean(supplier);
		Map<String,Object> mapRe  = new HashMap<String,Object>();
        for(Entry<String, Object> entry : mapRes.entrySet()){  
            String k=entry.getKey();  
            if(k.indexOf(".")<0 ){
            	mapRe.put(k, entry.getValue());
            }
        }
        mapRe.remove("delivercodetype");
		return PrintDataUtil.printData(modelMap, request, session, mapRe, type, "供应商列表", listSupplier, SupplierConstants.SUPPLIERIREPORT);
	}
	/**
	 * 检测供应商引用状态
	 * @param supplierType
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/checkSupplierQuote")
	@ResponseBody
	public String checkSupplierQuote(HttpSession session,Supplier supplier) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String res = "false";
		supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
		res = supplierService.checkSupplierQuote(supplier);
		return res;
	}
	/**
	 * 检测供应商编码是否已经存在
	 * @param session
	 * @param supplier
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/findSupplieByCode")
	@ResponseBody
	public String findSupplieByCode(HttpSession session,Supplier supplier) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String res = "false";
		supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
		supplier = supplierService.findSupplicerByCode(supplier);
		if(ValueCheck.IsNotEmpty(supplier)){
			res="fasle";//编码重复
		}else{
			res="true";
		}
		return res;
	}

	/**
	 * 选择供应商
	 * @param modelMap
	 * @param positn
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectOneDeliver")
	public ModelAndView selectOneDeliver(ModelMap modelMap, Deliver deliver, Page page, String defaultCode, String defaultName,String callBack,String sp_code) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("callBack", callBack);
		modelMap.put("sp_code", sp_code);
		modelMap.put("deliver", deliver);
		if(null!=defaultName){
			modelMap.put("defaultName", URLDecoder.decode(defaultName, "UTF-8"));
		}
		//供应商定义主界面
		modelMap.put("delivierListType", deliverService.findDeliverType());
		return new ModelAndView(SupplierConstants.SELECT_ONEDELIVERS2, modelMap);
	}
	/**
	 * 
	 * @Title: findAllDelivers2 
	 * @Description: (供货商列表右侧（选择单一供货商使用）) 
	 * @Author：LI Shuai
	 * @date：2014-1-23上午9:50:10
	 * @param modelMap
	 * @param page
	 * @param deliver
	 * @return
	 * @throws Exception  ModelAndView
	 * @throws
	 */
	@RequestMapping(value = "/deliverList2")
	public ModelAndView findAllDelivers2(ModelMap modelMap, Page page, Deliver deliver, HttpSession session,String callBack,String sp_code) throws Exception
	{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//供应商定义主界面
		CodeDes codeDes=new CodeDes();
		codeDes.setTyp(String.valueOf(CodeDes.S_DEL_TYPE));//供应商类型
		modelMap.put("codeDesList", codeDesService.findCodeDes(codeDes));
		//供应商权限筛选
		deliver.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
		deliver.setAccountId(session.getAttribute("accountId").toString());
		deliver.setSp_code(sp_code);
		modelMap.put("deliverList", deliverService.findDeliver(deliver, page));
		modelMap.put("pageobj", page);
		modelMap.put("typCode", deliver.getTypCode());
		modelMap.put("deliver", deliver);
		modelMap.put("callBack", callBack);
		return new ModelAndView(SupplierConstants.TABLE_DELIVERS2,modelMap);
	}
}
