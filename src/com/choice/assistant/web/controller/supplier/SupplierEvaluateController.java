package com.choice.assistant.web.controller.supplier;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.supplier.SupplierEvaluateConstants;
import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.supplier.Suppliereval;
import com.choice.assistant.domain.system.Evalitem;
import com.choice.assistant.service.supplier.SupplierEvaluateService;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.Local;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.Deliver;
import com.choice.scm.service.DeliverService;

/**
 * 供应商评价
 * Created by mc on 14-12-4.
 */
@Controller
@RequestMapping("evaluate")
public class SupplierEvaluateController {
    @Autowired
    private DeliverService deliverService;
    @Autowired
    private SupplierEvaluateService supplierEvaluateService;

    /**
     * 供应商评价显示
     * @param modelMap
     * @param session
     * @param supplier
     * @param page
     * @return
     * @throws Exception
     */
    @RequestMapping("/list")
    public ModelAndView list(ModelMap modelMap, HttpSession session, Deliver deliver, Page page) throws Exception{
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
//        deliver.setAcct(session.getAttribute("ChoiceAcct").toString());
        List<Deliver> supplierList = deliverService.findDeliver(deliver, page);
//        supplier.setEnablestate(supplier.getEnablestate()==null?0:supplier.getEnablestate());
        modelMap.put("supplierList", supplierList);
        modelMap.put("pageobj", page);
        modelMap.put("deliver", deliver);
        return new ModelAndView(SupplierEvaluateConstants.LIST, modelMap);
    }

    /**
     * 供应商评价信息
     * @param modelMap
     * @param session
     * @return
     */
    @RequestMapping("/listbottom")
    public ModelAndView listbottom(ModelMap modelMap, HttpSession session, Suppliereval suppliereval){
        try {
        	if(suppliereval.getPk_supplier()==null){
        		modelMap.put("supplierevalList", null);
        	}else{
        		modelMap.put("supplierevalList", supplierEvaluateService.listbottom(suppliereval, session.getAttribute("ChoiceAcct").toString()));
        	}
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return new ModelAndView(SupplierEvaluateConstants.LISTBOTTOM, modelMap);
    }

    @RequestMapping("/saveEval")
    @ResponseBody
    public String saveEvaluate(Evalitem evalitem,HttpSession session){
        try {
            supplierEvaluateService.saveEvalitem(evalitem,session.getAttribute("ChoiceAcct").toString());
            return "success";
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.SAVE_ERROR);
    }
    /**
     * 供应商评价体系管理
     * @param modelMap
     * @param session
     * @return
     */
    @RequestMapping("/showEvaluate")
    public ModelAndView showEvaluate(ModelMap modelMap,HttpSession session,String m,String delivercode){
        try {
            modelMap.put("evalitemList",supplierEvaluateService.queryEvalitem(new Evalitem(),session.getAttribute("ChoiceAcct").toString()));
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        if("1".equals(m)) {
            return new ModelAndView(SupplierEvaluateConstants.ADDEVALUATE, modelMap);
        }else{
            modelMap.put("delivercode",delivercode);
            return new ModelAndView(SupplierEvaluateConstants.EVALUATE,modelMap);
        }
    }

    @RequestMapping("evaluation")
    @ResponseBody
    public String addEvaluation(HttpSession session,Suppliereval suppliereval){
        try {
            return supplierEvaluateService.addEvaluation(suppliereval,session.getAttribute("ChoiceAcct").toString());
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.SAVE_ERROR);
    }
}
