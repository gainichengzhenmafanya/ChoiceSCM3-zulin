package com.choice.assistant.web.controller.supplier;

import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.supplier.AuditConstants;
import com.choice.assistant.domain.supplier.Auditsupplier;
import com.choice.assistant.domain.supplier.Supplier;
import com.choice.assistant.domain.supplier.SupplierType;
import com.choice.assistant.service.supplier.AuditService;
import com.choice.assistant.service.supplier.SupplierService;
import com.choice.assistant.util.AsstUtils;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 供应商审核
 * Created by mc on 14-10-29.
 */
@Controller
@RequestMapping("audit")
public class AuditController {
    @Autowired
    private SupplierService supplierService;
    @Autowired
    private AuditService auditService;
	@Autowired
	private LogsMapper logsMapper;

    @RequestMapping(value = "/list")
    public ModelAndView list(ModelMap modelMap,HttpSession session,String moduleId) throws CRUDException {
        Auditsupplier auditsupplier=new Auditsupplier();
        auditsupplier.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("audit",auditService.findAudit(auditsupplier));
        modelMap.put("moduleId",moduleId);
        return new ModelAndView(AuditConstants.LIST,modelMap);
    }

    /**
     * 供应商列表
     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/link")
    public ModelAndView queryDeliver(ModelMap modelMap,HttpSession session,Supplier supplier,Page page) throws Exception{
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        supplier.setAcct(session.getAttribute("ChoiceAcct").toString());
        supplier.setNojmu("1");
        List<Supplier> supplierList = supplierService.querySupplier(supplier,page);
        supplier.setVcodejmu(URLDecoder.decode(supplier.getVcodejmu(), "UTF-8"));
        supplier.setVnamejmu(URLDecoder.decode(supplier.getVnamejmu(), "UTF-8"));
        supplier.setVaddr(URLDecoder.decode(supplier.getVaddr(), "UTF-8"));
        supplier.setVtele(URLDecoder.decode(supplier.getVtele(), "UTF-8"));
        supplier.setVcontact(URLDecoder.decode(supplier.getVcontact(), "UTF-8"));
        supplier.setVmailaddr(URLDecoder.decode(supplier.getVmailaddr(), "UTF-8"));
        supplier.setVwebsite(URLDecoder.decode(supplier.getVwebsite(), "UTF-8"));
        supplier.setVfax(URLDecoder.decode(supplier.getVfax(), "UTF-8"));
        modelMap.put("supplierList", supplierList);
        modelMap.put("supplier", supplier);
        modelMap.put("pageobj", page);
        modelMap.put("vcode", supplier.getVcode());
        modelMap.put("enablestate", supplier.getEnablestate()==null?0:supplier.getEnablestate());
        return new ModelAndView(AuditConstants.LINK_SUPPLIER,modelMap);
    }
    /**
     * 供应商列表详细信息界面
     * @param modelMap
     * @param supplier
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value ="/listbottom")
    public ModelAndView bottom(ModelMap modelMap,Supplier supplier) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(supplier.getPk_supplier()!=null) {
            modelMap.put("supplier", supplierService.findSupplierByPk(supplier));
        }
        return new ModelAndView(AuditConstants.LINK_SUPPLIERBOTTOM,modelMap);
    }
    /**
     * 添加供应商信息 界面
     * @param modelMap
     * @param session
     * @param supplier
     * @return
     */
    @RequestMapping(value = "/showAddSupplier")
    public ModelAndView addSupplier(ModelMap modelMap,HttpSession session,Supplier supplier,String moduleId) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        SupplierType supplierType=new SupplierType();
        supplierType.setAcct(session.getAttribute("ChoiceAcct").toString());
        supplierType.setEnablestate(2);
        Auditsupplier auditsupplier=new Auditsupplier();
        auditsupplier.setVcode(supplier.getPk_supplierjum());
        auditsupplier.setAcct(session.getAttribute("ChoiceAcct").toString());
        List<Auditsupplier> list=auditService.findAudit(auditsupplier);
        auditsupplier=list!=null?list.get(0):auditsupplier;//如果不为null
        supplier.setVname(auditsupplier.getVname());//供应商名称
        supplier.setVaddr(auditsupplier.getVsupplieraddress());//地址
        supplier.setVtele(auditsupplier.getVsuppliertele());//电话
        supplier.setVcontact(auditsupplier.getVcontact());//联系人
        supplier.setVnamejmu(auditsupplier.getVname());
        supplier.setVmailaddr(auditsupplier.getVcompanyemail());//邮箱
        supplier.setVfax(auditsupplier.getVcompanyfax());//传真
        supplier.setVwebsite(auditsupplier.getVcompanyurl());//网址
        modelMap.put("supplierType", supplierService.queryAllSupplierType(supplierType));
        modelMap.put("supplier",supplier);
        modelMap.put("join",0);
        modelMap.put("moduleId",moduleId);
        return new ModelAndView(AuditConstants.ADD_SUPPLIER,modelMap);
    }

    /**
     * 供应商添加
     * @param supplier
     * @return
     */
    @RequestMapping(value = "/addSupplier")
    @ResponseBody
    public String addSupplier(HttpSession session,Supplier supplier){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        String msg= null;
        try {
            msg = supplierService.addSupplier(supplier, session.getAttribute("ChoiceAcct").toString(), CodeHelper.createUUID());
            if("ok".equals(msg)){
                auditService.delOneAudit(supplier.getPk_supplierjum());
        		//加入日志
        		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"待审核供应商中新增供应商:"+supplier.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
        		logsMapper.addLogs(logs);
                return "ok";
            }
        } catch (CRUDException e) {
            e.printStackTrace();
        }

        return "error";
    }

    /**
     * 待审核供应商删除
     * @param ids
     * @return
     */
    @RequestMapping(value = "/delSupplier")
    public ModelAndView delAudit(String ids,HttpSession session){
        try {
        	String names = "";
        	Auditsupplier auditsupplier = new Auditsupplier();
        	auditsupplier.setAcct(session.getAttribute("ChoiceAcct").toString());
        	auditsupplier.setPk_auditsupplier(AsstUtils.StringCodeReplace(ids));
        	List<Auditsupplier> listAuditsupplier = auditService.queryAudit(auditsupplier);
        	for(Auditsupplier auditsupplierd : listAuditsupplier){
        		names += ","+auditsupplierd.getVname();
        	}
            auditService.delAudit(ids);//加入日志
    		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"待审核供应商删除:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
    		logsMapper.addLogs(logs);
            return new ModelAndView(StringConstant.ACTION_DONE);
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return new ModelAndView(StringConstant.ERROR_DONE);
    }

    /**
     * 关联供应商
     * @param supplier
     * @return
     */
    @RequestMapping(value = "/linkSupplier")
    @ResponseBody
    public String linkSupplier(Supplier supplier){
        try {
            auditService.linkSupplier(supplier);
            auditService.delOneAudit(supplier.getPk_supplierjum());
            return "ok";
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return "error";
    }

    /**
     * 判定是否有关联
     * @param auditSupplier
     * @return
     */
    @RequestMapping("/islink")
    @ResponseBody
    public int islinkSupplier(String auditSupplier,HttpSession session){
        try {
            return auditService.isLink(auditSupplier,session.getAttribute("ChoiceAcct").toString());
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
