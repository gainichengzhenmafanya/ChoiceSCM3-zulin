package com.choice.assistant.web.controller.supplier;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.supplier.SupplierConstants;
import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.supplier.SupplierType;
import com.choice.assistant.domain.system.Coderule;
import com.choice.assistant.service.supplier.SupplierService;
import com.choice.assistant.service.supplier.SupplierTypeService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.util.AsstUtils;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Util;
/**
 * 供应商类别
 * @author 臧国良
 *
 */
@Controller
@RequestMapping("supplierType")
public class SupplierTypeController {
	
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private SupplierTypeService supplierTypeService;
    @Autowired
    private CoderuleService coderuleService;
	@Autowired
	private LogsMapper logsMapper;
    @Autowired
    private ContracttermsService contracttermsService;
	
    /**
     * 供应商类别
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/typeList")
    public ModelAndView typeList(ModelMap modelMap,SupplierType supplierType,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        SupplierType type=new SupplierType();
        type.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("typeList",supplierService.queryAllSupplierType(type));
        modelMap.put("type",supplierType);
        return new ModelAndView(SupplierConstants.TYPE,modelMap);
    }

    /**
     * 获取该类别下所有供应商类别
     * @param modelMap
     * @param supplierType
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/showType")
    public ModelAndView showType(ModelMap modelMap,HttpSession session,SupplierType supplierType) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        supplierType.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("supplierType",supplierService.queryAllSupplierType(supplierType));
        modelMap.put("type",supplierType);
        return new ModelAndView(SupplierConstants.TYPSHOW, modelMap);
    }

    /**
     * 显示供应商类别修改界面
     * @param modelMap
     * @param supplierType
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/showEdit")
    public ModelAndView showEdit(ModelMap modelMap,HttpSession session,SupplierType supplierType) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        int count=supplierService.haveSupplierType(supplierType.getPk_suppliertype());
        if(count>0){
            modelMap.put("quote",true);
        }
        SupplierType type=supplierService.findSupplierTypeByPk(supplierType);
        type.setParentcode(supplierType.getParentcode());
        type.setLvl(supplierType.getLvl());
        modelMap.put("type",type);
        modelMap.put("codeFormat",coderuleService.getFormat(CoderuleConstants.SUPPLIER_TYPE,session.getAttribute("ChoiceAcct").toString()));
        return new ModelAndView(SupplierConstants.TYPEEDIT, modelMap);
    }

    /**
     * 显示新增界面
     * @param modelMap
     * @param supplierType
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/showAdd")
    public ModelAndView showAddType(ModelMap modelMap,HttpSession session,SupplierType supplierType) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        Coderule coderule=new Coderule();
        coderule.setParentcode(supplierType.getParentcode());
        coderule.setLvl((supplierType.getLvl()==null||"".equals(supplierType.getLvl()))?0:Integer.valueOf(supplierType.getLvl()));
        coderule.setPk_coderule(CoderuleConstants.SUPPLIER_TYPE);
        coderule.setAcct(session.getAttribute("ChoiceAcct").toString());
        supplierType.setVcode(coderuleService.getCode(coderule));
        modelMap.put("codeFormat",coderuleService.getFormat(CoderuleConstants.SUPPLIER_TYPE,session.getAttribute("ChoiceAcct").toString()));
        modelMap.put("type",supplierType);
        return new ModelAndView(SupplierConstants.TYPEADD, modelMap);
    }

    /**
     * 新增供应商类别
     * @param supplierType
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "addType")
    public ModelAndView addType(ModelMap modelMap,SupplierType supplierType,HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        String msg=supplierService.addSupplierType(supplierType,session.getAttribute("ChoiceAcct").toString());
        if(!"ok".equals(msg)){
            modelMap.put("msg",msg);
            return new ModelAndView(StringConstant.ERROR_DONE);
        }
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增供应商类别::"+supplierType.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return new ModelAndView(StringConstant.ACTION_DONE);
    }

    /**
     * 修改供应商类别
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "updateType")
    public ModelAndView updateType(ModelMap modelMap,HttpSession session,SupplierType supplierType) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            supplierService.updateSupplierType(supplierType, session.getAttribute("ChoiceAcct").toString());
        }catch (CRUDException e){
            return new ModelAndView(StringConstant.ERROR_DONE);
        }
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改供应商类别::"+supplierType.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return new ModelAndView(StringConstant.ACTION_DONE);
    }

    /**
     * 删除类别
     * @param modelMap
     * @param ids
     * @return
     */
    @RequestMapping(value = "delType")
    public ModelAndView delType(ModelMap modelMap,String ids,HttpSession session){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
			String names = "";
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("colName","vname");
			paramMap.put("tableName","cscm_suppliertype");
			paramMap.put("acct",session.getAttribute("ChoiceAcct"));
			paramMap.put("pkCol","delivercodetype");
			paramMap.put("pk_id",AsstUtils.StringCodeReplace(ids));
			List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
			for(Map<String,Object> map : listMap){
				names += ","+map.get("vname");
				}
            String result=supplierService.delSupplierType(ids);
            if(!"ok".equals(result)){
                modelMap.put("msg",result);
                return new ModelAndView(StringConstant.ERROR_DONE,modelMap);
            }
			//加入日志
			Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除供应商类别:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
			logsMapper.addLogs(logs);
        } catch (CRUDException e) {
            return new ModelAndView(StringConstant.ERROR_DONE);
        }
        return new ModelAndView(StringConstant.ACTION_DONE);
    }

    /**
	 * 检测供应商类别引用状态
	 * @param supplierType
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/checkSupplierTypeQuote")
	@ResponseBody
	public String checkSupplierTypeQuote(HttpSession session,SupplierType supplierType) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String res = "false";
		supplierType.setAcct(session.getAttribute("ChoiceAcct").toString());
		res = supplierService.checkSupplierTypeQuote(supplierType);
		return res;
	}

	/**
	 * 跳转到供应商类别选择界面
	 * @param modelMap
	 * @param supplierType
	 * @param session
	 * @param callBack
	 * @param pk_check
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/chooseSupplierTypeTree")
	public ModelAndView chooseSupplierTypeTree(ModelMap modelMap, SupplierType supplierType,HttpSession session,String callBack,String pk_check) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supplierType.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("supplierTypeList",supplierTypeService.chooseSupplierTypeTree(supplierType,pk_check));
		modelMap.put("callBack", callBack);
		return new ModelAndView(SupplierConstants.CHOOSE_SUPPLIERTYPETREE, modelMap);
	}
}
