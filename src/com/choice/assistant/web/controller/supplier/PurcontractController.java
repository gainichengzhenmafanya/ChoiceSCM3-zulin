package com.choice.assistant.web.controller.supplier;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.supplier.PurcontractConstants;
import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.constants.system.LanguagesConstants;
import com.choice.assistant.domain.supplier.Contractclau;
import com.choice.assistant.domain.supplier.Contracteven;
import com.choice.assistant.domain.supplier.MaterialScope;
import com.choice.assistant.domain.supplier.Purcontract;
import com.choice.assistant.domain.supplier.Suitstore;
import com.choice.assistant.domain.supplier.Transationatta;
import com.choice.assistant.service.supplier.PurcontractService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.PrintDataUtil;
import com.choice.assistant.util.Util;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.Local;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.scm.domain.FirmDeliver;
import com.choice.scm.domain.Spprice;

/**
 * 采购合约
 * Created by mc on 14-10-24.
 */
@Controller
@RequestMapping("purcontract")
public class PurcontractController {
    @Autowired
    private PurcontractService purcontractService;
    @Autowired
    private CoderuleService coderuleService;
	@Autowired
	private LogsMapper logsMapper;

    /**
     * 数据查询
     *
     * @param modelMap
     * @param purcontract
     * @param page
     * @return
     */
    @RequestMapping(value = "/list")
    public ModelAndView purcontractList(ModelMap modelMap, HttpSession session, Purcontract purcontract, Page page) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        /*purcontractService.updatePurcontractState();*/
        purcontract.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("contractterms", purcontractService.findContractterms(purcontract.getAcct()));
        modelMap.put("purct", purcontractService.findPurcontract(purcontract, page));
        modelMap.put("page", page);
        modelMap.put("dsigndat", purcontract.getDsigndat());
        modelMap.put("pk_supplier", purcontract.getDelivercode() != null ? purcontract.getDelivercode().getCode() : "");
        modelMap.put("supplierName", purcontract.getDelivercode() != null ? purcontract.getDelivercode().getDes() : "");
        modelMap.put("vcontractcode", purcontract.getVcontractcode());
        modelMap.put("istate", purcontract.getIstate() == null ? 0 : purcontract.getIstate());
        return new ModelAndView(PurcontractConstants.LIST, modelMap);
    }

    /**
     * 采购合约新增-修改界面
     *
     * @return
     */
    @RequestMapping(value = "/skip")
    public ModelAndView purSkip(ModelMap modelMap, HttpSession session, Purcontract purcontract, String m) throws Exception {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        purcontract.setAcct(session.getAttribute("ChoiceAcct").toString());
        if (!"1".equals(m) || purcontract != null && purcontract.getPk_purcontract() != null && purcontract.getPk_purcontract() != "") {
            modelMap.put("pur", purcontractService.findPurcontractByPk(purcontract));
            modelMap.put("operate", 1);
        } else {
            purcontract.setVcontractcode(coderuleService.getCode(CoderuleConstants.PURCONTRACT,session.getAttribute("ChoiceAcct").toString()));
            purcontract.setPk_purcontract(CodeHelper.createUUID());
            modelMap.put("pur", purcontract);
            modelMap.put("operate", 0);
        }
        modelMap.put("batch", purcontractService.findBatch(purcontract));
        modelMap.put("items", purcontractService.findContractterms(session.getAttribute("ChoiceAcct").toString()));
        return new ModelAndView(PurcontractConstants.SKIP);
    }

    /**
     * 合约详细信息显示
     *
     * @param modelMap
     * @param purcontract
     * @return
     */
    @RequestMapping(value = "/listbottom")
    public ModelAndView purlistbottom(ModelMap modelMap, HttpSession session, Purcontract purcontract) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        purcontract.setAcct(session.getAttribute("ChoiceAcct").toString());
        if (purcontract != null && purcontract.getPk_purcontract() != null && purcontract.getPk_purcontract() != "") {
            modelMap.put("pur", purcontractService.findPurcontractByPk(purcontract));
        }
        return new ModelAndView(PurcontractConstants.LIST_BOTTOM, modelMap);
    }


    /**
     * 快速分配物品
     *
     * @return
     */
    @RequestMapping(value = "/quickMal")
    public ModelAndView quickMal(ModelMap modelMap, String delivercode, String pk_purcontract, int iquotebatch, String sp_code,String canupdate) throws CRUDException {
        modelMap.put("pk_purcontract", pk_purcontract);
        modelMap.put("iquotebatch", iquotebatch);
        modelMap.put("sp_code", sp_code);
        modelMap.put("delivercode", delivercode);
        modelMap.put("canupdate", canupdate);
        return new ModelAndView(PurcontractConstants.MATERIAL, modelMap);
    }

    /**
     * 快速分配物品 本地暂存
     *
     * @return
     */
    @RequestMapping(value = "/quickMalUnSave")
    public ModelAndView quickMalUnSave(ModelMap modelMap, String ids, String pk_material, String pk_supplier,String canupdate, int iquotebatch) throws CRUDException {
        modelMap.put("ids", ids);
        modelMap.put("sp_code", pk_material);
        modelMap.put("delivercode", pk_supplier);
        modelMap.put("canupdate", canupdate);
        modelMap.put("iquotebatch", iquotebatch);
        return new ModelAndView(PurcontractConstants.MATERIAL, modelMap);
    }

    /**
     * 快速分配物品
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/listMaterial")
    public ModelAndView addMaterial(ModelMap modelMap, HttpSession session, MaterialScope materialScope,String canupdate) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        materialScope.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("mat", purcontractService.queryMaterial(materialScope));
        modelMap.put("pk_material", materialScope.getPk_material());
        modelMap.put("dat", DateFormat.formatDate(new Date(), "yyyy-MM-dd"));
        modelMap.put("canupdate", canupdate);
        return new ModelAndView(PurcontractConstants.ALLMAL, modelMap);
    }

    /**
     * 获取所有门店
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/findStore")
    public ModelAndView findStore(ModelMap modelMap, HttpSession session, FirmDeliver firmDeliver, Suitstore suitstore, String ids) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if ((null!=suitstore.getSp_code() && !"".equals(suitstore.getSp_code())) && (ids == null || ids.equals(""))) {
            List<Suitstore> stores = purcontractService.findSuitstore(suitstore);//获得该合约已经有的门店
            if (stores != null && stores.size() > 0) {
                for (Suitstore s : stores) {
                    ids += s.getPositn().getCode() + ",";
                }
            }
        }
        firmDeliver.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("firmDeliverList", purcontractService.queryFirm(firmDeliver));
        modelMap.put("stores", ids);
        modelMap.put("iquotebatch", suitstore.getIquotebatch());
        modelMap.put("pk_purcontract", suitstore.getPk_purcontract());
        return new ModelAndView(PurcontractConstants.ALLSTORE, modelMap);
    }

    @RequestMapping(value = "/storeData")
    public ModelAndView findStoreData(ModelMap modelMap,HttpSession session, FirmDeliver firmDeliver, Suitstore suitstore, String ids){
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            if (ids == null || ids == "") {
                List<Suitstore> stores = null;//获得该合约已经有的门店
                suitstore.setAcct(session.getAttribute("ChoiceAcct").toString());
                stores = purcontractService.findSuitstore(suitstore);
                if (stores != null && stores.size() > 0) {
                    for (Suitstore s : stores) {
                        ids += s.getPositn().getCode() + ",";
                    }
                }
            }
            firmDeliver.setAcct(session.getAttribute("ChoiceAcct").toString());
            modelMap.put("supplierOrg", JSONArray.fromObject(purcontractService.queryFirm(firmDeliver)));
            modelMap.put("stores", ids);
            modelMap.put("pk_purcontract", suitstore.getPk_purcontract());
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return new ModelAndView(PurcontractConstants.ALLSTORE, modelMap);
    }

    /**
     * 根据合约pk与报价pK获取适用门店
     *
     * @param modelMap
     * @param suitstore
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/findSuitstore")
    public ModelAndView findSuitstore(ModelMap modelMap,HttpSession session,Suitstore suitstore) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        suitstore.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("store", purcontractService.findSuitstore(suitstore));
        return new ModelAndView(PurcontractConstants.STORELIST, modelMap);
    }

    /**
     * 保存合约
     *
     * @param purcontract
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/savePur")
    @ResponseBody
    public String savePurcontract(Purcontract purcontract, HttpSession session) {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
        	String res = purcontractService.savePurcontract(purcontract, session.getAttribute("ChoiceAcct").toString(), session.getAttribute("accountName").toString());

    		//加入日志
    		Logs logs=new Logs(CodeHelper.createUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增采购合约:"+purcontract.getVcontractname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
    		logsMapper.addLogs(logs);
            return res;
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.SAVE_ERROR);
    }

    /**
     * 保存合约
     *
     * @param purcontract
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/updatePur")
    @ResponseBody
    public String updatePurcontract(Purcontract purcontract, HttpSession session) {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
           String res = purcontractService.updatePurcontract(purcontract, session.getAttribute("ChoiceAcct").toString(),
                    session.getAttribute("accountId").toString(),session.getAttribute("accountName").toString());
    		//加入日志
    		Logs logs=new Logs(CodeHelper.createUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改采购合约:"+purcontract.getVcontractname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
    		logsMapper.addLogs(logs);
    		 return res;
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return Local.show(LanguagesConstants.UPDATE_ERROR);
    }

    /**
     * 删除合约
     *
     * @param ids
     * @param session
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/delPur")
    public ModelAndView delPurcontract(ModelMap modelMap, String ids,String versions, HttpSession session) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        Purcontract purcontract = new Purcontract();
        String names = "";
        Page page = new Page();
        page.setPageSize(Integer.MAX_VALUE);
        purcontract.setPk_purcontract(AsstUtils.StringCodeReplace(ids));
        purcontract.setAcct(session.getAttribute("ChoiceAcct").toString());
        List<Purcontract>  listPurcontracts= purcontractService.findPurcontract( purcontract,page);
        for(Purcontract purcontractd : listPurcontracts){
        	names += ","+purcontractd.getVcontractname();
        }
        String msg = purcontractService.delPurcontract(ids,versions,session.getAttribute("ChoiceAcct").toString(),
                session.getAttribute("accountId").toString(),session.getAttribute("accountName").toString());
        if (!"ok".equals(msg)) {
            modelMap.put("msg", msg);
            return new ModelAndView(StringConstant.ERROR_DONE, modelMap);
        }
		//加入日志
		Logs logs=new Logs(CodeHelper.createUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除采购合约:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return new ModelAndView(StringConstant.ACTION_DONE);
    }

    /**
     * 显示适用门店
     *
     * @param modelMap
     * @param suitstore
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/showStore")
    public ModelAndView showStore(ModelMap modelMap,HttpSession session,Suitstore suitstore) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        suitstore.setAcct(session.getAttribute("ChoiceAcct").toString());
        modelMap.put("stores", purcontractService.findSuitstore(suitstore));
        return new ModelAndView(PurcontractConstants.STORELIST, modelMap);
    }

    /**
     *  修改状态
     * @param modelMap
     * @param istate
     * @param oldistate
     * @param ids
     * @param versions
     * @param session
     * @param vbillno
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/updatePurState")
    @ResponseBody
    public String updatePurState(ModelMap modelMap, String istate,String oldistate,String ids,String versions,HttpSession session,String vbillno) throws CRUDException {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        if(ValueCheck.IsEmpty(istate)){
        	return "请输入状态值。";
        }
        String updateMsg = "";
        if("3".equals(istate)){
        	updateMsg = "生效";
        }else if("4".equals(istate)){
        	updateMsg = "终止";
        }else if("11".equals(istate)){
        	updateMsg = "取消生效，修改状态为待审核";
        	istate = "1";
        }else if("12".equals(istate)){
        	updateMsg = "取消终止，修改状态为待审核";
        	istate = "1";
        }else{
        	updateMsg = "更新状态";
        }
        String msg = purcontractService.updatePurState(istate, ids,versions,oldistate,session.getAttribute("accountId").toString(),session.getAttribute("accountName").toString());
        if (!"ok".equals(msg)) {
            return msg;
        }
		//加入日志
		Logs logs=new Logs(CodeHelper.createUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改采购合约状态:"+updateMsg+",合约编码："+vbillno,session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return "OK";
    }

    /**
     * 修改适用门店
     *
     * @param contractQuote
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/updateStore")
    @ResponseBody
    public String updateStore(Spprice spprice,HttpSession session, String ids) {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
        	spprice.setAcct(session.getAttribute("ChoiceAcct").toString());
            return purcontractService.updateStore(spprice, ids);
        } catch (CRUDException e) {
            return "error";
        }
    }

    /**
     * 文件上传
     *
     * @param pk_purcontract
     * @return
     */
    @RequestMapping(value = "/upload")
    @ResponseBody
    public String upload(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response, String pk_purcontract) {
        try {
            modelMap.put("file", purcontractService.upload(request, pk_purcontract));
            modelMap.put("msg", "ok");
            return JSONObject.fromObject(modelMap).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        modelMap.put("msg", "error");
        return JSONObject.fromObject(modelMap).toString();
    }

    /**
     * 附件下载
     *
     * @param request
     * @param response
     * @param transationatta
     */
    @RequestMapping("/download")
    public void download(HttpServletRequest request, HttpServletResponse response, String transationatta) {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            Transationatta tta = purcontractService.findTransationattaByPk(transationatta);
            String name = tta.getVattafile().substring(tta.getVattafile().lastIndexOf("/"));
            Util.download(request, response, name, "application/x-download", tta.getVattaname(), tta.getVattafile().substring(0, tta.getVattafile().lastIndexOf("/")));
        } catch (CRUDException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 附件删除
     * @param ids
     * @return
     */
    @RequestMapping("/deltta")
    @ResponseBody
    public String delTta(String ids) {
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        try {
            purcontractService.delTta(ids);
            return "ok";
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return "error";
    }

    /**
     * 获取报价明细
     * @param modelMap
     * @param session
     * @param purcontract
     * @param page
     * @return
     */
    @RequestMapping("/getContractQuote")
    public ModelAndView getContractQuoteByPur_id(ModelMap modelMap,HttpSession session,Purcontract purcontract,Page page){
        try {
        	if (purcontract != null && purcontract.getPk_purcontract() != null && !"".equals(purcontract.getPk_purcontract())) {
        		purcontract.setAcct(session.getAttribute("ChoiceAcct").toString());
        		modelMap.put("sppriceList", purcontractService.getContractQuoteByPur_id(purcontract, page));
			}
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        modelMap.put("page", page);
        modelMap.put("pk_purcontract", purcontract.getPk_purcontract());
        return new ModelAndView(PurcontractConstants.CONTRACTQUOTELIST,modelMap);
    }
	/**
	 * 打印
	 * @param modelMap
	 * @param vcodes
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/printPurcontract")
	public ModelAndView printPurcontract(ModelMap modelMap,HttpServletRequest request,HttpSession session,String vcodes,String type,Purcontract purcontract,String printFlag)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		purcontract.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		Purcontract purcontractd = purcontractService.findPurcontractByPk(purcontract);
		Map<String,Object> mapRes = MapBeanConvertUtil.convertBean(purcontractd);  
		Map<String,Object> mapRe  = new HashMap<String,Object>();
        for(Entry<String, Object> entry : mapRes.entrySet()){  
            String k=entry.getKey();  
            if(k.indexOf("pk_org.")<0 && k.indexOf("pk_supplier.")<0){
            	mapRe.put(k, entry.getValue());
            }
        }  
        mapRe.remove("pk_org");
        mapRe.remove("pk_supplier");
        mapRe.put("orgName", purcontractd.getPositncode().getDes());
        mapRe.put("supplierName", purcontractd.getDelivercode().getDes());
        mapRe.put("istateName", mapRes.get("istate")=="1"?"待审核":( mapRes.get("istate")=="3"?"生效":(mapRes.get("istate")=="4"?"终止":"待审核")));
		if("1".equals(printFlag)){
			List<Spprice> listContractQuote = purcontractd.getSppriceList();
			List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(listContractQuote);
			for(Map<String,Object> map : dataList){
				if("1".equals(map.get("ENABLESTATE"))){
					map.put("ENABLESTATE","待审核");
				}else if("2".equals(map.get("ENABLESTATE"))){
					map.put("ENABLESTATE","已审核");
				}else if("3".equals(map.get("ENABLESTATE"))){
					map.put("ENABLESTATE","停用");
				}else{
					map.put("ENABLESTATE","待审核");
				}
			}
			return PrintDataUtil.printDataMap(modelMap, request, session, mapRe, type, "采购合约-基本信息", dataList, PurcontractConstants.BASEIREPORT);
		}else if("2".equals(printFlag)){
			List<Contractclau> listContractclau = purcontractd.getContractclauList();
			return PrintDataUtil.printData(modelMap, request, session, mapRe, type, "采购合约-合约条款", listContractclau, PurcontractConstants.CLAUIREPORT);
		}else if("3".equals(printFlag)){
			List<Contracteven> listContracteven = purcontractd.getContractevenList();
			return PrintDataUtil.printData(modelMap, request, session, mapRe, type, "采购合约-合约大事记", listContracteven, PurcontractConstants.EVENTIERIREPORT);
		}else{
			List<Spprice> listContractQuote = purcontractd.getSppriceList();
			List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(listContractQuote);
			for(Map<String,Object> map : dataList){
				if("1".equals(map.get("ENABLESTATE"))){
					map.put("ENABLESTATE","待审核");
				}else if("2".equals(map.get("ENABLESTATE"))){
					map.put("ENABLESTATE","已审核");
				}else if("3".equals(map.get("ENABLESTATE"))){
					map.put("ENABLESTATE","停用");
				}else{
					map.put("ENABLESTATE","待审核");
				}
			}
			return PrintDataUtil.printDataMap(modelMap, request, session, mapRe, type, "采购合约-基本信息", dataList, PurcontractConstants.BASEIREPORT);
		}
	}
	/**
	 * 检测一个采购组织、一个供应商 采购合约是否时间重叠，重叠返回-1 不重叠返回1 
	 * @param modelMap
	 * @param session
	 * @param purcontract
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value="/checkPurcontract")
	@ResponseBody
	public String checkPurcontract(ModelMap modelMap, HttpSession session, Purcontract purcontract) throws CRUDException{
        DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
        String result = "-1";
        purcontract.setAcct(session.getAttribute("ChoiceAcct").toString());
        result = purcontractService.checkPurcontract(purcontract);
        return result;
		
	}
}
