package com.choice.assistant.web.controller.common;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choice.assistant.domain.common.CommonAssMethod;
import com.choice.assistant.service.common.CommonAssService;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Util;

/**
 * 后台公共调用方法
 * @author zgl
 * 2014年10月27日 
 */
@Controller
@RequestMapping(value = "commonAssMethod")
public class CommonAssController {
	
	@Autowired
	private CommonAssService commonAssService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 验证输入编码是否重复
	 * @author zgl
	 * 2014年10月27日 上午9:24:39
	 * @return
	 */
	@RequestMapping(value = "/validateVcode")
	@ResponseBody
	public String validateVcode(CommonAssMethod commonMethod,HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(ValueCheck.IsEmpty(session.getAttribute("ChoiceAcct"))){
			return null;
		}
		commonMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
		return commonAssService.validateVcode(commonMethod);
	}
	
	/**
	 * 获取排序列值
	 * @author zgl
	 * 2014年10月27日 下午2:39:41
	 * @param commonMethod
	 * @return
	 */
	@RequestMapping(value = "/getSortNo")
	@ResponseBody
	public String getSortNo(CommonAssMethod commonMethod,HttpSession session){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		commonMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
		return commonAssService.getSortNo(commonMethod);
	}
	/**
	 * 描述:设置是否启用
	 * 作者:zgl
	 * 日期:2014.10.27
	 * @param pk_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/IsEnable",method={org.springframework.web.bind.annotation.RequestMethod.POST})
	@ResponseBody
	public Object IsEnable(CommonAssMethod commonMethod,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String result = commonAssService.IsEnable(commonMethod);
		String state = commonMethod.getEnablestate().equals("2")?"启用":"停用";
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
		Logs logd=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,state+commonMethod.getModulename()+",编码【"+commonMethod.getVcode()+"】",session.getAttribute("ip").toString(),ProgramConstants.LOGIN);
		logsMapper.addLogs(logd);
		return result;
	}
	
	/** 
	 * 描述：验证排序重复
	 * @param commonMethod
	 * @return
	 * author:spt
	 * 日期：2014-10-27
	 */
	@RequestMapping(value = "/validateSortNo")
	@ResponseBody
	public Integer validateSortNo(CommonAssMethod commonMethod){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Integer retSortNo=commonAssService.validateSortNo(commonMethod);
		return retSortNo;
	} 
	
	/**
	 * 验证数据是否被引用,vtablename:引用的表，vcodename:引用的字段，vcode:被引用的值
	 * @author zgl
	 * 2014-10-27
	 * @return
	 */
	@RequestMapping(value = "/checkQuote")
	@ResponseBody
	public String checkQuote(CommonAssMethod commonMethod,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(ValueCheck.IsEmpty(session.getAttribute("ChoiceAcct"))){
			return null;
		}
		commonMethod.setAcct(session.getAttribute("ChoiceAcct").toString());
		return commonAssService.checkQuote(commonMethod);
	}
	//验证数据是否被引用    ----- vcodename字段中含有多个值的
	@RequestMapping(value = "/checkMultiCodeQuote")
	@ResponseBody
	public String checkMultiCodeQuote(CommonAssMethod commonMethod) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return commonAssService.checkMultiCodeQuote(commonMethod);
	}
}
