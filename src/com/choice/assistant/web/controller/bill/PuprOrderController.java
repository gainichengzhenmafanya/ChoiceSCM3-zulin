package com.choice.assistant.web.controller.bill;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.bill.PuprOrderConstants;
import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.assistant.domain.PurchaseSet.Inspectd;
import com.choice.assistant.domain.bill.PreOrder;
import com.choice.assistant.domain.bill.PreOrderd;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.bill.PuprOrderd;
import com.choice.assistant.domain.detailedlist.Purtempletd;
import com.choice.assistant.domain.detailedlist.Purtempletm;
import com.choice.assistant.service.bill.PreOrderService;
import com.choice.assistant.service.bill.PuprOrderService;
import com.choice.assistant.service.detailedlist.PurtempletService;
import com.choice.assistant.service.purchaseSet.InspectService;
import com.choice.assistant.service.system.ContracttermsService;
import com.choice.assistant.util.AsstUtils;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.ReadReportUrl;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.domain.Dis;
import com.choice.scm.domain.LinesFirm;
import com.choice.scm.domain.SupplyAcct;
import com.choice.scm.domain.Tax;
import com.choice.scm.service.AccountDisService;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.DisFenBoService;
import com.choice.scm.service.LinesFirmService;
import com.choice.scm.service.report.WzYueChaxunService;

/**
 * 订单管理
 * @author wangchao
 *
 */
@Controller
@RequestMapping("puprorder")
public class PuprOrderController {

	@Autowired
	private PuprOrderService puprOrderService;
	
	@Autowired
	private PurtempletService purtempletService;
	@Autowired
	private InspectService inspectService;
	@Autowired
	private LogsMapper logsMapper;
    @Autowired
    private ContracttermsService contracttermsService;
    @Autowired
    private CodeDesService codeDesService;
	private final static int PAGE_SIZE = 40;
	@Autowired
	private DisFenBoService disFenBoService;
	@Autowired
	private LinesFirmService linesFirmService;
	@Autowired
	private WzYueChaxunService wzYueChaxunService;
	@Autowired
	private AccountDisService accountDisService;
	@Autowired
	private PreOrderService preOrderService;
	
	/**
	 * 查询所有的订单信息
	 * @param modelMap
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllPuprOrders")
	public ModelAndView queryAllPuprOrders(ModelMap modelMap,PuprOrder puprOrder,HttpSession session,Page page,String from)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(!"tablemain".equals(from)){
			if (puprOrder.getBdate() == null || "".equals(puprOrder.getBdate())) {
				puprOrder.setBdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			}
			if (puprOrder.getEdate() == null || "".equals(puprOrder.getEdate())) {
				puprOrder.setEdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
			}
		}
		List<PuprOrder> puprorderList = puprOrderService.queryAllPuprOrders(puprOrder, page);
		modelMap.put("puprorder", puprOrder);
		String pk_puprorder = "";
		if (puprorderList.size()>0) {
			pk_puprorder = puprorderList.get(0).getPk_puprorder();
			modelMap.put("istate", puprorderList.get(0).getIstate());
		}
		modelMap.put("puprorderList", puprorderList);
		modelMap.put("pk_puprorder", pk_puprorder);
		//是否可以提交他人订单到商城 Y-可以 N不可以
		modelMap.put("vsubmitothorder", ValueCheck.IsEmpty(session.getAttribute("vsubmitothorder"))==true?"N":session.getAttribute("vsubmitothorder"));
		modelMap.put("pageobj", page);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,"查询订单",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(PuprOrderConstants.TABLE_PUPRORDER,modelMap);
	}
	
	/**
	 * 根据主表信息查询子表信息
	 * @param modelMap
	 * @param puprOrderd
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllPuprOrderds")
	public ModelAndView queryAllPuprOrderds(ModelMap modelMap,String pk_puprorder,HttpSession session,String istate)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PuprOrder puprOrder = new PuprOrder();
		puprOrder.setPk_puprorder(pk_puprorder);
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		//采购订单子表
		PuprOrderd puprOrderd = new PuprOrderd();
		puprOrderd.setPk_puprorder(pk_puprorder);
		puprOrderd.setAcct(session.getAttribute("ChoiceAcct").toString());
		
		
		List<PuprOrderd> puprOrderdList = new ArrayList<PuprOrderd>();
		if (pk_puprorder == null || "".equals(pk_puprorder)) {
			puprOrderdList = null;
		} else {
			puprOrderdList = puprOrderService.queryAllPuprOrderdsByPK(puprOrderd);
		}
		double summoney = 0.0;
		if (puprOrderdList != null) {
			for (PuprOrderd puprorderd : puprOrderdList) {
				summoney += puprorderd.getNmoney();
			}
			modelMap.put("length", puprOrderdList.size());
		} else {
			modelMap.put("length", 0);
		}
		//发货单明细
		if("3".equals(istate) || "4".equals(istate) || "5".equals(istate) || "6".equals(istate) || "7".equals(istate) ){
			modelMap.put("fahuoList", puprOrderdList);
		}
		//验货单明细
		Inspectd inspectd = new Inspectd();
		inspectd.setAcct(session.getAttribute("ChoiceAcct").toString());
		inspectd.setPk_puprorder(pk_puprorder);
		List<Inspectd> listInspectd = inspectService.queryInspectdByPuprorder(inspectd);
		double inspnmoney = 0.0;//验货金额
		if (listInspectd != null) {
			for (int i = 0; i < listInspectd.size(); i++) {
				inspnmoney += listInspectd.get(i).getNmoney();
			}
			modelMap.put("lengthinsp", listInspectd.size());
		} else {
			modelMap.put("lengthinsp", 0);
		}
		modelMap.put("inspnmoney", inspnmoney);
		//物流信息
		modelMap.put("listInspectd", listInspectd);
		modelMap.put("puprOrderdList", puprOrderdList);
		modelMap.put("summoney", summoney);
		modelMap.put("pk_puprorder", pk_puprorder);
		
		if(null!=pk_puprorder && !"".equals(pk_puprorder)){
			PuprOrder evalp = puprOrderService.findPuprorderByCode(puprOrder);
			modelMap.put("puprorder", evalp);
		}else{
			modelMap.put("puprorder", new PuprOrder());
		}
		
		
		modelMap.put("istate", istate);
		return new ModelAndView(PuprOrderConstants.TABLE_PUPRORDERD,modelMap);
	}
	
	/**
	 * 跳转到新增订单页面
	 * @param modelMap
	 * @param session
	 * @param billType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddPuprorder")
	public ModelAndView toAddPuprorder(ModelMap modelMap,HttpSession session,String billType,int sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PuprOrder puprOrder = new PuprOrder();
		puprOrder.setPk_account(session.getAttribute("accountId").toString());
		puprOrder.setAccountname(session.getAttribute("accountName").toString());
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		puprOrder.setVbillno(puprOrderService.getNextCGVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
//		puprOrder.setVbillno(coderuleService.getCode(CoderuleConstants.PURTEMPLET));
		String nowDate = DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");
		puprOrder.setDbilldate(nowDate);
		puprOrder.setDmaketime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		String dhopedate = DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", 1, 1),"yyyy-MM-dd");
		puprOrder.setDhopedate(dhopedate);
		puprOrder.setIbillfrom(3);
		puprOrder.setIstate(sta);
		puprOrder.setVpurchaser("");
		modelMap.put("puprOrder", puprOrder);
		List<Tax> taxList = codeDesService.findTaxRateList(new Tax(),new Page());
		modelMap.put("taxList", JSONArray.fromObject(taxList).toString());
		//订单补录
		if(sta==100){
			return new ModelAndView(PuprOrderConstants.TOADDPUPRORDERBULU,modelMap);
		}else{
			return new ModelAndView(PuprOrderConstants.TOADDPUPRORDER,modelMap);
		}
	}
	
	/**
	 * 校验单据号是否重复
	 * @param session
	 * @param vbillno
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findPuprorderByCode")
	@ResponseBody
	public boolean findPuprorderByCode(HttpSession session,String vbillno)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PuprOrder puprOrder = new PuprOrder();
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		puprOrder.setVbillno(vbillno);
		return null==puprOrderService.findPuprorderByCode(puprOrder)?true:false;
	}
	
	/**
	 * 添加采购订单
	 * @param modelMap
	 * @param session
	 * @param puprOrder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addPuprorder")
	@ResponseBody
	public String addPuprorder(ModelMap modelMap,HttpSession session,PuprOrder puprOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setPk_account(session.getAttribute("accountId").toString());
		puprOrder.setAccountname(session.getAttribute("accountName").toString());
		puprOrder.setVbillno(puprOrderService.getNextCGVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
		String res = puprOrderService.savePuprOrder(puprOrder,session.getAttribute("ChoiceAcct").toString());
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增订单，订单号：:"+puprOrder.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 跳转到预订单生成采购订单
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddPreorderList")
	public ModelAndView toAddPreorderList(ModelMap modelMap,HttpSession session,PreOrder preOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		preOrder.setAccountId(session.getAttribute("accountName").toString());
		//未生成的
		preOrder.setDtype("1");
		//必须选择采购组织才行
		if(preOrder.getPositncode()!=null){
			List<PreOrderd> list = preOrderService.queryAllPreOrderdsDeliver(preOrder);
			modelMap.put("preOrderds", list);
		}
		SimpleDateFormat sif = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		preOrder.setDdate(sif.format(cal.getTime()));
		preOrder.setBdate(sif.format(date));
		//值
		PuprOrder puprOrder = new PuprOrder();
		puprOrder.setPk_account(session.getAttribute("accountId").toString());
		puprOrder.setAccountname(session.getAttribute("accountName").toString());
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		String nowDate = DateFormat.getStringByDate(date, "yyyy-MM-dd");
		puprOrder.setDbilldate(nowDate);
		puprOrder.setDmaketime(DateFormat.getStringByDate(date, "yyyy-MM-dd HH:mm:ss"));
		
		//税率
		List<Tax> taxList = codeDesService.findTaxRateList(new Tax(),new Page());
		modelMap.put("taxListObj", taxList);
		modelMap.put("preOrder", preOrder);
		modelMap.put("puprOrder", puprOrder);
		return new ModelAndView(PuprOrderConstants.TOSAVEPREORDERDLIST,modelMap);
	}
	
	/**
	 * 添加采购订单2
	 * @param modelMap
	 * @param session
	 * @param puprOrder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addPuprorder2")
	@ResponseBody
	public String addPuprorder2(ModelMap modelMap,HttpSession session,PuprOrder puprOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setPk_account(session.getAttribute("accountId").toString());
		puprOrder.setAccountname(session.getAttribute("accountName").toString());
		puprOrder.setVbillno(puprOrderService.getNextCGVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
		puprOrder.setIstate(1);
		String res = puprOrderService.savePuprOrder2(puprOrder,session.getAttribute("ChoiceAcct").toString());
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增订单，订单号：:"+puprOrder.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 添加采购订单来自报货分拨
	 * @throws Exception
	 */
	@RequestMapping(value = "selectDisfenbo")
	public ModelAndView list(ModelMap modelMap,HttpSession session,Dis dis,String action,String ind1,String chectim1) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Logs logd = new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,
				"报货分拨查询",session.getAttribute("ip").toString(),ProgramConstants.SCM);
		logsMapper.addLogs(logd);
		String accountName=session.getAttribute("accountName").toString();
		if(null!=action && "init".equals(action)){//页面初次加载时
			dis.setMaded(new Date());
			modelMap.put("action", "init");
			dis.setYndo("NO");
			
		}else{
			if(null!=ind1 && !"".equals(ind1)){
				dis.setInd(dis.getMaded());
				dis.setMaded(null);
			}
			
			dis.setStartNum(0);
			dis.setEndNum(PAGE_SIZE);
			dis.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
			dis.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//新增的物资账号权限 判断是关联物资还是关联小类 wjf
			dis.setFxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FXQX"));
			dis.setGysqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "GYSQX"));
			dis.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));
			dis.setAccountId(session.getAttribute("accountId").toString());
			dis.setFbcxqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "FBCXQX"));
			dis.setInout("out");
			List<Dis> disesList1 = disFenBoService.findAllDisPage_cg(dis);
			int totalCount = disFenBoService.findAllDisCount(dis);//查总数量  应该是放上所有条件再查  不然和列表里对不起来  2014.11.19wjf
			//查询库存量--物资余额表数据
			SupplyAcct supplyAcct = new SupplyAcct();
			supplyAcct.setAcct(session.getAttribute("ChoiceAcct").toString());
			for(Dis d:disesList1){
				supplyAcct.setPositn(d.getPositnCode());
				supplyAcct.setSp_code(d.getSp_code());
				d.setCnt(wzYueChaxunService.findSupplyBalanceOnlyEndNum(supplyAcct));
				//判断是否生成采购订单是否为空   默认为N
				if (d.getIspuprorder() == null || "".equals(d.getIspuprorder())) {
					d.setIspuprorder("N");
				}
			}
			modelMap.put("disesList1", disesList1);//前20条数据
			dis.setAccountId(session.getAttribute("accountId").toString());
			modelMap.put("totalCount", totalCount);
			
			if(totalCount<=PAGE_SIZE){
				modelMap.put("currState", 1);
			}else{
				modelMap.put("currState", PAGE_SIZE*1.0/totalCount);//总页数
			}
			
			if(null!=ind1 && !"".equals(ind1)){
				dis.setMaded(dis.getInd());
			}
		}
		
		
		//配送
		modelMap.put("linesFirmList", linesFirmService.findLinesFirm(new LinesFirm()));//配送片区
		modelMap.put("codeDesList", codeDesService.findCodeDes(String.valueOf(CodeDes.S_PS_AREA)));//配送片区
		modelMap.put("accountName", accountName);
		modelMap.put("ind1", ind1);
		modelMap.put("chectim1", chectim1);
		modelMap.put("dis", dis);
		modelMap.put("currPage", 1);
		modelMap.put("pageSize", PAGE_SIZE);
		modelMap.put("disJson", JSONObject.fromObject(dis));
		String ZHGLTSQX = ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "ZHGLTSQX");
		modelMap.put("ZHGLTSQX", ZHGLTSQX);//加判断，是否加特殊权限判断，洞庭需要在报货分拨页面加价格修改限制 wjf 
		if("1".equals(ZHGLTSQX)){//如果是1，则去查询权限表
			modelMap.put("accountDis", accountDisService.getAccountDisByAccountId(session.getAttribute("accountId").toString()));//此账户是否有特殊权限 wjf
		}
		return new ModelAndView(PuprOrderConstants.DISFENBO, modelMap);
	}
	
	/**
	 * 跳转到修改采购订单的页面
	 * @param modelMap
	 * @param session
	 * @param pk_puprorder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdatePuprorder")
	public ModelAndView toUpdatePuprorder(ModelMap modelMap,HttpSession session,String pk_puprorder,int istate)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PuprOrder puprOrder = new PuprOrder();
		puprOrder.setPk_puprorder(pk_puprorder);
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		PuprOrder puprorder = puprOrderService.toUpdatePuprorder(puprOrder);
		puprorder.setModifier(session.getAttribute("accountName").toString());
		puprorder.setModifedtime(DateFormat.getTs());
		if(ValueCheck.IsEmpty(puprorder.getAccountname())){
			puprorder.setAccountname("");
		}else{
			puprorder.setAccountname(puprorder.getAccountname().trim());
		}
		puprorder.setIstate(istate);
		modelMap.put("puprOrder", puprorder);//主表信息
		PuprOrderd puprOrderd = new PuprOrderd();
		puprOrderd.setPk_puprorder(pk_puprorder);
		puprOrderd.setAcct(session.getAttribute("ChoiceAcct").toString());//子表信息
		modelMap.put("listPuprOrderd", puprOrderService.queryAllPuprOrderdsByPK(puprOrderd));
		//税率
		List<Tax> taxList = codeDesService.findTaxRateList(new Tax(),new Page());
		modelMap.put("taxListObj", taxList);
		modelMap.put("taxList", JSONArray.fromObject(taxList).toString());
		//订单补录
		if(istate==100){
			return new ModelAndView(PuprOrderConstants.TOUPDATEPUPRORDERBULU,modelMap);
		}else{
			return new ModelAndView(PuprOrderConstants.TOUPDATEPUPRORDER,modelMap);
		}
	}
	
	/**
	 * 修改采购订单信息
	 * @param modelMap
	 * @param session
	 * @param puprOrder
	 * @param sta
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updatePuprorder")
	@ResponseBody
	public String updatePuprorder(ModelMap modelMap,HttpSession session,PuprOrder puprOrder,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		puprOrder.setModifier(session.getAttribute("accountName").toString());
		for (int i = 0; i < puprOrder.getPuprOrderdList().size(); i++) {
			puprOrder.getPuprOrderdList().get(i).setAcct(session.getAttribute("ChoiceAcct").toString());
		}
		String ret = puprOrderService.updatePuprOrderm(puprOrder);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改订单。订单号：:"+puprOrder.getVbillno(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return ret;
	}
	
	/**
	 * 删除订单信息
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deletePuprorder")
	@ResponseBody
	public String deletePuprorder(ModelMap modelMap,HttpSession session,String pks)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PuprOrder puprOrder = new PuprOrder();
		List<String> ids = Arrays.asList(pks.split(","));
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());

    	String names = "";
    	Map<String,Object> paramMap = new HashMap<String,Object>();
    	paramMap.put("colName","vbillno");
    	paramMap.put("tableName","cscm_puprorder_m");
    	paramMap.put("acct",session.getAttribute("ChoiceAcct"));
    	paramMap.put("pkCol","pk_puprorder");
    	paramMap.put("pk_id",AsstUtils.StringCodeReplace(pks));
    	List<Map<String,Object>> listMap = contracttermsService.queryInfoList(paramMap);
    	for(Map<String,Object> map : listMap){
    		names += ","+map.get("vbillno");
    	}
		puprOrder.setPklsit(ids);
		puprOrderService.deletePuprorder(puprOrder);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除订单。订单号："+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return "OK";
	}
	
	/**
	 * 采购清单生成采购订单
	 * @param modelMap
	 * @param session
	 * @param pk_puprorder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toSavePuprorder")
	public ModelAndView toSavePuprorder(ModelMap modelMap,HttpSession session,String pks,String sta,PuprOrder puprOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PuprOrder puprorder = new PuprOrder();
		puprorder.setPk_account(session.getAttribute("accountId").toString());
		puprorder.setAccountname(session.getAttribute("accountName").toString());
		puprorder.setPositncode(puprOrder.getPositncode());
		puprorder.setPositnname(puprOrder.getPositnname());
		
		puprorder.setDbilldate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		puprorder.setDmaketime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		String nowDate = DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");
		puprorder.setDbilldate(nowDate);
		puprorder.setDmaketime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		String dhopedate = DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", 1, 1),"yyyy-MM-dd");
		puprorder.setDhopedate(dhopedate);
		puprorder.setIbillfrom(3);
		if (puprOrder.getDelivername() != "" && puprOrder.getDelivername() != null) {
			puprorder.setDelivername(puprOrder.getDelivername());
		}
		if (puprOrder.getPositncode() != "" && puprOrder.getPositncode() != null) {
			puprorder.setPositncode(puprOrder.getPositncode());
		}
		puprOrder.setVpurchaser("");
		modelMap.put("puprOrder", puprorder);
		Purtempletd purtempletd = new Purtempletd();
		List<String> ids = Arrays.asList(pks.split(","));
		purtempletd.setPkdlist(ids);
		purtempletd.setAcct(session.getAttribute("ChoiceAcct").toString());
		purtempletd.setDelivername(puprOrder.getDelivername());
		purtempletd.setPositncode(puprOrder.getPositncode());
		//账号物资类别权限
		String pk_accountid = null;
		String accountName = session.getAttribute("accountName").toString();
		if(!accountName.equals("admin")){
			pk_accountid = session.getAttribute("accountId").toString();
		}
		purtempletd.setPk_accountid(pk_accountid);
		
		List<Purtempletd> listPurtempletd = purtempletService.queryAllPurtempletd(purtempletd);
		modelMap.put("listPurtempletd", listPurtempletd);
		List<Purtempletd> listPurtempletds = purtempletService.queryPurtempletdBySupplier(purtempletd);
		String listPk_supplier = "";//主键
		String listSupplierCode = "";//编码
		String listSupplierName = "";//名称
		if (listPurtempletds.size() == 0) {
			listPk_supplier += puprorder.getPositncode() + ",";
			listSupplierCode += puprorder.getDelivercode() + ",";
			listSupplierName += puprorder.getDelivername() + ",";
		} else {
			for (int i = 0; i < listPurtempletds.size(); i++) {
				listPk_supplier += listPurtempletds.get(i).getPositncode() + ",";
				listSupplierCode += listPurtempletds.get(i).getDelivercode() + ",";
				listSupplierName += listPurtempletds.get(i).getDelivername() + ",";
			}
		}
		modelMap.put("listPk_supplier", listPk_supplier.subSequence(0, listPk_supplier.length()-1));
		modelMap.put("listSupplierCode", listSupplierCode.subSequence(0, listSupplierCode.length() - 1));
		modelMap.put("listSupplierName", listSupplierName.subSequence(0, listSupplierName.length() - 1));
		modelMap.put("pks", pks);
		return new ModelAndView(PuprOrderConstants.TOSAVEPUPRORDERD,modelMap);
	}
	
	/**
	 * 跳转到清单生成采购订单
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddPuprorderList")
	public ModelAndView toAddPuprorderList(ModelMap modelMap,HttpSession session,int sta,PuprOrder puprOrder,String purtempletid)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setPk_account(session.getAttribute("accountId").toString());
		puprOrder.setAccountname(session.getAttribute("accountName").toString());
		puprOrder.setVbillno(puprOrderService.getNextCGVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
		String nowDate = DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");
		puprOrder.setDbilldate(nowDate);
		puprOrder.setDmaketime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		String dhopedate = DateFormat.getStringByDate(DateFormat.getDateBefore(new Date(), "day", 1, 1),"yyyy-MM-dd");
		puprOrder.setDhopedate(dhopedate);
		puprOrder.setIbillfrom(3);
		puprOrder.setIstate(sta);
		if (purtempletid != null && purtempletid != "") {
			puprOrder.setPk_purtemplet(purtempletid);
		}
		puprOrder.setVpurchaser("");
		modelMap.put("puprOrder", puprOrder);
		modelMap.put("sta", sta);
		List<Purtempletd> listPurtempletd = new ArrayList<Purtempletd>();
		if (puprOrder.getPk_purtemplet() != null && puprOrder.getPk_purtemplet() != "") {
			Purtempletd purtempletd = new Purtempletd();
			purtempletd.setPositncode(puprOrder.getPositncode());
			purtempletd.setDelivercode(puprOrder.getDelivercode());
			purtempletd.setPk_purtemplet(puprOrder.getPk_purtemplet());
			purtempletd.setAcct(session.getAttribute("ChoiceAcct").toString());
			//账号物资类别权限
			String pk_accountid = null;
			String accountName = session.getAttribute("accountName").toString();
			if(!accountName.equals("admin")){
				pk_accountid = session.getAttribute("accountId").toString();
			}
			purtempletd.setPk_accountid(pk_accountid);
			//物资暂时不走权限控制js20161216
			purtempletd.setPk_accountid(null);
			purtempletd.setPositncode(null);
			listPurtempletd = purtempletService.queryAllPurtempletd(purtempletd);
		}
		modelMap.put("listPurtempletd", listPurtempletd);
		Purtempletm purtempletm = new Purtempletm();
		purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Purtempletm> listPurtempletm = purtempletService.getPurtempletmName(purtempletm);
		modelMap.put("listPurtempletm", listPurtempletm);
		modelMap.put("purtempletid", purtempletid);
		//税率
		List<Tax> taxList = codeDesService.findTaxRateList(new Tax(),new Page());
		modelMap.put("taxListObj", taxList);
		modelMap.put("taxList", JSONArray.fromObject(taxList).toString());
		return new ModelAndView(PuprOrderConstants.TOSAVEPUPRORDERDLIST,modelMap);
	}
	
	/**
	 * 查询所有的订单信息====用于弹出选择的界面
	 * @param modelMap
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/selectAllPuprOrders")
	public ModelAndView selectAllPuprOrders(ModelMap modelMap,PuprOrder puprOrder,HttpSession session,
			Page page,String callBack,String delivercode,String single,String domId)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		puprOrder.setDelivercode(delivercode);
		List<PuprOrder> puprorderList = puprOrderService.queryAllPuprOrders(puprOrder, page);
		String pk_puprorder = "";
		if (puprorderList.size()>0) {
			pk_puprorder = puprorderList.get(0).getPk_puprorder();
		}
		modelMap.put("puprorderList", puprorderList);
		modelMap.put("pk_puprorder", pk_puprorder);
		modelMap.put("istate", puprOrder.getIstate());
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		modelMap.put("single", single);
		modelMap.put("domId", domId);
		modelMap.put("puprorder", puprOrder);
		modelMap.put("delivercode", delivercode);
		return new ModelAndView(PuprOrderConstants.SELECTPUPRORDERDLIST,modelMap);
	}
	
	/**
	 * 保存评价信息
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saveEvaluate")
	@ResponseBody
	public String saveEvaluate(HttpSession session,PuprOrder puprOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		puprOrderService.saveEvaluate(puprOrder);
		return "OK";
	}
	
	
	/**
	 * 打印订单
	 * @param modelMap
	 * @param session
	 * @param pk_chksto
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/printPuprorder")
	public ModelAndView printPuprorder(ModelMap modelMap,HttpSession session,HttpServletRequest request,String type,PuprOrder puprOrder,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		page.setPageSize(Integer.MAX_VALUE);
		PuprOrder result = puprOrderService.queryAllPuprOrders(puprOrder, page).get(0);
		Map<String,Object> parameter = MapBeanConvertUtil.convertBean(result);
		PuprOrderd puprOrderd = new PuprOrderd();
		puprOrderd.setPk_puprorder(puprOrder.getPk_puprorder());
		List<PuprOrderd> puprOrderdList = puprOrderService.queryAllPuprOrderdsByPK(puprOrderd);
		
		List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(puprOrderdList);
		int istate = Integer.parseInt(String.valueOf(parameter.get("istate")));
		switch(istate){
			case 1 :parameter.put("vstate", "未审核");break;
			case 4 :parameter.put("vstate", "已审核");break;
			case 6 :parameter.put("vstate", "已入库");break;
			case 7 :parameter.put("vstate", "已结算");break;
//			case 1 :parameter.put("vstate", "未提交");break;
//			case 2 :parameter.put("vstate", "待发货");break;
//			case 3 :parameter.put("vstate", "待收货(已发货)");break;
//			case 4 :parameter.put("vstate", "已签收");break;
//			case 5 :parameter.put("vstate", "已签收 (返修/退换货中)");break;
//			case 6 :parameter.put("vstate", "已入库");break;
//			case 7 :parameter.put("vstate", "已完成");break;
//			case 8 :parameter.put("vstate", "待收货(待备货)");break;
//			case 9 :parameter.put("vstate", "待收货(备货中)");break;
//			case 10 :parameter.put("vstate", "待收货(备货完成)");break;
//			case -1 :parameter.put("vstate", "已撤销");break;
		}
		parameter.put("totalCount", ""+puprOrderdList.size());
        parameter.put("pk_puprorder",puprOrder.getPk_puprorder());
		//设置分页，查询所有数据
		modelMap.put("actionMap", parameter);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
	    parameter.put("report_name", "采购订单-基本信息");
	    modelMap.put("report_name", "采购订单-基本信息");//导出excel、pdf用到了
        modelMap.put("parameters", parameter);//ireport表头参数
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",dataList);
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,"/report/assistant/bill/PrintPuprorder.jasper");//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
	
	/**
	 * 检测选择的订单是不是当前登录用户新建的
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/checkOrderIsMine")
	@ResponseBody
	public String checkOrderIsMine(HttpSession session,PuprOrder puprOrder)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		puprOrder.setPk_account(session.getAttribute("accountId").toString());
		String str = "ok";
		str = puprOrderService.checkOrderIsMine(puprOrder);
		return str;
	}
	/**
	 * 更新订单状态
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updatePuprOrdersState")
	@ResponseBody
	public String updatePuprOrdersState(HttpSession session,PuprOrder puprOrder)throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		puprOrder.setTs(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDDHHMMSS));
		puprOrder.setIstate(4);
		//返回结果
		String returnStr = "OK";
		Map<String,String> map = new HashMap<String,String>();
		map.put("creator", session.getAttribute("accountName").toString());
		map.put("acct", session.getAttribute("ChoiceAcct").toString());
		//更新状态
		try{
			puprOrderService.updatePuprOrdersState(puprOrder);
		}catch(Exception e){
			e.printStackTrace();
			returnStr = "FAIL";
		}
		//判断上一步是否成功
		if("OK".equals(returnStr)){
			try{
			//生成验货单
			puprOrderService.createInspectOrder(puprOrder,map);
			}catch(Exception e){
				e.printStackTrace();
				returnStr = "FAIL";
			}
		}
		//返回
		return returnStr;
	}
	
	/**
	 * 根据单号查询订单的状态
	 * @param puprOrder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryPuprorderIstate")
	@ResponseBody
	public int queryPuprorderIstate(ModelMap modelMap,PuprOrder puprOrder,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		puprOrder.setAcct(acct);
		int istate = 1;
		//查询
		try{
			puprOrder = puprOrderService.queryAllPuprOrders(puprOrder,new Page()).get(0);
			istate = puprOrder.getIstate();
		}catch(Exception e){
			e.printStackTrace();
		}
		return istate;
	}
	

	/**
	 * 来自报货分拨生成采购订单
	 * @param puprOrder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updateDisfenbo")
	@ResponseBody
	public String updateDisfenbo(ModelMap modelMap,HttpSession session,PuprOrder puprorder) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String group = session.getAttribute("ChoiceAcct").toString();
		puprorder.setPk_account(session.getAttribute("accountId").toString());
		puprorder.setAccountname(session.getAttribute("accountName").toString());
		puprorder.setVbillno(puprOrderService.getNextCGVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
		String nowDate = DateFormat.getStringByDate(new Date(), "yyyy-MM-dd");
		puprorder.setDbilldate(nowDate);
		puprorder.setDmaketime(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
		puprorder.setIbillfrom(3);
		puprorder.setIstate(1);
		puprorder.setVpurchaser("");
		puprorder.setDhopedate(puprorder.getPuprOrderdList().get(0).getDhopedate());
		Double mon = 0.00;
		if(null==puprorder.getDelivercode() ||""==puprorder.getDelivercode()){
			puprorder.setDelivercode(puprorder.getPuprOrderdList().get(0).getDelivercode());
		}
		if(null==puprorder.getDelivername() || ""==puprorder.getDelivername()){
			puprorder.setDelivername(puprorder.getPuprOrderdList().get(0).getDelivername());
		}
		if(null==puprorder.getPositncode() || ""==puprorder.getPositncode()){
			puprorder.setPositncode(puprorder.getPuprOrderdList().get(0).getPositncode());
		}
		if(null==puprorder.getPositnname() || ""==puprorder.getPositnname()){
			puprorder.setPositnname(puprorder.getPuprOrderdList().get(0).getPositnname());
		}
		for(PuprOrderd p:puprorder.getPuprOrderdList()){
			p.setVbillno(puprOrderService.getNextCGVbillno(new Date(),session.getAttribute("ChoiceAcct").toString()));
			p.setAcct(group);
			mon+=p.getNmoney();
		}
		puprorder.setNmoney(mon);
		
		String a =puprOrderService.savePuprOrder(puprorder, group);
		return a;
	}
}
