package com.choice.assistant.web.controller.bill;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.bill.PreOrderConstants;
import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.assistant.domain.bill.PreOrder;
import com.choice.assistant.domain.bill.PreOrderd;
import com.choice.assistant.service.bill.PreOrderService;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

/**
 * 预订单管理
 * @author jinshuai
 *
 */
@Controller
@RequestMapping("preorder")
public class PreOrderController {
	
	@Autowired
	private PreOrderService preOrderService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private AccountPositnService accountPositnService;
	private SimpleDateFormat sif1 = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat sif2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 查询所有的预订单信息
	 * @param modelMap
	 * @param preOrder
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/list")
	public ModelAndView list(ModelMap modelMap,PreOrder preOrder,HttpSession session,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		preOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		AccountPositn accountPositn=accountPositnService.findAccountById(session.getAttribute("accountId").toString());
	/*	String positn = accountPositn == null?"":accountPositn.getPositn() == null?"":accountPositn.getPositn().getCode();
		preOrder.setPositncode(positn);*/
		preOrder.setAccountId(session.getAttribute("accountId").toString());//账号 权限用2014.12.22wjf
		preOrder.setCwqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "CWQX"));//是否启用仓位权限wjf
		//时间默认值
		if(preOrder.getBdate()==null&&preOrder.getEdate()==null){
			String dateStr = sif1.format(new Date());
			preOrder.setBdate(dateStr);
			preOrder.setEdate(dateStr);
		}
		List<PreOrder> preOrders = preOrderService.queryAllPreOrders(preOrder, page);
		int pk_preorder = 0;
		if (preOrders.size()>0) {
			pk_preorder = preOrders.get(0).getPk_preorder();
		}
		modelMap.put("preOrder", preOrder);
		modelMap.put("preOrders", preOrders);
		modelMap.put("pk_preorder", pk_preorder);
		modelMap.put("pageobj", page);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,"查询预订单",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(PreOrderConstants.LIST,modelMap);
	}
	
	/**
	 * 查询所有的预订单信息，用于生产采购订单
	 * @param modelMap
	 * @param preOrder
	 * @param session
	 * @param page
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/listToPuprorder")
	public ModelAndView listToPuprorder(ModelMap modelMap,PreOrder preOrder,HttpSession session,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		preOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		preOrder.setDtype("1");
		List<PreOrder> preOrders = preOrderService.queryAllPreOrders(preOrder, page);
		modelMap.put("preOrder", preOrder);
		modelMap.put("preOrders", preOrders);
		modelMap.put("pageobj", page);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,"查询预订单",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(PreOrderConstants.LIST2,modelMap);
	}
	
	/**
	 * 根据主表信息查询子表信息
	 * @param modelMap
	 * @param pk_preorder
	 * @param session
	 * @param dtype
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllPreOrderds")
	public ModelAndView queryAllPreOrderds(ModelMap modelMap,int pk_preorder,HttpSession session,String dtype)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		PreOrder preOrder = new PreOrder();
		preOrder.setPk_preorder(pk_preorder);
		preOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		//查询子表信息
		List<PreOrderd> preOrderds = new ArrayList<PreOrderd>();
		if(preOrder.getPk_preorder()!=0){
			preOrderds = preOrderService.queryAllPreOrderds(preOrder);
		}
		//查询字表信息
		modelMap.put("preOrderds", preOrderds);
		modelMap.put("dtype", dtype);
		return new ModelAndView(PreOrderConstants.LIST_PREORDERD,modelMap);
	}
	
	/**
	 * 跳转到新增预订单页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddPreorder")
	public ModelAndView toAddPreorder(ModelMap modelMap,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Date date = new Date();
		String ddate = sif1.format(date);
		String dtime = sif2.format(date);
		PreOrder preOrder = new PreOrder();
		preOrder.setDdate(ddate);
		preOrder.setDtime(dtime);
		preOrder.setAccountId(session.getAttribute("accountId").toString());
		preOrder.setAccountName(session.getAttribute("accountName").toString());
		modelMap.put("preOrder", preOrder);
		return new ModelAndView(PreOrderConstants.ADD,modelMap);
	}
	
	/**
	 * 跳转到修改预订单页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdatePreorder")
	public ModelAndView toUpdatePreorder(ModelMap modelMap,HttpSession session,PreOrder preOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		preOrder.setAccountId(session.getAttribute("accountId").toString());
		//得到预订单信息
		List<PreOrder> preOrders = preOrderService.queryAllPreOrders(preOrder, new Page());
		if(preOrders!=null&&preOrders.size()>0){
			preOrder = preOrders.get(0);
		}
		//子表
		preOrder.setPreOrderds(preOrderService.queryAllPreOrderds(preOrder));
		modelMap.put("preOrder", preOrder);
		return new ModelAndView(PreOrderConstants.UPDATE,modelMap);
	}
	
	/**
	 * 添加预订单
	 * @param modelMap
	 * @param session
	 * @param preOrder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addPreorder")
	@ResponseBody
	public String addPreorder(ModelMap modelMap,HttpSession session,PreOrder preOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountPositn accountPositn=accountPositnService.findAccountById(session.getAttribute("accountId").toString());
/*		String positncode = accountPositn == null?"":accountPositn.getPositn() == null?"":accountPositn.getPositn().getCode();
		String positndes = accountPositn == null?"":accountPositn.getPositn() == null?"":accountPositn.getPositn().getDes();
		preOrder.setPositncode(positncode);
		preOrder.setPositnname(positndes);*/
		preOrder.setAccountId(session.getAttribute("accountId").toString());
		preOrder.setAccountName(session.getAttribute("accountName").toString());
		preOrder.setDtype("1");
		String acct = session.getAttribute("ChoiceAcct").toString();
		preOrder.setAcct(acct);
		String res = preOrderService.savePreOrder(preOrder);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增预订单，订单号：:"+preOrder.getPk_preorder(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 修改预订单
	 * @param modelMap
	 * @param session
	 * @param preOrder
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/updatePreorder")
	@ResponseBody
	public String updatePreorder(ModelMap modelMap,HttpSession session,PreOrder preOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		AccountPositn accountPositn=accountPositnService.findAccountById(session.getAttribute("accountId").toString());
		String positncode = accountPositn == null?"":accountPositn.getPositn() == null?"":accountPositn.getPositn().getCode();
		String positndes = accountPositn == null?"":accountPositn.getPositn() == null?"":accountPositn.getPositn().getDes();
		preOrder.setPositncode(positncode);
		preOrder.setPositnname(positndes);
		preOrder.setAccountId(session.getAttribute("accountId").toString());
		preOrder.setAccountName(session.getAttribute("accountName").toString());
		preOrder.setDtype("1");
		String acct = session.getAttribute("ChoiceAcct").toString();
		preOrder.setAcct(acct);
		String res = preOrderService.savePreOrder2(preOrder);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"修改预订单，订单号：:"+preOrder.getPk_preorder(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 删除订单信息
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deletePreorder")
	@ResponseBody
	public String deletePreorder(ModelMap modelMap,HttpSession session,PreOrder preOrder)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		preOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
		preOrderService.delete(preOrder);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除预订单。订单号："+preOrder.getPk_preorder(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return "OK";
	}
	
}
