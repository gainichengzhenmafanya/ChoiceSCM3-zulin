package com.choice.assistant.web.controller.settlement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.settlement.ProsettlementConstants;
import com.choice.assistant.constants.system.CoderuleConstants;
import com.choice.assistant.domain.settlement.ProBill;
import com.choice.assistant.domain.settlement.ProPayment;
import com.choice.assistant.domain.settlement.Prosettlement;
import com.choice.assistant.service.settlement.ProsettlementService;
import com.choice.assistant.service.system.CoderuleService;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.ReadReportUrl;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Deliver;
import com.choice.scm.service.DeliverService;

/**
 * 采购结算
 * Created by mc on 14-10-29.
 */
@Controller
@RequestMapping("prosettlement")
public class ProsettlementController {
    @Autowired
    private ProsettlementService prosettlementService;
    @Autowired
    private CoderuleService coderuleService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private DeliverService deliverService;

    /**
     * 采购结算主界面
     * @param modelMap
     * @param prosettlement
     * @param page
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/list")
    public ModelAndView list(ModelMap modelMap,HttpSession session,Prosettlement prosettlement,Page page,String action) throws CRUDException {
    	if(null!=action && "init".equals(action)){
            prosettlement.setBdat(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
            prosettlement.setEdat(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
    		modelMap.put("prosettlement", new ArrayList<Prosettlement>());
    	}else{
    		if((prosettlement.getBdat()!=null && prosettlement.getBdat()!="")&&(prosettlement.getEdat()!=null && prosettlement.getEdat()!="")) {
                modelMap.put("prosettlement", prosettlementService.listPro(prosettlement,session.getAttribute("ChoiceAcct").toString(),page));
            }else{
                prosettlement.setBdat(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
                prosettlement.setEdat(DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
                modelMap.put("prosettlement", prosettlementService.listPro(prosettlement,session.getAttribute("ChoiceAcct").toString(),page));
            }
    	}
        modelMap.put("findPro",prosettlement);
        modelMap.put("pageobj",page);
        return new ModelAndView(ProsettlementConstants.LIST,modelMap);
    }

    /**
     * 采购结算也签
     * @param modelMap
     * @param prosettlement
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/listbottom")
    public ModelAndView listbottom(ModelMap modelMap,HttpSession session,Prosettlement prosettlement) throws CRUDException {
    	prosettlement.setAcct(session.getAttribute("ChoiceAcct").toString());
        if((prosettlement.getBdat()!=null && prosettlement.getBdat()!="")&&(prosettlement.getEdat()!=null && prosettlement.getEdat()!="")) {
            modelMap.put("proBill",prosettlementService.listProBill(prosettlement));
        }else{
            modelMap.put("proBill",null);
        }
        return new ModelAndView(ProsettlementConstants.BOTTOM,modelMap);
    }

    /**
     *采购结算弹窗
     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/popwin")
    public ModelAndView popwin(ModelMap modelMap,HttpSession session,String codes,String money,String type,Prosettlement prosettlement) throws Exception {
        modelMap.put("billnumber",coderuleService.getCode(CoderuleConstants.PAYMENT,session.getAttribute("ChoiceAcct").toString()));
        modelMap.put("prosettlement",prosettlement);
        Deliver deliver = new Deliver();
        deliver.setCode(prosettlement.getSupplier().getDelivercode());
        modelMap.put("deliver",deliverService.findDeliverByCode(deliver));
        modelMap.put("codes",codes);
        modelMap.put("type",type);
        modelMap.put("money",money);
        modelMap.put("date",DateFormat.getStringByDate(new Date(),"yyyy-MM-dd"));
        return new ModelAndView(ProsettlementConstants.POPWIN,modelMap);
    }

    /**
     * 生成付款单
     * @param proPayment
     * @return
     */
    @RequestMapping(value = "addPro")
    @ResponseBody
    public String addPro(HttpSession session,ProPayment proPayment) throws CRUDException {
       String res = prosettlementService.addPro(session.getAttribute("ChoiceAcct").toString(),session.getAttribute("accountName").toString(),proPayment);
        
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"生成付款单，付款单号：:"+res,session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return "success";
    }
    
    /**
	 * 打印采购结算单
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/printProsettlement")
	public ModelAndView printProsettlement(ModelMap modelMap,HttpSession session,HttpServletRequest request,String type,Prosettlement prosettlement,String billtype,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		prosettlement.setAcct(session.getAttribute("ChoiceAcct").toString());
		page.setPageSize(Integer.MAX_VALUE);
		prosettlement.setDelivercode(prosettlement.getSupplier().getDelivercode());
		Prosettlement  pst = prosettlementService.listPro(prosettlement,prosettlement.getAcct(),page).get(0);
		List<ProBill>  pstdList = prosettlementService.listProBill(prosettlement);
		List<ProBill> printlist = new ArrayList<ProBill>();
		for(ProBill probill:pstdList){
			if(billtype.equals("Y") && (probill.getIstate()==3 || probill.getIstate()==4)){
				printlist.add(probill);
			}else if(billtype.equals("N")&& (probill.getIstate()!=3 && probill.getIstate()!=4)){
				printlist.add(probill);
			}
		}
		List<Map<String,Object>> dataList = MapBeanConvertUtil.convertBeanListUpper(printlist);
		Map<String,Object> mapRes = MapBeanConvertUtil.convertBean(pst);
		Map<String,Object> prosettlementMap = MapBeanConvertUtil.convertBean(prosettlement);
		//设置分页，查询所有数据
		modelMap.put("actionMap", mapRes);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
	 	Map<String,Object>  parameters = mapRes;
        parameters.putAll(prosettlementMap);
        parameters.put("billtype", billtype);
	    parameters.put("report_name", billtype.equals("Y")?"采购结算已结账单":"采购结算未结账单");
	    modelMap.put("report_name", billtype.equals("Y")?"采购结算已结账单":"采购结算未结账单");//导出excel、pdf用到了
        modelMap.put("parameters", parameters);//ireport表头参数
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",dataList);
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,billtype.equals("Y")?"/report/assistant/bill/PrintProsettlementover.jasper":"/report/assistant/bill/PrintProsettlement.jasper");//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
}
