package com.choice.assistant.web.controller.settlement;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.settlement.PaymentConstants;
import com.choice.assistant.domain.settlement.Collinvoice;
import com.choice.assistant.domain.settlement.Paydetails;
import com.choice.assistant.domain.settlement.Payment;
import com.choice.assistant.service.settlement.PaymentAsstService;
import com.choice.assistant.service.system.PayMethodService;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.MapBeanConvertUtil;
import com.choice.assistant.util.ReadReportUrl;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.CodeDes;
import com.choice.scm.service.CodeDesService;

/**
 * 付款单
 * Created by mc on 14-10-31.
 */
@Controller
@RequestMapping("payment")
public class PaymentController {
    @Autowired
    public PaymentAsstService paymentService;
    @Autowired
    public PayMethodService payMethodService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private CodeDesService codeDesService;

    /**
     * 付款单主界面
     * @param modelMap
     * @param payment
     * @return
     */
    @RequestMapping(value = "/list")
    public ModelAndView list(ModelMap modelMap,HttpSession session,Payment payment,Page page) throws CRUDException {
        if(null==payment.getBdat() && null==payment.getEdat()){
        	payment.setBdat(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDD));
        	payment.setEdat(DateFormat.getStringByDate(new Date(), DateFormat.YYYYMMDD));
        }
        modelMap.put("payment",paymentService.findPayment(payment,session.getAttribute("ChoiceAcct").toString(), page));
        modelMap.put("pageobj",page);
        modelMap.put("findPay",payment);
        return new ModelAndView(PaymentConstants.LIST,modelMap);
    }

    /**
     * 付款单页签
     * @param modelMap
     * @param payment
     * @return
     */
    @RequestMapping(value = "/bottom")
    public ModelAndView bottom(ModelMap modelMap,Payment payment) throws CRUDException {
        modelMap.put("payment",paymentService.findPaymentd(payment));
        return new ModelAndView(PaymentConstants.BOTTOM,modelMap);
    }

    /**
     * 发票申请
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/applyinvoice")
    public ModelAndView applyinvoice(ModelMap modelMap,String codes,String moneys){
        modelMap.put("codes",codes);
        modelMap.put("moneys",moneys);
        return new ModelAndView(PaymentConstants.APPLYINVOICE,modelMap);
    }
    /**
     * 申请发票保存
     * @param session
     * @param payment
     * @return
     * @throws CRUDException
     */
    @RequestMapping(value = "/addApp")
    @ResponseBody
    public String addApplyinvoice(HttpSession session,Payment payment) throws CRUDException {
        
    	String res = paymentService.addApp(payment,session.getAttribute("ChoiceAcct").toString(),session.getAttribute("accountName").toString());

		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"申请发票,付款单号:"+payment.getVpaymentcode(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return res;
    }
    /**
     * 发票录入
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/collinvoice")
    public ModelAndView collinvoice(ModelMap modelMap,String codes,String moneys,String xkfp){
        modelMap.put("codes",codes);
        modelMap.put("moneys",moneys);
        modelMap.put("xkfp",xkfp);
        return new ModelAndView(PaymentConstants.COLLINVOICE,modelMap);
    }

    /**
     * 保存-发票录入
     * @param session
     * @param collinvoice
     * @return
     * @throws CRUDException 
     */
    @RequestMapping(value = "/addColl")
    @ResponseBody
    public String addCollinvoice(ModelMap modelMap,HttpSession session,Collinvoice collinvoice) throws CRUDException{
    	Payment payment = new Payment();
    	payment.setPk_payment(collinvoice.getCodes());
    	payment = 	paymentService.findPaymentByPk(payment);
        String msg=paymentService.addColl(session.getAttribute("ChoiceAcct").toString(),session.getAttribute("accountName").toString(),collinvoice);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"申请发票，付款单号："+payment.getVpaymentcode()+"，发票号:"+collinvoice.getVinvoicenum(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
        return msg;
    }

    /**
     * 付款
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/paymentd")
    public ModelAndView payment(ModelMap modelMap,String codes,String moneys, final HttpSession session){
        modelMap.put("codes",codes);
        modelMap.put("moneys",moneys);
        try {
    		//付款方式
    		CodeDes cds = new CodeDes();
    		cds.setTyp("22");
    		List<CodeDes> mhs = codeDesService.findCodeDes(cds,new Page());
            modelMap.put("mhs",mhs);
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        return new ModelAndView(PaymentConstants.PAYMENT,modelMap);
    }

    /**
     * 保存付款单子表
     * @param paydetails
     * @return
     */
    @RequestMapping(value = "/addPaydetails")
    @ResponseBody
    public String addPaydetails(HttpSession session,Payment payment,Paydetails paydetails){
    	String res = paymentService.addPaydetails(session.getAttribute("ChoiceAcct").toString(),paydetails,payment);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增付款，付款金额："+paydetails.getNpaymoney()+",付款人："+paydetails.getVpayaccount(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
    	return res;
        
    }
    
    /**
     * 审核结账
     * @param session
     * @param payment
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/checkEnd")
    @ResponseBody
    public String checkEnd(HttpSession session,Payment payment) throws Exception{
    	payment.setIpaymentstate(2);
    	payment.setPk_payment(CodeHelper.replaceCode(payment.getPk_payment()));
    	return paymentService.checkEnd(session.getAttribute("ChoiceAcct").toString(),payment);
    }

    /**
	 * 打印
	 * @param modelMap
	 * @param session
	 * @param pk_chksto
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/print")
	public ModelAndView print(ModelMap modelMap,HttpSession session,HttpServletRequest request,String type,String printtype,Payment payment,Page page)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		payment.setAcct(acct);
		Payment prep = paymentService.findPaymentByPk(payment);
		Payment paymentd = paymentService.findPaymentd(payment);
		
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		
		int ipaymentstate = prep.getIpaymentstate();
		switch(ipaymentstate){
			case 1 :prep.setVstate("未付款");break;
			case 2 :prep.setVstate("已付款");break;
			case 3 :prep.setVstate("支付未结束");break;
		}
		Map<String,Object> parameter = MapBeanConvertUtil.convertBean(prep);
		List<Map<String,Object>> p1list = MapBeanConvertUtil.convertBeanList(paymentd.getCollinvoices());
		parameter.put("parameter1", p1list);
		List<Map<String,Object>> p2list = MapBeanConvertUtil.convertBeanList(paymentd.getPaymentDList());
		parameter.put("parameter2", p2list);
		List<Map<String,Object>> p3list = MapBeanConvertUtil.convertBeanList(paymentd.getApplyinvoices());
		parameter.put("parameter3", p3list);
		List<Map<String,Object>> p4list = MapBeanConvertUtil.convertBeanList(paymentd.getPaydateiles());
		parameter.put("parameter4", p4list);
		
		//设置分页，查询所有数据
		modelMap.put("actionMap", parameter);//页面回调时保存的查询方法参数实体，打开页面时将参数在页面暂存，调用方法时再传回本方法
	   if(printtype.equals("bill")){
		   parameter.put("report_name", "付款单-账单详情");
		   modelMap.put("report_name", "付款单-账单详情");//导出excel、pdf用到了
	   }else{
		   parameter.put("report_name", "付款单-发票详情");
		   modelMap.put("report_name", "付款单-发票详情");//导出excel、pdf用到了
	   }
        parameter.put("printtype",printtype);
	    String separator = "";
		  //linux下需要加根目录
		  if("/".equals(File.separator)){   
			  separator = File.separator;
		  }
	    parameter.put("SUBREPORT_DIR", separator+PaymentController.class.getResource("").getPath().substring(1).replace("WEB-INF/classes/com/choice/assistant/web/controller/settlement", "report/assistant/bill"));
        modelMap.put("parameters", parameter);//ireport表头参数
	 	modelMap.put("action", request.getServletPath());//传入回调路径
		//获取并执行查询方法，获取查询结果，ireport表体参数List
		modelMap.put("List",dataList);
	 	Map<String,String> rs = ReadReportUrl.redReportUrl(type,printtype.equals("bill")?"/report/assistant/bill/Paymentzhangdan.jasper":"/report/assistant/bill/PaymentPaydfapiao.jasper");//判断跳转路径
        modelMap.put("reportUrl",rs.get("reportUrl"));//ireport文件地址
		return new ModelAndView(rs.get("url"),modelMap);
	}
}
