package com.choice.assistant.web.controller.tableMain;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.tableMain.TableMainOrdrConstants;
import com.choice.assistant.domain.bill.PuprOrder;
import com.choice.assistant.domain.detailedlist.Purtempletd;
import com.choice.assistant.domain.detailedlist.Purtempletm;
import com.choice.assistant.service.tableMain.TableMainOrdrService;
import com.choice.framework.exception.CRUDException;

/**
 * 首页订单
 * Created by zhb on 14-10-29.
 */
@Controller
@RequestMapping("tableMainOrdr")
public class TableMainOrdrController {
    @Autowired
    private TableMainOrdrService tableMainOrdrService;
   /**
    * 查询订单号所对应的首页订单
    * @param modelMap
    * @param session
    * @param vbillno
    * @return
    * @throws CRUDException
    */
    @RequestMapping(value = "/codeordrlist")
    public ModelAndView codeordrlist(ModelMap modelMap,HttpSession session,String vbillno) throws CRUDException {
    	PuprOrder puprOrder = new PuprOrder();
    	puprOrder.setAcct(session.getAttribute("ChoiceAcct").toString());
    	puprOrder.setVbillno(vbillno);
        modelMap.put("tableMainOrdrList",tableMainOrdrService.codeordrlist(puprOrder));
        modelMap.put("tableMainOrdrDetailList",tableMainOrdrService.codeordrDetailList(puprOrder));
        return new ModelAndView(TableMainOrdrConstants.CODEORDRLIST,modelMap);
    }
    @RequestMapping(value = "/cgqdlist")
    public ModelAndView cgqdlist(ModelMap modelMap,String id, HttpSession session,String vbillno) throws CRUDException {
    	Purtempletm purtempletm = new Purtempletm();
    	purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
    	purtempletm.setPk_purtemplet(id);
    	System.out.println(tableMainOrdrService.cgqdlist(purtempletm));
        modelMap.put("tableMainCgqdList",tableMainOrdrService.cgqdlist(purtempletm));
        Purtempletd purtempletd = new Purtempletd();
        purtempletd.setAcct(session.getAttribute("ChoiceAcct").toString());
        purtempletd.setPk_purtemplet(id);
        modelMap.put("tableMainCgqdLists",tableMainOrdrService.cgqdlists(purtempletd));
        return new ModelAndView(TableMainOrdrConstants.CGQDLIST,modelMap);
    }
}

