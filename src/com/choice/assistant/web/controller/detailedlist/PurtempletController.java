package com.choice.assistant.web.controller.detailedlist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.detailedlist.PurtempletConstants;
import com.choice.assistant.domain.detailedlist.Purtempletd;
import com.choice.assistant.domain.detailedlist.Purtempletm;
import com.choice.assistant.service.detailedlist.PurtempletService;
import com.choice.assistant.util.DateFormat;
import com.choice.assistant.util.ValueCheck;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Page;
import com.choice.orientationSys.util.Util;

@Controller
@RequestMapping("purtemplet")
public class PurtempletController {

	@Autowired
	private PurtempletService purtempletService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 查询所有的采购清单
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param purtempletm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllPurtempletm")
	public ModelAndView queryAllPurtempletm(ModelMap modelMap,HttpSession session,Page page,Purtempletm purtempletm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Purtempletm> listPurtempletm = purtempletService.queryAllPurtempletm(purtempletm,page);
		String pk_purtemplet = "";
		if (listPurtempletm.size()>0) {
			pk_purtemplet = listPurtempletm.get(0).getPk_purtemplet();
		}
		modelMap.put("listPurtempletm", listPurtempletm);
		modelMap.put("pk_purtemplet", pk_purtemplet);
		modelMap.put("pageobj", page);
		return new ModelAndView(PurtempletConstants.LISTPURTEMPLETM,modelMap);
	}
	
	/**
	 * 查询所有的采购清单子表数据
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param purtempletm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/queryAllPurtempletd")
	public ModelAndView queryAllPurtempletd(ModelMap modelMap,HttpSession session,String pk_purtempletm)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(null!=pk_purtempletm && !"".equals(pk_purtempletm)){
			Purtempletd purtempletd = new Purtempletd();
			purtempletd.setPk_purtemplet(pk_purtempletm);
			purtempletd.setAcct(session.getAttribute("ChoiceAcct").toString());
			List<Purtempletd> listPurtempletd = purtempletService.queryAllPurtempletd(purtempletd);
			modelMap.put("listPurtempletd", listPurtempletd);
		}
		return new ModelAndView(PurtempletConstants.LISTPURTEMPLETD,modelMap);
	}
	
	/**
	 * 跳转到添加采购清单的页面
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toAddPurtempletm")
	public ModelAndView toAddPurtempletm(ModelMap modelMap,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Purtempletm purtempletm = new Purtempletm();
		purtempletm.setDdate(DateFormat.getStringByDate(new Date(), "yyyy-MM-dd"));
		modelMap.put("purtempletm", purtempletm);
		modelMap.put("sta", sta);
		return new ModelAndView(PurtempletConstants.ADDPURTEMPLETD,modelMap);
	}
	
	/**
	 * 添加采购清单
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	@RequestMapping(value="/addPurtempletm")
	@ResponseBody
	public String addPurtempletm(Purtempletm purtempletm,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		purtempletm.setCreator(session.getAttribute("accountName").toString());
		purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
		for (int i = 0; i < purtempletm.getListPurtempletd().size(); i++) {
			purtempletm.getListPurtempletd().get(i).setAcct(session.getAttribute("ChoiceAcct").toString());
		}
		String res = purtempletService.addPurtempletm(purtempletm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.INSERT,"新增采购模版:"+purtempletm.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 删除清单信息
	 * @param modelMap
	 * @param session
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deletePurtempletm")
	public ModelAndView deletePurtempletm(ModelMap modelMap,HttpSession session,String pks)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Purtempletm purtempletm = new Purtempletm();
		List<String> ids = Arrays.asList(pks.split(","));
		String names = "";
		purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
		for(String str : ids){
			purtempletm.setPk_purtemplet(str);
			Purtempletm purtempletms = purtempletService.queryPurtempletmByPK(purtempletm);
			if(ValueCheck.IsNotEmpty(purtempletms)){
				names += ","+purtempletms.getVname();
			}
		}
		purtempletm.setPk_purtemplet(null);
		purtempletm.setPklist(ids);
		purtempletService.deletePurtempletm(purtempletm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.DELETE,"删除采购模版:"+names.substring(1),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return new ModelAndView(StringConstant.ACTION_DONE,modelMap);
	}
	
	/**
	 * 跳转到修改采购清单的界面
	 * @param modelMap
	 * @param session
	 * @param pk_purtemplet
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/toUpdatePurtempletm")
	public ModelAndView toUpdatePurtempletm(ModelMap modelMap,HttpSession session,String pk_purtemplet,String sta) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		Purtempletm purtempletm = new Purtempletm();
		purtempletm.setPk_purtemplet(pk_purtemplet);
		purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
		modelMap.put("purtempletm", purtempletService.queryPurtempletmByPK(purtempletm));
		Purtempletd purtempletd = new Purtempletd();
		purtempletd.setPk_purtemplet(pk_purtemplet);
		purtempletd.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Purtempletd> listPurtempletm = new ArrayList<Purtempletd>();
		listPurtempletm = purtempletService.queryAllPurtempletd(purtempletd);
		modelMap.put("listPurtempletm", listPurtempletm);
		modelMap.put("sta", sta);
		return new ModelAndView(PurtempletConstants.UPDATEPURTEMPLETD,modelMap);
	}
	
	/**
	 * 修改采购清单
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	@RequestMapping(value="/updatePurtempletm")
	@ResponseBody
	public String updatePurtempletm(Purtempletm purtempletm,HttpSession session,String sta)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		purtempletm.setModifier(session.getAttribute("accountName").toString());
		String res = purtempletService.updatePurtempletm(purtempletm);
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改采购模版:"+purtempletm.getVname(),session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		return res;
	}
	
	/**
	 * 查询所有的采购清单
	 * @param modelMap
	 * @param session
	 * @param page
	 * @param purtempletm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/selectAllPurtempletm")
	public ModelAndView selectAllPurtempletm(ModelMap modelMap,HttpSession session,Page page,Purtempletm purtempletm,String callBack)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Purtempletm> listPurtempletm = purtempletService.queryAllPurtempletm(purtempletm,page);
		String pk_purtemplet = "";
		if (listPurtempletm.size()>0) {
			pk_purtemplet = listPurtempletm.get(0).getPk_purtemplet();
		}
		modelMap.put("listPurtempletm", listPurtempletm);
		modelMap.put("pk_purtemplet", pk_purtemplet);
		modelMap.put("pageobj", page);
		modelMap.put("callBack", callBack);
		return new ModelAndView(PurtempletConstants.SELECTPURTEMPLETM,modelMap);
	}
	
	/**
	 * 验证采购清单名称
	 * @param purtempletm
	 * @param session
	 * @param sta
	 * @throws Exception
	 */
	@RequestMapping(value="/checkName")
	@ResponseBody
	public String checkName(Purtempletm purtempletm,HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		purtempletm.setAcct(session.getAttribute("ChoiceAcct").toString());
		List<Purtempletm> relist = purtempletService.getPurtempletmName(purtempletm);
		if(null!=relist && relist.size()>0){
			return "NOOK";
		}
		return "OK";
	}
}
