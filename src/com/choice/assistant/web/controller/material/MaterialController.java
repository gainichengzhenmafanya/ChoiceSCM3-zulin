package com.choice.assistant.web.controller.material;

import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.assistant.constants.material.MaterialConstants;
import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.assistant.domain.material.Material;
import com.choice.assistant.service.material.MaterialService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ForResourceFiles;
import com.choice.orientationSys.util.Page;
import com.choice.scm.constants.CostbomConstants;
import com.choice.scm.constants.FirmSupplyConstants;
import com.choice.scm.constants.SppriceQuickConstants;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Supply;
import com.choice.scm.service.CodeDesService;
import com.choice.scm.service.GrpTypService;
import com.choice.scm.service.SupplyService;

@Controller
@RequestMapping("material")
public class MaterialController {
	
	@Autowired
	CodeDesService codeDesService;
	@Autowired
	private SupplyService supplyService;
	@Autowired
	private MaterialService materialService;
	@Autowired
	private GrpTypService grpTypService;
	
	/**
	 * 根据选择的主键查询物资列表
	 * @param session
	 * @param material
	 * @param irateflag
	 * @return
	 * @throws Exception
	 * @author ZGL
	 * @Date 2015-01-05 15:38:18
	 */
	@RequestMapping(value="/queryMaterialByPk")
	@ResponseBody
	public List<Material> queryMaterialByPk(HttpSession session,Material material,Integer irateflag) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		material.setAcct(session.getAttribute("ChoiceAcct").toString());
		Page page = new Page();
		page.setPageSize(Integer.MAX_VALUE);
		return materialService.queryMaterial(material,page);
	}
	//===========================批量选择物资界面=========================
	/**
	 * 查询多条物资    弹出框 上
	 * @param modelMap
	 * @param defaultName 弹出框名称
	 * @param defaultCode	已经选择的物资
	 * @param type	跳转页面参数
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addSupplyBatchAsst")
	public ModelAndView addPositnBatch(ModelMap modelMap,String defaultName,String defaultCode,String type,String callBack) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		modelMap.put("defaultName", (null==defaultName||"".equals(defaultName))?defaultName:URLDecoder.decode(defaultName, "UTF-8"));
		modelMap.put("defaultCode", defaultCode);
		modelMap.put("callBack", callBack);
		if(type==null || "".equals(type)){
			return new ModelAndView(MaterialConstants.ADD_SUPPLYBATCH, modelMap);
		}else if ("quick".equals(type)){
			return new ModelAndView(SppriceQuickConstants.ADD_SUPPLY_BATCH_QUICK, modelMap);
		}else{
			return new ModelAndView(FirmSupplyConstants.ADD_SUPPLY_BATCH_FIRM, modelMap);
		}
	}
	/**
	 * 弹出物资选择框                左侧
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupplyLeftAsst")
	public ModelAndView selectSupplyLeft(ModelMap modelMap,HttpSession session,String defaultCode, String is_supply_x,String single,String ex1,String callBack,String deliver)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		modelMap.put("grpTypList", grpTypService.findAllGrpTypA(acct));//大类
		modelMap.put("grpList", grpTypService.findAllGrpA(acct,null));//中类
		modelMap.put("typList", grpTypService.findAllTypA(acct,null));//小类
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		modelMap.put("callBack", callBack);//默认选中的编码
		modelMap.put("single", single);//是否单选 默认单选  false多选
		modelMap.put("ex1", ex1);//是否是只查询加工品  wjf
		modelMap.put("deliver", deliver);//供应商
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){//虚拟物料  并且需要分组合并
			modelMap.put("is_supply_x", "Y_group");//虚拟物资编码
			return new ModelAndView(CostbomConstants.SELECT_SUPPLY_X, modelMap);
		}else{
			return new ModelAndView(MaterialConstants.SELECT_SUPPLY, modelMap);
		}
	}
	
	/**
	 * 弹出物资选择框                右侧
	 * @param modelMap
	 * @return    is_supply_x   虚拟物资编码
	 * @throws Exception
	 */
	@RequestMapping(value = "/selectSupply")
	public ModelAndView selectSupply(ModelMap modelMap, Supply supply, String level, String code,String single ,Page page,
			HttpSession session, String defaultCode, String is_supply_x,String deliver)throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		supply.setAcct(session.getAttribute("ChoiceAcct").toString());
		if(null!=is_supply_x && "Y_group".equals(is_supply_x)){
			supply.setIs_supply_x("Y_group");//虚拟物料  且是 分组合并
		}
		supply.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		supply.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		supply.setAccountId(session.getAttribute("accountId").toString());
		supply.setDeliver(deliver);
		modelMap.put("supplyList", supplyService.findAllSupplyByLeftGrpTyp(supply,level,code,page));
		modelMap.put("code", code);
		modelMap.put("level", level);
		modelMap.put("pageobj",page);
		modelMap.put("supply",supply);
		modelMap.put("defaultCode", defaultCode);//默认选中的编码
		//wangjie 2014年11月21日 14:04:40  报货单、出库单填制提交 物资支持多选
		modelMap.put("single", single );//是否单选 默认单选  false多选
		if(null!=is_supply_x && ("Y_group").equals(is_supply_x)){//是否虚拟物料
			modelMap.put("is_supply_x", "Y_group");
			return new ModelAndView(CostbomConstants.SELECT_TABLE_SUPPLY_X, modelMap);
		}else{
			return new ModelAndView(MaterialConstants.SELECT_TABLE_SUPPLY, modelMap);
		}
	}
	
	/**
	 * 查询合约报价
	 * @param pk_material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findMaterialNprice")
	@ResponseBody
	public Material findMaterialNprice(Material material, HttpSession session,String positncode,String delivercode)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		material.setAcct(session.getAttribute("ChoiceAcct").toString());
		material.setPositncode(positncode);
		material.setDelivercode(delivercode);
		return materialService.findMaterialNprice(material);
	}
	
	/**
	 * 查询供应物资范围报价
	 * @param pk_material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findMaterialNpricesupplier")
	@ResponseBody
	public Material findMaterialNpricesupplier(Material material, HttpSession session,String positncode,String delivercode)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		material.setAcct(session.getAttribute("ChoiceAcct").toString());
		material.setPositncode(positncode);
		material.setDelivercode(delivercode);
		return materialService.findMaterialNpricesupplier(material);
	}
	
	/**
	 * 根据输入的条件查询 前n条记录,用于申购时，可按照分店权限筛选
	 */
	
	@RequestMapping(value = "/findTop1", method=RequestMethod.POST)
	@ResponseBody
	public  List<Material> findTop1(Material material, HttpSession session)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//帐套
		material.setAcct(session.getAttribute("ChoiceAcct").toString());
		material.setWzqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZQX"));
		material.setWzzhqx(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "WZZHQX"));//物资账号权限是具体到物资还是具体到小类  wjf
		material.setAccountId(session.getAttribute("accountId").toString());
	
		return materialService.findMaterialTop(material);
	}
	
	/**
	 * 查询最小采购量
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findNmincntByRate")
	@ResponseBody
	public double findNmincntByRate(Material material,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		material.setAcct(session.getAttribute("ChoiceAcct").toString());
		double ncnt = materialService.findNmincntByRate(material);
		return ncnt;
	}
	
	/**
	 * 根据物资查询仓位
	 * @param material
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findPositnBySupply")
	@ResponseBody
	public List<Positn> findPositnBySupply(Material material,HttpSession session) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		material.setAcct(session.getAttribute("ChoiceAcct").toString());
		return materialService.findPositnBySupply(material);
	}
	
	/**
	 * 根据物资返回供应商
	 * @param material
	 * @return
	 */
	@RequestMapping(value="/findDeliverByMaterial")
	@ResponseBody
	public List<Deliver> findDeliverByMaterial(Material material){
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		 return materialService.findDeliverByMaterial(material);
	}
	
	/**
	 * 根据主键查询物资
	 * @param session
	 * @param pk_material
	 * @param pk_supplier
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/findMaterialByPk")
	@ResponseBody
	public Material findMaterialByPk(HttpSession session,String sp_code)throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String acct = session.getAttribute("ChoiceAcct").toString();
		return materialService.queryMaterialById(sp_code,acct);
	}
}
