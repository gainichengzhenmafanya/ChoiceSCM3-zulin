package com.choice.assistant.domain.settlement;

/**
 * 付款单-发票申请
 * Created by mc on 14-10-31.
 */
public class Applyinvoice {
    private String pk_payment;
    private String pk_applyinvoice;
    private String ddat;            //日期
    private String vinvoicenum;     //发票号
    private String vinvoicehead;    //发票头
    private Double ninvoicemoney;   //发票金额
    private String vapplyaccount;   //开票人
    private String vmemo;           //备注
    private Integer istate;         //状态
    private String creator;         //创建人
    private String creationtime;    //创建时间
    private Integer dr;
    private String ts;
    private String acct;

    public String getPk_payment() {
        return pk_payment;
    }

    public void setPk_payment(String pk_payment) {
        this.pk_payment = pk_payment;
    }

    public String getPk_applyinvoice() {
        return pk_applyinvoice;
    }

    public void setPk_applyinvoice(String pk_applyinvoice) {
        this.pk_applyinvoice = pk_applyinvoice;
    }

    public String getDdat() {
        return ddat;
    }

    public void setDdat(String ddat) {
        this.ddat = ddat;
    }

    public String getVinvoicenum() {
        return vinvoicenum;
    }

    public void setVinvoicenum(String vinvoicenum) {
        this.vinvoicenum = vinvoicenum;
    }

    public String getVinvoicehead() {
        return vinvoicehead;
    }

    public void setVinvoicehead(String vinvoicehead) {
        this.vinvoicehead = vinvoicehead;
    }

    public Double getNinvoicemoney() {
        return ninvoicemoney;
    }

    public void setNinvoicemoney(Double ninvoicemoney) {
        this.ninvoicemoney = ninvoicemoney;
    }

    public String getVapplyaccount() {
        return vapplyaccount;
    }

    public void setVapplyaccount(String vapplyaccount) {
        this.vapplyaccount = vapplyaccount;
    }

    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getCreator() {
        return creator;
    }

    public Integer getIstate() {
        return istate;
    }

    public void setIstate(Integer istate) {
        this.istate = istate;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(String creationtime) {
        this.creationtime = creationtime;
    }
}
