package com.choice.assistant.domain.settlement;

import com.choice.assistant.domain.supplier.Supplier;

/**
 * Created by mc on 14-10-31.
 */
public class ProPayment {
    private Supplier supplier;  //供应商
    private String billdate;    //单据日期
    private Double nmoney;      //付款金额
    private String billNo;      //付款号
    private Double imoney;      //发票金额

    private String codes;//需要生成付款单的单据PK
    private String pks;//需要生成付款单的单据(其他应付款)
    private String type;//单据类型 具体类型 详见ProsettlementConstants
    private String acct;

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public String getBilldate() {
        return billdate;
    }

    public void setBilldate(String billdate) {
        this.billdate = billdate;
    }

    public Double getNmoney() {
        return nmoney;
    }

    public void setNmoney(Double nmoney) {
        this.nmoney = nmoney;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Double getImoney() {
        return imoney;
    }

    public void setImoney(Double imoney) {
        this.imoney = imoney;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getPks() {
		return pks;
	}

	public void setPks(String pks) {
		this.pks = pks;
	}
}
