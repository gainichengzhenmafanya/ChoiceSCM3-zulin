package com.choice.assistant.domain.settlement;

/**
 * 付款单子表
 * Created by mc on 14-10-31.
 */
public class Payment_d {
    private String pk_payment;
    private String pk_paymentd;
    private Integer vbilltype;
    private String vbillnumber;
    private String pk_bills;
    private Double nypaymoney;
    private Double npaymoney;
    private String vpayaccount;//付款人
    private String dpaydat;//付款时间
    private String creator;//制单人
    private String creationtime;//制单日期
    private String vmemo;
    private String vbdef1;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;
    private Integer dr;
    private String ts;
    private String acct;

    public String getPk_payment() {
        return pk_payment;
    }

    public void setPk_payment(String pk_payment) {
        this.pk_payment = pk_payment;
    }

    public String getPk_paymentd() {
        return pk_paymentd;
    }

    public void setPk_paymentd(String pk_paymentd) {
        this.pk_paymentd = pk_paymentd;
    }

    public Integer getVbilltype() {
        return vbilltype;
    }

    public void setVbilltype(Integer vbilltype) {
        this.vbilltype = vbilltype;
    }

    public String getVbillnumber() {
        return vbillnumber;
    }

    public void setVbillnumber(String vbillnumber) {
        this.vbillnumber = vbillnumber;
    }

    public String getPk_bills() {
        return pk_bills;
    }

    public void setPk_bills(String pk_bills) {
        this.pk_bills = pk_bills;
    }

    public Double getNypaymoney() {
        return nypaymoney;
    }

    public void setNypaymoney(Double nypaymoney) {
        this.nypaymoney = nypaymoney;
    }

    public Double getNpaymoney() {
        return npaymoney;
    }

    public void setNpaymoney(Double npaymoney) {
        this.npaymoney = npaymoney;
    }

    public String getVpayaccount() {
        return vpayaccount;
    }

    public void setVpayaccount(String vpayaccount) {
        this.vpayaccount = vpayaccount;
    }

    public String getDpaydat() {
        return dpaydat;
    }

    public void setDpaydat(String dpaydat) {
        this.dpaydat = dpaydat;
    }

    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public String getVbdef1() {
        return vbdef1;
    }

    public void setVbdef1(String vbdef1) {
        this.vbdef1 = vbdef1;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(String creationtime) {
        this.creationtime = creationtime;
    }
}
