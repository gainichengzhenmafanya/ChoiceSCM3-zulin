package com.choice.assistant.domain.settlement;

/**
 * 付款明细表
 * Created by mc on 14-11-6.
 */
public class Paydetails {
    private String pk_payment;
    private String pk_paydetails;
    private Double npaymoney;//付款金额
    private String vpayaccount;//付款人
    private String dpaydat;//付款时间
    private String pk_paymodel;//支付方式
    private String vpaymodelname;//支付方式名称
    private String acct;
    private String vmemo;
    private String vbdef1;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;
    private Integer dr;
    private String ts;

    public String getPk_payment() {
        return pk_payment;
    }

    public void setPk_payment(String pk_payment) {
        this.pk_payment = pk_payment;
    }

    public String getPk_paydetails() {
        return pk_paydetails;
    }

    public void setPk_paydetails(String pk_paydetails) {
        this.pk_paydetails = pk_paydetails;
    }

    public Double getNpaymoney() {
        return npaymoney;
    }

    public void setNpaymoney(Double npaymoney) {
        this.npaymoney = npaymoney;
    }

    public String getVpayaccount() {
        return vpayaccount;
    }

    public void setVpayaccount(String vpayaccount) {
        this.vpayaccount = vpayaccount;
    }

    public String getDpaydat() {
        return dpaydat;
    }

    public void setDpaydat(String dpaydat) {
        this.dpaydat = dpaydat;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public String getVbdef1() {
        return vbdef1;
    }

    public void setVbdef1(String vbdef1) {
        this.vbdef1 = vbdef1;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getPk_paymodel() {
        return pk_paymodel;
    }

    public void setPk_paymodel(String pk_paymodel) {
        this.pk_paymodel = pk_paymodel;
    }

    public String getVpaymodelname() {
        return vpaymodelname;
    }

    public void setVpaymodelname(String vpaymodelname) {
        this.vpaymodelname = vpaymodelname;
    }
}
