package com.choice.assistant.domain.settlement;

import java.util.List;

import com.choice.assistant.domain.supplier.Supplier;

/**
 * 付款单主表
 * Created by mc on 14-10-31.
 */
public class Payment {
    private String acct;//
    private String pk_payment;//
    private String vpaymentcode;//付款单号
    private Supplier delivercode;//供应商
    private String dbilldat;//单据日期
    private Double nypaymoney;//应付金额
    private Double nxpaymoney;//需付金额
    private Double nwpaymoney;//已付金额
    private Double nnpaymoney;//未付金额
    private Double ninvoicemoney;//发票金额
    private Integer iisorinvoive;//是否已开发票
    private Integer ipaymentstate;//付款状态
    private String creator;//制单人
    private String creationtime;//制单日期
    private String modifier;//修改人
    private String modifiedtime;//修改时间
    private String vdef1;
    private String vdef2;
    private String vdef3;
    private String vdef4;
    private String vdef5;
    private Integer dr;
    private String ts;

    private String bdat;
    private String edat;
    private List<Payment_d> paymentDList;
    private List<Collinvoice> collinvoices;
    private List<Applyinvoice> applyinvoices;
    private List<Paydetails> paydateiles;
    private String codes;
    private String moneys;
    private String vstate;

    public Double getNnpaymoney() {
		return nnpaymoney;
	}

	public void setNnpaymoney(Double nnpaymoney) {
		this.nnpaymoney = nnpaymoney;
	}

	public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_payment() {
        return pk_payment;
    }

    public void setPk_payment(String pk_payment) {
        this.pk_payment = pk_payment;
    }

    public String getVpaymentcode() {
        return vpaymentcode;
    }

    public void setVpaymentcode(String vpaymentcode) {
        this.vpaymentcode = vpaymentcode;
    }

    public Supplier getPk_supplier() {
        return delivercode;
    }

    public void setPk_supplier(Supplier delivercode) {
        this.delivercode = delivercode;
    }

    public Supplier getDelivercode() {
		return delivercode;
	}

	public void setDelivercode(Supplier delivercode) {
		this.delivercode = delivercode;
	}

	public String getDbilldat() {
        return dbilldat;
    }

    public void setDbilldat(String dbilldat) {
        this.dbilldat = dbilldat;
    }

    public Double getNypaymoney() {
        return nypaymoney;
    }

    public void setNypaymoney(Double nypaymoney) {
        this.nypaymoney = nypaymoney;
    }

    public Double getNxpaymoney() {
        return nxpaymoney;
    }

    public void setNxpaymoney(Double nxpaymoney) {
        this.nxpaymoney = nxpaymoney;
    }

    public Double getNwpaymoney() {
        return nwpaymoney;
    }

    public void setNwpaymoney(Double nwpaymoney) {
        this.nwpaymoney = nwpaymoney;
    }

    public Double getNinvoicemoney() {
        return ninvoicemoney;
    }

    public void setNinvoicemoney(Double ninvoicemoney) {
        this.ninvoicemoney = ninvoicemoney;
    }

    public Integer getIisorinvoive() {
        return iisorinvoive;
    }

    public void setIisorinvoive(Integer iisorinvoive) {
        this.iisorinvoive = iisorinvoive;
    }

    public Integer getIpaymentstate() {
        return ipaymentstate;
    }

    public void setIpaymentstate(Integer ipaymentstate) {
        this.ipaymentstate = ipaymentstate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(String creationtime) {
        this.creationtime = creationtime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getModifiedtime() {
        return modifiedtime;
    }

    public void setModifiedtime(String modifiedtime) {
        this.modifiedtime = modifiedtime;
    }

    public String getVdef1() {
        return vdef1;
    }

    public void setVdef1(String vdef1) {
        this.vdef1 = vdef1;
    }

    public String getVdef2() {
        return vdef2;
    }

    public void setVdef2(String vdef2) {
        this.vdef2 = vdef2;
    }

    public String getVdef3() {
        return vdef3;
    }

    public void setVdef3(String vdef3) {
        this.vdef3 = vdef3;
    }

    public String getVdef4() {
        return vdef4;
    }

    public void setVdef4(String vdef4) {
        this.vdef4 = vdef4;
    }

    public String getVdef5() {
        return vdef5;
    }

    public void setVdef5(String vdef5) {
        this.vdef5 = vdef5;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public List<Payment_d> getPaymentDList() {
        return paymentDList;
    }

    public void setPaymentDList(List<Payment_d> paymentDList) {
        this.paymentDList = paymentDList;
    }

    public String getBdat() {
        return bdat;
    }

    public void setBdat(String bdat) {
        this.bdat = bdat;
    }

    public String getEdat() {
        return edat;
    }

    public void setEdat(String edat) {
        this.edat = edat;
    }

    public List<Collinvoice> getCollinvoices() {
        return collinvoices;
    }

    public void setCollinvoices(List<Collinvoice> collinvoices) {
        this.collinvoices = collinvoices;
    }

    public List<Applyinvoice> getApplyinvoices() {
        return applyinvoices;
    }

    public void setApplyinvoices(List<Applyinvoice> applyinvoices) {
        this.applyinvoices = applyinvoices;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public String getMoneys() {
        return moneys;
    }

    public void setMoneys(String moneys) {
        this.moneys = moneys;
    }

    public List<Paydetails> getPaydateiles() {
        return paydateiles;
    }

    public void setPaydateiles(List<Paydetails> paydateiles) {
        this.paydateiles = paydateiles;
    }

	public String getVstate() {
		return vstate;
	}

	public void setVstate(String vstate) {
		this.vstate = vstate;
	}
}
