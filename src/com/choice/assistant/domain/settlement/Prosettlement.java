package com.choice.assistant.domain.settlement;

import java.util.List;

import com.choice.assistant.domain.supplier.Supplier;

/**
 * 采购结算
 * Created by mc on 14-10-30.
 * 期初未接+本期进货-本期已结=期末未接
 */
public class Prosettlement {
    private Supplier supplier;
    private Double startUnset;//期初未结
    private Double nowPro;//本期进货
    private Double nowSet;//本期结算
    private Double endUnset;//期末未结
    private Double invoice;//已开发票
    private Double nInvoice;//未开发票
    private String bdat;
    private String edat;
    private String acct;

    private List<ProBill> proBills;
    private String delivercode;	//供应商编码、主键
    private String delivername;	//供应商名称
    
    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Double getStartUnset() {
        return startUnset;
    }

    public void setStartUnset(Double startUnset) {
        this.startUnset = startUnset;
    }

    public Double getNowPro() {
        return nowPro;
    }

    public void setNowPro(Double nowPro) {
        this.nowPro = nowPro;
    }

    public Double getNowSet() {
        return nowSet;
    }

    public void setNowSet(Double nowSet) {
        this.nowSet = nowSet;
    }

    public Double getEndUnset() {
        return endUnset;
    }

    public void setEndUnset(Double endUnset) {
        this.endUnset = endUnset;
    }

    public Double getInvoice() {
        return invoice;
    }

    public void setInvoice(Double invoice) {
        this.invoice = invoice;
    }

    public Double getnInvoice() {
        return nInvoice;
    }

    public void setnInvoice(Double nInvoice) {
        this.nInvoice = nInvoice;
    }

    public String getBdat() {
        return bdat;
    }

    public void setBdat(String bdat) {
        this.bdat = bdat;
    }

    public String getEdat() {
        return edat;
    }

    public void setEdat(String edat) {
        this.edat = edat;
    }

    public List<ProBill> getProBills() {
        return proBills;
    }

    public void setProBills(List<ProBill> proBills) {
        this.proBills = proBills;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

	public String getDelivercode() {
		return delivercode;
	}

	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}

	public String getDelivername() {
		return delivername;
	}

	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
}
