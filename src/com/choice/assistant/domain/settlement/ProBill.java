package com.choice.assistant.domain.settlement;

import com.choice.assistant.domain.supplier.Supplier;

/**
 * 采购结算子表
 * Created by mc on 14-10-30.
 */
public class ProBill {
    private String dbilldate;//单据日期
    private String dpaydate;//付款单日期
    private String vbillno; //单据号
    private Double nmoney;  //金额
    private Integer billType;//单据类型
    private String pk_bill; //单据pk
    private String proNumber;//付款单号
    private Integer istate; // 单据状态
    private Supplier supplier;//供应商

    public String getDbilldate() {
        return dbilldate;
    }

    public void setDbilldate(String dbilldate) {
        this.dbilldate = dbilldate;
    }

    public String getVbillno() {
        return vbillno;
    }

    public void setVbillno(String vbillno) {
        this.vbillno = vbillno;
    }

    public Double getNmoney() {
        return nmoney;
    }

    public void setNmoney(Double nmoney) {
        this.nmoney = nmoney;
    }

    public Integer getBillType() {
        return billType;
    }

    public void setBillType(Integer billType) {
        this.billType = billType;
    }

    public String getProNumber() {
        return proNumber;
    }

    public void setProNumber(String proNumber) {
        this.proNumber = proNumber;
    }

    public Integer getIstate() {
        return istate;
    }

    public void setIstate(Integer istate) {
        this.istate = istate;
    }

    public String getPk_bill() {
        return pk_bill;
    }

    public void setPk_bill(String pk_bill) {
        this.pk_bill = pk_bill;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public String getDpaydate() {
        return dpaydate;
    }

    public void setDpaydate(String dpaydate) {
        this.dpaydate = dpaydate;
    }
}
