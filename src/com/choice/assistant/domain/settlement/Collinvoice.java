package com.choice.assistant.domain.settlement;

/**
 * 付款单-关联发票
 * Created by mc on 14-10-31.
 */
public class Collinvoice {
    private String pk_payment;
    private String pk_collinvoice;
    private String ddat;//日期
    private Double ninvoicemoney;//发票金额
    private String vinvoicenum;//发票号
    private String vinvoceacccount;//开票人
    private String creator;//制单人
    private String creationtime;//制单日期
    private Integer dr;
    private String ts;
    private String acct;

    private String codes;

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public String getPk_payment() {
        return pk_payment;
    }

    public void setPk_payment(String pk_payment) {
        this.pk_payment = pk_payment;
    }

    public String getPk_collinvoice() {
        return pk_collinvoice;
    }

    public void setPk_collinvoice(String pk_collinvoice) {
        this.pk_collinvoice = pk_collinvoice;
    }

    public String getDdat() {
        return ddat;
    }

    public void setDdat(String ddat) {
        this.ddat = ddat;
    }

    public Double getNinvoicemoney() {
        return ninvoicemoney;
    }

    public void setNinvoicemoney(Double ninvoicemoney) {
        this.ninvoicemoney = ninvoicemoney;
    }

    public String getVinvoicenum() {
        return vinvoicenum;
    }

    public void setVinvoicenum(String vinvoicenum) {
        this.vinvoicenum = vinvoicenum;
    }

    public String getVinvoceacccount() {
        return vinvoceacccount;
    }

    public void setVinvoceacccount(String vinvoceacccount) {
        this.vinvoceacccount = vinvoceacccount;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(String creationtime) {
        this.creationtime = creationtime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
