package com.choice.assistant.domain;

public class AssStringConstant {
	//map
	public static final String IREPORT_HTML = "share/ireport/mapSource/html";  //html页面
	public static final String IREPORT_EXCEL = "share/ireport/mapSource/excel";  //excel页面
	public static final String IREPORT_PDF = "share/ireport/mapSource/pdf";  //pdf页面
	public static final String IREPORT_WORD = "share/ireport/mapSource/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL = "share/ireport/mapSource/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF = "share/ireport/mapSource/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD = "share/ireport/mapSource/printWord";  //直接打印word页面
	
	public static final String IREPORT_HTML_BEAN = "share/ireport/html";  //html页面
	public static final String IREPORT_EXCEL_BEAN = "share/ireport/excel";  //excel页面
	public static final String IREPORT_PDF_BEAN = "share/ireport/pdf";  //pdf页面
	public static final String IREPORT_WORD_BEAN = "share/ireport/word";  //word页面
	public static final String IREPORT_PRINT_EXCEL_BEAN = "share/ireport/printExcel";  //直接打印excel页面
	public static final String IREPORT_PRINT_PDF_BEAN = "share/ireport/printPdf";  //直接打印pdf页面
	public static final String IREPORT_PRINT_WORD_BEAN = "share/ireport/printWord";  //直接打印word页面
	public static final String TO_COLUMNS_CHOOSE_VIEW="share/columnsChoose";
}
