package com.choice.assistant.domain.supplier;

import com.choice.assistant.domain.util.ManagerName;

/**
 * 合约条款
 * Created by mc on 14-10-24.
 */
@ManagerName(value = "合约条款")
public class Contractclau {
    private String pk_purcontract;//采购合约主键
    private String pk_contractclau;//合约条款主键
    private String vname;//条款名称
    private String vcontent;//条款内容
    private String vmemo;//备注
    private Integer dr=0;
    private String ts;
    private String vbdef;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;
    private String acct;

    public String getPk_purcontract() {
        return pk_purcontract;
    }

    public void setPk_purcontract(String pk_purcontract) {
        this.pk_purcontract = pk_purcontract;
    }

    public String getPk_contractclau() {
        return pk_contractclau;
    }

    public void setPk_contractclau(String pk_contractclau) {
        this.pk_contractclau = pk_contractclau;
    }

    @ManagerName(value = "合约条款-名称")
    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }
    @ManagerName(value = "合约条款-内容")
    public String getVcontent() {
        return vcontent;
    }

    public void setVcontent(String vcontent) {
        this.vcontent = vcontent;
    }
    @ManagerName(value = "合约条款-备注")
    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVbdef() {
        return vbdef;
    }

    public void setVbdef(String vbdef) {
        this.vbdef = vbdef;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }
}
