package com.choice.assistant.domain.supplier;


/**
 * 采购合约 变更历史
 * Created by mc on 14-10-25.
 */
public class ChangeLog {
    private String pk_changelog;
    private String pk_purcontract;
    private String version;
    private String vchangecontent;
    private String vchangedate;
    private String vaccount;
    private String vaccountname;
    private String vdef5;
    private String vdef4;
    private String vdef3;
    private String vdef2;
    private String vdef1;
    private Integer dr=0;
    private String ts;

    public String getPk_changelog() {
        return pk_changelog;
    }

    public void setPk_changelog(String pk_changelog) {
        this.pk_changelog = pk_changelog;
    }

    public String getPk_purcontract() {
        return pk_purcontract;
    }

    public void setPk_purcontract(String pk_purcontract) {
        this.pk_purcontract = pk_purcontract;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVdef5() {
        return vdef5;
    }

    public void setVdef5(String vdef5) {
        this.vdef5 = vdef5;
    }

    public String getVdef4() {
        return vdef4;
    }

    public void setVdef4(String vdef4) {
        this.vdef4 = vdef4;
    }

    public String getVdef3() {
        return vdef3;
    }

    public void setVdef3(String vdef3) {
        this.vdef3 = vdef3;
    }

    public String getVdef2() {
        return vdef2;
    }

    public void setVdef2(String vdef2) {
        this.vdef2 = vdef2;
    }

    public String getVdef1() {
        return vdef1;
    }

    public void setVdef1(String vdef1) {
        this.vdef1 = vdef1;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVchangecontent() {
        return vchangecontent;
    }

    public void setVchangecontent(String vchangecontent) {
        this.vchangecontent = vchangecontent;
    }

    public String getVchangedate() {
        return vchangedate;
    }

    public void setVchangedate(String vchangedate) {
        this.vchangedate = vchangedate;
    }

    public String getVaccount() {
        return vaccount;
    }

    public void setVaccount(String vaccount) {
        this.vaccount = vaccount;
    }

    public String getVaccountname() {
        return vaccountname;
    }

    public void setVaccountname(String vaccountname) {
        this.vaccountname = vaccountname;
    }
}
