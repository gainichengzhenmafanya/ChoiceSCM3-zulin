package com.choice.assistant.domain.supplier;

/**
 * 采购合约附件
 * Created by mc on 14-10-24.
 */
public class Transationatta {
    private String pk_purcontract;//采购合约主键
    private String pk_transationatta;//主键
    private String vattafile;//附件路径
    private String vattaname;//附件名称
    private Integer dr;
    private String ts;
    private String vbdef;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;
    private String acct;

    public String getPk_purcontract() {
        return pk_purcontract;
    }

    public void setPk_purcontract(String pk_purcontract) {
        this.pk_purcontract = pk_purcontract;
    }

    public String getPk_transationatta() {
        return pk_transationatta;
    }

    public void setPk_transationatta(String pk_transationatta) {
        this.pk_transationatta = pk_transationatta;
    }

    public String getVattafile() {
        return vattafile;
    }

    public void setVattafile(String vattafile) {
        this.vattafile = vattafile;
    }

    public String getVattaname() {
        return vattaname;
    }

    public void setVattaname(String vattaname) {
        this.vattaname = vattaname;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVbdef() {
        return vbdef;
    }

    public void setVbdef(String vbdef) {
        this.vbdef = vbdef;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }
}
