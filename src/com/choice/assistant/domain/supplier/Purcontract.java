package com.choice.assistant.domain.supplier;

import java.util.List;

import com.choice.assistant.domain.util.ManagerName;
import com.choice.scm.domain.Deliver;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.Spprice;

/**
 * 采购合约
 * Created by mc on 14-10-24.
 */
@ManagerName(value = "采购合约")
public class Purcontract {
    private String acct;
    private String pk_purcontract;//采购合约主键
    private Positn positncode;  //组织
    private Deliver delivercode;//供应商主键
    private String vcontractcode;//合约编码
    private String vcontractname;//合约名称
    private String vcontracttyp;//合约类型
    private String vcontracttypcode;//合约类型编码
    private String pk_contractterms;//合约类型pk
    private String vcontractversion;//版本号
    private String dsigndat;//合约签订日期
    private String dstartdat;//计划生效日期
    private String denddat;//计划终止日期
    private String vpurcaccount;//采购人员
    private Integer istate;//状态1未审核  3 生效 11失效 4终止
    private Integer ihintday;//提前提醒天数 1 代表合约到期提前一天提醒
    private String vmemo;//对方单位说明
    private Integer dr=0;
    private String ts;
    private String vdef1;
    private String vdef2;
    private String vdef3;
    private String vdef4;
    private String vdef5;
    private List<Contracteven> contractevenList;//合约大事记
    private List<Spprice> sppriceList;//合约报价
    private List<ContractQuote> delContractQuoteList;//合约报价
    private List<Contractclau> contractclauList;//合约条款
    private List<Transationatta> transationattaList;//合约附件
    private List<ChangeLog> changeLogList;//合约修改记录
    private List<String> list;//首页订单反值所用
    
    public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_purcontract() {
        return pk_purcontract;
    }

    public void setPk_purcontract(String pk_purcontract) {
        this.pk_purcontract = pk_purcontract;
    }
    @ManagerName("采购组织")
    public void setPositncode(Positn positncode) {
    	this.positncode = positncode;
    }
    public Positn getPositncode() {
    	return positncode;
    }
    
    @ManagerName("供应商")
    
    public Deliver getDelivercode() {
    	return delivercode;
    }
    
    public void setDelivercode(Deliver delivercode) {
    	this.delivercode = delivercode;
    }

    public String getVcontractcode() {
        return vcontractcode;
    }

	public void setVcontractcode(String vcontractcode) {
        this.vcontractcode = vcontractcode;
    }
    @ManagerName("合约名称")
    public String getVcontractname() {
        return vcontractname;
    }

    public void setVcontractname(String vcontractname) {
        this.vcontractname = vcontractname;
    }
    @ManagerName("合约类型")
    public String getVcontracttyp() {
        return vcontracttyp;
    }

    public void setVcontracttyp(String vcontracttyp) {
        this.vcontracttyp = vcontracttyp;
    }
    @ManagerName("版本号")
    public String getVcontractversion() {
        return vcontractversion;
    }

    public void setVcontractversion(String vcontractversion) {
        this.vcontractversion = vcontractversion;
    }
    @ManagerName("合约签订日期")
    public String getDsigndat() {
        return dsigndat;
    }

    public void setDsigndat(String dsigndat) {
        this.dsigndat = dsigndat;
    }
    @ManagerName("计划生效日期")
    public String getDstartdat() {
        return dstartdat;
    }

    public void setDstartdat(String dstartdat) {
        this.dstartdat = dstartdat;
    }
    @ManagerName("计划终止日期")
    public String getDenddat() {
        return denddat;
    }

    public void setDenddat(String denddat) {
        this.denddat = denddat;
    }
    @ManagerName("采购人员")
    public String getVpurcaccount() {
        return vpurcaccount;
    }

    public void setVpurcaccount(String vpurcaccount) {
        this.vpurcaccount = vpurcaccount;
    }
    @ManagerName(value = "状态",isDB = true,enumName = "S")
    public Integer getIstate() {
        return istate;
    }

    public void setIstate(Integer istate) {
        this.istate = istate;
    }
    @ManagerName("对方单位说明")
    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVdef1() {
        return vdef1;
    }

    public void setVdef1(String vdef1) {
        this.vdef1 = vdef1;
    }

    public String getVdef2() {
        return vdef2;
    }

    public void setVdef2(String vdef2) {
        this.vdef2 = vdef2;
    }

    public String getVdef3() {
        return vdef3;
    }

    public void setVdef3(String vdef3) {
        this.vdef3 = vdef3;
    }

    public String getVdef4() {
        return vdef4;
    }

    public void setVdef4(String vdef4) {
        this.vdef4 = vdef4;
    }

    public String getVdef5() {
        return vdef5;
    }

    public void setVdef5(String vdef5) {
        this.vdef5 = vdef5;
    }

    public List<Contracteven> getContractevenList() {
        return contractevenList;
    }

    public void setContractevenList(List<Contracteven> contractevenList) {
        this.contractevenList = contractevenList;
    }

    public List<Spprice> getSppriceList() {
		return sppriceList;
	}

	public void setSppriceList(List<Spprice> sppriceList) {
		this.sppriceList = sppriceList;
	}

	@ManagerName(value = "合约条款")
    public List<Contractclau> getContractclauList() {
        return contractclauList;
    }

    public void setContractclauList(List<Contractclau> contractclauList) {
        this.contractclauList = contractclauList;
    }

    public List<Transationatta> getTransationattaList() {
        return transationattaList;
    }

    public void setTransationattaList(List<Transationatta> transationattaList) {
        this.transationattaList = transationattaList;
    }

    public List<ChangeLog> getChangeLogList() {
        return changeLogList;
    }

    public void setChangeLogList(List<ChangeLog> changeLogList) {
        this.changeLogList = changeLogList;
    }

    public List<ContractQuote> getDelContractQuoteList() {
        return delContractQuoteList;
    }

    public void setDelContractQuoteList(List<ContractQuote> delContractQuoteList) {
        this.delContractQuoteList = delContractQuoteList;
    }

    public String getVcontracttypcode() {
        return vcontracttypcode;
    }

    public void setVcontracttypcode(String vcontracttypcode) {
        this.vcontracttypcode = vcontracttypcode;
    }

    public String getPk_contractterms() {
        return pk_contractterms;
    }

    public void setPk_contractterms(String pk_contractterms) {
        this.pk_contractterms = pk_contractterms;
    }
    @ManagerName("提前提醒天数")
    public Integer getIhintday() {
        return ihintday;
    }

    public void setIhintday(Integer ihintday) {
        this.ihintday = ihintday;
    }
}
