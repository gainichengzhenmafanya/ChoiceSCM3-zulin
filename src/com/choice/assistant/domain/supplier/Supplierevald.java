package com.choice.assistant.domain.supplier;

/**
 * 供应商评价
 * Created by mc on 14-10-23.
 */
public class Supplierevald {
    private String delivercodeevald;
    private String vstandard;//指标
    private String vweight;//权重
    private Double nscore;//得分
    private String vmemo;//备注
    private String acct;
    private String delivercodeeval;
    private String vbdef1;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;
    private Integer dr;
    private String ts;

    public String getPk_supplierevald() {
        return delivercodeevald;
    }

    public void setPk_supplierevald(String delivercodeevald) {
        this.delivercodeevald = delivercodeevald;
    }

    public String getVstandard() {
        return vstandard;
    }

    public void setVstandard(String vstandard) {
        this.vstandard = vstandard;
    }

    public String getVweight() {
        return vweight;
    }

    public void setVweight(String vweight) {
        this.vweight = vweight;
    }

    public Double getNscore() {
        return nscore;
    }

    public void setNscore(Double nscore) {
        this.nscore = nscore;
    }

    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_suppliereval() {
        return delivercodeeval;
    }

    public void setPk_suppliereval(String delivercodeeval) {
        this.delivercodeeval = delivercodeeval;
    }

    public String getVbdef1() {
        return vbdef1;
    }

    public void setVbdef1(String vbdef1) {
        this.vbdef1 = vbdef1;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
