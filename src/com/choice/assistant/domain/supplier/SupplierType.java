package com.choice.assistant.domain.supplier;

import java.util.List;


/**
 * 供应商类别表
 * @author wangchao 
 *
 */
public class SupplierType {

    private String acct;//企业ID
    private String delivercodetype;//主键
    private String pk_father;//上级主键
    private String vcode;//编码
    private String vname;//名称
    private Integer enablestate;//启用状态
    private int dr=0;//删除表示
    private String ts;//
    private String vdef;
    private String vdef2;
    private String vdef3;
    private String vdef4;
    private String vdef5;

    private Integer isfromerp;//是否来自ERP    0否1是
    private Integer isdeal;//是否已处理 0否1是
    private String parentcode;//上级code
    private String lvl;//该节点的级别

	private SupplierType parentSupplierType;
	
	private List<SupplierType> childSupplierType ;
	private List<String> pks;
	  
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_suppliertype() {
		return delivercodetype;
	}
	public void setPk_suppliertype(String delivercodetype) {
		this.delivercodetype = delivercodetype;
	}
	public String getPk_father() {
		return pk_father;
	}
	public void setPk_father(String pk_father) {
		this.pk_father = pk_father;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}

    public Integer getEnablestate() {
        return enablestate;
    }

    public void setEnablestate(Integer enablestate) {
        this.enablestate = enablestate;
    }

    public String getParentcode() {
        return parentcode;
    }

    public void setParentcode(String parentcode) {
        this.parentcode = parentcode;
    }

    public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVdef() {
		return vdef;
	}
	public void setVdef(String vdef) {
		this.vdef = vdef;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public Integer getIsfromerp() {
		return isfromerp;
	}
	public void setIsfromerp(Integer isfromerp) {
		this.isfromerp = isfromerp;
	}
	public Integer getIsdeal() {
		return isdeal;
	}
	public void setIsdeal(Integer isdeal) {
		this.isdeal = isdeal;
	}

    public String getLvl() {
        return lvl;
    }

    public void setLvl(String lvl) {
        this.lvl = lvl;
    }
	public SupplierType getParentSupplierType() {
		return parentSupplierType;
	}
	public void setParentSupplierType(SupplierType parentSupplierType) {
		this.parentSupplierType = parentSupplierType;
	}
	public List<SupplierType> getChildSupplierType() {
		return childSupplierType;
	}
	public void setChildSupplierType(List<SupplierType> childSupplierType) {
		this.childSupplierType = childSupplierType;
	}
	public List<String> getPks() {
		return pks;
	}
	public void setPks(List<String> pks) {
		this.pks = pks;
	}
}
