package com.choice.assistant.domain.supplier;

import java.util.List;

/**
 * 供应商评价
 * Created by mc on 14-10-23.
 */
public class Suppliereval {
    private String acct;
    private String delivercodeeval;
    private String delivercode;
    private List<Supplierevald> supplierevalds;
    private String devaldate;//日期
    private Double nscore;//得分
    private Integer dr;
    private String ts;
    private String vbdef;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_suppliereval() {
        return delivercodeeval;
    }

    public void setPk_suppliereval(String delivercodeeval) {
        this.delivercodeeval = delivercodeeval;
    }

    public String getPk_supplier() {
        return delivercode;
    }

    public void setPk_supplier(String delivercode) {
        this.delivercode = delivercode;
    }

    public String getDelivercode() {
		return delivercode;
	}

	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}

	public List<Supplierevald> getSupplierevalds() {
        return supplierevalds;
    }

    public void setSupplierevalds(List<Supplierevald> supplierevalds) {
        this.supplierevalds = supplierevalds;
    }

    public String getDevaldate() {
        return devaldate;
    }

    public void setDevaldate(String devaldate) {
        this.devaldate = devaldate;
    }

    public Double getNscore() {
        return nscore;
    }

    public void setNscore(Double nscore) {
        this.nscore = nscore;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVbdef() {
        return vbdef;
    }

    public void setVbdef(String vbdef) {
        this.vbdef = vbdef;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }
}
