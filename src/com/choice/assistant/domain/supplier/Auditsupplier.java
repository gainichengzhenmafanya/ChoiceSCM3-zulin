package com.choice.assistant.domain.supplier;

/**
 * 待审核供应商
 * Created by mc on 14-11-7.
 */
public class Auditsupplier {
    private String pk_auditsupplier;//
    private String delivercode;// 供应商主键
    private String acct;//
    private String vcode;//供应商编码
    private String vname;//供应商名称
    private String vlevel;//供应商级别
    private String vsuppliertele;//供应商电话
    private String vsupplieraddress;//供应商地址
    private String vmemo;// 备注
    private String vdef;//
    private String vdef2;//
    private String vdef3;//
    private String vdef4;//
    private String vdef5;//
    private String ts;//
    private String dr;//
	private String vapplydesc;//申请原因
    private String vcontact;//联系人
    private String vcompanyfax;//传真
    private String vcompanyurl;//网址
    private String vcompanyemail;//邮箱

    public String getPk_auditsupplier() {
        return pk_auditsupplier;
    }

    public void setPk_auditsupplier(String pk_auditsupplier) {
        this.pk_auditsupplier = pk_auditsupplier;
    }

    public String getPk_supplier() {
        return delivercode;
    }

    public void setPk_supplier(String delivercode) {
        this.delivercode = delivercode;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getVlevel() {
        return vlevel;
    }

    public void setVlevel(String vlevel) {
        this.vlevel = vlevel;
    }

    public String getVsuppliertele() {
        return vsuppliertele;
    }

    public void setVsuppliertele(String vsuppliertele) {
        this.vsuppliertele = vsuppliertele;
    }

    public String getVsupplieraddress() {
        return vsupplieraddress;
    }

    public void setVsupplieraddress(String vsupplieraddress) {
        this.vsupplieraddress = vsupplieraddress;
    }

    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public String getVdef() {
        return vdef;
    }

    public void setVdef(String vdef) {
        this.vdef = vdef;
    }

    public String getVdef2() {
        return vdef2;
    }

    public void setVdef2(String vdef2) {
        this.vdef2 = vdef2;
    }

    public String getVdef3() {
        return vdef3;
    }

    public void setVdef3(String vdef3) {
        this.vdef3 = vdef3;
    }

    public String getVdef4() {
        return vdef4;
    }

    public void setVdef4(String vdef4) {
        this.vdef4 = vdef4;
    }

    public String getVdef5() {
        return vdef5;
    }

    public void setVdef5(String vdef5) {
        this.vdef5 = vdef5;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getDr() {
        return dr;
    }

    public void setDr(String dr) {
        this.dr = dr;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

	public String getVapplydesc() {
		return vapplydesc;
	}

	public void setVapplydesc(String vapplydesc) {
		this.vapplydesc = vapplydesc;
	}

    public String getVcontact() {
        return vcontact;
    }

    public void setVcontact(String vcontact) {
        this.vcontact = vcontact;
    }

	public String getVcompanyfax() {
		return vcompanyfax;
	}

	public void setVcompanyfax(String vcompanyfax) {
		this.vcompanyfax = vcompanyfax;
	}

	public String getVcompanyurl() {
		return vcompanyurl;
	}

	public void setVcompanyurl(String vcompanyurl) {
		this.vcompanyurl = vcompanyurl;
	}

	public String getVcompanyemail() {
		return vcompanyemail;
	}

	public void setVcompanyemail(String vcompanyemail) {
		this.vcompanyemail = vcompanyemail;
	}
}
