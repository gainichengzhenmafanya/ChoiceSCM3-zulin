package com.choice.assistant.domain.supplier;

import java.util.List;

public class SupplierTO {
	private String acct;//企业I
	private String pk_enlist;//供应商应募主键
	private String pk_recruit;//招募书主键
	private String pk_supplier;//供应商主键
	private String delivercode;//供货商编号
	private String delivername;//供货商名称
	private String vsuppliercontact;//联系人
	private String vsupplierphone;//联系人电话
	private String vsupplieraddress;//地址
	private Integer istate;//状态
	private String dr;//dr
	private String ts;//ts
	private String positncode;//供应商所属组织
	private String positncode_f;//供应商财务组织
	private List<String> pk_enlists;//主键集合
	
	public String getPk_supplier() {
		return pk_supplier;
	}
	public void setPk_supplier(String pk_supplier) {
		this.pk_supplier = pk_supplier;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public String getPositncode_f() {
		return positncode_f;
	}
	public void setPositncode_f(String positncode_f) {
		this.positncode_f = positncode_f;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public List<String> getPk_enlists() {
		return pk_enlists;
	}
	public void setPk_enlists(List<String> pk_enlists) {
		this.pk_enlists = pk_enlists;
	}
	public String getPk_org() {
		return positncode;
	}
	public void setPk_org(String positncode) {
		this.positncode = positncode;
	}
	public String getPk_org_f() {
		return positncode_f;
	}
	public void setPk_org_f(String positncode_f) {
		this.positncode_f = positncode_f;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_enlist() {
		return pk_enlist;
	}
	public void setPk_enlist(String pk_enlist) {
		this.pk_enlist = pk_enlist;
	}
	public String getPk_recruit() {
		return pk_recruit;
	}
	public void setPk_recruit(String pk_recruit) {
		this.pk_recruit = pk_recruit;
	}
	public String getDelivername() {
		return delivername;
	}
	public String getVsuppliercontact() {
		return vsuppliercontact;
	}
	public void setVsuppliercontact(String vsuppliercontact) {
		this.vsuppliercontact = vsuppliercontact;
	}
	public String getVsupplierphone() {
		return vsupplierphone;
	}
	public void setVsupplierphone(String vsupplierphone) {
		this.vsupplierphone = vsupplierphone;
	}
	public String getVsupplieraddress() {
		return vsupplieraddress;
	}
	public void setVsupplieraddress(String vsupplieraddress) {
		this.vsupplieraddress = vsupplieraddress;
	}
	public Integer getIstate() {
		return istate;
	}
	public void setIstate(Integer istate) {
		this.istate = istate;
	}
	public String getDr() {
		return dr;
	}
	public void setDr(String dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	
}
