package com.choice.assistant.domain.supplier;

import com.choice.framework.domain.system.Department;

/**
 * 适用门店
 * Created by mc on 14-10-22.
 */
public class SupplierOrg {
    public String delivercodeOrg;
    public String delivercode;
    public Department positncode;
    public String acct;
    public Integer dr=0;
    public String ts;
    public String vbdef;
    public String vbdef2;
    public String vbdef3;
    public String vbdef4;
    public String vbdef5;

    public String getPk_supplierOrg() {
        return delivercodeOrg;
    }

    public void setPk_supplierOrg(String delivercodeOrg) {
        this.delivercodeOrg = delivercodeOrg;
    }

    public String getPk_supplier() {
        return delivercode;
    }

    public void setPk_supplier(String delivercode) {
        this.delivercode = delivercode;
    }

    public Department getPk_org() {
        return positncode;
    }

    public void setPk_org(Department positncode) {
        this.positncode = positncode;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVbdef() {
        return vbdef;
    }

    public void setVbdef(String vbdef) {
        this.vbdef = vbdef;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }
}
