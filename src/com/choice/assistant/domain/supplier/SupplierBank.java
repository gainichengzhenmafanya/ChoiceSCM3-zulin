package com.choice.assistant.domain.supplier;

/**
 * 供应商银行表
 */
public class SupplierBank {
    private String delivercodebank;
    private String delivercode;
    private String vbankname;
    private String vbanknumber;
    private String vaccountname;
    private String acct;
    private Integer dr=0;
    private String ts;
    private String vbdef;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;

    public String getPk_supplierbank() {
        return delivercodebank;
    }

    public void setPk_supplierbank(String delivercodebank) {
        this.delivercodebank = delivercodebank;
    }

    public String getPk_supplier() {
        return delivercode;
    }

    public void setPk_supplier(String delivercode) {
        this.delivercode = delivercode;
    }

    public String getVbankname() {
        return vbankname;
    }

    public void setVbankname(String vbankname) {
        this.vbankname = vbankname;
    }

    public String getVbanknumber() {
        return vbanknumber;
    }

    public void setVbanknumber(String vbanknumber) {
        this.vbanknumber = vbanknumber;
    }

    public String getVaccountname() {
        return vaccountname;
    }

    public void setVaccountname(String vaccountname) {
        this.vaccountname = vaccountname;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVbdef() {
        return vbdef;
    }

    public void setVbdef(String vbdef) {
        this.vbdef = vbdef;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }
}
