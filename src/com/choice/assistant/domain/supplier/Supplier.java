package com.choice.assistant.domain.supplier;

import java.util.List;

import com.choice.assistant.domain.material.MaterialMall;

/**
 * 供应商信息
 * @author wangchao
 *
 */
public class Supplier {
	private String acct;//企业ID
	private String delivercode;//供应商主键
	private String vcode;//编码
	private String vname;//名称
	private SupplierType delivercodetype;//供应商类别
	private String suppliertype;//类别主键
	private String vcontact;//联系人
	private String vtele;//电话
	private Integer enablestate;//启用状态
	private String delivercodejum;//商城供应商主键
	private String vcodejmu;//商城供应商编码
	private String vnamejmu;//商城供应商名称
	private String pk_finan;//供应商财务组织
	private String vinit;//供应商助记码
	private Integer iaccttype;//账期类型 0 即时,1 一月,2 二月,3 季度,4 半年,5 一年
	private Integer iacctdate;//账单日
	private String vlegal;//法人
	private String vaddr;//地址
	private String vzipcode;//邮政编码
	private String vmailaddr;//邮箱
	private String vwebsite;//网站
	private String vfax;//传真
	private String varea;//地区
	private String vtaxno;//税号
	private Double nsignmonty;//注册资金
	private Integer bisnormal;//是否一般纳锐人
	private Integer bistax;//是否开发票
	private String vmemo;//备注
	private Integer dr=0;//删除标识
	private String ts;//时间戳
    private List<MaterialScope> materialScope;//供应物资范围
    private List<SupplierBank> supplierBank;//银行卡
    private List<SupplierOrg> supplierOrg;//适用门店
    private List<Suppliereval> suppliereval;//供应商评价
	private String vdef1;//自定义项1
	private String vdef2;//自定义项2
	private String vdef3;//自定义项3
	private String vdef4;//自定义项4
	private String vdef5;//自定义项5
	private String verppk;//ERP主键
	
	private Integer isfromerp;//是否来自ERP    0否1是
	private Integer isdeal;//是否已处理 0否1是
	private Integer sta;//当前状态 1新增2修改3删除
	private String vhstore;//商城默认商铺id-商城接口返回
	private Integer begin;
	private Integer end;
	private List<MaterialMall> listMaterialMall;//关联商城物资
	private String typename;
	private String pk_material;	//供应商供应物资范围物资主键
	private String pk_purtemplet;//采购模板主键
	private String suppliertypename;//类别名称
	
	private String nojmu;//查询未关联商城供应商

	
	public List<MaterialMall> getListMaterialMall() {
		return listMaterialMall;
	}

	public void setListMaterialMall(List<MaterialMall> listMaterialMall) {
		this.listMaterialMall = listMaterialMall;
	}

	public String getSuppliertype() {
		return suppliertype;
	}
	
	public void setSuppliertype(String suppliertype) {
		this.suppliertype = suppliertype;
	}
    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_supplier() {
        return delivercode;
    }

    public void setPk_supplier(String delivercode) {
        this.delivercode = delivercode;
    }

    public String getDelivercode() {
		return delivercode;
	}

	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}

	public SupplierType getDelivercodetype() {
		return delivercodetype;
	}

	public void setDelivercodetype(SupplierType delivercodetype) {
		this.delivercodetype = delivercodetype;
	}

	public String getDelivercodejum() {
		return delivercodejum;
	}

	public void setDelivercodejum(String delivercodejum) {
		this.delivercodejum = delivercodejum;
	}

	public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public SupplierType getPk_suppliertype() {
        return delivercodetype;
    }

    public void setPk_suppliertype(SupplierType delivercodetype) {
        this.delivercodetype = delivercodetype;
    }

    public String getVcontact() {
        return vcontact;
    }

    public void setVcontact(String vcontact) {
        this.vcontact = vcontact;
    }

    public String getVtele() {
        return vtele;
    }

    public void setVtele(String vtele) {
        this.vtele = vtele;
    }

    public Integer getEnablestate() {
        return enablestate;
    }

    public void setEnablestate(Integer enablestate) {
        this.enablestate = enablestate;
    }

    public String getPk_supplierjum() {
        return delivercodejum;
    }

    public void setPk_supplierjum(String delivercodejum) {
        this.delivercodejum = delivercodejum;
    }

    public String getVcodejmu() {
        return vcodejmu;
    }

    public void setVcodejmu(String vcodejmu) {
        this.vcodejmu = vcodejmu;
    }

    public String getVnamejmu() {
        return vnamejmu;
    }

    public void setVnamejmu(String vnamejmu) {
        this.vnamejmu = vnamejmu;
    }

    public String getPk_finan() {
        return pk_finan;
    }

    public void setPk_finan(String pk_finan) {
        this.pk_finan = pk_finan;
    }

    public String getVinit() {
        return vinit;
    }

    public void setVinit(String vinit) {
        this.vinit = vinit;
    }

    public Integer getIaccttype() {
        return iaccttype;
    }

    public void setIaccttype(Integer iaccttype) {
        this.iaccttype = iaccttype;
    }

    public Integer getIacctdate() {
        return iacctdate;
    }

    public void setIacctdate(Integer iacctdate) {
        this.iacctdate = iacctdate;
    }

    public String getVlegal() {
        return vlegal;
    }

    public void setVlegal(String vlegal) {
        this.vlegal = vlegal;
    }

    public String getVaddr() {
        return vaddr;
    }

    public void setVaddr(String vaddr) {
        this.vaddr = vaddr;
    }

    public String getVzipcode() {
        return vzipcode;
    }

    public void setVzipcode(String vzipcode) {
        this.vzipcode = vzipcode;
    }

    public String getVmailaddr() {
        return vmailaddr;
    }

    public void setVmailaddr(String vmailaddr) {
        this.vmailaddr = vmailaddr;
    }

    public String getVwebsite() {
        return vwebsite;
    }

    public void setVwebsite(String vwebsite) {
        this.vwebsite = vwebsite;
    }

    public String getVfax() {
        return vfax;
    }

    public void setVfax(String vfax) {
        this.vfax = vfax;
    }

    public String getVarea() {
        return varea;
    }

    public void setVarea(String varea) {
        this.varea = varea;
    }

    public String getVtaxno() {
        return vtaxno;
    }

    public void setVtaxno(String vtaxno) {
        this.vtaxno = vtaxno;
    }

    public Double getNsignmonty() {
        return nsignmonty;
    }

    public void setNsignmonty(Double nsignmonty) {
        this.nsignmonty = nsignmonty;
    }

    public Integer getBisnormal() {
        return bisnormal;
    }

    public void setBisnormal(Integer bisnormal) {
        this.bisnormal = bisnormal;
    }

    public Integer getBistax() {
        return bistax;
    }

    public void setBistax(Integer bistax) {
        this.bistax = bistax;
    }

    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public List<MaterialScope> getMaterialScope() {
        return materialScope;
    }

    public void setMaterialScope(List<MaterialScope> materialScope) {
        this.materialScope = materialScope;
    }

    public List<SupplierBank> getSupplierBank() {
        return supplierBank;
    }

    public void setSupplierBank(List<SupplierBank> supplierBank) {
        this.supplierBank = supplierBank;
    }

    public List<SupplierOrg> getSupplierOrg() {
        return supplierOrg;
    }

    public void setSupplierOrg(List<SupplierOrg> supplierOrg) {
        this.supplierOrg = supplierOrg;
    }

    public List<Suppliereval> getSuppliereval() {
        return suppliereval;
    }

    public void setSuppliereval(List<Suppliereval> suppliereval) {
        this.suppliereval = suppliereval;
    }

    public String getVdef1() {
        return vdef1;
    }

    public void setVdef1(String vdef1) {
        this.vdef1 = vdef1;
    }

    public String getVdef2() {
        return vdef2;
    }

    public void setVdef2(String vdef2) {
        this.vdef2 = vdef2;
    }

    public String getVdef3() {
        return vdef3;
    }

    public void setVdef3(String vdef3) {
        this.vdef3 = vdef3;
    }

    public String getVdef4() {
        return vdef4;
    }

    public void setVdef4(String vdef4) {
        this.vdef4 = vdef4;
    }

    public String getVdef5() {
        return vdef5;
    }

    public void setVdef5(String vdef5) {
        this.vdef5 = vdef5;
    }

	public String getVerppk() {
		return verppk;
	}

	public void setVerppk(String verppk) {
		this.verppk = verppk;
	}

	public Integer getIsfromerp() {
		return isfromerp;
	}

	public void setIsfromerp(Integer isfromerp) {
		this.isfromerp = isfromerp;
	}

	public Integer getIsdeal() {
		return isdeal;
	}

	public void setIsdeal(Integer isdeal) {
		this.isdeal = isdeal;
	}

	public Integer getSta() {
		return sta;
	}

	public void setSta(Integer sta) {
		this.sta = sta;
	}

	public String getVhstore() {
		return vhstore;
	}

	public void setVhstore(String vhstore) {
		this.vhstore = vhstore;
	}

	public Integer getBegin() {
		return begin;
	}

	public void setBegin(Integer begin) {
		this.begin = begin;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public String getPk_material() {
		return pk_material;
	}

	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}

	public String getPk_purtemplet() {
		return pk_purtemplet;
	}

	public void setPk_purtemplet(String pk_purtemplet) {
		this.pk_purtemplet = pk_purtemplet;
	}

	public String getSuppliertypename() {
		return suppliertypename;
	}

	public void setSuppliertypename(String suppliertypename) {
		this.suppliertypename = suppliertypename;
	}

	public String getNojmu() {
		return nojmu;
	}

	public void setNojmu(String nojmu) {
		this.nojmu = nojmu;
	}
}
