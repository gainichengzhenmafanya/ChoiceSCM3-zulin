package com.choice.assistant.domain.supplier;

import java.util.List;

import com.choice.assistant.domain.system.Unit;
import com.choice.assistant.domain.util.ManagerName;

/**
 * 合约报价
 * Created by mc on 14-10-24.
 */
@ManagerName(value = "合约报价")
public class ContractQuote {
    private String pk_contractquote;//合约报价主键
    private String pk_purcontract;//采购合约主表主键
    private Integer iquotebatch;//报价批次
    private String pk_material;//物资主键
    private String vmaterialcode;//物资编码
    private String vmaterialname;//物资名称
    private Unit pk_unit;//单位
    private String vbrand;//品牌
    private String vmaterialspec;//规格
    private Double nlastprice;//上次价格
    private Double nthisprice;//物资价格
    private Double nstartamount;//开始数量
    private Double nendamount;//截止数量
    private String dstartdat;//报价开始时间
    private String denddat;//报价结束时间
    private Integer enablestate;//启用状态 1 待审核 2已审核 3停用
    private String vmemo;//备注
    private Integer dr=0;
    private String ts;
    private String vbdef1;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;
    private String acct;
    private List<Suitstore> suitstoreList;

    public String getPk_contractquote() {
        return pk_contractquote;
    }

    public void setPk_contractquote(String pk_contractquote) {
        this.pk_contractquote = pk_contractquote;
    }

    public String getPk_purcontract() {
        return pk_purcontract;
    }

    public void setPk_purcontract(String pk_purcontract) {
        this.pk_purcontract = pk_purcontract;
    }

    @ManagerName(value = "合约批次",isConfirm = false)
    public Integer getIquotebatch() {
        return iquotebatch;
    }

    public void setIquotebatch(Integer iquotebatch) {
        this.iquotebatch = iquotebatch;
    }

    public String getPk_material() {
        return pk_material;
    }

    public void setPk_material(String pk_material) {
        this.pk_material = pk_material;
    }

    public String getVmaterialcode() {
        return vmaterialcode;
    }

    public void setVmaterialcode(String vmaterialcode) {
        this.vmaterialcode = vmaterialcode;
    }

    @ManagerName(value = "物资名称",isConfirm = false)
    public String getVmaterialname() {
        return vmaterialname;
    }

    public void setVmaterialname(String vmaterialname) {
        this.vmaterialname = vmaterialname;
    }

    public Unit getPk_unit() {
        return pk_unit;
    }

    public void setPk_unit(Unit pk_unit) {
        this.pk_unit = pk_unit;
    }

    public String getVbrand() {
        return vbrand;
    }

    public void setVbrand(String vbrand) {
        this.vbrand = vbrand;
    }

    public String getVmaterialspec() {
        return vmaterialspec;
    }

    public void setVmaterialspec(String vmaterialspec) {
        this.vmaterialspec = vmaterialspec;
    }

    public Double getNlastprice() {
        return nlastprice;
    }

    public void setNlastprice(Double nlastprice) {
        this.nlastprice = nlastprice;
    }

    @ManagerName(value = "物资价格")
    public Double getNthisprice() {
        return nthisprice;
    }

    public void setNthisprice(Double nthisprice) {
        this.nthisprice = nthisprice;
    }

    public Double getNstartamount() {
        return nstartamount;
    }

    public void setNstartamount(Double nstartamount) {
        this.nstartamount = nstartamount;
    }

    public Double getNendamount() {
        return nendamount;
    }

    public void setNendamount(Double nendamount) {
        this.nendamount = nendamount;
    }

    @ManagerName(value = "报价开始时间")
    public String getDstartdat() {
        return dstartdat;
    }

    public void setDstartdat(String dstartdat) {
        this.dstartdat = dstartdat;
    }
    @ManagerName(value = "报价结束时间")
    public String getDenddat() {
        return denddat;
    }

    public void setDenddat(String denddat) {
        this.denddat = denddat;
    }
    @ManagerName(value = "启用状态",isDB = true,enumName = "E")
    public Integer getEnablestate() {
        return enablestate;
    }

    public void setEnablestate(Integer enablestate) {
        this.enablestate = enablestate;
    }

    @ManagerName(value = "备注")
    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVbdef1() {
        return vbdef1;
    }

    public void setVbdef1(String vbdef1) {
        this.vbdef1 = vbdef1;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    @ManagerName(value = "适用门店")
    public List<Suitstore> getSuitstoreList() {
        return suitstoreList;
    }

    public void setSuitstoreList(List<Suitstore> suitstoreList) {
        this.suitstoreList = suitstoreList;
    }
}
