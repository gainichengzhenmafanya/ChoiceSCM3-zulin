package com.choice.assistant.domain.supplier;

import com.choice.assistant.domain.util.ManagerName;
import com.choice.scm.domain.Positn;

/**
 * 适用门店
 * Created by mc on 14-10-24.
 */
@ManagerName(value = "适用门店")
public class Suitstore {
    private String pk_purcontract;//采购合约主表主键
    private int iquotebatch;//合约报价批次
    private Positn positn;//适用门店
    private Integer dr;
    private String ts;
    private String acct;
    private String sp_code;

	public String getPk_purcontract() {
        return pk_purcontract;
    }

    public void setPk_purcontract(String pk_purcontract) {
        this.pk_purcontract = pk_purcontract;
    }

	public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

	public int getIquotebatch() {
		return iquotebatch;
	}

	public void setIquotebatch(int iquotebatch) {
		this.iquotebatch = iquotebatch;
	}

	public Positn getPositn() {
		return positn;
	}

	public void setPositn(Positn positn) {
		this.positn = positn;
	}

	public String getSp_code() {
		return sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
}
