package com.choice.assistant.domain.supplier;
/**
 * 供应商招募主表
 * @author zhb
 *
 */
public class SupplierRM {
	 private String acct;//企业ID
	 private String pk_recruit;//招募书主键
	 private String vcode; //编码
	 private String vname;//名称
	 private String vrmcategory;//招募品类
	 private String dreleasedat;//发布日期
	 private String denddat;//报名截止日期
	 private Integer istate;//状态 3-未开始 1-进行中 2-已完成
	 private Integer dr;//dr
	 private String ts;//ts
	 private Double vsregmoney;//注册资金
	 private String vindustry;//所属行业
	 private String vreqdescription;//需求描述
	 private Double vyearordrmoney;//年计划采购金额
	 private String vupfile;//上传附件
	 private String vmemo;//备注
	 private Integer ijyfs;
	 private Integer ifpyq;
	 private Integer ireg;
	 private String vmallid;
	 
	 private String msgStr;//返回信息临时存储字段
	
	
	public Integer getIreg() {
		return ireg;
	}
	public void setIreg(Integer ireg) {
		this.ireg = ireg;
	}
	public Integer getIjyfs() {
		return ijyfs;
	}
	public void setIjyfs(Integer ijyfs) {
		this.ijyfs = ijyfs;
	}
	public Integer getIfpyq() {
		return ifpyq;
	}
	public void setIfpyq(Integer ifpyq) {
		this.ifpyq = ifpyq;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVupfile() {
		return vupfile;
	}
	public void setVupfile(String vupfile) {
		this.vupfile = vupfile;
	}
	public String getVreqdescription() {
		return vreqdescription;
	}
	public void setVreqdescription(String vreqdescription) {
		this.vreqdescription = vreqdescription;
	}
	
	public String getVindustry() {
		return vindustry;
	}
	public void setVindustry(String vindustry) {
		this.vindustry = vindustry;
	}
	
	public String getVrmcategory() {
		return vrmcategory;
	}
	public void setVrmcategory(String vrmcategory) {
		this.vrmcategory = vrmcategory;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_recruit() {
		return pk_recruit;
	}
	public void setPk_recruit(String pk_recruit) {
		this.pk_recruit = pk_recruit;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getDreleasedat() {
		return dreleasedat;
	}
	public void setDreleasedat(String dreleasedat) {
		this.dreleasedat = dreleasedat;
	}
	public String getDenddat() {
		return denddat;
	}
	public void setDenddat(String denddat) {
		this.denddat = denddat;
	}
	public Integer getIstate() {
		return istate;
	}
	public void setIstate(Integer istate) {
		this.istate = istate;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public Double getVsregmoney() {
		return vsregmoney;
	}
	public void setVsregmoney(Double vsregmoney) {
		this.vsregmoney = vsregmoney;
	}
	public Double getVyearordrmoney() {
		return vyearordrmoney;
	}
	public void setVyearordrmoney(Double vyearordrmoney) {
		this.vyearordrmoney = vyearordrmoney;
	}
	public String getVmallid() {
		return vmallid;
	}
	public void setVmallid(String vmallid) {
		this.vmallid = vmallid;
	}
	public String getMsgStr() {
		return msgStr;
	}
	public void setMsgStr(String msgStr) {
		this.msgStr = msgStr;
	}
	 
}
