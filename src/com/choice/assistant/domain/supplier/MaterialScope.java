package com.choice.assistant.domain.supplier;

import java.util.List;

import com.choice.assistant.domain.material.MaterialMall;

/**
 * 供应物资范围
 * Created by mc on 14-10-22.
 */
public class MaterialScope {
    private String delivercode;     //供应商主键
    private String delivername;     //供应商主键
    private String pk_materialscope;//供应物资主键
    private String pk_material; //物资主键
    private String sp_code;           //物资编码
    private String sp_name ;          //物资名称
    private String pk_typ; //物资类别
    private String unit;         //物资单位
    private String acct;        //
    private String sp_desc ;      //物资规格
    private Double sp_price;          //标准价
    private String typ;       //物资分类编码
    private String typdes ;      //物资分类名称
    private Integer dr=0;            //
    private String ts ;             //
    private String vbdef ;          //
    private String vbdef2;          //
    private String vbdef3;          //
    private String vbdef4 ;         //
    private String vbdef5 ;         //
    private Integer isfromerp;//是否来自ERP    0否1是
    private Integer isdeal;//是否已处理 0否1是
    private List<String> listpk_materialscope;
    
	private String pk_materialjmu;//商城内主键
	private String vcodejmu;//商城内编码
	private String vnamejmu;//商城内名称
	private String vhspec;//商城规格id-接口返回
	private String vhstore;//店铺主键

	private String positncode;//采购组织编码	
	
	public String getDelivername() {
		return delivername;
	}

	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}

	public String getPositncode() {
		return positncode;
	}

	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}

	private Double tax; 
	private String taxdes; 

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public String getTaxdes() {
		return taxdes;
	}

	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}

	public String getPk_materialjmu() {
		return pk_materialjmu;
	}

	public void setPk_materialjmu(String pk_materialjmu) {
		this.pk_materialjmu = pk_materialjmu;
	}

	public String getVcodejmu() {
		return vcodejmu;
	}

	public void setVcodejmu(String vcodejmu) {
		this.vcodejmu = vcodejmu;
	}

	public String getVnamejmu() {
		return vnamejmu;
	}

	public void setVnamejmu(String vnamejmu) {
		this.vnamejmu = vnamejmu;
	}

	public String getVhspec() {
		return vhspec;
	}

	public void setVhspec(String vhspec) {
		this.vhspec = vhspec;
	}

	public List<String> getListpk_materialscope() {
		return listpk_materialscope;
	}

	public void setListpk_materialscope(List<String> listpk_materialscope) {
		this.listpk_materialscope = listpk_materialscope;
	}

	private MaterialMall materialMall;

	public String getDelivercode() {
		return delivercode;
	}

	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}

	public String getPk_materialscope() {
		return pk_materialscope;
	}

	public void setPk_materialscope(String pk_materialscope) {
		this.pk_materialscope = pk_materialscope;
	}

	public String getPk_material() {
		return pk_material;
	}

	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}

	public String getSp_code() {
		return sp_code;
	}

	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}

	public String getSp_name() {
		return sp_name;
	}

	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}

	public String getPk_typ() {
		return pk_typ;
	}

	public void setPk_typ(String pk_typ) {
		this.pk_typ = pk_typ;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public String getSp_desc() {
		return sp_desc;
	}

	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}

	public Double getSp_price() {
		return sp_price;
	}

	public void setSp_price(Double sp_price) {
		this.sp_price = sp_price;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getTypdes() {
		return typdes;
	}

	public void setTypdes(String typdes) {
		this.typdes = typdes;
	}

	public Integer getDr() {
		return dr;
	}

	public void setDr(Integer dr) {
		this.dr = dr;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getVbdef() {
		return vbdef;
	}

	public void setVbdef(String vbdef) {
		this.vbdef = vbdef;
	}

	public String getVbdef2() {
		return vbdef2;
	}

	public void setVbdef2(String vbdef2) {
		this.vbdef2 = vbdef2;
	}

	public String getVbdef3() {
		return vbdef3;
	}

	public void setVbdef3(String vbdef3) {
		this.vbdef3 = vbdef3;
	}

	public String getVbdef4() {
		return vbdef4;
	}

	public void setVbdef4(String vbdef4) {
		this.vbdef4 = vbdef4;
	}

	public String getVbdef5() {
		return vbdef5;
	}

	public void setVbdef5(String vbdef5) {
		this.vbdef5 = vbdef5;
	}

	public Integer getIsfromerp() {
		return isfromerp;
	}

	public void setIsfromerp(Integer isfromerp) {
		this.isfromerp = isfromerp;
	}

	public Integer getIsdeal() {
		return isdeal;
	}

	public void setIsdeal(Integer isdeal) {
		this.isdeal = isdeal;
	}

	public MaterialMall getMaterialMall() {
		return materialMall;
	}

	public void setMaterialMall(MaterialMall materialMall) {
		this.materialMall = materialMall;
	}

	public String getVhstore() {
		return vhstore;
	}

	public void setVhstore(String vhstore) {
		this.vhstore = vhstore;
	}
    
}
