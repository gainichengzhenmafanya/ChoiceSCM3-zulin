package com.choice.assistant.domain;

import com.choice.assistant.util.NoUse;
import com.choice.orientationSys.util.Page;

public class Condition {

	public String vcode;
	public String vname;
	public String vinit;
	public String querySnAst;//助手内订单编码
	@NoUse
	public Page pager;	//分页对象
	public String id;
	public String vdelivername;//商城供应商名称
	public String bdat;//开始日期
	public String edat;//结束日期
	public String vflag;	//标识
	
	
	public String getVdelivername() {
		return vdelivername;
	}
	public void setVdelivername(String vdelivername) {
		this.vdelivername = vdelivername;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getQuerySnAst() {
		return querySnAst;
	}
	public void setQuerySnAst(String querySnAst) {
		this.querySnAst = querySnAst;
	}
	public Page getPager() {
		return pager;
	}
	public void setPager(Page pager) {
		this.pager = pager;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBdat() {
		return bdat;
	}
	public void setBdat(String bdat) {
		this.bdat = bdat;
	}
	public String getEdat() {
		return edat;
	}
	public void setEdat(String edat) {
		this.edat = edat;
	}
	public String getVflag() {
		return vflag;
	}
	public void setVflag(String vflag) {
		this.vflag = vflag;
	}
	
}
