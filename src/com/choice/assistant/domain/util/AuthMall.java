package com.choice.assistant.domain.util;


/**
 * 商城登录通用安全模块实体类
 * @author ZGL
 *
 */
public class AuthMall {

	private String token;			//平台Salt + sessionId + crtTime 拼接后，经MD5加密的32位字符串
	private String sessionId;		//
	private String userId;			//用户编号
	private String platformId="4";	//平台编号4（固定值）
	private String crtTime;			//当前时间戳
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getCrtTime() {
		return crtTime;
	}
	public void setCrtTime(String crtTime) {
		this.crtTime = crtTime;
	}
	
}
