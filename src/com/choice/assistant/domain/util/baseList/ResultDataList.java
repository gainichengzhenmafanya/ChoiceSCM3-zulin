package com.choice.assistant.domain.util.baseList;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.util.PageCondition;

/**
 * 商城接口返回的结果对象
 * @author ZGL
 *
 */
public class ResultDataList {
	private List<Map<String,Object>> result;					//结果对象数组[{},{},...]																											
	private PageCondition pageCondition;			//分页对象
	
	public List<Map<String, Object>> getResult() {
		return result;
	}
	public void setResult(List<Map<String, Object>> result) {
		this.result = result;
	}
	public PageCondition getPageCondition() {
		return pageCondition;
	}
	public void setPageCondition(PageCondition pageCondition) {
		this.pageCondition = pageCondition;
	}
	
	
}
