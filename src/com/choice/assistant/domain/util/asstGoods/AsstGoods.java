package com.choice.assistant.domain.util.asstGoods;

public class AsstGoods {
	private String id;			//商城主键
	private String SN;			//编码
	private String name;		//名称
	private String unit;		//单位
	private String defPrice;	//默认价格-市场价
	private String price;		//规格价格
	private AsstStore store;	//供应商
	private String least;		//起购量
	private String volume;		//销量
	private String image;		//图片地址
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSN() {
		return SN;
	}
	public void setSN(String sN) {
		SN = sN;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getDefPrice() {
		return defPrice;
	}
	public void setDefPrice(String defPrice) {
		this.defPrice = defPrice;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public AsstStore getStore() {
		return store;
	}
	public void setStore(AsstStore store) {
		this.store = store;
	}
	public String getLeast() {
		return least;
	}
	public void setLeast(String least) {
		this.least = least;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}		
	
}
