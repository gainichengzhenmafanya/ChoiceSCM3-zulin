package com.choice.assistant.domain.util.asstGoods;

import java.util.Map;

public class AsstStore {

	private String id;
	private String name;
	private Map<String,Object> seller;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, Object> getSeller() {
		return seller;
	}
	public void setSeller(Map<String, Object> seller) {
		this.seller = seller;
	}
	
}
