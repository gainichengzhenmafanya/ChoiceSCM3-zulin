package com.choice.assistant.domain.util.asstGoods;

import java.util.List;

import com.choice.assistant.domain.util.PageCondition;

/**
 * 商城接口返回的结果对象
 * @author ZGL
 *
 */
public class ResultDataGoods {
	private List<AsstGoods> result;					//结果对象数组[{},{},...]																											
	private PageCondition pageCondition;			//分页对象
	
	public List<AsstGoods> getResult() {
		return result;
	}
	public void setResult(List<AsstGoods> result) {
		this.result = result;
	}
	public PageCondition getPageCondition() {
		return pageCondition;
	}
	public void setPageCondition(PageCondition pageCondition) {
		this.pageCondition = pageCondition;
	}
	
}
