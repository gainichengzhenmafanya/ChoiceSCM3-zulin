package com.choice.assistant.domain.util.asstGoods;

import java.util.Map;

public class AsstHspec {

	private String id;			//商城规格主键
	private String unit;		//规格单位
	private String price;		//规格价格
	private String stock;		//规格库存
	private String spec1;		//规格规格属性1
	private String value1;		//规格规格值1
	private String spec2;		//规格规格属性2
	private String value2;		//规格规格值2
	private String description;	//规格描述
	private String image;		//图片
	private Map<String,Object> goods;		//商品
	private Map<String,Object> store;		//供应商
	private Map<String,Object> seller;		//店铺
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getSpec1() {
		return spec1;
	}
	public void setSpec1(String spec1) {
		this.spec1 = spec1;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getSpec2() {
		return spec2;
	}
	public void setSpec2(String spec2) {
		this.spec2 = spec2;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Map<String, Object> getGoods() {
		return goods;
	}
	public void setGoods(Map<String, Object> goods) {
		this.goods = goods;
	}
	public Map<String, Object> getStore() {
		return store;
	}
	public void setStore(Map<String, Object> store) {
		this.store = store;
	}
	public Map<String, Object> getSeller() {
		return seller;
	}
	public void setSeller(Map<String, Object> seller) {
		this.seller = seller;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
}
