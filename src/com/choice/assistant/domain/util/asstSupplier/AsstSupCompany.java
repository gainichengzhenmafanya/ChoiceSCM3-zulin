package com.choice.assistant.domain.util.asstSupplier;

import java.util.Map;

public class AsstSupCompany {
	private String id;
	private String isAudit;
	private String name;
	private Map<String,Object> company;
	private String grade;
	private String auditment;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsAudit() {
		return isAudit;
	}
	public void setIsAudit(String isAudit) {
		this.isAudit = isAudit;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, Object> getCompany() {
		return company;
	}
	public void setCompany(Map<String, Object> company) {
		this.company = company;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getAuditment() {
		return auditment;
	}
	public void setAuditment(String auditment) {
		this.auditment = auditment;
	}
	
	
	
}
