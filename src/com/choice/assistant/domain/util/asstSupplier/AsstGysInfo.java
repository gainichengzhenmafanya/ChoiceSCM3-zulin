package com.choice.assistant.domain.util.asstSupplier;

import java.util.List;
import java.util.Map;

public class AsstGysInfo {
	
	private String id;
	private String isAudit;
	private String transactionCount;
	private String name;
	private Map<String,Object> company;
	private List<Map<String,Object>> qualify;
	private String grade;
	private String transactionMoney;
	private String auditment;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsAudit() {
		return isAudit;
	}
	public void setIsAudit(String isAudit) {
		this.isAudit = isAudit;
	}
	public String getTransactionCount() {
		return transactionCount;
	}
	public void setTransactionCount(String transactionCount) {
		this.transactionCount = transactionCount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, Object> getCompany() {
		return company;
	}
	public void setCompany(Map<String, Object> company) {
		this.company = company;
	}
	public List<Map<String, Object>> getQualify() {
		return qualify;
	}
	public void setQualify(List<Map<String, Object>> qualify) {
		this.qualify = qualify;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getTransactionMoney() {
		return transactionMoney;
	}
	public void setTransactionMoney(String transactionMoney) {
		this.transactionMoney = transactionMoney;
	}
	public String getAuditment() {
		return auditment;
	}
	public void setAuditment(String auditment) {
		this.auditment = auditment;
	}
}
