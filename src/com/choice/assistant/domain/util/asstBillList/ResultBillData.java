package com.choice.assistant.domain.util.asstBillList;

import java.util.List;

import com.choice.assistant.domain.util.PageCondition;
/**
 * 订单列表主类
 * @author ZGL
 *
 */
public class ResultBillData {

	private List<AssitBillResult> result;			//结果对象数组[{},{},...]																											
	private PageCondition pageCondition;			//分页对象
	
	public List<AssitBillResult> getResult() {
		return result;
	}
	public void setResult(List<AssitBillResult> result) {
		this.result = result;
	}
	public PageCondition getPageCondition() {
		return pageCondition;
	}
	public void setPageCondition(PageCondition pageCondition) {
		this.pageCondition = pageCondition;
	}
}
