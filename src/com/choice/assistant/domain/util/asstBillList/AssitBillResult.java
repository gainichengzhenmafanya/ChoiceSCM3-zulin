package com.choice.assistant.domain.util.asstBillList;

import java.util.List;
import java.util.Map;


/**
 * 订单列表实际数据实体类
 * @author ZGL
 *
 */
public class AssitBillResult {

	private Map<String,Object> orderInfo;			//助手订单对象
	private  String id;												//商城订单主键
	private  String snAst;											//暂无
	private  String sn;												//商城订单号
	private  String goodsTotalPrice;						//商品总金额
	private  String orderTotalPrice;							//订单总金额
	private  String created;										//暂无
	private  String buyMsg;										//暂无
	private  Map<String,Object> state;					//订单状态{"name":"待付款","code":100}
	private  Map<String,String> seller;					//暂无
	private  String modified;									//暂无
	private List<Map<String,Object>> goodsOrder;		//订单的商城商品信息
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSnAst() {
		return snAst;
	}
	public void setSnAst(String snAst) {
		this.snAst = snAst;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void setOrderTotalPrice(String orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getBuyMsg() {
		return buyMsg;
	}
	public void setBuyMsg(String buyMsg) {
		this.buyMsg = buyMsg;
	}
	public Map<String,Object> getState() {
		return state;
	}
	public void setState(Map<String,Object> state) {
		this.state = state;
	}
	public Map<String, String> getSeller() {
		return seller;
	}
	public void setSeller(Map<String, String> seller) {
		this.seller = seller;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Map<String, Object> getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(Map<String, Object> orderInfo) {
		this.orderInfo = orderInfo;
	}
	public String getGoodsTotalPrice() {
		return goodsTotalPrice;
	}
	public void setGoodsTotalPrice(String goodsTotalPrice) {
		this.goodsTotalPrice = goodsTotalPrice;
	}
	public List<Map<String, Object>> getGoodsOrder() {
		return goodsOrder;
	}
	public void setGoodsOrder(List<Map<String, Object>> goodsOrder) {
		this.goodsOrder = goodsOrder;
	}																									
}
