package com.choice.assistant.domain.util.asstBillInfo;

import java.util.List;
import java.util.Map;


/**
 * 订单明细实体类-商品规格信息
 * @author ZGL
 *
 */
public class AssitBillInfoSpec {

	private  String id;
	private  String unit;
	private  String price;
	private  String stock;
	private  String spec1;
	private  String spec2;
	private  String value1;
	private  String value2;
	private  List<Map<String,Object>> goods;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getSpec1() {
		return spec1;
	}
	public void setSpec1(String spec1) {
		this.spec1 = spec1;
	}
	public String getSpec2() {
		return spec2;
	}
	public void setSpec2(String spec2) {
		this.spec2 = spec2;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public List<Map<String, Object>> getGoods() {
		return goods;
	}
	public void setGoods(List<Map<String, Object>> goods) {
		this.goods = goods;
	}
	
}
