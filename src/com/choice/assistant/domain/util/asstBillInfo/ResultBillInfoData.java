package com.choice.assistant.domain.util.asstBillInfo;

import java.util.List;
import java.util.Map;

/**
 *  订单详情实体类-子类-废弃
 * @author ZGL
 *
 */
public class ResultBillInfoData {
	
	private  String id;												//订单编号
	private  String goodsTotalPrice;						//商品总金额
	private  String sn;												//订单SN
	private  String shippingSN;								//
	private  String orderTotalPrice;							//订单总金额
	private  String address;										//订单地址
	private  String consignee;									//联系人
	private  String state;											//订单状态
	private  String companyName;							//公司名称
	private List<Map<String,Object>> goods;		//订单信息
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGoodsTotalPrice() {
		return goodsTotalPrice;
	}
	public void setGoodsTotalPrice(String goodsTotalPrice) {
		this.goodsTotalPrice = goodsTotalPrice;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getShippingSN() {
		return shippingSN;
	}
	public void setShippingSN(String shippingSN) {
		this.shippingSN = shippingSN;
	}
	public String getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void setOrderTotalPrice(String orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getConsignee() {
		return consignee;
	}
	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public List<Map<String, Object>> getGoods() {
		return goods;
	}
	public void setGoods(List<Map<String, Object>> goods) {
		this.goods = goods;
	}
	
}
