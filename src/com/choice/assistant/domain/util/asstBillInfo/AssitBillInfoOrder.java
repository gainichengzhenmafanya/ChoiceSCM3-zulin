package com.choice.assistant.domain.util.asstBillInfo;
/**
 * 订单明细实体类-主类
 * @author ZGL
 *
 */
public class AssitBillInfoOrder {
	
	private  String id;												//订单编号
	private  String snAst;											//订单SN
	private  String sn;												//订单编码
	private  String orderTotalPrice;							//订单总价
	private  String created;										//订单状态
	private  String buyMsg;										//订单总价
	private  String state;											//订单状态
	private  String modified;									//采购商留言		
	private AssitBillInfoSpec spec; 						//订单信息
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSnAst() {
		return snAst;
	}
	public void setSnAst(String snAst) {
		this.snAst = snAst;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void setOrderTotalPrice(String orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getBuyMsg() {
		return buyMsg;
	}
	public void setBuyMsg(String buyMsg) {
		this.buyMsg = buyMsg;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public AssitBillInfoSpec getSpec() {
		return spec;
	}
	public void setSpec(AssitBillInfoSpec spec) {
		this.spec = spec;
	}
}
