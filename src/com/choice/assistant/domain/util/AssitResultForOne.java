package com.choice.assistant.domain.util;

import java.util.Map;

/**
 * 商城接口通用返回组件实体类
 * @author ZGL
 *
 */
public class AssitResultForOne {
	private String receiveTime;			//接口开始执行时间，格式：yyyyMMddHHmmssS																											
	private String sendTime;			//接口返回数据时间，格式：yyyyMMddHHmmssS																											
	private String code;				//接口执行状态码,有6位数字组成。暂时只返回SUCCESS和FAIL																											
	private String errmsg;				//错误消息,接口调用失败时，返回错误消息；成功时，返回空字符串																											
	private Map<String,Object> data;	//业务数据"接口调用失败时，返回空对象({})；
										//接口调用成功时，若返回单条数据，返回对象{key1:value1,key2:value2,...}
	
	public String getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}
	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
																												
}
