package com.choice.assistant.domain.util;

import java.util.List;
import java.util.Map;

/**
 * 商城接口返回的结果对象
 * @author ZGL
 *
 */
public class ResultData {
	private List<Map<String,Object>> result;					//结果对象数组[{},{},...]																											
	private PageCondition pageCondition;	//分页对象
	private List<Map<String,Object>> order;					//结果对象数组[{},{},...]	
	private String id;
	private String sn;
	
	public List<Map<String, Object>> getResult() {
		return result;
	}
	public void setResult(List<Map<String, Object>> result) {
		this.result = result;
	}
	public PageCondition getPageCondition() {
		return pageCondition;
	}
	public void setPageCondition(PageCondition pageCondition) {
		this.pageCondition = pageCondition;
	}
	public List<Map<String, Object>> getOrder() {
		return order;
	}
	public void setOrder(List<Map<String, Object>> order) {
		this.order = order;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	
	
}
