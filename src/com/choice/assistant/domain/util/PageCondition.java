package com.choice.assistant.domain.util;
/**
 * 商城接口返回的分页对象
 * @author ZGL
 *
 */
public class PageCondition {
	private String pageNo;		//当前页	,默认1，小于等于0时，自动赋值为1																											
	private String pageSize;	//每页数据量	,默认10，小于等于0时，自动赋值为10；最大100，大于100时，自动赋值为100																											
	private String allCount;	//数据量	,根据业务，检索出来的数据量，便于接口调用方，计算其他分页属性																											
	
	public String getPageNo() {
		return pageNo;
	}
	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getAllCount() {
		return allCount;
	}
	public void setAllCount(String allCount) {
		this.allCount = allCount;
	}

}
