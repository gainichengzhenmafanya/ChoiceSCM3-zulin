package com.choice.assistant.domain.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 方法的注解，这个注解可以映射方法的索引
 */
@Retention(RetentionPolicy.RUNTIME) // 注解会在class字节码文件中存在，在运行时可以通过反射获取到
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.TYPE})//定义注解的作用目标**作用范围字段、枚举的常量/方法
public @interface ManagerName {
    public String value() default "";//注解
    public boolean isDB() default false;//判断需要查询数据库
    public String tableName() default "";//表明
    public String tablePk() default "";//字段PK
    public String valueName() default "";//从数据库查询出来的值
    public String enumName() default "";//用来处理数据枚举
    public boolean isConfirm() default true;//是否需要验证
}
