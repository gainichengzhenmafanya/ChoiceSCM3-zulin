package com.choice.assistant.domain.util.uploadBill;

import java.util.List;


/**
 * 订单提交商城返回的result
 * @author ZGL
 *
 */
public class AssitUploadBillResult {

	private  String id;												//商城订单主键
	private  String sn;												//商城订单号
	private List<ResultOrder> order;						//	上传订单的商城返回信息
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public List<ResultOrder> getOrder() {
		return order;
	}
	public void setOrder(List<ResultOrder> order) {
		this.order = order;
	}
	
	
}
