package com.choice.assistant.domain.util.uploadBill;

import java.util.List;

import com.choice.assistant.domain.util.PageCondition;
/**
 *  订单提交商城返回的data
 * @author ZGL
 *
 */
public class UploadResultBillData {

	private List<AssitUploadBillResult> result;			//结果对象数组[{},{},...]																											
	private PageCondition pageCondition;			//分页对象
	
	public List<AssitUploadBillResult> getResult() {
		return result;
	}
	public void setResult(List<AssitUploadBillResult> result) {
		this.result = result;
	}
	public PageCondition getPageCondition() {
		return pageCondition;
	}
	public void setPageCondition(PageCondition pageCondition) {
		this.pageCondition = pageCondition;
	}
}
