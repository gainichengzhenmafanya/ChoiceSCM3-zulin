package com.choice.assistant.domain.util;

import java.util.List;
import java.util.Map;

import com.choice.assistant.domain.util.asstGoods.AsstStore;

public class AsstGoods {
	
	private List<Map<String,Object>> spec;	//商品规格信息
	private String adword;		//商品广告词
	private String defPrice;	//商品默认单价
	private String least;		//商品最低起购量
	private String isMianyi;	//商品价格是否面议
	private List<Map<String,Object>> image;		//商品图片信息
	private String SN;			//商品SN
	private String id;			//商品编号
	private String unit;		//商品单位
	private String stock;		//商品库存
	private List<Map<String,Object>> specName;	//商品规格属性
	private String description;	//商品描述
	private String name;		//商品名称
	private String volume;		//商品总销量
	private String brand;		//商品品牌
	private String monthVolume;	//商品月销量
	private String isRecommend;		//商品是否推荐
	private String minPrice;	//
	private AsstStore store;	//供应商
	private Map<String,Object> faq; //咨询数量
	
	public List<Map<String, Object>> getSpec() {
		return spec;
	}
	public void setSpec(List<Map<String, Object>> spec) {
		this.spec = spec;
	}
	public String getAdword() {
		return adword;
	}
	public void setAdword(String adword) {
		this.adword = adword;
	}
	public String getDefPrice() {
		return defPrice;
	}
	public void setDefPrice(String defPrice) {
		this.defPrice = defPrice;
	}
	public String getLeast() {
		return least;
	}
	public void setLeast(String least) {
		this.least = least;
	}
	public String getIsMianyi() {
		return isMianyi;
	}
	public void setIsMianyi(String isMianyi) {
		this.isMianyi = isMianyi;
	}
	public List<Map<String, Object>> getImage() {
		return image;
	}
	public void setImage(List<Map<String, Object>> image) {
		this.image = image;
	}
	public String getSN() {
		return SN;
	}
	public void setSN(String sN) {
		SN = sN;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public List<Map<String, Object>> getSpecName() {
		return specName;
	}
	public void setSpecName(List<Map<String, Object>> specName) {
		this.specName = specName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getMonthVolume() {
		return monthVolume;
	}
	public void setMonthVolume(String monthVolume) {
		this.monthVolume = monthVolume;
	}
	public String getIsRecommend() {
		return isRecommend;
	}
	public void setIsRecommend(String isRecommend) {
		this.isRecommend = isRecommend;
	}
	public String getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	public AsstStore getStore() {
		return store;
	}
	public void setStore(AsstStore store) {
		this.store = store;
	}
	public Map<String, Object> getFaq() {
		return faq;
	}
	public void setFaq(Map<String, Object> faq) {
		this.faq = faq;
	}
}
