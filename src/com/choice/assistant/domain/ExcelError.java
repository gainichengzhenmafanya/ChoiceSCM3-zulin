package com.choice.assistant.domain;

public class ExcelError {

	private int sheet;//sheet页码
	private int row;//行号
	private int col;//列号
	private String type;//错误类型
	private String attrname;//属性名
	private int length;//长度
	private int rowexist;
	private int colexist;
	public int getSheet() {
		return sheet;
	}
	public void setSheet(int sheet) {
		this.sheet = sheet;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAttrname() {
		return attrname;
	}
	public void setAttrname(String attrname) {
		this.attrname = attrname;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public int getRowexist() {
		return rowexist;
	}
	public void setRowexist(int rowexist) {
		this.rowexist = rowexist;
	}
	public int getColexist() {
		return colexist;
	}
	public void setColexist(int colexist) {
		this.colexist = colexist;
	}
}
