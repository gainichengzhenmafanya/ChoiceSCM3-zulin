package com.choice.assistant.domain.procurementtender;

import java.util.List;


/**
 * 采购招标-应标供应商信息
 * @author xiaoguai
 *
 */
public class CgTenderd {
	
	private String acct;				//	企业id
	private String pk_cgtender;			//招标书主键
	private String pk_cgtenderd;		//招标书子表主键
	private Integer dr;							//
	private String ts;								//
	private String delivercode;			//供应商主键
	private String delivername;		//供应商名称
	private String vcontract;				//联系人
	private String vphone;					//练习电话
	private String vaddr;						//地址
	private String varear;						//供应商经营范围
	private String vregion;					//供应商所属区域
	private String vdeliveryarea;			//供应商配送范围
	private String vdatetime;				//应标时间
	private String vtendername;			//招标类型名称
	private String vtendermemo;		//招标书内容
	private String pk_paymethod;		//支付方式主键
	private String vpaytypecode;		//支付方式编码
	private String vpaytypename;		//支付方式名称
	private List<String> listPk_cgtenderds;//主键集合
	
	public List<String> getListPk_cgtenderds() {
		return listPk_cgtenderds;
	}
	public void setListPk_cgtenderds(List<String> listPk_cgtenderds) {
		this.listPk_cgtenderds = listPk_cgtenderds;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_cgtender() {
		return pk_cgtender;
	}
	public void setPk_cgtender(String pk_cgtender) {
		this.pk_cgtender = pk_cgtender;
	}
	public String getPk_cgtenderd() {
		return pk_cgtenderd;
	}
	public void setPk_cgtenderd(String pk_cgtenderd) {
		this.pk_cgtenderd = pk_cgtenderd;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
//	public String getVdelivername() {
//		return delivername;
//	}
//	public void setVdelivername(String delivername) {
//		this.delivername = delivername;
//	}
	
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getVcontract() {
		return vcontract;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public void setVcontract(String vcontract) {
		this.vcontract = vcontract;
	}
	public String getVphone() {
		return vphone;
	}
	public void setVphone(String vphone) {
		this.vphone = vphone;
	}
	public String getVaddr() {
		return vaddr;
	}
	public void setVaddr(String vaddr) {
		this.vaddr = vaddr;
	}
	public String getVarear() {
		return varear;
	}
	public void setVarear(String varear) {
		this.varear = varear;
	}
	public String getVregion() {
		return vregion;
	}
	public void setVregion(String vregion) {
		this.vregion = vregion;
	}
	public String getVdeliveryarea() {
		return vdeliveryarea;
	}
	public void setVdeliveryarea(String vdeliveryarea) {
		this.vdeliveryarea = vdeliveryarea;
	}
	public String getVdatetime() {
		return vdatetime;
	}
	public void setVdatetime(String vdatetime) {
		this.vdatetime = vdatetime;
	}
	public String getVtendername() {
		return vtendername;
	}
	public void setVtendername(String vtendername) {
		this.vtendername = vtendername;
	}
	public String getVtendermemo() {
		return vtendermemo;
	}
	public void setVtendermemo(String vtendermemo) {
		this.vtendermemo = vtendermemo;
	}
	public String getPk_paymethod() {
		return pk_paymethod;
	}
	public void setPk_paymethod(String pk_paymethod) {
		this.pk_paymethod = pk_paymethod;
	}
	public String getVpaytypecode() {
		return vpaytypecode;
	}
	public void setVpaytypecode(String vpaytypecode) {
		this.vpaytypecode = vpaytypecode;
	}
	public String getVpaytypename() {
		return vpaytypename;
	}
	public void setVpaytypename(String vpaytypename) {
		this.vpaytypename = vpaytypename;
	}

	
}
