package com.choice.assistant.domain.procurementtender;

import java.util.List;

/**
 * 采购招标
 * @author xiaoguai
 *
 */
public class CgTender {

	private String pk_cgtender;//主键
	private String acct;//企业id
	private String vcgtendername;//招标名称
	private String pk_tender;//招标类型主键
	private String vtendercode;//招标类型编码
	private String vtendername;//招标类型名称
	private String vtendermemo;//招标内容
	private String pk_paymethod;//付款方式主键
	private String vpaytypecode;//付款方式编码
	private String vpaytypename;//付款方式名称
	private int vnvoicetype;//发票类型      1增值发票    2普通发票    3不需要发票
	private int ndelivery;//交货日期
	private String vreceiptaddr;//收货地址
	private String dregistrationedat;//报名截至日期
	private String dbidedat;//投标截至日期
	private double nregistermoney;//注册资金
	private char vyyzz;//营业执照        1/0
	private char vswdjz;//税务登记证        1/0
	private char vzzjgdm;//组织机构代码        1/0
	private char vhjgltxrzs;//环境管理体系认证书        1/0
	private char vqyzlgl;//企业质量管理体系认证书        1/0
	private String vcontact;//联系人
	private String vtelephone;//固定电话
	private String vmobilphone;//手机
	private String vemial;//邮箱
	private String isstat;//招标书状态  1 未发布 2已发布 3 已终止 4 已删除
	private int dr;
	private String ts;
	private String vcreatetime;//创建时间
	private String vreleasetime;//发布时间
	private String vstoptime;//停止时间

	//****拼接地址**************************
	private String country;//国家
	private String province;//省份
	private String city;//城市
	private String xxdzaddr;
	//****拼接地址**************************
	private List<String> listPk_cgtenders;//主键集合
	private String vmallid;//商城id

	 private String msgStr;//返回信息临时存储字段
	 private String vtotaladdr;//收货地址中文全路径
	
	public List<String> getListPk_cgtenders() {
		return listPk_cgtenders;
	}
	public void setListPk_cgtenders(List<String> listPk_cgtenders) {
		this.listPk_cgtenders = listPk_cgtenders;
	}
	public String getVcreatetime() {
		return vcreatetime;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getXxdzaddr() {
		return xxdzaddr;
	}
	public void setXxdzaddr(String xxdzaddr) {
		this.xxdzaddr = xxdzaddr;
	}
	public void setVcreatetime(String vcreatetime) {
		this.vcreatetime = vcreatetime;
	}
	public String getVreleasetime() {
		return vreleasetime;
	}
	public void setVreleasetime(String vreleasetime) {
		this.vreleasetime = vreleasetime;
	}
	public String getVstoptime() {
		return vstoptime;
	}
	public void setVstoptime(String vstoptime) {
		this.vstoptime = vstoptime;
	}
	public String getPk_cgtender() {
		return pk_cgtender;
	}
	public void setPk_cgtender(String pk_cgtender) {
		this.pk_cgtender = pk_cgtender;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getVcgtendername() {
		return vcgtendername;
	}
	public void setVcgtendername(String vcgtendername) {
		this.vcgtendername = vcgtendername;
	}
	public String getPk_tender() {
		return pk_tender;
	}
	public void setPk_tender(String pk_tender) {
		this.pk_tender = pk_tender;
	}
	public String getVtendercode() {
		return vtendercode;
	}
	public void setVtendercode(String vtendercode) {
		this.vtendercode = vtendercode;
	}
	public String getVtendername() {
		return vtendername;
	}
	public void setVtendername(String vtendername) {
		this.vtendername = vtendername;
	}
	public String getVtendermemo() {
		return vtendermemo;
	}
	public void setVtendermemo(String vtendermemo) {
		this.vtendermemo = vtendermemo;
	}
	public String getPk_paymethod() {
		return pk_paymethod;
	}
	public void setPk_paymethod(String pk_paymethod) {
		this.pk_paymethod = pk_paymethod;
	}
	public String getVpaytypecode() {
		return vpaytypecode;
	}
	public void setVpaytypecode(String vpaytypecode) {
		this.vpaytypecode = vpaytypecode;
	}
	public String getVpaytypename() {
		return vpaytypename;
	}
	public void setVpaytypename(String vpaytypename) {
		this.vpaytypename = vpaytypename;
	}
	public int getVnvoicetype() {
		return vnvoicetype;
	}
	public void setVnvoicetype(int vnvoicetype) {
		this.vnvoicetype = vnvoicetype;
	}
	public int getNdelivery() {
		return ndelivery;
	}
	public void setNdelivery(int ndelivery) {
		this.ndelivery = ndelivery;
	}
	public String getVreceiptaddr() {
		return vreceiptaddr;
	}
	public void setVreceiptaddr(String vreceiptaddr) {
		this.vreceiptaddr = vreceiptaddr;
	}
	public String getDregistrationedat() {
		return dregistrationedat;
	}
	public void setDregistrationedat(String dregistrationedat) {
		this.dregistrationedat = dregistrationedat;
	}
	public String getDbidedat() {
		return dbidedat;
	}
	public void setDbidedat(String dbidedat) {
		this.dbidedat = dbidedat;
	}
	public double getNregistermoney() {
		return nregistermoney;
	}
	public void setNregistermoney(double nregistermoney) {
		this.nregistermoney = nregistermoney;
	}
	public char getVyyzz() {
		return vyyzz;
	}
	public void setVyyzz(char vyyzz) {
		this.vyyzz = vyyzz;
	}
	public char getVswdjz() {
		return vswdjz;
	}
	public void setVswdjz(char vswdjz) {
		this.vswdjz = vswdjz;
	}
	public char getVzzjgdm() {
		return vzzjgdm;
	}
	public void setVzzjgdm(char vzzjgdm) {
		this.vzzjgdm = vzzjgdm;
	}
	public char getVhjgltxrzs() {
		return vhjgltxrzs;
	}
	public void setVhjgltxrzs(char vhjgltxrzs) {
		this.vhjgltxrzs = vhjgltxrzs;
	}
	public char getVqyzlgl() {
		return vqyzlgl;
	}
	public void setVqyzlgl(char vqyzlgl) {
		this.vqyzlgl = vqyzlgl;
	}
	public String getVcontact() {
		return vcontact;
	}
	public void setVcontact(String vcontact) {
		this.vcontact = vcontact;
	}
	public String getVtelephone() {
		return vtelephone;
	}
	public void setVtelephone(String vtelephone) {
		this.vtelephone = vtelephone;
	}
	public String getVmobilphone() {
		return vmobilphone;
	}
	public void setVmobilphone(String vmobilphone) {
		this.vmobilphone = vmobilphone;
	}
	public String getVemial() {
		return vemial;
	}
	public void setVemial(String vemial) {
		this.vemial = vemial;
	}
	public String getIsstat() {
		return isstat;
	}
	public void setIsstat(String isstat) {
		this.isstat = isstat;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVmallid() {
		return vmallid;
	}
	public void setVmallid(String vmallid) {
		this.vmallid = vmallid;
	}
	public String getMsgStr() {
		return msgStr;
	}
	public void setMsgStr(String msgStr) {
		this.msgStr = msgStr;
	}
	public String getVtotaladdr() {
		return vtotaladdr;
	}
	public void setVtotaladdr(String vtotaladdr) {
		this.vtotaladdr = vtotaladdr;
	}
	
}
