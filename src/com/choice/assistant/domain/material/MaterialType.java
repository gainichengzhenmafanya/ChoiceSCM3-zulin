package com.choice.assistant.domain.material;

import java.util.List;

/**
 * 物资类别信息表
 * @author wangchao
 *
 */
public class MaterialType {

	private String acct;//企业ID
	private String pk_materialtype;//主键
	private String pk_father;//上级主键
	private String vcode;//编码
	private String vname;//名称
	private int enablestate;//启用状态
	private int dr;//删除标识
	private String ts;//时间戳
	private String vdef;
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	
	private MaterialType parentMaterialtype;
	
	private List<MaterialType> childMaterialtype;
	
	private Integer isfromerp;//是否来自ERP    0否1是
	private Integer isdeal;//是否已处理 0否1是
	private String parentcode;//父级类别编码
	private List<String> pks;
	private List<String> parentpks;
	private String pk_accountid;//帐号id
	
	public String getPk_accountid() {
		return pk_accountid;
	}
	public void setPk_accountid(String pk_accountid) {
		this.pk_accountid = pk_accountid;
	}
	public List<String> getPks() {
		return pks;
	}
	public void setPks(List<String> pks) {
		this.pks = pks;
	}
	public MaterialType getParentMaterialtype() {
		return parentMaterialtype;
	}
	public void setParentMaterialtype(MaterialType parentMaterialtype) {
		this.parentMaterialtype = parentMaterialtype;
	}
	public List<MaterialType> getChildMaterialtype() {
		return childMaterialtype;
	}
	public void setChildMaterialtype(List<MaterialType> childMaterialtype) {
		this.childMaterialtype = childMaterialtype;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_materialtype() {
		return pk_materialtype;
	}
	public void setPk_materialtype(String pk_materialtype) {
		this.pk_materialtype = pk_materialtype;
	}
	public String getPk_father() {
		return pk_father;
	}
	public void setPk_father(String pk_father) {
		this.pk_father = pk_father;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public int getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(int enablestate) {
		this.enablestate = enablestate;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVdef() {
		return vdef;
	}
	public void setVdef(String vdef) {
		this.vdef = vdef;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public Integer getIsfromerp() {
		return isfromerp;
	}
	public void setIsfromerp(Integer isfromerp) {
		this.isfromerp = isfromerp;
	}
	public Integer getIsdeal() {
		return isdeal;
	}
	public void setIsdeal(Integer isdeal) {
		this.isdeal = isdeal;
	}
	public String getParentcode() {
		return parentcode;
	}
	public void setParentcode(String parentcode) {
		this.parentcode = parentcode;
	}
	public List<String> getParentpks() {
		return parentpks;
	}
	public void setParentpks(List<String> parentpks) {
		this.parentpks = parentpks;
	}
	  
	  
	
}
