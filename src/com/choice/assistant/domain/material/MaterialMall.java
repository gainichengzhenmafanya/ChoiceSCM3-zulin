package com.choice.assistant.domain.material;

public class MaterialMall {

	private String pk_materialmall;//主键
	private String acct;//企业id
	private String sp_code;//物资主键
	private String vhspec;//商城规格主键
	private String vjmuname;//商城物资名称
	private String vjmucode;//商城物资编码
	private String vhstore;//店铺主键
	private double sp_price;//报价
	private String vjmustorename;//店铺名称
	private String vjmudelivername;//商城供应商名称
	private String vjmudelivercode;//商城供应商编码
	private int dr;//
	private String ts;//
	
	public String getPk_materialmall() {
		return pk_materialmall;
	}
	public void setPk_materialmall(String pk_materialmall) {
		this.pk_materialmall = pk_materialmall;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getVhspec() {
		return vhspec;
	}
	public void setVhspec(String vhspec) {
		this.vhspec = vhspec;
	}
	public String getVjmuname() {
		return vjmuname;
	}
	public void setVjmuname(String vjmuname) {
		this.vjmuname = vjmuname;
	}
	public String getVjmucode() {
		return vjmucode;
	}
	public void setVjmucode(String vjmucode) {
		this.vjmucode = vjmucode;
	}
	public String getVhstore() {
		return vhstore;
	}
	public void setVhstore(String vhstore) {
		this.vhstore = vhstore;
	}
	public double getSp_price() {
		return sp_price;
	}
	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}
	public String getVjmustorename() {
		return vjmustorename;
	}
	public void setVjmustorename(String vjmustorename) {
		this.vjmustorename = vjmustorename;
	}
	public String getVjmudelivername() {
		return vjmudelivername;
	}
	public void setVjmudelivername(String vjmudelivername) {
		this.vjmudelivername = vjmudelivername;
	}
	public String getVjmudelivercode() {
		return vjmudelivercode;
	}
	public void setVjmudelivercode(String vjmudelivercode) {
		this.vjmudelivercode = vjmudelivercode;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
}
