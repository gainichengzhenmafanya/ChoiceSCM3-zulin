package com.choice.assistant.domain.material;

import java.util.List;

import com.choice.assistant.domain.system.Unit;

/**
 * 待审核物资实体类
 * @author wangchao
 *
 */
public class ReviMaterial {

	private String acct;//企业ID
	private String pk_revimaterial;//主键
	private String pk_material;//物资主键
	private String vcode;//物资编码
	private String vname;//物资名称
	private String vspecfication;//规格
	private String pk_unit;//单位主键
	private String delivercode;//供应商编码
	private String delivername;//供应商名称
	private String ts;
	private int dr;
	private String vdef1;
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	private Unit unit;
	private String vapplydesc;//申请原因
	private List<String> pklist;//主键集合
	private String vhspec;//商城物资规格主键
	private String vhstore;//店铺主键
	private String vjmustorename;//店铺名称
	private String vjmusupplierpk;//商城供应商主键
	private double nprice;//报价
	private double nleast;//起购量
	private String vunit;//商城物资单位
	
	public double getNprice() {
		return nprice;
	}
	public void setNprice(double nprice) {
		this.nprice = nprice;
	}
	public String getVhspec() {
		return vhspec;
	}
	public void setVhspec(String vhspec) {
		this.vhspec = vhspec;
	}
	public String getVhstore() {
		return vhstore;
	}
	public void setVhstore(String vhstore) {
		this.vhstore = vhstore;
	}
	public String getVjmustorename() {
		return vjmustorename;
	}
	public void setVjmustorename(String vjmustorename) {
		this.vjmustorename = vjmustorename;
	}
	public String getVjmusupplierpk() {
		return vjmusupplierpk;
	}
	public void setVjmusupplierpk(String vjmusupplierpk) {
		this.vjmusupplierpk = vjmusupplierpk;
	}
	public List<String> getPklist() {
		return pklist;
	}
	public void setPklist(List<String> pklist) {
		this.pklist = pklist;
	}
	public Unit getUnit() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_revimaterial() {
		return pk_revimaterial;
	}
	public void setPk_revimaterial(String pk_revimaterial) {
		this.pk_revimaterial = pk_revimaterial;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVspecfication() {
		return vspecfication;
	}
	public void setVspecfication(String vspecfication) {
		this.vspecfication = vspecfication;
	}
	public String getPk_unit() {
		return pk_unit;
	}
	public void setPk_unit(String pk_unit) {
		this.pk_unit = pk_unit;
	}
	public String getVdelivercode() {
		return delivercode;
	}
	public void setVdelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getVdelivername() {
		return delivername;
	}
	public void setVdelivername(String delivername) {
		this.delivername = delivername;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public String getVapplydesc() {
		return vapplydesc;
	}
	public void setVapplydesc(String vapplydesc) {
		this.vapplydesc = vapplydesc;
	}
	public double getNleast() {
		return nleast;
	}
	public void setNleast(double nleast) {
		this.nleast = nleast;
	}
	public String getVunit() {
		return vunit;
	}
	public void setVunit(String vunit) {
		this.vunit = vunit;
	}

	
}
