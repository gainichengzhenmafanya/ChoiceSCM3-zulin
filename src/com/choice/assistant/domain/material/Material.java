package com.choice.assistant.domain.material;

import com.choice.scm.domain.Supply;

/**
 * 物资信息表
 * @author wangchao
 *
 */
public class Material extends Supply{

	private String pk_material;
	private String positncode;//采购组织
	private String positninit;//采购组织
	private String positnname;//采购组织
	private String delivercode;//供应商编码
	private String delivername;//供应商名称
	private String deliverinit;//供应商缩写
	private double ncheckdiffrate;//验货差异率
	private double nminordercnt;//最小起订量
	
	private String irateflag;
	private String  vinit;
	private String  supplier;
	private String  str;
	private String  pk_account;
	
	private String ddate;//查询报价日期
	
	
	
	public String getPk_account() {
		return pk_account;
	}
	public void setPk_account(String pk_account) {
		this.pk_account = pk_account;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getVinit() {
		return vinit;
	}
	public void setVinit(String vinit) {
		this.vinit = vinit;
	}
	public String getIrateflag() {
		return irateflag;
	}
	public void setIrateflag(String irateflag) {
		this.irateflag = irateflag;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public String getDdate() {
		return ddate;
	}
	public void setDdate(String ddate) {
		this.ddate = ddate;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public double getNcheckdiffrate() {
		return ncheckdiffrate;
	}
	public void setNcheckdiffrate(double ncheckdiffrate) {
		this.ncheckdiffrate = ncheckdiffrate;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public String getDeliverinit() {
		return deliverinit;
	}
	public void setDeliverinit(String deliverinit) {
		this.deliverinit = deliverinit;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public double getNminordercnt() {
		return nminordercnt;
	}
	public void setNminordercnt(double nminordercnt) {
		this.nminordercnt = nminordercnt;
	}
	public String getPositnname() {
		return positnname;
	}
	public void setPositnname(String positnname) {
		this.positnname = positnname;
	}
	public String getPositninit() {
		return positninit;
	}
	public void setPositninit(String positninit) {
		this.positninit = positninit;
	}
}
