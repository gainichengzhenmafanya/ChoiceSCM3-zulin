package com.choice.assistant.domain.detailedlist;

import java.util.List;

/**
 * 采购清单子表
 * @author wangchao
 *
 */
public class Purtempletd {

	private String acct;//企业ID
	private String pk_purtempletd;//子表主键
	private String pk_purtemplet;//主表主键
	private String pk_material;//物资主键
	private String sp_code;//物资编码
	private String sp_name;//物资名称
	private String unit;//物资单位
	private double cnt;//采购数量
	private int isort;//排序
	private int dr;
	private String ts;
	private String sp_desc;//规格
	private double sp_price;//售价
	private String delivername;//供应商名称
	private String delivercode;//供应商主键
	private String vmemo;
	private List<String> pkdlist;
	private double nminamt;
	
	private Double tax;
	private String taxdes;
	
	private int beditprice;//物资价格是否来自合约 1是0否
	private String positncode;//采购组织
	private String pk_accountid;//账号
    private String wzqx;//物资权限，无筛选0，按照分店物资属性1，按照账号物资权限2
    private String wzzhqx;//物资账号权限再细分：0具体到物资  1具体到小类  wjf
    
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	public String getWzqx() {
		return wzqx;
	}
	public void setWzqx(String wzqx) {
		this.wzqx = wzqx;
	}
	public String getWzzhqx() {
		return wzzhqx;
	}
	public void setWzzhqx(String wzzhqx) {
		this.wzzhqx = wzzhqx;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_purtempletd() {
		return pk_purtempletd;
	}
	public void setPk_purtempletd(String pk_purtempletd) {
		this.pk_purtempletd = pk_purtempletd;
	}
	public String getPk_purtemplet() {
		return pk_purtemplet;
	}
	public void setPk_purtemplet(String pk_purtemplet) {
		this.pk_purtemplet = pk_purtemplet;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	public int getIsort() {
		return isort;
	}
	public void setIsort(int isort) {
		this.isort = isort;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public double getSp_price() {
		return sp_price;
	}
	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public String getPk_supplier() {
		return delivercode;
	}
	public void setPk_supplier(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public List<String> getPkdlist() {
		return pkdlist;
	}
	public void setPkdlist(List<String> pkdlist) {
		this.pkdlist = pkdlist;
	}
	public double getNminamt() {
		return nminamt;
	}
	public void setNminamt(double nminamt) {
		this.nminamt = nminamt;
	}
	public int getBeditprice() {
		return beditprice;
	}
	public void setBeditprice(int beditprice) {
		this.beditprice = beditprice;
	}
	public String getPk_accountid() {
		return pk_accountid;
	}
	public void setPk_accountid(String pk_accountid) {
		this.pk_accountid = pk_accountid;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	
	
}
