package com.choice.assistant.domain.detailedlist;

import java.util.List;

/**
 * 采购清单主表
 * @author wangchao
 *
 */
public class Purtempletm {

	private String acct;//企业ID
	private String pk_purtemplet;//主键
	private String vname;//名称
	private String vmemo;//摘要
	private String ddate;//单据日期
	private String creator;//创建人
	private String creationtime;//创建时间
	private String modifier;//修改人
	private String modifedtime;//修改时间
	private int dr;
	private String ts;
	private String vdef1;
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	private List<Purtempletd> listPurtempletd;
	private List<String> pklist;
	public List<String> getPklist() {
		return pklist;
	}
	public void setPklist(List<String> pklist) {
		this.pklist = pklist;
	}
	public List<Purtempletd> getListPurtempletd() {
		return listPurtempletd;
	}
	public void setListPurtempletd(List<Purtempletd> listPurtempletd) {
		this.listPurtempletd = listPurtempletd;
	}
	public String getDdate() {
		return ddate;
	}
	public void setDdate(String ddate) {
		this.ddate = ddate;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_purtemplet() {
		return pk_purtemplet;
	}
	public void setPk_purtemplet(String pk_purtemplet) {
		this.pk_purtemplet = pk_purtemplet;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifedtime() {
		return modifedtime;
	}
	public void setModifedtime(String modifedtime) {
		this.modifedtime = modifedtime;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	
}
