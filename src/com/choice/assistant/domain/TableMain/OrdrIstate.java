package com.choice.assistant.domain.TableMain;

public class OrdrIstate {
	private String acct;
	private String dqr="0";
	private String dsh="0";
	private String drk="0";
	private String djs="0";
	private String vbillno;
	private String delivername;
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getDqr() {
		return dqr;
	}
	public void setDqr(String dqr) {
		this.dqr = dqr;
	}
	public String getDsh() {
		return dsh;
	}
	public void setDsh(String dsh) {
		this.dsh = dsh;
	}
	public String getDrk() {
		return drk;
	}
	public void setDrk(String drk) {
		this.drk = drk;
	}
	public String getDjs() {
		return djs;
	}
	public void setDjs(String djs) {
		this.djs = djs;
	}
	public String getVbillno() {
		return vbillno;
	}
	public void setVbillno(String vbillno) {
		this.vbillno = vbillno;
	}
	public String getVdelivername() {
		return delivername;
	}
	public void setVdelivername(String delivername) {
		this.delivername = delivername;
	}
	
}
