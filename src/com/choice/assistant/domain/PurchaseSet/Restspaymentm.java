package com.choice.assistant.domain.PurchaseSet;

import java.util.List;


public class Restspaymentm {

	private String acct;			//企业ID
	private String pk_restspayment;		//其他付款单主键
	private String vcode;				//其他付款单编码
	private String delivercode;			//供应商主键
	private Double ntotalmoney;			//合计金额
	private Integer enablestate;		//启用状态
	private String vmemo;				//摘要
	private String creator;				//制单人
	private String creationtime;		//制单时间
	private String positncode;				//采购组织
	private Integer dr;					//
	private String ts;					//
	private String vdef1;				//
	private String vdef2;				//
	private String vdef3;				//
	private String vdef4;				//
	private String vdef5;				//
	
	private List<Restspaymentd> restspaymentdList;
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_restspayment() {
		return pk_restspayment;
	}
	public void setPk_restspayment(String pk_restspayment) {
		this.pk_restspayment = pk_restspayment;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getPk_supplier() {
		return delivercode;
	}
	public void setPk_supplier(String delivercode) {
		this.delivercode = delivercode;
	}
	public Double getNtotalmoney() {
		return ntotalmoney;
	}
	public void setNtotalmoney(Double ntotalmoney) {
		this.ntotalmoney = ntotalmoney;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getPk_org() {
		return positncode;
	}
	public void setPk_org(String positncode) {
		this.positncode = positncode;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public List<Restspaymentd> getRestspaymentdList() {
		return restspaymentdList;
	}
	public void setRestspaymentdList(List<Restspaymentd> restspaymentdList) {
		this.restspaymentdList = restspaymentdList;
	}
	

	
}
