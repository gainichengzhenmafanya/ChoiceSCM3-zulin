package com.choice.assistant.domain.PurchaseSet;

import java.util.List;

public class Backbillm {

	private String acct;			//企业ID
	private String pk_backbill;			//验货单主键
	private String vbillno;				//验货单单号
	private Double nmoney;				//合计金额
	private String dbilldate;			//单据日期
	private Integer istate;				//单据状态 1、已确认退货 2、未退货3、已结算
	private String creator;				//制单人
	private String creationtime;		//制单时间
	private String vmemo;				//摘要
	private String positncode;				//采购组织
	private Integer dr;					//
	private String ts;					//
	private String vdef1;				//
	private String vdef2;				//
	private String vdef3;				//
	private String vdef4;				//
	private String vdef5;				//
	private String dbilldateend;			//单据日期
	
	
	private List<Backbilld> backbilldList;
	
	private String delivername;//供应商名称
	private String delivercode;//供应商编码
	private String bdate;
	private String edate;
	
	private String pk_inspectlist;//验货单主键集合
	private String vinspectvouno;//验货单单号主键
	private String modifier;//修改人
	private String modifedtime;//修改时间
	private String vbacker;//确认验货人
	private String vbacktime;//确认验货时间 
	private String pk_inspects;//验货单主键
	private String pk_inspect;
	private String vinspectbillno;
	
	private String vouno;
	private int chkno;
	
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public int getChkno() {
		return chkno;
	}
	public void setChkno(int chkno) {
		this.chkno = chkno;
	}
	public String getVbacker() {
		return vbacker;
	}
	public void setVbacker(String vbacker) {
		this.vbacker = vbacker;
	}
	public String getVbacktime() {
		return vbacktime;
	}
	public void setVbacktime(String vbacktime) {
		this.vbacktime = vbacktime;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifedtime() {
		return modifedtime;
	}
	public void setModifedtime(String modifedtime) {
		this.modifedtime = modifedtime;
	}
	private List<String> pks;//主键集合，用作删除
	
	public List<String> getPks() {
		return pks;
	}
	public void setPks(List<String> pks) {
		this.pks = pks;
	}
	public String getPk_inspectlist() {
		return pk_inspectlist;
	}
	public void setPk_inspectlist(String pk_inspectlist) {
		this.pk_inspectlist = pk_inspectlist;
	}
	public String getVinspectvouno() {
		return vinspectvouno;
	}
	public void setVinspectvouno(String vinspectvouno) {
		this.vinspectvouno = vinspectvouno;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getAcct() {		
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_backbill() {
		return pk_backbill;
	}
	public void setPk_backbill(String pk_backbill) {
		this.pk_backbill = pk_backbill;
	}
	public String getVbillno() {
		return vbillno;
	}
	public void setVbillno(String vbillno) {
		this.vbillno = vbillno;
	}
	public String getPk_supplier() {
		return delivercode;
	}
	public void setPk_supplier(String delivercode) {
		this.delivercode = delivercode;
	}
	public Double getNmoney() {
		return nmoney;
	}
	public void setNmoney(Double nmoney) {
		this.nmoney = nmoney;
	}
	public String getDbilldate() {
		return dbilldate;
	}
	public void setDbilldate(String dbilldate) {
		this.dbilldate = dbilldate;
	}
	public Integer getIstate() {
		return istate;
	}
	public void setIstate(Integer istate) {
		this.istate = istate;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public List<Backbilld> getBackbilldList() {
		return backbilldList;
	}
	public void setBackbilldList(List<Backbilld> backbilldList) {
		this.backbilldList = backbilldList;
	}
	public String getDbilldateend() {
		return dbilldateend;
	}
	public void setDbilldateend(String dbilldateend) {
		this.dbilldateend = dbilldateend;
	}
	public String getPk_inspects() {
		return pk_inspects;
	}
	public void setPk_inspects(String pk_inspects) {
		this.pk_inspects = pk_inspects;
	}
	public String getPk_inspect() {
		return pk_inspect;
	}
	public void setPk_inspect(String pk_inspect) {
		this.pk_inspect = pk_inspect;
	}
	public String getVinspectbillno() {
		return vinspectbillno;
	}
	public void setVinspectbillno(String vinspectbillno) {
		this.vinspectbillno = vinspectbillno;
	}

	
}
