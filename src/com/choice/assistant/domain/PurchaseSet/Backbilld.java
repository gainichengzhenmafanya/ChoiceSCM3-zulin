package com.choice.assistant.domain.PurchaseSet;

public class Backbilld {

	private String acct;
	private String pk_backbilld;
	private String pk_backbill;
	private String pk_inspect;
	private String pk_material;
	private String sp_code;
	private String sp_name;
	private String sp_desc;
	private double nbacknum;
	private double nprice;
	private String vreason;
	private double nmoney;
	private Integer ibackbatch;
	private String pk_unit;
	private String unit3;
	private String unitcode;
	private double ninsnum;
	private String vmemo;
	private Integer dr;
	private String ts;
	private Double tax;
	private String taxdes;
	
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_backbilld() {
		return pk_backbilld;
	}
	public void setPk_backbilld(String pk_backbilld) {
		this.pk_backbilld = pk_backbilld;
	}
	public String getPk_backbill() {
		return pk_backbill;
	}
	public void setPk_backbill(String pk_backbill) {
		this.pk_backbill = pk_backbill;
	}
	public String getPk_inspect() {
		return pk_inspect;
	}
	public void setPk_inspect(String pk_inspect) {
		this.pk_inspect = pk_inspect;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public double getNbacknum() {
		return nbacknum;
	}
	public void setNbacknum(double nbacknum) {
		this.nbacknum = nbacknum;
	}
	public double getNprice() {
		return nprice;
	}
	public void setNprice(double nprice) {
		this.nprice = nprice;
	}
	public String getVreason() {
		return vreason;
	}
	public void setVreason(String vreason) {
		this.vreason = vreason;
	}
	public double getNmoney() {
		return nmoney;
	}
	public void setNmoney(double nmoney) {
		this.nmoney = nmoney;
	}
	public Integer getIbackbatch() {
		return ibackbatch;
	}
	public void setIbackbatch(Integer ibackbatch) {
		this.ibackbatch = ibackbatch;
	}
	public String getPk_unit() {
		return pk_unit;
	}
	public void setPk_unit(String pk_unit) {
		this.pk_unit = pk_unit;
	}
	
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	public double getNinsnum() {
		return ninsnum;
	}
	public void setNinsnum(double ninsnum) {
		this.ninsnum = ninsnum;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	
}
