package com.choice.assistant.domain.PurchaseSet;

import java.util.List;

public class Inspectm {

	private String acct;			//企业ID
	private String pk_inspect;			//验货单主键
	private String vbillno;				//验货单单号
	private String pk_puprorder;		//采购订单主键
	private String pk_arrival;			//到货单主表主键
	private Double nmoney;				//合计金额
	private String dbilldate;			//单据日期
	private Integer istate;				//单据状态1.已验货 2.未验货 3已结算【现在2.未入库 3.已结算 4.已入库】
	private String creator;				//制单人
	private String creationtime;		//制单时间
	private String vmemo;				//摘要
	private String positncode;			//采购组织
	private Integer dr;					//
	private String ts;					//
	private String vdef1;				//
	private String vdef2;				//
	private String vdef3;				//
	private String vdef4;				//
	private String vdef5;				//
	private String dbilldateend;			//单据日期
	private String delivercode;//供应商编码
	private String delivername;//供应商名称
	private List<String> pklist;//主键集合，用作删除
	private List<Inspectd> inspectdList;
	private String pk_puprorderList;//采购订单主键集合
	private String vpuproedername;//采购订单单号集合
	private String modifier;//修改人
	private String modifedtime;//修改时间
	private String bdate;
	private String edate;
	private String vinspect;//确认验货人
	private String vinspectdate;//确认验货实时间
	private String pk_puprorders;
	private String vouno;//入库凭证号
	private int chkno;//入库单号
	private String sort;
	
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getVouno() {
		return vouno;
	}
	public void setVouno(String vouno) {
		this.vouno = vouno;
	}
	public int getChkno() {
		return chkno;
	}
	public void setChkno(int chkno) {
		this.chkno = chkno;
	}
	public String getVinspect() {
		return vinspect;
	}
	public void setVinspect(String vinspect) {
		this.vinspect = vinspect;
	}
	public String getVinspectdate() {
		return vinspectdate;
	}
	public void setVinspectdate(String vinspectdate) {
		this.vinspectdate = vinspectdate;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifedtime() {
		return modifedtime;
	}
	public void setModifedtime(String modifedtime) {
		this.modifedtime = modifedtime;
	}
	public String getPk_puprorderList() {
		return pk_puprorderList;
	}
	public void setPk_puprorderList(String pk_puprorderList) {
		this.pk_puprorderList = pk_puprorderList;
	}
	public String getVpuproedername() {
		return vpuproedername;
	}
	public void setVpuproedername(String vpuproedername) {
		this.vpuproedername = vpuproedername;
	}
	
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public List<String> getPklist() {
		return pklist;
	}
	public void setPklist(List<String> pklist) {
		this.pklist = pklist;
	}
	public String getAcct() {		
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_inspect() {
		return pk_inspect;
	}
	public void setPk_inspect(String pk_inspect) {
		this.pk_inspect = pk_inspect;
	}
	public String getVbillno() {
		return vbillno;
	}
	public void setVbillno(String vbillno) {
		this.vbillno = vbillno;
	}
	public String getPk_puprorder() {
		return pk_puprorder;
	}
	public void setPk_puprorder(String pk_puprorder) {
		this.pk_puprorder = pk_puprorder;
	}
	public String getPk_arrival() {
		return pk_arrival;
	}
	public void setPk_arrival(String pk_arrival) {
		this.pk_arrival = pk_arrival;
	}
	public String getPk_supplier() {
		return delivercode;
	}
	public void setPk_supplier(String delivercode) {
		this.delivercode = delivercode;
	}
	public Double getNmoney() {
		return nmoney;
	}
	public void setNmoney(Double nmoney) {
		this.nmoney = nmoney;
	}
	public String getDbilldate() {
		return dbilldate;
	}
	public void setDbilldate(String dbilldate) {
		this.dbilldate = dbilldate;
	}
	public Integer getIstate() {
		return istate;
	}
	public void setIstate(Integer istate) {
		this.istate = istate;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreationtime() {
		return creationtime;
	}
	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	
	
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public List<Inspectd> getInspectdList() {
		return inspectdList;
	}
	public void setInspectdList(List<Inspectd> inspectdList) {
		this.inspectdList = inspectdList;
	}
	public String getDbilldateend() {
		return dbilldateend;
	}
	public void setDbilldateend(String dbilldateend) {
		this.dbilldateend = dbilldateend;
	}
	public String getPk_puprorders() {
		return pk_puprorders;
	}
	public void setPk_puprorders(String pk_puprorders) {
		this.pk_puprorders = pk_puprorders;
	}

	
}
