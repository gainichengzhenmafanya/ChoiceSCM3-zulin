package com.choice.assistant.domain.PurchaseSet;

import java.util.List;

/**
 * 其他应付款主表
 * @author liuqun
 *
 */
public class OtherPayablesm {

	private String pk_otherpayables;//主键
	private String vbillno;//单据号
	private int istate;//单据状态 1已保存2已审核3已结算
	private String pk_supplier;//供应商主键
	private String delivercode;//供应商编码
	private String delivername;//供应商名称
	private Double ntotalmoney;//总额
	private String vmemo;//备注
	private String dbilldate;//单据日期
	private String creator;
	private String creationtime;
	private String modifier;
	private String modifedtime;
	private int dr;
	private String ts;
	private String vdef1;
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	private String acct;
	
	private List<OtherPayablesd> otherpayablesdList;
	private List<String> pklist;
	
	private String bdat;
	private String edat;

	public String getPk_otherpayables() {
		return pk_otherpayables;
	}

	public void setPk_otherpayables(String pk_otherpayables) {
		this.pk_otherpayables = pk_otherpayables;
	}

	public String getVbillno() {
		return vbillno;
	}

	public void setVbillno(String vbillno) {
		this.vbillno = vbillno;
	}

	public String getPk_supplier() {
		return pk_supplier;
	}

	public void setPk_supplier(String pk_supplier) {
		this.pk_supplier = pk_supplier;
	}

	

	public String getDelivercode() {
		return delivercode;
	}

	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}

	public String getDelivername() {
		return delivername;
	}

	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}

	public Double getNtotalmoney() {
		return ntotalmoney;
	}

	public void setNtotalmoney(Double ntotalmoney) {
		this.ntotalmoney = ntotalmoney;
	}

	public String getVmemo() {
		return vmemo;
	}

	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreationtime() {
		return creationtime;
	}

	public void setCreationtime(String creationtime) {
		this.creationtime = creationtime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifedtime() {
		return modifedtime;
	}

	public void setModifedtime(String modifedtime) {
		this.modifedtime = modifedtime;
	}

	public int getDr() {
		return dr;
	}

	public void setDr(int dr) {
		this.dr = dr;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getVdef1() {
		return vdef1;
	}

	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}

	public String getVdef2() {
		return vdef2;
	}

	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}

	public String getVdef3() {
		return vdef3;
	}

	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}

	public String getVdef4() {
		return vdef4;
	}

	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}

	public String getVdef5() {
		return vdef5;
	}

	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}

	public String getAcct() {
		return acct;
	}

	public void setAcct(String acct) {
		this.acct = acct;
	}

	public List<OtherPayablesd> getOtherpayablesdList() {
		return otherpayablesdList;
	}

	public void setOtherpayablesdList(List<OtherPayablesd> otherpayablesdList) {
		this.otherpayablesdList = otherpayablesdList;
	}

	public List<String> getPklist() {
		return pklist;
	}

	public void setPklist(List<String> pklist) {
		this.pklist = pklist;
	}

	public String getDbilldate() {
		return dbilldate;
	}

	public void setDbilldate(String dbilldate) {
		this.dbilldate = dbilldate;
	}

	public int getIstate() {
		return istate;
	}

	public void setIstate(int istate) {
		this.istate = istate;
	}

	public String getBdat() {
		return bdat;
	}

	public void setBdat(String bdat) {
		this.bdat = bdat;
	}

	public String getEdat() {
		return edat;
	}

	public void setEdat(String edat) {
		this.edat = edat;
	}
	
}
