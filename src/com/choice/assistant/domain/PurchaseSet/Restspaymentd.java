package com.choice.assistant.domain.PurchaseSet;

public class Restspaymentd {
	private String pk_restspayment;
	private String pk_otherpayables;
	private String pk_restspayment_sub;
	private String vitem;
	private String pk_inspact;
	private Double nmoney;
	private String vmemo;
	private String vbdef;
	private String vbdef2;
	private String vbdef3;
	private String vbdef4;
	private String vbdef5;
	private Integer dr;
	private String ts;
	private String acct;
	
	public String getPk_restspayment() {
		return pk_restspayment;
	}
	public void setPk_restspayment(String pk_restspayment) {
		this.pk_restspayment = pk_restspayment;
	}
	public String getPk_otherpayables() {
		return pk_otherpayables;
	}
	public void setPk_otherpayables(String pk_otherpayables) {
		this.pk_otherpayables = pk_otherpayables;
	}
	public String getPk_restspayment_sub() {
		return pk_restspayment_sub;
	}
	public void setPk_restspayment_sub(String pk_restspayment_sub) {
		this.pk_restspayment_sub = pk_restspayment_sub;
	}
	public String getVitem() {
		return vitem;
	}
	public void setVitem(String vitem) {
		this.vitem = vitem;
	}
	public String getPk_inspact() {
		return pk_inspact;
	}
	public void setPk_inspact(String pk_inspact) {
		this.pk_inspact = pk_inspact;
	}
	public Double getNmoney() {
		return nmoney;
	}
	public void setNmoney(Double nmoney) {
		this.nmoney = nmoney;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVbdef() {
		return vbdef;
	}
	public void setVbdef(String vbdef) {
		this.vbdef = vbdef;
	}
	public String getVbdef2() {
		return vbdef2;
	}
	public void setVbdef2(String vbdef2) {
		this.vbdef2 = vbdef2;
	}
	public String getVbdef3() {
		return vbdef3;
	}
	public void setVbdef3(String vbdef3) {
		this.vbdef3 = vbdef3;
	}
	public String getVbdef4() {
		return vbdef4;
	}
	public void setVbdef4(String vbdef4) {
		this.vbdef4 = vbdef4;
	}
	public String getVbdef5() {
		return vbdef5;
	}
	public void setVbdef5(String vbdef5) {
		this.vbdef5 = vbdef5;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	
	
}
