package com.choice.assistant.domain.PurchaseSet;

public class Inspectd {

	private String acct;			//企业ID
	private String pk_inspectd;			//验货单子表主键
	private String pk_inspect;			//验货单主表主键
	private String pk_material;			//物资主键
	private String sp_code;				//物资编码
	private String sp_name;				//物资名称
	private Double npurnum;				//采购数量
	private Double narrnum;				//到货数量
	private Double ninsnum;				//验货数量
	private Double nprice;				//单价
	private Double nmoney;				//验货金额
	private Double ndiffnum;			//差异数量
	private Double ndiffmoney;			//差异金额
	private Integer itype;				//验货类型
	private String pk_inspacter;		//验货人
	private String dinspacttime;		//验货时间
	private String vmemo;				//备注
	private String pcno;//批次
	private String vbdef1;		
	private String vbdef2;
	private String vbdef3;
	private String vbdef4;
	private String vbdef5;
	private Integer dr;					//
	private String ts;					//
	private String pk_unit;
	private String unit3;
	private String unitcode;
	private String sp_desc;
	private String pk_inspection;//验货类别主键
	private String vinspectcode;//验货类别编码
	private String vinspectname;//验货类别名称
	private double vinspectcnt;//不合格数量
	private Integer qstate;//合格状态
	private double ncheckdiffrate;//验货差异率
	private String pk_puprorder;//采购订单主表主键====用于确认验货修改采购订单状态
	private String vpuprorderbillno;//采购订单号
	private String vinspectbillno;//验货单号
	
	private Double tax;
	private String taxdes;
	
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	public Integer getQstate() {
		return qstate;
	}
	public void setQstate(Integer qstate) {
		this.qstate = qstate;
	}
	public String getPcno() {
		return pcno;
	}
	public void setPcno(String pcno) {
		this.pcno = pcno;
	}
	public double getVinspectcnt() {
		return vinspectcnt;
	}
	public void setVinspectcnt(double vinspectcnt) {
		this.vinspectcnt = vinspectcnt;
	}
	public String getPk_puprorder() {
		return pk_puprorder;
	}
	public void setPk_puprorder(String pk_puprorder) {
		this.pk_puprorder = pk_puprorder;
	}
	public double getNcheckdiffrate() {
		return ncheckdiffrate;
	}
	public void setNcheckdiffrate(double ncheckdiffrate) {
		this.ncheckdiffrate = ncheckdiffrate;
	}
	public String getPk_inspection() {
		return pk_inspection;
	}
	public void setPk_inspection(String pk_inspection) {
		this.pk_inspection = pk_inspection;
	}
	public String getVinspectcode() {
		return vinspectcode;
	}
	public void setVinspectcode(String vinspectcode) {
		this.vinspectcode = vinspectcode;
	}
	public String getVinspectname() {
		return vinspectname;
	}
	public void setVinspectname(String vinspectname) {
		this.vinspectname = vinspectname;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_inspectd() {
		return pk_inspectd;
	}
	public void setPk_inspectd(String pk_inspectd) {
		this.pk_inspectd = pk_inspectd;
	}
	public String getPk_inspect() {
		return pk_inspect;
	}
	public void setPk_inspect(String pk_inspect) {
		this.pk_inspect = pk_inspect;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public Double getNpurnum() {
		return npurnum;
	}
	public void setNpurnum(Double npurnum) {
		this.npurnum = npurnum;
	}
	public Double getNarrnum() {
		return narrnum;
	}
	public void setNarrnum(Double narrnum) {
		this.narrnum = narrnum;
	}
	public Double getNinsnum() {
		return ninsnum;
	}
	public void setNinsnum(Double ninsnum) {
		this.ninsnum = ninsnum;
	}
	public Double getNprice() {
		return nprice;
	}
	public void setNprice(Double nprice) {
		this.nprice = nprice;
	}
	public Double getNmoney() {
		return nmoney;
	}
	public void setNmoney(Double nmoney) {
		this.nmoney = nmoney;
	}
	public Double getNdiffnum() {
		return ndiffnum;
	}
	public void setNdiffnum(Double ndiffnum) {
		this.ndiffnum = ndiffnum;
	}
	public Double getNdiffmoney() {
		return ndiffmoney;
	}
	public void setNdiffmoney(Double ndiffmoney) {
		this.ndiffmoney = ndiffmoney;
	}
	public Integer getItype() {
		return itype;
	}
	public void setItype(Integer itype) {
		this.itype = itype;
	}
	public String getPk_inspacter() {
		return pk_inspacter;
	}
	public void setPk_inspacter(String pk_inspacter) {
		this.pk_inspacter = pk_inspacter;
	}
	public String getDinspacttime() {
		return dinspacttime;
	}
	public void setDinspacttime(String dinspacttime) {
		this.dinspacttime = dinspacttime;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVbdef1() {
		return vbdef1;
	}
	public void setVbdef1(String vbdef1) {
		this.vbdef1 = vbdef1;
	}
	public String getVbdef2() {
		return vbdef2;
	}
	public void setVbdef2(String vbdef2) {
		this.vbdef2 = vbdef2;
	}
	public String getVbdef3() {
		return vbdef3;
	}
	public void setVbdef3(String vbdef3) {
		this.vbdef3 = vbdef3;
	}
	public String getVbdef4() {
		return vbdef4;
	}
	public void setVbdef4(String vbdef4) {
		this.vbdef4 = vbdef4;
	}
	public String getVbdef5() {
		return vbdef5;
	}
	public void setVbdef5(String vbdef5) {
		this.vbdef5 = vbdef5;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getPk_unit() {
		return pk_unit;
	}
	public void setPk_unit(String pk_unit) {
		this.pk_unit = pk_unit;
	}
	
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getVpuprorderbillno() {
		return vpuprorderbillno;
	}
	public void setVpuprorderbillno(String vpuprorderbillno) {
		this.vpuprorderbillno = vpuprorderbillno;
	}
	public String getVinspectbillno() {
		return vinspectbillno;
	}
	public void setVinspectbillno(String vinspectbillno) {
		this.vinspectbillno = vinspectbillno;
	}
	
	
}
