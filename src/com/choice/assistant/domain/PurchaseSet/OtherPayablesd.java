package com.choice.assistant.domain.PurchaseSet;


/**
 * 其他应付款子表
 * @author liuqun 
 *
 */
public class OtherPayablesd {

	private String pk_otherpayablesd;//子表主键
	private String pk_otherpayables;//其他应付款主键
	private String pk_inspect;//入库单主键
	private String vinspectno;//入库单单号	
	private Double nmoney;//金额
	private String vmemo;
	private String vbdef1;
	private String vbdef2;
	private String vbdef3;
	private String vbdef4;
	private String vbdef5;
	private int dr;
	private String ts;
	private String acct;
	private String pk_otherpay;//关联款项主键
	private String votherpayname;//关联款项名称
	
	
	public String getPk_otherpayablesd() {
		return pk_otherpayablesd;
	}
	public void setPk_otherpayablesd(String pk_otherpayablesd) {
		this.pk_otherpayablesd = pk_otherpayablesd;
	}
	public String getPk_otherpayables() {
		return pk_otherpayables;
	}
	public void setPk_otherpayables(String pk_otherpayables) {
		this.pk_otherpayables = pk_otherpayables;
	}
	public String getPk_inspect() {
		return pk_inspect;
	}
	public void setPk_inspect(String pk_inspect) {
		this.pk_inspect = pk_inspect;
	}
	public String getVinspectno() {
		return vinspectno;
	}
	public void setVinspectno(String vinspectno) {
		this.vinspectno = vinspectno;
	}
	public Double getNmoney() {
		return nmoney;
	}
	public void setNmoney(Double nmoney) {
		this.nmoney = nmoney;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVbdef1() {
		return vbdef1;
	}
	public void setVbdef1(String vbdef1) {
		this.vbdef1 = vbdef1;
	}
	public String getVbdef2() {
		return vbdef2;
	}
	public void setVbdef2(String vbdef2) {
		this.vbdef2 = vbdef2;
	}
	public String getVbdef3() {
		return vbdef3;
	}
	public void setVbdef3(String vbdef3) {
		this.vbdef3 = vbdef3;
	}
	public String getVbdef4() {
		return vbdef4;
	}
	public void setVbdef4(String vbdef4) {
		this.vbdef4 = vbdef4;
	}
	public String getVbdef5() {
		return vbdef5;
	}
	public void setVbdef5(String vbdef5) {
		this.vbdef5 = vbdef5;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_otherpay() {
		return pk_otherpay;
	}
	public void setPk_otherpay(String pk_otherpay) {
		this.pk_otherpay = pk_otherpay;
	}
	public String getVotherpayname() {
		return votherpayname;
	}
	public void setVotherpayname(String votherpayname) {
		this.votherpayname = votherpayname;
	}
}
