package com.choice.assistant.domain.bill;

/**
 * 预订单从表
 * @author Administrator
 *
 */
public class PreOrderd {
	private String acct;
	private int pk_preorderd;
	private int pk_preorder;
	private String sp_code;
	private String sp_name;
	private String sp_desc;
	private String unit;
	private double cnt;
	
	private String delivercode;
	private String delivername;
	private double tax;
	private String taxdes;
	private String memo;
	
	private String positncode;
	private String positnname;
	
	private double amt;
	private double sp_price;
	
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public double getSp_price() {
		return sp_price;
	}
	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public String getPositnname() {
		return positnname;
	}
	public void setPositnname(String positnname) {
		this.positnname = positnname;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public int getPk_preorderd() {
		return pk_preorderd;
	}
	public void setPk_preorderd(int pk_preorderd) {
		this.pk_preorderd = pk_preorderd;
	}
	public int getPk_preorder() {
		return pk_preorder;
	}
	public void setPk_preorder(int pk_preorder) {
		this.pk_preorder = pk_preorder;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getCnt() {
		return cnt;
	}
	public void setCnt(double cnt) {
		this.cnt = cnt;
	}
	
}
