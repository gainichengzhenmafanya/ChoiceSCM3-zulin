package com.choice.assistant.domain.bill;

import java.util.List;

/**
 * 采购订单子表
 * @author LQ
 *
 */
public class PuprOrderd {

	private String acct;//企业ID
	private String pk_puprorderd;//单据子表主键
	private String pk_puprorder;//采购订单主表主键
	private String pk_material;//物料主键
	private String sp_code;//物料编码
	private String sp_name;//物料名称
	private String unit;//单位
	private String sp_desc;//规格
	private double ncnt;//数量
	private double sp_price;//单价
	private double nmoney;//金额
	private String vmemo;//备注
	private int dr;
	private String ts;
	private String positncode;
	private String positnname;
	private List<String> pklist;//主表主键集合 == 用作查询过滤条件
	private String delivercode;
	private String delivername;
	private double nminamt;//冗余最小采购量
	private double ncheckdiffrate;//物资验货差异率
	private int istate;//主表单据状态
	private int beditprice;//物资价格是否来自合约 1是0否
	
	private String pk_chkstod;//记录报货单pk
	private String vbillno;//主表订单号
	private String dhopedate;//期望到货日
	private String pk_unit;
	private List<PuprOrderd> listPuprOrderd;
	
	private String  pk_material_jmu;
	private String vmaterialjumcode;
	private String vmaterialjumname;
	private String vspecfication;
	private double  nprice;
	private String vunitcode;  
	private String  vunitname;
	
	
	public static String TABLENAME = "cscm_puprorder_d";
	public static String PK_ID = "pk_puprorderd";
	
	private Double tax;
	private String taxdes;
	
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getTaxdes() {
		return taxdes;
	}
	public void setTaxdes(String taxdes) {
		this.taxdes = taxdes;
	}
	public String getVunitname() {
		return vunitname;
	}
	public void setVunitname(String vunitname) {
		this.vunitname = vunitname;
	}
	public String getVunitcode() {
		return vunitcode;
	}
	public void setVunitcode(String vunitcode) {
		this.vunitcode = vunitcode;
	}
	public double getNprice() {
		return nprice;
	}
	public void setNprice(double nprice) {
		this.nprice = nprice;
	}
	public String getVspecfication() {
		return vspecfication;
	}
	public void setVspecfication(String vspecfication) {
		this.vspecfication = vspecfication;
	}
	public String getVmaterialjumname() {
		return vmaterialjumname;
	}
	public void setVmaterialjumname(String vmaterialjumname) {
		this.vmaterialjumname = vmaterialjumname;
	}
	public String getVmaterialjumcode() {
		return vmaterialjumcode;
	}
	public void setVmaterialjumcode(String vmaterialjumcode) {
		this.vmaterialjumcode = vmaterialjumcode;
	}
	public String getPk_material_jmu() {
		return pk_material_jmu;
	}
	public void setPk_material_jmu(String pk_material_jmu) {
		this.pk_material_jmu = pk_material_jmu;
	}
	public List<PuprOrderd> getListPuprOrderd() {
		return listPuprOrderd;
	}
	public void setListPuprOrderd(List<PuprOrderd> listPuprOrderd) {
		this.listPuprOrderd = listPuprOrderd;
	}
	public String getPk_unit() {
		return pk_unit;
	}
	public void setPk_unit(String pk_unit) {
		this.pk_unit = pk_unit;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_puprorderd() {
		return pk_puprorderd;
	}
	public void setPk_puprorderd(String pk_puprorderd) {
		this.pk_puprorderd = pk_puprorderd;
	}
	public String getPk_puprorder() {
		return pk_puprorder;
	}
	public void setPk_puprorder(String pk_puprorder) {
		this.pk_puprorder = pk_puprorder;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSp_desc() {
		return sp_desc;
	}
	public void setSp_desc(String sp_desc) {
		this.sp_desc = sp_desc;
	}
	public double getNcnt() {
		return ncnt;
	}
	public void setNcnt(double ncnt) {
		this.ncnt = ncnt;
	}
	public double getSp_price() {
		return sp_price;
	}
	public void setSp_price(double sp_price) {
		this.sp_price = sp_price;
	}
	public double getNmoney() {
		return nmoney;
	}
	public void setNmoney(double nmoney) {
		this.nmoney = nmoney;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public String getPositnname() {
		return positnname;
	}
	public void setPositnname(String positnname) {
		this.positnname = positnname;
	}
	public List<String> getPklist() {
		return pklist;
	}
	public void setPklist(List<String> pklist) {
		this.pklist = pklist;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public double getNminamt() {
		return nminamt;
	}
	public void setNminamt(double nminamt) {
		this.nminamt = nminamt;
	}
	public double getNcheckdiffrate() {
		return ncheckdiffrate;
	}
	public void setNcheckdiffrate(double ncheckdiffrate) {
		this.ncheckdiffrate = ncheckdiffrate;
	}
	public int getIstate() {
		return istate;
	}
	public void setIstate(int istate) {
		this.istate = istate;
	}
	public int getBeditprice() {
		return beditprice;
	}
	public void setBeditprice(int beditprice) {
		this.beditprice = beditprice;
	}
	public String getPk_chkstod() {
		return pk_chkstod;
	}
	public void setPk_chkstod(String pk_chkstod) {
		this.pk_chkstod = pk_chkstod;
	}
	public String getVbillno() {
		return vbillno;
	}
	public void setVbillno(String vbillno) {
		this.vbillno = vbillno;
	}
	public String getDhopedate() {
		return dhopedate;
	}
	public void setDhopedate(String dhopedate) {
		this.dhopedate = dhopedate;
	}
}
