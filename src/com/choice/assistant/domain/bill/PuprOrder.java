package com.choice.assistant.domain.bill;

import java.util.List;

/**
 * 采购订单主表
 * @author LQ
 *
 */
public class PuprOrder {

	private String acct;//企业ID
	private String pk_puprorder;//订单主键
	private String vbillno;//采购单单号
	private String vbillno2;//采购单辅助号
	private String vbillno3;//采购单辅助号
	private String positncode;//采购组织
	private String positnname;//采购组织
	private String delivercode;//供应商主键
	private String delivername;//采购组织
	private int ibilltype;//单据类型
	private String dbilldate;//单据日期
	private int istate;//单据状态 1未提交 2待发货 3已发货 4已到货 5已验货 6已入库 7已结算   8待备货  9备货中 10备货完成  -1已撤销 100订单补录
	private String pk_account;//制单人ID
	private String dmaketime;//制单时间
	private String vpurchaser;//采购员
	private String addr;//配送地址主键
	private String vmemo;//备注
	private double nmoney;//订单总金额
	private int ibillfrom;//订单来源1商城 2ERP 3手工补录
	private String vfrombillno;//来源单号
	private String dhopedate;//要求到货日
	private String dsenddate;//供应商发货日
	private String dreceivedate;//到货日
	private String acct1;//集团1
	private String acct2;//集团2
	private String acct3;//集团3
	private String vdef1;//自定义项1
	private String vdef2;//自定义项2
	private String vdef3;//自定义项3
	private String vdef4;//自定义项4
	private String vdef5;//自定义项5
	private int dr;
	private String ts;
	
	private String pk_purtemplet;//清单主键
	private String detailedname;//清单名称
	private String pks;//主键集合
	private Integer isdeal;//是否已处理 0否1是
	private String modifier;//修改人
	private String modifedtime;//修改时间
	private String vcontact;//联系人
	private String vcontactphone;//联系电话
	private String vdistributionaddr;//配送地址
	private String listPk_supplier;//供应商主键集合
	private String listSupplierCode;//供应商编码集合
	private String listSupplierName;//供应商名称集合
	private List<String> listpksupplier;
	private String vstate;
	//----------------------查询条件--------------------------------
	public String bdate;
	public String edate;
	private String accountname;//制单人
	private List<String> pklsit;//主键集合
	//----------------------查询条件--------------------------------
	private String contact;//联系人
	private String contactphone;//联系电话
	private String distributionaddr;//配送地址
	private String vbillsn;//商城生成的订单号
	
	private String vevaluate;//评价信息
	
	private String pk_preorders;//回写预订单编号
	
	public String getPk_preorders() {
		return pk_preorders;
	}
	public void setPk_preorders(String pk_preorders) {
		this.pk_preorders = pk_preorders;
	}
	private String pk_addr;
	private String pk_transation;
	
	private String hasCreate;
	
	public String getHasCreate() {
		return hasCreate;
	}
	public void setHasCreate(String hasCreate) {
		this.hasCreate = hasCreate;
	}
	public String getPk_transation() {
		return pk_transation;
	}
	public void setPk_transation(String pk_transation) {
		this.pk_transation = pk_transation;
	}
	public String getPk_addr() {
		return pk_addr;
	}
	public void setPk_addr(String pk_addr) {
		this.pk_addr = pk_addr;
	}
	public List<String> getListpksupplier() {
		return listpksupplier;
	}
	public void setListpksupplier(List<String> listpksupplier) {
		this.listpksupplier = listpksupplier;
	}
	public String getListSupplierCode() {
		return listSupplierCode;
	}
	public void setListSupplierCode(String listSupplierCode) {
		this.listSupplierCode = listSupplierCode;
	}
	public String getListSupplierName() {
		return listSupplierName;
	}
	public void setListSupplierName(String listSupplierName) {
		this.listSupplierName = listSupplierName;
	}
	public String getVcontact() {
		return vcontact;
	}
	public void setVcontact(String vcontact) {
		this.vcontact = vcontact;
	}
	public String getVcontactphone() {
		return vcontactphone;
	}
	public void setVcontactphone(String vcontactphone) {
		this.vcontactphone = vcontactphone;
	}
	public String getVdistributionaddr() {
		return vdistributionaddr;
	}
	public void setVdistributionaddr(String vdistributionaddr) {
		this.vdistributionaddr = vdistributionaddr;
	}

	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getModifedtime() {
		return modifedtime;
	}
	public void setModifedtime(String modifedtime) {
		this.modifedtime = modifedtime;
	}
	public String getListPk_supplier() {
		return listPk_supplier;
	}
	public void setListPk_supplier(String listPk_supplier) {
		this.listPk_supplier = listPk_supplier;
	}
	public String getPks() {
		return pks;
	}
	public void setPks(String pks) {
		this.pks = pks;
	}
	public String getPk_purtemplet() {
		return pk_purtemplet;
	}
	public void setPk_purtemplet(String pk_purtemplet) {
		this.pk_purtemplet = pk_purtemplet;
	}
	public String getDetailedname() {
		return detailedname;
	}
	public void setDetailedname(String detailedname) {
		this.detailedname = detailedname;
	}
	private List<PuprOrderd> puprOrderdList;
	
	public static String TABLENAME = "cscm_puprorder_m";
	public static String PK_ID = "pk_puprorder";
	
	public List<String> getPklsit() {
		return pklsit;
	}
	public void setPklsit(List<String> pklsit) {
		this.pklsit = pklsit;
	}
	public String getAccountname() {
		return accountname;
	}
	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_puprorder() {
		return pk_puprorder;
	}
	public void setPk_puprorder(String pk_puprorder) {
		this.pk_puprorder = pk_puprorder;
	}
	public String getVbillno() {
		return vbillno;
	}
	public void setVbillno(String vbillno) {
		this.vbillno = vbillno;
	}
	public int getIbilltype() {
		return ibilltype;
	}
	public void setIbilltype(int ibilltype) {
		this.ibilltype = ibilltype;
	}
	public String getDbilldate() {
		return dbilldate;
	}
	public void setDbilldate(String dbilldate) {
		this.dbilldate = dbilldate;
	}
	public int getIstate() {
		return istate;
	}
	public void setIstate(int istate) {
		this.istate = istate;
	}
	public String getPk_account() {
		return pk_account;
	}
	public void setPk_account(String pk_account) {
		this.pk_account = pk_account;
	}
	public String getDmaketime() {
		return dmaketime;
	}
	public void setDmaketime(String dmaketime) {
		this.dmaketime = dmaketime;
	}
	public String getVpurchaser() {
		return vpurchaser;
	}
	public void setVpurchaser(String vpurchaser) {
		this.vpurchaser = vpurchaser;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public double getNmoney() {
		return nmoney;
	}
	public void setNmoney(double nmoney) {
		this.nmoney = nmoney;
	}
	public int getIbillfrom() {
		return ibillfrom;
	}
	public void setIbillfrom(int ibillfrom) {
		this.ibillfrom = ibillfrom;
	}
	public String getVfrombillno() {
		return vfrombillno;
	}
	public void setVfrombillno(String vfrombillno) {
		this.vfrombillno = vfrombillno;
	}
	public String getDhopedate() {
		return dhopedate;
	}
	public void setDhopedate(String dhopedate) {
		this.dhopedate = dhopedate;
	}
	public String getDsenddate() {
		return dsenddate;
	}
	public void setDsenddate(String dsenddate) {
		this.dsenddate = dsenddate;
	}
	public String getDreceivedate() {
		return dreceivedate;
	}
	public void setDreceivedate(String dreceivedate) {
		this.dreceivedate = dreceivedate;
	}
	public String getAcct1() {
		return acct1;
	}
	public void setAcct1(String acct1) {
		this.acct1 = acct1;
	}
	public String getAcct2() {
		return acct2;
	}
	public void setAcct2(String acct2) {
		this.acct2 = acct2;
	}
	public String getAcct3() {
		return acct3;
	}
	public void setAcct3(String acct3) {
		this.acct3 = acct3;
	}
	public String getVdef1() {
		return vdef1;
	}
	public void setVdef1(String vdef1) {
		this.vdef1 = vdef1;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public List<PuprOrderd> getPuprOrderdList() {
		return puprOrderdList;
	}
	public void setPuprOrderdList(List<PuprOrderd> puprOrderdList) {
		this.puprOrderdList = puprOrderdList;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public Integer getIsdeal() {
		return isdeal;
	}
	public void setIsdeal(Integer isdeal) {
		this.isdeal = isdeal;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getContactphone() {
		return contactphone;
	}
	public void setContactphone(String contactphone) {
		this.contactphone = contactphone;
	}
	public String getDistributionaddr() {
		return distributionaddr;
	}
	public void setDistributionaddr(String distributionaddr) {
		this.distributionaddr = distributionaddr;
	}
	public String getVbillsn() {
		return vbillsn;
	}
	public void setVbillsn(String vbillsn) {
		this.vbillsn = vbillsn;
	}
	public static String getTABLENAME() {
		return TABLENAME;
	}
	public static void setTABLENAME(String tABLENAME) {
		TABLENAME = tABLENAME;
	}
	public String getVbillno2() {
		return vbillno2;
	}
	public void setVbillno2(String vbillno2) {
		this.vbillno2 = vbillno2;
	}
	public String getVbillno3() {
		return vbillno3;
	}
	public void setVbillno3(String vbillno3) {
		this.vbillno3 = vbillno3;
	}
	public String getVstate() {
		return vstate;
	}
	public void setVstate(String vstate) {
		this.vstate = vstate;
	}
	public String getVevaluate() {
		return vevaluate;
	}
	public void setVevaluate(String vevaluate) {
		this.vevaluate = vevaluate;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public String getPositnname() {
		return positnname;
	}
	public void setPositnname(String positnname) {
		this.positnname = positnname;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
}
