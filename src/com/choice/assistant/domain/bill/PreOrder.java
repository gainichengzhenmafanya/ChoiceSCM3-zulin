package com.choice.assistant.domain.bill;

import java.util.List;

/**
 * 预订单主表
 * @author Administrator
 *
 */
public class PreOrder {
	private String acct;//账套
	private int pk_preorder;//主键
	private String positncode;//仓位编码
	private String positnname;//仓位名称
	private String dtype;//单据类型1未生成采购订单，2已生成采购订单
	private String ddate;//订单日期
	private String dtime;//订单时间
	private String accountId;//账号
	private String accountName;
	private List<PreOrderd> preOrderds;//子表
	private String bdate;
	private String edate;
	private String delivercode;
	private String delivername;
	private String cwqx;//是否走仓位权限
	
	public String getCwqx() {
		return cwqx;
	}
	public void setCwqx(String cwqx) {
		this.cwqx = cwqx;
	}
	private String pk_preorders;//主键多个
	
	public String getPk_preorders() {
		return pk_preorders;
	}
	public void setPk_preorders(String pk_preorders) {
		this.pk_preorders = pk_preorders;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public int getPk_preorder() {
		return pk_preorder;
	}
	public void setPk_preorder(int pk_preorder) {
		this.pk_preorder = pk_preorder;
	}
	public String getPositncode() {
		return positncode;
	}
	public void setPositncode(String positncode) {
		this.positncode = positncode;
	}
	public String getPositnname() {
		return positnname;
	}
	public void setPositnname(String positnname) {
		this.positnname = positnname;
	}
	public String getDtype() {
		return dtype;
	}
	public void setDtype(String dtype) {
		this.dtype = dtype;
	}
	public String getDdate() {
		return ddate;
	}
	public void setDdate(String ddate) {
		this.ddate = ddate;
	}
	public String getDtime() {
		return dtime;
	}
	public void setDtime(String dtime) {
		this.dtime = dtime;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public List<PreOrderd> getPreOrderds() {
		return preOrderds;
	}
	public void setPreOrderds(List<PreOrderd> preOrderds) {
		this.preOrderds = preOrderds;
	}
}
