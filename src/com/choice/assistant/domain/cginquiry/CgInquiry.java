package com.choice.assistant.domain.cginquiry;

import java.util.List;

/**
 * 采购询价
 * @author xiaoguai
 *
 */
public class CgInquiry {

	private String pk_cginquiry;//主键
	private String acct;//企业id
	private String pk_material;//物资主键
	private String sp_code;//物资编码
	private String sp_name;//物资名称
	private String delivercode;//供应商主键
	private String delivername;//供应商名称
	private String vsuppliercontact;//供应商联系人
	private String vsupplierphone;//供应商联系电话
	private String vinquirycontact;//询价人
	private String dinquirybdat;//询价时间
	private String dinquiryedat;//到期时间
	private String pk_inquiry;//询价类型主键
	private String vinquirycode;//询价类型编码
	private String vinquiryname;//询价类型名称
	private double ninquiryprice;//询价价格
	private String vmemo;//备注
	private int dr;
	private String ts;
	private List<String> listpk_cginquiry;//主键集合
	
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getDelivercode() {
		return delivercode;
	}
	public void setDelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getDelivername() {
		return delivername;
	}
	public void setDelivername(String delivername) {
		this.delivername = delivername;
	}
	public List<String> getListpk_cginquiry() {
		return listpk_cginquiry;
	}
	public void setListpk_cginquiry(List<String> listpk_cginquiry) {
		this.listpk_cginquiry = listpk_cginquiry;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getPk_cginquiry() {
		return pk_cginquiry;
	}
	public void setPk_cginquiry(String pk_cginquiry) {
		this.pk_cginquiry = pk_cginquiry;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public String getVsuppliercontact() {
		return vsuppliercontact;
	}
	public void setVsuppliercontact(String vsuppliercontact) {
		this.vsuppliercontact = vsuppliercontact;
	}
	public String getVsupplierphone() {
		return vsupplierphone;
	}
	public void setVsupplierphone(String vsupplierphone) {
		this.vsupplierphone = vsupplierphone;
	}
	public String getVinquirycontact() {
		return vinquirycontact;
	}
	public void setVinquirycontact(String vinquirycontact) {
		this.vinquirycontact = vinquirycontact;
	}
	public String getDinquirybdat() {
		return dinquirybdat;
	}
	public void setDinquirybdat(String dinquirybdat) {
		this.dinquirybdat = dinquirybdat;
	}
	public String getDinquiryedat() {
		return dinquiryedat;
	}
	public void setDinquiryedat(String dinquiryedat) {
		this.dinquiryedat = dinquiryedat;
	}
	public String getPk_inquiry() {
		return pk_inquiry;
	}
	public void setPk_inquiry(String pk_inquiry) {
		this.pk_inquiry = pk_inquiry;
	}
	public String getVinquirycode() {
		return vinquirycode;
	}
	public void setVinquirycode(String vinquirycode) {
		this.vinquirycode = vinquirycode;
	}
	public String getVinquiryname() {
		return vinquiryname;
	}
	public void setVinquiryname(String vinquiryname) {
		this.vinquiryname = vinquiryname;
	}
	public double getNinquiryprice() {
		return ninquiryprice;
	}
	public void setNinquiryprice(double ninquiryprice) {
		this.ninquiryprice = ninquiryprice;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	
	
}
