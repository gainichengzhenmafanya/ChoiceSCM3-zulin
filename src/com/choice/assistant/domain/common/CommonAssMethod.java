package com.choice.assistant.domain.common;
/**
 * 类说明
 * @author zgl
 * @version 创建时间：2014-10-27 21:51:32
 */
public class CommonAssMethod {
	
	private String vcode;// 编码值
	private String vcodename;// 编码字段名称
	private String vtablename;// 表名称
	private String isortno;// 排序列字段名称
	private String visortnoName;//排序字段名称
	
	private String enablestate;//2启用，3禁用
	private String ids;//设置是否启用的id数据集合
	private String pk_id;//id字段名称
	
	private String pk_id_name;//主键名称
	
	private String pk_store;//门店主键
	private String modulename;//模块名称 加日志用到
	private String params;//过滤条件
	private String acct;//多企业过滤用
	private String parent;//父级节点列名
	private String parentVal;//父级节点值
	
	public String getPk_id_name() {
		return pk_id_name;
	}
	public void setPk_id_name(String pk_id_name) {
		this.pk_id_name = pk_id_name;
	}
	public String getVisortnoName() {
		return visortnoName;
	}
	public void setVisortnoName(String visortnoName) {
		this.visortnoName = visortnoName;
	}
	public String getIsortno() {
		return isortno;
	}
	public void setIsortno(String isortno) {
		this.isortno = isortno;
	}
	public String getVcodename() {
		return vcodename;
	}
	public void setVcodename(String vcodename) {
		this.vcodename = vcodename;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVtablename() {
		return vtablename;
	}
	public void setVtablename(String vtablename) {
		this.vtablename = vtablename;
	}
	/**
	 * @return enablestate
	 */
	public String getEnablestate() {
		return enablestate;
	}
	/**
	 * @param enablestate 要设置的 enablestate
	 */
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	/**
	 * @return ids
	 */
	public String getIds() {
		return ids;
	}
	/**
	 * @param ids 要设置的 ids
	 */
	public void setIds(String ids) {
		this.ids = ids;
	}
	/**
	 * @return pk_id
	 */
	public String getPk_id() {
		return pk_id;
	}
	/**
	 * @param pk_id 要设置的 pk_id
	 */
	public void setPk_id(String pk_id) {
		this.pk_id = pk_id;
	}
	public String getPk_store() {
		return pk_store;
	}
	public void setPk_store(String pk_store) {
		this.pk_store = pk_store;
	}
	public String getModulename() {
		return modulename;
	}
	public void setModulename(String modulename) {
		this.modulename = modulename;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getParentVal() {
		return parentVal;
	}
	public void setParentVal(String parentVal) {
		this.parentVal = parentVal;
	}

}
