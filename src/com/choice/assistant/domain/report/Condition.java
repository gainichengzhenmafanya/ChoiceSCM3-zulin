package com.choice.assistant.domain.report;

import java.util.ArrayList;
import java.util.List;

import com.choice.assistant.util.NoUse;
import com.choice.orientationSys.util.Page;

/**
 * 会员报表的查询条件
 * @author lq
 *
 */
public class Condition {

	@NoUse
	private int page;	//请求页
	@NoUse
	private Page pager;	//分页对象
	@NoUse
	private int rows;	//每页行数
	private String sort;					//排序字段
	private String order;					//排序方式
    private String numCols; //数字字段
    private int decimal;//保留小数位
	
	private String bdat;	//开始日期
	private String edat;  //结束日期
	private String billno;//订单号

    private String positncode;//采购组织

    private String acct;
    private String pk_material;//物资编码
    private String pk_materialtype;//物资类别编码
    private String delivercode;//供应商
    private String thisMonth;//本月
    private Integer month;//月份
    private Integer big;//本月波动大于
    private Integer small;//本月波动小于

    private List<String> supplierPks=new ArrayList<String>();
    private List<String> materialPks=new ArrayList<String>();
    private List<String> materialtypePks=new ArrayList<String>();
    private List<String> orgPks=new ArrayList<String>();

    public Condition(){
		pager = new Page();
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
		pager.setNowPage(page);
	}

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
		pager.setPageSize(rows);
	}

	public String getBdat() {
		return bdat;
	}

	public void setBdat(String bdat) {
		this.bdat = bdat;
	}

	public String getEdat() {
		return edat;
	}

	public void setEdat(String edat) {
		this.edat = edat;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getBillno() {
		return billno;
	}

	public void setBillno(String billno) {
		this.billno = billno;
	}

    public String getPk_material() {
        return pk_material;
    }

    public void setPk_material(String pk_material) {
        this.pk_material = pk_material;
    }

    public String getPk_materialtype() {
        return pk_materialtype;
    }

    public void setPk_materialtype(String pk_materialtype) {
        this.pk_materialtype = pk_materialtype;
    }

    public String getThisMonth() {
        return thisMonth;
    }

    public void setThisMonth(String thisMonth) {
        this.thisMonth = thisMonth;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getBig() {
        return big;
    }

    public void setBig(Integer big) {
        this.big = big;
    }

    public Integer getSmall() {
        return small;
    }

    public void setSmall(Integer small) {
        this.small = small;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_supplier() {
        return delivercode;
    }

    public void setPk_supplier(String delivercode) {
        this.delivercode = delivercode;
    }

    public String getNumCols() {
        return numCols;
    }

    public void setNumCols(String numCols) {
        this.numCols = numCols;
    }

    public int getDecimal() {
        return decimal;
    }

    public void setDecimal(int decimal) {
        this.decimal = decimal;
    }

    public String getPk_org() {
        return positncode;
    }

    public void setPk_org(String positncode) {
        this.positncode = positncode;
    }

    public List<String> getMaterialPks() {
        return materialPks;
    }

    public void setMaterialPks(List<String> materialPks) {
        this.materialPks = materialPks;
    }

    public List<String> getMaterialtypePks() {
        return materialtypePks;
    }

    public void setMaterialtypePks(List<String> materialtypePks) {
        this.materialtypePks = materialtypePks;
    }

    public List<String> getOrgPks() {
        return orgPks;
    }

    public void setOrgPks(List<String> orgPks) {
        this.orgPks = orgPks;
    }

    public List<String> getSupplierPks() {
        return supplierPks;
    }

    public void setSupplierPks(List<String> supplierPks) {
        this.supplierPks = supplierPks;
    }
}
