package com.choice.assistant.domain.system;


/**
 * 常见问题
 * Created by mc on 15/2/3.
 */
public class Problems {
    private String pk_problems;
    private String vproblems;
    private String vanswer;
    private String acct;
    private String ts;
    private Integer dr=0;

    public String getPk_problems() {
        return pk_problems;
    }

    public void setPk_problems(String pk_problems) {
        this.pk_problems = pk_problems;
    }

    public String getVproblems() {
        return vproblems;
    }

    public void setVproblems(String vproblems) {
        this.vproblems = vproblems;
    }

    public String getVanswer() {
        return vanswer;
    }

    public void setVanswer(String vanswer) {
        this.vanswer = vanswer;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }
}
