package com.choice.assistant.domain.system;
/**
 * 付款方式
 * @author ZGL
 * 2014-12-07 16:49:11
 *
 */
public class PayMethod {
	
	private String pk_paymethod;		//主键
	private String vcode;				//编码
	private String vname;				//名称
	private Integer enablestate=2;		//
	private Integer dr;					
	private String acct;			//
	private String ts;
	
	public String getPk_paymethod() {
		return pk_paymethod;
	}
	public void setPk_paymethod(String pk_paymethod) {
		this.pk_paymethod = pk_paymethod;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	
	
}
