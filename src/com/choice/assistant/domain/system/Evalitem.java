package com.choice.assistant.domain.system;

import java.util.List;

/**
 * 评价项目
 * Created by mc on 14-10-23.
 */
public class Evalitem {
    private String acct;
    private String pk_evalitem; //主键
    private String vcode;       //编码
    private String vstandard;   //指标
    private String vweight;     //权重
    private Integer enablestate;//启用状态
    private String vmemo;       //备注
    private Integer dr;         //
    private String ts;          //
    private String vbdef;
    private String vbdef2;
    private String vbdef3;
    private String vbdef4;
    private String vbdef5;

    private List<Evalitem> evalitems;
    private List<Evalitem> delevalitems;

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_evalitem() {
        return pk_evalitem;
    }

    public void setPk_evalitem(String pk_evalitem) {
        this.pk_evalitem = pk_evalitem;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

    public String getVstandard() {
        return vstandard;
    }

    public void setVstandard(String vstandard) {
        this.vstandard = vstandard;
    }

    public String getVweight() {
        return vweight;
    }

    public void setVweight(String vweight) {
        this.vweight = vweight;
    }

    public Integer getEnablestate() {
        return enablestate;
    }

    public void setEnablestate(Integer enablestate) {
        this.enablestate = enablestate;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVbdef() {
        return vbdef;
    }

    public void setVbdef(String vbdef) {
        this.vbdef = vbdef;
    }

    public String getVbdef2() {
        return vbdef2;
    }

    public void setVbdef2(String vbdef2) {
        this.vbdef2 = vbdef2;
    }

    public String getVbdef3() {
        return vbdef3;
    }

    public void setVbdef3(String vbdef3) {
        this.vbdef3 = vbdef3;
    }

    public String getVbdef4() {
        return vbdef4;
    }

    public void setVbdef4(String vbdef4) {
        this.vbdef4 = vbdef4;
    }

    public String getVbdef5() {
        return vbdef5;
    }

    public void setVbdef5(String vbdef5) {
        this.vbdef5 = vbdef5;
    }

    public List<Evalitem> getEvalitems() {
        return evalitems;
    }

    public void setEvalitems(List<Evalitem> evalitems) {
        this.evalitems = evalitems;
    }

    public String getVmemo() {
        return vmemo;
    }

    public void setVmemo(String vmemo) {
        this.vmemo = vmemo;
    }

    public List<Evalitem> getDelevalitems() {
        return delevalitems;
    }

    public void setDelevalitems(List<Evalitem> delevalitems) {
        this.delevalitems = delevalitems;
    }
}
