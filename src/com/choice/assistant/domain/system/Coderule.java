package com.choice.assistant.domain.system;

/**
 * 编码规则
 * Created by mc on 14-10-18.
 */
public class Coderule {
    private String pk_coderule;  //主键
    private String vprojectname; //项目名称
    private Integer icodeseries; //编码级数
    private Integer enablestate; //启用状态
    private Integer itotallength=0; //总长度
    private Integer isrule;      //是否是编码规则 1 单据规则 2编码规则
    private String vmaxvalue;     //最大值
    private String positncode;
    private Integer ilength1;    //编码1长度
    private String itype1;       //编码1类型
    private String vvalue1;      //编码1值
    private Integer ilength2;
    private String itype2;
    private String vvalue2;
    private Integer ilength3;
    private String itype3;
    private String vvalue3;
    private Integer ilength4;
    private String itype4;
    private String vvalue4;
    private Integer ilength5;
    private String itype5;
    private String vvalue5;
    private Integer ilength6;
    private String itype6;
    private String vvalue6;
    private Integer ilength7;
    private String itype7;
    private String vvalue7;
    private String vpreview;

    private String vmax1;//最大值
    private String vmax2;
    private String vmax3;
    private String vmax4;
    private String vmax5;
    private String vmax6;
    private String vmax7;
    private String vmax8;

    private String vcet1;//当前流水标示
    private String vcet2;
    private String vcet3;
    private String vcet4;
    private String vcet5;
    private String vcet6;
    private String vcet7;
    private String vcet8;
    private String acct;

    private Integer ilength;
    private String itype;
    private String vvalue;
    private String vmax;
    private String vcet;

    private String ts;

    private String typecode;//类别编码
    private String parentcode;//上级编码
    private Integer lvl;//目前级别数

    public String getPk_coderule() {
        return pk_coderule;
    }

    public void setPk_coderule(String pk_coderule) {
        this.pk_coderule = pk_coderule;
    }

    public String getVprojectname() {
        return vprojectname;
    }

    public void setVprojectname(String vprojectname) {
        this.vprojectname = vprojectname;
    }

    public Integer getIcodeseries() {
        return icodeseries;
    }

    public void setIcodeseries(Integer icodeseries) {
        this.icodeseries = icodeseries;
    }

    public Integer getEnablestate() {
        return enablestate;
    }

    public void setEnablestate(Integer enablestate) {
        this.enablestate = enablestate;
    }

    public Integer getIlength1() {
        return ilength1;
    }

    public void setIlength1(Integer ilength1) {
        this.ilength1 = ilength1;
    }

    public String getItype1() {
        return itype1;
    }

    public void setItype1(String itype1) {
        this.itype1 = itype1;
    }

    public String getVvalue1() {
        return vvalue1;
    }

    public void setVvalue1(String vvalue1) {
        this.vvalue1 = vvalue1;
    }

    public Integer getIlength2() {
        return ilength2;
    }

    public void setIlength2(Integer ilength2) {
        this.ilength2 = ilength2;
    }

    public String getItype2() {
        return itype2;
    }

    public void setItype2(String itype2) {
        this.itype2 = itype2;
    }

    public String getVvalue2() {
        return vvalue2;
    }

    public void setVvalue2(String vvalue2) {
        this.vvalue2 = vvalue2;
    }

    public Integer getIlength3() {
        return ilength3;
    }

    public void setIlength3(Integer ilength3) {
        this.ilength3 = ilength3;
    }

    public String getItype3() {
        return itype3;
    }

    public void setItype3(String itype3) {
        this.itype3 = itype3;
    }

    public String getVvalue3() {
        return vvalue3;
    }

    public void setVvalue3(String vvalue3) {
        this.vvalue3 = vvalue3;
    }

    public Integer getIlength4() {
        return ilength4;
    }

    public void setIlength4(Integer ilength4) {
        this.ilength4 = ilength4;
    }

    public String getItype4() {
        return itype4;
    }

    public void setItype4(String itype4) {
        this.itype4 = itype4;
    }

    public String getVvalue4() {
        return vvalue4;
    }

    public void setVvalue4(String vvalue4) {
        this.vvalue4 = vvalue4;
    }

    public Integer getIlength5() {
        return ilength5;
    }

    public void setIlength5(Integer ilength5) {
        this.ilength5 = ilength5;
    }

    public String getItype5() {
        return itype5;
    }

    public void setItype5(String itype5) {
        this.itype5 = itype5;
    }

    public String getVvalue5() {
        return vvalue5;
    }

    public void setVvalue5(String vvalue5) {
        this.vvalue5 = vvalue5;
    }

    public Integer getIlength6() {
        return ilength6;
    }

    public void setIlength6(Integer ilength6) {
        this.ilength6 = ilength6;
    }

    public String getItype6() {
        return itype6;
    }

    public void setItype6(String itype6) {
        this.itype6 = itype6;
    }

    public String getVvalue6() {
        return vvalue6;
    }

    public void setVvalue6(String vvalue6) {
        this.vvalue6 = vvalue6;
    }

    public Integer getIlength7() {
        return ilength7;
    }

    public void setIlength7(Integer ilength7) {
        this.ilength7 = ilength7;
    }

    public String getItype7() {
        return itype7;
    }

    public void setItype7(String itype7) {
        this.itype7 = itype7;
    }

    public String getVvalue7() {
        return vvalue7;
    }

    public void setVvalue7(String vvalue7) {
        this.vvalue7 = vvalue7;
    }

    public String getVpreview() {
        return vpreview;
    }

    public void setVpreview(String vpreview) {
        this.vpreview = vpreview;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public Integer getItotallength() {
        return itotallength;
    }

    public void setItotallength(Integer itotallength) {
        this.itotallength = itotallength;
    }

    public String getVmaxvalue() {
        return vmaxvalue;
    }

    public void setVmaxvalue(String vmaxvalue) {
        this.vmaxvalue = vmaxvalue;
    }

    public Integer getIsrule() {
        return isrule;
    }

    public void setIsrule(Integer isrule) {
        this.isrule = isrule;
    }

    public String getTypecode() {
        return typecode;
    }

    public void setTypecode(String typecode) {
        this.typecode = typecode;
    }

    public String getParentcode() {
        return parentcode;
    }

    public void setParentcode(String parentcode) {
        this.parentcode = parentcode;
    }

    public Integer getLvl() {
        return lvl;
    }

    public void setLvl(Integer lvl) {
        this.lvl = lvl;
    }

    public String getVmax1() {
        return vmax1;
    }

    public void setVmax1(String vmax1) {
        this.vmax1 = vmax1;
    }

    public String getVmax2() {
        return vmax2;
    }

    public void setVmax2(String vmax2) {
        this.vmax2 = vmax2;
    }

    public String getVmax3() {
        return vmax3;
    }

    public void setVmax3(String vmax3) {
        this.vmax3 = vmax3;
    }

    public String getVmax4() {
        return vmax4;
    }

    public void setVmax4(String vmax4) {
        this.vmax4 = vmax4;
    }

    public String getVmax5() {
        return vmax5;
    }

    public void setVmax5(String vmax5) {
        this.vmax5 = vmax5;
    }

    public String getVmax6() {
        return vmax6;
    }

    public void setVmax6(String vmax6) {
        this.vmax6 = vmax6;
    }

    public String getVmax7() {
        return vmax7;
    }

    public void setVmax7(String vmax7) {
        this.vmax7 = vmax7;
    }

    public String getVcet1() {
        return vcet1;
    }

    public void setVcet1(String vcet1) {
        this.vcet1 = vcet1;
    }

    public String getVcet2() {
        return vcet2;
    }

    public void setVcet2(String vcet2) {
        this.vcet2 = vcet2;
    }

    public String getVcet3() {
        return vcet3;
    }

    public void setVcet3(String vcet3) {
        this.vcet3 = vcet3;
    }

    public String getVcet4() {
        return vcet4;
    }

    public void setVcet4(String vcet4) {
        this.vcet4 = vcet4;
    }

    public String getVcet5() {
        return vcet5;
    }

    public void setVcet5(String vcet5) {
        this.vcet5 = vcet5;
    }

    public String getVcet6() {
        return vcet6;
    }

    public void setVcet6(String vcet6) {
        this.vcet6 = vcet6;
    }

    public String getVcet7() {
        return vcet7;
    }

    public void setVcet7(String vcet7) {
        this.vcet7 = vcet7;
    }

    public Integer getIlength() {
        return ilength;
    }

    public void setIlength(Integer ilength) {
        this.ilength = ilength;
    }

    public String getItype() {
        return itype;
    }

    public void setItype(String itype) {
        this.itype = itype;
    }

    public String getVvalue() {
        return vvalue;
    }

    public void setVvalue(String vvalue) {
        this.vvalue = vvalue;
    }

    public String getVmax() {
        return vmax;
    }

    public void setVmax(String vmax) {
        this.vmax = vmax;
    }

    public String getVcet() {
        return vcet;
    }

    public void setVcet(String vcet) {
        this.vcet = vcet;
    }

    public String getPk_org() {
        return positncode;
    }

    public void setPk_org(String positncode) {
        this.positncode = positncode;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVmax8() {
        return vmax8;
    }

    public void setVmax8(String vmax8) {
        this.vmax8 = vmax8;
    }

    public String getVcet8() {
        return vcet8;
    }

    public void setVcet8(String vcet8) {
        this.vcet8 = vcet8;
    }
}
