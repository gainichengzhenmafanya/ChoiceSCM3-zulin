package com.choice.assistant.domain.system;

/**
 * 合约类型
 * Created by mc on 14-10-26.
 */
public class Contractterms {
    private String pk_contractterms;
    private String vcode;
    private String vname;
    private Integer enablestate;
    private Integer dr;
    private String acct;
    private String ts;

    public String getPk_contractterms() {
        return pk_contractterms;
    }

    public void setPk_contractterms(String pk_contractterms) {
        this.pk_contractterms = pk_contractterms;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public Integer getEnablestate() {
        return enablestate;
    }

    public void setEnablestate(Integer enablestate) {
        this.enablestate = enablestate;
    }

    public Integer getDr() {
        return dr;
    }

    public void setDr(Integer dr) {
        this.dr = dr;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
