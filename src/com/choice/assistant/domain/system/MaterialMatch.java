package com.choice.assistant.domain.system;

/**
 * 物资匹配中间表
 * @author ZGL
 *
 */
public class MaterialMatch {

	private String pk_materialmatch;				//主键
	private String acct;							//企业主键
	private String vcode;									//助手中物资编码
	private String vname;								//助手中物资名称
	private String vmallcode;							//商城物资编码
	private String vmallname;							//商城物资名称
	private String nleast;									//起购量
	private String vunit;									//单位
	private String nprice;									//单价
	private String delivercode;					//供应商编码
	private String delivername;					//供应商名称
	private String vsuppliercontract;				//供应商联系人
	private String vsuppliertel;						//供应商电话
	private String vsupplieraddr;					//供应商地址
	
	public String getPk_materialmatch() {
		return pk_materialmatch;
	}
	public void setPk_materialmatch(String pk_materialmatch) {
		this.pk_materialmatch = pk_materialmatch;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVmallcode() {
		return vmallcode;
	}
	public void setVmallcode(String vmallcode) {
		this.vmallcode = vmallcode;
	}
	public String getVmallname() {
		return vmallname;
	}
	public void setVmallname(String vmallname) {
		this.vmallname = vmallname;
	}
	public String getNleast() {
		return nleast;
	}
	public void setNleast(String nleast) {
		this.nleast = nleast;
	}
	public String getVunit() {
		return vunit;
	}
	public void setVunit(String vunit) {
		this.vunit = vunit;
	}
	public String getNprice() {
		return nprice;
	}
	public void setNprice(String nprice) {
		this.nprice = nprice;
	}
	public String getVdelivercode() {
		return delivercode;
	}
	public void setVdelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getVdelivername() {
		return delivername;
	}
	public void setVdelivername(String delivername) {
		this.delivername = delivername;
	}
	public String getVsuppliercontract() {
		return vsuppliercontract;
	}
	public void setVsuppliercontract(String vsuppliercontract) {
		this.vsuppliercontract = vsuppliercontract;
	}
	public String getVsuppliertel() {
		return vsuppliertel;
	}
	public void setVsuppliertel(String vsuppliertel) {
		this.vsuppliertel = vsuppliertel;
	}
	public String getVsupplieraddr() {
		return vsupplieraddr;
	}
	public void setVsupplieraddr(String vsupplieraddr) {
		this.vsupplieraddr = vsupplieraddr;
	}
	
}
