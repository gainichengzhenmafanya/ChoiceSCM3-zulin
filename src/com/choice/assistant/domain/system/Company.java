package com.choice.assistant.domain.system;

import java.util.List;

public class Company {

	private String acct;
	private String vmcnumber;		//主卡号
	private String vcode;			//企业号
	private String vcompname;		//企业名称
	private String vaddress;		//地址
	private String vphone;			//联系电话
	private String vname;			//联系人
	private String vadminpwd;		//超级管理员密码
	private String nregmoney;		//注册资金
	private String vregisternum;		//注册号
	private String vmemo;			//
	private String vcompname1;
	private String vcompname2;
	private String vcompname3;
	private String vcompname4;
	private String vcompname5;
	private String vcompname6;
	private String vaddress1;
	private String vaddress2;
	private String vaddress3;
	private String vaddress4;
	private String vaddress5;
	private String vaddress6;
	private String vmemo1;
	private String vmemo2;
	private String vmemo3;
	private String vmemo4;
	private String vmemo5;
	private String vmemo6;
	private Integer dr;
	private String ts;
	private Integer enablestate;
	
	private List<AccountPeri> listAccountPeri;	//会计期list
	private String vaccountyear;				//当前会计年
	private String vitfuserid;					//接口登录用户名
	private String vitfpwd;						//接口登录密码
	private String vcomptype;					//企业类型
	private String nstorequantity;			//门店数量
	private String nemployquantity;		//采购实体
	private String vzipcode;					//邮政编码
	private String vemail;						//邮箱
	private String vcity;						 	//所在城市
	private String vadminaccount;			//超级管理员账号
	private String vregdate;						//注册日期
	private String vcompanylevel;			//等级1:VIP; 2:金卡; 3:黑卡
	private String type;							//接口类型
	private String email;							//页面新增时输入的接收企业号、登录信息的邮箱
	private String vweburl;						//网址
	private String vauthkey;					//企业通过接口上传数据加密key
	private String vsubmitothorder;		//是否可以提交他人订单到商城 Y-可以 N不可以
			
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getVmcnumber() {
		return vmcnumber;
	}
	public void setVmcnumber(String vmcnumber) {
		this.vmcnumber = vmcnumber;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVcompname() {
		return vcompname;
	}
	public void setVcompname(String vcompname) {
		this.vcompname = vcompname;
	}
	public String getVaddress() {
		return vaddress;
	}
	public void setVaddress(String vaddress) {
		this.vaddress = vaddress;
	}
	public String getVphone() {
		return vphone;
	}
	public void setVphone(String vphone) {
		this.vphone = vphone;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVadminpwd() {
		return vadminpwd;
	}
	public void setVadminpwd(String vadminpwd) {
		this.vadminpwd = vadminpwd;
	}
	public String getNregmoney() {
		return nregmoney;
	}
	public void setNregmoney(String nregmoney) {
		this.nregmoney = nregmoney;
	}
	public String getVregisternum() {
		return vregisternum;
	}
	public void setVregisternum(String vregisternum) {
		this.vregisternum = vregisternum;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getVcompname1() {
		return vcompname1;
	}
	public void setVcompname1(String vcompname1) {
		this.vcompname1 = vcompname1;
	}
	public String getVcompname2() {
		return vcompname2;
	}
	public void setVcompname2(String vcompname2) {
		this.vcompname2 = vcompname2;
	}
	public String getVcompname3() {
		return vcompname3;
	}
	public void setVcompname3(String vcompname3) {
		this.vcompname3 = vcompname3;
	}
	public String getVcompname4() {
		return vcompname4;
	}
	public void setVcompname4(String vcompname4) {
		this.vcompname4 = vcompname4;
	}
	public String getVcompname5() {
		return vcompname5;
	}
	public void setVcompname5(String vcompname5) {
		this.vcompname5 = vcompname5;
	}
	public String getVcompname6() {
		return vcompname6;
	}
	public void setVcompname6(String vcompname6) {
		this.vcompname6 = vcompname6;
	}
	public String getVaddress1() {
		return vaddress1;
	}
	public void setVaddress1(String vaddress1) {
		this.vaddress1 = vaddress1;
	}
	public String getVaddress2() {
		return vaddress2;
	}
	public void setVaddress2(String vaddress2) {
		this.vaddress2 = vaddress2;
	}
	public String getVaddress3() {
		return vaddress3;
	}
	public void setVaddress3(String vaddress3) {
		this.vaddress3 = vaddress3;
	}
	public String getVaddress4() {
		return vaddress4;
	}
	public void setVaddress4(String vaddress4) {
		this.vaddress4 = vaddress4;
	}
	public String getVaddress5() {
		return vaddress5;
	}
	public void setVaddress5(String vaddress5) {
		this.vaddress5 = vaddress5;
	}
	public String getVaddress6() {
		return vaddress6;
	}
	public void setVaddress6(String vaddress6) {
		this.vaddress6 = vaddress6;
	}
	public String getVmemo1() {
		return vmemo1;
	}
	public void setVmemo1(String vmemo1) {
		this.vmemo1 = vmemo1;
	}
	public String getVmemo2() {
		return vmemo2;
	}
	public void setVmemo2(String vmemo2) {
		this.vmemo2 = vmemo2;
	}
	public String getVmemo3() {
		return vmemo3;
	}
	public void setVmemo3(String vmemo3) {
		this.vmemo3 = vmemo3;
	}
	public String getVmemo4() {
		return vmemo4;
	}
	public void setVmemo4(String vmemo4) {
		this.vmemo4 = vmemo4;
	}
	public String getVmemo5() {
		return vmemo5;
	}
	public void setVmemo5(String vmemo5) {
		this.vmemo5 = vmemo5;
	}
	public String getVmemo6() {
		return vmemo6;
	}
	public void setVmemo6(String vmemo6) {
		this.vmemo6 = vmemo6;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public List<AccountPeri> getListAccountPeri() {
		return listAccountPeri;
	}
	public void setListAccountPeri(List<AccountPeri> listAccountPeri) {
		this.listAccountPeri = listAccountPeri;
	}
	public String getVaccountyear() {
		return vaccountyear;
	}
	public void setVaccountyear(String vaccountyear) {
		this.vaccountyear = vaccountyear;
	}
	public String getVitfuserid() {
		return vitfuserid;
	}
	public void setVitfuserid(String vitfuserid) {
		this.vitfuserid = vitfuserid;
	}
	public String getVitfpwd() {
		return vitfpwd;
	}
	public void setVitfpwd(String vitfpwd) {
		this.vitfpwd = vitfpwd;
	}
	public String getVcomptype() {
		return vcomptype;
	}
	public void setVcomptype(String vcomptype) {
		this.vcomptype = vcomptype;
	}
	public String getNstorequantity() {
		return nstorequantity;
	}
	public void setNstorequantity(String nstorequantity) {
		this.nstorequantity = nstorequantity;
	}
	public String getNemployquantity() {
		return nemployquantity;
	}
	public void setNemployquantity(String nemployquantity) {
		this.nemployquantity = nemployquantity;
	}
	public String getVzipcode() {
		return vzipcode;
	}
	public void setVzipcode(String vzipcode) {
		this.vzipcode = vzipcode;
	}
	public String getVemail() {
		return vemail;
	}
	public void setVemail(String vemail) {
		this.vemail = vemail;
	}
	public String getVcity() {
		return vcity;
	}
	public void setVcity(String vcity) {
		this.vcity = vcity;
	}
	public String getVadminaccount() {
		return vadminaccount;
	}
	public void setVadminaccount(String vadminaccount) {
		this.vadminaccount = vadminaccount;
	}
	public String getVregdate() {
		return vregdate;
	}
	public void setVregdate(String vregdate) {
		this.vregdate = vregdate;
	}
	public String getVcompanylevel() {
		return vcompanylevel;
	}
	public void setVcompanylevel(String vcompanylevel) {
		this.vcompanylevel = vcompanylevel;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVweburl() {
		return vweburl;
	}
	public void setVweburl(String vweburl) {
		this.vweburl = vweburl;
	}
	public String getVauthkey() {
		return vauthkey;
	}
	public void setVauthkey(String vauthkey) {
		this.vauthkey = vauthkey;
	}
	public String getVsubmitothorder() {
		return vsubmitothorder;
	}
	public void setVsubmitothorder(String vsubmitothorder) {
		this.vsubmitothorder = vsubmitothorder;
	}
	
}
