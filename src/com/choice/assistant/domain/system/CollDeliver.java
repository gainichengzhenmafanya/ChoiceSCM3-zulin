package com.choice.assistant.domain.system;

/**
 * Created by zhb on 14-10-18.
 */
public class CollDeliver {
    public String pk_user;  //用户主键
    public String pk_colldeliver; //收藏夹供应商主键
    public String pk_account; //帐号主键
    public String pk_deliver; //供应商主键
    public String delivercode; //供应商编码
    public String delivername; //供应商名称
    public String vdeliverlevel; //供应商级别
    public String vdeliverphone; //供应商电话
    public String vdeliveraddress; //供应商地址
    public String vmemo;//备注
    public String acct; //企业id
    public Integer dr; //删除标志
    public String ts; //时间戳
    public String gysid;//传输主键
    public String voperatingrange;//经营范围
    public String vdeliveryarea;//配送范围
    public String varea;//地区
    
	public String getGysid() {
		return gysid;
	}
	public void setGysid(String gysid) {
		this.gysid = gysid;
	}
	public String getPk_user() {
		return pk_user;
	}
	public void setPk_user(String pk_user) {
		this.pk_user = pk_user;
	}
	public String getPk_colldeliver() {
		return pk_colldeliver;
	}
	public void setPk_colldeliver(String pk_colldeliver) {
		this.pk_colldeliver = pk_colldeliver;
	}
	public String getPk_account() {
		return pk_account;
	}
	public void setPk_account(String pk_account) {
		this.pk_account = pk_account;
	}
	public String getPk_deliver() {
		return pk_deliver;
	}
	public void setPk_deliver(String pk_deliver) {
		this.pk_deliver = pk_deliver;
	}
	public String getVdelivercode() {
		return delivercode;
	}
	public void setVdelivercode(String delivercode) {
		this.delivercode = delivercode;
	}
	public String getVdelivername() {
		return delivername;
	}
	public void setVdelivername(String delivername) {
		this.delivername = delivername;
	}
	public String getVdeliverlevel() {
		return vdeliverlevel;
	}
	public void setVdeliverlevel(String vdeliverlevel) {
		this.vdeliverlevel = vdeliverlevel;
	}
	public String getVdeliverphone() {
		return vdeliverphone;
	}
	public void setVdeliverphone(String vdeliverphone) {
		this.vdeliverphone = vdeliverphone;
	}
	public String getVdeliveraddress() {
		return vdeliveraddress;
	}
	public void setVdeliveraddress(String vdeliveraddress) {
		this.vdeliveraddress = vdeliveraddress;
	}
	public String getVmemo() {
		return vmemo;
	}
	public void setVmemo(String vmemo) {
		this.vmemo = vmemo;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVoperatingrange() {
		return voperatingrange;
	}
	public void setVoperatingrange(String voperatingrange) {
		this.voperatingrange = voperatingrange;
	}
	public String getVdeliveryarea() {
		return vdeliveryarea;
	}
	public void setVdeliveryarea(String vdeliveryarea) {
		this.vdeliveryarea = vdeliveryarea;
	}
	public String getVarea() {
		return varea;
	}
	public void setVarea(String varea) {
		this.varea = varea;
	}
	
	
}
