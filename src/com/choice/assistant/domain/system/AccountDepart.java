package com.choice.assistant.domain.system;

/**
 * Created by mc on 14-10-18.
 */
public class AccountDepart {
    public String pk_accountdepart;  //主键
    public String positncode; //组织主键
    public String pk_accountid; //帐号主键
    public String acct; //企业id
    public Integer dr; //删除标志
    public String ts; //时间戳
	public String getPk_accountdepart() {
		return pk_accountdepart;
	}
	public void setPk_accountdepart(String pk_accountdepart) {
		this.pk_accountdepart = pk_accountdepart;
	}
	public String getPk_org() {
		return positncode;
	}
	public void setPk_org(String positncode) {
		this.positncode = positncode;
	}
	public String getPk_accountid() {
		return pk_accountid;
	}
	public void setPk_accountid(String pk_accountid) {
		this.pk_accountid = pk_accountid;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
    

}
