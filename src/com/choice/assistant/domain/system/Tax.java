package com.choice.assistant.domain.system;

/**
 * 税率
 * @author wangchao
 */
public class Tax {
	
	private String acct;//企业id
	private String pk_tax;//主键
	private String vcode;//编码
	private String vname;//名称
	private double ntaxrate;//税率值
	private String enablestate="2";//启用状态
	private int dr;
	private String ts;
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_tax() {
		return pk_tax;
	}
	public void setPk_tax(String pk_tax) {
		this.pk_tax = pk_tax;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public double getntaxrate() {
		return ntaxrate;
	}
	public void setntaxrate(double ntaxrate) {
		this.ntaxrate = ntaxrate;
	}
	public String getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(String enablestate) {
		this.enablestate = enablestate;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	
}
