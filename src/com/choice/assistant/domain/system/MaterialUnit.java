package com.choice.assistant.domain.system;

public class MaterialUnit {

	private String pk_materialunit;//主键
	private String pk_material;//物资主键  
	private String pk_unit;//单位主键
	private String vunitcode;//单位编码
	private String vunitname;//单位名称
	private double nrate;//转换率
	private int bispurchase;//是否采购单位1 是 0否
	private int bissale;//是否销售单位1 是 0否
	private int bisstore;//是否库存单位1 是 0否
	private String acct;
	private int dr;
	private String ts;
	public String getPk_materialunit() {
		return pk_materialunit;
	}
	public void setPk_materialunit(String pk_materialunit) {
		this.pk_materialunit = pk_materialunit;
	}
	public String getPk_material() {
		return pk_material;
	}
	public void setPk_material(String pk_material) {
		this.pk_material = pk_material;
	}
	public String getPk_unit() {
		return pk_unit;
	}
	public void setPk_unit(String pk_unit) {
		this.pk_unit = pk_unit;
	}
	public String getVunitcode() {
		return vunitcode;
	}
	public void setVunitcode(String vunitcode) {
		this.vunitcode = vunitcode;
	}
	public String getVunitname() {
		return vunitname;
	}
	public void setVunitname(String vunitname) {
		this.vunitname = vunitname;
	}
	public double getNrate() {
		return nrate;
	}
	public void setNrate(double nrate) {
		this.nrate = nrate;
	}
	public int getBispurchase() {
		return bispurchase;
	}
	public void setBispurchase(int bispurchase) {
		this.bispurchase = bispurchase;
	}
	public int getBissale() {
		return bissale;
	}
	public void setBissale(int bissale) {
		this.bissale = bissale;
	}
	public int getBisstore() {
		return bisstore;
	}
	public void setBisstore(int bisstore) {
		this.bisstore = bisstore;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public int getDr() {
		return dr;
	}
	public void setDr(int dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}

	
}
