package com.choice.assistant.domain.system;
/**
 * 询价类型
 * @author ZGL
 *
 */
public class Inquiry {
	
	private String pk_inquiry;		//主键
	private String vcode;				//编码
	private String vname;				//名称
	private Integer enablestate=2;		//
	private Integer dr;					
	private String acct;			//
	private String ts;
	
	public String getPk_inquiry() {
		return pk_inquiry;
	}
	public void setPk_inquiry(String pk_inquiry) {
		this.pk_inquiry = pk_inquiry;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	
}
