package com.choice.assistant.domain.system;

/**
 * 组织物质权限
 * Created by mc on 14/11/27.
 */
public class DeptMaterial {
    private String pk_department;
    private String pk_material;
    private String acct;
    private String pk_deptmaterial;

    public String getPk_department() {
        return pk_department;
    }

    public void setPk_department(String pk_department) {
        this.pk_department = pk_department;
    }

    public String getPk_material() {
        return pk_material;
    }

    public void setPk_material(String pk_material) {
        this.pk_material = pk_material;
    }

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getPk_deptmaterial() {
        return pk_deptmaterial;
    }

    public void setPk_deptmaterial(String pk_deptmaterial) {
        this.pk_deptmaterial = pk_deptmaterial;
    }
}
