package com.choice.assistant.domain.system;
/**
 * 招标类型
 * @author ZGL
 *
 */
public class Tender {
	
	private String pk_tender;		//主键
	private String vcode;				//编码
	private String vname;				//名称
	private Integer enablestate;		//
	private Integer dr;					
	private String acct;			//
	private String ts;
	
	public String getPk_tender() {
		return pk_tender;
	}
	public void setPk_tender(String pk_tender) {
		this.pk_tender = pk_tender;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public Integer getEnablestate() {
		return enablestate;
	}
	public void setEnablestate(Integer enablestate) {
		this.enablestate = enablestate;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	
}
