package com.choice.assistant.domain.system;

public class AccountPeri {

	private String acct;
	private String pk_accountperi;	
	private String vaccountyear;	//年份
	private String vaccountmonth;	//月份
	private String dstartdate;		//开始日期
	private String denddate;		//技术日期
	private String vdef;
	private String vdef2;
	private String vdef3;
	private String vdef4;
	private String vdef5;
	private Integer dr;
	private String ts;
	
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getPk_accountperi() {
		return pk_accountperi;
	}
	public void setPk_accountperi(String pk_accountperi) {
		this.pk_accountperi = pk_accountperi;
	}
	public String getVaccountyear() {
		return vaccountyear;
	}
	public void setVaccountyear(String vaccountyear) {
		this.vaccountyear = vaccountyear;
	}
	public String getVaccountmonth() {
		return vaccountmonth;
	}
	public void setVaccountmonth(String vaccountmonth) {
		this.vaccountmonth = vaccountmonth;
	}
	public String getDstartdate() {
		return dstartdate;
	}
	public void setDstartdate(String dstartdate) {
		this.dstartdate = dstartdate;
	}
	public String getDenddate() {
		return denddate;
	}
	public void setDenddate(String denddate) {
		this.denddate = denddate;
	}
	public String getVdef() {
		return vdef;
	}
	public void setVdef(String vdef) {
		this.vdef = vdef;
	}
	public String getVdef2() {
		return vdef2;
	}
	public void setVdef2(String vdef2) {
		this.vdef2 = vdef2;
	}
	public String getVdef3() {
		return vdef3;
	}
	public void setVdef3(String vdef3) {
		this.vdef3 = vdef3;
	}
	public String getVdef4() {
		return vdef4;
	}
	public void setVdef4(String vdef4) {
		this.vdef4 = vdef4;
	}
	public String getVdef5() {
		return vdef5;
	}
	public void setVdef5(String vdef5) {
		this.vdef5 = vdef5;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}

	
}
