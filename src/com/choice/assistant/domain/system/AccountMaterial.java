package com.choice.assistant.domain.system;

/**
 * Created by mc on 14-10-18.
 */
public class AccountMaterial {
    public String pk_materialrule;  //主键
    public String pk_materiatyp; //物料类别主键
    public String pk_accountid; //帐号主键
    public String acct; //企业id
    public Integer dr; //删除标志
    public String ts; //时间戳
    
	public String getPk_materialrule() {
		return pk_materialrule;
	}
	public void setPk_materialrule(String pk_materialrule) {
		this.pk_materialrule = pk_materialrule;
	}
	public String getPk_materiatyp() {
		return pk_materiatyp;
	}
	public void setPk_materiatyp(String pk_materiatyp) {
		this.pk_materiatyp = pk_materiatyp;
	}
	public String getPk_accountid() {
		return pk_accountid;
	}
	public void setPk_accountid(String pk_accountid) {
		this.pk_accountid = pk_accountid;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}

}
