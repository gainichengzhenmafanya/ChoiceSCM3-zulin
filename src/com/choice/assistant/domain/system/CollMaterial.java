package com.choice.assistant.domain.system;

/**
 * Created by zhb on 14-10-18.
 */
public class CollMaterial {
    public String pk_user;  //用户主键
    public String pk_collmaterial; //收藏夹商品主键
    public String pk_account; //账号主键
    public String pk_mallmaterial; //商品主键
    public String vmaterialcode; //商品编码
    public String vmaterialname; //商品名称
    public String vmaterialspec; //规格
    public String nmatprice; //市场价
    public String nmallprice; //商城价
    public String namount; //起订量
    public String vmallsupplier; //供应商
    public String acct; //企业id
    public Integer dr; //删除标志
    public String ts; //收藏时间
    public String spid;//传输主键
    public String vunit;//计量单位
    
	public String getSpid() {
		return spid;
	}
	public void setSpid(String spid) {
		this.spid = spid;
	}
	public String getPk_user() {
		return pk_user;
	}
	public void setPk_user(String pk_user) {
		this.pk_user = pk_user;
	}
	public String getPk_collmaterial() {
		return pk_collmaterial;
	}
	public void setPk_collmaterial(String pk_collmaterial) {
		this.pk_collmaterial = pk_collmaterial;
	}
	public String getPk_account() {
		return pk_account;
	}
	public void setPk_account(String pk_account) {
		this.pk_account = pk_account;
	}
	public String getPk_mallmaterial() {
		return pk_mallmaterial;
	}
	public void setPk_mallmaterial(String pk_mallmaterial) {
		this.pk_mallmaterial = pk_mallmaterial;
	}
	public String getVmaterialcode() {
		return vmaterialcode;
	}
	public void setVmaterialcode(String vmaterialcode) {
		this.vmaterialcode = vmaterialcode;
	}
	public String getVmaterialname() {
		return vmaterialname;
	}
	public void setVmaterialname(String vmaterialname) {
		this.vmaterialname = vmaterialname;
	}
	public String getVmaterialspec() {
		return vmaterialspec;
	}
	public void setVmaterialspec(String vmaterialspec) {
		this.vmaterialspec = vmaterialspec;
	}
	public String getNmatprice() {
		return nmatprice;
	}
	public void setNmatprice(String nmatprice) {
		this.nmatprice = nmatprice;
	}
	public String getNmallprice() {
		return nmallprice;
	}
	public void setNmallprice(String nmallprice) {
		this.nmallprice = nmallprice;
	}
	public String getNamount() {
		return namount;
	}
	public void setNamount(String namount) {
		this.namount = namount;
	}
	public String getVmallsupplier() {
		return vmallsupplier;
	}
	public void setVmallsupplier(String vmallsupplier) {
		this.vmallsupplier = vmallsupplier;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public Integer getDr() {
		return dr;
	}
	public void setDr(Integer dr) {
		this.dr = dr;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVunit() {
		return vunit;
	}
	public void setVunit(String vunit) {
		this.vunit = vunit;
	}
    
}
