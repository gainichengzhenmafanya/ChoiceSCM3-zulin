package com.choice.common.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.choice.framework.util.ForResourceFiles;

public class LoadCompInfoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static String url = "";
	public static String username = "";
	public static String password = "";
	public static String driver = "";
	public static String initialSize = "";
	public static String minIdle = "";
	public static String maxActive = "";
	public static String maxWait = "";
	public static String timeBetweenEvictionRunsMillis = "";
	public static String minEvictableIdleTimeMillis = "";
	public static String validationQuery = "";
	public static String testWhileIdle = "";
	public static String testOnBorrow = "";
	public static String testOnReturn = "";
	public static String poolPreparedStatements = "";
	public static String maxOpenPreparedStatements = "";
	public static String removeAbandoned = "";
	public static String removeAbandonedTimeout = "";
	public static String logAbandoned = "";
	public static String logFilters = "";
	public static Map<String,String> groupmap=new HashMap<String,String>();
	public static final String filesrc=ForResourceFiles.getValByKey("config-boh.properties","filesrc");
	public static final String curProName=ForResourceFiles.getValByKey("config-boh.properties","curProName");
	// 设置动态连接数据库
	public static Map<Object, Object> targetDataSources = new HashMap<Object, Object>();
	public static Map<Object, Object> targetCompany = new HashMap<Object, Object>();//存储企业信息

	static {
		//获取数据库连接文件
				InputStream inputStream = LoadCompInfoServlet.class.getClassLoader().getResourceAsStream("jdbc.properties");
				Properties properties = new Properties();
				try {
					properties.load(inputStream);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//设置连接数据库连接参数
				driver = properties.getProperty("jdbc.driver_all");
				url = properties.getProperty("jdbc.url_all");
				username =properties.getProperty("jdbc.username_all");
				password =properties.getProperty("jdbc.password_all");	
				initialSize = properties.getProperty("pool.initialSize");
				minIdle = properties.getProperty("pool.minIdle");
				maxActive = properties.getProperty("pool.maxActive");
				maxWait = properties.getProperty("pool.maxWait");
				timeBetweenEvictionRunsMillis = properties.getProperty("pool.timeBetweenEvictionRunsMillis");
				minEvictableIdleTimeMillis = properties.getProperty("pool.minEvictableIdleTimeMillis");
				validationQuery = properties.getProperty("pool.validationQuery");
				testWhileIdle = properties.getProperty("pool.testWhileIdle");
				testOnBorrow = properties.getProperty("pool.testOnBorrow");
				testOnReturn = properties.getProperty("pool.testOnReturn");
				poolPreparedStatements = properties.getProperty("pool.poolPreparedStatements");
				maxOpenPreparedStatements = properties.getProperty("pool.maxOpenPreparedStatements");
				removeAbandoned = properties.getProperty("pool.removeAbandoned");
				removeAbandonedTimeout = properties.getProperty("pool.removeAbandonedTimeout");
				logAbandoned = properties.getProperty("pool.logAbandoned");
				logFilters = properties.getProperty("pool.logFilters");
	}

	public void init() throws ServletException {
		super.init();
	}

	public void doGet(HttpServletRequest httpservletrequest,
			HttpServletResponse httpservletresponse) throws ServletException,
			IOException {
		super.doGet(httpservletrequest, httpservletresponse);
	}

	public void destory() {
		super.destroy();
	}
}
