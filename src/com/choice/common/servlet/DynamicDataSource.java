package com.choice.common.servlet;

import java.util.logging.Logger;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.choice.framework.util.DataSourceSwitch;

/**
 * 获取数据源(依赖SPRING框架)
 * 
 * @author wang
 * @create 2015.08.07
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	private Jdbconfig jdbconfig;

	public DynamicDataSource(){}
	public DynamicDataSource(Jdbconfig jdbconfig){
		this.jdbconfig=jdbconfig;
		this.setTargetDataSources(jdbconfig.loadDataSources(null));
	}
	/*
	 * private DataSourceEntry dataSourceEntry;
	 * 
	 * @Override protected Object determineCurrentLookupKey() { return
	 * this.dataSourceEntry.get(); }
	 * 
	 * @Resource public void setDataSourceEntry(DataSourceEntry dataSourceEntry)
	 * { this.dataSourceEntry = dataSourceEntry; }
	 */

	@Override
	protected Object determineCurrentLookupKey() {
		return DataSourceSwitch.getDataSourceType();
	}

	@Override
	public Logger getParentLogger() {
		return null;
	}
}