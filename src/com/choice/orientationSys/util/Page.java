package com.choice.orientationSys.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.choice.assistant.constants.system.SysParamConstants;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.ForResourceFiles;

@Component
@Scope("prototype")
public class Page {
	private int nowPage;
	private int pageSize;
	private int count;
	private int maxSize;
	private String queryKey;
	private String sql;

	public Page() {
		this.nowPage = 1;
		String page_size_all = UserSpace.getToken()==null||StringUtils.isEmpty(UserSpace.getToken().getPkGroup()) ?"40":ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "PAGESIZE");
		int pageSizePz = 40;
		try {
			pageSizePz = Integer.parseInt(page_size_all);
		} catch (Exception e) {
		}
		this.pageSize = pageSizePz;
	}

	public int getOffset() {
		int tempOffest = (this.nowPage - 1) * this.pageSize;
		if (tempOffest < this.count) {
			return tempOffest;
		}
		return this.count / this.pageSize;
	}

	public int getLimit() {
		if (this.count - getOffset() > this.pageSize) {
			return this.pageSize;
		}
		return this.count - getOffset();
	}

	public int getPageCount() {
		if (this.count <= this.pageSize)
			return 1;
		if (this.count % this.pageSize == 0) {
			return this.count / this.pageSize;
		}

		return this.count / this.pageSize + 1;
	}

	public int getMaxSize() {
		this.maxSize = Integer.parseInt(ForResourceFiles.getParamValByCodeFromCacheDB(SysParamConstants.SCM, "MAXSIZE"));

		return this.maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public void setCount(int Count) {
		this.count = Count;
	}

	public int getCount() {
		return this.count;
	}

	public int getNowPage() {
		return this.nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getQueryKey() {
		return this.queryKey;
	}

	public void setQueryKey(String queryKey) {
		this.queryKey = queryKey;
	}

	public String getSql() {
		return this.sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
}