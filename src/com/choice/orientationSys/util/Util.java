package com.choice.orientationSys.util;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

public class Util {
	public static final char TRUE ='T';//是
	public static final char FALSE = 'F';//否
	public static String getUUID() {
		String uuid = UUID.randomUUID().toString();
		return uuid.replaceAll("-","");
	}
	 /**
	  * 获取客户端ip
	  * @param request
	  * @return
	  */
	public static String getIpAddr(HttpServletRequest request) { 
	     String ip = request.getHeader("x-forwarded-for"); 
	     if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	         ip = request.getHeader("Proxy-Client-IP"); 
	     } 
	     if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	         ip = request.getHeader("WL-Proxy-Client-IP"); 
	     } 
	     if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
	         ip = request.getRemoteAddr(); 
	     } 
	     // 返回一串ip值的话，取第一个
	     if(ip.contains(",")){
	    	 ip = ip.substring(0, ip.indexOf(","));
	     }
	     if("0:0:0:0:0:0:0:1".equals(ip) || ip.indexOf("0:0:0:0:0:0:0:1")>=0){
	    	 ip = "127.0.0.1";
	     }
	     return ip; 
	 }
	
	/**
	 * 得到指定位数的随机数串
	 * @param length
	 * @return
	 */
	public static String getRandomNumber(int length){
		if(length==0){
			return "";
		}
		String result = "";
		for(int i=0;i<length;i++){
			result = result + (int)(Math.random()*10);
		}
		return result;
	}
}
