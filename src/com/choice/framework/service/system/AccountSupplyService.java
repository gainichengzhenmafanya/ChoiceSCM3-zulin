package com.choice.framework.service.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.AccountSupply;
import com.choice.framework.persistence.system.AccountSupplyMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.scm.domain.Supply;

@Service
public class AccountSupplyService {
	
	@Autowired
	private AccountSupplyMapper accountSupplyMapper;

	/**
	 * 查询账号所有物资信息
	 * @param account
	 * @return
	 */
	public List<AccountSupply> findAccountSupply(AccountSupply accountSupply){
		return accountSupplyMapper.findAccountSupply(accountSupply);
	}
	
	/**
	 * 保存账号具有的物资范围限信息
	 * @param id
	 * @return
	 */
	public void saveAccountSupply(AccountSupply accountSupply){
		accountSupplyMapper.deleteAccountSupply(accountSupply);
		String[] supplyIdArray = accountSupply.getSp_code().split(",");
		for(String supplyId:supplyIdArray){
			AccountSupply newAccountSupply = new AccountSupply();
			newAccountSupply.setId(CodeHelper.createUUID());
			newAccountSupply.setSp_code(supplyId);
			newAccountSupply.setAccountId(accountSupply.getAccountId());
			accountSupplyMapper.saveAccountSupply(newAccountSupply);
		}
		
	}
	
	/**
	 * 分配物资范围复制到账号
	 * @return
	 *  
	 */
	public void copyToAccount(String accountId,String accountIdStr){
		AccountSupply accountSupply = new AccountSupply();
		accountSupply.setAccountId(accountId);
		List<AccountSupply> asList = findAccountSupply(accountSupply);
		String[] accountIds = accountIdStr.split(",");
		AccountSupply accountSupply1 = new AccountSupply();
		for(int j=0;j<accountIds.length;j++){
			accountSupply1.setAccountId(accountIds[j]);
			String sp_code = "";
			for(int i=0;i<asList.size();i++){
				sp_code+=asList.get(i).getSp_code()+",";
			}
			sp_code=sp_code.substring(0,sp_code.lastIndexOf(","));
			accountSupply1.setSp_code(sp_code);
			saveAccountSupply(accountSupply1);
		}
	}
	
	/**
	 * 根据大中小类添加账号物资权限
	 * @param accountId 账号
	 * @param classType 类别大0中1小2
	 * @param classCode 类别编码
	 */
	public void saveAccountSupply(String accountId, List<Supply> list) throws Exception{
		
		AccountSupply accountSupply = new AccountSupply();
		accountSupply.setAccountId(accountId);
//		accountSupplyMapper.deleteAccountSupply(accountSupply);//清空原数据    写这行代码的的人被豪享来骂死了
		for(Supply supply:list){
			AccountSupply newAccountSupply = new AccountSupply();
			newAccountSupply.setId(CodeHelper.createUUID());
			newAccountSupply.setSp_code(supply.getSp_code());//设置物资编码
			newAccountSupply.setAccountId(accountId);
			if(accountSupplyMapper.findOneAccountSupply(newAccountSupply)==0){
				accountSupplyMapper.saveAccountSupply(newAccountSupply);
			}
		}
	}
}
