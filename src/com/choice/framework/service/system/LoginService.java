package com.choice.framework.service.system;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.constants.system.LoginConstants;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountRole;
import com.choice.framework.domain.system.Module;
import com.choice.framework.domain.system.Operate;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountMapper;
import com.choice.framework.persistence.system.AccountRoleMapper;
import com.choice.framework.persistence.system.ModuleMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.MD5;
import com.choice.framework.vo.AccountCache;
import com.choice.framework.web.controller.system.LoginSecurity;
import com.choice.orientationSys.util.RegistCodeUtil;

@Service
public class LoginService {
	
	private static final Logger LOG = LoggerFactory.getLogger(LoginService.class);
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private AccountRoleMapper accountRoleMapper;
	
	@Autowired
	private ModuleMapper moduleMapper;
//	@Autowired
//	private RedisOpsForString<AccountCache> redisDao;
	
	/**
	 * 验证账号登录信息
	 * @param account
	 * @return
	 * @throws CRUDException
	 */
	public String validateLogin(Account account,HttpSession session) throws CRUDException{
		String info = StringConstant.TRUE;
		Account accountResult;
		
		//判断用户名是否为空
		if(account.getName() == null || account.getName().equals(""))
			return LoginConstants.MESSAGE_NAME_ISNULL;
		
		//判断用户密码是否为空
		else if(account.getPassword() == null || account.getPassword().equals(""))
			return LoginConstants.MESSAGE_PASSWORD_ISNULL;
		
		try{
			//判断用户名是否存在
			accountResult = accountMapper.findAccountByName(account.getName());
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_NAME_NOT_EXIST;
			
			//判断密码是否正确
			String password = account.getPassword();	//存放输入时的原始密码
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validatePassword(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_PASSWORD_ERROR;
			//账号状态未被审核
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validateStatus(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_STATUS_ERROR;
			
			
			account.setNames(accountResult.getNames());
			session.setAttribute("accountNames", accountResult.getNames());
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			throw new CRUDException(e);
		}
		
		return info;
	}
	public String validateLoginBySecurity(Account account, HttpSession session) throws CRUDException {
		String info = StringConstant.TRUE;
		Account accountResult;
		
		try{
			//判断用户名是否存在
			accountResult = accountMapper.findAccountByName(account.getName());
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_NAME_NOT_EXIST;
			
			if(LoginSecurity.INSTANCE.locked(account.getName())) {
		    	  LOG.warn("用户登录尝试次数过多, 请稍后重试, 用户名[{}]", account.getName());
		    	  return "用户登录尝试次数过多, 请稍后重试.";
		    }
			
			//判断密码是否正确
			String password = account.getPassword();	//存放输入时的原始密码
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validatePassword(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals(""))){
				LoginSecurity.INSTANCE.loginRecord(account.getName(), false);
				return LoginConstants.MESSAGE_PASSWORD_ERROR;
			}
			LoginSecurity.INSTANCE.loginRecord(account.getName(), true);
			//账号状态未被审核
			account.setPassword(MD5.md5(account.getName()+account.getPassword()));
			accountResult = accountMapper.validateStatus(account);
			account.setPassword(password);	//将密码还原为输入时的密码
			if(accountResult == null 
					|| (accountResult.getId() == null || accountResult.getId().equals("")))
				return LoginConstants.MESSAGE_STATUS_ERROR;
			
			
			account.setNames(accountResult.getNames());
			session.setAttribute("accountNames", accountResult.getNames());
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			throw new CRUDException(e);
		}
		
		return info;
	}
	
	public AccountCache loadAuthorityInfo(Account account, HttpSession session) throws CRUDException{
		//获取登录账号信息
		String password = account.getPassword();	//存放输入时的原始密码
		account.setPassword(MD5.md5(account.getName()+account.getPassword()));
		Account accountResult = accountMapper.validatePassword(account);
		account.setPassword(password);	//将密码还原为输入时的密码
		
		
		
		
		
//		StringBuffer  info=new StringBuffer();
//		String result="OK";
//		//判断是否注册
//		String regist=RegistCodeUtil.validateRegistCode();
//		String[] names = regist.split("@");  
//		try {
//			if("-1".equals(regist)){
//				info.append("注册码不正确，请及时注册！非注册用户不能访问系统！");
//				result="NO";
//				accountResult.setIsRegist("NO");
//			}else{
//				SimpleDateFormat sdf =new SimpleDateFormat("yyyyMMdd");
//				int days=Integer.parseInt(names[1])-Integer.parseInt(sdf.format(new Date()));
//				accountResult.setDays(days);
//				accountResult.setDates(names[1]);
//				if(days<0){
//					info.append("注册码已经过期，请及时注册！非注册用户不能访问系统！");
//					result="NO";
//				}else{
//					info.append("注册码有效期至："+names[1]);				
//				}
//		    }
//		} catch (Exception e) {
//			// TODO: handle exception
//			result="NO";
//		}finally{
//			accountResult.setRegist(info.toString());	
//		}
 
		
		
		
		//获取登陆账号关联的角色信息
		AccountRole accountRole = new AccountRole();
		accountRole.setAccountId(accountResult.getId());
		List<AccountRole> accountRoleList = accountRoleMapper.findAccountRole(accountRole);
		
		//获取登陆账号拥有的模块和操作信息
		Module mo = new Module();
		mo.setAccountId(accountResult.getId());
		mo.setProtyp(CodeHelper.replaceCode(ForResourceFiles.getValByKey("module.properties","ownmodule")));
		List<Module> oldModuleList = moduleMapper.findModuleByAccountProtyp(mo);
		
		//组合模块信息（用于菜单显示）
		List<Module> moduleList = this.comboModule(oldModuleList,"OK");
		
		//获取登陆账号拥有的操作信息
		HashMap<String, HashMap<String, Boolean>> moduleOperateMap = this.comboOperate(oldModuleList);
		
		//TODO:获取登陆账号拥有的操作范围(部门)信息
		
		
		//TODO:获取登陆账号拥有的操作范围(人员)信息
		
		
		//清空账号的缓存信息，并重新添加账号的缓存信息
		AccountCache accountCache = new AccountCache();
		accountCache.setAccount(accountResult);
		accountCache.setAccountRoleList(accountRoleList);
		accountCache.setModuleList(moduleList);
		accountCache.setModuleOperateMap(moduleOperateMap);
		
		//如果开启集群模式，从redis中取值
		String opencluster = ForResourceFiles.getValByKey("redis.properties","opencluster");
		if(null != opencluster && "1".equals(opencluster)){
			session.setAttribute(StringConstant.ACCOUNT_CACHE, accountCache);
			//redisDao.set(StringConstant.ACCOUNT_CACHE+accountResult.getId(),accountCache);
			//accountCache = (AccountCache)redisDao.get(StringConstant.ACCOUNT_CACHE+accountResult.getId());
		}
//		else{
//			CacheUtil cacheUtil = CacheUtil.getInstance();
//			if(cacheUtil.get(StringConstant.ACCOUNT_CACHE, accountResult.getId()) != null){
//				cacheUtil.flush(StringConstant.ACCOUNT_CACHE, accountResult.getId());
//			}
//			cacheUtil.put(StringConstant.ACCOUNT_CACHE, accountResult.getId(), accountCache);
//			return (AccountCache)cacheUtil.get(StringConstant.ACCOUNT_CACHE, accountResult.getId());
//		}
		return accountCache;
	}
	
	/**
	 * 获取需要显示的模块信息
	 * @param accountId
	 * @return
	 * @throws CRUDException
	 */
	private List<Module> comboModule(List<Module> moduleList,String result) throws CRUDException{
		//如果模块为空，则直接返回一个空的模块列表
		if(moduleList == null || moduleList.size() == 0)
			return new ArrayList<Module>();
		
		List<String> listCode = new ArrayList<String>();
		
		//模块编号
		String moduleCode;
		
		//截取的模块编号
		String code;
		
		//存放“-”分割后的编号
		String[] codes ;
		
		//每一级编号的长度
		int len = CodeHelper.CODE_LENGTH;
		
		//截取编号的截止位置
		int endIndex = 0;
		
		//遍历拥有操作的模块（只有最后一层模块有操作，其它模块没有操作），
		//根据模块的编号进行拆分，找到模块对应的父类及祖先
		for(Module module : moduleList){
			moduleCode = module.getCode();
			if(result.equals("NO")){
				return null;
			}
			codes = moduleCode.split("-");
			for(int i = 0,n = codes.length; i < n; i++){
				endIndex = len + (len+1)*i;
				code = moduleCode.substring(0, endIndex);
			//	System.out.println(code);
				if(!listCode.contains(code)){
					listCode.add(code);
				}
			}
		}
		
		 List<Module> listModule=new ArrayList<Module>();
		 List<String> listCode1000 = new ArrayList<String>();
		if(listCode.size()<1000){
			return moduleMapper.findModuleByCodes(listCode);
		}else{
			for (int i = 0; i < listCode.size(); i++) {
				listCode1000.add(listCode.get(i));
				if((i%999)==0 && i>0){  
					listModule.addAll(moduleMapper.findModuleByCodes(listCode1000));
					listCode1000.clear();
				}
			}
			listModule.addAll(moduleMapper.findModuleByCodes(listCode1000));
			return listModule;
		}
	}
	
	/**
	 * 获取需要显示的操作信息
	 * @param moduleList
	 * @return
	 * @throws CRUDException
	 */
	private HashMap<String, HashMap<String, Boolean>> comboOperate(List<Module> moduleList) throws CRUDException{
		HashMap<String, HashMap<String, Boolean>> moduleOperateMap = new HashMap<String, HashMap<String,Boolean>>();
		
		//遍历模块，判断模块是否有url，并且url的结构是否符合“/controllerMapping/methodMapping.do”的形式。
		//如果满足上述条件，则遍历模块下的操作，并组合成“HashMap<模块Id, HashMap<操作Id, 是否拥有操作>>”的形式。
		for(Module module : moduleList){
			if(module.getUrl() != null && !module.getUrl().equals("")
				&& module.getUrl().split("/").length > 1){
					
				String key = module.getUrl().split("/")[1];
				HashMap<String, Boolean> operateMap = 
						moduleOperateMap.containsKey(key) ? moduleOperateMap.get(key) : new HashMap<String, Boolean>();
				for(Operate operate : module.getChildOperate()){
					operateMap.put(operate.getType(), Boolean.TRUE);
				}
				moduleOperateMap.put(key, operateMap);
			}
		}
		
		return moduleOperateMap;
	}
	
	/**
	 * 验证账号登录信息
	 * @param account
	 * @return
	 * @throws CRUDException
	 */
	public String findUser(Account account) throws CRUDException{
		Account accountResult = accountMapper.findSingleAccount(account);
		if(null!=accountResult && null!=accountResult.getUser()){
			return accountResult.getUser().getName();
		}else{
			return "";
		}
	}
	/**
	 * 得到注册信息
	 */
	public String findRegist() throws CRUDException{
		return RegistCodeUtil.getMachineCode();
	}
	/**
	 * 保存注册信息
	 */
	public String saveRegist(String registCode,HttpSession session) throws CRUDException{
		
		String result=RegistCodeUtil.isRight(registCode);
		if("-1".equals(result)){
			return "-1";
//			return "注册码不正确！";
		}
		if("-2".equals(result)){
			return "-2";
//			return "注册码已过期！";
		}


        //指定要读取的文件路径,此处的test.txt放在工程的web目录下面  
//		fileWriter is = RegistCodeUtil.class.getResourceAsStream("/../../upload/regist.txt");  
		String ctxPath = session.getServletContext()
				.getRealPath("/")
				+ "/upload/regist.txt";
	 
          
    	PrintWriter pw = null;
		try {
			pw = new PrintWriter( new FileWriter(ctxPath) );
			pw.print(registCode); 
			return "【注册码有效期至："+result+"】";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{  
			pw.close(); 
		}
		return "【注册码有效期至："+result+"】";
	}
}
