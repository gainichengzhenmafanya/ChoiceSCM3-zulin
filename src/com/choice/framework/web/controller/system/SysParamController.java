package com.choice.framework.web.controller.system;

import static com.choice.assistant.constants.system.SysParamConstants.SBOH;
import static com.choice.assistant.constants.system.SysParamConstants.SCM;
import static com.choice.assistant.constants.system.SysParamConstants.SYS;
import static com.choice.assistant.constants.system.SysParamConstants.USER;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.redis.cache.SysParam;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.scm.service.SysParamService;

@Controller
@RequestMapping(value = "param")
public class SysParamController {
	
	private static final Logger LOG = LoggerFactory.getLogger(SysParamController.class);

	@Autowired
	private SysParamService sysParamService;
	
	/***
	 * 获取全部参数（根据登录用户的企业号、用户名）
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String getAllParams(ModelMap modelMap) throws Exception{
		
		// 获取企业号用户名
		String vcode = UserSpace.getToken().getPkGroup();   
		String userName = UserSpace.getAccount().getName(); 
		LOG.debug("findAllDepartment start with params , vcode is [{}], userName is [{}]" , vcode, userName);
		
		// 获取数据
		SysParam param = new SysParam();
		param.setVcode(vcode);
		param.setUserName(userName);
		List<SysParam> parmList = sysParamService.getAllParams(param);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dataList", CollectionUtils.isEmpty(parmList)?new ArrayList<SysParam>():parmList);
		resultMap.put("isAdmin", UserSpace.isAdmin());
		
		return JSONObject.toJSONString(resultMap);
	}
	
	/***
	 * 获取系统参数
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getSys", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String getSysParams(ModelMap modelMap) throws Exception{
		
		// 获取数据
		LOG.debug("getSysParams start !");
		List<SysParam> parmList = sysParamService.getSysParams();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dataList", CollectionUtils.isEmpty(parmList)?new ArrayList<SysParam>():parmList);
		resultMap.put("isAdmin", UserSpace.isAdmin());
		
		return JSONObject.toJSONString(resultMap);
	}
	
	/***
	 * 获取企业参数（根据登录用户的企业号）
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getScm", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String getScmParams(ModelMap modelMap) throws Exception{
		
		// 获取企业号
		String vcode = UserSpace.getToken().getPkGroup();
		LOG.debug("findAllDepartment start with params , vcode is [{}]" , vcode);
		
		// 获取数据
		SysParam param = new SysParam();
		param.setVcode(vcode);
		List<SysParam> parmList = sysParamService.getScmParams(param);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dataList", CollectionUtils.isEmpty(parmList)?new ArrayList<SysParam>():parmList);
		resultMap.put("isAdmin", UserSpace.isAdmin());
		
		return JSONObject.toJSONString(resultMap);
	}
	/***
	 * 获取门店参数（根据登录用户的企业号）
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getSboh", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String getSbohParams(ModelMap modelMap) throws Exception{
		
		// 获取企业号
		String vcode = UserSpace.getToken().getPkGroup();
		LOG.debug("findAllDepartment start with params , vcode is [{}]" , vcode);
		
		// 获取数据
		SysParam param = new SysParam();
		param.setVcode(vcode);
		List<SysParam> parmList = sysParamService.getSbohParams(param);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dataList", CollectionUtils.isEmpty(parmList)?new ArrayList<SysParam>():parmList);
		resultMap.put("isAdmin", UserSpace.isAdmin());
		
		return JSONObject.toJSONString(resultMap);
	}
	
	/***
	 * 获取用户参数（根据登录用户的用户名）
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getUser", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String getUserParams(ModelMap modelMap) throws Exception{
		
		// 获取用户名
		String userName = UserSpace.getAccount().getName();
		LOG.debug("getUserParams start with params ,  userName is [{}]" ,  userName);
		
		// 获取用户数据
		SysParam param = new SysParam();
		param.setUserName(userName);
		List<SysParam> parmList = sysParamService.getUserParams(param);
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dataList", CollectionUtils.isEmpty(parmList)?new ArrayList<SysParam>():parmList);
		resultMap.put("isAdmin", UserSpace.isAdmin());
		
		return JSONObject.toJSONString(resultMap);
	}
	
	/***
	 * 保存所有参数
	 * @param modelMap
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveAll", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String saveAll(ModelMap modelMap, SysParam param) throws CRUDException{
		
		// 传入参数校验
		String check = this.checkSaveAll(param);
		if(check!= null) {
			return  check;
		}
		
		// 开始修改
		boolean saveRes = sysParamService.updateMany(param.getParamList());
		Map<String ,Object> result = new HashMap<String ,Object>();
		result.put("result", saveRes);
		result.put("msg", saveRes?"成功":"失败");
		
		return JSONObject.toJSONString(result);
	}
	
	/***
	 * 保存单个实体
	 * @param modelMap
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	@RequestMapping(value = "/saveOne", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String saveOne(ModelMap modelMap, SysParam param) throws CRUDException{
		
		// 传入参数校验
		Map<String ,Object> result = new HashMap<String ,Object>();
		if(param == null) {
			LOG.warn("saveOne failed for param is null");
			result.put("result", false);
			result.put("msg", "传入参数为空");
			return JSONObject.toJSONString(result);
		}
		
		// 传入参数校验
		String vcode = UserSpace.getToken().getPkGroup();
		String userName = UserSpace.getAccount().getName();
		String checkRes = checkParamDemo(param, vcode,userName);
		if(!StringUtils.isEmpty(checkRes))
			return checkRes;
		
		// 修改参数
		boolean saveRes = sysParamService.update(param);
		result.put("result", saveRes);
		result.put("msg", saveRes?"成功":"失败");
		return JSONObject.toJSONString(result);
	}
	
	/***
	 * 保存所有参数验证
	 * @param param
	 * @return
	 * @throws CRUDException
	 */
	private String checkSaveAll(SysParam param) throws CRUDException{
		
		// 获取用户名、企业号
		String userName = UserSpace.getAccount().getName();
		String vcode = UserSpace.getToken().getPkGroup();
		LOG.debug("checkSaveAll start with params , userName is [{}], vcode is [{}]", userName, vcode);
		
		if(param == null|| CollectionUtils.isEmpty(param.getParamList())) {
			LOG.warn("checkSaveAll failed for param null , userName is [{}], vcode is [{}]", userName, vcode);
			return getResult(false, "传入参数为空");
		}
		
		// 校验参数
		for(SysParam demo :param.getParamList()){
			String checkDemo = checkParamDemo(demo, vcode, userName);
			if(!StringUtils.isEmpty(checkDemo))
				return checkDemo;
		}
		
		return null;
	}
	
	/***
	 * 保存参数实体验证
	 * @param demo
	 * @param vcode
	 * @param userName
	 * @return
	 * @throws CRUDException
	 */
	private String checkParamDemo(SysParam demo, String vcode, String userName) throws CRUDException{
		
		LOG.debug("checkParamDemo start with params , userName is [{}], vcode is [{}]", userName, vcode);
		if(StringUtils.isEmpty(demo.getParamCode())||StringUtils.isEmpty(demo.getParamValue())){
			LOG.warn("参数传递错误,参数编码或值为空, paramCode is [{}], paramValue is [{}]", demo.getParamCode(), demo.getParamValue());
			return getResult(false, "参数传递错误,参数名或编码为空");
		}
		String type = demo.getType();
		if(SBOH.equals(type)){
			
			if(StringUtils.isEmpty(sysParamService.getSbohParamByCodeFromCache(vcode, demo.getParamCode()))){
				LOG.warn("门店参数不存在, paramCode is [{}], paramValue is []", demo.getParamCode(), demo.getParamValue());
				return getResult(false, "企业参数不存在");
			}
			demo.setVcode(vcode);
		}else if(SCM.equals(type)){
			if(!UserSpace.isAdmin()) {
				LOG.warn("您不是系统管理员, 不能修改企业参数, paramCode is [{}], paramValue is []", demo.getParamCode(), demo.getParamValue());
				return getResult(false, "您不是系统管理员, 不能修改企业参数");
			}
			if(StringUtils.isEmpty(sysParamService.getScmParamByCodeFromCache(vcode, demo.getParamCode()))){
				LOG.warn("企业参数不存在, paramCode is [{}], paramValue is []", demo.getParamCode(), demo.getParamValue());
				return getResult(false, "企业参数不存在");
			}
			demo.setVcode(vcode);
			
		}else if(USER.equals(type)){
			if(StringUtils.isEmpty(sysParamService.getUserParamByCodeFromCache(userName, demo.getParamCode()))){
				LOG.warn("用户参数不存在, paramCode is [{}], paramValue is []", demo.getParamCode(), demo.getParamValue());
				return getResult(false, "用户参数不存在");
			}
			demo.setUserName(userName);
			
		} else if(SYS.equals(type)) {
			LOG.warn("系统参数不能修改, 如需修改请联系管理员, paramCode is [{}], paramValue is []", demo.getParamCode(), demo.getParamValue());
			return getResult(false, "系统参数不能修改, 如需修改请联系管理员");
		} else {
			LOG.warn("参数类型不正确, paramCode is [{}], type is []", demo.getParamCode(), type);
			return getResult(false, "参数类型不正确");
		}
		
		return null;
	}
	
	/***
	 * 获取JSon返回值
	 * @param success
	 * @param message
	 * @return
	 */
	private String getResult(boolean success , String message){
		Map<String ,Object> result = new HashMap<String ,Object>();
		result.put("result", success);
		result.put("msg", message);
		return JSONObject.toJSONString(result);
	}
	
}

