package com.choice.framework.web.controller.system;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.StringConstant;
import com.choice.framework.constants.system.AccountConstants;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.Department;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.domain.system.User;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountService;
import com.choice.framework.service.system.DepartmentService;
import com.choice.framework.service.system.UserService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.orientationSys.util.Util;

@Controller
@RequestMapping({"account"})
public class AccountController
{

  @Autowired
  private AccountService accountService;

  @Autowired
  private UserService userService;

  @Autowired
  private DepartmentService departmentService;

  @Autowired
  private LogsMapper logsMapper;

  @RequestMapping({"/list"})
  @RequiresPermissions("account:select")
  public ModelAndView findAllDepartment(ModelMap modelMap, Department department, String departmentId)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    List departmentList = this.departmentService.findDepartment(department);

    modelMap.put("departmentList", departmentList);

    if ((departmentId == null) || (departmentId.equals(""))) {
      departmentId = "00000000000000000000000000000000";
    }
    modelMap.put("department", this.departmentService.findDepartmentById(departmentId));

    return new ModelAndView(AccountConstants.LIST_ACCOUNT, modelMap);
  }

  @RequestMapping({"/findAccount"})
  @ResponseBody
  public Object findAllDepartment(ModelMap modelMap, Account account)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    List listAccount = this.accountService.findAccountEmp(account);
    return listAccount;
  }

  @RequestMapping({"/table"})
  public ModelAndView findUserAccount(ModelMap modelMap, String departmentCode)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    HashMap map = new HashMap();
    map.put("departmentCode", departmentCode);

    modelMap.put("userList", this.userService.findUserAccount(map));
    modelMap.put("SEX_RADIO", StringConstant.SEX_RADIO);
    modelMap.put("ACCOUNT_STATE", StringConstant.ACCOUNT_STATE);

    ReadProperties rp = new ReadProperties();
    String choice = rp.getStrByParam("choice");
    modelMap.put("choice", choice);
    return new ModelAndView(AccountConstants.TABLE_ACCOUNT, modelMap);
  }

  @RequestMapping({"/add"})
  public ModelAndView addAccount(ModelMap modelMap, String userId)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    modelMap.put("user", this.userService.findUserById(userId));

    return new ModelAndView(AccountConstants.ADD_ACCOUNT, modelMap);
  }

  @RequestMapping({"/update"})
  public ModelAndView updateAccount(ModelMap modelMap, String id)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    modelMap.put("account", this.accountService.findAccountById(id));

    return new ModelAndView(AccountConstants.UPDATE_ACCOUNT, modelMap);
  }

  @RequestMapping({"/updatePassword"})
  public ModelAndView updatePassword(ModelMap modelMap, HttpSession session) throws CRUDException {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    modelMap.put("account", this.accountService.findAccountById((String)session.getAttribute("accountId")));

    return new ModelAndView(AccountConstants.UPDATE_ACCOUNT, modelMap);
  }

  @RequestMapping({"/tableFromUser"})
  public ModelAndView findAccountFromUser(ModelMap modelMap, String userId)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    Account account = new Account();
    User user = new User();
    user.setId(userId);
    account.setUser(user);

    modelMap.put("accountList", this.accountService.findAccount(account));
    modelMap.put("ACCOUNT_STATE", StringConstant.ACCOUNT_STATE);
    modelMap.put("userId", userId);

    return new ModelAndView(AccountConstants.TABLE_ACCOUNT_FROM_USER, modelMap);
  }

  @RequestMapping({"/addFromUser"})
  public ModelAndView addAccountFromUser(ModelMap modelMap, String userId)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    modelMap.put("user", this.userService.findUserById(userId));

    return new ModelAndView(AccountConstants.ADD_ACCOUNT, modelMap);
  }

  @RequestMapping({"/updateFromUser"})
  public ModelAndView updateAccountFromUser(ModelMap modelMap, String id)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    modelMap.put("account", this.accountService.findAccountById(id));

    return new ModelAndView(AccountConstants.UPDATE_ACCOUNT, modelMap);
  }

  @RequestMapping(value={"/ajaxSaveByAdd"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String ajaxSaveByAdd(@RequestParam String userId, @RequestParam String name, @RequestParam String password, @RequestParam String names, HttpSession session)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);

    Logs logs = new Logs(Util.getUUID(), session.getAttribute("accountId").toString(), new Date(), "新增", "新增账号:" + name, session.getAttribute("ip").toString(), "0");
    this.logsMapper.addLogs(logs);

    User user = new User();
    user.setId(userId);
    Account account = new Account();
    account.setName(name);
    account.setPassword(password);
    account.setUser(user);
    account.setNames(names);

    return this.accountService.saveAccount(account);
  }

  @RequestMapping(value={"/ajaxValidate"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String ajaxValidate(@RequestBody Account account)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);
    return this.accountService.validatePassword(account);
  }

  @RequestMapping(value={"/ajaxSaveByUpdate"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String ajaxSaveByUpdate(@RequestBody Account account, HttpSession session)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);

    Logs logs = new Logs(Util.getUUID(), session.getAttribute("accountId").toString(), new Date(), "修改", "修改账号密码:" + account.getName(), session.getAttribute("ip").toString(), "0");
    this.logsMapper.addLogs(logs);

    return this.accountService.updatePassword(account);
  }

  @RequestMapping(value={"/ajaxChangeState"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String ajaxChangeAccountState(@RequestParam String state, @RequestParam String ids, HttpSession session)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);

    Logs logs = new Logs(Util.getUUID(), session.getAttribute("accountId").toString(), new Date(), "修改", "修改账号的状态", session.getAttribute("ip").toString(), "0");
    this.logsMapper.addLogs(logs);

    this.accountService.updateAccountState(state, ids);
    return "T";
  }

  @RequestMapping(value={"/ajaxDelete"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String delete(@RequestParam String ids, HttpSession session)
    throws Exception
  {
    DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);

    Logs logs = new Logs(Util.getUUID(), session.getAttribute("accountId").toString(), new Date(), "删除", "删除账号信息", session.getAttribute("ip").toString(), "0");
    this.logsMapper.addLogs(logs);

    this.accountService.deleteAccount(ids);
    return "T";
  }
}