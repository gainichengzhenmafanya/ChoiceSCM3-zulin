package com.choice.framework.web.controller.system;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 * 登录验证次数，防暴力破解
 * @author prolong
 *
 */
public class LoginSecurity {
	
	private static final Logger LOG = LoggerFactory.getLogger(LoginSecurity.class);
	
    private Map<String, List<Long>> tryRecords = new ConcurrentHashMap<String, List<Long>>(10);

    public static LoginSecurity INSTANCE = null;
    
    static {
    	if (INSTANCE == null)
    		initInstance();
    }
    
    private LoginSecurity(){}
    
    private static synchronized void initInstance(){
    	if(INSTANCE == null)
    		INSTANCE = new LoginSecurity();
    }

    private static final int maxTry = 5;

    private static final int TIME_LIMIT_IN_MINUTES = 5;

    public boolean locked(String userName) {
        List<Long> times = tryRecords.get(userName);
        if (times == null || times.size() < maxTry) {
            return false;
        }
        long curr = System.currentTimeMillis();

        if(times.size() >=maxTry) {
            for(int i = 0; i < maxTry; i++) {
                if (times.get(i) != 0 && (curr - times.get(i)) < 1000 * 60 * TIME_LIMIT_IN_MINUTES) {
                    if(LOG.isTraceEnabled()){
                    	LOG.trace("userName is ["+ userName+"] , 使用了["+ times.size()+"]次.");
                    }
                  return true;
                } 
                times.remove(i);
                if(LOG.isTraceEnabled()){
                	LOG.trace("userName is ["+ userName+"] , remove one start.");
                }
            }
        }
        
        return false;
    }

    public void loginRecord(String userName, boolean success) {
        List<Long> times = tryRecords.get(userName);
        if (times == null) {
            times = new ArrayList<Long>(maxTry);
            tryRecords.put(userName, times);
        }
        if (success) {
            times.clear();
            return;
        } 
        
        int idx = 0;
        long curr = System.currentTimeMillis();
        for (int i = 0; i < Math.min(maxTry, times.size()); i++) {
            if (times.get(i) < curr) {
                curr = times.get(i);
                idx = i;
            }
        }
        if (times.size() >= maxTry) {
            times.set(idx, System.currentTimeMillis());
        } else {
            times.add(System.currentTimeMillis());
        }
        
    }

    public void debug(String userName) {
        List<Long> records = tryRecords.get(userName);
        for (int i = 0; i < records.size(); i++) {
            System.out.println(records.get(i));
        }
        System.out.println("=============");
    }
}
