package com.choice.framework.web.controller.system;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.system.RoleTimeConstants;
import com.choice.framework.domain.system.RoleTime;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.service.system.RoleTimeService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.orientationSys.constants.StringConstant;

@Controller
@RequestMapping(value = "roleTime")
public class RoleTimeController {

	@Autowired
	private RoleTimeService roleTimeService;
	
	/**保存页面
	 * @param modelMap
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/savePage")
	public ModelAndView savePage(ModelMap modelMap,String roleId) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		RoleTime roleTime_Role = roleTimeService.findRoleTimeByRoleId(roleId);
		if(roleTime_Role == null){
			RoleTime roleTime = new RoleTime();
			roleTime.setRoleId(roleId);
			modelMap.put("roleTime", roleTime);
			modelMap.put("status", "add");
		}else{
			modelMap.put("roleTime", roleTime_Role);
			modelMap.put("status", "update");
		}
		return new ModelAndView(RoleTimeConstants.SAVE_ROLETIME, modelMap);
	}
	
	/**保存页面
	 * @param modelMap
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveOrUpdate")
	public ModelAndView saveOrUpdate(ModelMap modelMap,RoleTime roleTime,String status) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(status.equals("add")){
			roleTimeService.saveRoleTime(roleTime);
		}else{
			roleTimeService.updateRoleTime(roleTime);
		}
		return new ModelAndView(StringConstant.ACTION_DONE, modelMap);
	}
	
	@ResponseBody
	@RequestMapping(value = "findRoleTimeByAccountId")
	public RoleTime findRoleTimeByAccountId(HttpSession session) throws CRUDException{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		String accountId = session.getAttribute("accountId").toString();
		RoleTime roleTime = roleTimeService.findRoleTimeByAccountId(accountId);
		if(roleTime == null){
			roleTime = new RoleTime();
			roleTime.setAfterDay("3000");
			roleTime.setBeforeDay("3000");
		}
		return roleTime;
	}
}
