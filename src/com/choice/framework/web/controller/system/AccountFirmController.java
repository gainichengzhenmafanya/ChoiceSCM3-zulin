package com.choice.framework.web.controller.system;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.choice.framework.domain.system.AccountFirm;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountFirmService;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Util;


@Controller
@RequestMapping(value = "accountFirm")
public class AccountFirmController {
	
	@Autowired
	private AccountFirmService accountFirmService;
	@Autowired
	private LogsMapper logsMapper;
	
	/**
	 * 查询账户具有的分店
	 * @param accountFirm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/findFirmByAccountId")
	@ResponseBody
	public Object findFirmByAccountId(AccountFirm accountFirm,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.SELECT,"查询账户具有的分店",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		
		return accountFirmService.findAccountFirm(accountFirm);
	}
	
	/**
	 * 修改账户具有的分店
	 * @param accountFirm
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateAccountFirm")
	@ResponseBody
	public void updateAccountFirm(AccountFirm accountFirm,HttpSession session) throws Exception {
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		//加入日志
		Logs logs=new Logs(Util.getUUID(),session.getAttribute("accountId").toString(),new Date(),ProgramConstants.UPDATE,"修改账户具有的分店",session.getAttribute("ip").toString(),ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		
		accountFirmService.saveAccountFirm(accountFirm);
	}
	
}

