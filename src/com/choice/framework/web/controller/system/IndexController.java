package com.choice.framework.web.controller.system;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.choice.framework.constants.system.LoginConstants;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.domain.system.Module;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.ModuleService;
import com.choice.framework.shiro.exception.ActivationAccountException;
import com.choice.framework.shiro.exception.CaptchaException;
import com.choice.framework.shiro.exception.UnknownCompanyException;
import com.choice.framework.shiro.tools.CacheTools;
import com.choice.framework.shiro.tools.CaptchaUtil;
import com.choice.framework.shiro.tools.ShiroConstants;
import com.choice.framework.shiro.tools.Tools;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.WorkBench;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.MainInfoService;

/**
 * usedfor：shiro登录控制器
 * Created by javahao on 2017/7/27.
 * auth：JavaHao
 */
@Controller
@SessionAttributes({"accountId","accountName","ChoiceAcct","locale","crmFirmId","crmFirmDes","ip"})
public class IndexController {
    private static final Logger LOG = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    private AcctService acctService;
    @Autowired
    private MainInfoService mainInfoService;
    @Autowired
    private ModuleService moduleService;
    @Autowired
    private LogsMapper logsMapper;
    @Autowired
    private AccountPositnService accountPositnService;//查询当前登录用户的所属分店 wjf

    @RequestMapping(value = "/login")
    public String showLoginForm(HttpServletRequest req, Model model)
            throws CRUDException {
        String exceptionClassName = (String) req
                .getAttribute("shiroLoginFailure");
        String error = null;
        if(org.apache.commons.lang3.StringUtils.isBlank(exceptionClassName)&& UserSpace.isLogin()){
            return "redirect:/main.do";
        }
        if (CaptchaException.class.getName().equals(exceptionClassName)) {
            error = "验证码输入有误！";
        } else if (UnknownAccountException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误！";
        } else if (IncorrectCredentialsException.class.getName().equals(
                exceptionClassName)) {
            error = "用户名/密码错误！";
            AtomicInteger prc = CacheTools.get(ShiroConstants.PASSWORDRETRY_CACHE,
                    Tools.getRetryPwdKey(req.getParameter("name")));
            model.addAttribute(
                    "accLoginCount",
                    ShiroConstants.RETRY_LOGIN_COUNT
                            - prc.get());
        } else if (LockedAccountException.class.getName().equals(
                exceptionClassName)) {
            error = "您得账号已被锁定！请与管理员联系。";
        } else if (ExcessiveAttemptsException.class.getName().equals(
                exceptionClassName)) {
            error = "您的密码5次输入失败！已被锁定。请与管理员联系。";
//			userService.lockUser(req.getParameter("username"));
        }else if (ActivationAccountException.class.getName().equals(
                exceptionClassName)) {
            error = "您的账号未激活";
            model.addAttribute("showActivation", true);
        }else if (UnknownCompanyException.class.getName().equals(
                exceptionClassName)) {
            error = "未知企业ID";
        } else if (exceptionClassName != null) {
            error = "未知错误：请与管理员联系！";
        }
        model.addAttribute("token",UserSpace.getLoginToken());
        model.addAttribute("msg", error);
        return "login";
    }

    @RequestMapping({"/main","/"})
    public ModelAndView main(ModelMap modelMap)throws CRUDException{
        Account account = UserSpace.getToken().getAccount();
        modelMap.put("accountId", UserSpace.getAccountId());
        //查询当前用户所属分店wjf2014.11.21
        AccountPositn accountPositn=accountPositnService.findAccountById(UserSpace.getAccountId());
        modelMap.put("accountPositn", accountPositn == null?"":accountPositn.getPositn() == null?"":accountPositn.getPositn().getDes());
        modelMap.put("account", account);
        modelMap.put("accountName", account.getName());
        modelMap.put("locale", "zh_CN");
        //当前帐套     当前会计月
        Map<String,Object> map=acctService.findAcctMainInfo();
        modelMap.put("mainInfo",map);
        modelMap.put("sysDate", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
        //modelMap.put("ChoiceAcct",map.get("code"));
        modelMap.put("ChoiceAcct","1");
        //获取当前登录用户定义的常用操作
        WorkBench workBench = mainInfoService.findWorkBench(UserSpace.getAccountId());
        List<Module> tagList = new ArrayList<Module>();
        if(null!=workBench && null != workBench.getTagpage()){
            String ids = "'"+workBench.getTagpage().replace(",", "','")+"'";
            tagList = moduleService.findModuleByIds(ids);
        }
        modelMap.put("tagList",tagList);
        //加入登陆日志
        String ip= Util.getIpAddr(UserSpace.getRequest());
        modelMap.put("ip", ip);
        Logs logs=new Logs(Util.getUUID(),UserSpace.getAccountId(),new Date(), LoginConstants.LOGIN_EVENTS,LoginConstants.LOGIN_CONTENTS,ip, ProgramConstants.OVERALL);
        logsMapper.addLogs(logs);
        try {
            //版本号
            String   scmVersion= ForResourceFiles.getValByKey("config-scm.properties","scmVersion");
            String   bohVersion=ForResourceFiles.getValByKey("config-boh.properties","bohVersion");
            String   crmVersion=ForResourceFiles.getValByKey("config-crm.properties","crmVersion");

            String  version=scmVersion+"-"+bohVersion+"-"+crmVersion;
//				String  version=scmVersion;
            modelMap.put("version",version);
        } catch (Exception e) {
            // TODO: handle exception
            String  version=ForResourceFiles.getValByKey("config.properties","version");
            modelMap.put("version",version);
        }

        return new ModelAndView(LoginConstants.MAIN, modelMap);
    }

    private static final String SECRETKEY = "9AAB1D2EE004AAC3";
    public static final String KEY_CAPTCHA = "SE_KEY_MM_CODE";


    @RequestMapping(value="/getImgRandom" ,method={RequestMethod.GET, RequestMethod.POST})
    public void getImgRandom(HttpSession session, HttpServletResponse response) {
        OutputStream out = null;
        BufferedImage image = null;
        try {
            response.setContentType("image/jpeg");
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0);

            CaptchaUtil tool = new CaptchaUtil();
            StringBuffer code = new StringBuffer();
            image = tool.genRandomCodeImage(code);
            session.removeAttribute(KEY_CAPTCHA);
            session.setAttribute(KEY_CAPTCHA, code.toString());

            out = response.getOutputStream();
            ImageIO.write(image, "JPEG", out);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } finally{
            image.flush();
            if(out!=null){
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequestMapping("/unauthorized")
    public String unauthorized(){
        return "error/unauthorized";
    }
}
