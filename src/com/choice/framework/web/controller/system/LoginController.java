package com.choice.framework.web.controller.system;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.choice.common.servlet.LoadCompInfoServlet;
import com.choice.framework.constants.StringConstant;
import com.choice.framework.constants.system.LoginConstants;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountFirm;
import com.choice.framework.domain.system.AccountPositn;
import com.choice.framework.domain.system.Logs;
import com.choice.framework.domain.system.Module;
import com.choice.framework.persistence.system.LogsMapper;
import com.choice.framework.persistence.system.ModuleMapper;
import com.choice.framework.service.system.AccountFirmService;
import com.choice.framework.service.system.AccountPositnService;
import com.choice.framework.service.system.LoginService;
import com.choice.framework.service.system.ModuleService;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.DataSourceInstances;
import com.choice.framework.util.DataSourceSwitch;
import com.choice.framework.util.DateFormat;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.util.ProgramConstants;
import com.choice.framework.vo.AccountCache;
import com.choice.orientationSys.util.ReadProperties;
import com.choice.orientationSys.util.Util;
import com.choice.scm.domain.Positn;
import com.choice.scm.domain.WorkBench;
import com.choice.scm.service.AcctService;
import com.choice.scm.service.MainInfoService;
import com.choice.scm.service.PositnService;
import com.choice.tele.domain.Firm;
import com.choice.tele.service.FirmService;

@Controller
@RequestMapping(value = "login")
@SessionAttributes({"accountId","accountName","ChoiceAcct","locale","crmFirmId","crmFirmDes","ip"})
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	@Autowired
	private LogsMapper logsMapper;
	@Autowired
	private AcctService acctService;
	@Autowired
	private MainInfoService mainInfoService;
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private AccountFirmService accountFirmService;
	@Autowired
	private FirmService firmService;
	@Autowired
	private AccountPositnService accountPositnService;//查询当前登录用户的所属分店 wjf
	@Autowired
	private ModuleMapper moduleMapper;
	@Autowired
	private PositnService positnService;


	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
	/**
	 * 账号登录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loginIn")
	public ModelAndView loginIn(HttpServletResponse response,HttpSession session,HttpServletRequest request, ModelMap modelMap, Account account, 
			String remember,String locale, String userInfo) throws Exception{
		
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		if(userInfo != null && !"".equals(userInfo.trim())){
//			account.setPassword(CyptoUtils.decode(SECRETKEY, userInfo));
		}
		
		String loginCheck = this.checkLogin(account, session);
		if(!StringUtils.isEmpty(loginCheck)){
			Cookie pk_group = new Cookie("pk_group",account.getPk_group());
			Cookie nameCookie = new Cookie("accountName", account.getName());
			nameCookie.setMaxAge(10 * 24 * 60 * 60);
			nameCookie.setPath("/");
			pk_group.setMaxAge(10 * 24 * 60 * 60);
			pk_group.setPath("/");
			response.addCookie(pk_group);
			response.addCookie(nameCookie);
			modelMap.put("info", loginCheck);
			modelMap.put("loginOut", "loginOut");
			return new ModelAndView(LoginConstants.LOGIN, modelMap);
		}
		
		LOG.debug("企业号为:[{}]" , session.getAttribute("pk_group"));
		AccountCache accountCache = loginService.loadAuthorityInfo(account, session);
		modelMap.put("accountCache", accountCache);
		modelMap.put("accountId", accountCache.getAccount().getId());
		//查询当前用户所属分店wjf2014.11.21
		AccountPositn accountPositn=accountPositnService.findAccountById(accountCache.getAccount().getId());
	    modelMap.put("accountPositn", accountPositn == null?"":accountPositn.getPositn() == null?"":accountPositn.getPositn().getDes());
		modelMap.put("account", account);
		modelMap.put("accountName", account.getName());
		modelMap.put("locale", locale);
		
		//判断是否自动登录
		Cookie rememberCookie = new Cookie("remember", remember);
		rememberCookie.setMaxAge(10 * 24 * 60 * 60);
		rememberCookie.setPath("/");
		response.addCookie(rememberCookie);
		if (StringConstant.TRUE.equals(remember)) {
			Cookie pk_group = new Cookie("pk_group",account.getPk_group());
			Cookie nameCookie = new Cookie("accountName", account.getName());
			Cookie pwCookie = new Cookie("password", account.getPassword());
			Cookie localeCookie = new Cookie("locale",locale);
			pk_group.setMaxAge(10 * 24 * 60 * 60);
			pk_group.setPath("/");
			nameCookie.setMaxAge(10 * 24 * 60 * 60);
			nameCookie.setPath("/");
			pwCookie.setMaxAge(10 * 24 * 60 * 60);
			pwCookie.setPath("/");
			localeCookie.setMaxAge(10 * 24 * 60 * 60);
			localeCookie.setPath("/");
			response.addCookie(pk_group);
			response.addCookie(nameCookie);
			response.addCookie(pwCookie);
			response.addCookie(localeCookie);
		} else {
			Cookie pk_group = new Cookie("pk_group",account.getPk_group());
			Cookie nameCookie = new Cookie("accountName", account.getName());
			Cookie pwCookie = new Cookie("password", "");
			Cookie localeCookie = new Cookie("locale","");
//				nameCookie.setMaxAge(0);
			pk_group.setMaxAge(10 * 24 * 60 * 60);
			pk_group.setPath("/");
			nameCookie.setMaxAge(10 * 24 * 60 * 60);
			nameCookie.setPath("/");
			pwCookie.setMaxAge(0);
			pwCookie.setPath("/");
			localeCookie.setMaxAge(0);
			localeCookie.setPath("/");
			response.addCookie(pk_group);
			response.addCookie(nameCookie);
			response.addCookie(pwCookie);
			response.addCookie(localeCookie);
		}
		//当前帐套     当前会计月
		Map<String,Object> map=acctService.findAcctMainInfo();
		modelMap.put("mainInfo",map);
		modelMap.put("sysDate", DateFormat.getStringByDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
		//modelMap.put("ChoiceAcct",map.get("code"));
		modelMap.put("ChoiceAcct","1");
		//获取当前登录用户定义的常用操作
		WorkBench workBench = mainInfoService.findWorkBench(accountCache.getAccount().getId());
		List<Module> tagList = new ArrayList<Module>();
		if(null!=workBench && null != workBench.getTagpage()){
			String ids = "'"+workBench.getTagpage().replace(",", "','")+"'";
			tagList = moduleService.findModuleByIds(ids);
		}
		modelMap.put("tagList",tagList);
		//加入登陆日志
		String ip=Util.getIpAddr(request);
		modelMap.put("ip", ip);
		Logs logs=new Logs(Util.getUUID(),accountCache.getAccount().getId(),new Date(),LoginConstants.LOGIN_EVENTS,LoginConstants.LOGIN_CONTENTS,ip,ProgramConstants.OVERALL);
		logsMapper.addLogs(logs);
		try {
			//版本号  
			String   scmVersion=ForResourceFiles.getValByKey("config-scm.properties","scmVersion");
			String   bohVersion=ForResourceFiles.getValByKey("config-boh.properties","bohVersion");
			String   crmVersion=ForResourceFiles.getValByKey("config-crm.properties","crmVersion");
			
			String  version=scmVersion+"-"+bohVersion+"-"+crmVersion;
//				String  version=scmVersion; 
			modelMap.put("version",version);
		} catch (Exception e) {
			// TODO: handle exception
			String  version=ForResourceFiles.getValByKey("config.properties","version");
			modelMap.put("version",version);
		}
		/*
		 * 淮扬菜\豪享来 \广州酒家 end
		 */
		//只有CRM
		ReadProperties rp = new ReadProperties();
		String onlyCrm = rp.getStrByParam("choice");
		if(onlyCrm.substring(2, 3).equals("1")){
			
			AccountFirm accountFirm = new AccountFirm();
			accountFirm.setAccountId(accountCache.getAccount().getId());
			AccountFirm result = accountFirmService.findAccountFirmTop1(accountFirm);
			if(null != result){
				String firmId = result.getFirmId();
				modelMap.put("crmFirmId",firmId );
				DataSourceSwitch.setDataSourceType(DataSourceInstances.CRM);//选择数据源
				Firm firm = firmService.findFirmByFirmId(firmId);
				if(null!=firm){
					modelMap.put("crmFirmDes", firm.getFirmdes());
				}
			}
		}
		
		return new ModelAndView(LoginConstants.MAIN, modelMap);
	}
	
	public String checkLogin (Account account, HttpSession session) throws Exception {
		
		if(StringUtils.isEmpty(account.getPk_group())){
			LOG.warn("企业号输入为空");
			return "企业号输入为空";
		}
		if(StringUtils.isEmpty(account.getName())|| StringUtils.isEmpty(account.getPassword())) {
			LOG.warn("用户名/密码输入为空, 用户名[{}]", account.getName());
			return "用户名/密码输入为空";
		}
		if(StringUtils.isEmpty(account.getValidateCode())) {
			LOG.warn("验证码输入为空");
			return "验证码输入为空";
		}
//		String code = (String)session.getAttribute(KEY_CAPTCHA);
//		LOG.debug("validate code realCode is [{}], validateCode is [{}]", code , account.getValidateCode());
//		if(!code.equalsIgnoreCase(account.getValidateCode())) {
//			LOG.warn("验证码输入错误, realCode is [{}], validateCode is [{}]", code , account.getValidateCode());
//			return "验证码输入错误";
//		}
		String pk_group = LoadCompInfoServlet.groupmap.get(account.getPk_group().toUpperCase());
		if(StringUtils.isEmpty(pk_group)) {
			LOG.warn("企业号输入错误, pk_group is [{}]", account.getPk_group());
			return "无无效的企业号";
		}
		session.setAttribute("pk_group", pk_group);
		String passWordCheck = loginService.validateLoginBySecurity(account,session);
		if(!"T".equals(passWordCheck)){
			LOG.warn("用户登录校验失败, message is [{}]", passWordCheck);
			return passWordCheck;
		}
		
		return null;
	}
	
	/**
	 * 账号登出
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loginOut")
	public ModelAndView loginOut(HttpServletResponse response,HttpSession session, ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
	//	Cookie nameCookie = new Cookie("accountName", "");
		Cookie pwCookie = new Cookie("password", "");
		Cookie localeCookie = new Cookie("locale","");
		Cookie rememberCookie = new Cookie("remember", "F");
	//	nameCookie.setMaxAge(-1);
	//	nameCookie.setPath("/");
		pwCookie.setMaxAge(-1);
		pwCookie.setPath("/");
		localeCookie.setMaxAge(-1);
		localeCookie.setPath("/");
		rememberCookie.setMaxAge(-1);
		rememberCookie.setPath("/");
		
	//	response.addCookie(nameCookie);
		response.addCookie(pwCookie);
		response.addCookie(localeCookie);
		response.addCookie(rememberCookie);

		//session失效
		session.invalidate();
		modelMap.put("loginOut", "loginOut");
		return new ModelAndView(LoginConstants.LOGIN, modelMap);
	}
	
	/**
	 * 账号登出
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/loginNull")
	public ModelAndView loginNull(HttpServletResponse response,HttpSession session, ModelMap modelMap) throws Exception{
		DataSourceSwitch.setDataSourceType(DataSourceInstances.SCM);//选择数据源
		return null;
	}
	
	/**
	 * 注册跳转
	 */
	@RequestMapping(value = "/regist")
	public ModelAndView regist(HttpServletResponse response, ModelMap modelMap) throws Exception{
		String regist = loginService.findRegist();
		modelMap.put("regist", regist);
		return new ModelAndView("regist", modelMap);
	}
	/**
	 * 注册保存
	 */
	@RequestMapping(value = "/saveRegist")
	@ResponseBody
	public String saveRegist(HttpServletResponse response,HttpSession session, ModelMap modelMap,String registCode) throws Exception{
		return loginService.saveRegist(registCode,session);
	}
	
	
	/**********************************************手机BS**********************************************/
	/**
	 * 登录
	 * @param modelMap
	 * @return
	 * @throws Exception
	 * @author YGB
	 */
	@RequestMapping("/loginWAP")
	@ResponseBody
	public Object loginWAP(HttpSession session,String groupId,String username,String password,String jsonpcallback) throws Exception{
		DataSourceSwitch.setDataSourceType("compJdbcTemplate_"+groupId);//选择数据源
		Map<String, Object> map = new HashMap<String, Object>();
		Account account = new Account();
		account.setPk_group(groupId);
		account.setName(username);
		account.setPassword(password);
		String info="";
		if(StringUtils.isBlank(groupId) || "0".equals(groupId)){ //说明企业号无效
			info = "无效企业号!";
		}else{
			info = loginService.validateLogin(account,session);
		}
		if(info.equals(StringConstant.TRUE)){
			AccountCache accountCache = loginService.loadAuthorityInfo(account, session);
			map.put("accountId", accountCache.getAccount().getId());
			//菜单列表
			List<Module> oldModuleList = this.moduleMapper.findModuleByAccount(accountCache.getAccount().getId());
			List<String> listCode = new ArrayList<String>();
		    int len = CodeHelper.CODE_LENGTH;
		    int endIndex = 0;
		    for (Module module : oldModuleList) {
		      String moduleCode = module.getCode();
		      String[] codes = moduleCode.split("-");
		      int i = 0; for (int n = codes.length; i < n; i++) {
		        endIndex = len + (len + 1) * i;
		        String code = moduleCode.substring(0, endIndex);
		        if (!listCode.contains(code)) {
		          listCode.add(code);
		        }
		      }
		    }
		    map.put("moduleList", this.moduleMapper.findModuleByCodes(listCode));
		    //所属分店
		    AccountPositn accountPositn=accountPositnService.findAccountById(accountCache.getAccount().getId());
		    map.put("accountPositn", accountPositn.getPositn());
		    //可用分店
		    Positn positn = new Positn();
		    positn.setTyp("1203");
		    positn.setAccountId(accountCache.getAccount().getId());
		    map.put("positnList", positnService.findAllPositn(positn));
		    
		    map.put("acct", "1");//帐套
			map.put("account", account);
			map.put("accountName", account.getName());
			map.put("menu", "1,2,3,4");
		} else{
			map.put("loginOut", info);
		}
		return jsonpcallback + "( "+JSONObject.fromObject(map).toString()+");";
	}
	
}

