package com.choice.framework.redis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.util.Hashing;
import redis.clients.util.Pool;
import redis.clients.util.SafeEncoder;

/***
 * Jedis 实体封装
 * @author prolong
 *
 */
public class MyShardedJedis extends ShardedJedis{
	
	private static final Logger LOG = LoggerFactory.getLogger(MyShardedJedis.class);
	
	protected Pool<MyShardedJedis> dataSource = null;
	
	private ExecutorService executor;
	
	public MyShardedJedis(List<JedisShardInfo> shards) {
		super(shards);
		executor = Executors.newCachedThreadPool();
	}

	public MyShardedJedis(List<JedisShardInfo> shards, Hashing algo,
			Pattern keyTagPattern) {
		super(shards, algo, keyTagPattern);
		executor = Executors.newFixedThreadPool(shards.size());
	}

	public MyShardedJedis(List<JedisShardInfo> shards, Hashing algo) {
		super(shards, algo);
		executor = Executors.newFixedThreadPool(shards.size());
	}

	public MyShardedJedis(List<JedisShardInfo> shards, Pattern keyTagPattern) {
		super(shards, keyTagPattern);
		executor = Executors.newFixedThreadPool(shards.size());
	}
	
	public List<byte[]> mget(byte[] ... keys){
		HashMap<Jedis, List<byte[]>> map = new HashMap<Jedis, List<byte[]>>();
		for(byte[] key : keys) {
			Jedis j = getShard(key);
			if(map.containsKey(j)){
				List<byte[]> keyList = new LinkedList<byte[]>();
				keyList.add(key);
				map.put(j, keyList);
			} else {
				map.get(j).add(key);
			}
		}
		List<byte[]> valList = new LinkedList<byte[]>();
		List<Callable<List<byte[]>>> jobs = new ArrayList<Callable<List<byte[]>>>();
		List<byte[]> allKeys = new ArrayList<byte[]>();
		for(Entry<Jedis, List<byte[]>> entry : map.entrySet()) {
			Jedis j = entry.getKey();
            List<byte[]> keyList = entry.getValue();
            allKeys.addAll(keyList);
            byte[][] jKeys = keyList.toArray(new byte[keyList.size()][]);
            jobs.add(new JedisCallable(j, jKeys));
		} 
		
		// 合并数据
        try {
            List<Future<List<byte[]>>> taskGets = executor.invokeAll(jobs);
            for (Future<List<byte[]>> f : taskGets) {
                valList.addAll(f.get());
            }
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        } catch (ExecutionException e) {
            LOG.error(e.getMessage(), e);
        }
        HashMap<byte[], byte[]> keyVal = new HashMap<byte[], byte[]>();
        for (int i = 0; i < allKeys.size(); i++) {
            keyVal.put(allKeys.get(i), valList.get(i));
        }
        valList.clear();
        for (byte[] key : keys) {
            valList.add(keyVal.get(key));
        }
        return valList;
	}
	
	 /**
     * 获取多个key数据
     * @param keys
     * @return
     */
    public List<byte[]> mget(String... keys) {
        return mget(SafeEncoder.encodeMany(keys));
    }

    /**
     * 删除多个key
     * @param keys
     * @return
     */
    public long del(String... keys) {
        long sum = 0;
        for (String key : keys) {
            sum += del(key);
        }

        return sum;
    }

    public void setMyDataSource(Pool<MyShardedJedis> shardedJedisPool) {
        this.dataSource = shardedJedisPool;
    }

    @Override
    public void close() {
        if (dataSource != null) {
            boolean broken = false;

            for (Jedis jedis : getAllShards()) {
                if (jedis.getClient().isBroken()) {
                    broken = true;
                }
            }

            if (broken) {
                dataSource.returnBrokenResource(this);
            } else {
                this.resetState();
                dataSource.returnResource(this);
            }

        } else {
            disconnect();
        }
    }

}
