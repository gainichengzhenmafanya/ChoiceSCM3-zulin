package com.choice.framework.redis;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;

/***
 * Jedis Server服务
 * @author prolong
 *
 */
public class JedisServer {

	public static final Logger LOG = LoggerFactory.getLogger(JedisServer.class);

	private static JedisPoolConfig config = new JedisPoolConfig();

	private static MyShardedJedisPool pool = null;

	private static JedisServer jedisServer = null;

	private static Object lock = new Object();

	private JedisServer() {
	}

	public static JedisServer getJedisServer() {
		if (jedisServer == null) {
			synchronized (lock) {
				jedisServer = new JedisServer();
				jedisServer.init();
			}
		}

		return jedisServer;
	}

	/***
	 * 初始化
	 */
	private void init() {

		// 设置最大空闲
		config.setMaxIdle(RedisConfig.getInt("redis.maxidle"));

		// 设置最大等待时间
		config.setMaxWaitMillis(RedisConfig.getInt("redis.maxwaitmills"));
		// 池中最大连接数,默认50
		config.setMaxTotal(RedisConfig.getInt("redis.maxtotal", 50));
		// 设置连接的最小空闲时间,默认60分钟
		config.setMinEvictableIdleTimeMillis(RedisConfig.getInt("redis.minEvictableIdleTime", 60 * 60 * 1000));
		// borrow 一个jedis实例前是否进行validate操作
		config.setTestOnBorrow(true);

		pool = new MyShardedJedisPool(config, getJedisServerInfo());
	}

	/***
	 * 获取Redis配置信息并加载
	 * @return
	 */
	public List<JedisShardInfo> getJedisServerInfo() {
		String configStr = RedisConfig.getString("redis.server.list");
		String[] servers = configStr.split(";");
		// 获取配置文件的timeout,默认为2s
		int timeout = RedisConfig.getInt("redis.server.timeout", 2 * 1000);
		List<JedisShardInfo> jedisList = new ArrayList<JedisShardInfo>();

		for (int i = 0; i < servers.length; i++) {
			String server = servers[i];
			String[] serverParam = server.split(":");
			if (serverParam.length < 2) {
				continue;
			}
			String ip = serverParam[0];
			String sport = serverParam[1];
			int port = 0;
			try {
				port = Integer.parseInt(sport);
			} catch (NumberFormatException e) {
				LOG.error("ip[" + ip + "] port [" + sport + "] format error", e);
			}
			LOG.debug("MyShardedJedis server ip [{}], port [{}]", ip, port);
			JedisShardInfo jedisInfo = new JedisShardInfo(ip, port, timeout);
			if(1 == RedisConfig.getInt("redis.haspassword")){
				jedisInfo.setPassword(RedisConfig.getString("redis.auth"));
			}

			jedisList.add(jedisInfo);
		}
		return jedisList;
	}

	/***
	 * 获取Jedis实例
	 * @return
	 */
	public MyShardedJedis getJedis() {
		if (pool != null) {
			return pool.getResource();
		}
		LOG.error("pool 没有初始化...");
		return null;
	}

	/***
	 * 用完返回Jedis
	 * @param jedis
	 */
	public void returnJedis(MyShardedJedis jedis) {
		pool.returnResource(jedis);
	}

}
