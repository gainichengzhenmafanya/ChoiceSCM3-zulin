package com.choice.framework.redis.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.choice.framework.redis.JedisServer;
import com.choice.framework.redis.MyShardedJedis;

/***
 * 企业参数缓存
 * @author prolong
 *
 */
public class SbohMemCache implements IMemCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(SbohMemCache.class);

	private JedisServer jedisServer;
	private SbohMemCache(){jedisServer = JedisServer.getJedisServer();}
	
	private static ThreadLocal<SbohMemCache> bohMemCache;
	
	private static final String KEY = "_SBOH";
	
	/***
	 * 获取企业缓存实体
	 * @return
	 */
	public static SbohMemCache getInstance() {
		if(bohMemCache == null) bohMemCache = new SbohMemCacheThreadLocal();
		SbohMemCache sbohCache = bohMemCache.get();
		if(sbohCache == null)
			sbohCache = new SbohMemCache();
		return sbohCache;
	}

	/***
	 * 获取企业所有参数
	 */
	@Override
	public List<SysParam> getAllParams(String fieldHeader) {
		if(StringUtils.isEmpty(fieldHeader)) return null;
		MyShardedJedis jedis = jedisServer.getJedis();
		List<SysParam> scmParamList = null;
		try {
			Map<String, String> scmParams = jedis.hgetAll(fieldHeader+ KEY);
			if (scmParams == null || scmParams.isEmpty()) {
				return null;
			}
			scmParamList = new ArrayList<SysParam>();
			for (Map.Entry<String, String> map : scmParams.entrySet()) {
				SysParam param = new SysParam();
				param.setParamCode(map.getKey());
				param.setParamValue(map.getValue());
				scmParamList.add(param);
			}
			return scmParamList;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e );
			return null;
		} finally {
			jedisServer.returnJedis(jedis);
		}
	}

	/***
	 * 获取企业参数
	 */
	@Override
	public String getParamValueByKey(String... key) {
		if(key == null|| key.length != 2) return null;
		MyShardedJedis jedis = jedisServer.getJedis();
		try {
			return jedis.hget(key[0]+KEY, key[1]);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}finally{
			jedisServer.returnJedis(jedis);
		}
	}

	/***
	 * 设置企业参数
	 */
	@Override
	public boolean setParam(SysParam param) {
		if(param == null) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		long setRes = -1;
		try {
			setRes = jedis.hset(param.getVcode()+ KEY, param.getParamCode(), param.getParamValue());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally{
			jedisServer.returnJedis(jedis);
		}
		return setRes > -1;
	}

	/***
	 * 设置企业参数
	 */
	@Override
	public boolean setParam(String ... params) {
		if(params == null || params.length != 3) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		long setRes = -1;
		try {
			setRes = jedis.hset(params[0]+KEY, params[1], params[2]);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally{
			jedisServer.returnJedis(jedis);
		}
		return setRes > -1;
	}

	/***
	 * 设置企业参数
	 * @param vCode
	 * @param paramMap
	 * @return
	 */
	public boolean setParams(String vCode, Map<String, String> paramMap) {
		if(StringUtils.isEmpty(vCode)||paramMap == null) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		try {
			for(Map.Entry<String, String> map: paramMap.entrySet()){
				long res = jedis.hset(vCode + KEY, map.getKey(), map.getValue());
				if(res < 0) {
					LOG.error("sboh setParams failed , paramCode is [{}], paramValue is [{}]", map.getKey(), map.getValue());
				}
			}
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		} finally{
			jedisServer.returnJedis(jedis);
		}
	}

	/***
	 * 设置企业参数
	 */
	@Override
	public boolean setParams(List<SysParam> paramList) {
		if(paramList == null ||paramList.isEmpty()) return false;
		MyShardedJedis jedis = jedisServer.getJedis();
		try {
			for(SysParam demo: paramList){
				long res = jedis.hset(demo.getVcode()+ KEY, demo.getParamCode(), demo.getParamValue());
				if(res < 0) {
					LOG.warn("sboh setParams failed , paramCode is [{}], paramValue is [{}]", demo.getParamCode(), demo.getParamValue());
				}
			}
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		} finally{
			jedisServer.returnJedis(jedis);
		}
	}
	
	private static class SbohMemCacheThreadLocal extends ThreadLocal<SbohMemCache>{
		@Override
		protected synchronized SbohMemCache initialValue() {
			return new SbohMemCache();
		}
		
	}
	

}
