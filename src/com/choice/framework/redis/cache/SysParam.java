package com.choice.framework.redis.cache;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SysParam implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String type;

	private String paramCode;
	
	private String paramName;
	
	private String paramValue;
	
	private String vcode;
	
	private String userName;
	
	private String model;
	
	private String note;
	
	private Date updateOn;
	
	List<SysParam> paramList;
	
	public Integer getId() {
		return id;
	}

	public List<SysParam> getParamList() {
		return paramList;
	}

	public void setParamList(List<SysParam> paramList) {
		this.paramList = paramList;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVcode() {
		return vcode;
	}

	public void setVcode(String vcode) {
		this.vcode = vcode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	

}
