package com.choice.framework.shiro.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountRole;
import com.choice.framework.domain.system.Module;
import com.choice.framework.domain.system.Operate;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountRoleMapper;
import com.choice.framework.persistence.system.ModuleMapper;
import com.choice.framework.util.CodeHelper;
import com.choice.framework.util.ForResourceFiles;
import com.choice.framework.vo.AccountCache;

/**
 * usedfor：权限加载控制类
 * Created by javahao on 2017/8/8.
 * auth：JavaHao
 */
@Service
public class AuthorityInfoService {
    @Autowired
    private AccountRoleMapper accountRoleMapper;

    @Autowired
    private ModuleMapper moduleMapper;

    /**
     * 加载指定账号的权限
     * @param accountResult 账号
     * @return 账号缓存
     * @throws CRUDException 异常
     */
    public AccountCache loadAuthorityInfo(Account accountResult) throws CRUDException {


        //获取登陆账号关联的角色信息
        AccountRole accountRole = new AccountRole();
        accountRole.setAccountId(accountResult.getId());
        List<AccountRole> accountRoleList = accountRoleMapper.findAccountRole(accountRole);

        //获取登陆账号拥有的模块和操作信息
        Module mo = new Module();
        mo.setAccountId(accountResult.getId());
        mo.setProtyp(CodeHelper.replaceCode(ForResourceFiles.getValByKey("module.properties","ownmodule")));
        List<Module> oldModuleList = moduleMapper.findModuleByAccountProtyp(mo);

        //组合模块信息（用于菜单显示）
        List<Module> moduleList = this.comboModule(oldModuleList,"OK");

        //获取登陆账号拥有的操作信息
        HashMap<String, HashMap<String, Boolean>> moduleOperateMap = this.comboOperate(oldModuleList);

        //清空账号的缓存信息，并重新添加账号的缓存信息
        AccountCache accountCache = new AccountCache();
        accountCache.setAccount(accountResult);
        accountCache.setAccountRoleList(accountRoleList);
        accountCache.setModuleList(moduleList);
        accountCache.setModuleOperateMap(moduleOperateMap);

        return accountCache;
    }

    /**
     * 获取需要显示的模块信息
     * @param moduleList 模块集合
     * @param result 标识
     * @return 菜单
     * @throws CRUDException 异常
     */
    private List<Module> comboModule(List<Module> moduleList,String result) throws CRUDException{
        //如果模块为空，则直接返回一个空的模块列表
        if(moduleList == null || moduleList.size() == 0)
            return new ArrayList<Module>();

        List<String> listCode = new ArrayList<String>();

        //模块编号
        String moduleCode;

        //截取的模块编号
        String code;

        //存放“-”分割后的编号
        String[] codes ;

        //每一级编号的长度
        int len = CodeHelper.CODE_LENGTH;

        //截取编号的截止位置
        int endIndex = 0;

        //遍历拥有操作的模块（只有最后一层模块有操作，其它模块没有操作），
        //根据模块的编号进行拆分，找到模块对应的父类及祖先
        for(Module module : moduleList){
            moduleCode = module.getCode();
            if(result.equals("NO")){
                return null;
            }
            codes = moduleCode.split("-");
            for(int i = 0,n = codes.length; i < n; i++){
                endIndex = len + (len+1)*i;
                code = moduleCode.substring(0, endIndex);
                //	System.out.println(code);
                if(!listCode.contains(code)){
                    listCode.add(code);
                }
            }
        }

        List<Module> listModule=new ArrayList<Module>();
        List<String> listCode1000 = new ArrayList<String>();
        if(listCode.size()<1000){
            return moduleMapper.findModuleByCodes(listCode);
        }else{
            for (int i = 0; i < listCode.size(); i++) {
                listCode1000.add(listCode.get(i));
                if((i%999)==0 && i>0){
                    listModule.addAll(moduleMapper.findModuleByCodes(listCode1000));
                    listCode1000.clear();
                }
            }
            listModule.addAll(moduleMapper.findModuleByCodes(listCode1000));
            return listModule;
        }
    }

    /**
     * 获取需要显示的操作信息
     * @param moduleList
     * @return
     * @throws CRUDException
     */
    private HashMap<String, HashMap<String, Boolean>> comboOperate(List<Module> moduleList) throws CRUDException{
        HashMap<String, HashMap<String, Boolean>> moduleOperateMap = new HashMap<String, HashMap<String,Boolean>>();

        //遍历模块，判断模块是否有url，并且url的结构是否符合“/controllerMapping/methodMapping.do”的形式。
        //如果满足上述条件，则遍历模块下的操作，并组合成“HashMap<模块Id, HashMap<操作Id, 是否拥有操作>>”的形式。
        for(Module module : moduleList){
            if(module.getUrl() != null && !module.getUrl().equals("")
                    && module.getUrl().split("/").length > 1){

                String key = module.getUrl().split("/")[1];
                HashMap<String, Boolean> operateMap =
                        moduleOperateMap.containsKey(key) ? moduleOperateMap.get(key) : new HashMap<String, Boolean>();
                for(Operate operate : module.getChildOperate()){
                    operateMap.put(operate.getType(), Boolean.TRUE);
                }
                moduleOperateMap.put(key, operateMap);
            }
        }

        return moduleOperateMap;
    }
}
