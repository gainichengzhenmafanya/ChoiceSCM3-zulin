package com.choice.framework.shiro.shiroredis.manager;

import com.choice.framework.util.ForResourceFiles;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * usedfor：redis集群模式
 * Created by javahao on 2017/8/1.
 * auth：JavaHao
 */
public class MissBean implements Condition{
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        String opencluster = ForResourceFiles.getValByKey("redis.properties","opencluster");
        return !conditionContext.getBeanFactory().containsBean("curatorClient")&&
                (null != opencluster && "1".equals(opencluster));
    }
}
