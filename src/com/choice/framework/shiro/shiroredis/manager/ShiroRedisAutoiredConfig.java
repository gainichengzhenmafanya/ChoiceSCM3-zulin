package com.choice.framework.shiro.shiroredis.manager;

import com.choice.framework.redis.RedisConfig;
import com.choice.framework.shiro.shiroredis.RedisManager;
import com.choice.framework.shiro.shiroredis.cache.ShiroRedisCacheManager;
import com.choice.framework.shiro.shiroredis.cache.ShiroRedisCacheRepository;
import com.choice.framework.shiro.shiroredis.session.ShiroZookeeperSessionDao;
import com.choice.framework.shiro.shiroredis.zookeeper.CuratorClient;
import com.choice.framework.shiro.shiroredis.zookeeper.listener.RedisCacheListener;
import com.choice.framework.shiro.shiroredis.zookeeper.listener.ZookeeperListener;
import com.choice.framework.shiro.shiroredis.zookeeper.listener.ZookeeperSessionListener;
import com.choice.framework.shiro.tools.IDUtil;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * usedfor：shiro的redis以及cache在spring集群或者单机模式中依赖的实例管理器
 * Created by javahao on 2017/8/1.
 * auth：JavaHao
 */
@Configuration
public class ShiroRedisAutoiredConfig {

    @Bean
    @Conditional(CloudMode.class)
    public RedisManager redisManager(){
    	
        RedisManager redisManager = new RedisManager();
        String hostPort = RedisConfig.getString("redis.server.list");
        String[] hostPortList = hostPort.split(";");
        int idx = hostPortList[0].indexOf(":");
        redisManager.setHost(hostPortList[0].substring(0, idx));
        redisManager.setPort(Integer.parseInt(hostPortList[0].substring(idx+1 , hostPortList[0].length())));
        redisManager.setExpire(3600);
        if(1 == RedisConfig.getInt("redis.haspassword"))
        	redisManager.setAuth(RedisConfig.getString("redis.auth"));
        return redisManager;
    }

    @Bean
    @Conditional(CloudMode.class)
    public ShiroRedisCacheRepository shiroRedisCacheRepository(RedisManager redisManager, @Qualifier("secondLevelCacheManager")CacheManager secondLevelCacheManager){
        ShiroRedisCacheRepository shiroRedisCacheRepository = new ShiroRedisCacheRepository();
        shiroRedisCacheRepository.setRedisManager(redisManager);
        shiroRedisCacheRepository.setSecondLevelCacheManager(secondLevelCacheManager);
        return shiroRedisCacheRepository;
    }

    /**
     * 开启集群的cachemanager
     * @param shiroRedisCacheRepository 缓存仓库
     * @return 返回实例
     */
    @Bean(name = "cacheManager")
    @Conditional(CloudMode.class)
    public ShiroRedisCacheManager CloudCacheManager(ShiroRedisCacheRepository shiroRedisCacheRepository){
        ShiroRedisCacheManager shiroRedisCacheManager = new ShiroRedisCacheManager();
        shiroRedisCacheManager.setShiroCacheRepository(shiroRedisCacheRepository);
        return shiroRedisCacheManager;
    }

    /**
     * 标准单机模式
     * @return 返回实例
     */
    @Bean(name = "cacheManager")
    @Conditional(StandMode.class)
    public EhCacheManager StandCacheManager(){
        return new EhCacheManager();
    }


    /**
     * 集群环境sessiondao
     * @return 返回实例
     */
//    @Bean(name = "sessionDAO")
//    @Conditional(CloudMode.class)
//    public ShiroRedisCacheSessionDao CloudSessionDAO(ShiroRedisSessionRepository shiroRedisSessionRepository,IDUtil idUtil){
//        ShiroRedisCacheSessionDao shiroRedisSessionDao = new ShiroRedisCacheSessionDao();
//        shiroRedisSessionDao.setShiroSessionRepository(shiroRedisSessionRepository);
//        shiroRedisSessionDao.setSessionIdGenerator(idUtil);
//        return shiroRedisSessionDao;
//    }
    @Bean(name = "sessionDAO")
    @Conditional(CloudMode.class)
    public ShiroZookeeperSessionDao CloudSessionDAO(IDUtil idUtil){
        ShiroZookeeperSessionDao sessionDao = new ShiroZookeeperSessionDao();
        sessionDao.setSessionIdGenerator(idUtil);
        return sessionDao;
    }
    @Bean
    public IDUtil sessionIdGenerator(){
        return new IDUtil();
    }

    /**
     * 标准单机模式
     * @param idUtil sessionid生成器
     * @return sessionDao实例
     */
    @Bean(name = "sessionDAO")
    @Conditional(StandMode.class)
    public EnterpriseCacheSessionDAO StandSessionDAO(IDUtil idUtil){
        EnterpriseCacheSessionDAO enterpriseCacheSessionDAO = new EnterpriseCacheSessionDAO();
        enterpriseCacheSessionDAO.setSessionIdGenerator(idUtil);
        enterpriseCacheSessionDAO.setActiveSessionsCacheName("shiro-activeSessionCache");
        return enterpriseCacheSessionDAO;
    }

    @Bean(name = "curatorClient")
    @Conditional(MissBean.class)
    public CuratorClient startZookeeper(@Qualifier("secondLevelCacheManager")CacheManager secondLevelCacheManager){
//        EmbedZookeeperServer.start(2181);
        List<ZookeeperListener> linsteners = new ArrayList<ZookeeperListener>();
        linsteners.add(new RedisCacheListener(CuratorClient.CACHE_PATH,secondLevelCacheManager));
//        linsteners.add(new RedisSessionListener(CuratorClient.SESSION_PATH,secondLevelCacheManager));
        linsteners.add(new ZookeeperSessionListener(secondLevelCacheManager));
        CuratorClient.setListeners(linsteners);
        CuratorClient.registerListeners();
        return new CuratorClient();
    }
}
