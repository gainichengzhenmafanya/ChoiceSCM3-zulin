package com.choice.framework.shiro.shiroredis.manager;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.choice.framework.util.ForResourceFiles;

/**
 * usedfor：标准单机模式
 * Created by javahao on 2017/8/1.
 * auth：JavaHao
 */
public class StandMode implements Condition{
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        String opencluster = ForResourceFiles.getValByKey("redis.properties","opencluster");
        return null != opencluster && "0".equals(opencluster);
    }
}
