package com.choice.framework.shiro.shiroredis.zookeeper;

import com.choice.framework.shiro.shiroredis.zookeeper.listener.ZookeeperListener;
import com.choice.framework.shiro.tools.SerializeUtils;
import com.choice.framework.util.ForResourceFiles;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.UnhandledErrorListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * usedfor：
 * Created by javahao on 2017/8/15.
 * auth：JavaHao
 */
public class CuratorClient implements DisposableBean {
    private static Logger logger = LoggerFactory.getLogger(CuratorClient.class);
    private static CuratorFramework client;
    private static String connectionString= ForResourceFiles.getValByKey("zookeeper.properties","zookeeper.server.url");
    private static List<ZookeeperListener> listeners;
    /**
     * zookeeper缓存清除配置存储路径
     */
    public static final String CACHE_PATH="/scm/door/cache";
    public static final String SESSION_PATH="/scm/door/session";

    public static void setConnectionString(String connectionString) {
        CuratorClient.connectionString = connectionString;
    }

    public static void setListeners(List<ZookeeperListener> listeners) {
        CuratorClient.listeners = listeners;
    }

    /**
     * 删除节点
     * @param path 路径
     * @return 结果
     */
    public static boolean delete(String path){
        try {
            if(!checkExists(path))
                return true;
            getClient().delete().forPath(path);
        } catch (Exception e) {
            logger.error("删除节点失败："+path,e);
        }
        return true;
    }

    /**
     * 获取子节点数据集合
     * @param path 父节点
     * @param <T> 数据类型
     * @return 结果
     */
    public static <T> Set<T> getChilds(String path){
        Set<T> results = null;
        try {
            //如果节点不存在则返回空
            if(!checkExists(path))
                return null;
            List<String> childsPath = getClient().getChildren().forPath(path);
            results = new HashSet<T>();
            byte[] nodeData = null;
            for(String childPath : childsPath){
                if(StringUtils.isBlank(childPath))
                    continue;
                nodeData = get(path+"/"+childPath);
                T t = null;
                if(nodeData!=null&&(t=(T)SerializeUtils.deserialize(nodeData))!=null) {
                    results.add(t);
                }else {
                    delete(path+"/"+childPath);
                }
            }
        } catch (Exception e) {
            logger.error("获取子节点数据集合错误",e);
        }
        return results;
    }

    /**
     * 获取节点数据
     * @param path 节点路径
     * @return 结果
     */
    public static byte[] get(String path){
        try {
            //如果节点不存在则返回空
            if(!checkExists(path))
                return null;
            return getClient().getData().forPath(path);
        } catch (Exception e) {
            logger.error("获取节点数据错误："+path,e);
        }
        return null;
    }

    /**
     * 判断节点是否存在
     * @param path 节点路径
     * @return 结果
     */
    public static boolean checkExists(String path){
        //如果节点不存在则返fasle
        try {
            if(getClient().checkExists().forPath(path)==null)
                return false;
        } catch (Exception e) {
            logger.error("校验节点是否存在失败："+path,e);
        }
        return true;
    }

    /**
     * 设置zookeeper节点数据，有则更新无则插入
     * @param path 节点路径
     * @param obj 数据
     * @return 成功标识
     */
    public static boolean update(String path,Object obj){
        try {
            //如果节点不存在则返回空
            if(checkExists(path))
                getClient().setData().forPath(path,SerializeUtils.serialize(obj));
            else
                getClient().create().creatingParentsIfNeeded().forPath(path,SerializeUtils.serialize(obj));
        } catch (Exception e) {
            logger.error("设置节点数据失败："+path,e);
        }
        return true;
    }

    /**
     * 获取zookeeper操作客户端
     * @return 返回客户端
     */
    public static CuratorFramework createSimple(){
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);
        return CuratorFrameworkFactory.newClient(connectionString, retryPolicy);
    }

    public static CuratorFramework getClient() {
        if(client==null) {
            client = createSimple();
            client.start();
        }
        return client;
    }

    /**
     * 清除配置内所有节点缓存
     * @param vo 配置
     */
    public static void clearNodesCache(ClearCacheVo vo){
        try {
            String path = CACHE_PATH+"/"+vo.getCacheKey();
            Stat s = getClient().checkExists().forPath(path);
            //如果节点存在直接将清除缓存配置设置到此节点，不存在则创建节点并设置数据
            if(s!=null){
                getClient().setData().forPath(path,SerializeUtils.serialize(vo));
            }else
                getClient().create().creatingParentsIfNeeded().forPath(path,SerializeUtils.serialize(vo));
        } catch (Exception e) {
            logger.error("标识清除缓存节点配置失败！",e);
        }
    }

    /**
     * 清除配置内所有节点session
     * @param vo 配置
     */
    public static void clearSessionCache(ClearSessionVo vo){
        try {
            String path = SESSION_PATH+"/"+vo.getSession().getId();
            Stat s = getClient().checkExists().forPath(path);
            //如果节点存在直接将清除缓存配置设置到此节点，不存在则创建节点并设置数据
            if(s!=null){
                getClient().setData().forPath(path,SerializeUtils.serialize(vo));
            }else
                getClient().create().creatingParentsIfNeeded().forPath(path,SerializeUtils.serialize(vo));
        } catch (Exception e) {
            logger.error("标识清除Session节点配置失败！",e);
        }
    }

    /**
     * 注册zookeeper节点的监听
     */
    public static void registerListeners(){
        getClient().getConnectionStateListenable().addListener(new ConnectionStateListener() {
            @Override
            public void stateChanged(CuratorFramework client, ConnectionState newState) {
                logger.info("CuratorFramework state changed: {}", newState);
                if(newState == ConnectionState.CONNECTED || newState == ConnectionState.RECONNECTED){
                    for(ZookeeperListener listener : listeners){
                        listener.executor(client);
                        logger.info("Listener {} executed!", listener.getClass().getName());
                    }
                }
            }
        });

        getClient().getUnhandledErrorListenable().addListener(new UnhandledErrorListener() {
            @Override
            public void unhandledError(String message, Throwable e) {
                logger.info("CuratorFramework unhandledError: {}", message);
            }
        });
    }

    @Override
    public void destroy() throws Exception {
        getClient().close();
    }

    public static void main(String[] args) throws Exception {
        CuratorFramework client = getClient();
        Stat stat = client.checkExists().forPath("/scm/cache");
        System.out.println(stat);
        if(stat!=null)
            client.setData().forPath("/scm/cache", SerializeUtils.serialize( new ClearCacheVo("authorizationCache","5db4373868d24118959ac15f8f11d69d.authorizationCache")));
        else
            client.create().creatingParentsIfNeeded().forPath("/scm/cache", SerializeUtils.serialize( new ClearCacheVo("authorizationCache","c77ddd161f354f9ab665a998b358179d.authorizationCache")));
//        System.out.println(SerializeUtils.deserialize(client.getData().forPath("/scm/cache")));
        client.close();
    }
}
