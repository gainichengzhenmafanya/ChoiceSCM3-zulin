package com.choice.framework.shiro.shiroredis.zookeeper.listener;

import org.apache.curator.framework.CuratorFramework;

/**
 * usedfor：zookeeper监听类
 * Created by javahao on 2017/8/15.
 * auth：JavaHao
 */
public interface ZookeeperListener {
    void executor(CuratorFramework client);
}
