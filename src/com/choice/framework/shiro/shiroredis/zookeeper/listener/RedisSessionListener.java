package com.choice.framework.shiro.shiroredis.zookeeper.listener;

import com.choice.framework.shiro.shiroredis.zookeeper.ClearSessionVo;
import com.choice.framework.shiro.shiroredis.zookeeper.CuratorClient;
import com.choice.framework.shiro.tools.SerializeUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent.Type.CHILD_ADDED;
import static org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent.Type.CHILD_UPDATED;

/**
 * usedfor：二级session同步监听
 * Created by javahao on 2017/8/15.
 * auth：JavaHao
 */
public class RedisSessionListener implements ZookeeperListener {
    Logger log = (Logger) LoggerFactory.getLogger(this.getClass());
    private String path= CuratorClient.SESSION_PATH;

    private CacheManager cacheManager;

    public RedisSessionListener() {
    }

    public RedisSessionListener(String path, CacheManager cacheManager) {
        this.path = path;
        this.cacheManager = cacheManager;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public CacheManager getCacheManager() {
        return cacheManager;
    }

    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @Override
    public void executor(CuratorFramework client) {
        ExecutorService pool = Executors.newCachedThreadPool();
        PathChildrenCache childrenCache = new PathChildrenCache(client, CuratorClient.SESSION_PATH, true);
        PathChildrenCacheListener childrenCacheListener = new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
                ChildData child = event.getData();
                if(event.getType()==CHILD_ADDED||event.getType()==CHILD_UPDATED){
                    byte[] data = child.getData();
                    ClearSessionVo vo = (ClearSessionVo) SerializeUtils.deserialize(data);
                    log.debug(String.format("【%s】Session发生变化Zookeeper清空节点缓存数据！",vo.toString()));
                    Cache c = null;
                    if((c=cacheManager.getCache(vo.getCacheName()))!=null
                            &&c.get(vo.getSession().getId())!=null)
                        c.remove(vo.getSession().getId());
                }
            }
        };
        childrenCache.getListenable().addListener(childrenCacheListener,pool);
        try {
            childrenCache.start(PathChildrenCache.StartMode.POST_INITIALIZED_EVENT);
        } catch (Exception e) {
            log.error("",e);
        }
    }
}
