package com.choice.framework.shiro.shiroredis.zookeeper;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;

import java.io.Serializable;

/**
 * usedfor：清除session的vo对象
 * Created by javahao on 2017/8/15.
 * auth：JavaHao
 */
public class ClearSessionVo implements Serializable {
    private String cacheName= CachingSessionDAO.ACTIVE_SESSION_CACHE_NAME;
    private Session session;

    public ClearSessionVo() {
    }

    public ClearSessionVo(Session session) {
        this.session = session;
    }

    public ClearSessionVo(String cacheName, Session session) {
        this.cacheName = cacheName;
        this.session = session;
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public String toString() {
        return "ClearSessionVo{" +
                "cacheName='" + cacheName + '\'' +
                ", session=" + session +
                '}';
    }
}
