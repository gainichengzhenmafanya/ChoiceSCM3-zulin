package com.choice.framework.shiro.shiroredis.zookeeper;

import java.io.Serializable;

/**
 * usedfor：清除缓存vo对象
 * Created by javahao on 2017/8/15.
 * auth：JavaHao
 */
public class ClearCacheVo implements Serializable {
    private String cacheName;
    private String cacheKey;

    public ClearCacheVo() {
    }

    public ClearCacheVo(String cacheName, String cacheKey) {
        this.cacheName = cacheName;
        this.cacheKey = cacheKey;
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    @Override
    public String toString() {
        return "ClearCacheVo{" +
                "cacheName='" + cacheName + '\'' +
                ", cacheKey='" + cacheKey + '\'' +
                '}';
    }
}
