package com.choice.framework.shiro.shiroredis.session;

import com.choice.framework.shiro.shiroredis.zookeeper.CuratorClient;
import com.choice.framework.shiro.tools.SerializeUtils;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.ValidatingSession;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * usedfor：用zookeeper管理session
 * Created by javahao on 2017/8/16.
 * auth：JavaHao
 */
public class ShiroZookeeperSessionDao extends CachingSessionDAO {
    public static final String SHIRO_SESSION_ZK_ROOT_PATH="/scm/repository/shirosession";
    private boolean useMemCache = true;
    public ShiroZookeeperSessionDao() {
    }

    /**
     * SESSION ZK DAO 实例
     * 如果开户缓存
     * 用户登录时自动缓存, 用户登录超时自动删除
     * 由于shiro的cacheManager是全局的, 所以这里使用setActiveSessionsCache直接设置Cache来本地缓存, 而不使用全局zk缓存.
     * 由于同一用户可能会被路由到不同服务器,所以在doReadSession方法里也做了缓存增加.
     *
     * @param useMemCache 是否使用内存缓存登录信息
     */
    public ShiroZookeeperSessionDao(boolean useMemCache) {
        this.useMemCache = useMemCache;
    }

    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 缓存根路径, 结尾不加/
     */
    private String shiroSessionZKPath = SHIRO_SESSION_ZK_ROOT_PATH;

    /**
     * 缓存项前缀
     */
    private String sessionPrefix = "session-";

    /**
     * 设置Shiro Session 前缀 默认 session-
     *
     * @param sessionPrefix
     */
    public void setSessionPrefix(String sessionPrefix) {
        this.sessionPrefix = sessionPrefix;
    }

    /**
     * 设置Shiro在ZK服务器存放根路径
     *
     * @param shiroSessionZKPath 默认值："/scm/repository/shirosession"
     */
    public void setShiroSessionZKPath(String shiroSessionZKPath) {
        this.shiroSessionZKPath = shiroSessionZKPath;
    }

    /**
     * session更新
     * @param session
     * @throws UnknownSessionException
     */
    @Override
    public void update(Session session) throws UnknownSessionException {
        if (session == null || session.getId() == null) {
            logger.error("session argument cannot be null.");
        }
        try {
            //如果会话过期/停止 没必要再更新了
            if (session instanceof ValidatingSession && !((ValidatingSession) session).isValid()) {
                return;
            }
            if (session instanceof ShiroSession) {
                // 如果没有主要字段(除lastAccessTime以外其他字段)发生改变
                ShiroSession ss = (ShiroSession) session;
                if (!ss.isChanged()) {
                    return;
                }
                ss.setChanged(false);
                ss.setLastAccessTime(new Date());
                saveSession(session);
                this.cache(session, session.getId());
            }
        } catch (Exception e) {
            logger.error("更新Session失败", e);
        }
    }

    @Override
    protected void doUpdate(Session session) {
    }

    /**
     * session删除
     *
     * @param session
     */
    @Override
    public void delete(Session session) {
        if (session == null || session.getId() == null) {
            logger.error("session argument cannot be null.");
        }
        logger.debug("delete session for id: {}", session.getId());
        CuratorClient.delete(getPath(session.getId()));
        if (useMemCache) {
            this.uncache(session);
        }
    }

    @Override
    protected void doDelete(Session session) {
    }

    /**
     * 获取当前活跃的session, 当前在线数量
     *
     * @return
     */
    @Override
    public Collection<Session> getActiveSessions() {
        Set<Session> sessions = CuratorClient.getChilds(shiroSessionZKPath);
        Set<Session> sessionResults = new HashSet<Session>();
        for(Session s : sessions)
            if(s instanceof ShiroSession) {
                if (!((ShiroSession) s).outDate()) {
                    sessionResults.add(s);
                } else {
                    //清除掉过期的session
                    CuratorClient.delete(getPath(s.getId()));
                }
            }else{
                sessionResults.add(s);
            }

        if(sessionResults!=null)
            logger.debug("shiro getActiveSessions. size: {}", sessionResults.size());
        return sessionResults;
    }

    /**
     * 创建session, 用户登录
     *
     * @param session
     * @return
     */
    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        saveSession(session);
        return sessionId;
    }

    /**
     * session读取
     *
     * @param sessionId
     * @return
     */
    @Override
    protected Session doReadSession(Serializable sessionId) {
        if (sessionId == null) {
            logger.error("id is null!");
            return null;
        }
        logger.debug("doReadSession for path: {}", getPath(sessionId));

        Session session = getCachedSession(sessionId);
        byte[] byteData = CuratorClient.get(getPath(sessionId));
        if (byteData != null && byteData.length > 0) {
            session = (Session) SerializeUtils.deserialize(byteData);
            if (useMemCache) {
                this.cache(session, sessionId);
                logger.debug("doReadSession for path: {}, add cached !", getPath(sessionId));
            }
            return session;
        } else {
            return null;
        }
    }

    /**
     * 生成全路径
     *
     * @param sessID
     * @return
     */
    private String getPath(Serializable sessID) {
        return shiroSessionZKPath + '/' + sessionPrefix + sessID.toString();
    }

    /**
     * session读取或更新
     *
     * @param session
     */
    private void saveSession(Session session) {
        CuratorClient.update(getPath(session.getId()), session);
    }
    @Override
    public void setCacheManager(CacheManager cacheManager) {
        if(cacheManager instanceof EhCacheManager)
            super.setCacheManager(cacheManager);
    }
}
