package com.choice.framework.shiro.shiroredis.cache;

import org.apache.shiro.cache.Cache;

/**
 * usedfor：缓存仓库接口
 * Created by javahao on 2017/7/31.
 * auth：JavaHao
 */
public interface ShiroCacheRepository {
    <K, V> Cache<K, V> getCache(String name);

    void destroy();
}