package com.choice.framework.shiro.shiroredis.cache;

import com.choice.framework.shiro.shiroredis.RedisManager;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;

/**
 * usedfor：缓存仓库接口实现
 * Created by javahao on 2017/7/31.
 * auth：JavaHao
 */
public class ShiroRedisCacheRepository implements ShiroCacheRepository {

    private RedisManager redisManager;
    private CacheManager secondLevelCacheManager;

    public void setSecondLevelCacheManager(CacheManager secondLevelCacheManager) {
        this.secondLevelCacheManager = secondLevelCacheManager;
    }

    public RedisManager getRedisManager() {
        return redisManager;
    }

    public void setRedisManager(RedisManager redisManager) {
        this.redisManager = redisManager;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) {
        return new ShiroRedisCache<K, V>(redisManager, name,secondLevelCacheManager.getCache(name));
    }

    @Override
    public void destroy() {
        redisManager.init();
        redisManager.flushDB();
    }

}
