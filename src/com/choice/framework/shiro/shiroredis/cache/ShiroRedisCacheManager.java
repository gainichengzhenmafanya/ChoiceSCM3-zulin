package com.choice.framework.shiro.shiroredis.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.util.Destroyable;

/**
 * usedfor：cache管理容器
 * Created by javahao on 2017/7/31.
 * auth：JavaHao
 */
public class ShiroRedisCacheManager implements CacheManager, Destroyable {

    private ShiroCacheRepository shiroCacheRepository;

    public ShiroCacheRepository getShiroCacheRepository() {
        return shiroCacheRepository;
    }

    public void setShiroCacheRepository(ShiroCacheRepository shiroCacheRepository) {
        this.shiroCacheRepository = shiroCacheRepository;
    }

    @Override
    public void destroy() throws Exception {
        getShiroCacheRepository().destroy();
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        return getShiroCacheRepository().getCache(name);
    }

}
