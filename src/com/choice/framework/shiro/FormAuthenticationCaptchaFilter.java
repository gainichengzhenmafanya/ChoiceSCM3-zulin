package com.choice.framework.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import com.choice.framework.shiro.captcha.CaptchaServlet;
import com.choice.framework.shiro.exception.CaptchaException;
import com.choice.framework.shiro.tools.UserSpace;

/**
 *
 * @author <a href="http://www.micmiu.com">Michael Sun</a>
 */
public class FormAuthenticationCaptchaFilter extends FormAuthenticationFilter {

	private String captchaParam;
	private String pkGroup;

	public void setCaptchaParam(String captchaParam) {
		this.captchaParam = captchaParam;
	}

	public String getCaptchaParam() {
		return captchaParam;
	}

	protected String getCaptcha(ServletRequest request) {

		return WebUtils.getCleanParam(request, getCaptchaParam());

	}

	public String getPkGroup() {
		return pkGroup;
	}

	public void setPkGroup(String pkGroup) {
		this.pkGroup = pkGroup;
	}

	protected String getGroup(ServletRequest request) {

		return WebUtils.getCleanParam(request, getPkGroup());

	}

	@Override
	protected boolean executeLogin(ServletRequest request, ServletResponse response)
			throws Exception {
		ScmLoginToken token = (ScmLoginToken) createToken(request, response);
		if(token == null) {
			String msg = "createToken method implementation returned null. A valid non-null AuthenticationToken must be created in order to execute a login attempt.";
			throw new IllegalStateException(msg);
		} else {
			try {
				UserSpace.setLoginToken(token);
				doCaptchaValidate(token);
				Subject subject = this.getSubject(request, response);
				subject.login(token);
				WebUtils.getAndClearSavedRequest(request);
				return this.onLoginSuccess(token, subject, request, response);
			} catch (AuthenticationException var5) {
				return this.onLoginFailure(token, var5, request, response);
			}
		}
	}
	// 验证码校验  
    protected void doCaptchaValidate(ScmLoginToken token) {
    	//session中的图形码字符串  
    	String captcha = (String) SecurityUtils.getSubject().getSession()
				.getAttribute(CaptchaServlet.KEY_CAPTCHA);
        //比对  
    	if (null == captcha || !captcha.equalsIgnoreCase(token.getCaptcha())) {
			throw new CaptchaException("验证码错误");
		}
    } 
	protected AuthenticationToken createToken(

            ServletRequest request, ServletResponse response) {

		String username = getUsername(request);

		String password = getPassword(request);

		String captcha = getCaptcha(request);

		String group = getGroup(request);

		boolean rememberMe = isRememberMe(request);

		String host = getHost(request);

		return new ScmLoginToken(username,
				password.toCharArray(), rememberMe, host, captcha,group);

	}

}