package com.choice.framework.shiro;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.choice.framework.shiro.handler.Handler;
import com.choice.framework.shiro.tools.ShiroConstants;
import com.choice.framework.shiro.tools.Tools;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.util.MD5;

public class RetryLimitHashedCredentialsMatcher extends HashedCredentialsMatcher {

    private Cache<String, AtomicInteger> passwordRetryCache;
    private Cache<String, AuthenticationInfo> authenticationInfoCache;
    @Autowired(required = false)
    @Qualifier("loginSuccessHandler")
    private Handler handler;
    
	private static final Logger LOG = LoggerFactory.getLogger(RetryLimitHashedCredentialsMatcher.class);

    
    public RetryLimitHashedCredentialsMatcher(CacheManager cacheManager) {
        passwordRetryCache = cacheManager.getCache(ShiroConstants.PASSWORDRETRY_CACHE);
        authenticationInfoCache = cacheManager.getCache(ShiroConstants.AUTHENTICATION_CACHE);
    }

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        ScmLoginToken sct = (ScmLoginToken) token;
        String userName = sct.getUsername();

        boolean matches = check(sct,info);

        //super.doCredentialsMatch(token, info);
        if(matches) {
            //clear retry count
            passwordRetryCache.remove(Tools.getRetryPwdKey(userName));
            if(handler!=null)
                handler.execute();
            
        }else{
            //未登录成功清除身份认证缓存
            authenticationInfoCache.remove(userName);
            //retry count + 1
            AtomicInteger retryCount = passwordRetryCache.get(Tools.getRetryPwdKey(userName));
            if(retryCount == null) {
                retryCount = new AtomicInteger(1);
            }else{
                retryCount.incrementAndGet();
            }
            if(retryCount.get() > ShiroConstants.RETRY_LOGIN_COUNT) {
                //if retry count > 5 throw
                throw new ExcessiveAttemptsException();
            }
            passwordRetryCache.put(Tools.getRetryPwdKey(userName), retryCount);
        }
        return matches;
    }

    /**
     * 校验登录凭证
     * @param sct token
     * @return 结果
     */
    public boolean check(ScmLoginToken sct, AuthenticationInfo info){
        try {
            if(String.valueOf(info.getCredentials()).equals(MD5.md5(sct.getAccount().getName()+sct.getAccount().getPassword()))) {
                UserSpace.getSession().setAttribute("accountNames",sct.getAccount().getNames());
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
