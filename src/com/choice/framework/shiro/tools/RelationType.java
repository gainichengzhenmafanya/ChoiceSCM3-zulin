package com.choice.framework.shiro.tools;

/**
 * usedfor：关系枚举
 * Created by javahao on 2017/7/18.
 * auth：JavaHao
 */
public enum RelationType {
    OR,AND;
}
