package com.choice.framework.shiro.tools;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authc.UsernamePasswordToken;

import com.choice.framework.domain.system.Account;

/**
 * usedfor：记录账号以及企业信息
 * Created by javahao on 2017/7/29.
 * auth：JavaHao
 */
public class AccountToken implements Serializable {
    /**
     * 当前账户信息
     */
    private Account account;
    /**
     * 当前登录的token信息
     */
    private UsernamePasswordToken token;

    private Map<String,Object> cacheMap = new HashMap<String,Object>();

    public AccountToken() {
    }

    public AccountToken(Account account, UsernamePasswordToken token) {
        this.account = account;
        this.token = token;
    }

    /**
     * 从当前用户缓存中获取缓存值
     * @param key 缓存键
     * @param <T> 缓存值泛型
     * @return 结果
     */
    public <T> T getCache(String key){
        return (T) cacheMap.get(key);
    }

    /**
     * 设置当前用户的缓存
     * @param key 缓存键
     * @param value 缓存值
     */
    public void addCache(String key,Object value){
        cacheMap.put(key,value);
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public UsernamePasswordToken getToken() {
        return token;
    }

    public void setToken(UsernamePasswordToken token) {
        this.token = token;
    }
}
