package com.choice.framework.shiro.tools;

import com.choice.framework.domain.system.Account;
import com.choice.framework.shiro.ScmLoginToken;
import com.choice.framework.util.DataSourceSwitch;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;

/**
 * usedfor：用户空间，可以存储一些当前用户的信息和当前request、response、session
 * Created by javahao on 2017/7/27.
 * auth：JavaHao
 */
public class UserSpace {
    private static ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();
    private static ThreadLocal<HttpServletResponse> response = new ThreadLocal<HttpServletResponse>();
    private static ThreadLocal<HttpSession> session = new ThreadLocal<HttpSession>();
    private static ThreadLocal<AuthenticationToken> loginToken = new ThreadLocal<AuthenticationToken>();

    public static HttpServletRequest getRequest() {
        return request.get();
    }

    public static void setRequest(HttpServletRequest request) {
        UserSpace.request.set(request);
    }

    public static HttpServletResponse getResponse() {
        return response.get();
    }

    public static void setResponse(HttpServletResponse response) {
        UserSpace.response.set(response);
    }

    public static HttpSession getSession() {
        return session.get();
    }

    public static void setSession(HttpSession session) {
        UserSpace.session.set(session);
    }

    public static <T extends AuthenticationToken> T getLoginToken() {
        return (T)loginToken.get();
    }

    public static void setLoginToken(AuthenticationToken loginToken) {
        UserSpace.loginToken.set(loginToken);
    }

    /**
     * 判断当前用户是否登录
     * @return 是否登录标识
     */
    public static boolean isLogin(){
        return SecurityUtils.getSubject().isAuthenticated();
    }

    /**
     * 获取当前账号的token信息
     * @return 返回token
     */
    public static AccountToken getAccountToken(){
        Subject subject = SecurityUtils.getSubject();
        if(subject==null){
            subject.logout();
            return null;
        }
        return (AccountToken)subject.getPrincipal();
    }

    /**
     * 获取token
     * @return 返回结果
     */
    public static ScmLoginToken getToken(){
        if(isLogin())
            return (ScmLoginToken)getAccountToken().getToken();
        return null;
    }

    /**
     * 获取当前登录账号
     * @return 当前登录账号信息
     */
    public static Account getAccount(){
        if(isLogin())
            return getAccountToken().getAccount();
        return null;
    }

    /**
     * 获取当前登录账户的ID
     * @return 账户ID信息
     */
    public static String getAccountId(){
        Account account = null;
        if((account=getAccount())!=null)
            return account.getId();
        return null;
    }

    /**
     * 判断是否是管理员
     * @return 结果
     */
    public static boolean isAdmin(){
        return hasRoles(GlobalConstants.ROLES_ADMIN);
    }
    /**
     * 判断当前用户是否拥有某种角色
     * @param roles 判断的角色
     * @return 结果
     */
    public static boolean hasRoles(String... roles){
        return hasRoles(RelationType.AND, roles);
    }

    /**
     * 判断当前用户是否拥有某种角色
     * @param relationType 关系类型“并且”或者“或者”
     * @param roles 角色ids
     * @return 结果
     */
    public static boolean hasRoles(RelationType relationType,String... roles){
        boolean[] results = SecurityUtils.getSubject().hasRoles(Arrays.asList(roles));
        return Functions.isBoolean(relationType,results);
    }

    /**
     * 获取sessionid
     * @return 返回结果
     */
    public static String getSessionId(){
        return (String) SecurityUtils.getSubject().getSession().getId();
    }
    /**
     * 按照当前登录账号切换数据源
     */
    public static void switchDatasource(){
        String pkGroup = "";
        if(isLogin())
            pkGroup = ((ScmLoginToken)getAccountToken().getToken()).getPkGroup();
        else{
            String pkGroupRequest = getRequest().getParameter("pk_group");
            if(!StringUtils.isBlank(pkGroupRequest))
                pkGroup = pkGroupRequest;
            else
                return;
        }
        DataSourceSwitch.setDataSourceType("compJdbcTemplate_"+pkGroup);//选择数据源
    }
}
