package com.choice.framework.shiro.tools;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;

/**
 * usedfor：用于自定义el标签
 * Created by javahao on 2017/8/3.
 * auth：JavaHao
 */
public final class Functions {
    private static final transient Log log = LogFactory.getLog(Functions.class);

    /**
     * 是否有权限
     * @param url 请求地址，例如/user/list.do
     * @param operType 权限类型多个用逗号隔开
     * @return 结果
     */
    public static Boolean isPermitted(String url,String operType){
        return isRelationPermitted(RelationType.AND,url,operType);
    }
    /**
     * 是否有权限
     * @param url 请求地址，例如/user/list.do
     * @param operType 权限类型多个用逗号隔开
     * @return 结果
     */
    public static Boolean isRelationPermitted(RelationType relationType,String url,String operType){
        if(StringUtils.isEmpty(url)||StringUtils.isEmpty(operType))
            return false;
        String[] urlSplit = url.split("/");
        String module = getModule(urlSplit);
        if(module==null||module.length()<=0)
            return false;

        String[]  operArray = operType.split(",");
        String[] permissions = new String[operArray.length];
        for(int i = 0 ;i < operArray.length; i++)
            permissions[i]=module+":"+operArray[i];
        boolean[] results = SecurityUtils.getSubject().isPermitted(permissions);
        return isBoolean(relationType,results);
    }

    /**
     * 解析URL地址中的模块名称
     * @param urlsplit url分割
     * @return 模块名称
     */
    public static String getModule(String... urlsplit){
        if(urlsplit!=null&&urlsplit.length!=0){
            for(int i = 0; i<urlsplit.length; i++){
                if(urlsplit[i].length()>0&&urlsplit[i].indexOf(".do")!=-1)
                    return i>0?urlsplit[i-1]:urlsplit[i].replace(".do","");
            }
        }
        return null;
    }

    /**
     * 判断Boolean按照关系类型返回结果
     * @param relation 多资源关系
     * @param p shiro验证结果
     * @return 返回是否有权限访问
     */
    public static boolean isBoolean(RelationType relation, boolean[] p){
        boolean hasFalse=false;
        boolean hasTrue=false;
        for(Boolean b:p){
            if(!b)
                hasFalse = true;
            else
                hasTrue = true;
        }
        switch (relation){
            case AND :
                return !hasFalse;
            case OR :
                return hasTrue;
            default:return false;
        }
    }
}
