package com.choice.framework.shiro.tools;

/**
 * usedfor：全局变量存储
 * Created by javahao on 2017/8/7.
 * auth：JavaHao
 */
public final class GlobalConstants {
    /**
     * 管理员角色ID
     */
    public static final String ROLES_ADMIN="d3f34652eb03447b9cc8bb7375df675d";
}
