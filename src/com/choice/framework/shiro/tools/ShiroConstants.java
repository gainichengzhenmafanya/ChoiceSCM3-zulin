package com.choice.framework.shiro.tools;

/**
 * usedfor：shiro的常量配置
 * Created by javahao on 2017/7/27.
 * auth：JavaHao
 */
public final class ShiroConstants {
    /**
     * 密码重试次数缓存
     */
    public static final String PASSWORDRETRY_CACHE="pwd_retry_cache";
    /**
     * 身份认证缓存
     */
    public static final String AUTHENTICATION_CACHE="authenticationCache";
    /**
     * 账号重试尝试次数
     */
    public static final int RETRY_LOGIN_COUNT = 5;

    /**
     * 账号为激活状态
     */
    public static final String ACCOUNT_STATUS_ACTIVE = "1";
}
