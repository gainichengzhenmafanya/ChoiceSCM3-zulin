package com.choice.framework.shiro.tools;

/**
 * usedfor：工具类
 * Created by javahao on 2017/8/4.
 * auth：JavaHao
 */
public class Tools {
    /**
     * 获取重试密码次数缓存key
     * @param uname 用户名
     * @return 返回key结果
     */
    public static String getRetryPwdKey(String uname){
        return ShiroConstants.PASSWORDRETRY_CACHE+"_"+uname;
    }
}
