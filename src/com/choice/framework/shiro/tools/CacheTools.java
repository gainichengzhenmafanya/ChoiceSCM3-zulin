package com.choice.framework.shiro.tools;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;

/**
 * usedfor：缓存管理工具
 * Created by javahao on 2017/8/9.
 * auth：JavaHao
 */
public class CacheTools {
    private static CacheManager cacheManager = null;

    /**
     * 获取上下文中缓存管理器
     * @return 缓存管理器
     */
    public static CacheManager getCacheManager(){
        return cacheManager==null?cacheManager=SpringUtils.getBean(CacheManager.class):cacheManager;
    }

    /**
     * 按照缓存名称获取缓存对象
     * @param cacheName 缓存名称
     * @param <C> 缓存泛型
     * @return 缓存对象
     */
    public static <C extends Cache> C  getCache(String cacheName){
        return (C) getCacheManager().getCache(cacheName);
    }

    /**
     * 按照缓存域跟键获取缓存的值
     * @param cacheName 缓存名称
     * @param key 缓存键
     * @param <V> 缓存值泛型
     * @param <K> 缓存键泛型
     * @return 缓存结果
     */
    public static <V,K> V get(String cacheName,K key){
        Cache<K,V> cache = getCache(cacheName);
        if(cache!=null)
            return cache.get(key);
        return null;
    }

    /**
     * 往指定缓存域放入指定缓存
     * @param cacheName 缓存域
     * @param k 键
     * @param v 值
     * @param <K> 键泛型
     * @param <V> 值泛型
     * @return 成功标识
     */
    public static <K,V> boolean put(String cacheName,K k,V v){
        Cache<K,V> cache = getCache(cacheName);
        cache.put(k,v);
        return true;
    }

    /**
     * 删除缓存
     * @param cacheName 缓存名称
     * @param k 缓存键
     * @param <K> 键泛型
     * @param <V> 键值
     * @return 删除标识
     */
    public static <K,V> boolean remove(String cacheName,K k){
        Cache<K,V> cache = getCache(cacheName);
        cache.remove(k);
        return true;
    }
}
