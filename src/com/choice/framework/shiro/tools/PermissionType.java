package com.choice.framework.shiro.tools;

/**
 * usedfor：操作类型
 * Created by javahao on 2017/7/18.
 * auth：JavaHao
 */
public enum PermissionType {
    ALONE("alone","切换"),BUILD("build","同步"),CHECK("check","审核"),CREAT("creat","生成"),
    DELETE("delete","删除"),DETAIL("detail","明细"),EXPORT("export","导出"),IMPORT("import","导入"),
    INSERT("insert","新增"),MARK("mark","抽取"),PRINT("print","打印"),SELECT("select","查询"),SUPPER("supper","过滤"),
    UPDATE("update","修改"),WRITEOFF("writeoff","冲消");

    private String type;
    private String name;


    private PermissionType(String type,String name){
        this.type=type;
        this.name=name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "PermissionType{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
