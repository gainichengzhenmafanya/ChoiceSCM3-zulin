package com.choice.framework.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

import com.choice.framework.domain.system.Account;

/**
 * extends UsernamePasswordToken for captcha
 *
 * @author <a href="http://www.micmiu.com">Michael Sun</a>
 */
public class ScmLoginToken extends UsernamePasswordToken {

	private static final long serialVersionUID = 1L;

	private String id;

	private String captcha;

	private String pkGroup;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getPkGroup() {
		return pkGroup;
	}

	public void setPkGroup(String pkGroup) {
		this.pkGroup = pkGroup;
	}

	public Account getAccount(){
		Account account = new Account();
		account.setId(this.getId());
		account.setName(this.getUsername());
		account.setPassword(String.valueOf(this.getPassword()));
		account.setPk_group(this.getPkGroup());
//		account.setValidateCode(this.getCaptcha());
		return account;
	}

	public ScmLoginToken() {
		super();

	}

	public ScmLoginToken(String username, char[] password,
                         boolean rememberMe, String host, String captcha, String pkGroup) {
		super(username, password, rememberMe, host);
		this.captcha = captcha;
		this.pkGroup=pkGroup;
	}

}