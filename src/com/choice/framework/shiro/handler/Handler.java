package com.choice.framework.shiro.handler;

/**
 * usedfor：回调函数
 * Created by javahao on 2017/8/8.
 * auth：JavaHao
 */
public interface Handler {
    /**
     * 回调执行方法
     * @return 返回执行状态
     */
    boolean execute();
}
