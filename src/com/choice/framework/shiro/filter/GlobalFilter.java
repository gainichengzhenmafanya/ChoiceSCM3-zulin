package com.choice.framework.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.PathMatchingFilter;

import com.choice.framework.shiro.tools.UserSpace;

public class GlobalFilter extends PathMatchingFilter {

    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        UserSpace.setRequest((HttpServletRequest) request);
        UserSpace.setResponse((HttpServletResponse) response);
        UserSpace.setSession(UserSpace.getRequest().getSession());
        UserSpace.switchDatasource();
        return true;
    }
}
