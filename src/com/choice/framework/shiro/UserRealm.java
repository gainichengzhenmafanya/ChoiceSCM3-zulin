package com.choice.framework.shiro;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.choice.common.servlet.Jdbconfig;
import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.AccountRole;
import com.choice.framework.exception.CRUDException;
import com.choice.framework.persistence.system.AccountMapper;
import com.choice.framework.shiro.exception.ActivationAccountException;
import com.choice.framework.shiro.exception.UnknownCompanyException;
import com.choice.framework.shiro.service.AuthorityInfoService;
import com.choice.framework.shiro.tools.AccountToken;
import com.choice.framework.shiro.tools.ShiroConstants;
import com.choice.framework.shiro.tools.SpringUtils;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.framework.vo.AccountCache;

public class UserRealm extends AuthorizingRealm {

	private final transient Log log = LogFactory.getLog(UserRealm.class);
	@Autowired
	private AccountMapper accountMapper;
	@Autowired
	private AuthorityInfoService authorityInfoService;

    /**
     * 权限获取
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//    	Principal principal = (Principal)principals.getPrimaryPrincipal();
        AccountCache accountCache = null;
        try {
            accountCache = authorityInfoService.loadAuthorityInfo(UserSpace.getAccount());
        } catch (CRUDException e) {
            e.printStackTrace();
        }
        
        UserSpace.getSession().setAttribute("pk_group",UserSpace.getAccount().getPk_group());
        //加载资源信息
        Set<String> permissions = new HashSet<String>();
        if(accountCache!=null&&accountCache.getModuleOperateMap()!=null){
            UserSpace.getSession().setAttribute("accountCache",accountCache);
            for (Map.Entry<String,HashMap<String,Boolean>> module:accountCache.getModuleOperateMap().entrySet()){
                for(Map.Entry<String,Boolean> operate : module.getValue().entrySet()){
                    if(operate.getValue())
                        permissions.add(module.getKey()+":"+operate.getKey());
                }
            }
        }
        //加载角色信息
        Set<String> roles = new HashSet<String>();
        if(accountCache!=null&&accountCache.getAccountRoleList()!=null){
            for (AccountRole ar : accountCache.getAccountRoleList()){
                roles.add(ar.getRoleId());
            }
        }
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(roles);
        authorizationInfo.setStringPermissions(permissions);
        return authorizationInfo;
    }

    /**
     * 身份认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        ScmLoginToken slt = (ScmLoginToken) token;

        String username = slt.getUsername();


        if(!SpringUtils.getBean(Jdbconfig.class).contains(slt.getPkGroup()))
            throw new UnknownCompanyException("未知企业ID");//未知企业ID

        Account user = null;
		try {
			user = accountMapper.findAccountByName(username);
		} catch (Exception e) {
			log.error("按照用户名验证账号失败！", e);
		}
        if(user == null) {
            throw new UnknownAccountException("未知账号");//没找到帐号
        }
        if(!ShiroConstants.ACCOUNT_STATUS_ACTIVE.equals(user.getState())) {
            throw new ActivationAccountException("账号未激活"); //帐号未激活
        }
        //更新登录token中当前登录用户的id信息
        slt.setId(user.getId());
        user.setPk_group(slt.getPkGroup());
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                new AccountToken(user,slt), //用户凭证
                user.getPassword(), //密码
                getName()  //realm name
        );
        return authenticationInfo;
    }
    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    @Override
    protected void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }

    @Override
    protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
        return UserSpace.getSessionId()+".authorizationCache";
    }

    @Override
    protected Object getAuthenticationCacheKey(AuthenticationToken token) {
        return UserSpace.getSessionId()+".authenticationCache";
    }

    @Override
    protected Object getAuthenticationCacheKey(PrincipalCollection principals) {
        return UserSpace.getSessionId()+".authenticationCache";
    }
}
