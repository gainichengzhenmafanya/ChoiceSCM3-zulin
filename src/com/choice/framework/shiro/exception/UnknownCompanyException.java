package com.choice.framework.shiro.exception;

import org.apache.shiro.authc.DisabledAccountException;

/**
 * 未知企业
 */
public class UnknownCompanyException extends DisabledAccountException {
	private static final long serialVersionUID = 1L;
	public UnknownCompanyException(){
	}
	public UnknownCompanyException(String message){
		super(message);
	}
	public UnknownCompanyException(Throwable cause){
		super(cause);
	}
	public UnknownCompanyException(String message, Throwable cause){
		super(message, cause);
	}
}
