package com.choice.framework.persistence.system;

import java.util.List;
import java.util.Map;

import com.choice.framework.domain.system.AccountFirm;
import com.choice.framework.domain.system.AccountSupply;

public interface AccountSupplyMapper {
	
	/**
	 * 根据账号查询物资
	 * @param PositnRole
	 * @throws CRUDException 
	 */
//	public List<AccountSupply> findSupplysBySupply(String accountId);
	
	/**
	 * 查询账号所有物资信息
	 * @param accountSupply
	 * @return
	 */
	public List<AccountSupply> findAccountSupply(AccountSupply accountSupply);
	
	/**
	 * 查询账号默认第一个分店信息
	 * @param account
	 * @return
	 */
	public AccountFirm findAccountFirmTop1(AccountFirm accountFirm);
	
	/**
	 * 保存账号具有的物资信息
	 * @param accountSupply
	 * @return
	 */
	public void saveAccountSupply(AccountSupply accountSupply);
	
	/**
	 * 删除账号具有的物资信息
	 * @param accountSupply
	 * @return
	 */
	public void deleteAccountSupply(AccountSupply accountSupply);
	
	/**
	 * 分配物资范围复制到账号
	 *  @param accountId sql
	 *
	 */
	public void copyToAccount(Map<String, Object> map);

	/***
	 * 判断是否此物资已对应此账号wjf
	 * @param newAccountSupply
	 * @return
	 */
	public int findOneAccountSupply(AccountSupply newAccountSupply);
	
}