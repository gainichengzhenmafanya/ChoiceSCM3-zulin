package com.choice.framework.persistence.system;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.choice.framework.domain.system.Account;
import com.choice.framework.domain.system.User;
import com.choice.tele.domain.Firm;

public interface UserMapper {
	/**
	 * 查询所有人员
	 * @param user
	 * @return
	 */
	public List<User> findUser(User user);
	
	/**
	 * 根据人员id查询人员
	 * @param id
	 * @return
	 */
	public User findUserById(String id);
	
	/**
	 * 查询人员及账号信息
	 * @param map
	 * @return
	 */
	public List<User> findUserAccount(HashMap<String, Object> map);
	
	/**
	 *查询所有账号
	 *@return 
	 */
	public List<Account> findAllUserAccount();
	
	/**根据userid获取firmid
	 * @param userId
	 * @return
	 */
	public List<String> findFirmId(String userId);
	
	/**根据firmid获取分店名称
	 * @param frimId
	 * @return
	 */
	public List<Firm> findFirmDes(@Param("frimId")String frimId);
	
	/**
	 * 保存人员
	 * @param user
	 */
	public void saveUser(User user);
	
	/**
	 * 更新人员
	 * @param user
	 */
	public void updateUser(User user);
	
	
	/**
	 * 删除人员
	 * @param listId
	 */
	public void deleteUser(List<String> listId);
	
	/**
	 *通过编码查看对应部门下是否存在数据 
	 */
	public User findUserByCode(String code);
	
}