package com.choice.framework.util;

import java.io.Closeable;
import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamUtils {
	
	private static final Logger LOG = LoggerFactory.getLogger(StreamUtils.class);

	public static final void safeClose(Closeable c) {
		if (c == null)
			return;
		try {
			c.close();
		} catch (Throwable localThrowable) {
			LOG.error(localThrowable.getMessage(), localThrowable);
		}
	}

	public static void safeClose(Connection conn) {
		if (conn == null)
			return;
		try {
			conn.close();
		} catch (Throwable localThrowable) {
			LOG.error(localThrowable.getMessage(), localThrowable);
		}
	}
}