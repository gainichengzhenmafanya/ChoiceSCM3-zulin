package com.choice.framework.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	private static final String format = "yyyyMMddHHmmss";

	private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>();

	public static DateFormat getDateFormat() {
		DateFormat df = threadLocal.get();
		if (df == null) {
			df = new SimpleDateFormat(format);
			threadLocal.set(df);
		}
		return df;
	}

	public static String formatDate(Date date) throws ParseException {
		return getDateFormat().format(date);
	}

	public static Date parse(String data) throws ParseException {
		return getDateFormat().parse(data);
	}

	public static Date curr() {
		return Calendar.getInstance().getTime();
	}

	public static String getNowDateString() {
		try {
			return formatDate(curr());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
