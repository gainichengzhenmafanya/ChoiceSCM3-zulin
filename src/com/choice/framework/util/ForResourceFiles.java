package com.choice.framework.util;

import static com.choice.assistant.constants.system.SysParamConstants.SBOH;
import static com.choice.assistant.constants.system.SysParamConstants.SCM;
import static com.choice.assistant.constants.system.SysParamConstants.SYS;
import static com.choice.assistant.constants.system.SysParamConstants.USER;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.choice.framework.exception.CRUDException;
import com.choice.framework.shiro.tools.SpringUtils;
import com.choice.framework.shiro.tools.UserSpace;
import com.choice.scm.service.SysParamService;

public class ForResourceFiles {

	@Autowired
	private static SysParamService paramService;
	
	private static final Logger LOG = LoggerFactory.getLogger(ForResourceFiles.class);

	public static String getValByKey(String fileName, String parameterName) {
		String resourceName = "config.properties";
		if ((fileName != null) && (!"".equals(fileName))) {
			resourceName = fileName;
		}
		InputStream inputStream = ForResourceFiles.class.getClassLoader().getResourceAsStream(resourceName);
		Properties properties = new Properties();
		String result = null;
		try {
			if ((parameterName != null) && (!"".equals(parameterName))) {
				properties.load(inputStream);
				result = properties.getProperty(parameterName);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		} finally {
			StreamUtils.safeClose(inputStream);
		}
		
		return result;
	}

	public static String getValBySQL(String parameterName) {
		Connection con = DbConn.getConn();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String enum_value = null;
		String sql = null;
		try {
			sql = "select * from dict where id= ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, parameterName);
			rs = ps.executeQuery();
			while (rs.next()) {
				enum_value = rs.getString(3);
			}
		} catch (SQLException e1) {
			LOG.error(e1.getMessage(), e1);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}
				if (con != null)
					con.close();
			} catch (SQLException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return enum_value;
	}

	/***
	 * 获取参数, 先从缓存, 再从DB
	 * @param type
	 * @param paramCode
	 * @return
	 */
	public static String getParamValByCodeFromCacheDB(String type, String paramCode) {
		try {
	
			if (paramService == null) {
				paramService = SpringUtils.getBean("sysParamService");
			}
			if(SCM.equals(type)) {
				return paramService.getScmParamByCodeStr(UserSpace.getToken().getPkGroup(), paramCode);
			}
			if(SBOH.equals(type)){
				return paramService.getSbohParamByCodeStr(UserSpace.getToken().getPkGroup(), paramCode);
			}
			if(SYS.equals(type)) {
				return paramService.getSysParamByCodeStr(paramCode);
			}
			if(USER.equals(type)) {
				return paramService.getUserParamByCodeStr(UserSpace.getAccount().getName(), paramCode);
			}
			
			return null;
		} catch (CRUDException e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}
}
